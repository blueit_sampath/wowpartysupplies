<?php
namespace Checkout\Option;

use Core\Item\Item;

class CartItem extends Item
{

    protected $_productId = '';

    protected $_title;

    protected $_image;

    protected $_quantity = 1;

    protected $_sellPrice;

    protected $_costPrice;

    protected $_listPrice;

    protected $_sku = '';

    protected $_params = array();

    protected $_inItems = array();

    protected $_outItems = array();

    /**
     *
     * @return string $_productId
     */
    public function getProductId()
    {
        return $this->_productId;
    }

    /**
     *
     * @param string $_productId            
     * @param
     *            class_name
     */
    public function setProductId($_productId)
    {
        $this->_productId = $_productId;
        return $this;
    }

    /**
     *
     * @return string $_title
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     *
     * @param field_type $_title            
     * @param
     *            class_name
     */
    public function setTitle($_title)
    {
        $this->_title = $_title;
        return $this;
    }

    /**
     *
     * @return string $_image
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     *
     * @param field_type $_image            
     * @param
     *            class_name
     */
    public function setImage($_image)
    {
        $this->_image = $_image;
        return $this;
    }

    /**
     *
     * @return string $_quantity
     */
    public function getQuantity()
    {
        return $this->_quantity;
    }

    /**
     *
     * @param number $_quantity            
     * @param
     *            class_name
     */
    public function setQuantity($_quantity)
    {
        $this->_quantity = $_quantity;
        return $this;
    }

    /**
     *
     * @return string $_sellPrice
     */
    public function getSellPrice()
    {
        return $this->_sellPrice;
    }

    /**
     *
     * @param field_type $_sellPrice            
     * @param
     *            class_name
     */
    public function setSellPrice($_sellPrice)
    {
        $this->_sellPrice = $_sellPrice;
        return $this;
    }

    /**
     *
     * @return string $_costPrice
     */
    public function getCostPrice()
    {
        return $this->_costPrice;
    }

    /**
     *
     * @param field_type $_costPrice            
     * @param
     *            class_name
     */
    public function setCostPrice($_costPrice)
    {
        $this->_costPrice = $_costPrice;
        return $this;
    }

    /**
     *
     * @return string $_listPrice
     */
    public function getListPrice()
    {
        return $this->_listPrice;
    }

    /**
     *
     * @param field_type $_listPrice            
     * @param
     *            class_name
     */
    public function setListPrice($_listPrice)
    {
        $this->_listPrice = $_listPrice;
        return $this;
    }

    /**
     *
     * @return multitype:CartInItem
     */
    public function getInItems()
    {
        return $this->sortArray($this->_inItems);
    }

    /**
     *
     * @param array $_inItems            
     * @return \Checkout\Option\CartContainer
     */
    public function setInItems($_inItems)
    {
        $this->_inItems = $_inItems;
        return $this;
    }

    /**
     *
     * @param CartInItem $item            
     * @return \Checkout\Option\CartContainer
     */
    public function addInItem(CartInItem $item)
    {
        $this->_inItems[$item->getId()] = $item;
        
        return $this;
    }

    /**
     *
     * @param string $id            
     * @param string $title            
     * @param string $value            
     * @param array $params            
     * @return \Checkout\Option\CartInItem
     */
    public function addQuickInItem($id, $title = '', $value = '', $params = array())
    {
        $item = new CartInItem($id);
        $item->setTitle($title);
        $item->setValue($value);
        $item->setParams($params);
        $this->addInItem($item);
        return $item;
    }

    /**
     *
     * @return CartInItem boolean
     */
    public function getInItem($key)
    {
        if (isset($this->_inItems[$key])) {
            return $this->_inItems[$key];
        }
        return false;
    }

    /**
     *
     * @param string $key            
     * @return boolean
     */
    public function removeInItem($key)
    {
        if (isset($this->_inItems[$key])) {
            unset($this->_inItems[$key]);
            return true;
        }
        return false;
    }

    /**
     *
     * @return multitype:CartOutItem
     */
    public function getOutItems()
    {
        return $this->sortArray($this->_outItems);
    }

    /**
     *
     * @param unknown $_outItems            
     * @return \Checkout\Option\CartContainer
     */
    public function setOutItems($_outItems)
    {
        $this->_outItems = $_outItems;
        return $this;
    }

    /**
     *
     * @param CartOutItem $item            
     * @return \Checkout\Option\CartContainer
     */
    public function addOutItem(CartOutItem $item)
    {
        $this->_outItems[$item->getId()] = $item;
        return $this;
    }

    /**
     *
     * @param string $id            
     * @param string $title            
     * @param string $value            
     * @param array $params            
     * @return \Checkout\Option\CartOutItem
     */
    public function addQuickOutItem($id, $title = '', $value = '', $params = array())
    {
        $item = new CartOutItem($id);
        $item->setTitle($title);
        $item->setValue($value);
        $item->setParams($params);
        $this->addInItem($item);
        return $item;
    }

    /**
     *
     * @param string $key            
     * @return CartOutItem boolean
     */
    public function getOutItem($key)
    {
        if (isset($this->_outItems[$key])) {
            return $this->_outItems[$key];
        }
        return false;
    }

    /**
     *
     * @param string $key            
     * @return boolean
     */
    public function removeOutItem($key)
    {
        if (isset($this->_outItems[$key])) {
            unset($this->_outItems[$key]);
            return true;
        }
        return false;
    }

    /**
     *
     * @return string $_params
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     *
     * @param array $_params            
     * @return \Checkout\Option\CartContainer
     */
    public function setParams($_params)
    {
        $this->_params = $_params;
        return $this;
    }

    /**
     *
     * @param string $key            
     * @param multitype $value            
     * @return \Checkout\Option\CartContainer
     */
    public function addParam($key, $value)
    {
        $this->_params[$key] = $value;
        
        return $this;
    }

    /**
     *
     * @param string $key            
     * @return multitype: boolean
     */
    public function getParam($key)
    {
        if (isset($this->_params[$key])) {
            return $this->_params[$key];
        }
        return false;
    }

    /**
     *
     * @param string $key            
     * @return boolean
     */
    public function removeParam($key)
    {
        if (isset($this->_params[$key])) {
            unset($this->_params[$key]);
            return true;
        }
        return false;
    }

    /**
     *
     * @return string
     */
    public function getGeneratedKey()
    {
        $array['productId'] = $this->getProductId();
        if (! $this->getProductId()) {
            $array['sellPrice'] = $this->getSellPrice();
            $array['costPrice'] = $this->getSellPrice();
            $array['sellPrice'] = $this->getSellPrice();
        }
        $array['sku'] = $this->getSku();
        $array['title'] = $this->getTitle();
        $array['inItems'] = $this->_inItems;
        return md5(serialize($array));
    }

    public function merge(CartItem $item)
    {
        $qty = $this->getQuantity() + $item->getQuantity();
        $this->setQuantity($qty);
        
        $image = $item->getImage();
        if ($image) {
            $this->setImage($image);
        }
        $sku = $item->getSku();
        if ($sku) {
            $this->setSku($sku);
        }
        $outItems = $item->getOutItems();
        if (count($outItems)) {
            $oi = $this->getOutItems();
            $this->setOutItems($oi + $outItems);
        }
        $params = $item->getParams();
        if (count($params)) {
            $oi = $this->getParams();
            $this->setParams($oi + $params);
        }
        return $this;
    }

    public function getUnitPrice()
    {
        $price = $this->getSellPrice();
        $inItmes = $this->getInItems();
        if ($inItmes) {
            foreach ($inItmes as $inItem) {
                $value = $inItem->getValue();
                if ($inItem->getIncludePrice()) {
                    if (is_numeric($value)) {
                        $price = $price + $value;
                    }
                }
            }
        }
        return $price;
    }

    public function getTotalPrice()
    {
        $price = $this->getUnitPrice();
        $price = $price * $this->getQuantity();
        $outItems = $this->getOutItems();
        if ($outItems) {
            foreach ($outItems as $outItem) {
                $value = $outItem->getValue();
                if ($outItem->getIncludePrice()) {
                    if (is_numeric($value)) {
                        $price = $price + $value;
                    }
                }
            }
        }
        return $price;
    }

    /**
     *
     * @return string $_sku
     */
    public function getSku()
    {
        return $this->_sku;
    }

    /**
     *
     * @param string $_sku            
     * @param
     *            class_name
     */
    public function setSku($_sku)
    {
        $this->_sku = $_sku;
        return $this;
    }
}
