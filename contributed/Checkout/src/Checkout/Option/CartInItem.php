<?php

namespace Checkout\Option;

use Core\Item\Item;
use Core\Functions;

class CartInItem extends Item {
	protected $_value = null;
	protected $_title = '';
	protected $_params = array();
	protected $_includePrice = true;
	protected $_isCurrency = true;
	/**
	 * @return the $_isCurrency
	 */
	public function getIsCurrency() {
		return $this->_isCurrency;
	}

	/**
	 * @param boolean $_isCurrency
	 */
	public function setIsCurrency($_isCurrency) {
		$this->_isCurrency = $_isCurrency;
	}

	/**
	 * @return the $_includePrice
	 */
	public function getIncludePrice() {
		return $this->_includePrice;
	}

	/**
	 * @param boolean $_includePrice
	 */
	public function setIncludePrice($_includePrice) {
		$this->_includePrice = $_includePrice;
		return $this;
	}

	/**
	 * @return the $_title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * @param string $_title
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
	}

	
	/**
	 * @return string $_params
	 */
	public function getParams() {
		return $this->sortArray($this->_params);
	}
	
	/**
	 * @param multitype: $_params
	 * @param class_name
	 */
	public function setParams($_params) {
		$this->_params = $_params;
		return $this;
	}
	
	public function addParam($key, $value) {
			
		$this->_params[$key] = $value;
	
		return $this;
	}
	public function getParam($key) {
		if (isset ( $this->_params [$key] )) {
			return $this->_params [$key];
		}
		return false;
	}
	
	public function removeParam($key) {
		if (isset ( $this->_params [$key] )) {
			unset ( $this->_params[$key] );
			return true;
		}
		return false;
	}
	
	/**
	 * @return string $_value
	 */
	public function getValue() {
		return $this->_value;
	}

	/**
	 * @param NULL $_value
	 * @param class_name
	 */
	public function setValue($_value) {
		$this->_value = $_value;
		return $this;
	}

	
	public function displayValue(){
	    $sm  = Functions::getViewHelperManager();
	   $value = $this->getValue();
	   if($value && is_numeric($value) && $this->getIsCurrency()){
	       $currencyFormat = $sm->get('currencyFormat');
	       $value = $currencyFormat($value);
	   } 
	   return $value;
	}
	
	
}
