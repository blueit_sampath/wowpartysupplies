<?php

namespace Checkout\Option;

use Checkout\Option\CartItem;
use Checkout\Option\CartContainer;
use Checkout\Option\Validation\ValidationContainer;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

class CartEvent extends AbstractEvent {
	protected $validationContainer = null;
	protected $cart = null;
	protected $cartItem = null;
	public function getEventName() {
		return '';
	}
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$priority = $this->getPriority ();
		$eventName = $this->getEventName ();
		
		$sharedEventManager = $events->getSharedManager ();
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'validate-cart', array (
				$this,
				'preValidateCart' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'validate-cart-item', array (
				$this,
				'preValidateCartItem' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'add-cart-item', array (
				$this,
				'preAddCartItem' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'update-cart-item', array (
				$this,
				'preUpdateCartItem' 
		), $priority );
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'prepare-cart-item', array (
				$this,
				'prePrepareCartItem' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'remove-cart-item', array (
				$this,
				'preRemoveCartItem' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'prepare-cart', array (
				$this,
				'prePrepareCart' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'clear-cart', array (
				$this,
				'preClearCart' 
		), $priority );
		
		return $this;
	}
	public function preValidateCart($e) {
		$validationContainer = $e->getParam ( 'validationContainer' );
		$this->setValidationContainer ( $validationContainer );
		$cart = $e->getTarget ();
		$this->setCart ( $cart );
		return $this->validateCart ();
	}
	protected function validateCart() {
	}
	public function preValidateCartItem($e) {
		$validationContainer = $e->getParam ( 'validationContainer' );
		$this->setValidationContainer ( $validationContainer );
		$cartItem = $e->getParam ( 'cartItem' );
		$this->setCartItem ( $cartItem );
		$cart = $e->getTarget ();
		$this->setCart ( $cart );
		return $this->validateCartItem ();
	}
	protected function validateCartItem() {
	}
	public function preAddCartItem($e) {
		$cartItem = $e->getParam ( 'cartItem' );
		$this->setCartItem ( $cartItem );
		$cart = $e->getTarget ();
		$this->setCart ( $cart );
		return $this->addCartItem ();
	}
	protected function addCartItem() {
	}
	public function preUpdateCartItem($e) {
		$cartItem = $e->getParam ( 'cartItem' );
		$this->setCartItem ( $cartItem );
		$cart = $e->getTarget ();
		$this->setCart ( $cart );
		return $this->addCartItem ();
	}
	protected function updateCartItem() {
	}
	public function prePrepareCartItem($e) {
		$cartItem = $e->getParam ( 'cartItem' );
		$this->setCartItem ( $cartItem );
		$cart = $e->getTarget ();
		$this->setCart ( $cart );
		return $this->prepareCartItem ();
	}
	protected function prepareCartItem() {
	}
	public function preRemoveCartItem($e) {
		$cartItem = $e->getParam ( 'cartItem' );
		$this->setCartItem ( $cartItem );
		$cart = $e->getTarget ();
		$this->setCart ( $cart );
		return $this->removeCartItem ();
	}
	protected function removeCartItem() {
	}
	public function prePrepareCart($e) {
		$cart = $e->getTarget ();
		$this->setCart ( $cart );
		return $this->prepareCart ();
	}
	protected function prepareCart() {
	}
	public function preClearCart($e) {
		$cart = $e->getTarget ();
		$this->setCart ( $cart );
		return $this->clearCart ();
	}
	protected function clearCart() {
	}
	
	/**
	 *
	 * @return ValidationContainer $validationContainer
	 */
	public function getValidationContainer() {
		return $this->validationContainer;
	}
	
	/**
	 *
	 * @param NULL $validationContainer        	
	 * @param
	 *        	class_name
	 */
	public function setValidationContainer($validationContainer) {
		$this->validationContainer = $validationContainer;
		return $this;
	}
	
	/**
	 *
	 * @return CartContainer $cart
	 */
	public function getCart() {
		return $this->cart;
	}
	
	/**
	 *
	 * @param NULL $cart        	
	 * @param
	 *        	class_name
	 */
	public function setCart($cart) {
		$this->cart = $cart;
		return $this;
	}
	
	/**
	 *
	 * @return CartItem $cartItem
	 */
	public function getCartItem() {
		return $this->cartItem;
	}
	
	/**
	 *
	 * @param NULL $cartItem        	
	 * @param
	 *        	class_name
	 */
	public function setCartItem($cartItem) {
		$this->cartItem = $cartItem;
		return $this;
	}
}
