<?php

namespace Checkout\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\View\Helper\AbstractHelper;

class ProductAddToCartForm extends AbstractHelper implements ServiceLocatorAwareInterface, EventManagerAwareInterface {

    protected $_services;
    protected $_eventName;
    protected $_events;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->_services = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->_services->getServiceLocator();
    }

    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    public function getEventManager() {
        if (null === $this->_events) {
            $this->setEventManager(new EventManager());
        }
        return $this->_events;
    }

    public function __invoke($productId = 1) {
        $sm = $this->getServiceLocator();
        $form = $sm->get('Checkout\Form\ProductAddToCart\ProductAddToCartFactory');

        $cart = $sm->get('Cart');
        $form->get('product')->setValue($productId);
        $form->triggerAttachFormElements();

        $array = array(
            'productId' => $productId
        );
        $cartItemId = \Core\Functions::fromQuery('editCartItemId');
        if ($cartItemId) {
            $array['editCartItemId'] = $cartItemId;
            $existingCartItem = $cart->getCartItem($cartItemId);
            if ($existingCartItem) {
                $formData = $existingCartItem->getParam('formData');

                if (!is_array($formData)) {
                    $formData = array();
                }
                $formData['quantity'] = $existingCartItem->getQuantity();
                unset($formData['btnsubmit']);
                $form->setData($formData);
            }
        }
        $form->setAttribute('action', $this->getView()
                        ->url('addtocart', $array));

        return $form;
    }

}
