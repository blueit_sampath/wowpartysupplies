<?php

namespace Checkout\Controller;

use Zend\View\Model\ViewModel;
use Common\MVC\Controller\AbstractAdminController;
use Core\Functions;
use Common\MVC\Controller\AbstractFrontController;
use Checkout\Option\CartItem;
use Product\Api\ProductApi;
use Checkout\Option\CartContainer;
use ProductCategory;
use Product\Entity\ProductImage;

class CartController extends AbstractFrontController {

    /**
     *
     * @return CartContainer
     */
    public function getCart() {
        return $this->getServiceLocator()->get('Cart');
    }

    public function editAction() {
        
    }

    public function addtocartAction() {
        $productId = Functions::fromRoute('productId', 0);
        $quantity = Functions::fromRoute('quantity', 1);

        $isAjax = Functions::fromRoute('isAjax', 0);
        if ($productId) {
            if ($this->getRequest()->isPost()) {
                $form = $this->getServiceLocator()->get('Checkout\Form\ProductAddToCart\ProductAddToCartFactory');
                $form->get('product')->setValue($productId);
                $form->setData($this->params()->fromPost());
                $array = array('isAjax' => $isAjax, 'isError' => true, 'cartItem' => null, 'productId' => $productId);
                if ($form->isValid()) {
                    $form->save();
                    $data = $form->getData();
                    $result = $this->prepareCartItem($productId, $data['quantity'], $data);
                    $array['isError'] = true;
                    if (!$result) {
                        if (!$isAjax) {

                            return $this->redirect()->toRoute('product', array(
                                        'productId' => $productId
                            ));
                        }
                    }
                    $array['cartItem'] = $result;
                } else {
                    $this->addFormErrorMessages($form);
                }
            } else {
                $this->prepareCartItem($productId, $quantity);
            }
        }
        if (!$isAjax) {
            return $this->redirect()->toRoute('cart');
        }

        return $array;
    }

    public function previewAction() {
        $productId = Functions::fromRoute('productId', 0);
        $quantity = Functions::fromRoute('quantity', 1);
        $cartItem = null;

        if ($productId) {
            if ($this->getRequest()->isPost()) {
                $form = $this->getServiceLocator()->get('Checkout\Form\ProductAddToCart\ProductAddToCartFactory');
                $form->get('product')->setValue($productId);
                $form->setData($this->getRequest()->getPost());
                $form->isValid();
                $data = $form->getData();
                $cartItem = $this->prepareCartItem($productId, $data['quantity'], $data, true);
            }
        }
        $viewModel = new ViewModel(array(
            'cartItem' => $cartItem,
            'form' => $form
        ));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function quickaddtocartAction() {
        $productId = Functions::fromRoute('productId', Functions::fromQuery('productId', 0));
        $isAjax = Functions::fromRoute('isAjax', 0);

        $array = array('isAjax' => $isAjax, 'isError' => true, 'cartItem' => null);
        if ($productId) {

            $form = $this->getServiceLocator()->get('Checkout\Form\ProductAddToCart\ProductAddToCartFactory');
            $form->get('product')->setValue($productId);
            $form->setData($this->params()->fromQuery());
            if ($form->isValid()) {
                $form->save();
                $data = $form->getData();
                $result = $this->prepareCartItem($productId, $data['quantity'], $data);
                if (!$result) {
                    $array['isError'] = true;
                    if (!$isAjax) {
                        return $this->redirect()->toRoute('product', array(
                                    'productId' => $productId
                        ));
                    }
                }
                $array['cartItem'] = $result;
            } else {

                return $this->redirect()->toRoute('product-quickview', array(
                            'productId' => $productId
                ));
            }
        }
        return $array;
    }

    public function prepareCartItem($productId, $quantity, $data = array(), $preview = false) {
        $cart = $this->getCart();
        $product = ProductApi::getProductById($productId);
        if (!$product) {
            return;
        }
        $cartItem = new CartItem(time() . '_' . $productId); // dummy id it is not used
        $cartItem->addParam('formData', $data);
        $cartItem->setProductId($productId);
        $cartItem->setTitle($product->title);
        $cartItem->setSellPrice($product->sellPrice);
        $cartItem->setListPrice($product->listPrice);
        $cartItem->setCostPrice($product->costPrice);
        $cartItem->setSku($product->sku);
        if (isset($data['image']) && $data['image']) {
            $cartItem->setImage($data['image']);
        } else {
            $productImage = ProductApi::getTopProductImageByProductId($productId);
            if ($productImage && $productImage->file && $productImage->file->path) {
                $cartItem->setImage($productImage->file->path);
            }
        }
        $cartItem->setQuantity($quantity);
        $result = $cart->addCartItem($cartItem, $preview);
        if (!$result) {
            return $result;
        }
        if ($preview) {
            return $result;
        }

        /* removes the cart item which is to edited => virtually adding a new cart item and removes the already existing one */
        $editCartItemId = $this->params()->fromRoute('editCartItemId');
        if ($editCartItemId) {
            $cart->removeCartItem($editCartItemId);
        }
        return $result;
    }

    public function cartAction() {
        return array();
    }

    public function removecartitemAction() {
        $cartItemId = Functions::fromRoute('cartItemId', 0);
        $cart = $this->getCart();
        $cart->removeCartItem($cartItemId);
        return $this->redirect()->toRoute('cart');
    }

    public function updatequantityAction() {
        $cartItemId = Functions::fromRoute('cartItemId', 0);
        $quantity = Functions::fromQuery('quantity', 1);
        $cart = $this->getCart();
        $cart->updateQuantity($cartItemId, $quantity);
        return $this->redirect()->toRoute('cart');
    }

    public function loadminiAction() {
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel;
    }

}
