<?php
namespace Checkout\Form\ProductAddToCart;

use Common\Form\Form;

class ProductAddToCartForm extends Form

{

    public function init()
    {
        $this->add(array(
            'name' => 'product',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
        
        $this->add(array(
            'name' => 'image',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
        
        $this->add(array(
            'name' => 'quantity',
            'attributes' => array(
                'type' => 'text',
                'value' => 1
            ),
            'options' => array(
                'label' => 'Quantity'
            )
        ), array(
            'priority' => - 100
        ));
        
        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'class' => 'addtocartsubmit'
            )
        ), array(
            'priority' => - 100
        ));
    }

    public function hasFieldsets()
    {
        
       
        $fieldsets = $this->getFieldsets();
       
        
        if ($fieldsets && count($fieldsets)) {
            
          foreach($fieldsets as $fieldset){
              $elements = $fieldset->getElements();

              if($elements && count($elements)){
                  return true;
              }
              
          }
            return true;
        }
        return false;
    }
}