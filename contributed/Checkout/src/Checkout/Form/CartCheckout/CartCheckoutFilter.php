<?php

namespace Checkout\Form\CartCheckout;


use Zend\InputFilter\InputFilter;
use Config\Api\ConfigApi;

class CartCheckoutFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$value = ConfigApi::getConfigByKey ( 'ORDER_TC' );
		if ($value) {
			
			$this->add ( array (
					'name' => 'accept',
					'required' => true 
			)
			 );
		}
		
		$this->add ( array (
				'name' => 'comment',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
	}
} 