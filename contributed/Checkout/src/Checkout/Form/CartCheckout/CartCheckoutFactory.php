<?php
namespace Checkout\Form\CartCheckout;

use Common\Form\Option\AbstractFormFactory;

class CartCheckoutFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Checkout\Form\CartCheckout\CartCheckoutForm' );
		$form->setName ( 'cartCheckout' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Checkout\Form\CartCheckout\CartCheckoutFilter' );
	}
}
