<?php

namespace Checkout\Form\EditCheckoutAddress;

use Zend\InputFilter\InputFilter;
use Zend\Validator\Regex;

class EditCheckoutAddressFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
            $this->add ( array (
				'name' => 'formatedAddress',
				'required' => false,
                ));
		$this->add ( array (
				'name' => 'firstName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => "/^[a-zA-Z ,.'-]+$/",
										'messages' => array (
												Regex::NOT_MATCH => 'Use Letters & periods' 
										) 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => "/^[a-zA-Z ,.'-]+$/",
										'messages' => array (
												Regex::NOT_MATCH => 'Use Letters & periods' 
										) 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'email',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'EmailAddress' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'contactNumber',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => '/^(?!.*-.*-.*-)(?=(?:\d{8,13}$)|(?:(?=.{9,13}$)[^-]*-[^-]*$)|(?:(?=.{10,13}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/',
										'messages' => array (
												Regex::NOT_MATCH => 'Invalid Phone number' 
										) 
								) 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'addressLine1',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		)
		 );
		
		$this->add ( array (
				'name' => 'addressLine2',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'city',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'state',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'country',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'LocaleElement\Form\Filter\Country' 
						),
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				)
				 
		) );
		$this->add ( array (
				'name' => 'postcode',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'isDefault',
				'required' => false 
		) );
		$this->add ( array (
				'name' => 'addressId',
				'required' => false 
		) );
	}
} 