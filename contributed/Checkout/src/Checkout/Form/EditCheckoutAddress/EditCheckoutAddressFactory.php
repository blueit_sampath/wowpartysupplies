<?php
namespace Checkout\Form\EditCheckoutAddress;

use Common\Form\Option\AbstractFormFactory;

class EditCheckoutAddressFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressForm' );
		$form->setName ( 'editCheckoutAddress' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressFilter' );
	}
}
