<?php

namespace Checkout\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Checkout\Option\CartContainer;
use User\Api\UserApi;
use User\Entity\UserAddress;

class EditCheckoutAddress extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'firstName',
			'lastName',
			'email',
			'contactNumber',
			'city',
			'country',
			'state',
			'addressLine1',
			'addressLine2',
			'postcode' 
	);
	public function getFormName() {
		return 'editCheckoutAddress';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		$user = UserApi::getLoggedInUser ();
		$cart = $this->getCart ();
		$type = Functions::fromRoute ( 'type' );
		$user = UserApi::getLoggedInUser ();
		if ($user) {
			$params['id'] = $params['addressId'];
			$entity = UserApi::saveAddress($params, $user->id);
			$params['id'] =$entity->id;
		}
		
		$cart->addParam ( $type, $params );
	}
	public function getRecord() {
		$form = $this->getForm ();
		$id = $this->getId ();
		$cart = $this->getCart ();
		$type = Functions::fromRoute ( 'type' );
		$values = $cart->getParam ( $type );
		
		$addressId = Functions::fromQuery ( 'addressId', Functions::fromRoute ( 'addressId', null ) );
		if ($addressId !== null) {
			if ($addressId) {
				$entity = UserApi::getAddressById ( $addressId );
				if (! $entity) {
					return;
				}
				foreach ( $this->_columnKeys as $columnKey ) {
					try {
						if ($element = $form->get ( $columnKey )) {
							$element->setValue ( $entity->$columnKey );
						}
					} catch ( \Exception $e ) {
					}
				}
				$form->get ( 'addressId' )->setValue ( $addressId );
			}
			return;
		}
		
		if (! $values) {
			return;
		}
		foreach ( $this->_columnKeys as $columnKey ) {
			try {
				if ($element = $form->get ( $columnKey )) {
					$element->setValue ( $values [$columnKey] );
				}
			} catch ( \Exception $e ) {
			}
		}
		
		return true;
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	public function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
