<?php

namespace Checkout\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;
use Checkout\Option\CartContainer;

class CheckoutAddress extends AbstractMainFormEvent {
	public function getFormName() {
		return 'checkoutAddress';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		
		$user = UserApi::getLoggedInUser ();
		$cart = $this->getCart ();
		$shippingAddress = $params ['shippingAddress'];
		$billingAddress = $params ['billingAddress'];
		if ($shippingAddress ['copyAddress']) {
			$billingAddress = $shippingAddress;
		}
		
		if ($user) {
			$entity = UserApi::saveAddress ( $shippingAddress, $user->id );
			$billingAddress ['id'] = $entity->id;
			$shippingAddress ['id'] = $entity->id;
			if (! $shippingAddress ['copyAddress']) {
				$entity = UserApi::saveAddress ( $billingAddress, $user->id );
				$billingAddress ['id'] = $entity->id;
			}
		}
		$cart->addParam ( 'shippingAddress', $shippingAddress );
		$cart->addParam ( 'billingAddress', $billingAddress );
	}
	/**
	 *
	 * @return CartContainer
	 */
	public function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
