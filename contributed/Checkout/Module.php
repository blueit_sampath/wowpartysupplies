<?php

namespace Checkout;

use Core\Functions;

class Module {

    public function onBootstrap($e) {
        $em = $e->getApplication()->getEventManager();
        $em->attach(\Zend\Mvc\MvcEvent::EVENT_FINISH, array(
            $this,
            'saveCart'
        ));
        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
                    $this,
                    'attachAssets'
        ));
       
    }

    public function attachAssets($e) {
        $controller = $e->getTarget();

        if (!($controller instanceof \Zend\Mvc\Controller\AbstractActionController)) {
            return;
        }
        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();
        $viewHelperPluginManager = $serviceManager->get('view_helper_manager');
        $headScript = $viewHelperPluginManager->get('headScript');
        $basePath = $viewHelperPluginManager->get('basePath');
        $headScript->appendFile($basePath('/assets/Checkout/cart_events.js'));
    }

    public function saveCart($e) {
        $sm = Functions::getServiceLocator();
        $cart = $sm->get('Cart');
        $cart->save();
        return $cart;
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

}
