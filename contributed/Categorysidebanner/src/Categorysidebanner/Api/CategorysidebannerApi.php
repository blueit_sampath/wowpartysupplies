<?php

namespace Categorysidebanner\Api;

use Core\Functions;
use Common\Api\Api;

class CategorysidebannerApi extends Api {

    protected static $_entity = '\Categorysidebanner\Entity\Categorysidebanner';

    public static function getCategorysidebannerById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getCategorysidebannersByCategoryId($categoryId, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->setEventName('CategorysidebannerApi.getCategorysidebanners');
        $queryBuilder->addColumn('categorysidebanner.id', 'categorysidebannerId');

        if ($status !== null) {
            $queryBuilder->addWhere('categorysidebanner.status', 'categorysidebannerStatus');
            $queryBuilder->addParameter('categorysidebannerStatus', $status);
        }

        if (!$categoryId) {
            $queryBuilder->addWhere('categorysidebanner.category', 'categorysidebannerCategory',null,true);
        } else {
            $queryBuilder->addWhere('categorysidebanner.category', 'categorysidebannerCategory');
            $queryBuilder->addParameter('categorysidebannerCategory', $categoryId);
        }

        $queryBuilder->addOrder('categorysidebanner.weight', 'categorysidebannerWeight', 'desc');
        $queryBuilder->addGroup('categorysidebanner.id', 'categorysidebannerId');
        $item = $queryBuilder->addFrom(static::$_entity, 'categorysidebanner');
        return $queryBuilder->executeQuery();
    }

    public static function getSortedArray() {
        $array = static::getCategorysidebanners(null);
        return static::makeImageArray($array);
    }

    public static function makeImageArray($array) {
        $result = array();
        foreach ($array as $categorysidebannerResults) {
            $categorysidebannerId = $categorysidebannerResults ['categorysidebannerId'];
            $categorysidebanner = static::getCategorysidebannerById($categorysidebannerId);
            $result [$categorysidebanner->id] = array(
                'name' => $categorysidebanner->file->path
            );
        }
        return $result;
    }

}
