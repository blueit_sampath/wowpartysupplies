<?php

namespace Categorysidebanner\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategorysidebanner extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$categoryId = Functions::fromRoute ( 'categoryId' );
		if (! $categoryId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-categorysidebanner', array (
				'categoryId' => $categoryId 
		) );
		
                $tabContainer->add ( 'admin-categorysidebanner', 'Side Banners', $u, 900 );
		
		
		return $this;
	}
}

