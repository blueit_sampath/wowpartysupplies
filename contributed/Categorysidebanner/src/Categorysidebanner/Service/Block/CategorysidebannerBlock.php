<?php 
namespace Categorysidebanner\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class CategorysidebannerBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'categorysidebanner-block';

	public function getBlockName() {
		return 'categorysidebannerBlock';
	}
        public function getTitle() {
           return 'Category Side Banner Block';
        }
	public function getPriority() {
		return 1000;
	}
	
}

