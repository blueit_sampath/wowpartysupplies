<?php

namespace Categorysidebanner\Service\NestedSorting;

use Core\Functions;
use Categorysidebanner\Api\CategorysidebannerApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminCategorysidebanner extends AbstractNestedSortingEvent {
	protected $_entityName = '\Categorysidebanner\Entity\Categorysidebanner';
	protected $_column = 'path';
	public function getEventName() {
		return 'adminCategorysidebannerSort';
	}
	public function getPriority() {
		return 1000;
	}
	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (), array (
				'weight' => 'desc' 
		) );
		if ($results) {
			$column = $this->_column;
			foreach ( $results as $result ) {
				if ($result->file) {
					$array [$result->id] = array (
							'name' => $result->file->$column 
					);
				}
			}
		}
		return $array;
	}
        
        
}
