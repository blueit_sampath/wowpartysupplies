<?php
namespace Categorybanner\Form\AdminCategorybanner;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CategorybannerForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
                $this->add ( array (
				'name' => 'category',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'title',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Title' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'url',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'URL'
				)
		), array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'alt',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Alt Text' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Priority' 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->add ( array (
				'name' => 'image',
				'type' => 'File\Form\Element\FileUpload' 
		)
		, array (
				'priority' => 960 
		) );
		
		
		$this->getStatus ( 950 );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}