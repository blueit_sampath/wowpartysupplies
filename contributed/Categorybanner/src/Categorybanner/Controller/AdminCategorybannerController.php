<?php

namespace Categorybanner\Controller;

use Common\MVC\Controller\AbstractAdminController;
use Core\Functions;
use Zend\View\Model\ViewModel;

class AdminCategorybannerController extends AbstractAdminController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('Categorybanner\Form\AdminCategorybanner\CategorybannerFormFactory');
    }

    public function getSaveRedirector() {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-categorybanner-add', array(
                    'categoryId' => $form->get('category')->getValue(),
                    'categorybannerId' => $form->get('id')->getValue()
                ));
    }

}
