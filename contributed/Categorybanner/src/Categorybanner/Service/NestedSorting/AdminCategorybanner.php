<?php

namespace Categorybanner\Service\NestedSorting;

use Core\Functions;
use Categorybanner\Api\CategorybannerApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminCategorybanner extends AbstractNestedSortingEvent {
	protected $_entityName = '\Categorybanner\Entity\Categorybanner';
	protected $_column = 'path';
	public function getEventName() {
		return 'adminCategorybannerSort';
	}
	public function getPriority() {
		return 1000;
	}
	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (), array (
				'weight' => 'desc' 
		) );
		if ($results) {
			$column = $this->_column;
			foreach ( $results as $result ) {
				if ($result->file) {
					$array [$result->id] = array (
							'name' => $result->file->$column 
					);
				}
			}
		}
		return $array;
	}
        
        
}
