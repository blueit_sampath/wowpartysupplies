<?php

namespace Categorybanner\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategorybanner extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$categoryId = Functions::fromRoute ( 'categoryId' );
		if (! $categoryId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-categorybanner', array (
				'categoryId' => $categoryId 
		) );
		
                $tabContainer->add ( 'admin-categorybanner', 'Banners', $u, 900 );
		
		
		return $this;
	}
}

