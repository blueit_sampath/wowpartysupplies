<?php 
namespace Categorybanner\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class CategorybannerBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'categorybanner-block';

	public function getBlockName() {
		return 'categorybannerBlock';
	}
        public function getTitle() {
           return 'Category Banner Block';
        }
	public function getPriority() {
		return 1000;
	}
	
}

