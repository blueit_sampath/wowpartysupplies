<?php

namespace Categorybanner\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategorybanner extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategorybanner';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		
		$url = Functions::getUrlPlugin ();
		$categoryId = Functions::fromRoute('categoryId',0);
		$u = $url ( 'admin-categorybanner-add',array('categoryId' => $categoryId) );
		$item = $linkContainer->add ( 'admin-categorybanner-add', 'Add Banner', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
//		$u = $url ( 'admin-categorybanner-sort',array('categoryId' => $categoryId) );
//		$item = $linkContainer->add ( 'admin-categorybanner-sort', 'Arrange Banners', $u, 990 );
//		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

