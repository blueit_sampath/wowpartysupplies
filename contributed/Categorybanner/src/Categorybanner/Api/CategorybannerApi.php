<?php

namespace Categorybanner\Api;

use Core\Functions;
use Common\Api\Api;

class CategorybannerApi extends Api {

    protected static $_entity = '\Categorybanner\Entity\Categorybanner';

    public static function getCategorybannerById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getCategorybannersByCategoryId($categoryId, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->setEventName('CategorybannerApi.getCategorybanners');
        $queryBuilder->addColumn('categorybanner.id', 'categorybannerId');

        if ($status !== null) {
            $queryBuilder->addWhere('categorybanner.status', 'categorybannerStatus');
            $queryBuilder->addParameter('categorybannerStatus', $status);
        }

        if (!$categoryId) {
              $item = $queryBuilder->addWhere('categorybanner.category', 'category1',null,true);
        } else {

            $queryBuilder->addWhere('categorybanner.category', 'categorybannerCategory');
            $queryBuilder->addParameter('categorybannerCategory', $categoryId);
        }

        $queryBuilder->addOrder('categorybanner.weight', 'categorybannerWeight', 'desc');
        $queryBuilder->addGroup('categorybanner.id', 'categorybannerId');
        $item = $queryBuilder->addFrom(static::$_entity, 'categorybanner');
        return $queryBuilder->executeQuery();
    }

    public static function getSortedArray() {
        $array = static::getCategorybanners(null);
        return static::makeImageArray($array);
    }

    public static function makeImageArray($array) {
        $result = array();
        foreach ($array as $categorybannerResults) {
            $categorybannerId = $categorybannerResults ['categorybannerId'];
            $categorybanner = static::getCategorybannerById($categorybannerId);
            $result [$categorybanner->id] = array(
                'name' => $categorybanner->file->path
            );
        }
        return $result;
    }

}
