<?php

return array(
    'router' => array(
        'routes' => array(
            'admin-categorybanner' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/categorybanner[/ajax/:ajax][/category/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Categorybanner\Controller\AdminCategorybanner',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-categorybanner-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/categorybanner/add[/ajax/:ajax][/category/:categoryId][/:categorybannerId]',
                    'defaults' => array(
                        'controller' => 'Categorybanner\Controller\AdminCategorybanner',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categorybannerId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-categorybanner-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/categorybanner/sort[/ajax/:ajax][/category/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Categorybanner\Controller\AdminCategorybanner',
                        'action' => 'sort',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Categorybanner\Service\Grid\AdminCategorybanner' => 'Categorybanner\Service\Grid\AdminCategorybanner',
        'Categorybanner\Service\Form\AdminCategorybanner' => 'Categorybanner\Service\Form\AdminCategorybanner',
        'Categorybanner\Service\Tab\AdminCategorybanner' => 'Categorybanner\Service\Tab\AdminCategorybanner',
        'Categorybanner\Service\Link\AdminCategorybanner' => 'Categorybanner\Service\Link\AdminCategorybanner',
        'Categorybanner\Service\NestedSorting\AdminCategorybanner' => 'Categorybanner\Service\NestedSorting\AdminCategorybanner',
        'Categorybanner\Service\Block\CategorybannerBlock' => 'Categorybanner\Service\Block\CategorybannerBlock',
        'Categorybanner\Service\Tab\AdminCategorybannerConfig' => 'Categorybanner\Service\Tab\AdminCategorybannerConfig',
    ),
    'service_manager' => array(
        'invokables' => array(
            'Categorybanner\Service\Grid\AdminCategorybanner' => 'Categorybanner\Service\Grid\AdminCategorybanner',
            'Categorybanner\Service\Form\AdminCategorybanner' => 'Categorybanner\Service\Form\AdminCategorybanner',
            'Categorybanner\Form\AdminCategorybanner\CategorybannerForm' => 'Categorybanner\Form\AdminCategorybanner\CategorybannerForm',
            'Categorybanner\Form\AdminCategorybanner\CategorybannerFilter' => 'Categorybanner\Form\AdminCategorybanner\CategorybannerFilter',
            'Categorybanner\Service\Tab\AdminCategorybanner' => 'Categorybanner\Service\Tab\AdminCategorybanner',
            'Categorybanner\Service\Link\AdminCategorybanner' => 'Categorybanner\Service\Link\AdminCategorybanner',
            'Categorybanner\Service\NestedSorting\AdminCategorybanner' => 'Categorybanner\Service\NestedSorting\AdminCategorybanner',
            'Categorybanner\Service\Block\CategorybannerBlock' => 'Categorybanner\Service\Block\CategorybannerBlock',
            'Categorybanner\Service\Tab\AdminCategorybannerConfig' => 'Categorybanner\Service\Tab\AdminCategorybannerConfig',
        ),
        'factories' => array(
            'Categorybanner\Form\AdminCategorybanner\CategorybannerFormFactory' => 'Categorybanner\Form\AdminCategorybanner\CategorybannerFormFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Categorybanner\Controller\AdminCategorybanner' => 'Categorybanner\Controller\AdminCategorybannerController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Categorybanner' => __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'Categorybanner_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Categorybanner/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Categorybanner\Entity' => 'Categorybanner_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Categorybanner' => __DIR__ . '/../public'
            )
        )
    )
);
