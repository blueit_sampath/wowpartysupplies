<?php

namespace CardsavePayment\Form\AdminCardsavePaymentConfig;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CardsavePaymentConfigForm extends Form {

    public function init() {

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_TITLE',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Cardsave Payment Label'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_MERCHANTID',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Merchant ID'
            )
                ), array(
            'priority' => 990
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_PASSWORD',
            'attributes' => array(
                'type' => 'password'
            ),
            'options' => array(
                'label' => 'Password'
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_PAYMENTPROCESSORDOMAIN',
            'attributes' => array(
                'type' => 'text',
                'value' => "https://mms.cardsaveonlinepayments.com/Pages/PublicPages/PaymentForm.aspx"
            ),
            'options' => array(
                'label' => 'Payment Processor Domain'
            )
                ), array(
            'priority' => 970
        ));

        $select = new Select('PAYMENT_CARDSAVE_HASHMETHOD');
        $select->setValueOptions(array(
            'SHA1' => 'SHA1',
            'MD5' => 'MD5',
            'HMACMD5' => 'HMACMD5',
            'HMACSHA1' => 'HMACSHA1'
        ));
        $select->setLabel('Hash Method');
        $this->add($select, array(
            'priority' => 960
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_PRESHAREDKEY',
            'attributes' => array(
                'type' => 'text',
                'value' => ''
            ),
            'options' => array(
                'label' => 'Preshared Key'
            )
                ), array(
            'priority' => 950
        ));
        $select = new Select('PAYMENT_CARDSAVE_RESULTDELIVERYMETHOD');
        $select->setValueOptions(array(
            'POST' => 'POST',
            'SERVER' => 'SERVER',
            'SERVER_PULL' => 'SERVER_PULL'
        ));
        $select->setLabel('Result Delivery Method');
        $this->add($select, array(
            'priority' => 940
        ));

        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
