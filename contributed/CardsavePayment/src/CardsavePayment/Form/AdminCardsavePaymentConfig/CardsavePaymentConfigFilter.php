<?php

namespace CardsavePayment\Form\AdminCardsavePaymentConfig;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CardsavePaymentConfigFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_MERCHANTID',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_PASSWORD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
                , array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_PAYMENTPROCESSORDOMAIN',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_HASHMETHOD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_PRESHAREDKEY',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_CARDSAVE_RESULTDELIVERYMETHOD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));
    }

}
