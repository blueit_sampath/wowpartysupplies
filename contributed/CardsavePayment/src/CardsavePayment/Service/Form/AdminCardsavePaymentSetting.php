<?php
namespace CardsavePayment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminCardsavePaymentSetting extends AdminConfig
{

    protected $_columnKeys = array(
        'PAYMENT_CARDSAVE_TITLE',
        'PAYMENT_CARDSAVE_MERCHANTID',
         'PAYMENT_CARDSAVE_PASSWORD',
        'PAYMENT_CARDSAVE_PAYMENTPROCESSORDOMAIN',
        'PAYMENT_CARDSAVE_HASHMETHOD',
        'PAYMENT_CARDSAVE_PRESHAREDKEY',
        'PAYMENT_CARDSAVE_RESULTDELIVERYMETHOD'
    );

    public function getFormName()
    {
        return 'adminCardsavePaymentConfig';
    }

    public function getPriority()
    {
        return 1000;
    }
}
