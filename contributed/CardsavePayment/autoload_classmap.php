<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
  'CardsavePayment\Api\CardsavePaymentApi'                                      => __DIR__ . '/src/CardsavePayment/Api/CardsavePaymentApi.php',
  'CardsavePayment\Option\Cardsave'                                             => __DIR__ . '/src/CardsavePayment/Option/Cardsave.php',
  'CardsavePayment\Service\Payment\CardsavePaymentEvent'                        => __DIR__ . '/src/CardsavePayment/Service/Payment/CardsavePaymentEvent.php',
  'CardsavePayment\Service\Form\AdminCardsavePaymentSetting'                    => __DIR__ . '/src/CardsavePayment/Service/Form/AdminCardsavePaymentSetting.php',
  'CardsavePayment\Service\Tab\AdminPaymentSetting'                            => __DIR__ . '/src/CardsavePayment/Service/Tab/AdminPaymentSetting.php',
  'CardsavePayment\Controller\IndexController'                                 => __DIR__ . '/src/CardsavePayment/Controller/IndexController.php',
  'CardsavePayment\Controller\AdminCardsavePaymentController'                   => __DIR__ . '/src/CardsavePayment/Controller/AdminCardsavePaymentController.php',
  'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigFactory' => __DIR__ . '/src/CardsavePayment/Form/AdminCardsavePaymentConfig/CardsavePaymentConfigFactory.php',
  'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigForm'    => __DIR__ . '/src/CardsavePayment/Form/AdminCardsavePaymentConfig/CardsavePaymentConfigForm.php',
  'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigFilter'  => __DIR__ . '/src/CardsavePayment/Form/AdminCardsavePaymentConfig/CardsavePaymentConfigFilter.php',
  'CardsavePayment\Module'                                                     => __DIR__ . '/Module.php',
);
