<?php

namespace Contact\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminContact extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'email',
			'contactNumber' 
	);
	protected $_entity = '\Contact\Entity\Contact';
	protected $_entityName = 'contact';
	protected $_detailTemplateName = 'adminContactDetail'; // adminCategoryImage
	protected $_detailTemplate = 'common/admin-contact-detail';
	public function getEventName() {
		return 'adminContact';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'contact.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'contact.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'email' );
		$columnItem->setTitle ( 'Email' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'contact.email', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'contactNumber' );
		$columnItem->setTitle ( 'Contact Number' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'contact.contactNumber', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 960 );
		$columnItem->setFormat ( '{0:g}' );
		$columns->addColumn ( 'contact.createdDate', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
}
