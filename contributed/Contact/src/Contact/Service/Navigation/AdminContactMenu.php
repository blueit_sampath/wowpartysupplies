<?php 
namespace Contact\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminContactMenu extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'websiteMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Enquiry',
							'route' => 'admin-contact',
							'id' => 'contactMain',
							'iconClass' => 'glyphicon-retweet' 
					) 
			) );
			$page = $navigation->findOneBy ( 'id', 'contactMain' );
			$page->addPages ( array (
					array (
							'label' => 'Settings',
							'route' => 'admin-contact-config-general',
							'id' => 'admin-contact-config-general',
							'iconClass' => 'glyphicon-wrench',
					          'aClass' => 'zoombox'
					)
			) );
			
		
			$page->addPages ( array (
					array (
							'label' => 'Messages',
							'route' => 'admin-contact',
							'id' => 'admin-contact',
							'iconClass' => 'glyphicon-retweet',
							'aClass' => 'zoombox'
					)
			) );
		}
	}
}

