<?php
namespace Contact\Form\AdminContactConfigGeneral;

use Common\Form\Option\AbstractFormFactory;

class ConfigGeneralFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigGeneral\ConfigGeneralForm' );
		$form->setName ( 'adminContactConfigGeneral' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigGeneral\ConfigGeneralFilter' );
	}
}
