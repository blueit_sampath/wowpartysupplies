<?php
namespace Contact\Form\AdminContactConfigGeneral;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  ConfigGeneralFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
	    $this->add ( array (
	    		'name' => 'CONTACT_FROM_EMAIL',
	    		'required' => true,
	    		'filters' => array (
	    				array (
	    						'name' => 'StringTrim'
	    				)
	    		),
	    		'validators' => array (
	    				array (
	    						'name' => 'EmailAddress'
	    				)
	    		)
	    ) );
	    
	    $this->add ( array (
	    		'name' => 'CONTACT_FROM_NAME',
	    		'required' => true,
	    		'filters' => array (
	    				array (
	    						'name' => 'StringTrim'
	    				),
	    				array (
	    						'name' => 'StripTags'
	    				)
	    		),
	    		'validators' => array (
	    				array (
	    						'name' => 'StringLength',
	    						'options' => array (
	    								'min' => 1,
	    								'max' => 255
	    						)
	    				)
	    		)
	    ) );
	}
} 