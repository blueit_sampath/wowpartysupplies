<?php
namespace Contact\Form\AdminContactConfigUser;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class ConfigUserForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'CONTACT_USER_SUBJECT',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Subject' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'CONTACT_USER_BODYHTML',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full'
						 
				),
				'options' => array (
						'label' => 'HTML Message' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'CONTACT_USER_BODYPLAIN',
				'attributes' => array (
						'type' => 'textarea' 
				)
				,
				'options' => array (
						'label' => 'Plain Message' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}