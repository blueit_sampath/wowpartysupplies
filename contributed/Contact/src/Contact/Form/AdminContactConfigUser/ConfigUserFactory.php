<?php
namespace Contact\Form\AdminContactConfigUser;

use Common\Form\Option\AbstractFormFactory;

class ConfigUserFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigUser\ConfigUserForm' );
		$form->setName ( 'adminContactConfigUser' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Contact\Form\AdminContactConfigUser\ConfigUserFilter' );
	}
}
