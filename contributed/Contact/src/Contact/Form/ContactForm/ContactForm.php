<?php
namespace Contact\Form\ContactForm;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use User\Api\UserApi;
use Zend\Form\Element\Text;


class ContactForm extends Form 

{
	public function init() {
	    $this->add ( array (
	    		'name' => 'name',
	    		'attributes' => array (
	    				'type' => 'text'
	    		),
	    		'options' => array (
	    				'label' => 'Name'
	    		)
	    ), array(
            'priority' => 1000
        ));
	    if(Functions::hasModule('User')){
	    	if($user = UserApi::getLoggedInUser()){
	    		$this->get('name')->setValue($user->firstName.' '.$user->lastName);
	    		 
	    	}
	    }
        
        
        $email = new Text('email');
        $email->setLabel('E-Mail');
        if(Functions::hasModule('User')){
            if($user = UserApi::getLoggedInUser()){
                $email->setValue($user->email);
               
            }
        }
        
        $this->add($email, array (
	    		'priority' => 980
	    ));
        
        $this->add ( array (
        		'name' => 'contactNumber',
        		'attributes' => array (
        				'type' => 'text'
        		),
        		'options' => array (
        				'label' => 'Contact Number'
        		)
        ), array (
        		'priority' => 970
        ) );
        
        $this->add ( array (
        		'name' => 'message',
        		'attributes' => array (
        				'type' => 'textarea'
        		),
        		'options' => array (
        				'label' => 'Message'
        		)
        ), array (
        		'priority' => 100
        ) );
        
       // $this->addCaptcha();
       
        
        $this->add ( array (
        		'name' => 'btnsubmit',
        		'attributes' => array (
        				'type' => 'submit',
        				'value' => 'Submit'
        		)
        ), array (
        		'priority' => - 100
        ) );
		
		
	}
	
}