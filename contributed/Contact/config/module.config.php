<?php
return array(
    'router' => array(
        'routes' => array(
            'admin-contact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/contact',
                    'defaults' => array(
                        'controller' => 'Contact\Controller\AdminContact',
                        'action' => 'index'
                    )
                )
            ),
            'contact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact',
                    'defaults' => array(
                        'controller' => 'Contact\Controller\Contact',
                        'action' => 'contact'
                    )
                )
            ),
            'admin-contact-config-general' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/contact/config/general[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Contact\Controller\AdminContactConfigGeneral',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-contact-config-user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/contact/config/user[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Contact\Controller\AdminContactConfigUser',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-contact-config-admin' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/contact/config/admin[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Contact\Controller\AdminContactConfigAdmin',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Contact\Service\Grid\AdminContact' => 'Contact\Service\Grid\AdminContact',
        'Contact\Service\Navigation\AdminContactMenu' => 'Contact\Service\Navigation\AdminContactMenu',
        'Contact\Service\Form\AdminContactConfigGeneral' => 'Contact\Service\Form\AdminContactConfigGeneral',
        'Contact\Service\Form\AdminContactConfigUser' => 'Contact\Service\Form\AdminContactConfigUser',
        'Contact\Service\Form\AdminContactConfigAdmin' => 'Contact\Service\Form\AdminContactConfigAdmin',
        'Contact\Service\Tab\AdminContactConfig' => 'Contact\Service\Tab\AdminContactConfig',
        'Contact\Service\Form\ContactForm' => 'Contact\Service\Form\ContactForm',
        'Contact\Service\Form\ContactMail' => 'Contact\Service\Form\ContactMail',
        'Contact\Service\Navigation\ContactNavigation' => 'Contact\Service\Navigation\ContactNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Contact\Service\Grid\AdminContact' => 'Contact\Service\Grid\AdminContact',
            'Contact\Service\Navigation\AdminContactMenu' => 'Contact\Service\Navigation\AdminContactMenu',
            'Contact\Form\AdminContactConfigGeneral\ConfigGeneralForm' => 'Contact\Form\AdminContactConfigGeneral\ConfigGeneralForm',
            'Contact\Form\AdminContactConfigGeneral\ConfigGeneralFilter' => 'Contact\Form\AdminContactConfigGeneral\ConfigGeneralFilter',
            'Contact\Service\Form\AdminContactConfigGeneral' => 'Contact\Service\Form\AdminContactConfigGeneral',
            'Contact\Form\AdminContactConfigUser\ConfigUserForm' => 'Contact\Form\AdminContactConfigUser\ConfigUserForm',
            'Contact\Form\AdminContactConfigUser\ConfigUserFilter' => 'Contact\Form\AdminContactConfigUser\ConfigUserFilter',
            'Contact\Service\Form\AdminContactConfigUser' => 'Contact\Service\Form\AdminContactConfigUser',
            'Contact\Form\AdminContactConfigAdmin\ConfigAdminForm' => 'Contact\Form\AdminContactConfigAdmin\ConfigAdminForm',
            'Contact\Form\AdminContactConfigAdmin\ConfigAdminFilter' => 'Contact\Form\AdminContactConfigAdmin\ConfigAdminFilter',
            'Contact\Service\Form\AdminContactConfigAdmin' => 'Contact\Service\Form\AdminContactConfigAdmin',
            'Contact\Service\Tab\AdminContactConfig' => 'Contact\Service\Tab\AdminContactConfig',
            'Contact\Form\ContactForm\ContactForm' => 'Contact\Form\ContactForm\ContactForm',
            'Contact\Form\ContactForm\ContactFormFilter' => 'Contact\Form\ContactForm\ContactFormFilter',
            'Contact\Service\Form\ContactForm' => 'Contact\Service\Form\ContactForm',
            'Contact\Service\Form\ContactMail' => 'Contact\Service\Form\ContactMail',
            'Contact\Service\Navigation\ContactNavigation' => 'Contact\Service\Navigation\ContactNavigation'
        ),
        'factories' => array(
            'Contact\Form\AdminContactConfigGeneral\ConfigGeneralFactory' => 'Contact\Form\AdminContactConfigGeneral\ConfigGeneralFactory',
            'Contact\Form\AdminContactConfigUser\ConfigUserFactory' => 'Contact\Form\AdminContactConfigUser\ConfigUserFactory',
            'Contact\Form\AdminContactConfigAdmin\ConfigAdminFactory' => 'Contact\Form\AdminContactConfigAdmin\ConfigAdminFactory',
            'Contact\Form\ContactForm\ContactFormFactory' => 'Contact\Form\ContactForm\ContactFormFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Contact\Controller\AdminContact' => 'Contact\Controller\AdminContactController',
            'Contact\Controller\AdminContactConfigGeneral' => 'Contact\Controller\AdminContactConfigGeneralController',
            'Contact\Controller\AdminContactConfigUser' => 'Contact\Controller\AdminContactConfigUserController',
            'Contact\Controller\AdminContactConfigAdmin' => 'Contact\Controller\AdminContactConfigAdminController',
            'Contact\Controller\Contact' => 'Contact\Controller\ContactController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Contact' => __DIR__.'/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'Contact_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__.'/../src/Contact/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Contact\Entity' => 'Contact_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Contact' => __DIR__.'/../public'
            )
        )
    )
);
