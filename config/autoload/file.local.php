<?php
return array (
		
		'file_options' => array (
				'script_url' => '/fileupload',
				'upload_dir' => __DIR__ . '/../../public/files/',
				'upload_url' => '/files/',
				'user_dirs' => false,
				'mkdir_mode' => 0755,
				'param_name' => 'files',
				'delete_type' => 'DELETE',
				'access_control_allow_origin' => '*',
				'access_control_allow_credentials' => false,
				'access_control_allow_methods' => array (
						'OPTIONS',
						'HEAD',
						'GET',
						'POST',
						'PUT',
						'PATCH',
						'DELETE' 
				),
				'access_control_allow_headers' => array (
						'Content-Type',
						'Content-Range',
						'Content-Disposition' 
				),
				'download_via_php' => false,
				'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
				'accept_file_types' => '/.+$/i',
				'max_file_size' => null,
				'min_file_size' => 1,
				'max_number_of_files' => null,
				'max_width' => null,
				'max_height' => null,
				'min_width' => 1,
				'min_height' => 1,
				'discard_aborted_uploads' => true,
				'orient_image' => false,
				'image_versions' => array (
						'thumbnail' => array (
								'max_width' => 80,
								'max_height' => 80 
						) 
				),
				'include_js' => true,
				'include_css' => true
		) 
		
);