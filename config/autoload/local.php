<?php

/**

 * Local Configuration Override

 *

 * This configuration override file is for overriding environment-specific and

 * security-sensitive configuration information. Copy this file without the

 * .dist extension at the end and populate values as needed.

 *

 * @NOTE: This file is ignored from Git by default with the .gitignore included

 * in ZendSkeletonApplication. This is a good practice, as it prevents sensitive

 * credentials from accidentally being committed into version control.

 */

$dbParams = array(

    'database' => 'wowparty_spLys0241',

    'username' => 'root',

    'password' => '',

    'hostname' => 'localhost'

);



return array(

    'db' => array(

        'username' => $dbParams['username'],

        'password' => $dbParams['password']

    ),

    'doctrine' => array(

        

        'configuration' => array(

            'orm_default' => array(

                'metadata_cache' => 'array',

                'query_cache' => 'array',

                'result_cache' => 'array'

            )

        ),

        

        'connection' => array(

            'orm_default' => array(

                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',

                'params' => array(

                    'host' => $dbParams['hostname'],

                    'port' => '3306',

                    'user' => $dbParams['username'],

                    'password' => $dbParams['password'],

                    'dbname' => $dbParams['database']

                )

            )

        )

        ,

        'eventmanager' => array(

            'orm_default' => array(

                'subscribers' => array(

                    'Gedmo\Timestampable\TimestampableListener'

                )

            )

            

        ),

        'driver' => array (

						/*

						'translatable_metadata_driver' => array (

								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',

								'cache' => 'array',

								'paths' => array (

										__DIR__ . '/../../../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translatable/Entity' 

								) 

						),

						'logentry_metadata_driver' => array (

								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',

								'cache' => 'array',

								'paths' => array (

										__DIR__ . '/../../../vendor/gedmo/doctrine-extensions/lib/Gedmo/Loggable/Entity' 

								) 

						),

						'orm_default' => array (

								'drivers' => array (

										

										'Gedmo\Translatable\Entity' => 'translatable_metadata_driver',

										'Gedmo\Loggable\Entity\LogEntry' => 'logentry_metadata_driver' 

								) 

						) 

						*/

				)

    ),

    

    'service_manager' => array(

        'factories' => array(

            'Zend\Db\Adapter\Adapter' => function ($sm) use($dbParams)

            {

                return new Zend\Db\Adapter\Adapter(array(

                    'driver' => 'pdo',

                    'dsn' => 'mysql:dbname=' . $dbParams['database'] . ';host=' . $dbParams['hostname'],

                    'database' => $dbParams['database'],

                    'username' => $dbParams['username'],

                    'password' => $dbParams['password'],

                    'hostname' => $dbParams['hostname']

                ));

            }

        )

    )

);

