<?php

return array(
    'modules' => array(
        'AssetManager',
        'Application',
        // 'ZendDeveloperTools',
        'DoctrineModule',
        'DoctrineORMModule',
        'TwbBundle',
        'ZFTool',
        'Core',
        'Common',
        'SBForm',
        'Kendo',
        'BlucartGrid',
        'QueryBuilder',
        'Automate',
        'NestedSorting',
        'File',
        'Config',
        'Mail',
        'TagElement',
        'BlockManager',
        'FieldEdit',
        'Pdf',
        'LocaleElement',
        'MenuCategory',
        'MenuManager',
        'Dbtool',
        'Cron',
        // --------------
        'Checkout',
        'UserRole',
        'User',
        'Product',
        'Category',
        'ProductCategory',
        'OrderMain',
        'Payment',
        'Inventory',
        'Contact',
        'Attribute',
        'Addon',
        'ProductRecommended',
        'Seo',
        'SeoProduct',
        'SeoCategory',
        'SeoStaticpage',
        'Newsletter',
        'Coupon',
        'Shipping',
        'ShippingAmount',
        'ProductFeatured',
        'Brand',
        'BrandProduct',
        'Tag',
        'TagProduct',
        'Feature',
        'Vat',
        'Report',
        'Search',
        'Admin',
        'Banner',
        'Banner2',
        'StaticPage',
        'StaticBlock',
        'Testimonial',
        'TestPayment',
        'Import',
        'ImportProduct',
        'ImportAttribute',
        'ImportCategory',
        'CardsavePayment',
        'IridiumPayment',
        'PaymentsensePayment',
        'PaypalPayment',
	'PaypointPayment',
	'BarclaycardPayment',
        'Export',
        'ExportProduct',
        'ExportAttribute',
        'NochexPayment',
	'Pdfpremium',
	'Pdfhtml2pdf',
       // 'Cookiepolicy',
	'Blueit',
        'Wowpartysupplies',
        'WorldpayPayment',
    ),
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './module/project_theme',
            './module/project_specific',
            './module/blucart/theme',
            './module/blucart/contributed',
            './module/blucart/core',
            '/usr/local/lib/php/blucart/blucart_framework_v3_0/theme',
            '/usr/local/lib/php/blucart/blucart_framework_v3_0/contributed',
            '/usr/local/lib/php/blucart/blucart_framework_v3_0/core',
            '/usr/local/lib/blucart/blucart_framework_v3_0/theme',
            '/usr/local/lib/blucart/blucart_framework_v3_0/contributed',
            '/usr/local/lib/blucart/blucart_framework_v3_0/core',
            '/var/www/blucart_framework/theme',
            '/var/www/blucart_framework/contributed',
            '/var/www/blucart_framework/core',
            'c:/xampp/htdocs/blucart_framework/theme',
            'c:/xampp/htdocs/blucart_framework/contributed',
            'c:/xampp/htdocs/blucart_framework/core',
            'c:/wamp/www/blucart_framework/theme',
            'c:/wamp/www/blucart_framework/contributed',
            'c:/wamp/www/blucart_framework/core',
            './vendor'
        ),
        'config_glob_paths' => array(
            'config/autoload/{,*.}{global,local}.php'
        ),
        // Whether or not to enable a configuration cache.
        // If enabled, the merged configuration will be cached and used in
        // subsequent requests.
        //'config_cache_enabled' => true,

        // The key used to create the configuration cache file name.
        //'config_cache_key' => 'blucart_config_cache',

        // Whether or not to enable a module class map cache.
        // If enabled, creates a module class map cache which will be used
        // by in future requests, to reduce the autoloading process.
        //'module_map_cache_enabled' => true,

        // The key used to create the class map cache file name.
        //'module_map_cache_key' => 'blucart_module_cache',

        // The path in which to cache merged configuration.
        //'cache_dir' => './data/cache/modulecache',

        // Whether or not to enable modules dependency checking.
        // Enabled by default, prevents usage of modules that depend on other modules
        // that weren't loaded.
         //'check_dependencies' => true,
    )
);

