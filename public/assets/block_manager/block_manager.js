var BlockManager = Class.$extend({
			regionId : '',
			uri : '',
			blockSelector : '.block-container',
			blockItemSelector : 'div.block-item-wrapper',

			render : function() {
				var __class = this;

				__class.blockSelector = '#' + this.regionId + ' '
						+ this.blockSelector;

				this.init_sortable();
			

				$('[data-region="'+ __class.regionId + '"] a.save_block_container').on("click",
						function() {
							__class.saveBlock();
							return false;
						});

			$('[data-region="'+ __class.regionId + '"] a.remove_block_element').on('click', function() {
							
							__class.removeBlock(this);
							return false;
						});

			},
			init_sortable : function() {
				var __class = this;
				$(__class.blockSelector).sortable({
					'items' : __class.blockItemSelector,
					placeholder : "ui-state-highlight"
				});
			},
			
			loadBlock : function() {
				var __class = this;
				url = baseUrl + "block/load/region/" + this.regionId;

				$.ajax({
					type : "POST",
					url : url,
					dataType : 'json',
					data : {
						'uri' : __class.uri
					}

				}).done(
						function(results) {
							jQuery.each(results, function(key, value) {
								if ($(
										__class.blockSelector + ' '
												+ __class.blockItemSelector
												+ '[data-id="' + value.name
												+ '"]').size()) {

								} else {

									element = $(__class.blockSelector).append(
											value.block);

								}

							});

							$(__class.blockSelector + ' '+__class.blockItemSelector).each(
									function(key, value) {

										var name = $(this).attr('data-id');

										if (!results[name]) {
											$(this).remove();
										}

									});
							__class.init_zoombox();
							__class.init_toolbar();

						});

			},
			saveBlock : function() {

				var __class = this;
				var data = $(__class.blockSelector).sortable('serialize', {
					expression : /(.+)[-](.+)/
				});
				
				data += '&uri='+ __class.uri;
				
				url = baseUrl + "block/update/region/" + this.regionId;
				jQuery.ajax({
					type : "POST",
					url : url,
					dataType : 'json',
					data : data

				})

			},
			removeBlock : function(element) {
				var __class = this;
				var block = $(element).closest('.sample-empty-block-wrapper');
				blockId = $(block).attr('data-id');
				url = baseUrl + "block/remove/block/" + blockId + "/region/"
						+ this.regionId;
				jQuery.ajax({
					type : "GET",
					url : url,
					dataType : 'json',
				});
				$(block).remove();

			},
		});
