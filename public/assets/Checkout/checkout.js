$(function() {
	$('form#cartCheckout').on('change', 'input:radio,input:checkbox,select',
			function() {
				$('.block-cart-checkout').mask('Loading..');
				var form = $(this).closest('form');
				$.ajax({
					type : "POST",
					url : baseUrl + 'checkout/cart/preview',
					data : form.serialize(),
					success : function(data) {

						$('.block-cart-checkout').html(data);
						$('.block-cart-checkout').unmask();
					}
				});
			});

	$modal.on('hidden', function() {
		$.event.trigger({
			type : 'ajaxpageload',
			url : baseUrl + 'checkout'
		});
	});

});