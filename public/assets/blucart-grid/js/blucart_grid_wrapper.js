var BlucartGridWrapper = kendo.Class
        .extend({
            name: '',
            init: function(name) {
                this.name = name;
            },
            attachEvents: function() {

                this.bulkDelete();
                this.export();
                this.errorHandling();
                this.zoomboxReload();
                this.nestedSortingReload();
                this.gridReload();
            },
            export: function() {
                var __class = this;
                $('.' + __class.name + '   a.k-grid-export').click(function() {
                    var entityGrid = $("." + __class.name).data("kendoGrid");
                    var columns = entityGrid.columns;
                    tempColumns = {};
                    data = [];
                    
                    var columns = entityGrid.thead.find("th:visible:not(.k-hierarchy-cell)");
                   
                   $.each(columns, function(index,column){
                     
                        tempColumns[$(this).attr('data-field')] = $(this).text();
                   });
                   
                  
                    data.push(tempColumns);

                    var rows = entityGrid.select();
                    if (!rows || !rows.length) {
                        alert('Please select the records');
                        return;
                    }

                    rows.each(function(index, row) {
                        temp = {};
                        var selectedItem = entityGrid.dataItem(row);
                        $.each(tempColumns, function(in3, tempColumns) {
                            
                            temp[in3] = selectedItem[in3];
                            
                        });
                        data.push(temp);
                        // selectedItem has EntityVersionId and the rest of your model
                    });

                    var url = baseUrl + 'blucart-grid/export';
                  var form =   $('<form target="_blank" action="' + url + '" method="POST">' +
                            '<input type="hidden" name="data" value="">' +
                            '</form>') .appendTo($(document.body));
                    form.find('input[name="data"]').val(JSON.stringify(data));
                    
                    form.submit();
                    form.remove();
                    return false;
                });
            },
            bulkDelete: function() {
                var __class = this;
                $('.' + this.name + ' .k-grid-toolbar .k-grid-delete')
                        .click(
                                function() {
                                    var result = confirm('Are you sure want to delete the records ?');
                                    if (!result) {
                                        return false;
                                    }
                                    var grid = $('.' + __class.name).data(
                                            "kendoGrid");
                                    $.each(
                                            grid.tbody
                                            .find(".k-state-selected"),
                                            function(index, value) {
                                                grid.removeRow(value);
                                            });
                                    grid.dataSource.sync();
                                });
            },
            zoomboxReload: function() {
                var __class = this;
                $(document).on("zoomboxClose", function() {
                    var grid = $('.' + __class.name).data("kendoGrid");
                    grid.dataSource.read();
                });
            },
            nestedSortingReload: function() {
                var __class = this;
                $(document).on("completedNestedSorting", function() {
                    var grid = $('.' + __class.name).data("kendoGrid");
                    grid.dataSource.read();
                });
            },
            gridReload: function() {
                var __class = this;
                $(document).on("gridReload", function() {
                    var grid = $('.' + __class.name).data("kendoGrid");
                    grid.dataSource.read();
                });
            },
            errorHandling: function() {
                var __class = this;
                $('.' + this.name).data("kendoGrid").dataSource.bind("error",
                        function(e) {
                            if (e.errors) {

                                var message = '';
                                $.each(e.errors, function(key, value) {
                                        message += value + "\n";
                                   
                                });
                                var grid = $('.' + __class.name).data(
                                        "kendoGrid");
                               
                                grid.cancelChanges();
                                alert(message);
                               
                            }
                            return false;

                        });
            },
            attachSearchForm: function(formName) {
                var __class = this;
                $('form[name="' + formName + '"]').submit(function() {
                    return __class.filterForm(this);
                });
                $.expr[':'].hasClassStartingWith = function(el, i, selector) {
                    var re = new RegExp("\\b" + selector[3]);
                    return re.test(el.className);
                }
            },
            attachStatus: function(e) {

            },
            filterForm: function(form) {
                // iterate over all of the inputs for the form
                // element that was passed in
                var json = [];
                var __class = this;
                $(':input', form).each(
                        function() {
                            var type = this.type;
                            var tag = this.tagName.toLowerCase(); // normalize
                            // case
                            if (type != 'submit') {
                                name = this.name;
                                if ($(":input[name='" + name + "']:hasClassStartingWith('element-')").size()) {
                                    name = $(this).attr("class").split("element-")[1].split("-")
                                }


                                val = $(this).val();
                                if ($(this).hasClass('date')) {
                                    val = $.datepicker.parseDate("dd/mm/yy", val);
                                }

                                op = __class.getOperatorString($(this));
                                if (val) {
                                    json.push({
                                        field: name,
                                        operator: op,
                                        value: val
                                    });
                                }
                            }
                        });
                var grid = $('.' + __class.name).data("kendoGrid");
                dataSource = grid.dataSource;
                dataSource.filter(json);
                return false;
            },
            getOperatorString: function(element) {

                var cls = 'eq';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }

                cls = 'neq';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }
                cls = 'lt';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }

                cls = 'lte';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }

                cls = 'gt';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }

                cls = 'gte';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }

                cls = 'startswith';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }
                cls = 'endswith';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }

                cls = 'contains';
                if (element.hasClass('op-' + cls)) {

                    return cls;
                }
                cls = 'doesnotcontain';
                if (element.hasClass('op-' + cls)) {
                    return cls;
                }

                return 'eq';
            }

        });