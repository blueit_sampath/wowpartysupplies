<?php 
namespace Vat\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminVatConfigController extends AbstractAdminController {
	
	
    public function getFormFactory() {
    	return $this->getServiceLocator ()->get ( 'Vat\Form\AdminVatConfig\VatFactory' );
    }
    public function getSaveRedirector() {
    	$form = $this->getFormFactory ();
    	return $this->redirect ()->toRoute ( 'admin-vat-config' );
    }
	
}