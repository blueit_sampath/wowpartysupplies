<?php
namespace Vat\Service\Cart;

use Core\Functions;
use Checkout\Option\CartEvent;
use Product\Api\ProductApi;
use Attribute\Api\AttributeApi;
use Vat\Api\VatApi;

class CartVat extends CartEvent
{

    public function getPriority()
    {
        return 500;
    }

    public function prepareCart()
    {
        $cart = $this->getCart();
        $cart->removeOutItem('vat');
        $type = $cart->getCartType();
        $amount = null;
        $cartItems = $cart->getCartItems();
        if ($cartItems) {
            foreach ($cartItems as $cartItem) {
                $amount = $amount + $this->caluclatePrice($cartItem);
            }
        }
        if ($amount) {
            
            $vatType = VatApi::getVatType();
            $outItem = $cart->addQuickOutItem('vat', VatApi::getVatTitle(), $amount);
            $outItem->setIsCurrency(true);
            $outItem->setIncludePrice(true);
            if ($vatType == VatApi::INCLUSIVE) {
                
                $outItem->setIncludePrice(false);
            }
        }
        
        return true;
    }

    public function caluclatePrice($cartItem)
    {
        if (! VatApi::isVatEnabled()) {
            return;
        }
        
        $productId = $cartItem->getProductId();
        
        $status = 1;
        if ($productId) {
            $vatRecord = VatApi::getVatByProductId($productId);
            if ($vatRecord) {
                $status = $vatRecord->status;
            }
        }
        if (! $status) {
            return;
        }
        
        $amount = VatApi::caluclatePrice($cartItem->getSellPrice());
        $amount = $amount * $cartItem->getQuantity();
        
        return $amount;
    }
}

