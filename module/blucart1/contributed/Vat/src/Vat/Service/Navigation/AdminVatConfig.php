<?php
namespace Vat\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;

class AdminVatConfig extends AdminNavigationEvent
{

    public function common()
    {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'VAT',
                    'route' => 'admin-vat-config',
                    'id' => 'admin-vat-config',
                    'iconClass' => 'glyphicon glyphicon-wrench ',
                    'aClass' => 'zoombox'
                )
            ));
          
        }
        
    }
}

