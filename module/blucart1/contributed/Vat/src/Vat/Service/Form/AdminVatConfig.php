<?php 
namespace Vat\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminVatConfig extends AdminConfig {
	
    protected $_columnKeys = array (
    		'VAT_PERCENTAGE',
    		'VAT_TYPE',
    		'VAT_ENABLE',
            'VAT_TITLE'
        );
	public function getFormName() {
		return 'adminVatConfig';
	}
	public function getPriority() {
		return 1000;
	}
	
}
