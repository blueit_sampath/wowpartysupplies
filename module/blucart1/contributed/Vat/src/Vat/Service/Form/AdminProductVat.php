<?php

namespace Vat\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Vat\Api\VatApi;
use Common\Form\Option\AbstractFormEvent;

class AdminProductVat extends AbstractFormEvent {

    protected $_entityName = '\Vat\Entity\VatProduct';

    public function getFormName() {
        return 'adminProductAdd';
    }

    public function getPriority() {
        return 500;
    }

    public function postInitEvent() {
        $form = $this->getForm();

        $form->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'vatEnable',
            'options' => array(
                'label' => 'Enable Vat',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 900
        ));

        $inputFilter = $form->getInputFilter();
        $inputFilter->add(array(
            'name' => 'vatEnable',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));

        return $this;
    }

    public function save() {
        $resultContainer = $this->getFormResultContainer();
        $form = $this->getForm();
        $params = $form->getData();
        $em = $this->getEntityManager();

        $productItem = $resultContainer->get('product');

        if ($productItem) {
            $product = $productItem->getValue();

            $entity = VatApi::getVatByProductId($product->id);

            if (!$entity) {
                $entity = new $this->_entityName();
                $entity->product = $product;
            }
            if ($params['vatEnable']) {
                $entity->status = $params['vatEnable'];
            } else {
                $entity->status = 0;
            }

            $em->persist($entity);
            $em->flush();

            if ($entity) {
                $resultContainer->add('vat', $entity);
            }
        }

        return true;
    }

    public function getRecord() {

        $productId = Functions::fromRoute('productId');
        if (!$productId) {
            return;
        }

        $form = $this->getForm();
        $em = $this->getEntityManager();

        $entity = VatApi::getVatByProductId($productId);
        if (!$entity) {
            $form->get('vatEnable')->setValue(1);
            return;
        }

        $form->get('vatEnable')->setValue($entity->status);


        return true;
    }

}
