<?php 
namespace Vat\Api;
use Common\Api\Api;
use Core\Functions;
use Config\Api\ConfigApi;
class  VatApi extends Api{

    protected static $_entity = '\Vat\Entity\VatProduct';
    const INCLUSIVE = 1;
    const EXCLUSIVE = 0;
    
    public static function getVatById($id) {
    	$em = Functions::getEntityManager ();
    	return $em->find ( static::$_entity, $id );
    }
    
    public static function getVatByProductId($productId) {
    	$em = static::getEntityManager ();
    	return $em->getRepository ( static::$_entity )->findOneBy ( array (
    			'product' => $productId
    	) );
    }
    
    public static function getVatPercentage(){
        return ConfigApi::getConfigByKey('VAT_PERCENTAGE');
    }
    
    public static function getVatTitle(){
    	return ConfigApi::getConfigByKey('VAT_TITLE', 'VAT');
    }
    
    public static function getVatType(){
    	return ConfigApi::getConfigByKey('VAT_TYPE');
    }
    
    public static function isVatEnabled(){
    	return ConfigApi::getConfigByKey('VAT_ENABLE',0);
    }
    
    public static function caluclatePrice($sellPrice){
        $percentage = static::getVatPercentage();
        
        $vatType = static::getVatType();
        $amount = null;
        if($vatType == static::INCLUSIVE){
            $percentage = ($percentage/100) + 1;
            $excludingPrice = $sellPrice / $percentage;
            $amount = $sellPrice - $excludingPrice;
        } else {
            $amount = $sellPrice * $percentage / 100;
        }
        return $amount;
    }
}
