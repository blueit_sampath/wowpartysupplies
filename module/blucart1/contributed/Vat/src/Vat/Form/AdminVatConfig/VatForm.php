<?php
namespace Vat\Form\AdminVatConfig;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use Zend\Form\Element\Radio;
use Common\Api\Api;
use Vat\Api\VatApi;


class VatForm extends Form 

{
	public function init() {
		
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'VAT_ENABLE',
				'options' => array (
						'label' => 'Enable Vat',
						'use_hidden_element' => true
				)
		), array (
				'priority' => 1000
		) );
		
		$this->add ( array (
				'name' => 'VAT_TITLE',
				'attributes' => array (
						'type' => 'text',
				),
				'options' => array (
						'label' => 'VAT Title',
					
				)
		), array (
				'priority' => 990
		) );
		
		$radio = new Radio ( 'VAT_TYPE' );
		$radio->setLabel ( 'VAT Type' );
		$radio->setValueOptions ( array(
			VatApi::INCLUSIVE => 'Inclusive',
		    VatApi::EXCLUSIVE => 'Exclusive',
	    
		) );
		$this->add ( $radio, array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'VAT_PERCENTAGE',
				'attributes' => array (
						'type' => 'text',
				),
				'options' => array (
						'label' => 'VAT Percentage',
						'appendText'   => '%',
				)
		), array (
				'priority' => 980
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}