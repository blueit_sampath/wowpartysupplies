<?php
namespace Vat\Form\AdminVatConfig;

use Common\Form\Option\AbstractFormFactory;

class VatFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Vat\Form\AdminVatConfig\VatForm' );
		$form->setName ( 'adminVatConfig' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Vat\Form\AdminVatConfig\VatFilter' );
	}
}
