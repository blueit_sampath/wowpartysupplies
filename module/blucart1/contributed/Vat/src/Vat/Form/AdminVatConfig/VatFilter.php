<?php
namespace Vat\Form\AdminVatConfig;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  VatFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'VAT_ENABLE',
				'required' => false,
				
		) );
		$this->add ( array (
				'name' => 'VAT_TYPE',
				'required' => false,
		
		) );
		$this->add ( array (
				'name' => 'VAT_PERCENTAGE',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Float' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'VAT_TITLE',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						)
				) 
		) );
		
	}
} 