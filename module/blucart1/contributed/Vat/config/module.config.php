<?php
return array(
    'router' => array(
        'routes' => array(
            'admin-vat-config' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/vat/config',
                    'defaults' => array(
                        'controller' => 'Vat\Controller\AdminVatConfig',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Vat\Service\Form\AdminVatConfig' => 'Vat\Service\Form\AdminVatConfig',
        'Vat\Service\Navigation\AdminVatConfig' => 'Vat\Service\Navigation\AdminVatConfig',
        'Vat\Service\Form\AdminProductVat' => 'Vat\Service\Form\AdminProductVat',
        'Vat\Service\Cart\CartVat' => 'Vat\Service\Cart\CartVat'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Vat\Form\AdminVatConfig\VatForm' => 'Vat\Form\AdminVatConfig\VatForm',
            'Vat\Form\AdminVatConfig\VatFilter' => 'Vat\Form\AdminVatConfig\VatFilter',
            'Vat\Service\Form\AdminVatConfig' => 'Vat\Service\Form\AdminVatConfig',
            'Vat\Service\Navigation\AdminVatConfig' => 'Vat\Service\Navigation\AdminVatConfig',
            'Vat\Service\Form\AdminProductVat' => 'Vat\Service\Form\AdminProductVat',
            'Vat\Service\Cart\CartVat' => 'Vat\Service\Cart\CartVat'
        ),
        'factories' => array(
            'Vat\Form\AdminVatConfig\VatFactory' => 'Vat\Form\AdminVatConfig\VatFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Vat\Controller\AdminVatConfig' => 'Vat\Controller\AdminVatConfigController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Vat' => __DIR__.'/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'Vat_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__.'/../src/Vat/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Vat\Entity' => 'Vat_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Vat' => __DIR__.'/../public'
            )
        )
    )
);
