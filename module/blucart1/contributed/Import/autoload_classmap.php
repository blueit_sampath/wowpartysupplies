<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
  'ImportTest\SampleTest'                      => __DIR__ . '/tests/Import/SampleTest.php',
  'ImportTest\Framework\TestCase'              => __DIR__ . '/tests/Import/Framework/TestCase.php',
  'Import\Option\ColumnItem'                   => __DIR__ . '/src/Import/Option/ColumnItem.php',
  'Import\Option\ImportContainer'              => __DIR__ . '/src/Import/Option/ImportContainer.php',
  'Import\Option\AbstractImportEvent'          => __DIR__ . '/src/Import/Option/AbstractImportEvent.php',
  'Import\Option\ImportValidationItem'         => __DIR__ . '/src/Import/Option/ImportValidationItem.php',
  'Import\Option\ImportValidationContainer'    => __DIR__ . '/src/Import/Option/ImportValidationContainer.php',
  'Import\Service\Navigation\ImportNavigation' => __DIR__ . '/src/Import/Service/Navigation/ImportNavigation.php',
  'Import\Controller\AdminImportController'    => __DIR__ . '/src/Import/Controller/AdminImportController.php',
  'Import\Form\AdminImport\ImportForm'         => __DIR__ . '/src/Import/Form/AdminImport/ImportForm.php',
  'Import\Form\AdminImport\ImportFactory'      => __DIR__ . '/src/Import/Form/AdminImport/ImportFactory.php',
  'Import\Form\AdminImport\ImportFilter'       => __DIR__ . '/src/Import/Form/AdminImport/ImportFilter.php',
  'Import\Module'                              => __DIR__ . '/Module.php',
);
