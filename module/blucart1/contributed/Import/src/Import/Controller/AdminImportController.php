<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Import for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Import\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Core\Functions;
use Zend\View\Model\ViewModel;
use Common\MVC\Controller\AbstractAdminController;
use Import\Option\ImportContainer;
use Zend\Filter\Decompress;

class AdminImportController extends AbstractAdminController {

    protected $_importContainer = '';

    public function getFormFactory() {
        return $this->getServiceLocator()->get('Import\Form\AdminImport\ImportFactory');
    }

    public function importAction() {
        $importContainer = $this->getImportContainer();
        $form = $this->getFormFactory();

        $request = $this->getRequest();
        if ($this->getRequest()->isPost()) {

            $data = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            $form->setData($data);
            if ($form->isValid()) {
                $form->save();
                $seperator = $form->get('seperator')->getValue();
                $dataArray = $this->readFile($data['fileupload']['tmp_name'], $seperator);

                if (isset($data['imageFolder']) && isset($data['imageFolder']['tmp_name']) && $data['imageFolder']['tmp_name']) {

                    $zip = new \ZipArchive ();
                    if ($zip->open($data['imageFolder']['tmp_name']) === true) {

                        for ($i = 0; $i < $zip->numFiles; $i ++) {
                            $zip->extractTo('data/import', array(
                                $zip->getNameIndex($i)
                            ));
                        }
                        $zip->close();
                    }
                }


                $viewModel = new ViewModel(array(
                    'form' => $form,
                    'data' => $dataArray,
                    'importContainer' => $importContainer
                ));

                $viewModel->setTemplate('import/admin-import/show-rows');

                return $viewModel;
            } else {
                $this->notValid();
            }
        } else {
            $form->getRecord();
        }
        $viewModel = new ViewModel(array(
            'form' => $form,
            'importContainer' => $importContainer
        ));

        return $viewModel;
    }

    public function afterSave($name, $seperator = ',') {

        $data = $this->readFile($name, $seperator);

        Functions::addSuccessMessage('Imported Successfully');
        return $data;
    }

    public function readFile($fileName, $seperator = ',') {
        $dataArray = array();
        if (($handle = fopen($fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, $seperator)) !== FALSE) {

                $dataArray[] = $data;
            }
            fclose($handle);
        }
        return $dataArray;
    }

    public function saveAction() {
        $importContainer = $this->getImportContainer();
        $data = $this->getRequest()
                ->getPost()
                ->toArray();

        $importContainer->setData($data);
        $importContainer->prepare();

        try {
            if ($importContainer->isValid()) {
                $importContainer->save();
            }
        } catch (\Exception $e) {
            Functions::addErrorMessage($e->getMessage());
        }
        return array(
            'importContainer' => $importContainer
        );
    }

    /**
     *
     * @return ImportContainer
     */
    public function getImportContainer() {
        $eventName = $this->params()->fromRoute('eventName', '');

        if (!$this->_importContainer) {
            $this->_importContainer = $this->getServiceLocator()->get('ImportContainer');
            $this->_importContainer->setEventName($eventName);
            $this->_importContainer->loadSchema();
        }
        return $this->_importContainer;
    }

}
