<?php
namespace Import\Option;

use Core\Item\Container\ItemContainer;

class ImportValidationContainer extends ItemContainer
{

    const ERROR = 'error';

    const SUCCESS = 'success';

    const WARNING = 'warning';

    const INFO = 'info';

    protected $_importValid = false;

    /**
     *
     * @return importValid $_importValid
     */
    public function getImportValid()
    {
        return $this->_importValid;
    }

    /**
     *
     * @param boolean $_importValid            
     */
    public function setImportValid($_importValid)
    {
        $this->_importValid = $_importValid;
    }

    /**
     *
     * @param string $uniqueKey            
     * @param string $message            
     * @param string $errorType            
     * @param integer $weight            
     * @return \Import\Option\ImportValidationItem
     */
    public function add($uniqueKey, $message, $messageType = self::ERROR, $weight = null, $validState = false)
    {
        $item = new ImportValidationItem($uniqueKey);
        
        $item->setMessageType($messageType);
        $item->setMessage($message);
        $item->setWeight($weight);
        $item->setValidState($validState);
        $this->addItem($item);
        return $item;
    }

    /**
     *
     * @param string $id            
     * @return \Import\Option\ImportValidationItem
     */
    public function get($id)
    {
        return $this->getItem($id);
    }

    /**
     *
     * @return boolean
     */
    public function isValid()
    {
        $items = $this->getItems();
        $result = true;
        foreach ($items as $item) {
            $result = ($result && $item->getValidState());
        }
        return ($this->getImportValid() && $result);
    }
}
