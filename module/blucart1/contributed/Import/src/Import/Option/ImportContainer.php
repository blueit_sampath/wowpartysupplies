<?php

namespace Import\Option;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;
use Core\Functions;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Form\Element\Select;
use Core\Item\Container\ItemContainer;

class ImportContainer extends ItemContainer implements ServiceLocatorAwareInterface, EventManagerAwareInterface {

    protected $_title = '';
    protected $_data = array();
    protected $_entities = array();
    protected $_columns = array();
    protected $_isNewRecord = true;
    protected $_inputFilter = null;
    protected $_eventName = null;
    protected $_services;
    protected $_events;

    /**
     *
     * @return the $_title
     */
    public function getTitle() {
        return $this->_title;
    }

    /**
     *
     * @param string $_title            
     */
    public function setTitle($_title) {
        $this->_title = $_title;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->_services = $serviceLocator;
    }

    /**
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator() {
        return $this->_services;
    }

    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    /**
     *
     * @return EventManagerInterface
     */
    public function getEventManager() {
        if (null === $this->_events) {
            $this->setEventManager(new EventManager());
        }
        return $this->_events;
    }

    /**
     *
     * @return the $_eventName
     */
    public function getEventName() {
        return $this->_eventName;
    }

    /**
     *
     * @param field_type $_eventName            
     */
    public function setEventName($_eventName) {
        $this->_eventName = $_eventName;
    }

    /**
     *
     * @return the $_data
     */
    public function getRawData() {
        return $this->_data;
    }

    /**
     *
     * @return the $_data
     */
    public function getData() {
        $inputFilter = $this->getInputFilter();
        return $inputFilter->getValues();
    }

    /**
     *
     * @param multitype: $_data            
     */
    public function setData($_data) {
        $this->_data = $_data;

        return $this;
    }

    /**
     *
     * @return the $_entities
     */
    public function getEntities() {
        return $this->_entities;
    }

    /**
     *
     * @param multitype: $_entities            
     */
    public function setEntities($_entities) {
        $this->_entities = $_entities;
    }

    public function addEntity($key, $value) {
        $this->_entities[$key] = $value;
        return $this->_entities[$key];
    }

    public function getEntity($entity) {
        if (isset($this->_entities[$entity])) {
            return $this->_entities[$entity];
        }
        return false;
    }

    public function removeEntity($entity) {
        if (isset($this->_entities[$entity])) {
            unset($this->_entities[$entity]);
            return true;
        }
        return false;
    }

    public function clearEntities() {
        $this->_entities = array();
        return true;
    }

    /**
     *
     * @return multitype:ColumnItem
     */
    public function getColumns() {
        return $this->sortArray($this->_columns);
    }

    public function setColumns($_columns = array()) {
        $this->_columns = $_columns;
        return $this;
    }

    /**
     *
     * @param unknown $key            
     * @return multitype:ColumnItem boolean
     */
    public function getColumn($key) {
        if (isset($this->_columns[$key])) {
            return $this->_columns[$key];
        }
        return false;
    }

    public function removeColumn($key) {
        if (isset($this->_columns[$key])) {
            unset($this->_columns[$key]);
            return true;
        }
        return false;
    }

    public function clearColumns() {
        $this->_columns = array();
        return $this;
    }

    /**
     *
     * @param unknown $key            
     * @param unknown $title            
     * @param unknown $inputArray            
     * @param string $weight            
     * @return \Import\Option\ColumnItem
     */
    public function addColumn($key, $title, $inputArray = array(), $weight = null) {
        $item = new ColumnItem($key);
        $item->setTitle($title);
        $item->setInputArray($inputArray);
        $item->setWeight($weight);
        $this->_columns[$key] = $item;
        return $item;
    }

    public function prepare() {
        $columns = $this->getColumns();
        $inputFilter = $this->getInputFilter();
        foreach ($columns as $key => $column) {
            $array = $column->getInputArray();
            $array['name'] = $key;
            $input = $inputFilter->add($array);
        }
        $inputFilter->setData($this->getRawData());

        return $inputFilter;
    }

    /**
     *
     * @return the $_isNewRecord
     */
    public function getIsNewRecord() {
        return $this->_isNewRecord;
    }

    /**
     *
     * @param boolean $_isNewRecord            
     */
    public function setIsNewRecord($_isNewRecord) {
        $this->_isNewRecord = $_isNewRecord;
    }

    /**
     *
     * @return \Zend\InputFilter\InputFilter
     */
    public function getInputFilter() {
        if (!$this->_inputFilter) {
            $this->_inputFilter = new InputFilter();
        }

        return $this->_inputFilter;
    }

    /**
     *
     * @param field_type $_inputFilter            
     */
    public function setInputFilter($_inputFilter) {
        $this->_inputFilter = $_inputFilter;
        return $this;
    }

    public function isValid() {
        $this->prepare();

        $inputFilter = $this->getInputFilter();

        $importValidationContainer = new ImportValidationContainer();
        $params = array(
            'importValidationContainer' => $importValidationContainer
        );
        $this->getEventManager()->trigger($this->getEventName() . '-isValid-import.pre', $this, $params);
        $result = $inputFilter->isValid();
        $importValidationContainer->setImportValid($result);
        $this->getEventManager()->trigger($this->getEventName() . '-isValid-import.post', $this, $params);

        $validState = $importValidationContainer->isValid();
        if (!$validState) {
            $this->injectMessages($importValidationContainer);
        }
        return $validState;
    }

    protected function injectMessages(ImportValidationContainer $importValidationContainer) {
        $inputFilter = $this->getInputFilter();
        foreach ($inputFilter->getInvalidInput() as $key => $error) {
            $key;
            $a = array();
            foreach ($error->getMessages() as $v) {
                $a[] = $v;
            }

            $item = $this->getColumn($key);
            Functions::addErrorMessage($item->getTitle() . ': ' . implode(',', $a));
        }
        $items = $importValidationContainer->getItems();
        foreach ($items as $item) {
            if ($item->getMessage()) {
                Functions::addMessage($item->getMessage(), $item->getMessageType());
            }
        }
        return $this;
    }

    public function loadSchema() {
        $this->getEventManager()->trigger($this->getEventName() . '-loadschema.pre', $this);
    }

    public function save() {
        $this->getEventManager()->trigger($this->getEventName() . '-save.pre', $this);
    }

    public function getSelectObject($name, $header) {
        $options = array(
            '' => 'Select'
        );

        $columns = $this->getColumns();
        $key = '';
        foreach ($columns as $column) {
            if ($column->getTitle() == $header) {
                $key = $column->getId();
            }
            $options[$column->getId()] = $column->getTitle();
        }

        $select = new Select($name);
        $select->setValueOptions($options);
        if ($key) {
            $select->setValue($key);
        }
        return $select;
    }

}
