<?php

namespace Import\Option;

use Core\Functions;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractImportEvent extends AbstractEvent {

    protected $_importContainer = null;
    protected $_importValidationContainer = null;
    protected $_event = null;

    abstract public function getEventName();

    public function getPriority() {
        return null;
    }

    public function attach(EventManagerInterface $events) {
        $eventName = $this->getEventName();
        $priority = $this->getPriority();

        $sharedEventManager = $events->getSharedManager();
        $this->_listeners[] = $sharedEventManager->attach('*', $eventName . '-loadschema.pre', array(
            $this,
            'preLoadSchema'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $eventName . '-isValid-import.pre', array(
            $this,
            'preIsValid'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $eventName . '-isValid-import.post', array(
            $this,
            'postIsValid'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $eventName . '-save.pre', array(
            $this,
            'preSave'
                ), $priority);

        return $this;
    }

    public function preLoadSchema($e) {
        $importContainer = $e->getTarget();
        $this->setImportContainer($importContainer);
        $this->setEvent($e);
        return $this->loadSchema();
    }

    public function loadSchema() {
        
    }

    public function preIsValid($e) {
        $importContainer = $e->getTarget();

        $this->setImportContainer($importContainer);
        $importValidationContainer = $e->getParam('importValidationContainer');
        $this->setImportValidationContainer($importValidationContainer);
        $this->setEvent($e);
        return $this->isValid();
    }

    public function isValid() {
        
    }

    public function postIsValid($e) {
        $importContainer = $e->getTarget();
        $this->setImportContainer($importContainer);
        $importValidationContainer = $e->getParam('importValidationContainer');
        $this->setImportValidationContainer($importValidationContainer);
        $this->setEvent($e);
        return $this->isValid();
    }

    public function isValidPost() {
        
    }

    public function preSave($e) {
        $importContainer = $e->getTarget();
        $this->setImportContainer($importContainer);
        $this->setEvent($e);
        return $this->save();
    }

    public function save() {
        
    }

    /**
     *
     * @return the $_event
     */
    public function getEvent() {
        return $this->_event;
    }

    /**
     *
     * @param field_type $_event            
     */
    public function setEvent($_event) {
        $this->_event = $_event;
    }

    /**
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager() {
        return Functions::getEntityManager();
    }

    /**
     *
     * @return ImportContainer
     */
    public function getImportContainer() {
        return $this->_importContainer;
    }

    /**
     *
     * @param ImportContainer $_importContainer            
     */
    public function setImportContainer($_importContainer) {
        $this->_importContainer = $_importContainer;
    }

    /**
     *
     * @return ImportValidationContainer
     */
    public function getImportValidationContainer() {
        return $this->_importValidationContainer;
    }

    /**
     *
     * @param ImportValidationContainer $_importValidationContainer            
     */
    public function setImportValidationContainer($_importValidationContainer) {
        $this->_importValidationContainer = $_importValidationContainer;
    }

    public function findImage($imageName) {

        
        $it = new \RecursiveDirectoryIterator("data/import");
        foreach (new \RecursiveIteratorIterator($it) as $file) {
            if (fnmatch("*/".$imageName, $file)) {
                return $file->getRealPath();
                
            }
        }
        
        return false;
    }
    
     public function saveImage($image, $imageName = '',$title = '',$alt = '') {
        if ($image) {
            if ((strpos($image, "http://") !== false) || (strpos($image, "https://") !== false)) {
                return \File\Api\FileApi::saveImageOnDisk($image,$imageName, $title, $alt);
            }
            $img = $this->findImage($image);
            if ($img) {
                return \File\Api\FileApi::saveImageOnDisk($img, $imageName,$title, $alt);
            }
             Functions::addWarningMessage($image . ': invalid image');
        }
       
        return false;
    }
      public function createSlug($string) {
        $slug = preg_replace('/[^A-Za-z0-9_]+/', '_', $string);
        return $slug;
    }

}
