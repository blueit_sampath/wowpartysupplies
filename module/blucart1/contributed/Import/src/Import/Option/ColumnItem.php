<?php
namespace Import\Option;

use Core\Item\Item;

class ColumnItem extends Item
{

    protected $_title;

    protected $_inputArray = array();

    /**
     *
     * @return the $_title
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     *
     * @return the $_inputArray
     */
    public function getInputArray()
    {
        return $this->_inputArray;
    }

    /**
     *
     * @param field_type $_title            
     */
    public function setTitle($_title)
    {
        $this->_title = $_title;
    }

    /**
     *
     * @param multitype: $_inputArray            
     */
    public function setInputArray($_inputArray)
    {
        $this->_inputArray = $_inputArray;
    }
}
