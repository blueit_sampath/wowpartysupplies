<?php

namespace Import\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ImportNavigation extends \Admin\Navigation\Event\AdminNavigationEvent {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMain');
        
            $page->addPages(array(
                array(
                    'label' => 'Import',
                    'uri' => '#',
                    'id' => 'productMainImportMain',
                    'iconClass' => 'glyphicon-wrench',
                    'priority' => 10000
                )
            ));
        
    }

}
