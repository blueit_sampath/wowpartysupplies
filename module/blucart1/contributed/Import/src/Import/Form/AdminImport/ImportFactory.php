<?php
namespace Import\Form\AdminImport;

use Common\Form\Option\AbstractFormFactory;

class ImportFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Import\Form\AdminImport\ImportForm' );
		$form->setName ( 'adminImport' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Import\Form\AdminImport\ImportFilter' );
	}
}
