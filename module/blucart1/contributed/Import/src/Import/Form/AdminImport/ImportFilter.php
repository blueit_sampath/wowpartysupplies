<?php
namespace Import\Form\AdminImport;

use Zend\InputFilter\InputFilter;

class ImportFilter extends InputFilter

{

    protected $inputFilter;

    public function __construct()
    {
        $this->add(array(
            'name' => 'fileupload',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'fileextension',
                    'options' => array(
                        'extension' => array(
                            'csv'
                        ),
                        'case' => true
                    )
                )
            )
        ));
        
        $this->add(array(
            'name' => 'imageFolder',
            'required' => false,
            
            'validators' => array(
                array(
                    'name' => 'fileextension',
                    'options' => array(
                        'extension' => array(
                            'zip'
                        ),
                        'case' => true
                    )
                )
                
            )
        ));

        
        
        $this->add(array(
            'name' => 'seperator',
            'required' => true
        ));
    }
} 