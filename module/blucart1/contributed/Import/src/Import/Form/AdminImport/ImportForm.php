<?php
namespace Import\Form\AdminImport;
use Common\Form\Form;
use Zend\Form\Element\File;

class ImportForm extends Form

{

    public function init()
    {
        $myFile = new File('fileupload');
        $myFile->setLabel('Upload CSV File');
        $this->add($myFile, array(
            'priority' => 1000
        ));
        
        

        $file = new File('imageFolder');
        $file->setLabel('Images Folder');
        $this->add($file, array(
        		'priority' => 990
        ));
        
        $this->add(array(
            'name' => 'seperator',
            'attributes' => array(
                'type' => 'text',
                'value' => ','
            ),
            'options' => array(
                'label' => 'Seperator'
            )
        ), array(
            'priority' => 980
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Next >'
            )
        ), array(
            'priority' => - 100
        ));
    }
}