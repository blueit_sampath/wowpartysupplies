<?php

return array(
    'controllers' => array('invokables' => array('Import\Controller\AdminImport' => 'Import\Controller\AdminImportController')),
    'service_manager' => array(
        'invokables' => array(
            'ImportContainer' => 'Import\Option\ImportContainer',
            'Import\Form\AdminImport\ImportForm' => 'Import\Form\AdminImport\ImportForm',
            'Import\Form\AdminImport\ImportFilter' => 'Import\Form\AdminImport\ImportFilter',
            'Import\Service\Navigation\ImportNavigation' => 'Import\Service\Navigation\ImportNavigation'
        ),
        'shared' => array('ImportContainer' => false),
        'factories' => array('Import\Form\AdminImport\ImportFactory' => 'Import\Form\AdminImport\ImportFactory')
    ),
    'router' => array('routes' => array(
            'admin-import' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/import[/eventName/:eventName]',
                    'defaults' => array(
                        'controller' => 'Import\Controller\AdminImport',
                        'action' => 'import'
                    ),
                    'constraints' => array('ajax' => 'true|false')
                )
            ),
            'admin-import-save' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/import/save[/eventName/:eventName]',
                    'defaults' => array(
                        'controller' => 'Import\Controller\AdminImport',
                        'action' => 'save',
                        'ajax' => 'true'
                    ),
                    'constraints' => array('ajax' => 'true|false')
                )
            )
        )),
    'view_manager' => array('template_path_stack' => array('Import' => __DIR__ . '/../view')),
    'events' => array('Import\Service\Navigation\ImportNavigation' => 'Import\Service\Navigation\ImportNavigation')
);
