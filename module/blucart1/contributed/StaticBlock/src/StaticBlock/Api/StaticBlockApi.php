<?php

namespace StaticBlock\Api;

use Core\Functions;
use Common\Api\Api;

class StaticBlockApi extends Api {
	protected static $_entity = '\StaticBlock\Entity\StaticBlock';
	public static function getStaticBlockById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getStaticBlocks($status = true) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'staticBlock.id', 'staticBlockId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'staticBlock.status', 'staticBlockStatus' );
			$queryBuilder->addParameter ( 'staticBlockStatus', $status );
		}
		$item = $queryBuilder->addFrom ( static::$_entity, 'staticBlock' );
		return $queryBuilder->executeQuery ();
	}
	public static function getStaticBlockByName($name, $status = true) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findOneBy ( array (
				'name' => $name,
				'status' => $status 
		) );
	}
}
