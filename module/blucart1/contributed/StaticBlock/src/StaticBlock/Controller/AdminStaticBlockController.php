<?php 
namespace StaticBlock\Controller;
use Common\MVC\Controller\AbstractAdminController;


class AdminStaticBlockController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'StaticBlock\Form\AdminStaticBlock\StaticBlockFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-static-block-add', array (
						'staticBlockId' => $form->get ( 'id' )->getValue ()
				) );
	}
	
	
}