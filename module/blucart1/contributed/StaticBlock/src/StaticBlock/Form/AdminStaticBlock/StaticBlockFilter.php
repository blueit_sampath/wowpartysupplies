<?php

namespace StaticBlock\Form\AdminStaticBlock;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class StaticBlockFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 255
                    )
                )
            )
        ));
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'regex',
                    'options' => array(
                        'pattern' => "/^[a-zA-Z0-9_]+$/",
                        'messages' => array(
                            \Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers or _'
                        )
                    )
                ),
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => array(
                            $this,
                            'checkStaticBlock'
                        ),
                        'messages' => array(
                            \Zend\Validator\Callback::INVALID_VALUE => "Static Block Name already exists",
                        )
                    )
                )
            )
        ));
        $this->add(array(
            'name' => 'status',
            'required' => true
                )
        );
        $this->add(array(
            'name' => 'description',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));
    }

    public function checkStaticBlock($value, $data = array()) {
        $entity = \StaticBlock\Api\StaticBlockApi::getStaticBlockByName($value, null);
        if (!$entity) {
            return true;
        }
        if ($entity->id == $data['id']) {
            return true;
        }
        return false;
    }

}
