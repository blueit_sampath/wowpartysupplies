<?php
namespace StaticBlock\Form\AdminStaticBlock;

use Common\Form\Option\AbstractFormFactory;

class StaticBlockFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'StaticBlock\Form\AdminStaticBlock\StaticBlockForm' );
		$form->setName ( 'adminStaticBlockAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'StaticBlock\Form\AdminStaticBlock\StaticBlockFilter' );
	}
}
