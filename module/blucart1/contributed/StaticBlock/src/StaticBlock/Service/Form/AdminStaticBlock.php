<?php

namespace StaticBlock\Service\Form;

use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminStaticBlock extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'description',
			'name',
			'status' 
	);
	protected $_entity = '\StaticBlock\Entity\StaticBlock';
	protected $_entityName = 'staticBlock';
	public function getFormName() {
		return 'adminStaticBlockAdd';
	}
	public function getPriority() {
		return 1000;
	}
        
        public function afterGetRecord($entity) {
            parent::afterGetRecord($entity);
            $form = $this->getForm();
            $form->get('name')->setAttribute('readonly', true);
        }
	
}
