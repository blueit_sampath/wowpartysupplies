<?php

namespace StaticBlock\Service\Grid;


use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;

use Kendo\Lib\Grid\Option\Toolbar\Toolbar;

use BlucartGrid\Option\ColumnItem;

use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminStaticBlock extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'name',
			'status' 
	);
	protected $_entity = '\StaticBlock\Entity\StaticBlock';
	protected $_entityName = 'staticBlock';
	public function getEventName() {
		return 'adminStaticBlock';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setIsPrimary(true);
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'staticBlock.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'staticBlock.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'staticBlock.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Staus' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'staticBlock.status', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	
}
