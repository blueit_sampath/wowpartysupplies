<?php 
namespace StaticBlock\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminStaticBlockNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$block = $navigation->findOneBy ( 'id', 'websiteMain' );
		if($block){
			$block->addPages ( array (
					array (
							'label' => 'Content Blocks',
							'route' => 'admin-static-block',
							'id' => 'admin-static-block',
							'iconClass' => 'glyphicon-bullhorn' 
					) 
			) );
		}
	}
}

