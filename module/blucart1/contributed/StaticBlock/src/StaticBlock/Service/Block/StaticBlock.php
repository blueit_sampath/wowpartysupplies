<?php

namespace StaticBlock\Service\Block;

use BlockManager\Api\BlockApi;
use StaticBlock\Api\StaticBlockApi;
use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventCollection;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventInterface;

class StaticBlock extends AbstractBlockEvent {
	protected $_blockTemplate = 'static-block';
	public function getBlockName() {
		return 'staticBlock';
	}
	public function getTitle() {
		return 'Static Block';
	}
	public function getPriority() {
		return 1000;
	}
	public function attach(EventManagerInterface $events) {
		$priority = $this->getPriority ();
		$sharedEventManager = $events->getSharedManager ();
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'block-render-name', array (
				$this,
				'preRenderName' 
		), $priority );
		$array = array ();
		$results = StaticBlockApi::getStaticBlocks ();
		foreach ( $results as $result ) {
			$blockId = $result ['staticBlockId'];
			$staticBlock = StaticBlockApi::getStaticBlockById ( $blockId );
			$array [] = $staticBlock->name. '_' . $this->getBlockName () . '-block-render';
		}
		$this->_listeners [] = $sharedEventManager->attach ( '*', $array, array (
				$this,
				'preRender' 
		), $priority );
		
		return $this;
	}
	public function renderName($e) {
		$blockContainer = $this->getBlockContainer ();
		$results = StaticBlockApi::getStaticBlocks ();
		foreach ( $results as $result ) {
			$blockId = $result ['staticBlockId'];
			$staticBlock = StaticBlockApi::getStaticBlockById ( $blockId );
			$blockContainer->add ( $staticBlock->name . '_' . $this->getBlockName (), $staticBlock->title );
		}
		return true;
	}
	public function render($e) {
		$eventName = $e->getName ();
		$blockName = preg_match ( "/(.*?)_" . $this->getBlockName () . "-block-render$/", $eventName, $match ) ? $match [1] : "";
		if (! $blockName) {
			return;
		}
		
		$entity = StaticBlockApi::getStaticBlockByName ( $blockName );
		if (! $entity) {
			return;
		}
		
		if ($this->_blockTemplate) {
			$blockContainer = $this->getBlockContainer ();
			
			$renderer = Functions::getServiceLocator ()->get ( 'viewrenderer' );
			$item = $blockContainer->add ( $entity->name. '_' . $this->getBlockName () , $entity->title );
			$renderer->render ( $this->getBlockTemplate (), array (
					'item' => $item,
					'blockContainer' => $blockContainer,
					'entity' => $entity 
			) );
		}
	}
}

