<?php 
namespace StaticBlock\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminStaticBlock extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminStaticBlock';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-static-block-add');
		$item = $linkContainer->add ( 'admin-static-block-add', 'Add Block', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

