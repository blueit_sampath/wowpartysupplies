<?php

namespace StaticBlock\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminStaticBlock extends AbstractTabEvent {
	public function getEventName() {
		return 'adminStaticBlockAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$staticBlockId = Functions::fromRoute ( 'staticBlockId' );
		if(!$staticBlockId){
			return;
		}
		$u = $url ( 'admin-static-block-add', array (
				'staticBlockId' => $staticBlockId 
		) );
		$tabContainer->add ( 'admin-static-block-add', 'General Information', $u, 1000 );
		
		return $this;
	}
}

