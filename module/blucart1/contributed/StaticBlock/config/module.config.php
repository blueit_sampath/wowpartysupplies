<?php

return array (
		'router' => array (
				'routes' => array (
						'admin-static-block' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/static/block',
										'defaults' => array (
												'controller' => 'StaticBlock\Controller\AdminStaticBlock',
												'action' => 'index' 
										) 
								) 
						),
						'admin-static-block-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/static/block/add[/ajax/:ajax][/:staticBlockId]',
										'defaults' => array (
												'controller' => 'StaticBlock\Controller\AdminStaticBlock',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'StaticBlock\Service\Grid\AdminStaticBlock' => 'StaticBlock\Service\Grid\AdminStaticBlock',
				'StaticBlock\Service\Link\AdminStaticBlock' => 'StaticBlock\Service\Link\AdminStaticBlock',
				'StaticBlock\Service\Tab\AdminStaticBlock' => 'StaticBlock\Service\Tab\AdminStaticBlock',
				'StaticBlock\Service\Form\AdminStaticBlock' => 'StaticBlock\Service\Form\AdminStaticBlock',
				'StaticBlock\Service\Navigation\AdminStaticBlockNavigation' => 'StaticBlock\Service\Navigation\AdminStaticBlockNavigation',
				'StaticBlock\Service\Block\StaticBlock' => 'StaticBlock\Service\Block\StaticBlock' 
		),
		'service_manager' => array (
				'invokables' => array (
						'StaticBlock\Service\Grid\AdminStaticBlock' => 'StaticBlock\Service\Grid\AdminStaticBlock',
						'StaticBlock\Service\Link\AdminStaticBlock' => 'StaticBlock\Service\Link\AdminStaticBlock',
						'StaticBlock\Service\Tab\AdminStaticBlock' => 'StaticBlock\Service\Tab\AdminStaticBlock',
						'StaticBlock\Form\AdminStaticBlock\StaticBlockForm' => 'StaticBlock\Form\AdminStaticBlock\StaticBlockForm',
						'StaticBlock\Form\AdminStaticBlock\StaticBlockFilter' => 'StaticBlock\Form\AdminStaticBlock\StaticBlockFilter',
						'StaticBlock\Service\Form\AdminStaticBlock' => 'StaticBlock\Service\Form\AdminStaticBlock',
						'StaticBlock\Service\Navigation\AdminStaticBlockNavigation' => 'StaticBlock\Service\Navigation\AdminStaticBlockNavigation',
						'StaticBlock\Service\Block\StaticBlock' => 'StaticBlock\Service\Block\StaticBlock' 
				),
				'factories' => array (
						'StaticBlock\Form\AdminStaticBlock\StaticBlockFactory' => 'StaticBlock\Form\AdminStaticBlock\StaticBlockFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'StaticBlock\Controller\AdminStaticBlock' => 'StaticBlock\Controller\AdminStaticBlockController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'StaticBlock' => __DIR__ . '/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						'StaticBlock_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__ . '/../src/StaticBlock/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										'StaticBlock\Entity' => 'StaticBlock_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'StaticBlock' => __DIR__ . '/../public' 
						) 
				) 
		) 
);
