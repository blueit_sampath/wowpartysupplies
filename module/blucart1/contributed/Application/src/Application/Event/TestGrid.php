<?php

namespace Application\Event;

use Zend\View\Model\ViewModel;
use Kendo\Lib\Grid\Option\Column\Command\CommandItem;
use QueryBuilder\Option\QueryBuilder;
use BlucartGrid\Option\BlucartGridResult;
use BlucartGrid\Option\BlucartGrid;
use BlucartGrid\Event\AbstractBlucartGridEvent;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;
use Kendo\Lib\Datasource\Transport\Transports;
use Kendo\Lib\Datasource\Transport\TransportItem;
use BlucartGrid\Option\ColumnItem;
use Kendo\Lib\Grid\Option\Column\Columns;
use Kendo\Lib\Core\JavascriptFunction;
use Kendo\Lib\Option\Aggregate\AggregateItem;
use Kendo\Lib\Option\Aggregate\Aggregates;
use Kendo\Lib\Datasource\Datasource;

class TestGrid extends AbstractBlucartGridEvent {
	public function getEventName() {
		return 'test';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 100 );
		$columns->addColumn ( 'user.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'firstName' );
		$columnItem->setTitle ( 'FirstName' );
		$columnItem->setFooterTemplate ( "Total Count: #=count#" );
		$columnItem->setWeight ( 90 );
		$columns->addColumn ( 'user.firstName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'lastName' );
		$columnItem->setTitle ( 'LastName' );
		$columnItem->setWeight ( 80 );
		$columns->addColumn ( 'user.lastName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'dob' );
		$columnItem->setTitle ( 'Date of birth' );
		$columnItem->setType ( 'date' );
		
		$columnItem->setFormat ( '{0:dd/MM/yyyy}' );
		$columnItem->setFilterable ( array (
				'ui' => "datetimepicker" 
		) );
		
		$columnItem->setWeight ( 70 );
		
		$columns->addColumn ( 'user.dob', $columnItem );
		
		return $columns;
	}
	public function postSchema($e) {
		parent::postSchema ( $e );
		$grid = $this->getGrid ();
		$aggregate = $grid->getDataSource ()->getAggregate ();
		if (! $aggregate) {
			$aggregate = new Aggregates ();
			$grid->getDataSource ()->setAggregate ( $aggregate );
		}
		$aggregateItem = new AggregateItem ();
		$aggregateItem->setField ( 'firstName' );
		$aggregateItem->setAggregate ( 'count' );
		$aggregate->addAggregate ( 'firstName_count', $aggregateItem );
		
		$aggregateItem = new AggregateItem ();
		$aggregateItem->setField ( 'id' );
		$aggregateItem->setAggregate ( 'sum' );
		$aggregate->addAggregate ( 'id_sum', $aggregateItem );
		
		$aggregateItem = new AggregateItem ();
		$aggregateItem->setField ( 'id' );
		$aggregateItem->setAggregate ( 'count' );
		$aggregate->addAggregate ( 'id_count', $aggregateItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$queryBuilder->addFrom ( 'Application\Entity\User', 'user' );
		return true;
	}
	public function postUpdate($e) {
		parent::postUpdate ( $e );
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		$params = $resultObject->getResults ();
		
		if ($params && isset ( $params ['id'] )) {
			$em = $this->getEntityManager ();
			$user = $em->find ( 'Application\Entity\User', $params ['id'] );
			$user->firstName = $params ['firstName'];
			$user->lastName = $params ['lastName'];
			$dob = new \DateTime ();
			$user->dob = $dob->setTimestamp ( strtotime ( $params ['dob'] ) );
			$resultContainer->add ( 'user', $user );
			$em->flush ();
		}
		return true;
	}
	public function preDestroy($e) {
		parent::postDestroy ( $e );
		$resultObject = $this->getGrid ()->getResultObject ();
		$validateContainer = $this->getGrid ()->getValidateContainer ();
		$params = $resultObject->getResults ();
		if ($params ['id'] == 5) {
			$validateContainer->add ( 'user', 'record cannot be deleted', 'error', false );
		}
		return false;
	}
	public function postDestroy($e) {
		parent::postDestroy ( $e );
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		$params = $resultObject->getResults ();
		
		if ($params && isset ( $params ['id'] )) {
			
			$em = $this->getEntityManager ();
			$user = $em->find ( 'Application\Entity\User', $params ['id'] );
			
			if ($user) {
				
				$resultContainer->add ( 'user', $user );
				$em->remove ( $user );
			}
			$em->flush ();
		}
		return true;
	}
	public function details($e) {
		parent::details ( $e );
		$detailContainer = $this->getGrid ()->getDetailContainer ();
		
		$renderer = $this->getServiceLocator ()->get ( 'viewrenderer' );
		$model = new ViewModel ( array () );
		$model->setTemplate ( 'test/user-detail', array (
				'grid' => $this->getGrid () 
		) );
		$html = $renderer->render ( $model );
		
		$item = $detailContainer->add ( 'user', 'Contact Information', $html, 1000 );
	}
	public function postCreate($e) {
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		$params = $resultObject->getResults ();
		
		if ($params && isset ( $params ['id'] )) {
			$em = $this->getEntityManager ();
			$user = new \Application\Entity\User ();
			$user->firstName = $params ['firstName'];
			$user->lastName = $params ['lastName'];
			$dob = new \DateTime ();
			$user->dob = $dob->setTimestamp ( strtotime ( $params ['dob'] ) );
			$em->persist ( $user );
			$em->flush ();
			$params ['id'] = $user->id;
			$resultObject->setResults ( $params );
			$resultContainer->add ( 'user', $user );
		}
		return true;
	}
}
