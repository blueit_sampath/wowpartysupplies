<?php

namespace Application\Event;

use Core\Form\Option\AbstractFormEvent;

use Zend\EventManager\SharedEventManager;

use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

class TestEvent extends AbstractFormEvent {
	public function getFormName(){
		return 'test';
	}
	
	
	public function postIsValidEvent(){
		$formResultContainer = $this->getFormResultContainer();
		$formResultContainer->setFormValid(false);
		
		return $this;
	}
}
