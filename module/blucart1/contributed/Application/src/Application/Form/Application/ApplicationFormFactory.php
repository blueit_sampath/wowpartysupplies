<?php

namespace Application\Form\Application;

use Common\Form\Option\AbstractFormFactory;

class ApplicationFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Application\Form\Application\ApplicationForm' );
		
		$form->setLimit ( $this->getServiceLocator ()->get ( 'Application' )->getMvcEvent ()->getRouteMatch ()->getParam ( 'limit', 10 ) );
		$form->setName ( 'test' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Application\Form\Application\ApplicationFilter' );
	}
}
