<?php

namespace Application\Form\Application;

use Zend\Form\Fieldset;

use Common\Form\Form;
use Common\Form\AbstractForm;
use Zend\Captcha;
use Zend\Form\Element;

class ApplicationForm extends Form 

{
	protected $_limit = 5;
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'ModuleName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'ModuleName eg.User' 
				),
				'options' => array(
						'label' => 'Module Name'
						) 
		)
		 );
		
		$this->add ( array (
				'name' => 'ControllerName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'ControllerName' 
				),
				'options' => array(
						'label' => 'Controller Name'
				)
		)
		 );
		
		$this->add ( array (
				'name' => 'ServiceName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'ServiceName' 
				),
				'options' => array(
						'label' => 'Service Name'
				)
		)
		 );
		$this->add ( array (
				'name' => 'EventName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'EventName' 
				),
				'options' => array(
						'label' => 'Event Name'
				)
		)
		 );
		
		$this->add ( array (
				'name' => 'Entity',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Entity' 
				),
				'options' => array(
						'label' => 'Entity'
				)
		)
		 );
		
		$this->add ( array (
				'name' => 'EntityName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'EntityName' 
				),
				'options' => array(
						'label' => 'Entity Name'
				)
		)
		 );
		
		$this->createColumns ( );
		
	
		
		
		$this->add ( array (
				'name' => 'submit',
				'type' => 'submit',
				'attributes' => array (
						'value' => 'Submit' 
				) 
		) );
	}
	
	public function setLimit($limit){
		$this->_limit = $limit;
		return $this;
	}
	public function getLimit(){
		return $this->_limit;
	}
	public function createColumns() {
		$limit = $this->getLimit();
		for($i = 0; $i < $limit; $i ++) {
			
			$this->add ( array (
					'name' => 'ColumnName_'.$i,
					'type' => 'Zend\Form\Element\Text',
					'attributes' => array (
							'placeholder' => 'ColumnName eg.name' 
					),
					'options' => array(
							'label' => 'Column Name'
					)
					 
			)
			 );
			
			$this->add ( array (
					'name' => 'Key_'.$i,
					'type' => 'Zend\Form\Element\Text',
					'attributes' => array (
							'placeholder' => 'Key eg.user.name' 
					) 
			)
			 );
			
			$this->add ( array (
					'name' => 'Title_'.$i,
					'type' => 'Zend\Form\Element\Text',
					'attributes' => array (
							'placeholder' => 'Title eg.Name'
					)
			)
			);
			$this->add ( array (
					'name' => 'Type_'.$i,
					'type' => 'Zend\Form\Element\Select',
					'attributes' => array (
							'placeholder' => '' 
					),
					'options' => array (
							
							'value_options' => array (
									'string' => 'string',
									'number' => 'number',
									'boolean' => 'boolean',
									'date' => 'date' 
							) 
					) 
			)
			 );
			
			$this->add(array(
					'name' => 'Editable_'.$i,
					'type' => 'Zend\Form\Element\Radio',
					'attributes' => array(
							'value' => '0',
					),
					'options' => array(
							
							'value_options' => array(
									'true' => 'true',
									'false' => 'false'
							),
					),
			));
			
		}
	}
}