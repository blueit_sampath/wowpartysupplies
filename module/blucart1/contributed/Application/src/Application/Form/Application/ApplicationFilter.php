<?php

namespace Application\Form\Application;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ApplicationFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'text',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						
						array (
								'name' => 'Regex',
								
								'options' => array (
										'pattern' => '/^[a-zA-Z]+$/i' 
								) 
						) 
				)
				 
		)
		 );
		
	
		
		
		
		
	}
} 