<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application\Controller;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryBuilder;
use Core\Item\Item;
use Core\Item\Container\ItemContainer;
use Application\Form\ApplicationForm;
use Application\Form\ApplicationFilter;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {
	/*
	 * (non-PHPdoc) @see
	 * \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
	public function indexAction() {
// 		$em = $this->getServiceLocator ()->get ( 'Doctrine\ORM\EntityManager' );
// 		$repository = $em->getRepository ( 'Gedmo\\Translatable\\Entity\\Translation' );
// 		$article = new \Application\Entity\User ();
// 		$article->firstName = 'US';
// 		$repository->translate($article, 'firstName', 'de_DE', 'my article de');
// 		$em->persist ( $article );
// 		$em->flush ();
	}
	
	public function testGridAction(){
		
	}
	
	public function translation(){
		$em = $this->getServiceLocator ()->get ( 'Doctrine\ORM\EntityManager' );
		/*
		
		
		$repository = $em->getRepository ( 'Gedmo\\Translatable\\Entity\\Translation' );
		
		$em = $this->getServiceLocator ()->get ( 'Doctrine\ORM\EntityManager' );
		$repository = $em->getRepository ( 'Gedmo\\Translatable\\Entity\\Translation' );
		
		$article = new \Application\Entity\User ();
		$article->firstName = 'US';
		$repository->translate($article, 'firstName', 'de_DE', 'my article de');
		$em->persist ( $article );
		$em->flush ();
		*/
		
		$dql = "SELECT u.firstName FROM \Application\Entity\User u ORDER BY u.id";
		$query = $em->createQuery ( $dql ); // set the translation query hint
		$query->setHint ( \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE, 'de_DE' );
		$query->setHint(
				\Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
				'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
		);
		$articles = $query->getResult (); // array hydration
		var_dump ( $articles );
	}
}