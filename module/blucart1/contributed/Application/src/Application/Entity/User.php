<?php

namespace Application\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 * @Gedmo\Loggable
 *
 */
class User implements Translatable {
	/**
	 *
	 *@ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
	 */
	private $firstName;
	
	/**
	 *
	 * @ORM\Column(name="last_name", type="string", length=255,nullable=true)
	 */
	private $lastName;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="dob", type="datetime", length=255,
	 *      nullable=true)
	 */
	private $dob;
	/**
	 * @Gedmo\Locale
	 */
	private $locale;
	
	/**
	 * @var datetime $created
	 *
	 * @Gedmo\Timestampable(on="create")
	 * @ORM\Column(type="datetime")
	 */
	private $created;
	
	/**
	 * @var datetime $updated
	 *
	 * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	private $updated;
	
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else
			return $this->$property;
	}
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" .\ucfirst ( $property ) )) {
			$method_name = "set" .\ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}
	
}
