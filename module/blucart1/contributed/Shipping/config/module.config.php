<?php

return array (
		'router' => array (
				'routes' => array (
						'shipping-caluclate' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/shipping-caluclate',
										'defaults' => array (
												'controller' => 'Shipping\Controller\ShippingCaluclate',
												'action' => 'index' 
										) 
								) 
						),
						'admin-shipping-setting' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/setting[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'Shipping\Controller\AdminSetting',
												'action' => 'add',
												'ajax' => 'true' 
										) 
								) 
						),
						'admin-shipping-status' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/status',
										'defaults' => array (
												'controller' => 'Shipping\Controller\AdminShippingStatus',
												'action' => 'index' 
										) 
								) 
						),
						'admin-shipping-status-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/status/add[/ajax/:ajax][/:shippingStatusId]',
										'defaults' => array (
												'controller' => 'Shipping\Controller\AdminShippingStatus',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'shippingStatusId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-order-view-shipping' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/order/view/shipping[/ajax/:ajax][/:orderId]',
										'defaults' => array (
												'controller' => 'Shipping\Controller\AdminOrderViewShipping',
												'action' => 'index',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'orderId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-shipping-config-status' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/config/status[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'Shipping\Controller\AdminShippingConfigStatus',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'Shipping\Service\Grid\AdminShippingStatus' => 'Shipping\Service\Grid\AdminShippingStatus',
				'Shipping\Service\Form\AdminShippingStatus' => 'Shipping\Service\Form\AdminShippingStatus',
				'Shipping\Service\Link\AdminShipping' => 'Shipping\Service\Link\AdminShipping',
				'Shipping\Service\Content\AdminShippingOrderAddress' => 'Shipping\Service\Content\AdminShippingOrderAddress',
				'Shipping\Service\Content\AdminShippingOrder' => 'Shipping\Service\Content\AdminShippingOrder',
				'Shipping\Service\Link\AdminOrderViewShipping' => 'Shipping\Service\Link\AdminOrderViewShipping',
				'Shipping\Service\Form\AdminOrderViewShipping' => 'Shipping\Service\Form\AdminOrderViewShipping',
				'Shipping\Service\Token\AdminOrderViewShipping' => 'Shipping\Service\Token\AdminOrderViewShipping',
				'Shipping\Service\Grid\AdminShippingOrder' => 'Shipping\Service\Grid\AdminShippingOrder',
				'Shipping\Service\Grid\AdminShippingOrderUser' => 'Shipping\Service\Grid\AdminShippingOrderUser',
				'Shipping\Service\Grid\AdminOrderDetailShippingAddress' => 'Shipping\Service\Grid\AdminOrderDetailShippingAddress',
				'Shipping\Service\Grid\AdminOrderDetailShipping' => 'Shipping\Service\Grid\AdminOrderDetailShipping',
				'Shipping\Service\Form\AdminOrderSearch' => 'Shipping\Service\Form\AdminOrderSearch',
				'Shipping\Service\Navigation\AdminShippingStatusNavigation' => 'Shipping\Service\Navigation\AdminShippingStatusNavigation',
				'Shipping\Service\Form\AdminShippingConfigStatus' => 'Shipping\Service\Form\AdminShippingConfigStatus',
				'Shipping\Service\Tab\AdminShippingConfig' => 'Shipping\Service\Tab\AdminShippingConfig',
				'Shipping\Service\Content\UserShippingOrderAddress' => 'Shipping\Service\Content\UserShippingOrderAddress',
				'Shipping\Service\Content\UserShippingOrder' => 'Shipping\Service\Content\UserShippingOrder',
				'Shipping\Service\Form\AdminSetting' => 'Shipping\Service\Form\AdminSetting',
				'Shipping\Service\Content\CartHelperShipping' => 'Shipping\Service\Content\CartHelperShipping',
				'Shipping\Service\Form\CheckoutShipping' => 'Shipping\Service\Form\CheckoutShipping',
				'Shipping\Service\Cart\CartShipping' => 'Shipping\Service\Cart\CartShipping',
				'Shipping\Service\Form\CheckoutShippingSave' => 'Shipping\Service\Form\CheckoutShippingSave' 
		),
		'service_manager' => array (
				'invokables' => array (
						'Shipping\Service\Grid\AdminShippingStatus' => 'Shipping\Service\Grid\AdminShippingStatus',
						'Shipping\Form\AdminShippingStatus\ShippingStatusForm' => 'Shipping\Form\AdminShippingStatus\ShippingStatusForm',
						'Shipping\Form\AdminShippingStatus\ShippingStatusFilter' => 'Shipping\Form\AdminShippingStatus\ShippingStatusFilter',
						'Shipping\Service\Form\AdminShippingStatus' => 'Shipping\Service\Form\AdminShippingStatus',
						'Shipping\Service\Link\AdminShipping' => 'Shipping\Service\Link\AdminShipping',
						'Shipping\Service\Content\AdminShippingOrderAddress' => 'Shipping\Service\Content\AdminShippingOrderAddress',
						'Shipping\Service\Content\AdminShippingOrder' => 'Shipping\Service\Content\AdminShippingOrder',
						'Shipping\Service\Link\AdminOrderViewShipping' => 'Shipping\Service\Link\AdminOrderViewShipping',
						'Shipping\Form\AdminOrderViewShipping\ShippingForm' => 'Shipping\Form\AdminOrderViewShipping\ShippingForm',
						'Shipping\Form\AdminOrderViewShipping\ShippingFilter' => 'Shipping\Form\AdminOrderViewShipping\ShippingFilter',
						'Shipping\Service\Form\AdminOrderViewShipping' => 'Shipping\Service\Form\AdminOrderViewShipping',
						'Shipping\Service\Token\AdminOrderViewShipping' => 'Shipping\Service\Token\AdminOrderViewShipping',
						'Shipping\Service\Grid\AdminShippingOrder' => 'Shipping\Service\Grid\AdminShippingOrder',
						'Shipping\Service\Grid\AdminShippingOrderUser' => 'Shipping\Service\Grid\AdminShippingOrderUser',
						'Shipping\Service\Grid\AdminOrderDetailShippingAddress' => 'Shipping\Service\Grid\AdminOrderDetailShippingAddress',
						'Shipping\Service\Grid\AdminOrderDetailShipping' => 'Shipping\Service\Grid\AdminOrderDetailShipping',
						'Shipping\Service\Form\AdminOrderSearch' => 'Shipping\Service\Form\AdminOrderSearch',
						'Shipping\Service\Navigation\AdminShippingStatusNavigation' => 'Shipping\Service\Navigation\AdminShippingStatusNavigation',
						'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusForm' => 'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusForm',
						'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusFilter' => 'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusFilter',
						'Shipping\Service\Form\AdminShippingConfigStatus' => 'Shipping\Service\Form\AdminShippingConfigStatus',
						'Shipping\Service\Tab\AdminShippingConfig' => 'Shipping\Service\Tab\AdminShippingConfig',
						'Shipping\Service\Content\UserShippingOrderAddress' => 'Shipping\Service\Content\UserShippingOrderAddress',
						'Shipping\Service\Content\UserShippingOrder' => 'Shipping\Service\Content\UserShippingOrder',
						'Shipping\Service\Form\AdminSetting' => 'Shipping\Service\Form\AdminSetting',
						'ShippingContainer' => 'Shipping\Option\ShippingContainer',
						'Shipping\Form\AdminSetting\SettingForm' => 'Shipping\Form\AdminSetting\SettingForm',
						'Shipping\Form\AdminSetting\SettingFilter' => 'Shipping\Form\AdminSetting\SettingFilter',
						'Shipping\Form\ShippingCaluclate\ShippingCaluclateForm' => 'Shipping\Form\ShippingCaluclate\ShippingCaluclateForm',
						'Shipping\Form\ShippingCaluclate\ShippingCaluclateFilter' => 'Shipping\Form\ShippingCaluclate\ShippingCaluclateFilter',
						'Shipping\Service\Content\CartHelperShipping' => 'Shipping\Service\Content\CartHelperShipping',
						'Shipping\Service\Form\CheckoutShipping' => 'Shipping\Service\Form\CheckoutShipping',
						'Shipping\Service\Cart\CartShipping' => 'Shipping\Service\Cart\CartShipping',
						'Shipping\Service\Form\CheckoutShippingSave' => 'Shipping\Service\Form\CheckoutShippingSave' 
				),
				'factories' => array (
						'Shipping\Form\AdminSetting\SettingFactory' => 'Shipping\Form\AdminSetting\SettingFactory',
						'Shipping\Form\AdminShippingStatus\ShippingStatusFactory' => 'Shipping\Form\AdminShippingStatus\ShippingStatusFactory',
						'Shipping\Form\AdminOrderViewShipping\ShippingFactory' => 'Shipping\Form\AdminOrderViewShipping\ShippingFactory',
						'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusFactory' => 'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusFactory',
						'Shipping\Form\ShippingCaluclate\ShippingCaluclateFactory' => 'Shipping\Form\ShippingCaluclate\ShippingCaluclateFactory' 
				),
				'shared' => array (
						'ShippingContainer' => false 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'Shipping\Controller\AdminShippingStatus' => 'Shipping\Controller\AdminShippingStatusController',
						'Shipping\Controller\AdminOrderViewShipping' => 'Shipping\Controller\AdminOrderViewShippingController',
						'Shipping\Controller\AdminShippingConfigGeneral' => 'Shipping\Controller\AdminShippingConfigGeneralController',
						'Shipping\Controller\AdminShippingConfigStatus' => 'Shipping\Controller\AdminShippingConfigStatusController',
						'Shipping\Controller\AdminSetting' => 'Shipping\Controller\AdminSettingController',
						'Shipping\Controller\ShippingCaluclate' => 'Shipping\Controller\ShippingCaluclateController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'Shipping' => __DIR__ . '/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						'Shipping_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__ . '/../src/Shipping/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										'Shipping\Entity' => 'Shipping_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'Shipping' => __DIR__ . '/../public' 
						) 
				) 
		) 
);
