<?php

namespace Shipping\Option;

use Core\Item\Item;

class ShippingItem extends Item {
	protected $_title;
	protected $_value;
	protected $_params = array();
	/**
	 * @return string $_title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * @param field_type $_title
	 * @param class_name
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
		return $this;
	}

	/**
	 * @return string $_value
	 */
	public function getValue() {
		return $this->_value;
	}

	/**
	 * @param field_type $_value
	 * @param class_name
	 */
	public function setValue($_value) {
		$this->_value = $_value;
		return $this;
	}

	/**
	 * @return string $_params
	 */
	public function getParams() {
		return $this->_params;
	}

	/**
	 * @param multitype: $_params
	 * @param class_name
	 */
	public function setParams($_params) {
		$this->_params = $_params;
		return $this;
	}
	
	
	public function addParam($key, $value) {
			
		$this->_params[$key] = $value;
	
		return $this;
	}
	public function getParam($key) {
		if (isset ( $this->_params [$key] )) {
			return $this->_params [$key];
		}
		return false;
	}
	
	public function removeParam($key) {
		if (isset ( $this->_params [$key] )) {
			unset ( $this->_params[$key] );
			return true;
		}
		return false;
	}

}
