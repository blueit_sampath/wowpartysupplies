<?php

namespace Shipping\Option;

use Checkout\Option\ShippingItem;
use Checkout\Option\CartContainer;
use Checkout\Option\Validation\ValidationContainer;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractShippingEvent extends AbstractEvent {
	protected $shippingContainer = null;
	protected $shippingItem = null;
	abstract public function getName();
	
	public function getTitle(){
		return $this->getName();
	}
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$priority = $this->getPriority ();
		$sharedEventManager = $events->getSharedManager ();
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'shipping-get-method', array (
				$this,
				'preShippingGetMethod'
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'shipping-get-quote', array (
				$this,
				'preShippingQuote' 
		), $priority );
		
		return $this;
	}
	public function preShippingGetMethod($e) {
		$shippingContainer = $e->getTarget();
		$this->setShippingContainer( $shippingContainer );
		return $this->shippingGetMethod();
	}
	protected function shippingGetMethod() {
		$shippingContainer = $this->getShippingContainer();
		$item = $shippingContainer->add($this->getName(),$this->getTitle());
		return $item;
	}
	
	public function preShippingQuote($e) {
		$shippingContainer = $e->getTarget();
		$this->setShippingContainer( $shippingContainer );
		return $this->shippingQuote();
	}
	protected function shippingQuote() {
		$shippingContainer = $this->getShippingContainer();
		
	}
	/**
	 * @return ShippingContainer
	 */
	public function getShippingContainer() {
		return $this->shippingContainer;
	}

	/**
	 * @return ShippingItem
	 */
	public function getShippingItem() {
		return $this->shippingItem;
	}

	/**
	 * @param field_type $shippingContainer
	 * @param class_name
	 */
	public function setShippingContainer($shippingContainer) {
		$this->shippingContainer = $shippingContainer;
		return $this;
	}

	/**
	 * @param field_type $shippingItem
	 * @param class_name
	 */
	public function setShippingItem($shippingItem) {
		$this->shippingItem = $shippingItem;
		return $this;
	}

	
	

}
