<?php

namespace Shipping\Option;

use Core\Item\Container\ItemContainer;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Shipping\Option\ShippingItem;

class ShippingContainer extends ItemContainer implements ServiceLocatorAwareInterface, EventManagerAwareInterface {
	protected $_services;
	protected $_events;
	
	protected $_country = '';
	protected $_state = '';
	protected $_postcode = '';
	const SHIPPING_GET_METHOD = 'shipping-get-method';
	const SHIPPING_GET_QUOTE = 'shipping-get-quote';
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services->getServiceLocator ();
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (null === $this->_events) {
			$this->setEventManager ( new EventManager () );
		}
		return $this->_events;
	}
	public function prepare($eventName = 'shipping-get-method', $params = array()) {
		$this->getEventManager ()->trigger ( $eventName, $this, $params );
	}
	public function add($name, $title, $value='', $array = array(), $weight = null) {
		$shippingItem = new ShippingItem ( $name );
		$shippingItem->setTitle ( $title );
		$shippingItem->setValue ( $value );
		$shippingItem->setParams ( $array );
		$shippingItem->setWeight ( $weight );
		$this->addItem ( $shippingItem );
		return $shippingItem;
	}
	/**
	 * @return string $_country
	 */
	public function getCountry() {
		return $this->_country;
	}

	/**
	 * @param string $_country
	 * @param class_name
	 */
	public function setCountry($_country) {
		$this->_country = $_country;
		return $this;
	}

	/**
	 * @return string $_state
	 */
	public function getState() {
		return $this->_state;
	}

	/**
	 * @param string $_state
	 * @param class_name
	 */
	public function setState($_state) {
		$this->_state = $_state;
		return $this;
	}

	/**
	 * @return string $_postcode
	 */
	public function getPostcode() {
		return $this->_postcode;
	}

	/**
	 * @param string $_postcode
	 * @param class_name
	 */
	public function setPostcode($_postcode) {
		$this->_postcode = $_postcode;
		return $this;
	}

}
