<?php 
namespace Shipping\Controller;
use Shipping\Api\ShippingApi;

use Payment\Api\PaymentApi;

use Payment\Api\PaymentStatusApi;

use Config\Api\ConfigApi;

use OrderMain\Api\OrderApi;

use Core\Functions;

use Common\MVC\Controller\AbstractAdminController;

class AdminOrderViewShippingController extends AbstractAdminController {
	
	
	
	public function indexAction() {
		$form = $this->getServiceLocator ()->get ( 'Shipping\Form\AdminOrderViewShipping\ShippingFactory' );
	
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
	
				if ($form->get ( 'notifyCustomer' )->getValue ()) {
					$orderId = Functions::fromRoute ( 'orderId', 0 );
					$order = OrderApi::getOrderById ( $orderId );
					$tokenContainer = $this->parseEmail ();
					$this->getRequest ()->getQuery ()->set ( 'subject', $tokenContainer->getSubject () );
					$this->getRequest ()->getQuery ()->set ( 'htmlMessage', $tokenContainer->getBodyHtml () );
					$this->getRequest ()->getQuery ()->set ( 'plainMessage', $tokenContainer->getBodyText () );
					$this->getRequest ()->getQuery ()->set ( 'fromEmail', ConfigApi::getConfigByKey ( 'ORDER_EMAIL' ) );
					$this->getRequest ()->getQuery ()->set ( 'fromName', ConfigApi::getConfigByKey ( 'ORDER_NAME' ) );
					$this->getRequest ()->getQuery ()->set ( 'toEmails', $order->email );
					$this->getRequest ()->getQuery ()->set ( 'toNames', $order->firstName . ' ' . $order->lastName );
					$this->getRequest ()->getQuery ()->set ( 'redirectUrl', $this->url ()->fromRoute ( 'admin-order-view-shipping', array (
							'orderId' => $form->get ( 'order' )->getValue ()
					) ) );
					return $this->forward ()->dispatch ( 'Mail\Controller\AdminMail', array (
							'action' => 'add'
					) );
				} else {
					return $this->redirect ()->toRoute ( 'admin-order-view-shipping', array (
							'orderId' => $form->get ( 'order' )->getValue ()
					) );
				}
				;
			}
		} else {
				
			$form->getRecord ();
		}
		return array (
				'form' => $form
		);
	}
	
	/**
	 *
	 * @return \Common\Option\Token\TokenContainer
	 */
	public function parseEmail() {
		$orderId = Functions::fromRoute ( 'orderId', 0 );
		$shipping = ShippingApi::getShippingOrderByOrderId($orderId);
		$tokenContainer = $this->getTokenContainer ();
		$tokenContainer->addParam ( 'orderId', $orderId );
		$tokenContainer->setSubject ( $shipping->shippingStatus->subject );
		$tokenContainer->setBodyHtml ( $shipping->shippingStatus->bodyHtml );
		$tokenContainer->setBodyText ( $shipping->shippingStatus->bodyPlain );
		$tokenContainer->prepare ( 'adminOrderMail' );
		return $tokenContainer;
	}
	
	/**
	 *
	 * @return TokenContainer
	 */
	public function getTokenContainer() {
		return $this->getServiceLocator ()->get ( 'TokenContainer' );
	}
	
	
}