<?php 
namespace Shipping\Controller;

use Core\Functions;

use Common\MVC\Controller\AbstractFrontController;
use Shipping\Option\ShippingContainer;
use Zend\View\Model\ViewModel;
use Config\Api\ConfigApi;


class ShippingCaluclateController extends AbstractFrontController {
	
	public function indexAction() {
	
		$country = Functions::fromQuery('country');
		$state = Functions::fromQuery('state');
		$postcode = Functions::fromQuery('postcode');
		
		$result = ConfigApi::getCountryNameByCountryCode($country);
		if($result){
			$country = $result;
		}
		
		$shippingContainer = $this->getShippingContainer();
		$shippingContainer->setCountry($country);
		$shippingContainer->setState($state);
		$shippingContainer->setPostcode($postcode);
		$shippingContainer->prepare(ShippingContainer::SHIPPING_GET_QUOTE);
		$viewModel = new ViewModel(array(
			'shippingContainer' => $shippingContainer
		));
		
		return $viewModel;
		
	}
	
	
	
	/**
	 * 
	 * @return ShippingContainer
	 */
	public function getShippingContainer(){
		$sm = $this->getServiceLocator();
		return $sm->get('ShippingContainer');
	}
	
	
	
	
}