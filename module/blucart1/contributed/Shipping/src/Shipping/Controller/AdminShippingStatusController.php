<?php

namespace Shipping\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminShippingStatusController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Shipping\Form\AdminShippingStatus\ShippingStatusFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-shipping-status-add', array (
				'shippingStatusId' => $form->get ( 'id' )->getValue () 
		) );
	}
}