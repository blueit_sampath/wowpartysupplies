<?php

namespace Shipping\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminShippingConfigStatusController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-shipping-config-status' );
	}
}