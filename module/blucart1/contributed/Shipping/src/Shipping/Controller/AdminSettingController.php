<?php

namespace Shipping\Controller;

use Common\MVC\Controller\AbstractAdminController;


class AdminSettingController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Shipping\Form\AdminSetting\SettingFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-shipping-setting' );
	}
	
	
}