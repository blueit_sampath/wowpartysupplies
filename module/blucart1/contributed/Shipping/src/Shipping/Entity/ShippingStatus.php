<?php

namespace Shipping\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ShippingStatus
 *
 * @ORM\Table(name="shipping_status")
 * @ORM\Entity
 */
class ShippingStatus {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="name", type="string", length=255,
	 *      nullable=true)
	 */
	private $name;
	
	/**
	 *
	 * @var integer @ORM\Column(name="weight", type="integer", nullable=true)
	 */
	private $weight;
	
	
	
	/**
	 *
	 * @var text @ORM\Column(name="subject", type="string", length=255,
	 *      nullable=true)
	 */
	private $subject;
	
	/**
	 *
	 * @var text @ORM\Column(name="body_html", type="text", nullable=true)
	 */
	private $bodyHtml;
	/**
	 *
	 * @var text @ORM\Column(name="body_plain", type="text", nullable=true)
	 */
	private $bodyPlain;
	
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
	public function __isset($name){
		return true;
	}
}
