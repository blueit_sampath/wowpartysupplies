<?php

namespace Shipping\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ShippingOrder
 *
 * @ORM\Table(name="shipping_order")
 * @ORM\Entity
 */
class ShippingOrder {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="name", type="string", length=255,
	 *      nullable=true)
	 */
	private $name;
	
	/**
	 *
	 * @var string @ORM\Column(name="tracking_no", type="string",
	 *      length=255, nullable=true)
	 */
	private $trackingNo;
	
	/**
	 *
	 * @var float @ORM\Column(name="amount", type="float", nullable=true)
	 */
	private $amount;
	
	/**
	 *
	 * @var \OrderMain
	 *      @ORM\ManyToOne(targetEntity="\OrderMain\Entity\OrderMain")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="order_main_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $orderMain;
	
	/**
	 *
	 * @var \ShippingStatus @ORM\ManyToOne(targetEntity="ShippingStatus")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="shipping_status_id",
	 *      referencedColumnName="id",nullable=true,onDelete="SET NULL")
	 *      })
	 */
	private $shippingStatus;
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
	
	public function __isset($name){
		return true;
	}
}
