<?php

namespace Shipping\Api;

use Common\Api\Api;

class ShippingStatusApi extends Api {
	protected static $_entity = '\Shipping\Entity\ShippingStatus';
	public static function getShippingStatusById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getShippingStatuses() {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'shippingStatus.id', 'shippingStatusId' );
		$queryBuilder->addOrder ( 'shippingStatus.weight', 'shippingStatusWeight', 'desc' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'shippingStatus' );
		return $queryBuilder->executeQuery ();
	}
}
