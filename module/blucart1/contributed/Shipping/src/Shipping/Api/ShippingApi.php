<?php 
namespace Shipping\Api;

use Common\Api\Api;
class  ShippingApi  extends Api {
	protected static $_entity = '\Shipping\Entity\ShippingStatus';
	protected static $_addressEntity = '\Shipping\Entity\ShippingOrderAddress';
	protected static $_orderEntity = '\Shipping\Entity\ShippingOrder';
	
	
	public static function getShippingAddressByOrderId($orderId) {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_addressEntity )->findOneBy ( array (
				'orderMain' => $orderId
		) );
	}
	
	public static function getShippingOrderByOrderId($orderId) {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_orderEntity )->findOneBy ( array (
				'orderMain' => $orderId
		) );
	}
}
