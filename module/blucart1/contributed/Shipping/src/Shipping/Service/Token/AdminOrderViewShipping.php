<?php

namespace Shipping\Service\Token;

use Shipping\Api\ShippingApi;

use Core\Functions;
use Common\Option\Token\AbstractTokenEvent;

class AdminOrderViewShipping extends AbstractTokenEvent {
	public function getEventName() {
		return 'adminOrderMail';
	}
	public function token() {
		$tokenContainer = $this->getTokenContainer ();
		$orderId = $tokenContainer->getParam ( 'orderId' );
		$entity = ShippingApi::getShippingOrderByOrderId($orderId);
		$item = $tokenContainer->add ( 'shipping', $entity, '', 900 );
		
		$entity = ShippingApi::getShippingAddressByOrderId($orderId);
		$item = $tokenContainer->add ( 'shippingAddress', $entity, '', 900 );
		
		return $this;
	}
}

