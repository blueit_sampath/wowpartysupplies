<?php 
namespace Shipping\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class CartHelperShipping extends AbstractContentEvent {

	protected $_contentTemplate = 'common/cart-helper-shipping';
	protected $_contentName = 'cartHelperShipping';
	public function getEventName() {
		return 'cartHelper';
	}
	public function getPriority() {
		return 1000;
	}
	
}

