<?php 
namespace Shipping\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class UserShippingOrder extends AbstractContentEvent {

	protected $_contentTemplate = 'common/user-shipping-order';
	protected $_contentName = 'userShippingOrder';
	public function getEventName() {
		return 'userOrderContentTop';
	}
	public function getPriority() {
		return 1000;
	}
	
}

