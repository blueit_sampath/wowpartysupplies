<?php 
namespace Shipping\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminShippingOrderAddress extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-shipping-order-address';
	protected $_contentName = 'adminShippingOrderAddress';
	public function getEventName() {
		return 'adminOrderContentTop';
	}
	public function getPriority() {
		return 1000;
	}
	
}

