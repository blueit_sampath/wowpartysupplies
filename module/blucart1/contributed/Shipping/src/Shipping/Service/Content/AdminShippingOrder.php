<?php 
namespace Shipping\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminShippingOrder extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-shipping-order';
	protected $_contentName = 'adminShippingOrder';
	public function getEventName() {
		return 'adminOrderContentTop';
	}
	public function getPriority() {
		return 1000;
	}
	
}

