<?php 
namespace Shipping\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class UserShippingOrderAddress extends AbstractContentEvent {

	protected $_contentTemplate = 'common/user-shipping-order-address';
	protected $_contentName = 'userShippingOrderAddress';
	public function getEventName() {
		return 'userOrderContentTop';
	}
	public function getPriority() {
		return 1000;
	}
	
}

