<?php 
namespace Shipping\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminShipping extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminShippingStatus';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-shipping-status-add');
		$item = $linkContainer->add ( 'admin-shipping-status-add', 'Add Shipping Status', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
	
}

