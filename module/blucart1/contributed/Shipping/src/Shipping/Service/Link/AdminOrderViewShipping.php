<?php

namespace Shipping\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminOrderViewShipping extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminOrderView';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$orderId = Functions::fromRoute ( 'orderId' );
		$u = $url ( 'admin-order-view-shipping', array (
				'orderId' => $orderId 
		) );
		$item = $linkContainer->add ( 'admin-order-view-shipping', 'Change Shipping Status', $u, 800 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

