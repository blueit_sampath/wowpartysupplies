<?php 
namespace Shipping\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;



class AdminShippingOrderUser extends AdminShippingOrder {
	
	
	public function getEventName() {
		return 'adminOrderUser';
		
	}
	
	
}
