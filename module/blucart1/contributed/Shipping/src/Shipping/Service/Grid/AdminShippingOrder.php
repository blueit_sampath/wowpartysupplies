<?php 
namespace Shipping\Service\Grid;

use BlucartGrid\Event\AbstractBlucartGridEvent;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;
class AdminShippingOrder extends AbstractBlucartGridEvent {
	

	protected $_entity = '\Shipping\Entity\ShippingOrder';
	protected $_entityName = 'shippingOrder';
	public function getEventName() {
		return 'adminOrder';
		
	}
	
	public function getPriority(){
		return 800;
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
	
		$array = array ();
	
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
	
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'shippingStatusName' );
		$columnItem->setTitle ( 'Shipping Status' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 930 );
	
		$columns->addColumn ( 'shippingStatus.name', $columnItem );
	
		return $columns;
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
	
		$orderFrom = $queryBuilder->getFrom ( 'orderMain' );
		$joinItem = new QueryJoinItem ( $this->_entity, 'shippingOrder', 'left join', 'orderMain.id = shippingOrder.orderMain' );
		$joinItem2 = new QueryJoinItem ( 'shippingOrder.shippingStatus', 'shippingStatus' );
		$joinItem->addJoin ( $joinItem2 );
		$orderFrom->addJoin ( $joinItem );
		$queryBuilder->addGroup ( 'orderMain.id', 'orderMainId' );
	
		return true;
	}
	
	
}
