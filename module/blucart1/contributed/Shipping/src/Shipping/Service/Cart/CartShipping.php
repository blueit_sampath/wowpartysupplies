<?php

namespace Shipping\Service\Cart;

use Core\Functions;
use Checkout\Option\CartEvent;
use Checkout\Option\CartContainer;
use Shipping\Option\ShippingContainer;

class CartShipping extends CartEvent {
	public function prepareCart() {
		$cart = $this->getCart ();
		
		$cart->removeOutItem ( 'shipping' );
		
		$type = $cart->getCartType ();
		$shippingContainer = $this->getShippingContainer();
		
		if ($type == CartContainer::CART_PREVIEW) {
			$param = $cart->getParam ( 'dataPreview' );
			$this->bindData($param, $shippingContainer, $cart);
		}
		if ($type == CartContainer::CART_COMPLETE) {
			$param = $cart->getParam ( 'formData' );
			$this->bindData($param, $shippingContainer, $cart);
		}
	}
	
	public function bindData($param, $shippingContainer, $cart){
		if (! $param) {
			return;
		}
		if (isset ( $param ['cartCheckoutShipping'] ['shipping'] ) && $param ['cartCheckoutShipping'] ['shipping']) {
			$id = $param ['cartCheckoutShipping'] ['shipping'];
			if ($item = $shippingContainer->getItem ( $id )) {
				$cart->addQuickOutItem ( 'shipping', $item->getTitle (), $item->getValue (), array (
						'shippingId' => $item->getId ()
				) );
			}
		}
	}
	/**
	 *
	 * @return ShippingContainer
	 */
	public function getShippingContainer() {
		$sm = Functions::getServiceLocator ();
		$shippingContainer = $sm->get ( 'ShippingContainer' );
		$cart = $this->getCart ();
		$array = array ();
		$params = $cart->getParam ( 'shippingAddress' );
		if (! $params) {
			return $array;
		}
		$shippingContainer->setCountry ( $params ['country'] );
		$shippingContainer->setState ( $params ['state'] );
		$shippingContainer->setPostcode ( $params ['postcode'] );
		$shippingContainer->prepare ( ShippingContainer::SHIPPING_GET_QUOTE );
		
		return $shippingContainer;
	}
}

