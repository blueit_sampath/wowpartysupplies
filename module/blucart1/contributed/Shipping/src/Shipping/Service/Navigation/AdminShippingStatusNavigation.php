<?php 
namespace Shipping\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminShippingStatusNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		
		$page = $navigation->findOneBy ( 'id', 'orderMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Shipping',
							'uri' => '#',
							'id' => 'orderMainShippingMain',
							'iconClass' => 'glyphicon-envelope'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderMainShippingMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Setting',
							'route' => 'admin-shipping-setting',
							'id' => 'admin-shipping-setting',
							'aClass' => 'zoombox',
							'iconClass' => 'glyphicon-wrench'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderMainShippingMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Shipping Status',
							'route' => 'admin-shipping-status',
							'id' => 'admin-shipping-status',
							'iconClass' => 'glyphicon-ok' 
					) 
			) );
		}
		
		
		
		$page = $navigation->findOneBy ( 'id', 'orderMainShippingMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Shipping Status Setting',
							'route' => 'admin-shipping-config-status',
							'id' => 'admin-shipping-config-status',
							'iconClass' => 'glyphicon glyphicon-wrench',
							'aClass' => 'zoombox'
					)
			) );
		}
	}
}

