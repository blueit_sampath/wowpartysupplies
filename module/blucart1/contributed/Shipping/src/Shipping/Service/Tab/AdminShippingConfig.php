<?php

namespace Shipping\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminShippingConfig extends AbstractTabEvent {
	public function getEventName() {
		return 'adminShippingConfig';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-shipping-config-status' );
		$tabContainer->add ( 'admin-shipping-config-status', 'Status', $u, 980 );
		
		return $this;
	}
}

