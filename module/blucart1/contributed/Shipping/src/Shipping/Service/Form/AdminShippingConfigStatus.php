<?php

namespace Shipping\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminShippingConfigStatus extends AdminConfig {
	protected $_columnKeys = array (
			'SHIPPING_STATUS_DEFAULT_SUBJECT',
			'SHIPPING_STATUS_DEFAULT_HTMLMESSAGE',
			'SHIPPING_STATUS_DEFAULT_PLAINMESSAGE' 
	);
	public function getFormName() {
		return 'adminShippingConfigStatus';
	}
	public function getPriority() {
		return 1000;
	}
}
