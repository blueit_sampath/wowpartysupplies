<?php

namespace Shipping\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;


class CheckoutShipping extends AbstractMainFormEvent {
	public function getFormName() {
		return 'cartCheckout';
	}
	public function getPriority() {
		return 500;
	}
	public function preInitEvent() {
		$form = $this->getForm ();
		$form->add ( array (
				'name' => 'cartCheckoutShipping',
				'type' => 'Shipping\Form\CartCheckoutShippingFieldset',
				'options' => array (
						'legend' => 'Delivery' 
				) 
		), array (
				'priority' => 1000 
		) );
		if(!$form->hasElements('cartCheckoutShipping')){
		$form->remove('cartCheckoutShipping');	
		}
	}
}
