<?php

namespace Shipping\Service\Form;

use Shipping\Api\ShippingStatusApi;
use Shipping\Entity\ShippingStatus;
use OrderMain\Api\OrderApi;
use Shipping\Api\ShippingApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderViewShipping extends AbstractMainFormEvent {
	protected $_columnKeys = array ();
	protected $_entity = '';
	protected $_entityName = '';
	public function getFormName() {
		return 'adminOrderViewShipping';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		$params = $this->parseSaveParams ( $params );
		$em = $this->getEntityManager ();
		
		if (isset ( $params ['order'] ) && $params ['order']) {
			$entity = ShippingApi::getShippingOrderByOrderId ( $params ['order'] );
		} else {
			$entity = new $this->_entity ();
			$entity->order = OrderApi::getOrderById ( $params ['order'] );
		}
		$entity->shippingStatus = ShippingStatusApi::getShippingStatusById ( $params ['shippingStatus'] );
		$entity->trackingNo = $params ['trackingNo'];
		$em->persist ( $entity );
		$em->flush ();
		return true;
	}
	public function getRecord() {
		$form = $this->getForm ();
		$orderId = Functions::fromRoute ( 'orderId' );
		$form->get ( 'order' )->setValue ( $orderId );
		
		$entity = ShippingApi::getShippingOrderByOrderId ( $orderId );
		
		if ($entity && $entity->shippingStatus) {
			$form->get ( 'shippingStatus' )->setValue ( $entity->shippingStatus->id );
			$form->get ( 'trackingNo' )->setValue ( $entity->trackingNo );
		}
	}
}
