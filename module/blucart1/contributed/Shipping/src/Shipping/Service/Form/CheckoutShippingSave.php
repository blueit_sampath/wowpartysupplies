<?php

namespace Shipping\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;
use OrderMain\Api\OrderApi;

class CheckoutShippingSave extends AbstractMainFormEvent {
	public function getFormName() {
		return 'cartCheckout';
	}
	public function getPriority() {
		return 500;
	}
	public function save() {
		$form = $this->getForm ();
		$user = UserApi::getLoggedInUser ();
		$cart = $this->getCart ();
		$em = Functions::getEntityManager ();
		
		$shippingAddress = $cart->getParam ( 'shippingAddress' );
		
		$orderId = $cart->getParam ( 'orderId' );
		if (! $orderId) {
			return;
		}
		$orderMain = OrderApi::getOrderById ( $orderId );
		if (! $orderMain) {
			return;
		}
		
		/* Order Shipping Address */
		$orderShippingAddress = new \Shipping\Entity\ShippingOrderAddress ();
		$array = array (
				'firstName',
				'lastName',
				'email',
				'addressLine1',
				'addressLine2',
				'city',
				'state',
				'country',
				'postcode',
				'contactNumber' 
		);
		foreach ( $array as $key ) {
			$orderShippingAddress->$key = $shippingAddress [$key];
		}
		$orderShippingAddress->orderMain = $orderMain;
		$em->persist ( $orderShippingAddress );
		
	
		/* Order Shipping */
		$shipping = $cart->getOutItem ( 'shipping' );
		if ($shipping) {
			$orderShipping = new \Shipping\Entity\ShippingOrder ();
			$orderShipping->name = $shipping->getParam ( 'shippingId' );
			$orderShipping->amount = $shipping->getValue ();
			$orderShipping->orderMain = $orderMain;
			$em->persist ( $orderShipping );
		}
		
		$em->flush ();
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	protected function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
