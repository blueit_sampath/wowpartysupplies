<?php

namespace Shipping\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use Config\Api\ConfigApi;
use Core\Functions;

class AdminSetting extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'SHIPPING_METHOD',
			
	);
	public function getFormName() {
		return 'adminShippingSetting';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$shippingValue = '';
		if(isset($params['SHIPPING_METHOD'])){
			$shippingValue = implode(',', $params['SHIPPING_METHOD']);
		} 
		ConfigApi::createOrUpdateKey ( 'SHIPPING_METHOD', $shippingValue );
		
		return true;
	}
	
	public function getRecord() {
		$form = $this->getForm ();
		
		if($form->has('SHIPPING_METHOD')){
			$value = ConfigApi::getConfigByKey ( 'SHIPPING_METHOD', null );
			
			if ($value) {
				$value = explode(',', $value);
				$form->get('SHIPPING_METHOD')->setValue ( $value );
			}
		}
		return true;
	}
	
}
