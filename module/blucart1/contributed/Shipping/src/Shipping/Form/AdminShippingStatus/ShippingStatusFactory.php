<?php
namespace Shipping\Form\AdminShippingStatus;

use Common\Form\Option\AbstractFormFactory;

class ShippingStatusFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Shipping\Form\AdminShippingStatus\ShippingStatusForm' );
		$form->setName ( 'adminShippingStatusAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Shipping\Form\AdminShippingStatus\ShippingStatusFilter' );
	}
}
