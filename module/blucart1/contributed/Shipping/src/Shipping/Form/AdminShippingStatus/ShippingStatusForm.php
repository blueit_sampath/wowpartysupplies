<?php
namespace Shipping\Form\AdminShippingStatus;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class ShippingStatusForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden'
				)
		) );
		
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'Name'
				)
		), array (
				'priority' => 1000
		) );
		
		
		
		$this->add ( array (
				'name' => 'weight',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'Priority'
				)
		), array (
				'priority' => 960
		) );
		
		$this->add ( array (
				'name' => 'mailFieldset',
				'type' => 'Shipping\Form\AdminShippingStatus\MailFieldset',
				'options' => array (
						'legend' => 'Mail Settings'
				)
		),
		
				array (
						'priority' => 940
				) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit'
				)
		), array (
				'priority' => - 100
		) );
	}
	
}