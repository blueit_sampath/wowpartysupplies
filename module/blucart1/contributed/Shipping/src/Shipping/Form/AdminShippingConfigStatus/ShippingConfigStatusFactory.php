<?php
namespace Shipping\Form\AdminShippingConfigStatus;

use Common\Form\Option\AbstractFormFactory;

class ShippingConfigStatusFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusForm' );
		$form->setName ( 'adminShippingConfigStatus' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Shipping\Form\AdminShippingConfigStatus\ShippingConfigStatusFilter' );
	}
}
