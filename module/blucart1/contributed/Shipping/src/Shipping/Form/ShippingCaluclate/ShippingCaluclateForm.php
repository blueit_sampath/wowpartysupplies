<?php
namespace Shipping\Form\ShippingCaluclate;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use LocaleElement\Form\Element\CountryPicker;
use LocaleElement\Form\Element\StatePicker;


class ShippingCaluclateForm extends Form 

{
	public function init() {
		$country = new CountryPicker('country');
		$country->setLabel('Country');
		$this->add($country,array(
			'priority' => 1000
				
		));
		
		$state = new StatePicker('state');
		$state->setLabel('State');
		$state->setCountry('country');
		$this->add($state, array(
			'priority' => 990
		));
		
		
		
		$this->add ( array (
				'name' => 'postcode',
				'attributes' => array (
						'type' => 'text',
				),
				'options' => array (
						'label' => 'Postcode' 
				) 
		), array (
				'priority' => 980 
		) );
		
		
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Calculate' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}