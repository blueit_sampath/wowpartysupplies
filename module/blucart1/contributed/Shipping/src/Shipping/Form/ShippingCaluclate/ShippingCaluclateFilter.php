<?php

namespace Shipping\Form\ShippingCaluclate;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ShippingCaluclateFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'country',
				'required' => true 
		)
		 );
		
		$this->add ( array (
				'name' => 'state',
				'required' => false 
		)
		 );
		
		$this->add ( array (
				'name' => 'postcode',
				'required' => false 
		)
		 );
	}
} 