<?php
namespace Shipping\Form\ShippingCaluclate;

use Common\Form\Option\AbstractFormFactory;

class ShippingCaluclateFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Shipping\Form\ShippingCaluclate\ShippingCaluclateForm' );
		$form->setName ( 'shippingCaluclate' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Shipping\Form\ShippingCaluclate\ShippingCaluclateFilter' );
	}
}
