<?php

namespace Shipping\Form;


use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Form\Element\Radio;
use Core\Functions;
use Shipping\Option\ShippingContainer;
use Checkout\Option\CartContainer;

class CartCheckoutShippingFieldset extends Fieldset implements InputFilterProviderInterface {
	public function __construct($name = 'cartCheckoutShipping') {
		parent::__construct ( $name );
		
		$array = $this->getShippingQuotes ();
		if (count ( $array )) {
			$radio = new Radio ( 'shipping' );
			$radio->setValueOptions ( $array );
			$radio->setAttribute('data-checkout-change', 'shipping');
			$this->add ( $radio, array (
					'priority' => 1000 
			) );
		}
	}
	
	/**
	 * Should return an array specification compatible with
	 * {@link Zend\InputFilter\Factory::createInputFilter()}.
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		$array = $this->getShippingQuotes ();
		if (count ( $array )) {
			return array (
					'shipping' => array (
							'required' => true 
					) 
			);
		}
		return array ();
	}
	/**
	 * 
	 * @return multitype:|multitype:string
	 */
	public function getShippingQuotes() {
		$array = array ();
		$cart = $this->getCart ();
		$shippingContainer = $this->getShippingContainer ();
		$params = $cart->getParam ( 'shippingAddress' );
		if (! $params) {
			return $array;
		}
	
		$shippingContainer->setCountry ( $params ['country'] );
		$shippingContainer->setState ( $params ['state'] );
		$shippingContainer->setPostcode ( $params ['postcode'] );
		$shippingContainer->prepare ( ShippingContainer::SHIPPING_GET_QUOTE );
		$items = $shippingContainer->getItems ();
		$currencyFormat = Functions::getViewHelperManager()->get('currencyFormat');
		foreach ( $items as $item ) {
			$array [$item->getId ()] = $item->getTitle () . ' (' . $currencyFormat($item->getValue ()) . ')';
		}
	
		return $array;
	}
	/**
	 *
	 * @return ShippingContainer
	 */
	public function getShippingContainer() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'ShippingContainer' );
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	public function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
	
}
