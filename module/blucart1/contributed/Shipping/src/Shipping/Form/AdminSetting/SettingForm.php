<?php

namespace Shipping\Form\AdminSetting;

use Core\Functions;
use Common\Form\Form;
use Zend\Form\Element\MultiCheckbox;

class SettingForm extends Form 

{
	public function init() {
		$array = $this->getShippingMethods ();
		if (count ( $array )) {
			$element = new MultiCheckbox ( 'SHIPPING_METHOD' );
			$element->setLabel ( 'Shipping Modules' );
			$element->setValueOptions ( $array );
			$this->add ( $element, array (
					'priority' => 1000 
			) );
		}
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getShippingMethods() {
		$array = array ();
		$sm = Functions::getServiceLocator ();
		$shippingContainer = $sm->get ( 'ShippingContainer' );
		$shippingContainer->prepare ();
		$items = $shippingContainer->getItems ();
		foreach ( $items as $item ) {
			$array [$item->getId ()] = $item->getTitle ();
		}
		return $array;
	}
}