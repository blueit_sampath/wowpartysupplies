<?php
namespace Shipping\Form\AdminSetting;

use Common\Form\Option\AbstractFormFactory;

class SettingFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Shipping\Form\AdminSetting\SettingForm' );
		$form->setName ( 'adminShippingSetting' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Shipping\Form\AdminSetting\SettingFilter' );
	}
}
