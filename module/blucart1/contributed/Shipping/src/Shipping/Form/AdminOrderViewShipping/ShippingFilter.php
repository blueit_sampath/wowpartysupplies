<?php
namespace Shipping\Form\AdminOrderViewShipping;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  ShippingFilter extends InputFilter 

{
	protected $inputFilter;
public function __construct() {
		
		$this->add ( array (
				'name' => 'shippingStatus',
				'required' => true,
				
		) );
		$this->add ( array (
				'name' => 'trackingNo',
				'required' => true,
		
		) );
		$this->add ( array (
				'name' => 'notifyCustomer',
				'required' => false,
				
		) );
	}
} 