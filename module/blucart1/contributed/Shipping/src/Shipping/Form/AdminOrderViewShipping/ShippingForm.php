<?php
namespace Shipping\Form\AdminOrderViewShipping;

use Shipping\Api\ShippingStatusApi;

use Shipping\Api\ShippingApi;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class ShippingForm extends Form 

{
public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'order',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'shippingStatus' );
		$select->setLabel ( 'Shipping Status' );
		$select->setValueOptions ( $this->getShippingStatus () );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		
		$this->add ( array (
				'type' => 'text',
				'name' => 'trackingNo',
				'options' => array (
						'label' => 'Tracking Number'		
				)
		), array (
				'priority' => 990
		) );
		
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'notifyCustomer',
				'options' => array (
						'label' => 'Notify Customer',
						'use_hidden_element' => true 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getShippingStatus() {
		$array = array (
				'' => '' 
		);
		
		$statuses = ShippingStatusApi::getShippingStatuses();
		foreach ( $statuses as $statusId ) {
			$status = ShippingStatusApi::getShippingStatusById( $statusId ['shippingStatusId'] );
			$array [$status->id] = $status->name;
		}
		return $array;
	}
	
}