<?php
namespace Shipping\Form\AdminOrderViewShipping;

use Common\Form\Option\AbstractFormFactory;

class ShippingFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Shipping\Form\AdminOrderViewShipping\ShippingForm' );
		$form->setName ( 'adminOrderViewShipping' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Shipping\Form\AdminOrderViewShipping\ShippingFilter' );
	}
}
