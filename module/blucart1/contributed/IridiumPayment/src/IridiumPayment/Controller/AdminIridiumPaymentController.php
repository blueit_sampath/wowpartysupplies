<?php
namespace IridiumPayment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminIridiumPaymentController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-iridiumpayment-setting');
    }
}