<?php
namespace IridiumPayment\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminPaymentSetting extends AbstractTabEvent
{

    public function getEventName()
    {
        return 'adminPaymentSetting';
    }

    public function tab()
    {
        $tabContainer = $this->getTabContainer();
        $url = Functions::getUrlPlugin();
        
        $u = $url('admin-iridiumpayment-setting');
        $tabContainer->add('admin-iridiumpayment-setting', 'Iridium Payment', $u, 500);
        
        return $this;
    }
}

