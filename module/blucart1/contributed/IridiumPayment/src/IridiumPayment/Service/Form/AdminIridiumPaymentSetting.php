<?php
namespace IridiumPayment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminIridiumPaymentSetting extends AdminConfig
{

    protected $_columnKeys = array(
        'PAYMENT_IRIDIUM_TITLE',
        'PAYMENT_IRIDIUM_MERCHANTID',
         'PAYMENT_IRIDIUM_PASSWORD',
        'PAYMENT_IRIDIUM_PAYMENTPROCESSORDOMAIN',
        'PAYMENT_IRIDIUM_HASHMETHOD',
        'PAYMENT_IRIDIUM_PRESHAREDKEY',
        'PAYMENT_IRIDIUM_RESULTDELIVERYMETHOD'
    );

    public function getFormName()
    {
        return 'adminIridiumPaymentConfig';
    }

    public function getPriority()
    {
        return 1000;
    }
}
