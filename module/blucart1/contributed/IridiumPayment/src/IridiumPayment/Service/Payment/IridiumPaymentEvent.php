<?php
namespace IridiumPayment\Service\Payment;

use Core\Functions;
use Checkout\Option\CartContainer;
use Payment\Option\AbstractPaymentEvent;
use Config\Api\ConfigApi;

class IridiumPaymentEvent extends AbstractPaymentEvent
{

    public function getName()
    {
        return 'iridiumPayment';
    }

    public function getTitle()
    {
        $title = ConfigApi::getConfigByKey('PAYMENT_IRIDIUM_TITLE');
        if (! $title) {
            $title = 'Iridium';
        }
        return $title;
    }

    public function getRouteName()
    {
        return 'iridiumpayment-send';
    }
}
