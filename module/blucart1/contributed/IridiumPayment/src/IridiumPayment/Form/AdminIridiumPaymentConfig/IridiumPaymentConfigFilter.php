<?php

namespace IridiumPayment\Form\AdminIridiumPaymentConfig;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IridiumPaymentConfigFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'PAYMENT_IRIDIUM_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_IRIDIUM_MERCHANTID',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_IRIDIUM_PASSWORD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
                , array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_IRIDIUM_PAYMENTPROCESSORDOMAIN',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_IRIDIUM_HASHMETHOD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_IRIDIUM_PRESHAREDKEY',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_IRIDIUM_RESULTDELIVERYMETHOD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));
    }

}
