<?php
namespace IridiumPayment\Form\AdminIridiumPaymentConfig;

use Common\Form\Option\AbstractFormFactory;

class IridiumPaymentConfigFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigForm');
        $form->setName('adminIridiumPaymentConfig');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigFilter');
    }
}
