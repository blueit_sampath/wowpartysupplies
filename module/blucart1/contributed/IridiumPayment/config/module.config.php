<?php
return array(
    'router' => array(
        'routes' => array(
            'iridiumpayment-send' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/iridiumpayment/send[/:orderId]',
                    'defaults' => array(
                        'controller' => 'IridiumPayment\Controller\Index',
                        'action' => 'send'
                    )
                )
            ),
            'iridiumpayment-land' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/iridiumpayment/land',
                    'defaults' => array(
                        'controller' => 'IridiumPayment\Controller\Index',
                        'action' => 'land'
                    )
                )
            ),
            'iridiumpayment-error' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/iridiumpayment/error',
                    'defaults' => array(
                        'controller' => 'IridiumPayment\Controller\Index',
                        'action' => 'error'
                    )
                )
            ),
            'admin-iridiumpayment-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/iridiumpayment/setting[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'IridiumPayment\Controller\AdminIridiumPayment',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            )
        )
    ),
    'events' => array(
        'IridiumPayment\Service\Payment\IridiumPaymentEvent' => 'IridiumPayment\Service\Payment\IridiumPaymentEvent',
        'IridiumPayment\Service\Form\AdminIridiumPaymentSetting' => 'IridiumPayment\Service\Form\AdminIridiumPaymentSetting',
        'IridiumPayment\Service\Tab\AdminPaymentSetting' => 'IridiumPayment\Service\Tab\AdminPaymentSetting'
    ),
    'service_manager' => array(
        'invokables' => array(
            'IridiumPayment\Service\Payment\IridiumPaymentEvent' => 'IridiumPayment\Service\Payment\IridiumPaymentEvent',
            'IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigForm' => 'IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigForm',
            'IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigFilter' => 'IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigFilter',
            'IridiumPayment\Service\Form\AdminIridiumPaymentSetting' => 'IridiumPayment\Service\Form\AdminIridiumPaymentSetting',
            'IridiumPayment\Service\Tab\AdminPaymentSetting' => 'IridiumPayment\Service\Tab\AdminPaymentSetting'
        ),
        'factories' => array(
            'IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigFactory' => 'IridiumPayment\Form\AdminIridiumPaymentConfig\IridiumPaymentConfigFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'IridiumPayment\Controller\Index' => 'IridiumPayment\Controller\IndexController',
            'IridiumPayment\Controller\AdminIridiumPayment' => 'IridiumPayment\Controller\AdminIridiumPaymentController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'IridiumPayment' => __DIR__ . '/../view'
        )
    ),
    
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'IridiumPayment' => __DIR__ . '/../public'
            )
        )
    )
);
