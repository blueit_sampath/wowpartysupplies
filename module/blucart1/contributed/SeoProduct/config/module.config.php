<?php

namespace SeoProduct;

return array(
    'router' => array(
        'routes' => array(
            'admin-seo-config-product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo/config/product[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'SeoProduct\Controller\AdminSeoConfig',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            ),
            'admin-seo-product-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo/product/add[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'SeoProduct\Controller\AdminSeo',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
        )
    ),
    'events' => array(
        'SeoProduct\Service\Form\AdminSeoConfigGeneral' => 'SeoProduct\Service\Form\AdminSeoConfigGeneral',
        'SeoProduct\Service\Link\AdminSeo' => 'SeoProduct\Service\Link\AdminSeo',
        'SeoProduct\Service\Tab\AdminSeo' => 'SeoProduct\Service\Tab\AdminSeo',
        'SeoProduct\Service\Form\AdminSeo' => 'SeoProduct\Service\Form\AdminSeo',
    ),
    'service_manager' => array(
        'invokables' => array(
            'SeoProduct\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm' => 'SeoProduct\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm',
            'SeoProduct\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter' => 'SeoProduct\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter',
            'SeoProduct\Service\Form\AdminSeoConfigGeneral' => 'SeoProduct\Service\Form\AdminSeoConfigGeneral',
            'SeoProduct\Service\Link\AdminSeo' => 'SeoProduct\Service\Link\AdminSeo',
            'SeoProduct\Service\Form\AdminSeo' => 'SeoProduct\Service\Form\AdminSeo',
            'SeoProduct\Service\Tab\AdminSeo' => 'SeoProduct\Service\Tab\AdminSeo',
            'SeoProduct\Form\AdminSeo\SeoFilter' => 'SeoProduct\Form\AdminSeo\SeoFilter',
            'SeoProduct\Form\AdminSeo\SeoForm' => 'SeoProduct\Form\AdminSeo\SeoForm',
          
        ),
        'factories' => array(
            'SeoProduct\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory' => 'SeoProduct\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory',
             'SeoProduct\Form\AdminSeo\SeoFormFactory' => 'SeoProduct\Form\AdminSeo\SeoFormFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'SeoProduct\Controller\AdminSeoConfig' => 'SeoProduct\Controller\AdminSeoConfigController',
            'SeoProduct\Controller\AdminSeo' => 'SeoProduct\Controller\AdminSeoController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view'
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __NAMESPACE__ => __DIR__ . '/../public'
            )
        )
    )
);
