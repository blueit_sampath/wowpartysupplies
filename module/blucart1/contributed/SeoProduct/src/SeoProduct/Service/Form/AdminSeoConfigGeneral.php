<?php

namespace SeoProduct\Service\Form;

use Config\Service\Form\AdminConfig;

class AdminSeoConfigGeneral extends AdminConfig {

    protected $_columnKeys = array(
        'SEO_PRODUCT_TITLE',
        'SEO_PRODUCT_KEYWORDS',
        'SEO_PRODUCT_DESCRIPTION',
        'SEO_PRODUCT_ROBOTS',
        'SEO_PRODUCT_URL'
    );

    public function getFormName() {
        return 'adminSeoProductConfigGeneral';
    }

    public function getPriority() {
        return 1000;
    }

    public function save() {
        parent::save();
        $form = $this->getForm();
        $params = $form->getData();

        $forceBuild = false;
        if (isset($params['forceBuild'])) {
            $forceBuild = true;
        }
        \SeoProduct\Api\SeoProductApi::buildUrls($forceBuild);
    }

}
