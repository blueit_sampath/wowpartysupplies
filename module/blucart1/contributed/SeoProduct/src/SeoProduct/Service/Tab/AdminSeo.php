<?php

namespace SeoProduct\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminSeo extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$productId = Functions::fromRoute ( 'productId' );
		if (! $productId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-seo-product-add', array (
				'productId' => $productId 
		) );
		$tabContainer->add ( 'admin-seo-product-add', 'SEO', $u, 100 );
		
		return $this;
	}
}

