<?php

namespace SeoProduct\Api;

use Core\Functions;
use Common\Api\Api;
use \Config\Api\ConfigApi;

class SeoProductApi extends Api {

    protected static $_entity = '\Seo\Entity\Seo';

    public static function buildUrls($forceBuild) {

        $em = Functions::getEntityManager();
        if ($forceBuild) {
            $dql = 'delete from ' . static::$_entity . ' u where u.url like \'/product/%\'';
            $query = $em->createQuery($dql);
            $query->getResult();
        }
        $products = \Product\Api\ProductApi::getAllProducts(null);
        foreach ($products as $product) {
            $params = array('product' => $product);
            $url = static::getProductUrl($product->id);
            if (\Seo\Api\SeoApi::getByUrl($url)) {
                continue;
            }

            $pageTitle = '';

            if ($value = ConfigApi::getConfigByKey('SEO_PRODUCT_TITLE')) {
                $pageTitle = $value;
            }
            $pageTitle = static::substitute($pageTitle, $params);
            $array['pageTitle'] = $pageTitle;

            $keywords = '';

            if ($value = ConfigApi::getConfigByKey('SEO_PRODUCT_KEYWORDS')) {
                $keywords = $value;
            }

            $keywords = static::substitute($keywords, $params);
            $array['metaKeywords'] = $keywords;


            if ($value = ConfigApi::getConfigByKey('SEO_PRODUCT_DESCRIPTION')) {
                $description = $value;
            }
            $description = static::substitute($description, $params);
            $array['metaDescription'] = $description;

            $robots = '';

            if ($value = \Config\Api\ConfigApi::getConfigByKey('SEO_PRODUCT_ROBOTS')) {
                $robots = $value;
            }



            $robots = static::substitute($robots, $params);
            $array['robots'] = $robots;
            $alias = $product->title;

            if ($value = ConfigApi::getConfigByKey('SEO_PRODUCT_URL')) {
                $alias = $value;
            }

            $array['alias'] = static::substitute($alias, $params);
            $array['url'] = $url;
            \Seo\Api\SeoApi::insertSeoWithAlias($url, $array);
        }
    }

    public static function getProductUrl($productId){
        return '/product/'.$productId;
    }
    public static function substitute($string, $params) {
        $twigPlugin = Functions::getViewHelperManager()->get('twig');
        $string = $twigPlugin($string, $params);
        $string = str_replace('\{', '{', $string);
        $string = str_replace('\}', '}', $string);
        return $string;
    }

}
