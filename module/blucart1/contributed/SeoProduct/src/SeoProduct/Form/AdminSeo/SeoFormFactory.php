<?php
namespace SeoProduct\Form\AdminSeo;

use Common\Form\Option\AbstractFormFactory;

class SeoFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'SeoProduct\Form\AdminSeo\SeoForm' );
		$form->setName ( 'adminSeoProductAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'SeoProduct\Form\AdminSeo\SeoFilter' );
	}
}
