<?php

namespace SeoProduct\Form\AdminSeoConfigGeneral;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SeoConfigGeneralFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'SEO_PRODUCT_URL',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_PRODUCT_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_PRODUCT_DESCRIPTION',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_PRODUCT_KEYWORDS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_PRODUCT_ROBOTS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'forceBuild',
            'required' => false,
        ));

       
    }

}
