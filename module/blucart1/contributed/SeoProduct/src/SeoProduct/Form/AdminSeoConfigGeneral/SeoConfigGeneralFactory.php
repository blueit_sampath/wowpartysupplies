<?php
namespace SeoProduct\Form\AdminSeoConfigGeneral;

use Common\Form\Option\AbstractFormFactory;

class SeoConfigGeneralFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'SeoProduct\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm' );
		$form->setName ( 'adminSeoProductConfigGeneral' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'SeoProduct\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter' );
	}
}
