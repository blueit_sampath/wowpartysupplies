<?php

namespace SeoProduct\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminSeoController extends AbstractAdminController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('SeoProduct\Form\AdminSeo\SeoFormFactory');
    }

    public function getSaveRedirector() {
        return $this->redirect()->toRoute('admin-seo-product-add', array(
                    'productId' => \Core\Functions::fromRoute('productId')
        ));
    }

}
