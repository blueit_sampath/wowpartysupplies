<?php

namespace Testimonial\Controller;

use Common\MVC\Controller\AbstractAdminController;

use Core\Functions;

use Zend\View\Model\ViewModel;

class AdminTestimonialController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Testimonial\Form\AdminTestimonial\TestimonialFormFactory' );
	}
	
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-testimonial-add', array (
				'testimonialId' => $form->get ( 'id' )->getValue () 
		) );
	}
}