<?php 
namespace Testimonial\Service\Navigation;

use Admin\Navigation\Event\AdminNavigationEvent;
use Core\Functions;

class AdminTestimonial extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'websiteMain' );
	
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Testimonials',
							'route' => 'admin-testimonial',
							'id' => 'admin-testimonial',
							'iconClass' => 'glyphicon glyphicon-film' 
					) 
			) );
		}
	}
}

