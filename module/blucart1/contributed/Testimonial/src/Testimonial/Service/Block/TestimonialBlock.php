<?php 
namespace Testimonial\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class TestimonialBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'testimonial-block';

	public function getBlockName() {
		return 'testimonialBlock';
	}
	public function getPriority() {
		return 1000;
	}
	
}

