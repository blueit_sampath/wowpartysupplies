<?php

namespace Testimonial\Service\Form;

use File\Api\FileApi;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminTestimonial extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'testimonial',
			'weight',
			'status',
			'url' 
	);
	protected $_entity = '\Testimonial\Entity\Testimonial';
	protected $_entityName = 'testimonial';
	public function getFormName() {
		return 'adminTestimonialAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		FileApi::deleteFiles ( Functions::fromPost ( 'image_delete', '' ) );
		
		$image = $params ['image'];
		
		if ($image) {
			$array = explode ( ',', $image );
			foreach ( $array as $file ) {
				$imageEntity = FileApi::createOrUpdateFile ( $file, $params ['name'], $params ['name'] );
			}
		}
		
		$entity->file = $imageEntity;
	}
	
	public function afterGetRecord($entity){
		$form = $this->getForm();
		$form->get ( 'image' )->setValue ( $entity->file->path );
		
	}
	
}
