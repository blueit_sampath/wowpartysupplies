<?php

namespace Testimonial\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminTestimonial extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminTestimonial';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-testimonial-add' );
		$item = $linkContainer->add ( 'admin-testimonial-add', 'Add Testimonial', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-testimonial-sort' );
		$item = $linkContainer->add ( 'admin-testimonial-sort', 'Arrange Testimonials', $u, 990 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

