<?php

namespace Testimonial\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use File\Api\FileApi;

class AdminTestimonial extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'status',
			'weight' 
	);
	protected $_entity = '\Testimonial\Entity\Testimonial';
	protected $_entityName = 'testimonial';
	public function getEventName() {
		return 'adminTestimonial';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'testimonial.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'testimonial.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'filePath' );
		$columnItem->setTitle ( 'Image' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'file.path', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'testimonial.status', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Priority' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 950 );
		$columns->addColumn ( 'testimonial.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 990 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 980 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem ( 'testimonial.file', 'file' );
		$item->addJoin ( $joinItem );
		return true;
	}
	
	public function afterDestroy($params, $entity) {
		$em = $this->getEntityManager ();
		if ($entity->file && $entity->file->path) {
			FileApi::deleteFile ( $entity->file->path );
		}
		if ($entity->file) {
			$em->remove ( $entity->file );
		}
	}
}
