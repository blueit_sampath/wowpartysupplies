<?php

namespace Testimonial\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminTestimonial extends AbstractTabEvent {
	public function getEventName() {
		return 'adminTestimonialAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$testimonialId = Functions::fromRoute ( 'testimonialId' );
		if (! $testimonialId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-testimonial-add', array (
				'testimonialId' => $testimonialId 
		) );
		$tabContainer->add ( 'admin-testimonial-add', 'General', $u, 1000 );
		
		return $this;
	}
}

