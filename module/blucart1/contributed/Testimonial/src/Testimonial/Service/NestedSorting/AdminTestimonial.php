<?php

namespace Testimonial\Service\NestedSorting;

use Core\Functions;
use Testimonial\Api\TestimonialApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminTestimonial extends AbstractNestedSortingEvent {
	protected $_entityName = '\Testimonial\Entity\Testimonial';
	protected $_column = 'path';
	public function getEventName() {
		return 'adminTestimonialSort';
	}
	public function getPriority() {
		return 1000;
	}
	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (), array (
				'weight' => 'desc' 
		) );
		if ($results) {
			$column = $this->_column;
			foreach ( $results as $result ) {
				if ($result->file) {
					$array [$result->id] = array (
							'name' => $result->file->$column 
					);
				}
			}
		}
		return $array;
	}
}
