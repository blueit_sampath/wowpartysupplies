<?php

namespace Testimonial\Api;

use Core\Functions;
use Common\Api\Api;

class TestimonialApi extends Api {
	protected static $_entity = '\Testimonial\Entity\Testimonial';
	public static function getTestimonialById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getTestimonials($status = true, $page = 1, $perPage = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->setEventName ( 'TestimonialApi.getTestimonials' );
		$queryBuilder->addColumn ( 'testimonial.id', 'testimonialId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'testimonial.status', 'testimonialStatus' );
			$queryBuilder->addParameter ( 'testimonialStatus', $status );
		}
		$queryBuilder->addOrder ( 'testimonial.weight', 'testimonialWeight', 'desc' );
		$queryBuilder->addGroup ( 'testimonial.id', 'testimonialId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'testimonial' );
		return $queryBuilder->executeQuery ( ($page-1) * $perPage, $perPage );
	}
	public static function getSortedArray() {
		$array = static::getTestimonials ( null );
		return static::makeImageArray ( $array );
	}
	public static function makeImageArray($array) {
		$result = array ();
		foreach ( $array as $testimonialResults ) {
			$testimonialId = $testimonialResults ['testimonialId'];
			$testimonial = static::getTestimonialById ( $testimonialId );
			$result [$testimonial->id] = array (
					'name' => $testimonial->file->path 
			);
		}
		return $result;
	}
}
