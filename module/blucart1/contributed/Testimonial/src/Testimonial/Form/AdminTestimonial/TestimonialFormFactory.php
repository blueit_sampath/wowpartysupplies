<?php
namespace Testimonial\Form\AdminTestimonial;

use Common\Form\Option\AbstractFormFactory;

class TestimonialFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Testimonial\Form\AdminTestimonial\TestimonialForm' );
		$form->setName ( 'adminTestimonialAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Testimonial\Form\AdminTestimonial\TestimonialFilter' );
	}
}
