<?php

namespace Testimonial\Form\AdminTestimonial;

use Common\Form\Form;

class TestimonialForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'testimonial',
				'attributes' => array (
						'type' => 'textarea' 
				),
				'options' => array (
						'label' => 'Testimonial' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Priority' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'image',
				'type' => 'File\Form\Element\FileUpload' 
		), array (
				'priority' => 970 
		) );
		
		$this->getStatus ( 960 );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}