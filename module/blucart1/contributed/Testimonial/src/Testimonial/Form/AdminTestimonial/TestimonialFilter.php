<?php

namespace Testimonial\Form\AdminTestimonial;

use Zend\Validator\Regex;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TestimonialFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'name',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						),
						
						 
				) 
		) );
		$this->add ( array (
				'name' => 'image',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'testimonial',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				)
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		
		
		
		$this->add ( array (
				'name' => 'status',
				'required' => true,
				'validators' => array (
						
						array (
								'name' => 'Int' 
						) 
				) 
		) );
	}
} 