<?php
return array (
		'router' => array (
				'routes' => array (
						'admin-testimonial' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/testimonial',
										'defaults' => array (
												'controller' => 'Testimonial\Controller\AdminTestimonial',
												'action' => 'index' 
										) 
								) 
						),
						'admin-testimonial-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/testimonial/add[/ajax/:ajax][/:testimonialId]',
										'defaults' => array (
												'controller' => 'Testimonial\Controller\AdminTestimonial',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'testimonialId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-testimonial-sort' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/testimonial/sort[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'Testimonial\Controller\AdminTestimonial',
												'action' => 'sort',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'Testimonial\Service\Grid\AdminTestimonial' => 'Testimonial\Service\Grid\AdminTestimonial',
				'Testimonial\Service\Form\AdminTestimonial' => 'Testimonial\Service\Form\AdminTestimonial',
				'Testimonial\Service\Tab\AdminTestimonial' => 'Testimonial\Service\Tab\AdminTestimonial',
				'Testimonial\Service\Link\AdminTestimonial' => 'Testimonial\Service\Link\AdminTestimonial',
				'Testimonial\Service\NestedSorting\AdminTestimonial' => 'Testimonial\Service\NestedSorting\AdminTestimonial',
				'Testimonial\Service\Block\TestimonialBlock' => 'Testimonial\Service\Block\TestimonialBlock',
				'Testimonial\Service\Navigation\AdminTestimonial' => 'Testimonial\Service\Navigation\AdminTestimonial' 
		),
		'service_manager' => array (
				'invokables' => array (
						'Testimonial\Service\Grid\AdminTestimonial' => 'Testimonial\Service\Grid\AdminTestimonial',
						'Testimonial\Service\Form\AdminTestimonial' => 'Testimonial\Service\Form\AdminTestimonial',
						'Testimonial\Form\AdminTestimonial\TestimonialForm' => 'Testimonial\Form\AdminTestimonial\TestimonialForm',
						'Testimonial\Form\AdminTestimonial\TestimonialFilter' => 'Testimonial\Form\AdminTestimonial\TestimonialFilter',
						'Testimonial\Service\Tab\AdminTestimonial' => 'Testimonial\Service\Tab\AdminTestimonial',
						'Testimonial\Service\Link\AdminTestimonial' => 'Testimonial\Service\Link\AdminTestimonial',
						'Testimonial\Service\NestedSorting\AdminTestimonial' => 'Testimonial\Service\NestedSorting\AdminTestimonial',
						'Testimonial\Service\Block\TestimonialBlock' => 'Testimonial\Service\Block\TestimonialBlock',
						'Testimonial\Service\Navigation\AdminTestimonial' => 'Testimonial\Service\Navigation\AdminTestimonial' 
				),
				'factories' => array (
						'Testimonial\Form\AdminTestimonial\TestimonialFormFactory' => 'Testimonial\Form\AdminTestimonial\TestimonialFormFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'Testimonial\Controller\AdminTestimonial' => 'Testimonial\Controller\AdminTestimonialController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'Testimonial' => __DIR__.'/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						'Testimonial_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__.'/../src/Testimonial/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										'Testimonial\Entity' => 'Testimonial_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'Testimonial' => __DIR__.'/../public' 
						) 
				) 
		) 
);
