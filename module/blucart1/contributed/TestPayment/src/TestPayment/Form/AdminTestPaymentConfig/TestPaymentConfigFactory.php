<?php
namespace TestPayment\Form\AdminTestPaymentConfig;

use Common\Form\Option\AbstractFormFactory;

class TestPaymentConfigFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigForm');
        $form->setName('adminTestPaymentConfig');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigFilter');
    }
}
