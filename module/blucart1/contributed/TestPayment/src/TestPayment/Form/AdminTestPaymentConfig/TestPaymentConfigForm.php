<?php
namespace TestPayment\Form\AdminTestPaymentConfig;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class TestPaymentConfigForm extends Form

{

    public function init()
    {
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'PAYMENT_TEST_TITLE',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Test Payment Label'
            )
        ), array(
            'priority' => 1000
        ));
        
        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }
}