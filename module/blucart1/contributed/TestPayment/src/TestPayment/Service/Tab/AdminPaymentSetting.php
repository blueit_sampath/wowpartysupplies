<?php 
namespace TestPayment\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminPaymentSetting extends AbstractTabEvent {
	public function getEventName() {
		return 'adminPaymentSetting';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-testpayment-setting');
		$tabContainer->add ( 'admin-testpayment-setting', 'Test Payment', $u,500 );
		
		return $this;
	}
}

