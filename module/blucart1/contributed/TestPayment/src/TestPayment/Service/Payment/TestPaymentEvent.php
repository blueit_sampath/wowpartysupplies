<?php
namespace TestPayment\Service\Payment;

use Core\Functions;
use Checkout\Option\CartContainer;
use Payment\Option\AbstractPaymentEvent;
use Config\Api\ConfigApi;

class TestPaymentEvent extends AbstractPaymentEvent
{

    public function getName()
    {
        return 'testPayment';
    }

    public function getTitle()
    {
        $title = ConfigApi::getConfigByKey('PAYMENT_TEST_TITLE');
        if (! $title) {
            $title = 'Test Payment';
        }
        return $title;
    }

    public function getRouteName()
    {
        return 'testpayment-send';
    }
}
