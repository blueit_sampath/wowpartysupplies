<?php
namespace TestPayment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminTestPaymentSetting extends AdminConfig
{

    protected $_columnKeys = array(
        'PAYMENT_TEST_TITLE'
    )
    ;

    public function getFormName()
    {
        return 'adminTestPaymentConfig';
    }

    public function getPriority()
    {
        return 1000;
    }
}
