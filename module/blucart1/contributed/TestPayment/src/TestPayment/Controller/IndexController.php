<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace TestPayment\Controller;

use Core\Functions;
use Payment\Option\PaymentController;

class IndexController extends PaymentController
{

    const GATEWAY_NAME = 'testPayment';

    public function sendAction()
    {
        if (! $this->validatePath()) {
            return $this->redirect()->toRoute('checkout');
        }
        
        $orderId = Functions::fromRoute('orderId');
        
        return $this->redirect()->toRoute('testpayment-land', array(
            'orderId' => $orderId
        ));
    }

    public function landAction()
    {
        $orderId = Functions::fromRoute('orderId');
        $cart = $this->getCart();
        $viewModel = $this->complete($orderId);
        $cart->clearCart();
        return $viewModel;
    }
}


