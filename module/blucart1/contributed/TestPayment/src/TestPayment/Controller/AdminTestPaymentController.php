<?php
namespace TestPayment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminTestPaymentController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-testpayment-setting');
    }
}