<?php
return array(
    'router' => array(
        'routes' => array(
            'testpayment-send' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/testpayment/send[/:orderId]',
                    'defaults' => array(
                        'controller' => 'TestPayment\Controller\Index',
                        'action' => 'send'
                    )
                )
            ),
            'testpayment-land' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/testpayment/land[/:orderId]',
                    'defaults' => array(
                        'controller' => 'TestPayment\Controller\Index',
                        'action' => 'land'
                    )
                )
            ),
            'admin-testpayment-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/testpayment/setting[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'TestPayment\Controller\AdminTestPayment',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            )
        )
    ),
    'events' => array(
        'TestPayment\Service\Payment\TestPaymentEvent' => 'TestPayment\Service\Payment\TestPaymentEvent',
        'TestPayment\Service\Form\AdminTestPaymentSetting' => 'TestPayment\Service\Form\AdminTestPaymentSetting',
        'TestPayment\Service\Tab\AdminPaymentSetting' => 'TestPayment\Service\Tab\AdminPaymentSetting'
    ),
    'service_manager' => array(
        'invokables' => array(
            'TestPayment\Service\Payment\TestPaymentEvent' => 'TestPayment\Service\Payment\TestPaymentEvent',
            'TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigForm' => 'TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigForm',
            'TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigFilter' => 'TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigFilter',
            'TestPayment\Service\Form\AdminTestPaymentSetting' => 'TestPayment\Service\Form\AdminTestPaymentSetting',
            'TestPayment\Service\Tab\AdminPaymentSetting' => 'TestPayment\Service\Tab\AdminPaymentSetting'
        ),
        'factories' => array(
            'TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigFactory' => 'TestPayment\Form\AdminTestPaymentConfig\TestPaymentConfigFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'TestPayment\Controller\Index' => 'TestPayment\Controller\IndexController',
            'TestPayment\Controller\AdminTestPayment' => 'TestPayment\Controller\AdminTestPaymentController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'TestPayment' => __DIR__ . '/../view'
        )
    ),
   
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'TestPayment' => __DIR__ . '/../public'
            )
        )
    )
);
