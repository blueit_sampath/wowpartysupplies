<?php
return array(
    'router' => array(
        'routes' => array(
            'paypalpayment-send' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/paypalpayment/send[/:orderId]',
                    'defaults' => array(
                        'controller' => 'PaypalPayment\Controller\Index',
                        'action' => 'send'
                    )
                )
            ),
            'paypalpayment-land' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/paypalpayment/land[/:orderId]',
                    'defaults' => array(
                        'controller' => 'PaypalPayment\Controller\Index',
                        'action' => 'land'
                    )
                )
            ),
            'paypalpayment-error' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/paypalpayment/error',
                    'defaults' => array(
                        'controller' => 'PaypalPayment\Controller\Index',
                        'action' => 'error'
                    )
                )
            ),
             'paypalpayment-ipn' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/paypalpayment/ipn',
                    'defaults' => array(
                        'controller' => 'PaypalPayment\Controller\Index',
                        'action' => 'ipn'
                    )
                )
            ),
            'admin-paypalpayment-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/paypalpayment/setting[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'PaypalPayment\Controller\AdminPaypalPayment',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            )
        )
    ),
    'events' => array(
        'PaypalPayment\Service\Payment\PaypalPaymentEvent' => 'PaypalPayment\Service\Payment\PaypalPaymentEvent',
        'PaypalPayment\Service\Form\AdminPaypalPaymentSetting' => 'PaypalPayment\Service\Form\AdminPaypalPaymentSetting',
        'PaypalPayment\Service\Tab\AdminPaymentSetting' => 'PaypalPayment\Service\Tab\AdminPaymentSetting'
    ),
    'service_manager' => array(
        'invokables' => array(
            'PaypalPayment\Service\Payment\PaypalPaymentEvent' => 'PaypalPayment\Service\Payment\PaypalPaymentEvent',
            'PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigForm' => 'PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigForm',
            'PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigFilter' => 'PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigFilter',
            'PaypalPayment\Service\Form\AdminPaypalPaymentSetting' => 'PaypalPayment\Service\Form\AdminPaypalPaymentSetting',
            'PaypalPayment\Service\Tab\AdminPaymentSetting' => 'PaypalPayment\Service\Tab\AdminPaymentSetting'
        ),
        'factories' => array(
            'PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigFactory' => 'PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'PaypalPayment\Controller\Index' => 'PaypalPayment\Controller\IndexController',
            'PaypalPayment\Controller\AdminPaypalPayment' => 'PaypalPayment\Controller\AdminPaypalPaymentController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'PaypalPayment' => __DIR__ . '/../view'
        )
    ),
    
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'PaypalPayment' => __DIR__ . '/../public'
            )
        )
    )
);
