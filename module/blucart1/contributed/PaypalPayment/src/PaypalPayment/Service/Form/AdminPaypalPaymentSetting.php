<?php

namespace PaypalPayment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminPaypalPaymentSetting extends AdminConfig {

    protected $_columnKeys = array(
        'PAYMENT_PAYPAL_TITLE',
        'PAYMENT_PAYPAL_BUSINESS',
        'PAYMENT_PAYPAL_ENVIRONMENT',
        'PAYMENT_PAYPAL_CANCEL_URL',
    );

    public function getFormName() {
        return 'adminPaypalPaymentConfig';
    }

    public function getPriority() {
        return 1000;
    }

}
