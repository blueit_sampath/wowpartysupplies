<?php
namespace PaypalPayment\Service\Payment;

use Core\Functions;
use Checkout\Option\CartContainer;
use Payment\Option\AbstractPaymentEvent;
use Config\Api\ConfigApi;

class PaypalPaymentEvent extends AbstractPaymentEvent
{

    public function getName()
    {
        return 'paypalPayment';
    }

    public function getTitle()
    {
        $title = ConfigApi::getConfigByKey('PAYMENT_PAYPAL_TITLE');
        if (! $title) {
            $title = 'Paypal';
        }
        return $title;
    }

    public function getRouteName()
    {
        return 'paypalpayment-send';
    }
}
