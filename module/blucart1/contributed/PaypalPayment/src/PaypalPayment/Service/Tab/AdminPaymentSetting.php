<?php
namespace PaypalPayment\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminPaymentSetting extends AbstractTabEvent
{

    public function getEventName()
    {
        return 'adminPaymentSetting';
    }

    public function tab()
    {
        $tabContainer = $this->getTabContainer();
        $url = Functions::getUrlPlugin();
        
        $u = $url('admin-paypalpayment-setting');
        $tabContainer->add('admin-paypalpayment-setting', 'Paypal Payment', $u, 500);
        
        return $this;
    }
}

