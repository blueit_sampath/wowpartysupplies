<?php

namespace PaypalPayment\Controller;

use Core\Functions;
use Payment\Option\PaymentController;
use Zend\View\Model\ViewModel;
use Config\Api\ConfigApi;

class IndexController extends PaymentController {

    const GATEWAY_NAME = 'paypalPayment';

    public function sendAction() {
        if (!$this->validatePath()) {
            return $this->redirect()->toRoute('checkout');
        }
        $orderId = Functions::fromRoute('orderId');
        $viewModel = new ViewModel(array(
            'orderId' => $orderId
        ));
        return $viewModel;
    }

    public function landAction() {
        $cart = $this->getCart();
        $orderId = Functions::fromRoute('orderId', 0);
        if(!$orderId){
            return $this->redirect()->toRoute('home');
        }
        $orderPayment = \Payment\Api\PaymentApi::getPaymentOrderByOrderId($orderId);
        if (!($orderPayment && $orderPayment->paymentRefNo)) {
            $viewModel = $this->pending($orderId, $Message);
            return $viewModel;
        }
        $viewModel = new ViewModel(array('orderId' =>$orderId));
        $viewModel->setTemplate('payment/payment/complete');
        return $viewModel;
    }

    public function ipnAction() {
        $env = ConfigApi::getConfigByKey('PAYMENT_PAYPAL_ENVIRONMENT', 'LIVE');
        $certificate = ConfigApi::getConfigByKey('PAYMENT_PAYPAL_CERTIFICATE');
        $business = ConfigApi::getConfigByKey('PAYMENT_PAYPAL_BUSINESS');
        ini_set('log_errors', true);
        ini_set('error_log', 'data/paypal_ipn_errors.log');
        $ipnListener = new \PaypalPayment\Option\IpnListener();
 
        if ($env != 'LIVE') {
            $ipnListener->use_sandbox = true;
        }

        if ($certificate) {
            $ipnListener->setCertificate($certificate);
        }

        try {
            $verified = $ipnListener->processIpn();
        } catch (Exception $e) {
            error_log($e->getMessage());
            exit(0);
        }

        if ($verified) {

            $orderId = $_POST['custom'] ? $_POST['custom'] : 0;

            $order = \OrderMain\Api\OrderApi::getOrderById($orderId);
            if (!$order) {
                return false;
            }
            $errmsg = '';
            // stores errors from fraud checks
            // 1. Make sure the payment status is "Completed" 
            if ($_POST['payment_status'] != 'Completed') {
                exit(0);
            }

           
  if (trim($_POST['business']) != trim($business)) {
                $errmsg .= "'receiver_email' does not match: ";
                $errmsg .= $_POST['business'] . "\n";
            }

            // 3. Make sure the amount(s) paid match
            if ($_POST['mc_gross'] != $order->amount) {
                $errmsg .= "'mc_gross' does not match: ";
                $errmsg .= $_POST['mc_gross'] . "\n";
            }


            // 4. Make sure the currency code matches
            if ($_POST['mc_currency'] != ConfigApi::getCurrencyCode()) {
                $errmsg .= "'mc_currency' does not match: ";
                $errmsg .= $_POST['mc_currency'] . "\n";
            }
            // TODO: Check for duplicate txn_id

            if (!empty($errmsg)) {

                // manually investigate errors from the fraud checking
                $body = "IPN failed fraud checks: \n$errmsg\n\n";
                $body .= $ipnListener->getTextReport();
                \OrderMain\Api\OrderApi::saveLog($orderId, $body);

            } else {
                $transactionId = $_POST['txn_id'];
                $this->complete($orderId, $transactionId);

            }
        } else {
            error_log('Not Verified Request ' . $ipnListener->getTextReport());
            exit(0);
        }
        exit;
    }

}
