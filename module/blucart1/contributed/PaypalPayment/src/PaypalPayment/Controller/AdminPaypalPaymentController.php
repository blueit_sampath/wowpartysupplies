<?php
namespace PaypalPayment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminPaypalPaymentController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-paypalpayment-setting');
    }
}