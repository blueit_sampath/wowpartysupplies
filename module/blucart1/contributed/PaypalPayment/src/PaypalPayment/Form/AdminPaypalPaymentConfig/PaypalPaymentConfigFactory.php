<?php
namespace PaypalPayment\Form\AdminPaypalPaymentConfig;

use Common\Form\Option\AbstractFormFactory;

class PaypalPaymentConfigFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigForm');
        $form->setName('adminPaypalPaymentConfig');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('PaypalPayment\Form\AdminPaypalPaymentConfig\PaypalPaymentConfigFilter');
    }
}
