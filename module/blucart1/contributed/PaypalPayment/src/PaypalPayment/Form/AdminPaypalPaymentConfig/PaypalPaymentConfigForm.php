<?php

namespace PaypalPayment\Form\AdminPaypalPaymentConfig;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class PaypalPaymentConfigForm extends Form {

    public function init() {

        $this->add(array(
            'name' => 'PAYMENT_PAYPAL_TITLE',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Paypal Payment Label'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYPAL_BUSINESS',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Business Email'
            )
                ), array(
            'priority' => 990
        ));

        $select = new Select('PAYMENT_PAYPAL_ENVIRONMENT');
        $select->setValueOptions(array(
            'LIVE' => 'LIVE',
            'SANDBOX' => 'SANDBOX',
           
        ));
        $select->setLabel('Paypal Environment');
        $this->add($select, array(
            'priority' => 980
        ));
         $this->add(array(
            'name' => 'PAYMENT_PAYPAL_CANCEL_URL',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Paypal Cancel Url'
            )
                ), array(
            'priority' => 970
        ));

          $this->add(array(
            'name' => 'PAYMENT_PAYPAL_CERTIFICATE',
            'attributes' => array(
                'type' => 'textarea'
            ),
            'options' => array(
                'label' => 'Paypal Certificate'
            )
                ), array(
            'priority' => 960
        ));

        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
