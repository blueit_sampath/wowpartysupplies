<?php

namespace PaypalPayment\Form\AdminPaypalPaymentConfig;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PaypalPaymentConfigFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'PAYMENT_PAYPAL_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYPAL_BUSINESS',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYPAL_ENVIRONMENT',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
                , array(
                    'name' => 'StripTags'
                )
            )
        ));
        
         $this->add(array(
            'name' => 'PAYMENT_PAYPAL_CANCEL_URL',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYPAL_CERTIFICATE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

       
    }

}
