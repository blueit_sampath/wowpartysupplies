<?php

namespace ImportCategory\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ImportCategoryNavigation extends \Admin\Navigation\Event\AdminNavigationEvent {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMainImportMain');
       
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Import Categories',
                    'route' => 'admin-import',
                    'params' => array(
                        'eventName' => 'adminCategoryImport' 
                    ),
                    'id' => 'admin-import-product',
                    'iconClass' => 'glyphicon-briefcase'
                )
            ));
        }
    }

}
