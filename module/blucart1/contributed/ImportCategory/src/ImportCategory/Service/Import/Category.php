<?php

namespace ImportCategory\Service\Import;

use Import\Option\AbstractImportEvent;
use Category\Api\CategoryApi;
use Core\Functions;

class Category extends AbstractImportEvent {

    public function getEventName() {
        return 'adminCategoryImport';
    }

    public function getPriority() {
        return 1000;
    }

    public function loadSchema() {
        $importContainer = $this->getImportContainer();
        $importContainer->setTitle('Import Categories');
        $importContainer->addColumn('title', 'Title', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 1,
                        'max' => 255
                    )
                )
            )
                ), 1000);

        $importContainer->addColumn('name', 'Name', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 990);

        $importContainer->addColumn('shortDescription', 'Short Description', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 980);

        $importContainer->addColumn('description', 'Description', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 970);

        $importContainer->addColumn('parentCategory', 'Parent Category', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 960);

        $importContainer->addColumn('weight', 'Priority', array(
            'name' => 'weight',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
                ), 950);

        $importContainer->addColumn('status', 'Status', array(
            'name' => 'status',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'Int'
                )
            )
                ), 940);

        $importContainer->addColumn('resetImages', 'Images(Replace Existing)', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 930);
        $importContainer->addColumn('addImages', 'Images(Add To List)', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 920);
    }

    public function isValid() {
        $importContainer = $this->getImportContainer();
        $inputFilter = $importContainer->getInputFilter();
        $data = $importContainer->getRawData();

        if (isset($data['name'])) {
            $name = $data['name'];
            $name = $this->createSlug($name);
            if ($name) {
                $category = CategoryApi::getCategoryByName($name);
                if ($category) {
                    $inputFilter->get('title')->setRequired(false);
                }
            }
        }
    }

    public function save() {
        $importContainer = $this->getImportContainer();
        $data = $importContainer->getData();
        $columns = array(
            'name',
            'title',
            'shortDescription',
            'description',
            'weight',
            'status'
        );

        $name = $data['name'];
        $name = $this->createSlug($name);
        if ($name && ($category = CategoryApi::getCategoryByName($name))) {
            
        } else {
            $category = new \Category\Entity\Category();
            $category->status = 1;
            $importContainer->setIsNewRecord(true);
        }
        foreach ($columns as $column) {
            if (isset($data[$column])) {
                $category->$column = $data[$column];
            }
            
        }
        $category->name = $name;

        if (isset($data['parentCategory'])) {
            if ($data['parentCategory']) {
                $parentCategoryName = $this->createSlug($data['parentCategory']);
                $parent = CategoryApi::getCategoryByName($parentCategoryName);
                if ($parent) {
                    $category->parent = $parent;
                }
            } else {
                $category->parent = null;
            }
        }

        $em = $this->getEntityManager();
        $em->persist($category);
        $em->flush();
        $importContainer->addEntity('category', $category);

        $this->saveCategoryImages($data, $category);

        if (Functions::hasModule('MenuCategory')) {
            \MenuCategory\Api\MenuCategoryApi::syncAllMenus($category->id, true);
        }
        Functions::addSuccessMessage('Saved Successfully');
    }

    public function saveCategoryImages($data, $category) {

        if (isset($data['resetImages'])) {
            $em = $this->getEntityManager();
            $categoryImages = CategoryApi::getCategoryImageByCategoryId($category->id, null);
            foreach ($categoryImages as $categoryImageArray) {
                $categoryImage = CategoryApi::getCategoryImageById($categoryImageArray['categoryImageId']);
                \File\Api\FileApi::deleteFile($categoryImage->file->path);
                $em->remove($categoryImage);
            }
            $em->flush();

            $this->saveImages($data['resetImages'], $category);
        }


        if (isset($data['addImages'])) {
            $this->saveImages($data['addImages'], $category);
        }
    }

    public function saveImages($images, $category) {
        $em = $this->getEntityManager();
        $images = explode(';', $images);
        foreach ($images as $image) {
            $image = trim($image);
            $file = $this->saveImage($image, $category->title, $category->title, $category->title);
            if ($file) {
                $categoryImage = new \Category\Entity\CategoryImage();
                $categoryImage->file = $file;
                $categoryImage->category = $category;
                $categoryImage->status = 1;

                $em->persist($categoryImage);
            }
        }
        $em->flush();
    }

}
