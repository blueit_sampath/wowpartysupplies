<?php

return array(
    'events' => array(
        'ImportCategory\Service\Import\Category' => 'ImportCategory\Service\Import\Category',
        'ImportCategory\Service\Navigation\ImportCategoryNavigation' => 'ImportCategory\Service\Navigation\ImportCategoryNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'ImportCategory\Service\Import\Category' => 'ImportCategory\Service\Import\Category',
            'ImportCategory\Service\Navigation\ImportCategoryNavigation' => 'ImportCategory\Service\Navigation\ImportCategoryNavigation'
        ),
        'factories' => array()
    ),
    'controllers' => array('invokables' => array()),
    'view_manager' => array('template_path_stack' => array('ImportCategory' => __DIR__ . '/../view')),
    'asset_manager' => array('resolver_configs' => array('paths' => array('ImportCategory' => __DIR__ . '/../public')))
);
