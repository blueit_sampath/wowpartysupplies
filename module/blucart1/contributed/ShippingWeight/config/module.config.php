<?php

namespace ShippingWeight;

return array (
		'router' => array (
				'routes' => array (
						'admin-shipping-weight' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/weight',
										'defaults' => array (
												'controller' => 'ShippingWeight\Controller\AdminShippingWeight',
												'action' => 'index' 
										) 
								) 
						),
						'admin-shipping-weight-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/weight/add[/ajax/:ajax][/:shippingWeightId]',
										'defaults' => array (
												'controller' => 'ShippingWeight\Controller\AdminShippingWeight',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'shippingWeightId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-shipping-weight-country' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/weight/country/:shippingWeightId[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'ShippingWeight\Controller\AdminShippingWeightCountry',
												'action' => 'index',
												'ajax' => 'true' 
										) 
								) 
						),
						'admin-shipping-weight-country-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/weight/country/add[/ajax/:ajax][/:shippingWeightId][/:shippingWeightCountryId]',
										'defaults' => array (
												'controller' => 'ShippingWeight\Controller\AdminShippingWeightCountry',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'shippingWeightId' => '[0-9]+',
												'shippingWeightCountryId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'ShippingWeight\Service\Grid\AdminShippingWeight' => 'ShippingWeight\Service\Grid\AdminShippingWeight',
				'ShippingWeight\Service\Link\AdminShippingWeight' => 'ShippingWeight\Service\Link\AdminShippingWeight',
				'ShippingWeight\Service\Tab\AdminShippingWeight' => 'ShippingWeight\Service\Tab\AdminShippingWeight',
				'ShippingWeight\Service\Form\AdminShippingEvent' => 'ShippingWeight\Service\Form\AdminShippingEvent',
				'ShippingWeight\Service\Grid\AdminShippingWeightCountry' => 'ShippingWeight\Service\Grid\AdminShippingWeightCountry',
				'ShippingWeight\Service\Form\AdminShippingWeightCountryForm' => 'ShippingWeight\Service\Form\AdminShippingWeightCountryForm',
				'ShippingWeight\Service\Link\AdminShippingWeightCountry' => 'ShippingWeight\Service\Link\AdminShippingWeightCountry',
				'ShippingWeight\Service\Navigation\AdminShippingWeightNavigation' => 'ShippingWeight\Service\Navigation\AdminShippingWeightNavigation',
				'ShippingWeight\Service\Shipping\ShippingWeightEvent' => 'ShippingWeight\Service\Shipping\ShippingWeightEvent',
		),
		'service_manager' => array (
				'invokables' => array (
						'ShippingWeight\Service\Grid\AdminShippingWeight' => 'ShippingWeight\Service\Grid\AdminShippingWeight',
						'ShippingWeight\Service\Link\AdminShippingWeight' => 'ShippingWeight\Service\Link\AdminShippingWeight',
						'ShippingWeight\Service\Tab\AdminShippingWeight' => 'ShippingWeight\Service\Tab\AdminShippingWeight',
						'ShippingWeight\Form\AdminShippingWeight\ShippingWeightForm' => 'ShippingWeight\Form\AdminShippingWeight\ShippingWeightForm',
						'ShippingWeight\Form\AdminShippingWeight\ShippingWeightFilter' => 'ShippingWeight\Form\AdminShippingWeight\ShippingWeightFilter',
						'ShippingWeight\Service\Form\AdminShippingEvent' => 'ShippingWeight\Service\Form\AdminShippingEvent',
						'ShippingWeight\Service\Grid\AdminShippingWeightCountry' => 'ShippingWeight\Service\Grid\AdminShippingWeightCountry',
						'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryForm' => 'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryForm',
						'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryFilter' => 'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryFilter',
						'ShippingWeight\Service\Form\AdminShippingWeightCountryForm' => 'ShippingWeight\Service\Form\AdminShippingWeightCountryForm',
						'ShippingWeight\Service\Link\AdminShippingWeightCountry' => 'ShippingWeight\Service\Link\AdminShippingWeightCountry',
						'ShippingWeight\Service\Navigation\AdminShippingWeightNavigation' => 'ShippingWeight\Service\Navigation\AdminShippingWeightNavigation',
						'ShippingWeight\Service\Shipping\ShippingWeightEvent' => 'ShippingWeight\Service\Shipping\ShippingWeightEvent',
				),
				'factories' => array (
						'ShippingWeight\Form\AdminShippingWeight\ShippingWeightFactory' => 'ShippingWeight\Form\AdminShippingWeight\ShippingWeightFactory',
						'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryFactory' => 'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'ShippingWeight\Controller\AdminShippingWeight' => 'ShippingWeight\Controller\AdminShippingWeightController',
						'ShippingWeight\Controller\AdminShippingWeightCountry' => 'ShippingWeight\Controller\AdminShippingWeightCountryController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						__NAMESPACE__ => __DIR__ . '/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						__NAMESPACE__ . '_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								__NAMESPACE__ => __DIR__ . '/../public' 
						) 
				) 
		) 
);
