<?php

namespace ShippingWeight\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminShippingWeightCountryController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		
		return $this->redirect ()->toRoute ( 'admin-shipping-weight-country-add', array (
				'shippingWeightId' => $form->get ( 'shippingWeight' )->getValue (),
				'shippingWeightCountryId' => $form->get ( 'id' )->getValue () 
		) );
	}
}