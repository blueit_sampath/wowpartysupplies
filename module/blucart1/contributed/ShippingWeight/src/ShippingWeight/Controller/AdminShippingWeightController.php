<?php 
namespace ShippingWeight\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminShippingWeightController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'ShippingWeight\Form\AdminShippingWeight\ShippingWeightFactory' );
	}
	
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		
		return $this->redirect ()->toRoute ( 'admin-shipping-weight-add', array (
				'shippingWeightId' => $form->get ( 'id' )->getValue ()
		) );
	}
	
	
}