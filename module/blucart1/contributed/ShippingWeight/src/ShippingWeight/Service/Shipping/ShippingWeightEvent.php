<?php

namespace ShippingWeight\Service\Shipping;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Shipping\Option\AbstractShippingEvent;

class ShippingWeightEvent extends AbstractShippingEvent {
	public function getName(){
		return 'shippingWeight';
	}
	public function getTitle(){
		return 'Shipping By Weight';
	}
	
}
