<?php

namespace ShippingWeight\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminShippingEvent extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'title',
			'minWeight',
			'maxWeight',
			'status' 
	);
	protected $_entity = '\ShippingWeight\Entity\ShippingWeight';
	protected $_entityName = 'shippingWeight';
	public function getFormName() {
		return 'adminShippingWeightAdd';
	}
	public function getPriority() {
		return 1000;
	}
	
	
}
