<?php

namespace ShippingWeight\Service\Form;

use ShippingWeight\Api\ShippingWeightApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminShippingWeightCountryForm extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'country',
			'amount' 
	);
	protected $_entity = 'ShippingWeight\Entity\ShippingWeightCountry';
	protected $_entityName = 'shippingWeightCountry';
	public function getFormName() {
		return 'adminShippingWeightCountryAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		if ($params ['shippingWeight']) {
			$entity->shippingWeight = ShippingWeightApi::getShippingWeightById ( $params ['shippingWeight'] );
		}
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$form->get ( 'shippingWeight' )->setValue ( Functions::fromRoute ( 'shippingWeightId', 0 ) );
	}
}
