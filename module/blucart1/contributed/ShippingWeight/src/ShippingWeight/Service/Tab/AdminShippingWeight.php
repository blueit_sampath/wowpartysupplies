<?php

namespace ShippingWeight\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminShippingWeight extends AbstractTabEvent {
	public function getEventName() {
		return 'adminShippingWeightAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$shippingWeightId = Functions::fromRoute ( 'shippingWeightId', null );
		if (! $shippingWeightId) {
			return;
		}
		$u = $url ( 'admin-shipping-weight-add', array (
				'shippingWeightId' => $shippingWeightId 
		) );
		$tabContainer->add ( 'admin-shipping-weight-add', 'General', $u, 1000 );
		
		$u = $url ( 'admin-shipping-weight-country', array (
				'shippingWeightId' => $shippingWeightId 
		) );
		$tabContainer->add ( 'admin-shipping-weight-country', 'Countries', $u, 1000 );
		
		return $this;
	}
}

