<?php

namespace ShippingWeight\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminShippingWeightCountry extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminShippingWeightCountry';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$shippingWeightId = Functions::fromRoute ( 'shippingWeightId', null );
		if (! $shippingWeightId) {
			return;
		}
		$u = $url ( 'admin-shipping-weight-country-add', array (
				'shippingWeightId' => $shippingWeightId 
		) );
		$item = $linkContainer->add ( 'admin-shipping-weight-country-add', 'Add Country', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

