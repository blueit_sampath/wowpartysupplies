<?php 
namespace ShippingWeight\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminShippingWeight extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminShippingWeight';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-shipping-weight-add');
		$item = $linkContainer->add ( 'admin-shipping-weight-add', 'Add New Shipping Weight', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

