<?php 
namespace ShippingWeight\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminShippingWeightNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'orderMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Shipping By Weight',
							'route' => 'admin-shipping-weight',
							'id' => 'admin-shipping-weight',
							'iconClass' => 'glyphicon glyphicon-road' 
					) 
			) );
		}
	}
}

