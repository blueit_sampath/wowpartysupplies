<?php

namespace ShippingWeight\Service\Grid;

use Config\Api\ConfigApi;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminShippingWeight extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'title',
			'minWeight',
			'maxWeight',
			'status' 
	);
	protected $_entity = '\ShippingWeight\Entity\ShippingWeight';
	protected $_entityName = 'shippingWeight';
	protected $_detailTemplateName = 'adminShippingWeightCountry';
	protected $_detailTemplate = 'common/admin-shipping-weight-country';
	public function getEventName() {
		return 'adminShippingWeight';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		$array = array ();
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'shippingWeight.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'shippingWeight.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'shippingWeight.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'minWeight' );
		$columnItem->setTitle ( 'Minimum Weight' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columnItem->setFormat ( '{0} ' . ConfigApi::getWeightSymbol () );
		$columns->addColumn ( 'shippingWeight.minWeight', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'maxWeight' );
		$columnItem->setTitle ( 'Maximum Weight' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columnItem->setFormat ( '{0} ' . ConfigApi::getWeightSymbol () );
		$columns->addColumn ( 'shippingWeight.maxWeight', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'shippingWeight.status', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
}
