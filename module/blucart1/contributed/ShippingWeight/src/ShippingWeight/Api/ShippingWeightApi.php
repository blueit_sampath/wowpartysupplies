<?php

namespace ShippingWeight\Api;

use Common\Api\Api;

class ShippingWeightApi extends Api {
	protected static $_entity = '\ShippingWeight\Entity\ShippingWeight';
	protected static $_entityCountry = '\ShippingWeight\Entity\ShippingWeightCountry';
	public static function getShippingWeightById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getShippingWeightCountryById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entityCountry, $id );
	}
	public static function getShippingWeightCountryByCountryAndShippingWeightId($country,$shippingWeightId) {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entityCountry )->findOneBy ( array (
				'country' => $country,
				'shippingWeight' => $shippingWeightId
		) );
	}
	public static function getShippingWeights() {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'shippingWeight.id', 'shippingWeightId' );
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'shippingWeight' );
		return $queryBuilder->executeQuery ();
	}
	
	public static function getShippingWeightCountriesByShippingWeightId($shippingWeight) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'shippingWeightCountry.id', 'shippingWeightCountryId' );
		$item = $queryBuilder->addFrom ( static::$_entityCountry, 'shippingWeightCountry' );
		$item = $queryBuilder->addWhere ( 'shippingWeight.id', 'shippingWeightId' );
		$queryBuilder->addParameter ( 'shippingWeightId', $shippingWeight );
		return $queryBuilder->executeQuery ();
	}
}
