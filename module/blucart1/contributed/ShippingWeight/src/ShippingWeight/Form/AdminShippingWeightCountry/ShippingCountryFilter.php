<?php

namespace ShippingWeight\Form\AdminShippingWeightCountry;

use ShippingWeight\Api\ShippingWeightApi;
use Zend\Validator\Callback;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ShippingCountryFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'country',
				'required' => true,
				'validators' => array (
						array (
								'name' => 'Callback',
								'options' => array (
										'callback' => array (
												$this,
												'checkCountry' 
										),
										'messages' => array (
												Callback::INVALID_VALUE => "Country already exists" 
										) 
								) 
						) 
				) 
		)
		 );
		
		$this->add ( array (
				'name' => 'amount',
				'required' => true,
				'validators' => array (
						
						array (
								'name' => 'Float' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'shippingWeight',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
	}
	public function checkCountry($value, $context) {
		$entity = ShippingWeightApi::getShippingWeightCountryByCountryAndShippingWeightId ( $value, $context ['shippingWeight'] );
		if ($entity) {
			if ($entity->id == $context ['id']) {
				return true;
			}
			return false;
		}
		return true;
	}
} 