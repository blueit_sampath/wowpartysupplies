<?php

namespace ShippingWeight\Form\AdminShippingWeightCountry;

use Config\Api\ConfigApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class ShippingCountryForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'shippingWeight',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'country' );
		$select->setLabel ( 'Country' );
		$select->setValueOptions ( $this->getCountries () );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'amount',
				'attributes' => array (
						'type' => 'text' 
				)
				,
				'options' => array (
						'label' => 'Amount',
						'prependText' => ConfigApi::getCurrencySymbol () 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCountries() {
		$array = array (
				'all' => 'All' 
		);
		$countries = ConfigApi::getCountries ();
		foreach ( $countries as $country ) {
			$array [$country->name] = $country->name;
		}
		return $array;
	}
}