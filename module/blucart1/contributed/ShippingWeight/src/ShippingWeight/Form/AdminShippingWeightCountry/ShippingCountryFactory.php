<?php
namespace ShippingWeight\Form\AdminShippingWeightCountry;

use Common\Form\Option\AbstractFormFactory;

class ShippingCountryFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryForm' );
		$form->setName ( 'adminShippingWeightCountryAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ShippingWeight\Form\AdminShippingWeightCountry\ShippingCountryFilter' );
	}
}
