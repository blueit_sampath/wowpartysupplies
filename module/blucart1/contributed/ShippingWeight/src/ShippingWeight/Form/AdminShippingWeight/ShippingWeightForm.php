<?php

namespace ShippingWeight\Form\AdminShippingWeight;

use Config\Api\ConfigApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class ShippingWeightForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'title',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Title' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'minWeight',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Minimum Weight',
						'appendText' => ConfigApi::getWeightSymbol () 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'maxWeight',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Maximum Weight',
						'appendText' => ConfigApi::getWeightSymbol () 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->getStatus ( 960 );
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}