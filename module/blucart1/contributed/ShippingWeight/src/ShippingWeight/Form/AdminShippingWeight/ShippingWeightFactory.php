<?php
namespace ShippingWeight\Form\AdminShippingWeight;

use Common\Form\Option\AbstractFormFactory;

class ShippingWeightFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ShippingWeight\Form\AdminShippingWeight\ShippingWeightForm' );
		$form->setName ( 'adminShippingWeightAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ShippingWeight\Form\AdminShippingWeight\ShippingWeightFilter' );
	}
}
