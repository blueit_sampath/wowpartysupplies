<?php

namespace BrandProduct\Controller;

use Common\MVC\Controller\AbstractAdminController;


class AdminBrandProductController extends AbstractAdminController{
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'BrandProduct\Form\AdminBrandProduct\BrandProductFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-brand-product-add', array (
						'productId' => $form->get ( 'product' )->getValue () 
				) );
	}
	
}