<?php

namespace BrandProduct\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminBrandProduct extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		if (! $productId) {
			return;
		}
		$u = $url ( 'admin-brand-product', array (
				'productId' => $productId 
		) );
		
		$tabContainer->add ( 'admin-brand-product', 'Brands', $u, 985 );
		
		return $this;
	}
}

