<?php 
namespace BrandProduct\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class ProductBrand extends AbstractContentEvent {

	protected $_contentTemplate = 'common/product-brand-quick-information';
	protected $_contentName = 'productBrandQuickInformation';
	public function getEventName() {
		return 'productQuickInformation';
	}
	public function getPriority() {
		return ;
	}
	
}

