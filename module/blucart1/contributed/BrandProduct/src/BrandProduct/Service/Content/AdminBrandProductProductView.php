<?php 
namespace BrandProduct\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminBrandProductProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-brand-product-product-view';
	protected $_contentName = 'adminBrandProductProductView';
	public function getEventName() {
		return 'adminProductViewTop';
	}
	public function getPriority() {
		return 600;
	}
	
}

