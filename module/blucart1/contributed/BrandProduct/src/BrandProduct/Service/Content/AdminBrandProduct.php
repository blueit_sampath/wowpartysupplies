<?php 
namespace BrandProduct\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminBrandProduct extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-brand-product';
	protected $_contentName = 'adminBrandProduct';
	public function getEventName() {
		return 'adminBrandView';
	}
	public function getPriority() {
		return 1000;
	}
	
}

