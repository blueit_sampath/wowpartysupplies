<?php 
namespace BrandProduct\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminBrandProduct extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminBrandProduct';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		if(!$productId){
			return;
		}
		$u = $url ( 'admin-brand-product-add', array (
				'productId' => $productId
		) );
		$item = $linkContainer->add ( 'admin-brand-product-add', 'Add Brand Products', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

