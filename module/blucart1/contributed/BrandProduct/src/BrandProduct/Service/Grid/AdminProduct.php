<?php

namespace BrandProduct\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractBlucartGridEvent;

class AdminProduct extends AbstractBlucartGridEvent {
	protected $_entity = 'BrandProduct\Entity\BrandProduct';
	protected $_entityName = 'BrandProduct';
	public function getEventName() {
		return 'adminProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'brands' );
		$columnItem->setTitle ( 'Brands' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 995 );
		$columnItem->setIsHaving ( true );
		$columnItem->setBelongsToEntity ( 'brand' );
		$columns->addColumn ( 'GroupConcat(Distinct brand.title)', $columnItem );
		
		return $columns;
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		
		$productFrom = $queryBuilder->getFrom ( 'product' );
		$joinItem = new QueryJoinItem ( '\BrandProduct\Entity\BrandProduct', 'brandProduct', 'left join', 'product.id = brandProduct.product' );
		$joinItem2 = new QueryJoinItem ( 'brandProduct.brand', 'brand' );
		$joinItem->addJoin ( $joinItem2 );
		
		$productFrom->addJoin ( $joinItem );
		
		
		return true;
	}
	public function postCreate($e) {
	}
	public function postUpdate($e) {
	}
	public function postDestroy($e) {
	}
}
