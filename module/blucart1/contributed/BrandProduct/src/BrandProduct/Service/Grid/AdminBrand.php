<?php

namespace BrandProduct\Service\Grid;

use BlucartGrid\Event\AbstractBlucartGridEvent;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminBrand extends AbstractBlucartGridEvent {
	protected $_detailTemplateName =  'adminBrand';
	protected $_detailTemplate = 'common/admin-brand';
	public function getEventName() {
		return 'adminBrand';
	}
	public function details($e) {
		parent::details ( $e );
		
		if ($this->_detailTemplate) {
			$detailContainer = $this->getGrid ()->getDetailContainer ();
			$renderer = $this->getServiceLocator ()->get ( 'viewrenderer' );
			$resultObject = $this->getGrid ()->getResultObject ();
			$params = $resultObject->getResults ();
			$item = $detailContainer->add ( $this->_detailTemplateName );
			$renderer->render ( $this->_detailTemplate, array (
					'item' => $item,
					'params' => $params 
			) );
		}
	}
}
