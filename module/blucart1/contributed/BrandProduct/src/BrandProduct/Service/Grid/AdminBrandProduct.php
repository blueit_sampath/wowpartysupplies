<?php

namespace BrandProduct\Service\Grid;

use Core\Functions;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminBrandProduct extends AbstractMainBlucartGridEvent {
	protected $_entity = '\BrandProduct\Entity\BrandProduct';
	protected $_entityName = 'brandProduct';
	public function getEventName() {
		return 'adminBrandProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productId' );
		$columnItem->setTitle ( 'Product ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'product.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productTitle' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'product.title', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$brandId = Functions::fromQuery ( 'brandId', 0 );
		$joinItem = new QueryJoinItem ( 'brandProduct.brand', 'brand' );
		$fromItem->addJoin ( $joinItem );
		
		$joinItem = new QueryJoinItem ( 'brandProduct.product', 'product' );
		$fromItem->addJoin ( $joinItem );
		if ($brandId) {
			$queryBuilder->addWhere ( 'brand.id', 'brandId' );
			$queryBuilder->setParameter ( 'brandId', $brandId );
		}
		
		return $fromItem;
	}
}
