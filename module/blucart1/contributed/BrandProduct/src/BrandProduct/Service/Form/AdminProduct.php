<?php

namespace BrandProduct\Service\Form;

use Brand\Api\BrandApi;

use Product\Api\ProductApi;
use BrandProduct\Api\BrandProductApi;
use Zend\Form\Element\Select;
use Core\Functions;
use Common\Form\Option\AbstractFormEvent;

class AdminProduct extends AbstractFormEvent {
	protected $_columnKeys = array ();
	protected $_entityName = '\BrandProduct\Entity\BrandProduct';
	public function getFormName() {
		return 'adminProductAdd';
	}
	public function postInitEvent() {
		$form = $this->getForm ();
		
		$select = new Select ( 'brandProduct' );
		$select->setLabel ( 'Brands' );
		$select->setAttribute ( 'multiple', true );
		$select->setValueOptions ( $this->getBrands () );
		$form->add ( $select, array (
				'priority' => 925 
		) );
		
		$inputFilter = $form->getInputFilter ();
		$inputFilter->add ( array (
				'name' => 'brandProduct',
				'required' => false 
		) );
	}
	public function getBrands() {
		$array = array ();
		$brands = BrandApi::getBrands();
		
		foreach ( $brands as $res ) {
			$brandId = $res ['brandId'];
			
			$brand = BrandApi::getBrandById ( $brandId );
			$array [$brandId] = $brand->title;
		}
		
		return $array;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		$product = $resultContainer->get ( 'product' );
		
		if (isset ( $params ['brandProduct'] ) || $params ['brandProduct'] === null) {
			
			if ($product) {
				$product = $product->getValue ();
				if (! ($params ['brandProduct']) || ! count ( $params ['brandProduct'] )) {
					
					$dql = ' delete from ' . $this->_entityName . ' u where u.product = ' . $product->id;
					$em->createQuery ( $dql )->execute ();
					$resultContainer->add ( 'brandProduct', null );
				} else {
					$results = BrandProductApi::getBrandByProductId ( $product->id );
					$categories = array ();
					if ($results) {
						foreach ( $results as $result ) {
							$categories [] = $result ['brandId'];
						}
					}
					$temp = array_diff ( $params ['brandProduct'], $categories );
					if ($temp && count ( 'temp' )) {
						$temp1 = array ();
						foreach ( $temp as $value ) {
							$entity = new $this->_entityName ();
							$entity->product = $product;
							$entity->brand = BrandApi::getBrandById ( $value );
							$em->persist ( $entity );
						}
					}
					$em->flush ();
					$temp = array_diff ( $categories, $params ['brandProduct'] );
					if ($temp && count ( 'temp' )) {
						$dql = ' delete from ' . $this->_entityName . ' u where u.brand in (' . implode ( ',', $temp ) . ')';
						$em->createQuery ( $dql )->execute ();
					}
				}
			}
		}
		
		return true;
	}
	public function getRecord() {
		$form = $this->getForm ();
		$productId = Functions::fromRoute ( 'productId' );
		if (! $productId) {
			return;
		}
		
		$results = BrandProductApi::getBrandByProductId ( $productId, null );
		$temp = array ();
		if ($results) {
			
			foreach ( $results as $result ) {
				$temp [] = $result ['brandId'];
			}
		}
		
		$form->get ( 'brandProduct' )->setValue ( $temp );
		
		return true;
	}
}
