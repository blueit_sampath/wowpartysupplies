<?php

namespace BrandProduct\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use BrandProduct\Entity\BrandProduct;
use BrandProduct\Api\BrandProductApi;
use Product\Api\ProductApi;
use Brand\Api\BrandApi;
use Core\Functions;

class AdminBrandProduct extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight' 
	);
	protected $_entity = '\BrandProduct\Entity\BrandProduct';
	protected $_entityName = 'brandProduct';
	public function getFormName() {
		return 'adminBrandProductAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		if (isset ( $params ['product'] ) && $params ['product'] && $params ['brand']) {
			$product = ProductApi::getProductById ( $params ['product'] );
			
			if ($product) {
				$results = BrandProductApi::getBrandByProductId ( $product->id, null );
				$array = array ();
				if ($results) {
					foreach ( $results as $result ) {
						$array [] = ( int ) $result ['brandId'];
					}
				}
				foreach ( $params ['brand'] as $r ) {
					if (! in_array ( ( int ) $r, $array )) {
						$e = new BrandProduct ();
						$e->product = $product;
						$e->brand = BrandApi::getBrandById ( $r );
						$em->persist ( $e );
					}
				}
				
				$em->flush ();
			}
		}
		
		return true;
	}
	public function getRecord() {
		$id = Functions::fromRoute ( 'productId' );
		if (! $id) {
			return;
		}
		$form = $this->getForm ();
		$form->get ( 'product' )->setValue ( $id );
		
		return true;
	}
}
