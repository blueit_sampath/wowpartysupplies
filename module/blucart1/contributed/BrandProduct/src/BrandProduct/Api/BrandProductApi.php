<?php

namespace BrandProduct\Api;

use Common\Api\Api;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;

class BrandProductApi extends Api {
	protected static $_entity = 'BrandProduct\Entity\BrandProduct';
	public static function getProductCatetoryById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getBrandProductByProductId($productId) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'brandProduct.id', 'brandProductId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'brandProduct' );
		$joinItem = new QueryJoinItem ( 'brandProduct.brand', 'brand' );
		$item->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem ( 'brandProduct.product', 'product' );
		$item->addJoin ( $joinItem );
		$item = $queryBuilder->addWhere ( 'product.id', 'productId' );
		$queryBuilder->addParameter ( 'productId', $productId );
		$queryBuilder->addOrder ( 'brandProduct.weight', 'brandProductWeight', 'desc' );
		return $queryBuilder->executeQuery ();
	}
	public static function getBrandByProductId($productId, $status = true) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'brand.id', 'brandId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'brandProduct' );
		$joinItem = new QueryJoinItem ( 'brandProduct.brand', 'brand' );
		$item->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem ( 'brandProduct.product', 'product' );
		$item->addJoin ( $joinItem );
		$item = $queryBuilder->addWhere ( 'product.id', 'productId' );
		$queryBuilder->addParameter ( 'productId', $productId );
		if ($status) {
			$item = $queryBuilder->addWhere ( 'brand.status', 'brandStatus' );
			$queryBuilder->addParameter ( 'brandStatus', 1 );
		}
		
		$queryBuilder->addOrder ( 'brandProduct.weight', 'brandProductWeight', 'desc' );
		$queryBuilder->addOrder ( 'brand.weight', 'brandWeight', 'desc' );
		return $queryBuilder->executeQuery ();
	}
}
