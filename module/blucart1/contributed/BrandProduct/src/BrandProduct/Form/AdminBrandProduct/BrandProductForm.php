<?php

namespace BrandProduct\Form\AdminBrandProduct;

use Brand\Api\BrandApi;
use Product\Api\ProductApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class BrandProductForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'product',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'brand' );
		$select->setLabel ( 'Brand' );
		$select->setAttribute ( 'multiple', true );
		$select->setValueOptions ( $this->getBrands () );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getBrands() {
		$array = array ();
		$brands = BrandApi::getBrands ();
		
		foreach ( $brands as $res ) {
			$brandId = $res ['brandId'];
			
			$brand = BrandApi::getBrandById ( $brandId );
			$array [$brandId] = $brand->title;
		}
		
		return $array;
	}
}