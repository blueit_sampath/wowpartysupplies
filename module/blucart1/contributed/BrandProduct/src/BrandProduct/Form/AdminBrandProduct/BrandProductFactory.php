<?php
namespace BrandProduct\Form\AdminBrandProduct;

use Common\Form\Option\AbstractFormFactory;

class BrandProductFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'BrandProduct\Form\AdminBrandProduct\BrandProductForm' );
		$form->setName ( 'adminBrandProductAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'BrandProduct\Form\AdminBrandProduct\BrandProductFilter' );
	}
}
