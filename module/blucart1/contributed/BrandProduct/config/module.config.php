<?php
		
		return array(
    'router' => array('routes' => array(
            'admin-brand-product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/brand/product/[:productId][/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'BrandProduct\Controller\AdminBrandProduct',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-brand-product-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/brand/product/add[/:productId]',
                    'defaults' => array(
                        'controller' => 'BrandProduct\Controller\AdminBrandProduct',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-brand-product-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/brand/product/sort[/:productId]',
                    'defaults' => array(
                        'controller' => 'BrandProduct\Controller\AdminBrandProduct',
                        'action' => 'sort',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                )
            )),
    'events' => array(
        'BrandProduct\Service\Grid\AdminProduct' => 'BrandProduct\Service\Grid\AdminProduct',
        'BrandProduct\Service\Form\AdminProduct' => 'BrandProduct\Service\Form\AdminProduct',
        'BrandProduct\Service\Content\AdminBrandProductProductView' => 'BrandProduct\Service\Content\AdminBrandProductProductView',
        'BrandProduct\Service\Grid\AdminBrandProduct' => 'BrandProduct\Service\Grid\AdminBrandProduct',
        'BrandProduct\Service\Form\AdminBrandProduct' => 'BrandProduct\Service\Form\AdminBrandProduct',
        'BrandProduct\Service\Tab\AdminBrandProduct' => 'BrandProduct\Service\Tab\AdminBrandProduct',
        'BrandProduct\Service\Link\AdminBrandProduct' => 'BrandProduct\Service\Link\AdminBrandProduct',
        'BrandProduct\Service\Content\AdminBrandProduct' => 'BrandProduct\Service\Content\AdminBrandProduct',
        'BrandProduct\Service\Grid\AdminBrandProductProduct' => 'BrandProduct\Service\Grid\AdminBrandProductProduct',
        'BrandProduct\Service\Content\ProductBrand' => 'BrandProduct\Service\Content\ProductBrand'
        ),
    'service_manager' => array(
        'invokables' => array(
            'BrandProduct\Service\Grid\AdminProduct' => 'BrandProduct\Service\Grid\AdminProduct',
            'BrandProduct\Service\Form\AdminProduct' => 'BrandProduct\Service\Form\AdminProduct',
            'BrandProduct\Service\Content\AdminBrandProductProductView' => 'BrandProduct\Service\Content\AdminBrandProductProductView',
            'BrandProduct\Service\Grid\AdminBrandProduct' => 'BrandProduct\Service\Grid\AdminBrandProduct',
            'BrandProduct\Service\Form\AdminBrandProduct' => 'BrandProduct\Service\Form\AdminBrandProduct',
            'BrandProduct\Form\AdminBrandProduct\BrandProductForm' => 'BrandProduct\Form\AdminBrandProduct\BrandProductForm',
            'BrandProduct\Form\AdminBrandProduct\BrandProductFilter' => 'BrandProduct\Form\AdminBrandProduct\BrandProductFilter',
            'BrandProduct\Service\Tab\AdminBrandProduct' => 'BrandProduct\Service\Tab\AdminBrandProduct',
            'BrandProduct\Service\Link\AdminBrandProduct' => 'BrandProduct\Service\Link\AdminBrandProduct',
            'BrandProduct\Service\Content\AdminBrandProduct' => 'BrandProduct\Service\Content\AdminBrandProduct',
            'BrandProduct\Service\Grid\AdminCategory' => 'BrandProduct\Service\Grid\AdminCategory',
            'BrandProduct\Service\Grid\AdminBrandProductProduct' => 'BrandProduct\Service\Grid\AdminBrandProductProduct',
            'BrandProduct\Service\Content\ProductBrand' => 'BrandProduct\Service\Content\ProductBrand'
            ),
        'factories' => array('BrandProduct\Form\AdminBrandProduct\BrandProductFactory' => 'BrandProduct\Form\AdminBrandProduct\BrandProductFactory')
        ),
    'controllers' => array('invokables' => array('BrandProduct\Controller\AdminBrandProduct' => 'BrandProduct\Controller\AdminBrandProductController')),
    'view_manager' => array('template_path_stack' => array('BrandProduct' => __DIR__.'/../view')),
    'doctrine' => array('driver' => array(
            'BrandProduct_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__.'/../src/BrandProduct/Entity')
                ),
            'orm_default' => array('drivers' => array('BrandProduct\Entity' => 'BrandProduct_driver'))
            )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('BrandProduct' => __DIR__.'/../public')))
    );
