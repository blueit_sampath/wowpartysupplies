<?php 
namespace Inventory\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminInventoryProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-inventory-product-view';
	protected $_contentName = 'adminInventoryProductView';
	public function getEventName() {
		return 'adminProductViewTop';
	}
	public function getPriority() {
		return 900;
	}
	
}

