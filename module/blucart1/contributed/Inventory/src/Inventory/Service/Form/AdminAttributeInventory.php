<?php

namespace Inventory\Service\Form;

use Inventory\Api\InventoryApi;
use Zend\Form\Element\Select;
use Core\Functions;
use Common\Form\Option\AbstractFormEvent;

class AdminAttributeInventory extends AbstractFormEvent {
	
	protected $_entityName = '\Inventory\Entity\StockAttribute';
	public function getFormName() {
		return 'adminAttributeOptionProductAdd';
	}
	public function getPriority() {
		return 800;
	}
	public function postInitEvent() {
		$form = $this->getForm ();
		
		$form->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'stockStatus',
				'options' => array (
						'label' => 'Enable Stock',
						'use_hidden_element' => true 
				) 
		), array (
				'priority' => 925 
		) );
		
		$form->add ( array (
				'name' => 'stock',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Stock' 
				) 
		), array (
				'priority' => 925 
		) );
		
		$inputFilter = $form->getInputFilter ();
		$inputFilter->add ( array (
				'name' => 'stock',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		$inputFilter->add ( array (
				'name' => 'stockStatus',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		$renderer = $this->getServiceLocator ()->get ( 'viewrenderer' );
		$renderer->render ( 'common/admin-product-stock-js' ); // javascript
		return $this;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		$attributeOptionProduct = $resultContainer->get ( 'attributeOptionProduct' );
	
		
		if ($attributeOptionProduct) {
			$attributeOptionProduct = $attributeOptionProduct->getValue ();
			$entity = InventoryApi::getStockByAttributeOptionProductId ( $attributeOptionProduct->id );
			
			if ($params ['stockStatus']) {
				
				if (! $entity) {
					$entity = new $this->_entityName ();
					$entity->attributeOptionProduct = $attributeOptionProduct;
				}
				$entity->qty = $params ['stock'];
				$entity->status = $params ['stockStatus'];
				$em->persist ( $entity );
			} else {
				if ($entity) {
					$entity->status = 0;
				}
			}
			
			if (! $params ['stock'] && $entity) {
				$em->remove ( $entity );
			}
			
			$em->flush ();
			
			if ($entity) {
				$resultContainer->add ( 'stock', $entity );
			}
		}
		
		return true;
	}
	public function getRecord() {
		$attributeOptionProductId = Functions::fromRoute ( 'attributeOptionProductId' );
		if (! $attributeOptionProductId) {
			return;
		}
		
		$form = $this->getForm ();
		$em = $this->getEntityManager ();
		
		$entity = InventoryApi::getStockByAttributeOptionProductId ( $attributeOptionProductId );
		if (! $entity) {
			
			return;
		}
		
		$form->get ( 'stockStatus' )->setValue ( $entity->status );
		$form->get ( 'stock' )->setValue ( $entity->qty );
		
		return true;
	}
}
