<?php

namespace Inventory\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Checkout\Option\CartContainer;
use Inventory\Api\InventoryApi;

class CartInventorySave extends AbstractMainFormEvent {
	public function getFormName() {
		return 'cartCheckout';
	}
	public function getPriority() {
		return 500;
	}
	public function save() {
		$form = $this->getForm ();
		$cart = $this->getCart ();
		$em = Functions::getEntityManager ();
		/* Order Product Save */
		$orderId = $cart->getParam ( 'orderId' );
		if (! $orderId) {
			return;
		}
		$cartItems = $cart->getCartItems ();
		if (count ( $cartItems )) {
			foreach ( $cartItems as $cartItem ) {
				$attributeItem = $cartItem->getInItem ( 'attribute' );
				if ($attributeItem) {
					$attributeOptionProductId = $attributeItem->getParam ( 'attributeOptionProductId' );
					InventoryApi::blockStockAttribute($attributeOptionProductId, $orderId, $cartItem->getQuantity());
					
				} else {
					$productId = $cartItem->getProductId();
					if($productId){
						InventoryApi::blockStock($productId, $orderId, $cartItem->getQuantity());
					}
				}
			}
		}
		
		return;
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	protected function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
	
	
}
