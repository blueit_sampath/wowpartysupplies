<?php

namespace Inventory\Service\Form;

use Inventory\Api\InventoryApi;
use Zend\Form\Element\Select;
use Core\Functions;
use Common\Form\Option\AbstractFormEvent;

class AdminInventory extends AbstractFormEvent {
	protected $_columnKeys = array (
			'id',
			'qty',
			'product' 
	);
	protected $_entityName = '\Inventory\Entity\Stock';
	public function getFormName() {
		return 'adminProductAdd';
	}
	public function getPriority() {
		return 800;
	}
	public function postInitEvent() {
		$form = $this->getForm ();
		
		$form->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'stockStatus',
				'options' => array (
						'label' => 'Enable Stock',
						'use_hidden_element' => true 
				) 
		), array (
				'priority' => 925 
		) );
		
		$form->add ( array (
				'name' => 'stock',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Stock' 
				) 
		), array (
				'priority' => 925 
		) );
		
		$inputFilter = $form->getInputFilter ();
		$inputFilter->add ( array (
				'name' => 'stock',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		$inputFilter->add ( array (
				'name' => 'stockStatus',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		$renderer = $this->getServiceLocator ()->get ( 'viewrenderer' );
		$renderer->render ( 'common/admin-product-stock-js' ); // javascript
		return $this;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		$productItem = $resultContainer->get ( 'product' );
		
		if ($productItem) {
			$product = $productItem->getValue ();
			
			$entity = InventoryApi::getStockByProductId ( $product->id );
			
			
			if ($params ['stockStatus']) {
				
				if (! $entity) {
					$entity = new $this->_entityName ();
					$entity->product = $product;
				} 
				$entity->qty = $params ['stock'];
				$entity->status = $params ['stockStatus'];
				$em->persist ( $entity );
				
			} else {
				if ($entity) {
					$entity->status = 0;
				}
				
			}
			
			if (! $params ['stock'] && $entity) {
				$em->remove ( $entity );
				
			}
			
			$em->flush ();
		
			if ($entity) {
				$resultContainer->add ( 'stock', $entity );
			}
		}
		
		return true;
	}
	public function getRecord() {
		$productId = Functions::fromRoute ( 'productId' );
		if (! $productId) {
			return;
		}
		
		$form = $this->getForm ();
		$em = $this->getEntityManager ();
		
		$entity = InventoryApi::getStockByProductId ( $productId );
		if (! $entity) {
			
			return;
		}
		
		$form->get ( 'stockStatus' )->setValue ( $entity->status );
		$form->get ( 'stock' )->setValue ( $entity->qty );
		
		return true;
	}
}
