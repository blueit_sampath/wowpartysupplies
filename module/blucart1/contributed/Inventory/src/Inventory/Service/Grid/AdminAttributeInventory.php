<?php

namespace Inventory\Service\Grid;

use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractBlucartGridEvent;
use Inventory\Api\InventoryApi;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Zend\View\Model\ViewModel;

class AdminAttributeInventory extends AbstractBlucartGridEvent {
	protected $_columnKeys = array ();
	protected $_entity = '\Inventory\Entity\StockAttribute';
	protected $_entityName = 'stock';
	public function getEventName() {
		return 'adminAttributeOptionProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'stockQty' );
		$columnItem->setTitle ( 'Stock' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 935 );
		$columnItem->setBelongsToEntity ( 'stock' );
		$columns->addColumn ( 'stock.qty', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'stockStatus' );
		$columnItem->setTitle ( 'Stock Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 934 );
		$columnItem->setBelongsToEntity ( 'stock' );
		$columns->addColumn ( 'stock.status', $columnItem );
		
		$renderer = $this->getServiceLocator ()->get ( 'viewrenderer' );
		$renderer->render ( 'common/admin-grid-stock-status-template-js', array (
				'grid' => $grid 
		) ); // javascript
		
		return $columns;
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		
		$attributeOptionProduct = $queryBuilder->getFrom ( 'attributeOptionProduct' );
	
		$joinItem = new QueryJoinItem ( $this->_entity, $this->_entityName, 'left join', 'attributeOptionProduct.id = stock.attributeOptionProduct' );
		$attributeOptionProduct->addJoin ( $joinItem );
		
		$queryBuilder->addGroup ( 'attributeOptionProduct.id', 'attributeOptionProductId' );
		
		return true;
	}
	public function postUpdate($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		$params = $resultObject->getResults ();
		$attributeOptionProduct = $resultContainer->get ( 'attributeOptionProduct' );
		
		if ($attributeOptionProduct) {
			$em = $this->getEntityManager ();
			$attributeOptionProduct = $attributeOptionProduct->getValue ();
			$entity = InventoryApi::getStockByAttributeOptionProductId( $attributeOptionProduct->id );
			
			if (isset ( $params ['stockQty'] ) && ($params ['stockQty'] !== '')) {
				if (! $entity) {
					$entity = new $this->_entity ();
				} else {
					if ($entity->qty === ( int ) $params ['stockQty']) {
						return;
					}
				}
				$entity->status = $params ['stockStatus'];
				$entity->qty = $params ['stockQty'];
				$entity->attributeOptionProduct = $attributeOptionProduct;
				$em->persist ( $entity );
				$em->flush ();
			} else {
				if ($entity) {
					$em->remove ( $entity );
					$em->flush ();
				}
			}
		}
		return true;
	}
	public function postDestroy($e) {
	}
	public function postCreate($e) {
	}
}
