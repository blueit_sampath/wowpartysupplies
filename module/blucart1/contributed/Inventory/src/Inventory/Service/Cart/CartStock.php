<?php

namespace Inventory\Service\Cart;

use Core\Functions;
use Checkout\Option\CartEvent;
use Product\Api\ProductApi;
use Attribute\Api\AttributeApi;
use Inventory\Api\InventoryApi;

class CartStock extends CartEvent {
	public function validateCartItem() {
		$cart = $this->getCart ();
		$cartItem = $this->getCartItem ();
		return $this->checkStock ( $cartItem );
	}
	public function validateCart() {
		$cart = $this->getCart ();
		$items = $cart->getCartItems ();
		foreach ( $items as $item ) {
			$this->checkStock ( $item );
		}
	}
	public function checkStock($cartItem) {
		$validateCartContainer = $this->getValidationContainer ();
		$productId = $cartItem->getProductId ();
		
		if (! $productId) {
			return;
		}
		$product = ProductApi::getProductById ( $productId );
		if (! $product) {
			return;
		}
		
		$attributeInItem = $cartItem->getInItem ( 'attribute' );
		
		if ($attributeInItem) {
			$attributeOptionProductId = $attributeInItem->getParam ( 'attributeOptionProductId' );
			
			if ($attributeOptionProductId) {
				
				if (! InventoryApi::checkOrGetAttributeStock ( $attributeOptionProductId, $cartItem->getQuantity () )) {
					$validateCartContainer->add ( 'attribute_stock', $product->title . '(' . $attributeInItem->getTitle () . ') is Out of Stock' );
					return;
				}
			} else {
				
				if (! InventoryApi::checkOrGetStock ( $productId, $cartItem->getQuantity () )) {
					$validateCartContainer->add ( 'product_stock', $product->title . ' is Out of Stock' );
					return;
				}
			}
		} else {
			
			if (! InventoryApi::checkOrGetStock ( $productId, $cartItem->getQuantity () )) {
				
				$validateCartContainer->add ( 'product_stock', $product->title . ' is Out of Stock' );
				return;
			}
		}
	}
}

