<?php

namespace Inventory\Api;

use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use OrderMain\Api\OrderStatusApi;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Common\Api\Api;
use Core\Functions;
use OrderMain\Api\OrderApi;
use Inventory\Entity\StockBlock;
use Product\Api\ProductApi;
use Attribute\Api\AttributeApi;

class InventoryApi extends Api {
	protected static $_entity = '\Inventory\Entity\Stock';
	protected static $_entityAttribute = '\Inventory\Entity\StockAttribute';
	protected static $_entityBlock = '\Inventory\Entity\StockBlock';
	protected static $_entityAttributeBlock = '\Inventory\Entity\StockAttributeBlock';
	public static function getStockById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getStockAttributeById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entityAttribute, $id );
	}
	public static function getStockByProductId($productId) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findOneBy ( array (
				'product' => $productId 
		) );
	}
	
	public static function getStockByAttributeId($attributeOptionProductId) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entityAttribute )->findOneBy ( array (
				'attributeOptionProduct' => $attributeOptionProductId 
		) );
	}
	public static function getStockBlockByAttributeOptionProductIdAndOrderId($attributeOptionProductId, $orderId) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entityAttributeBlock )->findOneBy ( array (
				'attributeOptionProduct' => $attributeOptionProductId,
				'orderMain' => $orderId
		) );
	}
	
	public static function getStockBlockByProductIdAndOrderId($productId, $orderId) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entityAttribute )->findOneBy ( array (
				'product' => $productId,
				'orderMain' => $orderId
		) );
	}
	
	public static function getStockByAttributeOptionProductId($attributeOptionProduct) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entityAttribute )->findOneBy ( array (
				'attributeOptionProduct' => $attributeOptionProduct 
		) );
	}
	public static function getStockReport($skip = 0, $limit = PHP_INT_MAX) {
		$em = Functions::getEntityManager ();
		
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "stock.id", 'stockId' );
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'stock' );
		
		$queryBuilder->addWhere ( 'stock.status', 'stockStatus' );
		$queryBuilder->setParameter ( 'stockStatus', 1 );
		
		$queryBuilder->addOrder ( 'stock.qty', 'stockQty', 'asc' );
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getStockAttributeReport($skip = 0, $limit = PHP_INT_MAX) {
		$em = Functions::getEntityManager ();
		
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( "stockAttribute.id", 'stockAttributeId' );
		$item = $queryBuilder->addFrom ( static::$_entityAttribute, 'stockAttribute' );
		$queryBuilder->addWhere ( 'stockAttribute.status', 'stockAttributeStatus' );
		$queryBuilder->setParameter ( 'stockAttributeStatus', 1 );
		$queryBuilder->addOrder ( 'stockAttribute.qty', 'stockAttributeQty', 'asc' );
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function checkOrGetStock($productId, $qty = null, $entity = true) {
		$record = static::getStockByProductId ( $productId );
		if (! $record) {
			return true;
		}
		
		if (! $record->status) {
			return true;
		}
		
		
		if ($record->qty) {
			if ($qty) {
				if ($qty <= $record->qty) {
					return $record->qty;
				}
				return false;
			}
			return $record->qty;
		}
		
		return false;
	}
	public static function checkOrGetAttributeStock($attributeOptionProductId, $qty = null) {
		$record = static::getStockByAttributeId ( $attributeOptionProductId );
		
		if (! $record) {
			return true;
		}
		
		if (! $record->status) {
			return true;
		}
		
		if ($record->qty) {
			if ($qty) {
				if ($qty <= $record->qty) {
					return $record->qty;
				}
				return false;
			}
			return $record->qty;
		}
		
		return false;
	}
	public static function blockStock($productId, $orderId, $qty) {
		$em = static::getEntityManager ();
		$orderMain = OrderApi::getOrderById ( $orderId );
		if (! $orderMain) {
			return false;
		}
		$record = static::getStockByProductId ( $productId );
		if (! $record) {
			return true;
		}
		if (! $record->status) {
			return true;
		}
		
		$entity = new static::$_entityBlock ();
		$entity->qty = $qty;
		$entity->product = ProductApi::getProductById ( $productId );
		$entity->orderMain = OrderApi::getOrderById ( $orderId );
		$em->persist ( $entity );
		
		$record->qty = $record->qty - $qty;
		$em->persist ( $record );
		
		$em->flush ();
		return $entity;
	}
	public static function releaseStock($productId, $orderId) {
		$em = static::getEntityManager ();
		$entity = static::getStockBlockByProductIdAndOrderId ( $productId, $orderId );
		if (! $entity) {
			return;
		}
		$qty = $entity->qty;
		$em->remove ( $entity );
		$em->flush ();
		$record = static::getStockByProductId ( $productId );
		if (! $record) {
			return true;
		}
		$record->qty = $record->qty + $qty;
		$em->persist ( $record );
		$em->flush ();
		return $entity;
	}
	public static function releaseStockByOrderId($orderId) {
		$em = static::getEntityManager ();
		$results = $em->getRepository ( static::$_entityBlock )->findBy ( array (
				'orderMain' => $orderId
		) );
		foreach($results as $result){
			static::releaseStock($result->product->id, $orderId);
		}
		return true;
		
	}
	public static function removeStock($productId, $orderId) {
		$em = static::getEntityManager ();
		$entity = static::getStockBlockByProductIdAndOrderId ( $productId, $orderId );
		if (! $entity) {
			return;
		}
		
		$em->remove ( $entity );
		$em->flush ();
		return true;
	}
	public static function removeStockByOrderId($orderId) {
		$em = static::getEntityManager ();
		$results = $em->getRepository ( static::$_entityBlock )->findBy ( array (
				'orderMain' => $orderId
		) );
		foreach($results as $result){
			static::removeStock($result->product->id, $orderId);
		}
		return true;
	
	}
	/* stock attributes */
	
	public static function blockStockAttribute($attributeOptionProductId, $orderId, $qty) {
		$em = static::getEntityManager ();
		$orderMain = OrderApi::getOrderById ( $orderId );
		if (! $orderMain) {
			return false;
		}
		$record = static::getStockByAttributeOptionProductId ( $attributeOptionProductId );
		if (! $record) {
			return true;
		}
		if (! $record->status) {
			return true;
		}
	
		$entity = new static::$_entityAttributeBlock ();
		$entity->qty = $qty;
		$entity->attributeOptionProduct = AttributeApi::getAttributeOptionProductById ( $attributeOptionProductId );
		$entity->orderMain = OrderApi::getOrderById ( $orderId );
		$em->persist ( $entity );
	
		$record->qty = $record->qty - $qty;
		$em->persist ( $record );
	
		$em->flush ();
		
		return $entity;
	}
	public static function releaseStockAttribute($attributeOptionProductId, $orderId) {
		$em = static::getEntityManager ();
		$entity = static::getStockBlockByAttributeOptionProductIdAndOrderId ( $attributeOptionProductId, $orderId );
		if (! $entity) {
			return;
		}
		$qty = $entity->qty;
		$em->remove ( $entity );
		$em->flush ();
		$record = static::getStockByAttributeOptionProductId ( $attributeOptionProductId );
		if (! $record) {
			return true;
		}
		$record->qty = $record->qty + $qty;
		$em->persist ( $record );
		$em->flush ();
		
		return $entity;
	}
	public static function releaseStockAttributeByOrderId($orderId) {
		$em = static::getEntityManager ();
		$results = $em->getRepository ( static::$_entityAttributeBlock )->findBy ( array (
				'orderMain' => $orderId
		) );
		foreach($results as $result){
			static::releaseStockAttribute($result->attributeOptionProduct->id, $orderId);
		}
		return true;
	
	}
	public static function removeStockAttribute($attributeOptionProductId, $orderId) {
		$em = static::getEntityManager ();
		$entity = static::getStockBlockByAttributeOptionProductIdAndOrderId ( $attributeOptionProductId, $orderId );
		if (! $entity) {
			return;
		}
	
		$em->remove ( $entity );
		$em->flush ();
		return true;
	}
	public static function removeStockAttributeByOrderId($orderId) {
		$em = static::getEntityManager ();
		$results = $em->getRepository ( static::$_entityAttributeBlock )->findBy ( array (
				'orderMain' => $orderId
		) );
		foreach($results as $result){
			static::removeStockAttribute($result->attributeOptionProduct->id, $orderId);
		}
		return true;
	
	}
	
}
