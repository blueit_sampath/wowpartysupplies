<?php

namespace Inventory\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Stock
 *
 * @ORM\Table(name="stock_block")
 * @ORM\Entity
 */
class StockBlock
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="qty", type="string", length=255, nullable=true)
     */
    private $qty;
    

   
   /**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="\Product\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $product;
    
    /**
     * @var \OrderMain
     *
     * @ORM\ManyToOne(targetEntity="\OrderMain\Entity\OrderMain")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_main_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $orderMain;
    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     *
     */
    public function __get($property) {
    	if (\method_exists ( $this, "get" . \ucfirst ( $property ) )) {
    		$method_name = "get" . \ucfirst ( $property );
    		return $this->$method_name ();
    	} else {
    		 
    		if (is_object ( $this->$property )) {
    			// return $this->$property->id;
    		}
    		return $this->$property;
    	}
    }
    
    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     *
     */
    public function __set($property, $value) {
    	if (\method_exists ( $this, "set" .\ucfirst ( $property ) )) {
    		$method_name = "set" .\ucfirst ( $property );
    		 
    		$this->$method_name ( $value );
    	} else
    		$this->$property = $value;
    }


}
