<?php
return array(
    'router' => array(
        'routes' => array()
    ),
    'events' => array(
        'Inventory\Service\Form\AdminInventory' => 'Inventory\Service\Form\AdminInventory',
        'Inventory\Service\Grid\AdminInventory' => 'Inventory\Service\Grid\AdminInventory',
        'Inventory\Service\Form\AdminAttributeInventory' => 'Inventory\Service\Form\AdminAttributeInventory',
        'Inventory\Service\Grid\AdminAttributeInventory' => 'Inventory\Service\Grid\AdminAttributeInventory',
        'Inventory\Service\Content\AdminInventoryProductView' => 'Inventory\Service\Content\AdminInventoryProductView',
        'Inventory\Service\Cart\CartStock' => 'Inventory\Service\Cart\CartStock',
        'Inventory\Service\Form\CartInventorySave' => 'Inventory\Service\Form\CartInventorySave'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Inventory\Service\Form\AdminInventory' => 'Inventory\Service\Form\AdminInventory',
            'Inventory\Service\Grid\AdminInventory' => 'Inventory\Service\Grid\AdminInventory',
            'Inventory\Service\Form\AdminAttributeInventory' => 'Inventory\Service\Form\AdminAttributeInventory',
            'Inventory\Service\Grid\AdminAttributeInventory' => 'Inventory\Service\Grid\AdminAttributeInventory',
            'Inventory\Service\Content\AdminInventoryProductView' => 'Inventory\Service\Content\AdminInventoryProductView',
            'Inventory\Service\Cart\CartStock' => 'Inventory\Service\Cart\CartStock',
            'Inventory\Service\Form\CartInventorySave' => 'Inventory\Service\Form\CartInventorySave'
        ),
        'factories' => array()
    ),
    'controllers' => array(
        'invokables' => array()
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Inventory' => __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'Inventory_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Inventory/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Inventory\Entity' => 'Inventory_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Inventory' => __DIR__ . '/../public'
            )
        )
    )
);
