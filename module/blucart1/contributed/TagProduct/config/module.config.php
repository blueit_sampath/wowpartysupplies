<?php
		
		return array(
    'router' => array('routes' => array(
            'admin-tag-product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/tag/product/[:productId][/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'TagProduct\Controller\AdminTagProduct',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-tag-product-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/tag/product/add[/:productId]',
                    'defaults' => array(
                        'controller' => 'TagProduct\Controller\AdminTagProduct',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-tag-product-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/tag/product/sort[/:productId]',
                    'defaults' => array(
                        'controller' => 'TagProduct\Controller\AdminTagProduct',
                        'action' => 'sort',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                )
            )),
    'events' => array(
        'TagProduct\Service\Grid\AdminProduct' => 'TagProduct\Service\Grid\AdminProduct',
        'TagProduct\Service\Form\AdminProduct' => 'TagProduct\Service\Form\AdminProduct',
        'TagProduct\Service\Content\AdminTagProductProductView' => 'TagProduct\Service\Content\AdminTagProductProductView',
        'TagProduct\Service\Grid\AdminTagProduct' => 'TagProduct\Service\Grid\AdminTagProduct',
        'TagProduct\Service\Form\AdminTagProduct' => 'TagProduct\Service\Form\AdminTagProduct',
        'TagProduct\Service\Tab\AdminTagProduct' => 'TagProduct\Service\Tab\AdminTagProduct',
        'TagProduct\Service\Link\AdminTagProduct' => 'TagProduct\Service\Link\AdminTagProduct',
        'TagProduct\Service\Content\AdminTagProduct' => 'TagProduct\Service\Content\AdminTagProduct',
        'TagProduct\Service\Grid\AdminTagProductProduct' => 'TagProduct\Service\Grid\AdminTagProductProduct',
        'TagProduct\Service\Content\ProductTag' => 'TagProduct\Service\Content\ProductTag'
        ),
    'service_manager' => array(
        'invokables' => array(
            'TagProduct\Service\Grid\AdminProduct' => 'TagProduct\Service\Grid\AdminProduct',
            'TagProduct\Service\Form\AdminProduct' => 'TagProduct\Service\Form\AdminProduct',
            'TagProduct\Service\Content\AdminTagProductProductView' => 'TagProduct\Service\Content\AdminTagProductProductView',
            'TagProduct\Service\Grid\AdminTagProduct' => 'TagProduct\Service\Grid\AdminTagProduct',
            'TagProduct\Service\Form\AdminTagProduct' => 'TagProduct\Service\Form\AdminTagProduct',
            'TagProduct\Form\AdminTagProduct\TagProductForm' => 'TagProduct\Form\AdminTagProduct\TagProductForm',
            'TagProduct\Form\AdminTagProduct\TagProductFilter' => 'TagProduct\Form\AdminTagProduct\TagProductFilter',
            'TagProduct\Service\Tab\AdminTagProduct' => 'TagProduct\Service\Tab\AdminTagProduct',
            'TagProduct\Service\Link\AdminTagProduct' => 'TagProduct\Service\Link\AdminTagProduct',
            'TagProduct\Service\Content\AdminTagProduct' => 'TagProduct\Service\Content\AdminTagProduct',
            'TagProduct\Service\Grid\AdminCategory' => 'TagProduct\Service\Grid\AdminCategory',
            'TagProduct\Service\Grid\AdminTagProductProduct' => 'TagProduct\Service\Grid\AdminTagProductProduct',
            'TagProduct\Service\Content\ProductTag' => 'TagProduct\Service\Content\ProductTag'
            ),
        'factories' => array('TagProduct\Form\AdminTagProduct\TagProductFactory' => 'TagProduct\Form\AdminTagProduct\TagProductFactory')
        ),
    'controllers' => array('invokables' => array('TagProduct\Controller\AdminTagProduct' => 'TagProduct\Controller\AdminTagProductController')),
    'view_manager' => array('template_path_stack' => array('TagProduct' => __DIR__.'/../view')),
    'doctrine' => array('driver' => array(
            'TagProduct_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__.'/../src/TagProduct/Entity')
                ),
            'orm_default' => array('drivers' => array('TagProduct\Entity' => 'TagProduct_driver'))
            )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('TagProduct' => __DIR__.'/../public')))
    );
