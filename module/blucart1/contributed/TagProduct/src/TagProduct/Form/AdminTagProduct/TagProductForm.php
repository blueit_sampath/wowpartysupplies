<?php

namespace TagProduct\Form\AdminTagProduct;

use Tag\Api\TagApi;
use Product\Api\ProductApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class TagProductForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'product',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'tag' );
		$select->setLabel ( 'Tag' );
		$select->setAttribute ( 'multiple', true );
		$select->setValueOptions ( $this->getTags () );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getTags() {
		$array = array ();
		$tags = TagApi::getTags ();
		
		foreach ( $tags as $res ) {
			$tagId = $res ['tagId'];
			
			$tag = TagApi::getTagById ( $tagId );
			$array [$tagId] = $tag->title;
		}
		
		return $array;
	}
}