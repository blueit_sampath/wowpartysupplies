<?php
namespace TagProduct\Form\AdminTagProduct;

use Common\Form\Option\AbstractFormFactory;

class TagProductFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'TagProduct\Form\AdminTagProduct\TagProductForm' );
		$form->setName ( 'adminTagProductAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'TagProduct\Form\AdminTagProduct\TagProductFilter' );
	}
}
