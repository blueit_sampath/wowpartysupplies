<?php

namespace TagProduct\Api;

use Common\Api\Api;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;

class TagProductApi extends Api {

    protected static $_entity = 'TagProduct\Entity\TagProduct';

    public static function getProductCatetoryById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getTagProductByProductId($productId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('tagProduct.id', 'tagProductId');
        $item = $queryBuilder->addFrom(static::$_entity, 'tagProduct');
        $joinItem = new QueryJoinItem('tagProduct.tag', 'tag');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('tagProduct.product', 'product');
        $item->addJoin($joinItem);
        $item = $queryBuilder->addWhere('product.id', 'productId');
        $queryBuilder->addParameter('productId', $productId);
        $queryBuilder->addOrder('tagProduct.weight', 'tagProductWeight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getTagByProductId($productId, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('tag.id', 'tagId');
        $item = $queryBuilder->addFrom(static::$_entity, 'tagProduct');
        $joinItem = new QueryJoinItem('tagProduct.tag', 'tag');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('tagProduct.product', 'product');
        $item->addJoin($joinItem);
        $item = $queryBuilder->addWhere('product.id', 'productId');
        $queryBuilder->addParameter('productId', $productId);
        if ($status) {
            $item = $queryBuilder->addWhere('tag.status', 'tagStatus');
            $queryBuilder->addParameter('tagStatus', 1);
        }

        $queryBuilder->addOrder('tagProduct.weight', 'tagProductWeight', 'desc');
        $queryBuilder->addOrder('tag.weight', 'tagWeight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function hasTagNameForProductByCategoryId($categoryId, $tagName, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn(count('tag.id'), 'tagIdCount');
        $item = $queryBuilder->addFrom(static::$_entity, 'tagProduct');
        $joinItem = new QueryJoinItem('tagProduct.tag', 'tag');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('tagProduct.product', 'product');
        $item->addJoin($joinItem);


        $joinItem2 = new QueryJoinItem('\ProductCategory\Entity\ProductCategory', 'productCategory', 'inner join', 'product.id = productCategory.product');
        $joinItem->addJoin($joinItem2);

        $joinItem3 = new QueryJoinItem('productCategory.category', 'category');
        $joinItem2->addJoin($joinItem3);

        $whereItem = $queryBuilder->addWhere('category.id', 'categoryId');
        $queryBuilder->addParameter('categoryId', $categoryId);

        if ($status) {
            $item = $queryBuilder->addWhere('tag.status', 'tagStatus');
            $queryBuilder->addParameter('tagStatus', 1);
        }

        $item = $queryBuilder->addWhere('tag.title', 'tagName');
        $queryBuilder->addParameter('tagName', $tagName);

        $results = $queryBuilder->executeQuery();
      
         if(count($results)){
            $array = (array)$results;
            $array = array_shift($array);
            return $array['tagIdCount'];
        }
        return false;
    }

}
