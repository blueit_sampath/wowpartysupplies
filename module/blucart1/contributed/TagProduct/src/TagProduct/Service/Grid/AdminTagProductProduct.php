<?php

namespace TagProduct\Service\Grid;

use Core\Functions;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminTagProductProduct extends AbstractMainBlucartGridEvent {
	protected $_entity = '\TagProduct\Entity\TagProduct';
	protected $_entityName = 'tagProduct';
	protected $_columnKeys = array (
			'id',
			'weight'
	);
	public function getEventName() {
		return 'adminTagProductProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		$grid = $this->getGrid ();
		$array = array ();
		$productId = Functions::fromRoute ( 'productId', 0 );
		if ($productId) {
			$grid->addAdditionalParameter ( 'productId', $productId );
		}
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'tagProduct.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'tagTitle' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'tag.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Priority' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'tagProduct.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 600 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$productId = Functions::fromQuery ( 'productId', 0 );
		$joinItem = new QueryJoinItem ( 'tagProduct.tag', 'tag' );
		$fromItem->addJoin ( $joinItem );
		
		$joinItem = new QueryJoinItem ( 'tagProduct.product', 'product' );
		$fromItem->addJoin ( $joinItem );
		if ($productId) {
			$queryBuilder->addWhere ( 'product.id', 'productId' );
			$queryBuilder->setParameter ( 'productId', $productId );
		}
		
		return $fromItem;
	}
}
