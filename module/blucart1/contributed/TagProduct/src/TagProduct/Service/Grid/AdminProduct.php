<?php

namespace TagProduct\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractBlucartGridEvent;

class AdminProduct extends AbstractBlucartGridEvent {
	protected $_entity = 'TagProduct\Entity\TagProduct';
	protected $_entityName = 'TagProduct';
	public function getEventName() {
		return 'adminProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'tags' );
		$columnItem->setTitle ( 'Tags' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 995 );
		$columnItem->setIsHaving ( true );
		$columnItem->setBelongsToEntity ( 'tag' );
		$columns->addColumn ( 'GroupConcat(Distinct tag.title)', $columnItem );
		
		return $columns;
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		
		$productFrom = $queryBuilder->getFrom ( 'product' );
		$joinItem = new QueryJoinItem ( '\TagProduct\Entity\TagProduct', 'tagProduct', 'left join', 'product.id = tagProduct.product' );
		$joinItem2 = new QueryJoinItem ( 'tagProduct.tag', 'tag' );
		$joinItem->addJoin ( $joinItem2 );
		
		$productFrom->addJoin ( $joinItem );
		
		
		return true;
	}
	public function postCreate($e) {
	}
	public function postUpdate($e) {
	}
	public function postDestroy($e) {
	}
}
