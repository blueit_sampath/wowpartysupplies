<?php

namespace TagProduct\Service\Grid;

use Core\Functions;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminTagProduct extends AbstractMainBlucartGridEvent {
	protected $_entity = '\TagProduct\Entity\TagProduct';
	protected $_entityName = 'tagProduct';
	public function getEventName() {
		return 'adminTagProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productId' );
		$columnItem->setTitle ( 'Product ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'product.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productTitle' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'product.title', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$tagId = Functions::fromQuery ( 'tagId', 0 );
		$joinItem = new QueryJoinItem ( 'tagProduct.tag', 'tag' );
		$fromItem->addJoin ( $joinItem );
		
		$joinItem = new QueryJoinItem ( 'tagProduct.product', 'product' );
		$fromItem->addJoin ( $joinItem );
		if ($tagId) {
			$queryBuilder->addWhere ( 'tag.id', 'tagId' );
			$queryBuilder->setParameter ( 'tagId', $tagId );
		}
		
		return $fromItem;
	}
}
