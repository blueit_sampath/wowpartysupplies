<?php 
namespace TagProduct\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminTagProduct extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminTagProduct';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		if(!$productId){
			return;
		}
		$u = $url ( 'admin-tag-product-add', array (
				'productId' => $productId
		) );
		$item = $linkContainer->add ( 'admin-tag-product-add', 'Add Tag Products', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

