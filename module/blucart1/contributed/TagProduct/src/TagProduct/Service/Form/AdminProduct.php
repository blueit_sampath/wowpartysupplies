<?php

namespace TagProduct\Service\Form;

use Tag\Api\TagApi;

use Product\Api\ProductApi;
use TagProduct\Api\TagProductApi;
use Zend\Form\Element\Select;
use Core\Functions;
use Common\Form\Option\AbstractFormEvent;

class AdminProduct extends AbstractFormEvent {
	protected $_columnKeys = array ();
	protected $_entityName = '\TagProduct\Entity\TagProduct';
	public function getFormName() {
		return 'adminProductAdd';
	}
	public function postInitEvent() {
		$form = $this->getForm ();
		
		$select = new Select ( 'tagProduct' );
		$select->setLabel ( 'Tags' );
		$select->setAttribute ( 'multiple', true );
		$select->setValueOptions ( $this->getTags () );
		$form->add ( $select, array (
				'priority' => 925 
		) );
		
		$inputFilter = $form->getInputFilter ();
		$inputFilter->add ( array (
				'name' => 'tagProduct',
				'required' => false 
		) );
	}
	public function getTags() {
		$array = array ();
		$tags = TagApi::getTags();
		
		foreach ( $tags as $res ) {
			$tagId = $res ['tagId'];
			
			$tag = TagApi::getTagById ( $tagId );
			$array [$tagId] = $tag->title;
		}
		
		return $array;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		$product = $resultContainer->get ( 'product' );
		
		if (isset ( $params ['tagProduct'] ) || $params ['tagProduct'] === null) {
			
			if ($product) {
				$product = $product->getValue ();
				if (! ($params ['tagProduct']) || ! count ( $params ['tagProduct'] )) {
					
					$dql = ' delete from ' . $this->_entityName . ' u where u.product = ' . $product->id;
					$em->createQuery ( $dql )->execute ();
					$resultContainer->add ( 'tagProduct', null );
				} else {
					$results = TagProductApi::getTagByProductId ( $product->id );
					$categories = array ();
					if ($results) {
						foreach ( $results as $result ) {
							$categories [] = $result ['tagId'];
						}
					}
					$temp = array_diff ( $params ['tagProduct'], $categories );
					if ($temp && count ( 'temp' )) {
						$temp1 = array ();
						foreach ( $temp as $value ) {
							$entity = new $this->_entityName ();
							$entity->product = $product;
							$entity->tag = TagApi::getTagById ( $value );
							$em->persist ( $entity );
						}
					}
					$em->flush ();
					$temp = array_diff ( $categories, $params ['tagProduct'] );
					if ($temp && count ( 'temp' )) {
						$dql = ' delete from ' . $this->_entityName . ' u where u.tag in (' . implode ( ',', $temp ) . ')';
						$em->createQuery ( $dql )->execute ();
					}
				}
			}
		}
		
		return true;
	}
	public function getRecord() {
		$form = $this->getForm ();
		$productId = Functions::fromRoute ( 'productId' );
		if (! $productId) {
			return;
		}
		
		$results = TagProductApi::getTagByProductId ( $productId, null );
		$temp = array ();
		if ($results) {
			
			foreach ( $results as $result ) {
				$temp [] = $result ['tagId'];
			}
		}
		
		$form->get ( 'tagProduct' )->setValue ( $temp );
		
		return true;
	}
}
