<?php

namespace TagProduct\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use TagProduct\Entity\TagProduct;
use TagProduct\Api\TagProductApi;
use Product\Api\ProductApi;
use Tag\Api\TagApi;
use Core\Functions;

class AdminTagProduct extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight' 
	);
	protected $_entity = '\TagProduct\Entity\TagProduct';
	protected $_entityName = 'tagProduct';
	public function getFormName() {
		return 'adminTagProductAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		if (isset ( $params ['product'] ) && $params ['product'] && $params ['tag']) {
			$product = ProductApi::getProductById ( $params ['product'] );
			
			if ($product) {
				$results = TagProductApi::getTagByProductId ( $product->id, null );
				$array = array ();
				if ($results) {
					foreach ( $results as $result ) {
						$array [] = ( int ) $result ['tagId'];
					}
				}
				foreach ( $params ['tag'] as $r ) {
					if (! in_array ( ( int ) $r, $array )) {
						$e = new TagProduct ();
						$e->product = $product;
						$e->tag = TagApi::getTagById ( $r );
						$em->persist ( $e );
					}
				}
				
				$em->flush ();
			}
		}
		
		return true;
	}
	public function getRecord() {
		$id = Functions::fromRoute ( 'productId' );
		if (! $id) {
			return;
		}
		$form = $this->getForm ();
		$form->get ( 'product' )->setValue ( $id );
		
		return true;
	}
}
