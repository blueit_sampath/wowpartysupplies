<?php

namespace TagProduct\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminTagProduct extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		if (! $productId) {
			return;
		}
		$u = $url ( 'admin-tag-product', array (
				'productId' => $productId 
		) );
		
		$tabContainer->add ( 'admin-tag-product', 'Tags', $u, 985 );
		
		return $this;
	}
}

