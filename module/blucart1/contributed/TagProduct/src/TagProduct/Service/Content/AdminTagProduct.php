<?php 
namespace TagProduct\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminTagProduct extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-tag-product';
	protected $_contentName = 'adminTagProduct';
	public function getEventName() {
		return 'adminTagView';
	}
	public function getPriority() {
		return 1000;
	}
	
}

