<?php 
namespace TagProduct\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminTagProductProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-tag-product-product-view';
	protected $_contentName = 'adminTagProductProductView';
	public function getEventName() {
		return 'adminProductViewTop';
	}
	public function getPriority() {
		return 600;
	}
	
}

