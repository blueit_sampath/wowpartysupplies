<?php

namespace TagProduct\Controller;

use Common\MVC\Controller\AbstractAdminController;


class AdminTagProductController extends AbstractAdminController{
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'TagProduct\Form\AdminTagProduct\TagProductFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-tag-product-add', array (
						'productId' => $form->get ( 'product' )->getValue () 
				) );
	}
	
}