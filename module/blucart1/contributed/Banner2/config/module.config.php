<?php

return array (
		'router' => array (
				'routes' => array (
						'admin-banner2' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/banner2',
										'defaults' => array (
												'controller' => 'Banner2\Controller\AdminBanner2',
												'action' => 'index' 
										) 
								) 
						),
						'admin-banner2-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/banner2/add[/ajax/:ajax][/:banner2Id]',
										'defaults' => array (
												'controller' => 'Banner2\Controller\AdminBanner2',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'banner2Id' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-banner2-sort' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/banner2/sort[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'Banner2\Controller\AdminBanner2',
												'action' => 'sort',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'Banner2\Service\Grid\AdminBanner2' => 'Banner2\Service\Grid\AdminBanner2',
				'Banner2\Service\Form\AdminBanner2' => 'Banner2\Service\Form\AdminBanner2',
				'Banner2\Service\Tab\AdminBanner2' => 'Banner2\Service\Tab\AdminBanner2',
				'Banner2\Service\Link\AdminBanner2' => 'Banner2\Service\Link\AdminBanner2',
				'Banner2\Service\NestedSorting\AdminBanner2' => 'Banner2\Service\NestedSorting\AdminBanner2',
				'Banner2\Service\Block\Banner2Block' => 'Banner2\Service\Block\Banner2Block',
				'Banner2\Service\Navigation\AdminBanner2' => 'Banner2\Service\Navigation\AdminBanner2' 
		),
		'service_manager' => array (
				'invokables' => array (
						'Banner2\Service\Grid\AdminBanner2' => 'Banner2\Service\Grid\AdminBanner2',
						'Banner2\Service\Form\AdminBanner2' => 'Banner2\Service\Form\AdminBanner2',
						'Banner2\Form\AdminBanner2\Banner2Form' => 'Banner2\Form\AdminBanner2\Banner2Form',
						'Banner2\Form\AdminBanner2\Banner2Filter' => 'Banner2\Form\AdminBanner2\Banner2Filter',
						'Banner2\Service\Tab\AdminBanner2' => 'Banner2\Service\Tab\AdminBanner2',
						'Banner2\Service\Link\AdminBanner2' => 'Banner2\Service\Link\AdminBanner2',
						'Banner2\Service\NestedSorting\AdminBanner2' => 'Banner2\Service\NestedSorting\AdminBanner2',
						'Banner2\Service\Block\Banner2Block' => 'Banner2\Service\Block\Banner2Block',
						'Banner2\Service\Navigation\AdminBanner2' => 'Banner2\Service\Navigation\AdminBanner2' 
				),
				'factories' => array (
						'Banner2\Form\AdminBanner2\Banner2FormFactory' => 'Banner2\Form\AdminBanner2\Banner2FormFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'Banner2\Controller\AdminBanner2' => 'Banner2\Controller\AdminBanner2Controller' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'Banner2' => __DIR__.'/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						'Banner2_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__.'/../src/Banner2/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										'Banner2\Entity' => 'Banner2_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'Banner2' => __DIR__.'/../public' 
						) 
				) 
		) 
);
