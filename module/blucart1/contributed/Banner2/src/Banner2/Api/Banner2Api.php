<?php

namespace Banner2\Api;

use Core\Functions;
use Common\Api\Api;

class Banner2Api extends Api {
	protected static $_entity = '\Banner2\Entity\Banner2';
	public static function getBanner2ById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getBanner2s($status = true) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->setEventName('Banner2Api.getBanner2s');
		$queryBuilder->addColumn ( 'banner2.id', 'banner2Id' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'banner2.status', 'banner2Status' );
			$queryBuilder->addParameter ( 'banner2Status', $status );
		}
		$queryBuilder->addOrder ( 'banner2.weight', 'banner2Weight', 'desc' );
		$queryBuilder->addGroup ( 'banner2.id', 'banner2Id' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'banner2' );
		return $queryBuilder->executeQuery ();
	}
	public static function getSortedArray() {
		$array = static::getBanner2s ( null );
		return static::makeImageArray ( $array );
	}
	public static function makeImageArray($array) {
		$result = array ();
		foreach ( $array as $banner2Results ) {
			$banner2Id = $banner2Results ['banner2Id'];
			$banner2 = static::getBanner2ById ( $banner2Id );
			$result [$banner2->id] = array (
					'name' => $banner2->file->path 
			);
		}
		return $result;
	}
}
