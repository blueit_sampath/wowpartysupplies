<?php 
namespace Banner2\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class Banner2Block extends AbstractBlockEvent {

	protected $_blockTemplate = 'banner2-block';

	public function getBlockName() {
		return 'banner2Block';
	}
	public function getPriority() {
		return 1000;
	}
	
}

