<?php

namespace Banner2\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminBanner2 extends AbstractTabEvent {
	public function getEventName() {
		return 'adminBanner2Add';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$banner2Id = Functions::fromRoute ( 'banner2Id' );
		if (! $banner2Id) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-banner2-add', array (
				'banner2Id' => $banner2Id 
		) );
		$tabContainer->add ( 'admin-banner2-add', 'General', $u, 1000 );
		
		return $this;
	}
}

