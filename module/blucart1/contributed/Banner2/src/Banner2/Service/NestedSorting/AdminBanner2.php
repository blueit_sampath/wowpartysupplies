<?php

namespace Banner2\Service\NestedSorting;

use Core\Functions;
use Banner2\Api\Banner2Api;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminBanner2 extends AbstractNestedSortingEvent {
	protected $_entityName = '\Banner2\Entity\Banner2';
	protected $_column = 'path';
	public function getEventName() {
		return 'adminBanner2Sort';
	}
	public function getPriority() {
		return 1000;
	}
	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (), array (
				'weight' => 'desc' 
		) );
		if ($results) {
			$column = $this->_column;
			foreach ( $results as $result ) {
				if ($result->file) {
					$array [$result->id] = array (
							'name' => $result->file->$column 
					);
				}
			}
		}
		return $array;
	}
}
