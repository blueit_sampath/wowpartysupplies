<?php

namespace Banner2\Service\Form;

use File\Api\FileApi;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminBanner2 extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight',
			'status',
			'url' 
	);
	protected $_entity = '\Banner2\Entity\Banner2';
	protected $_entityName = 'banner2';
	public function getFormName() {
		return 'adminBanner2Add';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		FileApi::deleteFiles ( Functions::fromPost ( 'image_delete', '' ) );
		
		$image = $params ['image'];
		
		if ($image) {
			$array = explode ( ',', $image );
			foreach ( $array as $file ) {
				$imageEntity = FileApi::createOrUpdateFile ( $file, $params ['title'], $params ['alt'] );
			}
		}
		
		$entity->file = $imageEntity;
	}
	
	public function afterGetRecord($entity){
		$form = $this->getForm();
		$form->get ( 'title' )->setValue ( $entity->file->title );
		$form->get ( 'alt' )->setValue ( $entity->file->alt );
		$form->get ( 'image' )->setValue ( $entity->file->path );
		
	}
	
}
