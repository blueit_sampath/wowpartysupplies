<?php

namespace Banner2\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminBanner2 extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminBanner2';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-banner2-add' );
		$item = $linkContainer->add ( 'admin-banner2-add', 'Add Banner2', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-banner2-sort' );
		$item = $linkContainer->add ( 'admin-banner2-sort', 'Arrange Banners', $u, 990 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

