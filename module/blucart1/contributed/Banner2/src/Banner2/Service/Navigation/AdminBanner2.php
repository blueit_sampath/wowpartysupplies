<?php 
namespace Banner2\Service\Navigation;

use Admin\Navigation\Event\AdminNavigationEvent;
use Core\Functions;

class AdminBanner2 extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'websiteMain' );
	
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Side Banner',
							'route' => 'admin-banner2',
							'id' => 'admin-banner2',
							'iconClass' => 'glyphicon-film' 
					) 
			) );
		}
	}
}

