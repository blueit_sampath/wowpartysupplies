<?php
namespace Banner2\Form\AdminBanner2;

use Common\Form\Option\AbstractFormFactory;

class Banner2FormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Banner2\Form\AdminBanner2\Banner2Form' );
		$form->setName ( 'adminBanner2Add' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Banner2\Form\AdminBanner2\Banner2Filter' );
	}
}
