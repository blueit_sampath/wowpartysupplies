<?php

namespace Banner2\Controller;

use Common\MVC\Controller\AbstractAdminController;

use Core\Functions;

use Zend\View\Model\ViewModel;

class AdminBanner2Controller extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Banner2\Form\AdminBanner2\Banner2FormFactory' );
	}
	
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-banner2-add', array (
				'banner2Id' => $form->get ( 'id' )->getValue () 
		) );
	}
}