<?php

return array(
    'controllers' => array('invokables' => array('Export\Controller\AdminExport' => 'Export\Controller\AdminExportController')),
    'service_manager' => array(
        'invokables' => array(
            'ExportContainer' => 'Export\Option\ExportContainer',
            'Export\Form\AdminExport\ExportForm' => 'Export\Form\AdminExport\ExportForm',
            'Export\Form\AdminExport\ExportFilter' => 'Export\Form\AdminExport\ExportFilter',
            'Export\Service\Navigation\ExportNavigation' => 'Export\Service\Navigation\ExportNavigation'
        ),
        'shared' => array('ExportContainer' => false),
        'factories' => array('Export\Form\AdminExport\ExportFactory' => 'Export\Form\AdminExport\ExportFactory')
    ),
    'router' => array('routes' => array(
            'admin-export' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/export[/eventName/:eventName]',
                    'defaults' => array(
                        'controller' => 'Export\Controller\AdminExport',
                        'action' => 'export'
                    ),
                    'constraints' => array('ajax' => 'true|false')
                )
            ),
            'admin-export-save' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/export/save[/eventName/:eventName]',
                    'defaults' => array(
                        'controller' => 'Export\Controller\AdminExport',
                        'action' => 'save',
                        'ajax' => 'true'
                    ),
                    'constraints' => array('ajax' => 'true|false')
                )
            )
        )),
    'view_manager' => array('template_path_stack' => array('Export' => __DIR__ . '/../view')),
    'events' => array('Export\Service\Navigation\ExportNavigation' => 'Export\Service\Navigation\ExportNavigation')
);
