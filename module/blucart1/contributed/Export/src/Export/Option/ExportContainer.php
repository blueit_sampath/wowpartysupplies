<?php

namespace Export\Option;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;
use Core\Functions;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Form\Element\Select;
use Core\Item\Container\ItemContainer;

class ExportContainer extends ItemContainer implements ServiceLocatorAwareInterface, EventManagerAwareInterface {

    protected $_title = '';
    protected $_dataItems = array();
    protected $_entities = array();
    protected $_columns = array();
    protected $_selectedColumns = array();
    protected $_eventName = null;
    protected $_services;
    protected $_events;

    /**
     *
     * @return the $_title
     */
    public function getTitle() {
        return $this->_title;
    }

    /**
     *
     * @param string $_title            
     */
    public function setTitle($_title) {
        $this->_title = $_title;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->_services = $serviceLocator;
    }

    /**
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator() {
        return $this->_services;
    }

    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    /**
     *
     * @return EventManagerInterface
     */
    public function getEventManager() {
        if (null === $this->_events) {
            $this->setEventManager(new EventManager());
        }
        return $this->_events;
    }

    /**
     *
     * @return the $_eventName
     */
    public function getEventName() {
        return $this->_eventName;
    }

    /**
     *
     * @param field_type $_eventName            
     */
    public function setEventName($_eventName) {
        $this->_eventName = $_eventName;
    }

    /**
     *
     * @return the $_data
     */
    public function getSelectedColumns() {
        return $this->_selectedColumns;
    }

    /**
     *
     * @param multitype: $_data            
     */
    public function setSelectedColumns($_data) {
        $this->_selectedColumns = $_data;

        return $this;
    }

    /**
     *
     * @return multitype:ColumnItem
     */
    public function getColumns() {
        return $this->sortArray($this->_columns);
    }

    public function setColumns($_columns = array()) {
        $this->_columns = $_columns;
        return $this;
    }

    /**
     *
     * @param unknown $key            
     * @return multitype:ColumnItem boolean
     */
    public function getColumn($key) {
        if (isset($this->_columns[$key])) {
            return $this->_columns[$key];
        }
        return false;
    }

    public function removeColumn($key) {
        if (isset($this->_columns[$key])) {
            unset($this->_columns[$key]);
            return true;
        }
        return false;
    }

    public function clearColumns() {
        $this->_columns = array();
        return $this;
    }

    /**
     *
     * @param unknown $key            
     * @param unknown $title            
     * @param unknown $inputArray            
     * @param string $weight            
     * @return \Export\Option\ColumnItem
     */
    public function addColumn($key, $title, $weight = null) {
        $item = new ColumnItem($key);
        $item->setTitle($title);
        $item->setWeight($weight);
        $this->_columns[$key] = $item;
        return $item;
    }

    public function loadSchema() {
        $this->getEventManager()->trigger($this->getEventName() . '-export-loadschema.pre', $this);
    }

    public function fetch() {

        $this->getEventManager()->trigger($this->getEventName() . '-export-fetch.pre', $this);
    }

    /**
     *
     * @param unknown $key            
     * @param unknown $title            
     * @param unknown $inputArray            
     * @param string $weight            
     * @return \Export\Option\ColumnItem
     */
    public function addDataItem($key, $array = array(), $weight = null) {
        $item = new DataItem($key);
        $item->setDataArray($array);
        $item->setWeight($weight);
        $this->_dataItems[$key] = $item;
        return $item;
    }

    /**
     *
     * @return multitype:DataItemItem
     */
    public function getDataItems() {
        return $this->sortArray($this->_dataItems);
    }

    public function setDataItems($_dataItems = array()) {
        $this->_dataItems = $_dataItems;
        return $this;
    }

    /**
     *
     * @param unknown $key            
     * @return multitype:DataItemItem boolean
     */
    public function getDataItem($key) {
        if (isset($this->_dataItems[$key])) {
            return $this->_dataItems[$key];
        }
        return false;
    }

    public function removeDataItem($key) {
        if (isset($this->_dataItems[$key])) {
            unset($this->_dataItems[$key]);
            return true;
        }
        return false;
    }

    public function clearDataItems() {
        $this->_dataItems = array();
        return $this;
    }

    public function getDataToArray($includeHeaders = true) {

        $data = array();

        $selectedColumns = $this->_selectedColumns;
        if ($includeHeaders) {

            $temp = array();
            foreach ($selectedColumns as $column) {
                $columnItem = $this->getColumn($column);
                if ($columnItem) {
                    $temp[] = $columnItem->getTitle();
                } else {
                    $temp[] = '';
                }
            }
            $data[] = $temp;
        }
        $dataObjects = $this->getDataItems();

        foreach ($dataObjects as $dataObject) {

            $temp1 = $dataObject->getDataArray();
            $temp = array();
            foreach ($selectedColumns as $column) {
                $value = '';
                if (isset($temp1[$column])) {
                    $value = $temp1[$column];
                    if ( is_array($value)) {
                        $value = implode(';', $value);
                    }
                }

                $temp[$column] = $value;
            }
            $data[] = $temp;
        }
        return $data;
    }

}
