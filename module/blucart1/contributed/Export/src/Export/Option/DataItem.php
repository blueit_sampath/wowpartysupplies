<?php
namespace Export\Option;

use Core\Item\Item;

class DataItem extends Item
{

    protected $_dataArray = array();


    /**
     *
     * @return the $_title
     */
    public function getDataArray()
    {
        return $this->_dataArray;
    }

   

    /**
     *
     * @param field_type $_title            
     */
    public function setDataArray($_dataArray)
    {
        $this->_dataArray = $_dataArray;
        return;
    }

   
}
