<?php

namespace Export\Option;

use Core\Functions;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractExportEvent extends AbstractEvent {

    protected $_importContainer = null;
    protected $_importValidationContainer = null;
    protected $_event = null;

    abstract public function getEventName();

    public function getPriority() {
        return null;
    }

    public function attach(EventManagerInterface $events) {
        $eventName = $this->getEventName();
        $priority = $this->getPriority();

        $sharedEventManager = $events->getSharedManager();
        $this->_listeners[] = $sharedEventManager->attach('*', $eventName . '-export-loadschema.pre', array(
            $this,
            'preLoadSchema'
                ), $priority);
        $this->_listeners[] = $sharedEventManager->attach('*', $eventName . '-export-fetch.pre', array(
            $this,
            'preFetch'
                ), $priority);

      

        return $this;
    }

    public function preLoadSchema($e) {
        $importContainer = $e->getTarget();
        $this->setExportContainer($importContainer);
        $this->setEvent($e);
        return $this->loadSchema();
    }

    public function loadSchema() {
        
    }

   

    public function preFetch($e) {
        $importContainer = $e->getTarget();
        $this->setExportContainer($importContainer);
        $this->setEvent($e);
        return $this->fetch();
    }

    public function fetch() {
        
    }
    
   

    /**
     *
     * @return the $_event
     */
    public function getEvent() {
        return $this->_event;
    }

    /**
     *
     * @param field_type $_event            
     */
    public function setEvent($_event) {
        $this->_event = $_event;
    }

    /**
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager() {
        return Functions::getEntityManager();
    }

    /**
     *
     * @return ExportContainer
     */
    public function getExportContainer() {
        return $this->_importContainer;
    }

    /**
     *
     * @param ExportContainer $_importContainer            
     */
    public function setExportContainer($_importContainer) {
        $this->_importContainer = $_importContainer;
    }

   

}
