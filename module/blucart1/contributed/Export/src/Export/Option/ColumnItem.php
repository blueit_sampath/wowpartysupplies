<?php
namespace Export\Option;

use Core\Item\Item;

class ColumnItem extends Item
{

    protected $_title;


    /**
     *
     * @return the $_title
     */
    public function getTitle()
    {
        return $this->_title;
    }

   

    /**
     *
     * @param field_type $_title            
     */
    public function setTitle($_title)
    {
        $this->_title = $_title;
    }

   
}
