<?php

namespace Export\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ExportNavigation extends \Admin\Navigation\Event\AdminNavigationEvent {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMain');
        
            $page->addPages(array(
                array(
                    'label' => 'Export',
                    'uri' => '#',
                    'id' => 'productMainExportMain',
                    'iconClass' => 'glyphicon-wrench',
                    'priority' => 10000
                )
            ));
        
    }

}
