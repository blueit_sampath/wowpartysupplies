<?php
namespace Export\Form\AdminExport;

use Common\Form\Option\AbstractFormFactory;

class ExportFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Export\Form\AdminExport\ExportForm' );
		$form->setName ( 'adminExport' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Export\Form\AdminExport\ExportFilter' );
	}
}
