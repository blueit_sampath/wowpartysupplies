<?php
namespace Export\Form\AdminExport;

use Zend\InputFilter\InputFilter;

class ExportFilter extends InputFilter

{

    protected $inputFilter;

    public function __construct()
    {
        $this->add(array(
            'name' => 'columns',
            'required' => true,
            
        ));
        
        
        $this->add(array(
            'name' => 'seperator',
            'required' => true
        ));
    }
} 