<?php

namespace Export\Form\AdminExport;

use Common\Form\Form;
use Zend\Form\Element\File;

class ExportForm extends Form {

    public function init() {
        $columns = $this->getColumns();
        if ($columns) {
            $select = new \Zend\Form\Element\Select('columns');
            $select->setLabel('Please Select Columns');
            $select->setAttribute('multiple', true);
            $select->setValueOptions($this->getColumns());
            $this->add($select, array(
                'priority' => 1000
            ));
        }
        $this->add(array(
            'name' => 'seperator',
            'attributes' => array(
                'type' => 'text',
                'value' => ','
            ),
            'options' => array(
                'label' => 'Seperator'
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Export'
            )
                ), array(
            'priority' => - 100
        ));
    }

    public function getColumns() {
        $exportContainer = $this->getExportContainer();
        $exportContainer->setEventName(\Core\Functions::fromRoute('eventName'));
        $exportContainer->loadSchema();
        $columns = $exportContainer->getColumns();
        $array = array();
        foreach ($columns as $column) {
            $array[$column->getId()] = $column->getTitle();
        }
        return $array;
    }

    /**
     * 
     * @return \Export\Option\ExportContainer
     */
    public function getExportContainer() {
        $sm = \Core\Functions::getServiceLocator();
        return $sm->get('ExportContainer');
    }

}
