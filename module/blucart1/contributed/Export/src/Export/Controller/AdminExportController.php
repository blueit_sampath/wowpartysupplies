<?php

namespace Export\Controller;

use Zend\View\Model\ViewModel;
use Common\MVC\Controller\AbstractAdminController;
use Export\Option\ExportContainer;

class AdminExportController extends AbstractAdminController {

    protected $_exportContainer = '';

    public function getFormFactory() {
        return $this->getServiceLocator()->get('Export\Form\AdminExport\ExportFactory');
    }

    public function exportAction() {
        $exportContainer = $this->getExportContainer();
        $form = $this->getFormFactory();

        $request = $this->getRequest();
        if ($this->getRequest()->isPost()) {

            $data = $this->getRequest()->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $form->save();
                $columns = $form->get('columns')->getValue();
                $seperator = $form->get('seperator')->getValue();
                return $this->writeFile($columns, $seperator);
            } else {
                $this->notValid();
            }
        } else {
            $form->getRecord();
        }
        $viewModel = new ViewModel(array(
            'form' => $form,
            'exportContainer' => $exportContainer
        ));

        return $viewModel;
    }

    public function writeFile($selectedColumns, $seperator = ',') {

        $exportContainer = $this->getExportContainer();
        $exportContainer->setSelectedColumns($selectedColumns);
        $exportContainer->fetch();
        $array = $exportContainer->getDataToArray();
        $this->arrayToCsvDownload($array, 'export_'.date('d_m_Y').'.csv', $seperator);
        return $exportContainer;
    }

    /**
     *
     * @return ExportContainer
     */
    public function getExportContainer() {
        $eventName = $this->params()->fromRoute('eventName', '');

        if (!$this->_exportContainer) {
            $this->_exportContainer = $this->getServiceLocator()->get('ExportContainer');
            $this->_exportContainer->setEventName($eventName);
            $this->_exportContainer->loadSchema();
        }
        return $this->_exportContainer;
    }

    function arrayToCsvDownload($array, $filename = "export.csv", $delimiter = ";") {
        
        $f = fopen('php://memory', 'w');
        foreach ($array as $line) {
            fputcsv($f, $line, $delimiter);
        }
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="' . $filename . '";');
        fpassthru($f);
        exit;
    }

}
