<?php

namespace Coupon\Controller;

use Common\MVC\Controller\AbstractAdminController;
use Core\Functions;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminCouponController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Coupon\Form\AdminCoupon\CouponFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-coupon-add', array (
				'couponId' => $form->get ( 'id' )->getValue () 
		) );
	}
}