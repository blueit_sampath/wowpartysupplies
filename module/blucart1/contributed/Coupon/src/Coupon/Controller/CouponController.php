<?php

namespace Coupon\Controller;


use Common\MVC\Controller\AbstractFrontController;
use Checkout\Option\CartContainer;

class CouponController extends AbstractFrontController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Coupon\Form\ApplyCoupon\CouponFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'cart');
	}
	
	public function addAction() {
		$form = $this->getFormFactory ();
	
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				return $this->getSaveRedirector ();
			} else {
				$this->addFormErrorMessages($form);
			}
		} else {
			$form->getRecord ();
		}
	//	return array('form' => $form);
		return $this->getSaveRedirector();
	}
	
	public function removeAction() {
		$cart = $this->getCart();
		$cart->removeParam('coupon');
		$cart->removeOutItem('coupon');
		//	return array('form' => $form);
		return $this->getSaveRedirector();
	}
	
	/**
	 * 
	 * @return CartContainer
	 */
	public function getCart(){
		return $this->getServiceLocator()->get('Cart');
	}
	
}