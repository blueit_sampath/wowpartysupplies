<?php

namespace Coupon\Entity;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * CouponOrder
 *
 * @ORM\Table(name="coupon_order")
 * @ORM\Entity
 */
class CouponOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;


    /**
     * @var \Coupon
     *
     * @ORM\ManyToOne(targetEntity="Coupon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="coupon_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $coupon;

    /**
     * @var \OrderMain
     *
     * @ORM\ManyToOne(targetEntity="\OrderMain\Entity\OrderMain")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_main_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $orderMain;
    /**
     *
     * @var \DateTime @ORM\Column(name="created_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="create")
     */
    private $createdDate;
    
    /**
     *
     * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="update")
     */
    private $updatedDate;
    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     *
     */
    public function __get($property) {
    	if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
    		$method_name = "get" .\ucfirst ( $property );
    		return $this->$method_name ();
    	} else {
    			
    		if (is_object ( $this->$property )) {
    			// return $this->$property->id;
    		}
    		return $this->$property;
    	}
    }
    
    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     *
     */
    public function __set($property, $value) {
    	if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
    		$method_name = "set" . \ucfirst ( $property );
    			
    		$this->$method_name ( $value );
    	} else
    		$this->$property = $value;
    }


}
