<?php

namespace Coupon\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Coupon
 *
 * @ORM\Table(name="coupon")
 * @ORM\Entity
 */
class Coupon {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="name", type="string", length=255,
	 *      nullable=true)
	 */
	private $name;
	
	/**
	 *
	 * @var string @ORM\Column(name="coupon", type="string", length=255,
	 *      nullable=true)
	 */
	private $coupon;
	
	/**
	 *
	 * @var integer @ORM\Column(name="usage_limit", type="integer",
	 *      nullable=true)
	 */
	private $usageLimit;
	
	/**
	 *
	 * @var integer @ORM\Column(name="usage_limit_per_user", type="integer",
	 *      nullable=true)
	 */
	private $usageLimitPerUser;
	
	/**
	 *
	 * @var float @ORM\Column(name="min_amount", type="float", nullable=true)
	 */
	private $minAmount;
	
	/**
	 *
	 * @var boolean @ORM\Column(name="is_percentage_discount", type="boolean",
	 *      nullable=true)
	 */
	private $isPercentageDiscount;
	
	/**
	 *
	 * @var float @ORM\Column(name="discount", type="float", nullable=true)
	 */
	private $discount;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="start_date", type="date", nullable=true)
	 */
	private $startDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="end_date", type="date", nullable=true)
	 */
	private $endDate;
	
	/**
	 *
	 * @var boolean @ORM\Column(name="status", type="boolean", nullable=true)
	 */
	private $status;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $property        	
	 * @return mixed
	 *
	 */
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
}
