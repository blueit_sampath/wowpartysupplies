<?php
namespace Coupon\Form\ApplyCoupon;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class CouponForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'coupon',
				'attributes' => array (
						'type' => 'text',
						
				),
				'options' => array (
						'label' => 'Coupon' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Apply Coupon' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}