<?php
namespace Coupon\Form\ApplyCoupon;

use Common\Form\Option\AbstractFormFactory;

class CouponFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Coupon\Form\ApplyCoupon\CouponForm' );
		$form->setName ( 'couponForm' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Coupon\Form\ApplyCoupon\CouponFilter' );
	}
}
