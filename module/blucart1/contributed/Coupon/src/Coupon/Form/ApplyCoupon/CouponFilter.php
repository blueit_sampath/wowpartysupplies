<?php
namespace Coupon\Form\ApplyCoupon;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Callback;
use User\Api\UserApi;
use Coupon\Api\CouponApi;

class  CouponFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
	
		$this->add ( array (
				'name' => 'coupon',
				'required' => true,
				'validators' => array (
						array (
								'name' => 'Callback',
								'options' => array (
										'callback' => array (
												$this,
												'checkCoupon'
										),
										'messages' => array (
												Callback::INVALID_VALUE => "Invalid Coupon",
						
										)
								)
						)
				) 
		) );
		
		
		
		
	}
	
	public function checkCoupon($value) {
	
		$user = UserApi::getLoggedInUser();
		if(!CouponApi::checkCoupon($value, $user)){
			return false;
		}
		return true;
	}
	
} 