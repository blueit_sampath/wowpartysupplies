<?php
namespace Coupon\Form\AdminCoupon;

use Common\Form\Option\AbstractFormFactory;

class CouponFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Coupon\Form\AdminCoupon\CouponForm' );
		$form->setName ( 'adminCouponAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Coupon\Form\AdminCoupon\CouponFilter' );
	}
}
