<?php
namespace Coupon\Form\AdminCoupon;

use Zend\Validator\Callback;

use Coupon\Api\CouponApi;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  CouponFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'name',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags'
						),
						array (
								'name' => 'StringTrim'
						)
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'max' => 255
								)
						)
				)
		) );
		
		$this->add ( array (
				'name' => 'coupon',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags'
						),
						array (
								'name' => 'StringTrim'
						)
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255
								)
						),
						array (
								'name' => 'Callback',
								'options' => array (
										'callback' => array (
												$this,
												'checkCoupon'
										),
										'messages' => array (
												Callback::INVALID_VALUE => "Coupon Code Already Exits",
													
										)
								)
						)
				)
		) );
		
		$this->add ( array (
				'name' => 'usageLimit',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'usageLimitPerUser',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int'
						)
				)
		) );
		
		$this->add ( array (
				'name' => 'minAmount',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Float'
						)
				)
		) );
		$this->add ( array (
				'name' => 'isPercentageDiscount',
				'required' => false,
				
		) );
		$this->add ( array (
				'name' => 'discount',
				'required' => true,
				'validators' => array (
						array (
								'name' => 'Float'
						)
				)
		) );
		$this->add ( array (
				'name' => 'startDate',
				'required' => false,
				
		) );
		$this->add ( array (
				'name' => 'endDate',
				'required' => false,
		
		) );
		
		$this->add ( array (
				'name' => 'status',
				'required' => true,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
	}
	public function checkCoupon($value,$context){
		$entity = CouponApi::getCouponByCoupon($value);
		if($entity){
			if(!$context['id']){
				return false;
			}
			if($context['id'] == $entity->id){
				return true;
			}
			return false;
				
		}
		return true;
	}
} 