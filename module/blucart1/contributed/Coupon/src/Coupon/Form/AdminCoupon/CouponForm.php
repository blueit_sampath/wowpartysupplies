<?php

namespace Coupon\Form\AdminCoupon;

use Config\Api\ConfigApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class CouponForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'coupon',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Coupon Code' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'usageLimit',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Usage Limit' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'usageLimitPerUser',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Usage Per User Limit' 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->add ( array (
				'name' => 'minAmount',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Minimum Amount',
						'prependText' => ConfigApi::getCurrencySymbol() 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'isPercentageDiscount',
				'options' => array (
						'label' => 'Is Percentage Discount',
						'use_hidden_element' => true 
				) 
		), array (
				'priority' => 950 
		) );
		
		$this->add ( array (
				'name' => 'discount',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Discount' 
				) 
		)
		, array (
				'priority' => 940 
		) );
		
		$this->add ( array (
				'name' => 'startDate',
				'attributes' => array (
						'type' => 'text',
						'class' => 'date' 
				),
				'options' => array (
						'label' => 'Start date' 
				) 
		), array (
				'priority' => 930 
		) );
		
		$this->add ( array (
				'name' => 'endDate',
				'attributes' => array (
						'type' => 'text',
						'class' => 'date' 
				),
				'options' => array (
						'label' => 'End date' 
				) 
		), array (
				'priority' => 920 
		) );
		
		$this->getStatus ( 910 );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}