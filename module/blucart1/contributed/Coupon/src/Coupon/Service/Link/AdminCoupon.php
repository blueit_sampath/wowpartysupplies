<?php 
namespace Coupon\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCoupon extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCoupon';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-coupon-add');
		$item = $linkContainer->add ( 'admin-coupon-add', 'Add Coupon', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

