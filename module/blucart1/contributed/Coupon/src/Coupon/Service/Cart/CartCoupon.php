<?php

namespace Coupon\Service\Cart;

use Core\Functions;
use Checkout\Option\CartEvent;
use Product\Api\ProductApi;
use Attribute\Api\AttributeApi;
use Inventory\Api\InventoryApi;
use Coupon\Api\CouponApi;
use User\Api\UserApi;

class CartCoupon extends CartEvent {
	public function prepareCart() {
		$cart = $this->getCart ();
		$param = $cart->getParam ( 'coupon' );
		if (! $param) {
			return;
		}
		$result = CouponApi::getCouponByCoupon ( $param );
		if (! $result) {
			return;
		}
		$price = $cart->getSubTotalPrice ();
		$value = $result->discount;
		if ($result->isPercentageDiscount) {
			$value = $price * $result->discount / 100;
		}
		$value = $value * - 1;
		
		$cart->addQuickOutItem ( 'coupon', 'Discount Coupon', $value, array (
				'couponId' => $result->coupon 
		) );
	}
	public function validateCart() {
		$cart = $this->getCart ();
		$coupon = $cart->getOutItem ( 'coupon' );
		$validateCartContainer = $this->getValidationContainer ();
		if (! $coupon)
			return true;
		$couponCode = $coupon->getParam('couponId');
		$user = UserApi::getLoggedInUser();
		$userId = false;
		if($user){
			$userId = $user->id;
		}
		
		if(!CouponApi::checkCoupon($couponCode, $userId)){
			$validateCartContainer->add ( 'coupon', 'Coupon cannot be applied' );
		}
		
		
	}
}

