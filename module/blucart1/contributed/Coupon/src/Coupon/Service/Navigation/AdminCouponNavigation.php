<?php 
namespace Coupon\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminCouponNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'marketingMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Coupon',
							'route' => 'admin-coupon',
							'id' => 'admin-coupon',
							'iconClass' => 'glyphicon-gift' 
					) 
			) );
		}
	}
}

