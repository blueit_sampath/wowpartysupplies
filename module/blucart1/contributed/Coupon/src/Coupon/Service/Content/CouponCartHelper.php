<?php 
namespace Coupon\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class CouponCartHelper extends AbstractContentEvent {

	protected $_contentTemplate = 'common/coupon-cart-helper';
	protected $_contentName = 'coupon-cart-helper';
	public function getEventName() {
		return 'cartHelper';
	}
	public function getPriority() {
		return 500;
	}
	
}

