<?php

namespace Coupon\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;
use Coupon\Api\CouponApi;
use OrderMain\Api\OrderApi;

class CheckoutCouponSave extends AbstractMainFormEvent {
	public function getFormName() {
		return 'cartCheckout';
	}
	public function getPriority() {
		return 500;
	}
	public function save() {
		$form = $this->getForm ();
		$user = UserApi::getLoggedInUser ();
		$cart = $this->getCart ();
		$em = Functions::getEntityManager ();
		
	
		$orderId = $cart->getParam ( 'orderId' );
		if (! $orderId) {
			return;
		}
		$orderMain = OrderApi::getOrderById ( $orderId );
		if (! $orderMain) {
			return;
		}
	
		
		/* Coupon */
		$couponItem = $cart->getOutItem ( 'coupon' );
		if ($couponItem) {
			$couponOrder = new \Coupon\Entity\CouponOrder ();
			$couponCode = $couponItem->getParam ( 'couponId' );
			$coupon = CouponApi::getCouponByCoupon($couponCode);
			$couponOrder->amount = $couponItem->getValue ();
			$couponOrder->coupon = $coupon;
			$couponOrder->orderMain = $orderMain;
			$em->persist ( $couponOrder );
			$em->flush ();
		}
		
		
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	protected function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
