<?php

namespace Coupon\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use Core\Functions;

class AdminCoupon extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'coupon',
			'usageLimit',
			'usageLimitPerUser',
			'minAmount',
			'isPercentageDiscount',
			'discount',
			'status',
			'startDate',
			'endDate' 
	);
	protected $_entity = '\Coupon\Entity\Coupon';
	protected $_entityName = 'coupon';
	public function getFormName() {
		return 'adminCouponAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		if ($params ['startDate']) {
			$dateTime = new \DateTime ();
			$entity->startDate = $dateTime->setTimestamp ( strtotime ( $params ['startDate'] ) );
		} else {
			$entity->startDate = null;
		}
		if ($params ['endDate']) {
			$dateTime = new \DateTime ();
			$entity->endDate = $dateTime->setTimestamp ( strtotime ( $params ['endDate'] ) );
		} else {
			$entity->endDate = null;
		}
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		if ($entity->startDate) {
			$form->get ( 'startDate' )->setValue ( $entity->startDate->format ( 'Y-m-d' ) );
		}
		if ($entity->endDate) {
			$form->get ( 'endDate' )->setValue ( $entity->endDate->format ( 'Y-m-d' ) );
		}
                $form->get ( 'name' )->setAttribute ( 'readonly',true );
	}
}
