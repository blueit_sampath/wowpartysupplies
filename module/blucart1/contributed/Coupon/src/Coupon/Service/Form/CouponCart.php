<?php

namespace Coupon\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Checkout\Option\CartContainer;

class CouponCart extends AbstractMainFormEvent {
	public function getFormName() {
		return 'couponForm';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$cart = $this->getCart ();
		$form = $this->getForm ();
		$couponCode = $form->get ( 'coupon' )->getValue ();
		$cart->addParam ( 'coupon', $couponCode );
	}
	public function getRecord() {
		$cart = $this->getCart ();
		$form = $this->getForm ();
		$coupon = $cart->getOutItem ( 'coupon' );
		if ($coupon) {
			$couponCode = $coupon->getParam ( 'couponId' );
			$form->get ( 'coupon' )->setValue ( $couponCode );
		}
	}
	/**
	 *
	 * @return CartContainer
	 */
	public function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
