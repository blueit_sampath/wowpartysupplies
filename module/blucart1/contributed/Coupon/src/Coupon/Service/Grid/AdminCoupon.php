<?php

namespace Coupon\Service\Grid;



use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;

use Kendo\Lib\Grid\Option\Toolbar\Toolbar;

use BlucartGrid\Option\ColumnItem;

use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminCoupon extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'coupon',
			'usageLimit',
			'usageLimitPerUser',
			'minAmount',
			'isPercentageDiscount',
			'discount',
			'startDate',
			'endDate',
			'status' 
	)
	;
	protected $_entityName = 'coupon';
	protected $_entity = '\Coupon\Entity\Coupon';
	public function getEventName() {
		return 'adminCoupon';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary(true);
		$columns->addColumn ( 'coupon.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'coupon.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'couponCode' );
		$columnItem->setTitle ( 'Coupon' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'coupon.coupon', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'usageLimit' );
		$columnItem->setTitle ( 'Usage Limit' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'coupon.usageLimit', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'usageLimitPerUser' );
		$columnItem->setTitle ( 'Usage Limit Per User' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'coupon.usageLimitPerUser', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'minAmount' );
		$columnItem->setTitle ( 'Minimum Amount' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:c}' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 950 );
		$columns->addColumn ( 'coupon.minAmount', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'isPercentageDiscount' );
		$columnItem->setTitle ( 'Is Percentage Discount' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 940 );
		$columns->addColumn ( 'coupon.isPercentageDiscount', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'discount' );
		$columnItem->setTitle ( 'Discount' );
		$columnItem->setType ( 'number' );
                $columnItem->setFormat ( '{0:n2}' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 930 );
		$columns->addColumn ( 'coupon.discount', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'startDate' );
		$columnItem->setTitle ( 'Start Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 920 );
		$columnItem->setFormat ( '{0:d}' );
		$columns->addColumn ( 'coupon.startDate', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'endDate' );
		$columnItem->setTitle ( 'End Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 910 );
		$columnItem->setFormat ( '{0:d}' );
		$columns->addColumn ( 'coupon.endDate', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 900 );
		$columns->addColumn ( 'coupon.status', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	
	
	public function beforeUpdate($params, $entity){
		if($params['startDate'] == ""){
			$entity->startDate = null;
		}
		if($params['endDate'] == ""){
			$entity->endDate = null;
		}
	}
	
}
