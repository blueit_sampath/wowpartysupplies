<?php

namespace Coupon\Api;

use Common\Api\Api;
use Core\Functions;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use User\Api\UserApi;

class CouponApi extends Api {
	protected static $_entity = '\Coupon\Entity\Coupon';
	protected static $_entityOrder = '\Coupon\Entity\CouponOrder';
	public static function getCouponById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getCouponByCoupon($couponCode) {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findOneBy ( array (
				'coupon' => $couponCode 
		) );
	}
        public static function getCouponByName($name) {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findOneBy ( array (
				'name' => $name 
		) );
	}
	public static function getCouponOrderByCouponId($couponId) {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entityOrder )->findBy ( array (
				'coupon' => $couponId 
		) );
	}
	public static function getAllCoupons($status = null) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'coupon.id', 'couponId' );
		$queryBuilder->addFrom ( static::$_entity, 'coupon' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'coupon.status', 'couponStatus' );
			$queryBuilder->addParameter ( 'couponStatus', $status );
		}
		return $queryBuilder->executeQuery ();
	}
	public static function getCouponOrderByUserId($userId) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'couponOrder.id', 'couponOrderId' );
		$item = $queryBuilder->addFrom ( static::$_entityOrder, 'couponOrder' );
		
		$queryBuilder->addWhere ( 'user.id', 'userId' );
		$queryBuilder->addParameter ( 'userId', $userId );
		
		$joinItem = new QueryJoinItem ( 'couponOrder.orderMain', 'orderMain' );
		$joinItem2 = new QueryJoinItem ( 'orderMain.user', 'user' );
		$joinItem->addJoin ( $joinItem2 );
		$item->addJoin ( $joinItem );
		
		return $queryBuilder->executeQuery ();
	}
	public static function checkCoupon($value, $userId = false) {
		$sm = Functions::getServiceLocator ();
		$cart = $sm->get ( 'Cart' );
		
		$result = static::getCouponByCoupon ( $value );
		if (! $result) {
			return false;
		}
		if (! $result->status) {
			return false;
		}
		
		if ($result->usageLimit) {
			$couponOrders = static::getCouponOrderByCouponId ( $result->id );
			$count = count ( $couponOrders );
			if ($result->usageLimit < $count) {
				return false;
			}
		}
		
		if ($result->usageLimitPerUser) {
			
			if (! $userId) {
				return false;
			}
			$couponOrders = static::getCouponOrderByUserId ( $userId );
			$count = count ( $couponOrders );
			if ($result->usageLimitPerUser < $count) {
				return false;
			}
		}
		$subTotalPrice = $cart->getSubTotalPrice ();
		if ($result->minAmount) {
			if ($subTotalPrice < $result->minAmount) {
				return false;
			}
		}
		$time = time ();
		if ($result->startDate) {
			if ($result->startDate->getTimeStamp () > $time) {
				return false;
			}
		}
		if ($result->endDate) {
			if ($result->endDate->getTimeStamp () < $time) {
				return false;
			}
		}
		return true;
	}
}
