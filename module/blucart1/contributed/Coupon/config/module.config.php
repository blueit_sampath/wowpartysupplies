<?php

return array (
		'router' => array (
				'routes' => array (
						'admin-coupon' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/coupon',
										'defaults' => array (
												'controller' => 'Coupon\Controller\AdminCoupon',
												'action' => 'index' 
										) 
								) 
						),
						'admin-coupon-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/coupon/add[/ajax/:ajax][/:couponId]',
										'defaults' => array (
												'controller' => 'Coupon\Controller\AdminCoupon',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'couponId' => '[0-9]+',
												'ajax' => 'true' 
										) 
								) 
						),
						'coupon-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/coupon/add',
										'defaults' => array (
												'controller' => 'Coupon\Controller\Coupon',
												'action' => 'add' 
										) 
								) 
						),
						'coupon-remove' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/coupon/remove',
										'defaults' => array (
												'controller' => 'Coupon\Controller\Coupon',
												'action' => 'remove' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'Coupon\Service\Grid\AdminCoupon' => 'Coupon\Service\Grid\AdminCoupon',
				'Coupon\Service\Form\AdminCoupon' => 'Coupon\Service\Form\AdminCoupon',
				'Coupon\Service\Link\AdminCoupon' => 'Coupon\Service\Link\AdminCoupon',
				'Coupon\Service\Navigation\AdminCouponNavigation' => 'Coupon\Service\Navigation\AdminCouponNavigation',
				'Coupon\Service\Form\CouponCart' => 'Coupon\Service\Form\CouponCart',
				'Coupon\Service\Content\CouponCartHelper' => 'Coupon\Service\Content\CouponCartHelper',
				'Coupon\Service\Cart\CartCoupon' => 'Coupon\Service\Cart\CartCoupon',
				'Coupon\Service\Form\CheckoutCouponSave' => 'Coupon\Service\Form\CheckoutCouponSave' 
		),
		'service_manager' => array (
				'invokables' => array (
						'Coupon\Service\Grid\AdminCoupon' => 'Coupon\Service\Grid\AdminCoupon',
						'Coupon\Form\AdminCoupon\CouponForm' => 'Coupon\Form\AdminCoupon\CouponForm',
						'Coupon\Form\AdminCoupon\CouponFilter' => 'Coupon\Form\AdminCoupon\CouponFilter',
						'Coupon\Service\Form\AdminCoupon' => 'Coupon\Service\Form\AdminCoupon',
						'Coupon\Service\Link\AdminCoupon' => 'Coupon\Service\Link\AdminCoupon',
						'Coupon\Service\Navigation\AdminCouponNavigation' => 'Coupon\Service\Navigation\AdminCouponNavigation',
						'Coupon\Form\ApplyCoupon\CouponForm' => 'Coupon\Form\ApplyCoupon\CouponForm',
						'Coupon\Form\ApplyCoupon\CouponFilter' => 'Coupon\Form\ApplyCoupon\CouponFilter',
						'Coupon\Service\Form\CouponCart' => 'Coupon\Service\Form\CouponCart',
						'Coupon\Service\Content\CouponCartHelper' => 'Coupon\Service\Content\CouponCartHelper',
						'Coupon\Service\Cart\CartCoupon' => 'Coupon\Service\Cart\CartCoupon',
						'Coupon\Service\Form\CheckoutCouponSave' => 'Coupon\Service\Form\CheckoutCouponSave' 
				),
				'factories' => array (
						'Coupon\Form\AdminCoupon\CouponFactory' => 'Coupon\Form\AdminCoupon\CouponFactory',
						'Coupon\Form\ApplyCoupon\CouponFactory' => 'Coupon\Form\ApplyCoupon\CouponFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'Coupon\Controller\AdminCoupon' => 'Coupon\Controller\AdminCouponController',
						'Coupon\Controller\Coupon' => 'Coupon\Controller\CouponController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'Coupon' => __DIR__.'/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						'Coupon_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__.'/../src/Coupon/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										'Coupon\Entity' => 'Coupon_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'Coupon' => __DIR__.'/../public' 
						) 
				) 
		) 
);
