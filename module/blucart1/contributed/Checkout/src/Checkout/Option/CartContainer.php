<?php
namespace Checkout\Option;

use Core\Item\Container\ItemContainer;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Checkout\Option\Validation\ValidationContainer;
use Core\Functions;

class CartContainer extends ItemContainer implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{

    protected $_cartItems = array();

    protected $_params = array();

    protected $_inItems = array();

    protected $_outItems = array();

    const CART_PREVIEW = 'cartPreview';

    const CART_COMPLETE = 'cartComplete';

    const CART_DISPLAY = 'cartDisplay';

    const VALIDATE_CART = 'validate-cart';

    const VALIDATE_CART_ITEM = 'validate-cart-item';

    const ADD_CART_ITEM = 'add-cart-item';

    const UPDATE_CART_ITEM = 'update-cart-item';

    const PREPARE_CART_ITEM = 'prepare-cart-item';

    const REMOVE_CART_ITEM = 'remove-cart-item';

    const PREPARE_CART = 'prepare-cart';

    const CLEAR_CART = 'clear-cart';

    const CART_SESSION_NAME = 'blucart';

    protected $_services;

    protected $_events;

    protected $_cartType;

    public function __construct()
    {
        $this->loadSession();
    }

    public function getSession()
    {
        $session = new \Zend\Session\Container(self::CART_SESSION_NAME);
        
        return $session;
    }

    public function clearCart()
    {
        $array = array(
            'cartItems',
            'inItems',
            'outItems',
            'params'
        );
        foreach ($array as $value) {
            $f = '_' . $value;
            $this->$f = null;
        }
        
        $this->save();
    }

    public function save()
    {
        $this->prepare();
        $session = $this->getSession();
        $array = array(
            'cartItems',
            'inItems',
            'outItems',
            'params'
        );
        foreach ($array as $value) {
            $f = '_' . $value;
            $session->$value = $this->$f;
        }
        return $session;
    }

    public function loadSession()
    {
        $session = $this->getSession();
        $array = array(
            'cartItems',
            'inItems',
            'outItems',
            'params'
        );
        foreach ($array as $value) {
            $f = '_' . $value;
            
            if (! isset($session->$value)) {
                $session->$value = array();
            }
            $this->$f = $session->$value;
        }
        
        return $session;
    }

    public function prepare($type = self::CART_DISPLAY)
    {
        $this->setCartType($type);
        $this->prepareCartEvent();
    }

    public function isValid()
    {
        $this->prepareCartEvent();
        $validateContainer = $this->validateCartEvent();
        if (! $validateContainer->isValid()) {
            $validateContainer->displayMessages();
            return false;
        }
        return true;
    }

    /**
     *
     * @return multitype:CartItem
     */
    public function getCartItems()
    {
        $cartItems = $this->sortArray($this->_cartItems);
        if ($cartItems) {
            foreach ($cartItems as $key => $cartItem) {
                $cartItems[$key] = $this->prepareCartItemEvent($cartItem);
            }
        }
        
        return $cartItems;
    }

    public function setCartItems($_cartItems)
    {
        $this->_cartItems = $_cartItems;
        
        return $this;
    }

    /**
     *
     * @param CartItem $item            
     * @return CartItem
     */
    public function addCartItem(CartItem $item, $preview = false)
    {
        $this->prepareCartItemEvent($item);
        $generatedKey = $item->getGeneratedKey();
        
        if ($preview) {
            $this->setCartType(self::CART_PREVIEW);
            $validateContainer = $this->validateCartItemEvent($item);
            $validateContainer->displayMessages();
            return $item;
        }
        
        if ($existingCartItem = $this->getCartItem($generatedKey)) {
            $clonedItem = clone $existingCartItem;
            $clonedItem->merge($item);
            $validateContainer = $this->validateCartItemEvent($clonedItem);
            if (! $validateContainer->isValid()) {
                $validateContainer->displayMessages();
                return false;
            }
            $existingCartItem->merge($item);
            $this->updateCartItemEvent($existingCartItem);
        } else {
            $validateContainer = $this->validateCartItemEvent($item);
            if (! $validateContainer->isValid()) {
                $validateContainer->displayMessages();
                return false;
            }
            
            $this->_cartItems[$generatedKey] = $item;
            $this->addCartItemEvent($item);
        }
        return $this->_cartItems[$generatedKey];
    }

    public function getCartItem($key)
    {
        if (isset($this->_cartItems[$key])) {
            $cartItem = $this->_cartItems[$key];
            $this->prepareCartItemEvent($cartItem);
            return $cartItem;
        }
        return false;
    }

    public function removeCartItem($key)
    {
        if ($cartItem = $this->getCartItem($key)) {
            $this->removeCartItemEvent($cartItem);
            unset($this->_cartItems[$key]);
            return true;
        }
        
        $this->save();
        return false;
    }

    public function updateQuantity($cartItemId, $quantity)
    {
        $item = $this->getCartItem($cartItemId);
        if ($item) {
            $q1 = $item->getQuantity();
            $item->setQuantity($quantity);
            $validateContainer = $this->validateCartItemEvent($item);
            if (! $validateContainer->isValid()) {
                $validateContainer->displayMessages();
                $item->setQuantity($q1);
                return false;
            }
        }
        return $item;
    }

    /**
     *
     * @return multitype:CartItem
     */
    public function getInItems()
    {
        return $this->sortArray($this->_inItems);
    }

    /**
     *
     * @param array $_inItems            
     * @return \Checkout\Option\CartContainer
     */
    public function setInItems($_inItems)
    {
        $this->_inItems = $_inItems;
        return $this;
    }

    /**
     *
     * @param CartInItem $item            
     * @return \Checkout\Option\CartContainer
     */
    public function addInItem(CartInItem $item)
    {
        $this->_inItems[$item->getId()] = $item;
        
        return $this;
    }

    /**
     *
     * @param string $id            
     * @param string $title            
     * @param string $value            
     * @param array $params            
     * @return \Checkout\Option\CartInItem
     */
    public function addQuickInItem($id, $title = '', $value = '', $params = array())
    {
        $item = new CartInItem($id);
        $item->setTitle($title);
        $item->setValue($value);
        $item->setParams($params);
        $this->addInItem($item);
        return $item;
    }

    /**
     *
     * @return CartInItem boolean
     */
    public function getInItem($key)
    {
        if (isset($this->_inItems[$key])) {
            return $this->_inItems[$key];
        }
        return false;
    }

    /**
     *
     * @param string $key            
     * @return boolean
     */
    public function removeInItem($key)
    {
        if (isset($this->_inItems[$key])) {
            unset($this->_inItems[$key]);
            return true;
        }
        return false;
    }

    /**
     *
     * @return multitype:CartOutItem
     */
    public function getOutItems()
    {
        return $this->sortArray($this->_outItems);
    }

    /**
     *
     * @param unknown $_outItems            
     * @return \Checkout\Option\CartContainer
     */
    public function setOutItems($_outItems)
    {
        $this->_outItems = $_outItems;
        return $this;
    }

    /**
     *
     * @param CartOutItem $item            
     * @return \Checkout\Option\CartContainer
     */
    public function addOutItem(CartOutItem $item)
    {
        $this->_outItems[$item->getId()] = $item;
        return $this;
    }

    /**
     *
     * @param string $id            
     * @param string $title            
     * @param string $value            
     * @param array $params            
     * @return \Checkout\Option\CartOutItem
     */
    public function addQuickOutItem($id, $title = '', $value = '', $params = array())
    {
        $item = new CartOutItem($id);
        $item->setTitle($title);
        $item->setValue($value);
        $item->setParams($params);
        $this->addOutItem($item);
        return $item;
    }

    /**
     *
     * @param string $key            
     * @return CartOutItem boolean
     */
    public function getOutItem($key)
    {
        if (isset($this->_outItems[$key])) {
            return $this->_outItems[$key];
        }
        return false;
    }

    /**
     *
     * @param string $key            
     * @return boolean
     */
    public function removeOutItem($key)
    {
        if (isset($this->_outItems[$key])) {
            unset($this->_outItems[$key]);
            return true;
        }
        return false;
    }

    /**
     *
     * @return string $_params
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     *
     * @param array $_params            
     * @return \Checkout\Option\CartContainer
     */
    public function setParams($_params)
    {
        $this->_params = $_params;
        return $this;
    }

    /**
     *
     * @param string $key            
     * @param multitype $value            
     * @return \Checkout\Option\CartContainer
     */
    public function addParam($key, $value)
    {
        $this->_params[$key] = $value;
        
        return $this;
    }

    /**
     *
     * @param string $key            
     * @return multitype: boolean
     */
    public function getParam($key)
    {
        if (isset($this->_params[$key])) {
            return $this->_params[$key];
        }
        return false;
    }

    /**
     *
     * @param unknown $key            
     * @return boolean
     */
    public function removeParam($key)
    {
        if (isset($this->_params[$key])) {
            unset($this->_params[$key]);
            return true;
        }
        return false;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->_services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->_services->getServiceLocator();
    }

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    public function getEventManager()
    {
        if (null === $this->_events) {
            $this->setEventManager(new EventManager());
        }
        return $this->_events;
    }

    /**
     *
     * @return \Checkout\Option\CartContainer
     */
    public function clearCartEvent()
    {
        $params = array();
        $this->getEventManager()->trigger(self::CLEAR_CART, $this, $params);
        return $this;
    }

    /**
     *
     * @param CartItem $cartItem            
     * @return CartItem
     */
    public function removeCartItemEvent($cartItem)
    {
        $params = array(
            'cartItem' => $cartItem
        );
        $this->getEventManager()->trigger(self::REMOVE_CART_ITEM, $this, $params);
        return $cartItem;
    }

    /**
     *
     * @param CartItem $cartItem            
     * @return CartItem
     */
    public function updateCartItemEvent($cartItem)
    {
        $params = array(
            'cartItem' => $cartItem
        );
        $this->getEventManager()->trigger(self::UPDATE_CART_ITEM, $this, $params);
        return $cartItem;
    }

    /**
     *
     * @param CartItem $cartItem            
     * @return CartItem
     */
    public function addCartItemEvent($cartItem)
    {
        $params = array(
            'cartItem' => $cartItem
        );
        $this->getEventManager()->trigger(self::ADD_CART_ITEM, $this, $params);
        return $cartItem;
    }

    /**
     *
     * @return \Checkout\Option\CartContainer
     */
    public function validateCartEvent()
    {
        $validationContainer = new ValidationContainer();
        $params = array(
            'validationContainer' => $validationContainer
        );
        $this->getEventManager()->trigger(self::VALIDATE_CART, $this, $params);
        return $validationContainer;
    }

    /**
     *
     * @param CartItem $cartItem            
     * @return CartItem
     */
    public function validateCartItemEvent($cartItem)
    {
        $validationContainer = new ValidationContainer();
        $params = array(
            'cartItem' => $cartItem,
            'validationContainer' => $validationContainer
        );
        $this->getEventManager()->trigger(self::VALIDATE_CART_ITEM, $this, $params);
        return $validationContainer;
    }

    /**
     *
     * @param CartItem $cartItem            
     * @return CartItem
     */
    public function prepareCartItemEvent($cartItem)
    {
        $params = array(
            'cartItem' => $cartItem
        );
        $this->getEventManager()->trigger(self::PREPARE_CART_ITEM, $this, $params);
        return $cartItem;
    }

    /**
     *
     * @return \Checkout\Option\CartContainer
     */
    public function prepareCartEvent()
    {
        $params = array();
        $this->getEventManager()->trigger(self::PREPARE_CART, $this, $params);
        return $this;
    }

    public function getSubTotalPrice()
    {
        $price = 0;
        
        $cartItems = $this->getCartItems();
        if ($cartItems) {
            foreach ($cartItems as $cartItem) {
                $price = $price + $cartItem->getTotalPrice();
            }
        }
        $inItmes = $this->getInItems();
        if ($inItmes) {
            foreach ($inItmes as $inItem) {
                $value = $inItem->getValue();
                if ($inItem->getIncludePrice()) {
                    if (is_numeric($value)) {
                        $price = $price + $value;
                    }
                }
            }
        }
        return $price;
    }

    public function getTotalPrice()
    {
        $price = $this->getSubTotalPrice();
        $outItems = $this->getOutItems();
        if ($outItems) {
            foreach ($outItems as $outItem) {
                $value = $outItem->getValue();
                if ($outItem->getIncludePrice()) {
                    if (is_numeric($value)) {
                        $price = $price + $value;
                    }
                }
            }
        }
        if ($price < 0) {
            $price = 0;
        }
        return $price;
    }

    /**
     *
     * @return string $_cartType
     */
    public function getCartType()
    {
        return $this->_cartType;
    }

    /**
     *
     * @param field_type $_cartType            
     * @param
     *            class_name
     */
    public function setCartType($_cartType)
    {
        $this->_cartType = $_cartType;
        return $this;
    }
}
