<?php

namespace Checkout\Option\Validation;

use Core\Item\Container\ItemContainer;
use Core\Functions;

class ValidationContainer extends ItemContainer {
	const ERROR = 'error';
	const SUCCESS = 'success';
	const WARNING = 'warning';
	const INFO = 'info';
	
	/**
	 *
	 * @param string $uniqueKey        	
	 * @param string $message        	
	 * @param string $errorType        	
	 * @param integer $weight        	
	 * @return \Checkout\Option\Validation\ValidationItem
	 */
	public function add($uniqueKey, $message, $messageType = self::ERROR, $weight = null, $validState = false) {
		$item = new ValidationItem ( $uniqueKey );
		
		$item->setMessageType ( $messageType );
		$item->setMessage ( $message );
		$item->setWeight ( $weight );
		$item->setValidState ( $validState );
		$this->addItem ( $item );
		return $item;
	}
	
	public function displayMessages(){
		foreach($this->getItems() as $item){
			if($message = $item->getMessage()){
				Functions::addMessage($message, $item->getMessageType());
			}
		}
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return \Checkout\Option\Validation\ValidationItem
	 */
	public function get($id) {
		return $this->getItem ( $id );
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isValid() {
		$items = $this->getItems ();
		$result = true;
		foreach ( $items as $item ) {
			$result = ($result && $item->getValidState ());
		}
		return $result;
	}
}
