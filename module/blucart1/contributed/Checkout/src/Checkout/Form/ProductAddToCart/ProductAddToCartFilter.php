<?php

namespace Checkout\Form\ProductAddToCart;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductAddToCartFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'quantity',
				'required' => true,
				'validators' => array (
						array (
								'name' => 'Int' 
						),
						array (
								'name' => 'greater_than',
								'options' => array ('inclusive' => true,'min' => 1)) 
						) 
				) 
		) ;
	}
} 