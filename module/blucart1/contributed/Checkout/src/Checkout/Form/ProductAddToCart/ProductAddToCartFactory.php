<?php
namespace Checkout\Form\ProductAddToCart;

use Common\Form\Option\AbstractFormFactory;

class ProductAddToCartFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Checkout\Form\ProductAddToCart\ProductAddToCartForm' );
		$form->setName ( 'productAddToCartForm' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Checkout\Form\ProductAddToCart\ProductAddToCartFilter' );
	}
}
