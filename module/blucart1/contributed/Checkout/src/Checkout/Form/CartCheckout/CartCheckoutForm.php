<?php

namespace Checkout\Form\CartCheckout;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use Config\Api\ConfigApi;
use LocaleElement\Form\Element\CheckboxHtmlText;

class CartCheckoutForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'comment',
				'attributes' => array (
						'type' => 'textarea' 
				),
				'options' => array (
						'label' => 'Comment' 
				) 
		), array (
				'priority' => 100 
		) );
		
		$value = ConfigApi::getConfigByKey ( 'ORDER_TC' );
		if ($value) {
			$checkbox = new CheckboxHtmlText ( 'accept' );
			$checkbox->setHtmlText ( $value );
			$this->add ( $checkbox, array (
					'priority' => 90 
			) );
		}
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}