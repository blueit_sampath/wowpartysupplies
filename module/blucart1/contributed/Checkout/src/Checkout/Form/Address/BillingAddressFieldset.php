<?php

namespace Checkout\Form\Address;

use Zend\Form\Element\Select;
use Config\Api\ConfigApi;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

use LocaleElement\Form\Element\CountryPicker;
use LocaleElement\Form\Element\StatePicker;
use Zend\Validator\Regex;

class BillingAddressFieldset extends ShippingAddressFieldset {

    public function __construct($name = 'billingAddress') {
        parent::__construct($name);

        $address = $this->get('formattedAddress');
        $address->setAttribute('data-details', 'fieldset.shippingAddress');
        $this->remove('copyAddress');
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array \
     */
    public function getInputFilterSpecification() {
        $shippingAddress = \Core\Functions::fromPost('shippingAddress');
        if (!($shippingAddress && $shippingAddress['copyAddress'])) {
            return parent::getInputFilterSpecification();
        }
        $array = array();
        foreach ($this->getElements() as $element) {
            $array[$element->getName()] = array(
                'required' => false
            );
        }
        return $array;
    }

}
