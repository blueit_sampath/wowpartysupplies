<?php

namespace Checkout\Form\Address;

use Zend\Form\Element\Select;
use Config\Api\ConfigApi;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use LocaleElement\Form\Element\CountryPicker;
use LocaleElement\Form\Element\StatePicker;
use Zend\Validator\Regex;

class ShippingAddressFieldset extends Fieldset implements InputFilterProviderInterface {

    public function __construct($name = 'shippingAddress') {
        parent::__construct($name);
 
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));


        $this->add(array(
            'name' => 'firstName',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'First Name'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'lastName',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Last Name'
            )
                ), array(
            'priority' => 990
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Email'
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'name' => 'contactNumber',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Contact Number'
            )
                ), array(
            'priority' => 970
        ));
        $formattedAddress = new \LocaleElement\Form\Element\AddressPicker('formattedAddress');
        $formattedAddress->setAttribute('placeholder', 'Type in an Address eg. 119 Bawdsey Avenue London');
        $formattedAddress->setAttribute('data-details', 'fieldset.shippingAddress');
        $this->add($formattedAddress, array(
            'priority' => 960
        ));
        $this->add(array(
            'name' => 'addressLine1',
            'attributes' => array(
                'type' => 'text',
                'data-geo' => 'street_number'
            ),
            'options' => array(
                'label' => 'Address Line 1'
            )
                ), array(
            'priority' => 950
        ));

        $this->add(array(
            'name' => 'addressLine2',
            'attributes' => array(
                'type' => 'text',
                'data-geo' => 'route'
            ),
            'options' => array(
                'label' => 'Address Line 2'
            )
                ), array(
            'priority' => 940
        ));

        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'data-geo' => 'locality'
            ),
            'options' => array(
                'label' => 'City'
            )
                ), array(
            'priority' => 930
        ));

        $country = new CountryPicker('country');
        $country->setLabel('Country');
        $country->setAttribute('data-geo', 'country_short');
        $this->add($country, array(
            'priority' => 920
        ));

        $this->add(array(
            'name' => 'state',
            'attributes' => array(
                'type' => 'text',
                'data-geo' => 'administrative_area_level_1'
            ),
            'options' => array(
                'label' => 'State/County'
            )
                ), array(
            'priority' => 910
        ));
//		$state = new StatePicker ( 'state' );
//		$state->setLabel ( 'State' );
//		$state->setCountry ( 'billingAddress[country]' );
//		$this->add ( $state, array (
//				'priority' => 920 
//		) );
//		
        $this->add(array(
            'name' => 'postcode',
            'attributes' => array(
                'type' => 'text',
                'data-geo' => 'postal_code'
            ),
            'options' => array(
                'label' => 'Postcode'
            )
                ), array(
            'priority' => 900
        ));
        $checkbox = new \LocaleElement\Form\Element\CheckboxHtmlText('copyAddress');
        $checkbox->setHtmlText('billing address is same as above');
        $this->add($checkbox, array(
            'priority' => 900
        ));
       
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array \
     */
    public function getInputFilterSpecification() {
        return array(
            'formattedAddress' => array(
                'required' => false,
            ),
            'firstName' => array(
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    ),
                    array(
                        'name' => 'StripTags'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 255
                        )
                    ),
                    array(
                        'name' => 'regex',
                        'options' => array(
                            'pattern' => "/^[a-zA-Z ,.'-]+$/",
                            'messages' => array(
                                Regex::NOT_MATCH => 'Use Letters & periods'
                            )
                        )
                    )
                )
            ),
            'lastName' => array(
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    ),
                    array(
                        'name' => 'StripTags'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 255
                        )
                    ),
                    array(
                        'name' => 'regex',
                        'options' => array(
                            'pattern' => "/^[a-zA-Z ,.'-]+$/",
                            'messages' => array(
                                Regex::NOT_MATCH => 'Use Letters & periods'
                            )
                        )
                    )
                )
            ),
            'email' => array(
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'EmailAddress'
                    )
                )
            ),
            'contactNumber' => array(
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'regex',
                        'options' => array(
                            'pattern' => '/^(?!.*-.*-.*-)(?=(?:\d{8,13}$)|(?:(?=.{9,13}$)[^-]*-[^-]*$)|(?:(?=.{10,13}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/',
                            'messages' => array(
                                Regex::NOT_MATCH => 'Invalid Phone number'
                            )
                        )
                    )
                )
            ),
            'addressLine1' => array(
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    ),
                    array(
                        'name' => 'StripTags'
                    )
                )
            ),
            'addressLine2' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    ),
                    array(
                        'name' => 'StripTags'
                    )
                )
            ),
            'city' => array(
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    ),
                    array(
                        'name' => 'StripTags'
                    )
                )
            ),
            'state' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    ),
                    array(
                        'name' => 'StripTags'
                    )
                )
            ),
            'country' => array(
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'LocaleElement\Form\Filter\Country'
                    ),
                    array(
                        'name' => 'StringTrim'
                    ),
                    array(
                        'name' => 'StripTags'
                    )
                )
            ),
            'copyAddress' => array(
                'required' => false
            ),
            'postcode' => array(
                'name' => 'postcode',
                'required' => true,
                'filters' => array(
                    array(
                        'name' => 'StringTrim'
                    ),
                    array(
                        'name' => 'StripTags'
                    )
                )
            )
                )
        ;
    }

}
