<?php
namespace Checkout\Form\Address;

use Common\Form\Option\AbstractFormFactory;

class AddressFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Checkout\Form\Address\AddressForm' );
		$form->setName ( 'checkoutAddress' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Checkout\Form\Address\AddressFilter' );
	}
}
