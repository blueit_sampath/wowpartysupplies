<?php

namespace Checkout\Form\Address;

use Common\Form\Form;

class AddressForm extends Form {

    public function init() {

        $this->add(array(
            'name' => 'shippingAddress',
            'type' => 'Checkout\Form\Address\ShippingAddressFieldset',
            
            'attributes' => array(
                'class' => 'shippingAddress',
            )
                ), array(
            'priority' => 1000
        ));
        $this->add(array(
            'name' => 'billingAddress',
            'type' => 'Checkout\Form\Address\BillingAddressFieldset',
            'attributes' => array(
                'class' => 'billingAddress',
            )
                ), array(
            'priority' => 900
        ));


        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Continue'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
