<?php

namespace Checkout\Controller;

use Common\MVC\Controller\AbstractFrontController;
use Checkout\Option\CartContainer;
use Zend\View\Model\ViewModel;
use User\Api\UserApi;

class CheckoutController extends AbstractFrontController {

    /**
     *
     * @return CartContainer
     */
    public function getCart() {
        return $this->getServiceLocator()->get('Cart');
    }

    public function checkoutAction() {

        $cart = $this->getCart();
        if (!$cart->isValid()) {
            return $this->redirect()->toRoute('cart');
        }

        $cart = $this->getCart();
        $cartItems = $cart->getCartItems();
        if (!$cartItems || !count($cartItems)) {
            return $this->redirect()->toRoute('cart');
        }

        $user = UserApi::getLoggedInUser();
        $url = null;
        $defaultAddress = null;
        if ($user) {
            $billingAddress = $cart->getParam('billingAddress');
            if (!$billingAddress) {
                $defaultAddress = UserApi::getDefaultAddressByUserId($user->id);
                if (!$defaultAddress) {
                    return $this->redirect()->toRoute('checkout-step2');
                }
                $this->loadShippingAndBillingAddress($defaultAddress);
            }
        } else {
            $billingAddress = $cart->getParam('billingAddress');
            if (!($billingAddress && count($billingAddress))) {
                return $this->redirect()->toRoute('checkout-step1');
            }
        }
        $form = $this->getServiceLocator()->get('Checkout\Form\CartCheckout\CartCheckoutFactory');
// 		$data = $cart->getParam('dataPreview' );
// 		if($data){
// 		$form->setData($data);
// 		}
        if ($this->getRequest()->isPost()) {

            $form->setData($this->params()->fromPost());
            if ($form->isValid()) {
                $form->save();
                $data = $form->getData();
            } else {
                $this->addFormErrorMessages($form);
            }
        } else {
            $form->getRecord();
        }

        return array(
            'url' => $url,
            'form' => $form
        );
    }

    public function step1Action() {
        $data = array();
        if ($this->getRequest()->isPost()) {
            $params = $this->params()->fromPost();
            if ($params ['checkoutOptions'] == 'guest') {
                return $this->redirect()->toRoute('checkout-step2');
            } else {
                return $this->redirect()->toRoute('checkout-register');
            }
        }
        $viewModal = new ViewModel($data);

        return $viewModal;
    }

    public function shippingbillingModalAction() {
        $form = $this->getServiceLocator()->get('Checkout\Form\Address\AddressFactory');
        $viewModel = new ViewModel(array(
            'form' => $form
                ));
        return $viewModel;
    }

    public function step2Action() {
        $form = $this->getServiceLocator()->get('Checkout\Form\Address\AddressFactory');

        if ($this->getRequest()->isPost()) {
            $form->setData($this->params()->fromPost());
            if ($form->isValid()) {
                $form->save();
                return $this->redirect()->toRoute('checkout');
            } else {
                $this->notValid();
            }
        } else {
            $form->getRecord();
        }
        $viewModel = new ViewModel(array(
            'form' => $form
                ));
        return $viewModel;
    }

    public function editaddressAction() {
        $form = $this->getServiceLocator()->get('Checkout\Form\EditCheckoutAddress\EditCheckoutAddressFactory');
        $isSave = false;
        if ($this->getRequest()->isPost()) {
            $form->setData($this->params()->fromPost());
            if ($form->isValid()) {
                $form->save();
                $isSave = true;
            } else {
                $this->notValid();
            }
        } else {
            $form->getRecord();
        }
        return array(
            'form' => $form,
            'isSave' => $isSave
                );
    }

    public function loadShippingAndBillingAddress($defaultAddress) {
        $array = array(
            'id',
            'firstName',
            'lastName',
            'email',
            'contactNumber',
            'city',
            'country',
            'state',
            'addressLine1',
            'addressLine2',
            'postcode'
        );
        $temp = array();

        foreach ($array as $key) {
            $temp[$key] = $defaultAddress->$key;
        }
        $cart = $this->getCart();
        $cart->addParam('shippingAddress', $temp);
        $cart->addParam('billingAddress', $temp);
    }

    public function cartPreviewAction() {
        $form = $this->getServiceLocator()->get('Checkout\Form\CartCheckout\CartCheckoutFactory');
        $cart = $this->getCart();
        if ($this->getRequest()->isPost()) {
            $form->setData($this->params()->fromPost());
            $form->isValid();
            $data = $form->getData();

            $cart->addParam('dataPreview', $data);
            $cart->save();
        }
        $viewModel = new ViewModel(array(
            'form' => $form
                ));
        $viewModel = new ViewModel ();
        $viewModel->setTerminal(true);
        return $viewModel;
    }

}
