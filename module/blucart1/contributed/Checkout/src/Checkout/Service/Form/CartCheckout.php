<?php

namespace Checkout\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Checkout\Option\CartContainer;

class CartCheckout extends AbstractMainFormEvent {
	public function getFormName() {
		return 'cartCheckout';
	}
	public function getPriority() {
		return 1000;
	}
	public function getRecord() {
		$cart = $this->getCart ();
		$form = $this->getForm ();
		$data = $cart->getParam ( 'dataPreview' );
		if ($data) {
			$form->setData ( $data );
		}
	}
	public function save() {
		$cart = $this->getCart ();
		$form = $this->getForm ();
		$data = $form->getData ();
		$cart->addParam ( 'formData', $data );
		$cart->prepare(CartContainer::CART_COMPLETE);
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	protected function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
