<?php

namespace Checkout\Service\Navigation;

use Common\Navigation\Event\NavigationEvent;

class CheckoutNavigation extends NavigationEvent {

    public function breadcrumb() {
        parent::breadcrumb();
        $page = array(
            'label' => 'Cart',
            'route' => 'cart',
            'id' => 'cart'
        );

        $this->addDefaultMenu($page);

        $page = array(
            'label' => 'Checkout',
            'route' => 'checkout',
            'id' => 'checkout'
        );

        $this->findPagesAndAddMenuByUrl($this->getUrl('cart'), $page);

        $page = array(
            'label' => 'Checkout',
            'route' => 'checkout-step1',
            'id' => 'checkout-step1'
        );

        $this->findPagesAndAddMenuByUrl($this->getUrl('cart'), $page);
    }

}
