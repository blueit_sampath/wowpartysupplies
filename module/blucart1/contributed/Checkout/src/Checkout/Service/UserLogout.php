<?php

namespace Checkout\Service;

use Core\Functions;
use Doctrine\ORM\EntityManager;
use Common\Form\Form;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

class UserLogout extends AbstractEvent {

    public function attach(EventManagerInterface $events) {
        $sharedEventManager = $events->getSharedManager();
        $this->_listeners[] = $sharedEventManager->attach('*', 'user-logout', array(
            $this,
            'clearSessionAddress'
                ), 500);
        $this->_listeners[] = $sharedEventManager->attach('*', 'user-login', array(
            $this,
            'clearSessionAddress'
                ), 500);


        return $this;
    }

    public function clearSessionAddress() {
        $sm = Functions::getServiceLocator();
        $cart = $sm->get('Cart');
        $cart->removeParam('billingAddress');
        $cart->removeParam('shippingAddress');
    }

}
