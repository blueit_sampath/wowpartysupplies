<?php 
namespace Checkout\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class LoadMiniCart extends AbstractBlockEvent {

	protected $_blockTemplate = 'load-mini-cart';

	public function getBlockName() {
		return 'loadMiniCart';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'Mini Cart';
	}
	
}

