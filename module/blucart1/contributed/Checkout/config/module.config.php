<?php

return array(
    'router' => array(
        'routes' => array(
            'addtocart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/addtocart[/cartItem/:cartItemId][/product/:productId][/quantity/:quantity][/option/:option][/image/:image][/editCartItem/:editCartItemId][/isAjax/:isAjax]',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Cart',
                        'action' => 'addtocart',
                        'quantity' => 1
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'quantity' => '[0-9]+',
                        'cartItemId' => '[a-zA-Z0-9]+',
                        'editCartItemId' => '[a-zA-Z0-9]+'
                    )
                )
            ),
            'addtocart-preview' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/addtocart[/cartItem/:cartItemId][/product/:productId][/quantity/:quantity][/option/:option][/image/:image][/editCartItem/:editCartItemId][/isAjax/:isAjax]/preview/true',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Cart',
                        'action' => 'preview',
                        'quantity' => 1
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'quantity' => '[0-9]+',
                        'cartItemId' => '[a-zA-Z0-9]+',
                        'editCartItemId' => '[a-zA-Z0-9]+'
                    )
                )
            ),
            'quickaddtocart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quickaddtocart[/product/:productId][/editCartItem/:editCartItemId][/isAjax/:isAjax][/preview/:preview]',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Cart',
                        'action' => 'quickaddtocart'
                    )
                )
            ),
            'removecartitem' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/removecartitem[/cartItem/:cartItemId]',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Cart',
                        'action' => 'removecartitem',
                        'quantity' => 1
                    ),
                    'constraints' => array(
                        'cartItemId' => '[a-zA-Z0-9]+'
                    )
                )
            ),
            'updatequantity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/updatequantity[/cartItem/:cartItemId]',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Cart',
                        'action' => 'updatequantity'
                    ),
                    'constraints' => array(
                        'cartItemId' => '[a-zA-Z0-9]+'
                    )
                )
            ),
            'cart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cart',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Cart',
                        'action' => 'cart'
                    )
                )
            ),
            'cart-loadminicart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cart/loadmini',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Cart',
                        'action' => 'loadmini'
                    )
                )
            ),
            'checkout' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/checkout',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Checkout',
                        'action' => 'checkout'
                    )
                )
            ),
            'checkout-shippingbilling-modal' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/checkout/shippingbilling/modal',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Checkout',
                        'action' => 'shippingbilling-modal'
                    )
                )
            ),
            'checkout-step1' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/checkout/step1',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Checkout',
                        'action' => 'step1'
                    )
                )
            ),
            'checkout-step2' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/checkout/step2',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Checkout',
                        'action' => 'step2'
                    )
                )
            ),
            'checkout-editaddress' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/checkout/editaddress[/type/:type][/addressId/:addressId]',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Checkout',
                        'action' => 'editaddress'
                    ),
                    'constraints' => array(
                        'addressId' => '[0-9]+'
                    )
                )
            ),
            'checkout-cart-preview' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/checkout/cart/preview',
                    'defaults' => array(
                        'controller' => 'Checkout\Controller\Checkout',
                        'action' => 'cart-preview'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Checkout\Service\Form\CheckoutAddress' => 'Checkout\Service\Form\CheckoutAddress',
        'Checkout\Service\Form\EditCheckoutAddress' => 'Checkout\Service\Form\EditCheckoutAddress',
        'Checkout\Service\Form\CartCheckout' => 'Checkout\Service\Form\CartCheckout',
        'Checkout\Service\Block\LoadMiniCart' => 'Checkout\Service\Block\LoadMiniCart',
        'Checkout\Service\Navigation\CheckoutNavigation' => 'Checkout\Service\Navigation\CheckoutNavigation',
        'Checkout\Service\UserLogout' => 'Checkout\Service\UserLogout'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Checkout\Form\ProductAddToCart\ProductAddToCartForm' => 'Checkout\Form\ProductAddToCart\ProductAddToCartForm',
            'Checkout\Form\ProductAddToCart\ProductAddToCartFilter' => 'Checkout\Form\ProductAddToCart\ProductAddToCartFilter',
            'Cart' => 'Checkout\Option\CartContainer',
            'Checkout\Form\Address\AddressForm' => 'Checkout\Form\Address\AddressForm',
            'Checkout\Form\Address\AddressFilter' => 'Checkout\Form\Address\AddressFilter',
            'Checkout\Service\Form\CheckoutAddress' => 'Checkout\Service\Form\CheckoutAddress',
            'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressForm' => 'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressForm',
            'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressFilter' => 'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressFilter',
            'Checkout\Service\Form\EditCheckoutAddress' => 'Checkout\Service\Form\EditCheckoutAddress',
            'Checkout\Form\CartCheckout\CartCheckoutForm' => 'Checkout\Form\CartCheckout\CartCheckoutForm',
            'Checkout\Form\CartCheckout\CartCheckoutFilter' => 'Checkout\Form\CartCheckout\CartCheckoutFilter',
            'Checkout\Service\Form\CartCheckout' => 'Checkout\Service\Form\CartCheckout',
            'Checkout\Service\Block\LoadMiniCart' => 'Checkout\Service\Block\LoadMiniCart',
            'Checkout\Service\Navigation\CheckoutNavigation' => 'Checkout\Service\Navigation\CheckoutNavigation',
            'Checkout\Service\UserLogout' => 'Checkout\Service\UserLogout'
        ),
        'factories' => array(
            'Checkout\Form\ProductAddToCart\ProductAddToCartFactory' => 'Checkout\Form\ProductAddToCart\ProductAddToCartFactory',
            'Checkout\Form\Address\AddressFactory' => 'Checkout\Form\Address\AddressFactory',
            'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressFactory' => 'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressFactory',
            'Checkout\Form\CartCheckout\CartCheckoutFactory' => 'Checkout\Form\CartCheckout\CartCheckoutFactory'
        ),
        'shared' => array(
            'Checkout\Form\ProductAddToCart\ProductAddToCartFactory' => false,
            'Checkout\Form\ProductAddToCart\ProductAddToCartForm' => false,
            'Checkout\Form\ProductAddToCart\ProductAddToCartFilter' => false,
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Checkout\Controller\Cart' => 'Checkout\Controller\CartController',
            'Checkout\Controller\Checkout' => 'Checkout\Controller\CheckoutController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Checkout' => __DIR__ . '/../view'
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Checkout' => __DIR__ . '/../public'
            )
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'productAddToCartForm' => 'Checkout\View\Helper\ProductAddToCartForm'
        )
    )
);
