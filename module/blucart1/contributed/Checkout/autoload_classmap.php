<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
  'Checkout\Api\CheckoutApi'                                     => __DIR__ . '/src/Checkout/Api/CheckoutApi.php',
  'Checkout\View\Helper\ProductAddToCartForm'                    => __DIR__ . '/src/Checkout/View/Helper/ProductAddToCartForm.php',
  'Checkout\Option\Validation\ValidationContainer'               => __DIR__ . '/src/Checkout/Option/Validation/ValidationContainer.php',
  'Checkout\Option\Validation\ValidationItem'                    => __DIR__ . '/src/Checkout/Option/Validation/ValidationItem.php',
  'Checkout\Option\CartOutItem'                                  => __DIR__ . '/src/Checkout/Option/CartOutItem.php',
  'Checkout\Option\CartEvent'                                    => __DIR__ . '/src/Checkout/Option/CartEvent.php',
  'Checkout\Option\CartInItem'                                   => __DIR__ . '/src/Checkout/Option/CartInItem.php',
  'Checkout\Option\CartItem'                                     => __DIR__ . '/src/Checkout/Option/CartItem.php',
  'Checkout\Option\CartContainer'                                => __DIR__ . '/src/Checkout/Option/CartContainer.php',
  'Checkout\Service\Block\LoadMiniCart'                          => __DIR__ . '/src/Checkout/Service/Block/LoadMiniCart.php',
  'Checkout\Service\Form\CartCheckout'                           => __DIR__ . '/src/Checkout/Service/Form/CartCheckout.php',
  'Checkout\Service\Form\CheckoutAddress'                        => __DIR__ . '/src/Checkout/Service/Form/CheckoutAddress.php',
  'Checkout\Service\Form\EditCheckoutAddress'                    => __DIR__ . '/src/Checkout/Service/Form/EditCheckoutAddress.php',
  'Checkout\Controller\CartController'                           => __DIR__ . '/src/Checkout/Controller/CartController.php',
  'Checkout\Controller\CheckoutController'                       => __DIR__ . '/src/Checkout/Controller/CheckoutController.php',
  'Checkout\Form\CartCheckout\CartCheckoutForm'                  => __DIR__ . '/src/Checkout/Form/CartCheckout/CartCheckoutForm.php',
  'Checkout\Form\CartCheckout\CartCheckoutFactory'               => __DIR__ . '/src/Checkout/Form/CartCheckout/CartCheckoutFactory.php',
  'Checkout\Form\CartCheckout\CartCheckoutFilter'                => __DIR__ . '/src/Checkout/Form/CartCheckout/CartCheckoutFilter.php',
  'Checkout\Form\ProductAddToCart\ProductAddToCartFactory'       => __DIR__ . '/src/Checkout/Form/ProductAddToCart/ProductAddToCartFactory.php',
  'Checkout\Form\ProductAddToCart\ProductAddToCartFilter'        => __DIR__ . '/src/Checkout/Form/ProductAddToCart/ProductAddToCartFilter.php',
  'Checkout\Form\ProductAddToCart\ProductAddToCartForm'          => __DIR__ . '/src/Checkout/Form/ProductAddToCart/ProductAddToCartForm.php',
  'Checkout\Form\Address\ShippingAddressFieldset'                => __DIR__ . '/src/Checkout/Form/Address/ShippingAddressFieldset.php',
  'Checkout\Form\Address\AddressForm'                            => __DIR__ . '/src/Checkout/Form/Address/AddressForm.php',
  'User\Form\UserAddress\UserAddressForm'                        => __DIR__ . '/src/Checkout/Form/Address/UserAddressForm.php',
  'User\Form\UserAddress\UserAddressFilter'                      => __DIR__ . '/src/Checkout/Form/Address/UserAddressFilter.php',
  'Checkout\Form\Address\BillingAddressFieldset'                 => __DIR__ . '/src/Checkout/Form/Address/BillingAddressFieldset.php',
  'Checkout\Form\Address\AddressFilter'                          => __DIR__ . '/src/Checkout/Form/Address/AddressFilter.php',
  'Checkout\Form\Address\AddressFactory'                         => __DIR__ . '/src/Checkout/Form/Address/AddressFactory.php',
  'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressFilter'  => __DIR__ . '/src/Checkout/Form/EditCheckoutAddress/EditCheckoutAddressFilter.php',
  'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressForm'    => __DIR__ . '/src/Checkout/Form/EditCheckoutAddress/EditCheckoutAddressForm.php',
  'Checkout\Form\EditCheckoutAddress\EditCheckoutAddressFactory' => __DIR__ . '/src/Checkout/Form/EditCheckoutAddress/EditCheckoutAddressFactory.php',
  'Checkout\Module'                                              => __DIR__ . '/Module.php',
);
