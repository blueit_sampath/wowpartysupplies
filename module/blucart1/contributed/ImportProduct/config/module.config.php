<?php

return array(
    'events' => array(
        'ImportProduct\Service\Import\Product' => 'ImportProduct\Service\Import\Product',
        'ImportProduct\Service\Navigation\ImportProductNavigation' => 'ImportProduct\Service\Navigation\ImportProductNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'ImportProduct\Service\Import\Product' => 'ImportProduct\Service\Import\Product',
            'ImportProduct\Service\Navigation\ImportProductNavigation' => 'ImportProduct\Service\Navigation\ImportProductNavigation'
        ),
        'factories' => array()
    ),
    'controllers' => array('invokables' => array()),
    'view_manager' => array('template_path_stack' => array('ImportProduct' => __DIR__ . '/../view')),
    
    'asset_manager' => array('resolver_configs' => array('paths' => array('ImportProduct' => __DIR__ . '/../public')))
);
