<?php

namespace ImportProduct\Service\Import;

use Import\Option\AbstractImportEvent;
use Product\Api\ProductApi;
use Core\Functions;

class Product extends AbstractImportEvent {

    public function getEventName() {
        return 'adminProductImport';
    }

    public function getPriority() {
        return 1000;
    }

    public function loadSchema() {
        $importContainer = $this->getImportContainer();
        $importContainer->setTitle('Import Product');
        $importContainer->addColumn('title', 'Title', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 1,
                        'max' => 255
                    )
                )
            )
                ), 1000);

        $importContainer->addColumn('sku', 'SKU', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 990);

        $importContainer->addColumn('shortDescription', 'Short Description', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 980);

        $importContainer->addColumn('description', 'Description', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 970);

        $importContainer->addColumn('sellPrice', 'Sell Price', array(
            'required' => true,
            'filters' => array(
                array('name' => '\Common\Form\Filter\EscapeCurrencyFromString')
            ),
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
                ), 960);

        $importContainer->addColumn('costPrice', 'Cost Price', array(
            'required' => false,
            'filters' => array(
                array('name' => '\Common\Form\Filter\EscapeCurrencyFromString')
            ),
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
                ), 950);

        $importContainer->addColumn('listPrice', 'List Price', array(
            'name' => 'listPrice',
            'required' => false,
            'filters' => array(
                array('name' => '\Common\Form\Filter\EscapeCurrencyFromString')
            ),
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
                ), 940);

        $importContainer->addColumn('weight', 'Priority', array(
            'name' => 'weight',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
                ), 930);

        $importContainer->addColumn('status', 'Status', array(
            'name' => 'status',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'Int'
                )
            )
                ), 920);

        $importContainer->addColumn('resetImages', 'Images(Replace Existing)', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 910);
        $importContainer->addColumn('addImages', 'Images(Add To List)', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 900);

        if (Functions::hasModule('ProductCategory')) {

            $importContainer->addColumn('resetCategories', 'Categories(Replace Existing)', array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                )
                    ), 890);

            $importContainer->addColumn('addCategories', 'Categories(Add To List)', array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'StripTags'
                    ),
                    array(
                        'name' => 'StringTrim'
                    )
                )
                    ), 880);
        }
        if (Functions::hasModule('Inventory')) {

            $importContainer->addColumn('stockStatus', 'Stock Status', array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Int'
                    )
                )
                    ), 870);

            $importContainer->addColumn('addStock', 'Stock(Add Stock)', array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Int'
                    )
                )
                    ), 860);

            $importContainer->addColumn('resetStock', 'Stock(Reset Stock)', array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Int'
                    )
                )
                    ), 860);
        }

        if (Functions::hasModule('Vat')) {
            $importContainer->addColumn('isVat', 'Is Vat?', array(
                'required' => false,
                'filters' => array(
                    array('name' => '\Common\Form\Filter\EscapeCurrencyFromString')
                ),
                'validators' => array(
                    array(
                        'name' => 'Int'
                    )
                )
                    ), 860);
        }
    }

    public function isValid() {
        $importContainer = $this->getImportContainer();
        $inputFilter = $importContainer->getInputFilter();
        $data = $importContainer->getRawData();

        if (isset($data['sku'])) {
            $sku = $data['sku'];
            if ($sku) {
                $product = ProductApi::getProductBySku($sku);
                if ($product) {
                    $inputFilter->get('title')->setRequired(false);

                    $inputFilter->get('sellPrice')->setRequired(false);
                    $inputFilter->get('status')->setRequired(false);
                }
            }
        }
    }

    public function save() {
        $importContainer = $this->getImportContainer();
        $data = $importContainer->getData();
        $columns = array(
            'sku',
            'title',
            'shortDescription',
            'description',
            'listPrice',
            'costPrice',
            'sellPrice',
            'weight',
            'status'
        );

        $sku = $data['sku'];
        if ($sku && ($product = ProductApi::getProductBySku($sku))) {
            
        } else {
            $product = new \Product\Entity\Product();
            $product->status = 1;
            $importContainer->setIsNewRecord(true);
        }
        foreach ($columns as $column) {
            if (isset($data[$column])) {
                $product->$column = $data[$column];
            }
        }

        $em = $this->getEntityManager();
        $em->persist($product);
        $em->flush();
        $importContainer->addEntity('product', $product);

        $this->saveProductImages($data, $product);
        if (Functions::hasModule('ProductCategory')) {
            $this->saveProductCategories($data, $product);
        }

        if (Functions::hasModule('Inventory')) {
            $this->saveStock($data, $product);
        }

        if (Functions::hasModule('Vat')) {
            if (isset($data['isVat'])) {
                $this->saveVat($data['isVat'], $product);
            }
        }
        Functions::addSuccessMessage('Saved Successfully');
    }

    public function saveProductImages($data, $product) {

        if (isset($data['resetImages'])) {
            $em = $this->getEntityManager();
            $productImages = ProductApi::getProductImageByProductId($product->id, null);
            foreach ($productImages as $productImageArray) {
                $productImage = ProductApi::getProductImageById($productImageArray['productImageId']);
                \File\Api\FileApi::deleteFile($productImage->file->path);
                $em->remove($productImage);
            }
            $em->flush();

            $this->saveImages($data['resetImages'], $product);
        }


        if (isset($data['addImages'])) {
            $this->saveImages($data['addImages'], $product);
        }
    }

    public function saveImages($images, $product) {
        $em = $this->getEntityManager();
        $images = explode(';', $images);
        foreach ($images as $image) {
            $image = trim($image);
            $file = $this->saveImage($image, $product->title, $product->title, $product->title);
            if ($file) {
                $productImage = new \Product\Entity\ProductImage();
                $productImage->file = $file;
                $productImage->product = $product;
                $productImage->status = 1;

                $em->persist($productImage);
            }
        }
        $em->flush();
    }

    public function saveProductCategories($data, $product) {

        if (isset($data['resetCategories'])) {
            $em = $this->getEntityManager();
            $productCategories = \ProductCategory\Api\ProductCategoryApi::getProductCategoryByProductId($product->id);
            foreach ($productCategories as $productCategoryArray) {
                $productCategory = \ProductCategory\Api\ProductCategoryApi::getProductCatetoryById($productCategoryArray['productCategoryId']);
                $em->remove($productCategory);
            }
            $em->flush();

            $this->saveProductCategory($data['resetCategories'], $product);
        }


        if (isset($data['addCategories'])) {
            $this->saveProductCategory($data['addCategories'], $product);
        }
    }

    public function saveProductCategory($categories, $product) {
        $em = $this->getEntityManager();
        $categories = explode(';', $categories);

        foreach ($categories as $category) {
            $category = trim($category);
            $category = $this->createSlug($category);
            if ($category) {
                $categoryEntity = \Category\Api\CategoryApi::getCategoryByName($category);


                if ($categoryEntity) {
                    if (!\ProductCategory\Api\ProductCategoryApi::productHasCategory($product->id, $categoryEntity->id)) {
                        $productCategory = new \ProductCategory\Entity\ProductCategory();
                        $productCategory->product = $product;
                        $productCategory->category = $categoryEntity;
                        $em->persist($productCategory);
                        $em->flush();
                    }
                } else {
                    Functions::addWarningMessage($category . ': Category not found');
                }
            }
        }
    }

    public function saveStock($data, $product) {
        $em = $this->getEntityManager();
        $stockEntity = \Inventory\Api\InventoryApi::getStockByProductId($product->id);
        if (!$stockEntity) {
            $stockEntity = new \Inventory\Entity\Stock();
        }

        if (isset($data['stockStatus']) && $data['stockStatus'] !== '') {
            $stockEntity->status = $data['stockStatus'];
        }


        if (isset($data['addStock'])) {
            $stockEntity->qty = $stockEntity->qty + $data['addStock'];
        }

        if (isset($data['resetStock'])) {
            $stockEntity->qty = $data['resetStock'];
        }
        $stockEntity->product = $product;
        $em->persist($stockEntity);
        $em->flush();
    }

    public function saveVat($vat, $product) {
        $em = $this->getEntityManager();

        $entity = \Vat\Api\VatApi::getVatByProductId($product->id);


        if (!$entity) {
            $entity = new \Vat\Entity\VatProduct();
            $entity->product = $product;
        }

        $entity->status = $vat;
        $em->persist($entity);

        $em->flush();



        return true;
    }

  

}
