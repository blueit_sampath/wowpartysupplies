<?php

namespace ImportProduct\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ImportProductNavigation extends \Admin\Navigation\Event\AdminNavigationEvent {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMainImportMain');
       
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Import Products',
                    'route' => 'admin-import',
                    'params' => array(
                        'eventName' => 'adminProductImport' 
                    ),
                    'id' => 'admin-import-product',
                    'iconClass' => 'glyphicon-briefcase'
                )
            ));
        }
    }

}
