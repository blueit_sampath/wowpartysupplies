<?php
return array(
    'router' => array(
        'routes' => array(
            'quote' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/quote',
                    'defaults' => array(
                        'controller' => 'Quote\Controller\Index',
                        'action' => 'index'
                    )
                )
            ),
            'quote-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/quote/view[/:quoteId]',
                    'defaults' => array(
                        'controller' => 'Quote\Controller\Index',
                        'action' => 'view'
                    ),
                    'constraints' => array(
                        'quoteId' => '[0-9]+'
                    )
                )
            ),
            'quote-print' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/quote/print[/:quoteId]',
                    'defaults' => array(
                        'controller' => 'Quote\Controller\Index',
                        'action' => 'print'
                    ),
                    'constraints' => array(
                        'quoteId' => '[0-9]+'
                    )
                )
            ),
            'quote-download' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/quote/download[/:quoteId]',
                    'defaults' => array(
                        'controller' => 'Quote\Controller\Index',
                        'action' => 'download'
                    ),
                    'constraints' => array(
                        'quoteId' => '[0-9]+'
                    )
                )
            ),
            'quote-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quote/add',
                    'defaults' => array(
                        'controller' => 'Quote\Controller\Index',
                        'action' => 'add'
                    )
                )
            ),
            'quote-load' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quote/load[/:quoteId]',
                    'defaults' => array(
                        'controller' => 'Quote\Controller\Index',
                        'action' => 'load'
                    ),
                    'constraints' => array(
                        'quoteId' => '[0-9]+'
                    )
                )
            ),
            'quote-remove' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quote/remove[/:quoteId]',
                    'defaults' => array(
                        'controller' => 'Quote\Controller\Index',
                        'action' => 'remove'
                    ),
                    'constraints' => array(
                        'quoteId' => '[0-9]+'
                    )
                )
            ),
            'admin-quote-config-customer-notification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/quote/config/customer/notification[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Quote\Controller\AdminQuoteConfigCustomerNotification',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-quote-config-admin-notification' => array(
            		'type' => 'segment',
            		'options' => array(
            				'route' => '/admin/quote/config/admin/notification[/ajax/:ajax]',
            				'defaults' => array(
            						'controller' => 'Quote\Controller\AdminQuoteConfigAdminNotification',
            						'action' => 'add',
            						'ajax' => 'true'
            				),
            				'constraints' => array(
            						'ajax' => 'true|false'
            				)
            		)
            )
        )
    ),
    'events' => array(
        'Quote\Service\Form\QuoteForm' => 'Quote\Service\Form\QuoteForm',
        'Quote\Service\Navigation\QuoteAccountNavigation' => 'Quote\Service\Navigation\QuoteAccountNavigation',
        'Quote\Service\Form\AdminQuoteCustomerNotificationConfig' => 'Quote\Service\Form\AdminQuoteCustomerNotificationConfig',
        'Quote\Service\Form\AdminQuoteAdminNotificationConfig' => 'Quote\Service\Form\AdminQuoteAdminNotificationConfig',
        'Quote\Service\Tab\AdminOrderQuote' => 'Quote\Service\Tab\AdminOrderQuote',
        'Quote\Service\Form\QuoteFormMail' => 'Quote\Service\Form\QuoteFormMail'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Quote\Form\Quote\QuoteForm' => 'Quote\Form\Quote\QuoteForm',
            'Quote\Form\Quote\QuoteFilter' => 'Quote\Form\Quote\QuoteFilter',
            'Quote\Service\Form\QuoteForm' => 'Quote\Service\Form\QuoteForm',
            'Quote\Service\Navigation\QuoteAccountNavigation' => 'Quote\Service\Navigation\QuoteAccountNavigation',
            'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteForm' => 'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteForm',
            'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteFilter' => 'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteFilter',
            'Quote\Service\Form\AdminQuoteCustomerNotificationConfig' => 'Quote\Service\Form\AdminQuoteCustomerNotificationConfig',
            'Quote\Service\Tab\AdminOrderQuote' => 'Quote\Service\Tab\AdminOrderQuote',
            'Quote\Service\Form\QuoteFormMail' => 'Quote\Service\Form\QuoteFormMail',
            'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteForm' => 'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteForm',
            'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteFilter' => 'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteFilter',
            'Quote\Service\Form\AdminQuoteAdminNotificationConfig' => 'Quote\Service\Form\AdminQuoteAdminNotificationConfig',
            
        ),
        'factories' => array(
            'Quote\Form\Quote\QuoteFactory' => 'Quote\Form\Quote\QuoteFactory',
            'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteFactory' => 'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteFactory',
            'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteFactory' => 'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Quote\Controller\Index' => 'Quote\Controller\IndexController',
            'Quote\Controller\AdminQuoteConfigCustomerNotification' => 'Quote\Controller\AdminQuoteConfigCustomerNotificationController',
            'Quote\Controller\AdminQuoteConfigAdminNotification' => 'Quote\Controller\AdminQuoteConfigAdminNotificationController',
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Quote' => __DIR__.'/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'Quote_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__.'/../src/Quote/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Quote\Entity' => 'Quote_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Quote' => __DIR__.'/../public'
            )
        )
    )
);
