<?php

namespace Quote\Api;

use Core\Functions;
use Checkout\Option\CartContainer;
use User\Api\UserApi;
use Common\Api\Api;

class QuoteApi extends Api {
	protected static $_entity = '\Quote\Entity\Quote';
	public static function getQuoteById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function removeQuote($quoteId) {
		$em = static::getEntityManager ();
		$quote = static::getQuoteById ( $quoteId );
		if (! $quote) {
			return false;
		}
		$em->remove ( $quote );
		$em->flush ();
		return true;
	}
	public static function getQuoteByUserId($userId) {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findBy ( array (
				'user' => $userId 
		) );
	}
	public static function saveQuote($title, $userId) {
		$cart = static::getCart ();
		$array = array ();
		$array ['params'] = $cart->getParams ();
		
		$items = $cart->getCartItems ();
		
		foreach ( $items as $item ) {
			$newItem = clone $item;
			$newItem->setInItems ( array () );
			$newItem->setOutItems ( array () );
			$array ['cartItems'] [$item->getId ()] = $newItem;
		}
		$text_serialize = serialize ( $array );
		$amount = $cart->getSubTotalPrice ();
		
		$entity = new static::$_entity ();
		$entity->title = $title;
		$entity->amount = $amount;
		$entity->quote = $text_serialize;
		$entity->user = UserApi::getUserById ( $userId );
		$em = static::getEntityManager ();
		$em->persist ( $entity );
		$em->flush ();
		return $entity;
	}
	public static function loadQuote($quoteId) {
		$quote = static::getQuoteById ( $quoteId );
		if (! $quote) {
			return false;
		}
		$cart = static::getCart ();
		$cart->clearCart ();
		$cart->save ();
		
		$array = unserialize ( $quote->quote );
		
		$cart->setParams ( $array ['params'] );
		
		$items = $array ['cartItems'];
		foreach ( $items as $item ) {
			$item = $cart->prepareCartItemEvent ( $item );
			$cart->addCartItem ( $item );
		}
		$cart->save ();
		return true;
	}
	
	public static function cloneQuote($quoteId) {
		$quote = static::getQuoteById ( $quoteId );
		if (! $quote) {
			return false;
		}
		$cartContainer = static::getCart ();
		$cart = clone $cartContainer; 
		$cart->clearCart ();
		$array = unserialize ( $quote->quote );
		$cart->setParams ( $array ['params'] );
		$items = $array ['cartItems'];
		foreach ( $items as $item ) {
			$item = $cart->prepareCartItemEvent ( $item );
			$cart->addCartItem ( $item );
		}
		return $cart;
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	public static function getCart() {
		$sm = Functions::getServiceLocator ();
		$cart = $sm->get ( 'Cart' );
		return $cart;
	}
}
