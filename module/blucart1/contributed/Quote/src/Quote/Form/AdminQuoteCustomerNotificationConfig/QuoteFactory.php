<?php
namespace Quote\Form\AdminQuoteCustomerNotificationConfig;

use Common\Form\Option\AbstractFormFactory;

class QuoteFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteForm' );
		$form->setName ( 'adminQuoteCustomerNotificationConfig' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteFilter' );
	}
}
