<?php
namespace Quote\Form\Quote;

use Common\Form\Option\AbstractFormFactory;

class QuoteFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Quote\Form\Quote\QuoteForm' );
		$form->setName ( 'quoteForm' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Quote\Form\Quote\QuoteFilter' );
	}
}
