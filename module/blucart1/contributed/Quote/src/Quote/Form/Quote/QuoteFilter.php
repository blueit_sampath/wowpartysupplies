<?php
namespace Quote\Form\Quote;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  QuoteFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'title',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						),
						array (
								'name' => 'StripTags'
						)
				)
		) );
		
	}
} 