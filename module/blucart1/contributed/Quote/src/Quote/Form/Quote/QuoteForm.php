<?php
namespace Quote\Form\Quote;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class QuoteForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
	
		
		$this->add ( array (
				'name' => 'title',
				'attributes' => array (
						'type' => 'text',
				),
				'options' => array (
						'label' => 'Save as' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}