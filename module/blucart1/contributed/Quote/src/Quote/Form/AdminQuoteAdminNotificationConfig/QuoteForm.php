<?php
namespace Quote\Form\AdminQuoteAdminNotificationConfig;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class QuoteForm extends Form

{

    public function init()
    {
       
        $this->add(array(
            'name' => 'QUOTE_ADMIN_NOTIFICATION_SUBJECT',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Quote Admin Notification Subject'
            )
        ), array(
            'priority' => 1000
        ));
        
        $this->add(array(
            'name' => 'QUOTE_ADMIN_NOTIFICATION_HTMLMESSAGE',
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'wysiwg_full'
            ),
            'options' => array(
                'label' => 'Quote Admin Notification HTML Message'
            )
        ), array(
            'priority' => 990
        ));
        
        $this->add(array(
            'name' => 'QUOTE_ADMIN_NOTIFICATION_PLAINMESSAGE',
            'attributes' => array(
                'type' => 'textarea'
            )
            ,
            'options' => array(
                'label' => 'Quote Admin Notification Plain Message'
            )
        ), array(
            'priority' => 990
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'QUOTE_ADMIN_NOTIFICATION_ATTACH_QUOTE',
            'options' => array(
                'label' => 'Attach Quote',
                'use_hidden_element' => true
            )
        ), array(
            'priority' => 980
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }
}