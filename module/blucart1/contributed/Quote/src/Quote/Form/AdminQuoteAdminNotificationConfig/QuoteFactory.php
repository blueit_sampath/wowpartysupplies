<?php
namespace Quote\Form\AdminQuoteAdminNotificationConfig;

use Common\Form\Option\AbstractFormFactory;

class QuoteFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteForm' );
		$form->setName ( 'adminQuoteAdminNotificationConfig' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteFilter' );
	}
}
