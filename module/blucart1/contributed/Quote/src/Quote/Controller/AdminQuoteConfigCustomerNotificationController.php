<?php

namespace Quote\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminQuoteConfigCustomerNotificationController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Quote\Form\AdminQuoteCustomerNotificationConfig\QuoteFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-quote-config-customer-notification' );
	}
}