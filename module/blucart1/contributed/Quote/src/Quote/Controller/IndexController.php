<?php

namespace Quote\Controller;

use Common\MVC\Controller\AbstractAdminController;
use Common\MVC\Controller\AbstractFrontController;
use Zend\View\Model\ViewModel;
use Core\Functions;
use Quote\Api\QuoteApi;
use User\Api\UserApi;
use Pdf\Option\Pdf;

class IndexController extends AbstractFrontController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('Quote\Form\Quote\QuoteFactory');
    }

    public function getSaveRedirector() {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('quote');
    }

    public function indexAction() {
        if (!UserApi::getLoggedInUser()) {
            return $this->accessDenied();
        }
    }

    public function addAction() {
        if (!UserApi::getLoggedInUser()) {
            Functions::addErrorMessage('Please login to save your quote');
            return $this->redirect()->toRoute('user-login');
        }
        return parent::addAction();
    }

    public function loadAction() {
        $quoteId = Functions::fromRoute('quoteId', 0);
        if (!$user = UserApi::getLoggedInUser()) {
            return $this->accessDenied();
        }
        $quote = QuoteApi::getQuoteById($quoteId);
        if (!$quote || ($quote->user->id != $user->id)) {
            return $this->accessDenied();
        }

        QuoteApi::loadQuote($quoteId);
        return $this->redirect()->toRoute('cart');
    }

    public function removeAction() {
        $quoteId = Functions::fromRoute('quoteId', 0);
        if (!$user = UserApi::getLoggedInUser()) {
            return $this->accessDenied();
        }
        $quote = QuoteApi::getQuoteById($quoteId);
        if (!$quote || ($quote->user->id != $user->id)) {
            return $this->accessDenied();
        }
        QuoteApi::removeQuote($quoteId);
        return $this->redirect()->toRoute('quote');
    }

    public function viewAction() {
        $quoteId = Functions::fromRoute('quoteId', 0);
        if (!$user = UserApi::getLoggedInUser()) {
            return $this->accessDenied();
        }
        $quote = QuoteApi::getQuoteById($quoteId);
        if (!$quote || ($quote->user->id != $user->id)) {
            return $this->accessDenied();
        }
        $cartContainer = QuoteApi::cloneQuote($quoteId);
        return array(
            'cartContainer' => $cartContainer
        );
    }

    public function printAction() {
        $quoteId = Functions::fromRoute('quoteId', 0);
        if (!$user = UserApi::getLoggedInUser()) {
            return $this->accessDenied();
        }
        $quote = QuoteApi::getQuoteById($quoteId);
        if (!$quote || ($quote->user->id != $user->id)) {
            return $this->accessDenied();
        }
        $cartContainer = QuoteApi::cloneQuote($quoteId);
        $viewModel = new ViewModel(array(
            'quoteId' => $quoteId,
            'cartContainer' => $cartContainer
        ));
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function downloadAction() {
        $quoteId = Functions::fromRoute('quoteId', 0);
        if (!$user = UserApi::getLoggedInUser()) {
            return $this->accessDenied();
        }
        $quote = QuoteApi::getQuoteById($quoteId);
        if (!$quote || ($quote->user->id != $user->id)) {
            return $this->accessDenied();
        }
        $cartContainer = QuoteApi::cloneQuote($quoteId);

        $renderer = Functions::getServiceLocator()->get('viewrenderer');

        $string = $renderer->render('common/order-invoice', array(
            'orderId' => $quoteId,
            'cartContainer' => $cartContainer
        ));
        $pdf = new Pdf();
        $pdf->WriteHTML($string);

        $pdf->sendToBrowserAsDownload("quote_" . $quoteId . ".pdf");

        exit();
    }

}
