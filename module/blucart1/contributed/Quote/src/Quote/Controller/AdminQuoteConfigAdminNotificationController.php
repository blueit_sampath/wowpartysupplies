<?php

namespace Quote\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminQuoteConfigAdminNotificationController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Quote\Form\AdminQuoteAdminNotificationConfig\QuoteFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-quote-config-admin-notification' );
	}
}