<?php 
namespace Quote\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminOrderQuote extends AbstractTabEvent {
	public function getEventName() {
		return 'adminOrderConfig';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-quote-config-customer-notification');
		$tabContainer->add ( 'admin-quote-config-customer-notification', 'Quote Customer Setting', $u,100 );
		$u = $url ( 'admin-quote-config-admin-notification');
		$tabContainer->add ( 'admin-quote-config-admin-notification', 'Quote Admin Setting', $u,90 );
		
		return $this;
	}
}

