<?php 
namespace Quote\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminQuoteCustomerNotificationConfig extends AdminConfig {
    protected $_columnKeys = array(
    		'QUOTE_CUSTOMER_NOTIFICATION_PLAINMESSAGE',
    		'QUOTE_CUSTOMER_NOTIFICATION_HTMLMESSAGE',
    		'QUOTE_CUSTOMER_NOTIFICATION_SUBJECT',
    		'QUOTE_CUSTOMER_NOTIFICATION_ATTACH_QUOTE'
    );
	public function getFormName() {
		return 'adminQuoteCustomerNotificationConfig';
	}
	public function getPriority() {
		return 1000;
	}
	
}
