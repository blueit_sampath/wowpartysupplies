<?php

namespace Quote\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Api\ConfigApi;
use Pdf\Option\Pdf;
use Quote\Api\QuoteApi;
use Common\Option\Token\TokenContainer;

class QuoteFormMail extends AbstractMainFormEvent {

    public function getFormName() {
        return 'quoteForm';
    }

    public function getPriority() {
        return 100;
    }

    public function save() {

        $resultContainer = $this->getFormResultContainer();
        $quote = $resultContainer->get('quote')->getValue();
        $this->sendEmail($quote);
    }

    /**
     *
     * @return \Common\Option\Token\TokenContainer
     */
    public function sendEmail($quote) {
        $subject = ConfigApi::getConfigByKey('QUOTE_CUSTOMER_NOTIFICATION_SUBJECT');
        $bodyHtml = ConfigApi::getConfigByKey('QUOTE_CUSTOMER_NOTIFICATION_HTMLMESSAGE');
        $bodyPlain = ConfigApi::getConfigByKey('QUOTE_CUSTOMER_NOTIFICATION_PLAINMESSAGE');

        $fromName = ConfigApi::getConfigByKey('ORDER_NAME', '');
        $fromEmail = ConfigApi::getConfigByKey('ORDER_EMAIL');

        if (!$fromEmail) {
            return;
        }

        if ($subject && ($bodyHtml || $bodyPlain)) {

            $tokenContainer = $this->getTokenContainer();
            $tokenContainer->addParam('quote', $quote);
            $tokenContainer->addParam('quoteId', $quote->id);
            $tokenContainer->setSubject($subject);
            $tokenContainer->setBodyHtml($bodyHtml);
            $tokenContainer->setBodyText($bodyPlain);
            $tokenContainer->setFromEmail($fromEmail);
            $tokenContainer->setFromName($fromName);
            $tokenContainer->setToEmail($quote->user->email);
            $tokenContainer->setToName($quote->user->firstName . ' ' . $quote->user->lastName);
            $tokenContainer->add('quote', $quote);
            $tokenContainer->add('user', $quote->user);

            $mail = $tokenContainer->prepare('quoteMail');
            if (ConfigApi::getConfigByKey('QUOTE_CUSTOMER_NOTIFICATION_ATTACH_QUOTE')) {

                $cartContainer = QuoteApi::cloneQuote($quote->id);

                $renderer = Functions::getServiceLocator()->get('viewrenderer');

                $invoice = $renderer->render('common/download-quote', array(
                    'quoteId' => $quote->id,
                    'cartContainer' => $cartContainer
                ));
                $pdf = new Pdf();
                $pdf->WriteHTML($invoice);
                $invoice = $pdf->getContent();

                $attachment = $mail->addAttachment('quote_' . $quote->id, $invoice, 'application/pdf', 'quote_' . $quote->id);
            }
            $tokenContainer->sendMail();
        }


        $subject = ConfigApi::getConfigByKey('QUOTE_ADMIN_NOTIFICATION_SUBJECT');
        $bodyHtml = ConfigApi::getConfigByKey('QUOTE_ADMIN_NOTIFICATION_HTMLMESSAGE');
        $bodyPlain = ConfigApi::getConfigByKey('QUOTE_ADMIN_NOTIFICATION_PLAINMESSAGE');
        $toName = ConfigApi::getConfigByKey('ORDER_NOTIFICATION_NAME');
        $toEmail = ConfigApi::getConfigByKey('ORDER_NOTIFICATION_EMAIL');


        if (!$fromEmail) {
            return;
        }

        if ($subject && ($bodyHtml || $bodyPlain)) {

            $tokenContainer = $this->getTokenContainer();
            $tokenContainer->addParam('quote', $quote);
            $tokenContainer->addParam('user', $quote->user);
            $tokenContainer->addParam('quoteId', $quote->id);
            $tokenContainer->setSubject($subject);
            $tokenContainer->setBodyHtml($bodyHtml);
            $tokenContainer->setBodyText($bodyPlain);
            $tokenContainer->setFromEmail($fromEmail);
            $tokenContainer->setFromName($fromName);

            $tokenContainer->setToEmail($toEmail);
            $tokenContainer->setToName($toName);

            $tokenContainer->add('quote', $quote);
            $mail = $tokenContainer->prepare('quoteMail');
            if (ConfigApi::getConfigByKey('QUOTE_ADMIN_NOTIFICATION_ATTACH_QUOTE')) {
                $cartContainer = QuoteApi::cloneQuote($quote->id);

                $renderer = Functions::getServiceLocator()->get('viewrenderer');

                $invoice = $renderer->render('common/download-quote', array(
                    'quoteId' => $quote->id,
                    'cartContainer' => $cartContainer
                ));

                $pdf = new Pdf();
                $pdf->WriteHTML($invoice);
                $invoice = $pdf->getContent();

                $attachment = $mail->addAttachment('quote_' . $quote->id, $invoice, 'application/pdf', 'quote_' . $quote->id);
            }
            $tokenContainer->sendMail();
        }


        return $tokenContainer;
    }

    /**
     *
     * @return TokenContainer
     */
    public function getTokenContainer() {
        return $this->getServiceLocator()->get('TokenContainer');
    }

}
