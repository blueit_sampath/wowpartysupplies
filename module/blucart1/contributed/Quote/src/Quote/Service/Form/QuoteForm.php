<?php

namespace Quote\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;
use Quote\Api\QuoteApi;

class QuoteForm extends AbstractMainFormEvent {
	public function getFormName() {
		return 'quoteForm';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		$title = $params ['title'];
		$user = UserApi::getLoggedInUser ();
		$entity = QuoteApi::saveQuote ( $title, $user->id );

		$resultContainer = $this->getFormResultContainer();
		$resultContainer->add('quote', $entity);
		
		return true;
	}
}
