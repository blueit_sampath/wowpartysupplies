<?php 
namespace Quote\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminQuoteAdminNotificationConfig extends AdminConfig {
    protected $_columnKeys = array(
    		'QUOTE_ADMIN_NOTIFICATION_PLAINMESSAGE',
    		'QUOTE_ADMIN_NOTIFICATION_HTMLMESSAGE',
    		'QUOTE_ADMIN_NOTIFICATION_SUBJECT',
    		'QUOTE_ADMIN_NOTIFICATION_ATTACH_QUOTE'
    );
	public function getFormName() {
		return 'adminQuoteAdminNotificationConfig';
	}
	public function getPriority() {
		return 1000;
	}
	
}
