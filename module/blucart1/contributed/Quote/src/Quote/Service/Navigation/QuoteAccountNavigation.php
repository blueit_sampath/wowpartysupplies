<?php
namespace Quote\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;
use User\Api\UserApi;
use Quote\Api\QuoteApi;

class QuoteAccountNavigation extends NavigationEvent
{

    public function account()
    {
        $navigation = $this->getNavigation();
        $user = UserApi::getLoggedInUser();
        if (! $user) {
            return;
        } else {
            $navigation->addPages(array(
                array(
                    'label' => 'My Quotes',
                    'route' => 'quote',
                    'id' => 'quote',
                    'order' => 800
                )
            )
            );
        }
    }

    public function breadcrumb()
    {
        parent::breadcrumb();
        
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'account');
        
        $user = UserApi::getLoggedInUser();
        if (! $user) {
            return;
        } else {
            $page->addPages(array(
                array(
                    'label' => 'My Quotes',
                    'route' => 'quote',
                    'id' => 'quote',
                    'order' => 800
                )
            )
            );
            $routeName = Functions::getMatchedRouteName();
            if ($routeName == 'quote-view') {
                $page = $navigation->findOneBy('id', 'quote');
                if ($page) {
                    $quoteId = Functions::fromRoute('quoteId', 0);
                    $quote = QuoteApi::getQuoteById($quoteId);
                    if ($quote) {
                        $page->addPages(array(
                            array(
                                'label' => $quote->title,
                                'route' => 'quote-view',
                                'params' => array(
                                    'quoteId' => $quoteId
                                ),
                                'id' => 'quote_'.$quoteId,
                                'order' => 800
                            )
                        )
                        );
                    }
                }
            }
        }
    }
}

