<?php
namespace Tag\Form\AdminTag;

use Common\Form\Option\AbstractFormFactory;

class TagFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Tag\Form\AdminTag\TagForm' );
		$form->setName ( 'adminTagAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Tag\Form\AdminTag\TagFilter' );
	}
}
