<?php
namespace Tag\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminTag extends AbstractTabEvent {
	public function getEventName() {
		return 'adminTagAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$tagId = Functions::fromRoute ( 'tagId' );
		if (! $tagId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-tag-add', array (
				'tagId' => $tagId 
		) );
		$tabContainer->add ( 'admin-tag-add', 'General', $u, 1000 );
		
		return $this;
	}
}

