<?php

namespace Tag\Service\Form;

use File\Api\FileApi;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminTag extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'weight',
        'status',
        'title'
    );
    protected $_entity = '\Tag\Entity\Tag';
    protected $_entityName = 'tag';

    public function getFormName() {
        return 'adminTagAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function parseSaveEntity($params, $entity) {
        FileApi::deleteFiles(Functions::fromPost('image_delete', ''));

        $image = $params ['image'];

        if ($image) {
            $array = explode(',', $image);
            foreach ($array as $file) {
                $imageEntity = FileApi::createOrUpdateFile($file, $params ['title'], $params ['title']);
            }
            $entity->file = $imageEntity;
        }
    }

    public function afterGetRecord($entity) {
        $form = $this->getForm();
        if ($entity->file) {
            $form->get('image')->setValue($entity->file->path);
        }
    }

}
