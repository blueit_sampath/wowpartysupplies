<?php

namespace Tag\Service\NestedSorting;

use Core\Functions;
use Tag\Api\TagApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminTag extends AbstractNestedSortingEvent {
	protected $_entityName = '\Tag\Entity\Tag';
	protected $_column = 'title';
	public function getEventName() {
		return 'adminTagSort';
	}
	public function getPriority() {
		return 1000;
	}
	
}
