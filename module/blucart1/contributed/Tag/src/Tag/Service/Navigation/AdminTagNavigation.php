<?php 
namespace Tag\Service\Navigation;



class AdminTagNavigation extends \Admin\Navigation\Event\AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'productMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Tags',
							'route' => 'admin-tag',
							'id' => 'admin-tag',
							'iconClass' => 'glyphicon-font' 
					) 
			) );
		}
	}
}

