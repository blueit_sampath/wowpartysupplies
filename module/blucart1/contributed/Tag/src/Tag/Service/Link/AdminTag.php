<?php

namespace Tag\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminTag extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminTag';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-tag-add' );
		$item = $linkContainer->add ( 'admin-tag-add', 'Add Tag', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-tag-sort' );
		$item = $linkContainer->add ( 'admin-tag-sort', 'Arrange Tags', $u, 990 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

