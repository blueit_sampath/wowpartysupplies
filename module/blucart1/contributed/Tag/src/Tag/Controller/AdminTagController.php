<?php

namespace Tag\Controller;

use Common\MVC\Controller\AbstractAdminController;

use Core\Functions;

use Zend\View\Model\ViewModel;

class AdminTagController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Tag\Form\AdminTag\TagFormFactory' );
	}
	
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-tag-add', array (
				'tagId' => $form->get ( 'id' )->getValue () 
		) );
	}
}