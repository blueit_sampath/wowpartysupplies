<?php

namespace Tag\Api;

use Core\Functions;
use Common\Api\Api;

class TagApi extends Api {
	protected static $_entity = '\Tag\Entity\Tag';
	public static function getTagById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getTags($status = true) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->setEventName ( 'TagApi.getTags' );
		$queryBuilder->addColumn ( 'tag.id', 'tagId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'tag.status', 'tagStatus' );
			$queryBuilder->addParameter ( 'tagStatus', $status );
		}
		$queryBuilder->addOrder ( 'tag.weight', 'tagWeight', 'desc' );
		$queryBuilder->addGroup ( 'tag.id', 'tagId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'tag' );
		return $queryBuilder->executeQuery ();
	}
	
}
