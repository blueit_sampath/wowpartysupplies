<?php
		
		return array(
    'router' => array('routes' => array(
            'admin-tag' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/tag',
                    'defaults' => array(
                        'controller' => 'Tag\Controller\AdminTag',
                        'action' => 'index'
                        )
                    )
                ),
            'admin-tag-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/tag/add[/ajax/:ajax][/:tagId]',
                    'defaults' => array(
                        'controller' => 'Tag\Controller\AdminTag',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'tagId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-tag-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/tag/sort[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Tag\Controller\AdminTag',
                        'action' => 'sort',
                        'ajax' => 'true'
                        ),
                    'constraints' => array('ajax' => 'true|false')
                    )
                )
            )),
    'events' => array(
        'Tag\Service\Grid\AdminTag' => 'Tag\Service\Grid\AdminTag',
        'Tag\Service\Form\AdminTag' => 'Tag\Service\Form\AdminTag',
        'Tag\Service\Tab\AdminTag' => 'Tag\Service\Tab\AdminTag',
        'Tag\Service\Link\AdminTag' => 'Tag\Service\Link\AdminTag',
        'Tag\Service\NestedSorting\AdminTag' => 'Tag\Service\NestedSorting\AdminTag',
        'Tag\Service\Navigation\AdminTagNavigation' => 'Tag\Service\Navigation\AdminTagNavigation'
        ),
    'service_manager' => array(
        'invokables' => array(
            'Tag\Service\Grid\AdminTag' => 'Tag\Service\Grid\AdminTag',
            'Tag\Service\Form\AdminTag' => 'Tag\Service\Form\AdminTag',
            'Tag\Form\AdminTag\TagForm' => 'Tag\Form\AdminTag\TagForm',
            'Tag\Form\AdminTag\TagFilter' => 'Tag\Form\AdminTag\TagFilter',
            'Tag\Service\Tab\AdminTag' => 'Tag\Service\Tab\AdminTag',
            'Tag\Service\Link\AdminTag' => 'Tag\Service\Link\AdminTag',
            'Tag\Service\NestedSorting\AdminTag' => 'Tag\Service\NestedSorting\AdminTag',
            'Tag\Service\Navigation\AdminTagNavigation' => 'Tag\Service\Navigation\AdminTagNavigation'
            ),
        'factories' => array('Tag\Form\AdminTag\TagFormFactory' => 'Tag\Form\AdminTag\TagFormFactory')
        ),
    'controllers' => array('invokables' => array('Tag\Controller\AdminTag' => 'Tag\Controller\AdminTagController')),
    'view_manager' => array('template_path_stack' => array('Tag' => __DIR__.'/../view')),
    'doctrine' => array('driver' => array(
            'Tag_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__.'/../src/Tag/Entity')
                ),
            'orm_default' => array('drivers' => array('Tag\Entity' => 'Tag_driver'))
            )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('Tag' => __DIR__.'/../public')))
    );
