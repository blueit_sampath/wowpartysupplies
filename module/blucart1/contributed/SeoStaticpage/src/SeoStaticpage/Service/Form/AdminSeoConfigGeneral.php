<?php

namespace SeoStaticpage\Service\Form;

use Config\Service\Form\AdminConfig;

class AdminSeoConfigGeneral extends AdminConfig {

    protected $_columnKeys = array(
        'SEO_STATICPAGE_TITLE',
        'SEO_STATICPAGE_KEYWORDS',
        'SEO_STATICPAGE_DESCRIPTION',
        'SEO_STATICPAGE_ROBOTS',
        'SEO_STATICPAGE_URL'
    );

    public function getFormName() {
        return 'adminSeoStaticpageConfigGeneral';
    }

    public function getPriority() {
        return 1000;
    }

    public function save() {
        parent::save();
        $form = $this->getForm();
        $params = $form->getData();

        $forceBuild = false;
        if (isset($params['forceBuild'])) {
            $forceBuild = true;
        }
        \SeoStaticpage\Api\SeoStaticpageApi::buildUrls($forceBuild);
    }

}
