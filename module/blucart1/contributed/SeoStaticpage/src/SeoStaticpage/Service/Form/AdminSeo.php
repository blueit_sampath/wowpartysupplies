<?php

namespace SeoStaticpage\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;

class AdminSeo extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'alias',
        'pageTitle',
        'metaKeywords',
        'metaDescription',
        'robots'
    );
    protected $_entity = '\Seo\Entity\Seo';
    protected $_entityName = 'seo';

    public function getFormName() {
        return 'adminSeoStaticpageAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function save() {
        $em = $this->getEntityManager();
        $staticpageId = \Core\Functions::fromRoute('staticPageId', 0);
        $form = $this->getForm();
        $params = $form->getData();
        if (!$staticpageId) {
            return;
        }
        $staticpageUrl = \SeoStaticpage\Api\SeoStaticpageApi::getStaticpageUrl($staticpageId);
        $entity = \Seo\Api\SeoApi::getByUrl($staticpageUrl);
        if (!$entity) {
            $entity = new \Seo\Entity\Seo();
        }
        $entity->url = $staticpageUrl;
        $entity->alias = \Seo\Api\SeoApi::createSlug($params['alias']);
        $entity->pageTitle = $params['pageTitle'];
        $entity->metaKeywords = $params['metaKeywords'];
        $entity->metaDescription = $params['metaDescription'];
        $entity->robots = $params['robots'];
        $em->persist($entity);
        $em->flush();
        $resultContainer = $this->getFormResultContainer();
        $resultContainer->add($this->_entityName, $entity);
        return $entity;
    }

    public function getRecord() {
        $staticpageId = \Core\Functions::fromRoute('staticPageId', 0);
        $staticpageUrl = \SeoStaticpage\Api\SeoStaticpageApi::getStaticpageUrl($staticpageId);
        $form = $this->getForm();
        if (!$staticpageId) {
            return;
        }
        $entity = \Seo\Api\SeoApi::getByUrl($staticpageUrl);
        if (!$entity) {
            return;
        }
        $form->get('alias')->setValue($entity->alias);
        $form->get('pageTitle')->setValue($entity->pageTitle);
        $form->get('metaKeywords')->setValue($entity->metaKeywords);
        $form->get('metaDescription')->setValue($entity->metaDescription);
        $form->get('robots')->setValue($entity->robots);
    }

}
