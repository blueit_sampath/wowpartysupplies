<?php

namespace SeoStaticpage\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminSeo extends AbstractTabEvent {
	public function getEventName() {
		return 'adminStaticPageAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$staticPageId = Functions::fromRoute ( 'staticPageId' );
		if (! $staticPageId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-seo-staticpage-add', array (
				'staticPageId' => $staticPageId 
		) );
		$tabContainer->add ( 'admin-seo-staticpage-add', 'SEO', $u, 100 );
		
		return $this;
	}
}

