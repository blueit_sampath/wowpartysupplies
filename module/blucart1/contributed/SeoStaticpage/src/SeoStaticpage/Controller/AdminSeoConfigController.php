<?php

namespace SeoStaticpage\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminSeoConfigController extends AbstractAdminController {

   	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-seo-config-staticpage' );
	}
}
