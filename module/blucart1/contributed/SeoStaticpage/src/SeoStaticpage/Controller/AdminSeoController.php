<?php

namespace SeoStaticpage\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminSeoController extends AbstractAdminController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('SeoStaticpage\Form\AdminSeo\SeoFormFactory');
    }

    public function getSaveRedirector() {
        return $this->redirect()->toRoute('admin-seo-staticpage-add', array(
                    'staticPageId' => \Core\Functions::fromRoute('staticPageId')
        ));
    }

}
