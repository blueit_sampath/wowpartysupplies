<?php
namespace SeoStaticpage\Form\AdminSeo;

use Common\Form\Option\AbstractFormFactory;

class SeoFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'SeoStaticpage\Form\AdminSeo\SeoForm' );
		$form->setName ( 'adminSeoStaticpageAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'SeoStaticpage\Form\AdminSeo\SeoFilter' );
	}
}
