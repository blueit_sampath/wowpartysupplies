<?php

namespace SeoStaticpage\Form\AdminSeo;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class SeoForm extends Form {

    public function init() {
        $this->setAttribute('method', 'post');

       

        $this->add(array(
            'name' => 'alias',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Alias'
            )
                ), array(
            'priority' => 990
        ));


        $this->add(array(
            'name' => 'pageTitle',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Page Title'
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'name' => 'metaKeywords',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Meta Keywords'
            )
                ), array(
            'priority' => 970
        ));

        $this->add(array(
            'name' => 'metaDescription',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Meta Description'
            )
                ), array(
            'priority' => 960
        ));

        $this->add(array(
            'name' => 'robots',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Robots'
            )
                ), array(
            'priority' => 950
        ));


        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
