<?php
namespace SeoStaticpage\Form\AdminSeoConfigGeneral;

use Common\Form\Option\AbstractFormFactory;

class SeoConfigGeneralFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm' );
		$form->setName ( 'adminSeoStaticpageConfigGeneral' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter' );
	}
}
