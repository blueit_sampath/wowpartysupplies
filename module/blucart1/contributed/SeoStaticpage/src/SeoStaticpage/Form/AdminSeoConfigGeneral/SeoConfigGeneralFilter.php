<?php

namespace SeoStaticpage\Form\AdminSeoConfigGeneral;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SeoConfigGeneralFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'SEO_STATICPAGE_URL',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_STATICPAGE_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_STATICPAGE_DESCRIPTION',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_STATICPAGE_KEYWORDS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_STATICPAGE_ROBOTS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'forceBuild',
            'required' => false,
        ));

       
    }

}
