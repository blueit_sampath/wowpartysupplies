<?php

namespace SeoStaticpage\Api;

use Core\Functions;
use Common\Api\Api;
use \Config\Api\ConfigApi;

class SeoStaticpageApi extends Api {

    protected static $_entity = '\Seo\Entity\Seo';

    public static function buildUrls($forceBuild) {

        $em = Functions::getEntityManager();
        if ($forceBuild) {
            $dql = 'delete from ' . static::$_entity . ' u where u.url like \'/static-page/%\'';
            $query = $em->createQuery($dql);
            $query->getResult();
        }
        $staticpages = \StaticPage\Api\StaticPageApi::getAllStaticPages(null);
        foreach ($staticpages as $staticpage) {
            $params = array('staticPage' => $staticpage,'staticpage' => $staticpage);
        
            $url = static::getStaticpageUrl($staticpage->id);
            if (\Seo\Api\SeoApi::getByUrl($url)) {
                continue;
            }

            $pageTitle = '';

            if ($value = ConfigApi::getConfigByKey('SEO_STATICPAGE_TITLE')) {
                $pageTitle = $value;
            }
            $pageTitle = static::substitute($pageTitle, $params);
            $array['pageTitle'] = $pageTitle;

            $keywords = '';

            if ($value = ConfigApi::getConfigByKey('SEO_STATICPAGE_KEYWORDS')) {
                $keywords = $value;
            }

            $keywords = static::substitute($keywords, $params);
            $array['metaKeywords'] = $keywords;


            if ($value = ConfigApi::getConfigByKey('SEO_STATICPAGE_DESCRIPTION')) {
                $description = $value;
            }
            $description = static::substitute($description, $params);
            $array['metaDescription'] = $description;

            $robots = '';

            if ($value = \Config\Api\ConfigApi::getConfigByKey('SEO_STATICPAGE_ROBOTS')) {
                $robots = $value;
            }



            $robots = static::substitute($robots, $params);
            $array['robots'] = $robots;
            $alias = $staticpage->title;

            if ($value = ConfigApi::getConfigByKey('SEO_STATICPAGE_URL')) {
                $alias = $value;
            }

            $array['alias'] = static::substitute($alias, $params);
            $array['url'] = $url;
            \Seo\Api\SeoApi::insertSeoWithAlias($url, $array);
        }
    }

    public static function getStaticpageUrl($staticpageId){
        return '/static-page/'.$staticpageId;
    }
    public static function substitute($string, $params) {
        $twigPlugin = Functions::getViewHelperManager()->get('twig');
        $string = $twigPlugin($string, $params);
        $string = str_replace('\{', '{', $string);
        $string = str_replace('\}', '}', $string);
        return $string;
    }

}
