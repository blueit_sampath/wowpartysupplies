<?php

namespace SeoStaticpage;

return array(
    'router' => array(
        'routes' => array(
            'admin-seo-config-staticpage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo/config/staticpage[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'SeoStaticpage\Controller\AdminSeoConfig',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            ),
            'admin-seo-staticpage-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo/staticpage/add[/ajax/:ajax][/:staticPageId]',
                    'defaults' => array(
                        'controller' => 'SeoStaticpage\Controller\AdminSeo',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'staticPageId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
        )
    ),
    'events' => array(
        'SeoStaticpage\Service\Form\AdminSeoConfigGeneral' => 'SeoStaticpage\Service\Form\AdminSeoConfigGeneral',
        'SeoStaticpage\Service\Link\AdminSeo' => 'SeoStaticpage\Service\Link\AdminSeo',
        'SeoStaticpage\Service\Tab\AdminSeo' => 'SeoStaticpage\Service\Tab\AdminSeo',
        'SeoStaticpage\Service\Form\AdminSeo' => 'SeoStaticpage\Service\Form\AdminSeo',
    ),
    'service_manager' => array(
        'invokables' => array(
            'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm' => 'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm',
            'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter' => 'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter',
            'SeoStaticpage\Service\Form\AdminSeoConfigGeneral' => 'SeoStaticpage\Service\Form\AdminSeoConfigGeneral',
            'SeoStaticpage\Service\Link\AdminSeo' => 'SeoStaticpage\Service\Link\AdminSeo',
            'SeoStaticpage\Service\Form\AdminSeo' => 'SeoStaticpage\Service\Form\AdminSeo',
            'SeoStaticpage\Service\Tab\AdminSeo' => 'SeoStaticpage\Service\Tab\AdminSeo',
            'SeoStaticpage\Form\AdminSeo\SeoFilter' => 'SeoStaticpage\Form\AdminSeo\SeoFilter',
            'SeoStaticpage\Form\AdminSeo\SeoForm' => 'SeoStaticpage\Form\AdminSeo\SeoForm',
          
        ),
        'factories' => array(
            'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory' => 'SeoStaticpage\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory',
             'SeoStaticpage\Form\AdminSeo\SeoFormFactory' => 'SeoStaticpage\Form\AdminSeo\SeoFormFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'SeoStaticpage\Controller\AdminSeoConfig' => 'SeoStaticpage\Controller\AdminSeoConfigController',
            'SeoStaticpage\Controller\AdminSeo' => 'SeoStaticpage\Controller\AdminSeoController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view'
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __NAMESPACE__ => __DIR__ . '/../public'
            )
        )
    )
);
