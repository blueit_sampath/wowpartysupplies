<?php

namespace Feature\Form\AdminCategoryFeature;

use Feature\Api\FeatureApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CategoryFeatureForm extends Form 

{
	public function init() {
		
		$select = new Select ( 'features' );
		$select->setLabel ( 'Features' );
		$select->setValueOptions ( $this->getCartFeatures () );
		$select->setAttribute ( 'multiple', 'true' );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCartFeatures() {
		$array = array (
				'' => '' 
		);
		$results = FeatureApi::getFeatures ();
		
		foreach ( $results as $result ) {
			$feature = FeatureApi::getFeatureById ( $result ['featureId'] );
			$array [$feature->id] = $feature->title;
		}
		return $array;
	}
}