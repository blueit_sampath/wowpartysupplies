<?php
namespace Feature\Form\AdminCategoryFeature;

use Common\Form\Option\AbstractFormFactory;

class CategoryFeatureFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminCategoryFeature\CategoryFeatureForm' );
		$form->setName ( 'adminCategoryFeature' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminCategoryFeature\CategoryFeatureFilter' );
	}
}
