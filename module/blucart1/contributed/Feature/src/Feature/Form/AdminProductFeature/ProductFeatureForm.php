<?php

namespace Feature\Form\AdminProductFeature;

use Feature\Api\FeatureApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class ProductFeatureForm extends Form 

{
	public function init() {
		
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		$this->formFeatureOptions();
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function formFeatureOptions() {
		$productId = Functions::fromRoute ( 'productId' );
		$features = FeatureApi::getFeatureProductByProductId ( $productId );
		foreach ( $features as $feature ) {
		
			$featureProductId = $feature ['featureProductId'];
			
			$featureProduct = FeatureApi::getFeatureProductById ( $featureProductId );
			
			$this->add ( array (
					'name' => 'feature_' . $featureProduct->id,
					'type' => 'TagElement\Form\Element\Tag',
					
					'options' => array (
							'label' => $featureProduct->title?$featureProduct->title: $featureProduct->feature->title 
					) 
			), array (
					'priority' => 1000 
			) );
			
			
		}
	}
}