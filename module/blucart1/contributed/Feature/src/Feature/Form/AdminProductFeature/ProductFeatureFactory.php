<?php
namespace Feature\Form\AdminProductFeature;

use Common\Form\Option\AbstractFormFactory;

class ProductFeatureFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminProductFeature\ProductFeatureForm' );
		$form->setName ( 'adminProductFeatureAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminProductFeature\ProductFeatureFilter' );
	}
}
