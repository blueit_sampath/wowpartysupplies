<?php

namespace Feature\Form\AdminBulkFeature;

use Feature\Api\FeatureApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class BulkFeatureForm extends Form 

{
	public function init() {
	
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'features' );
		$select->setLabel ( 'Features' );
		$select->setAttribute('multiple', 'multiple');
		$select->setValueOptions ( $this->getCartFeatures () );
		$this->add ( $select, array (
				'priority' => 950 
		) );
		
		
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Next >' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCartFeatures() {
		$array = array ();
		$results = FeatureApi::getFeatures ();
		foreach ( $results as $result ) {
			$featureId = $result ['featureId'];
			$feature = FeatureApi::getFeatureById ( $featureId );
			$array [$feature->id] = $feature->name;
		}
		return $array;
	}
}