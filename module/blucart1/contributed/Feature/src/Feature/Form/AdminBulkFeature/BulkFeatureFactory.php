<?php
namespace Feature\Form\AdminBulkFeature;

use Common\Form\Option\AbstractFormFactory;

class BulkFeatureFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminBulkFeature\BulkFeatureForm' );
		$form->setName ( 'adminBulkFeatureAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminBulkFeature\BulkFeatureFilter' );
	}
}
