<?php

namespace Feature\Form;

use Feature\Api\FeatureApi;
use Core\Functions;
use Zend\Form\Element\MultiCheckbox;
use Config\Api\ConfigApi;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class FeatureFilterFieldset extends Fieldset implements InputFilterProviderInterface {
	protected $_inputs = array ();
	public function __construct() {
		parent::__construct ( 'features' );
		
		$this->addFeatureElements ();
	}
	public function addFeatureElements() {
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		
		$results = FeatureApi::getFeatureByCategoryId ( $categoryId );
		
		foreach ( $results as $result ) {
			
			$featureCategory = FeatureApi::getFeatureCategoryById ( $result ['featureCategoryId'] );
			$feature = $featureCategory->feature;
			
			$results2 = FeatureApi::getFeatureOptionByFeatureId ( $feature->id );
			$array = array ();
			foreach ( $results2 as $result2 ) {
				
				
				$option = FeatureApi::getFeatureOptionById ( $result2 ['featureOptionId'] );
				$array [$option->id] = $option->title;
			}
			
			
			if (count ( $array )) {
				$this->_inputs ['feature_' . $feature->id] = array (
						'required' => false 
				);
				$multiCheckbox = new MultiCheckbox ( 'feature_' . $feature->id );
				$multiCheckbox->setLabel ( $featureCategory->title ? $featureCategory->title : $feature->title );
				$multiCheckbox->setValueOptions (
						$array 
				 );
				$this->add ( $multiCheckbox );
			}
		}
	}
	
	/**
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		return $this->_inputs;
		
	}
}
