<?php

namespace Feature\Form\AdminFeatureOption;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class FeatureOptionFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            ), 'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'regex',
                    'options' => array(
                        'pattern' => "/^[a-zA-Z0-9_]+$/",
                        'messages' => array(
                            \Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers or _'
                        )
                    )
                ),
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => array(
                            $this,
                            'checkFeatureOption'
                        ),
                        'messages' => array(
                            \Zend\Validator\Callback::INVALID_VALUE => "Feature Option already exists",
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'weight',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));
    }
    
     public function checkFeatureOption($value, $data = array()) {
        $entity = \Feature\Api\FeatureApi::getFeatureOptionByName($value);
        if (!$entity) {
            return true;
        }
        if ($entity->id == $data['id']) {
            return true;
        }
        return false;
    }

}
