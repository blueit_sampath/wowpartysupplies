<?php
namespace Feature\Form\AdminFeatureOption;

use Common\Form\Option\AbstractFormFactory;

class FeatureOptionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeatureOption\FeatureOptionForm' );
		$form->setName ( 'adminFeatureOptionAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeatureOption\FeatureOptionFilter' );
	}
}
