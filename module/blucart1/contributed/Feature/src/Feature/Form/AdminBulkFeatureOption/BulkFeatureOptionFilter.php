<?php

namespace Feature\Form\AdminBulkFeatureOption;

use Feature\Api\FeatureApi;
use Core\Functions;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BulkFeatureOptionFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->formFeatureOptions();
	}
	public function formFeatureOptions() {
	$features = Functions::fromPost ( 'features',Functions::fromQuery('features') );
		if (is_string ( $features )) {
			$features = explode ( ',', $features );
		}
		foreach ( $features as $featureId ) {
			$feature = FeatureApi::getFeatureById ( $featureId );
			$options = FeatureApi::getFeatureOptionByFeatureId ( $featureId );
			$array = array ();
			
			$this->add ( array (
					'name' => 'feature_' . $featureId,
					'required' => false,
			) );
		}
	}
} 