<?php
namespace Feature\Form\AdminBulkFeatureOption;

use Common\Form\Option\AbstractFormFactory;

class BulkFeatureOptionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionForm' );
		$form->setName ( 'adminBulkFeatureOptionAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionFilter' );
	}
}
