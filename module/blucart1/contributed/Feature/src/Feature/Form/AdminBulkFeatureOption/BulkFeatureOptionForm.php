<?php

namespace Feature\Form\AdminBulkFeatureOption;

use Feature\Api\FeatureApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class BulkFeatureOptionForm extends Form 

{
	public function init() {
		
		$this->formFeatureOptions ();
		
		$this->add ( array (
				'name' => 'features',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Generate' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function formFeatureOptions() {
		$features = Functions::fromPost ( 'features',Functions::fromQuery('features') );
		if (is_string ( $features )) {
			$features = explode ( ',', $features );
		}
		
		foreach ( $features as $featureId ) {
			$feature = FeatureApi::getFeatureById ( $featureId );
			$options = FeatureApi::getFeatureOptionByFeatureId ( $featureId );
			$array = array ();
			foreach ( $options as $result ) {
				$option = FeatureApi::getFeatureOptionById ( $result ['featureOptionId'] );
				$array [$option->id] = $option->name;
			}
			$select = new Select ( 'feature_' . $featureId );
			$select->setLabel ( $feature->name . ' Options' );
			$select->setAttribute ( 'multiple', 'multiple' );
			$select->setValueOptions ( $array );
			$select->setValue ( array_keys ( $array ) );
			$this->add ( $select, array (
					'priority' => 1000 
			) );
		}
	}
}