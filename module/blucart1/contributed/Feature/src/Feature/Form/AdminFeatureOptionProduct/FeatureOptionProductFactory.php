<?php
namespace Feature\Form\AdminFeatureOptionProduct;

use Common\Form\Option\AbstractFormFactory;

class FeatureOptionProductFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductForm' );
		$form->setName ( 'adminFeatureOptionProductAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductFilter' );
	}
}
