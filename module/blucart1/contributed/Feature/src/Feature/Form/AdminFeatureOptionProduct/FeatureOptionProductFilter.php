<?php

namespace Feature\Form\AdminFeatureOptionProduct;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class FeatureOptionProductFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'name',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
// 				'validators' => array (
// 						array (
// 								'name' => 'Regex',
// 								'options' => array (
// 										'pattern' => '/^[A-Za-z0-9]+$/' 
// 								) 
// 						) 
// 				) 
		) );
		
		
	
		
		
		
	}
} 