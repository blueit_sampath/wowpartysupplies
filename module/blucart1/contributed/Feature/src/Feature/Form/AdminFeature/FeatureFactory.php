<?php
namespace Feature\Form\AdminFeature;

use Common\Form\Option\AbstractFormFactory;

class FeatureFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeature\FeatureForm' );
		$form->setName ( 'adminFeatureAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeature\FeatureFilter' );
	}
}
