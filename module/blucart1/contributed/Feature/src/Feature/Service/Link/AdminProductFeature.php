<?php

namespace Feature\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProductFeature extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminFeatureProductAdd';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$productId = Functions::fromRoute ( 'productId' );
		$u = $url ( 'admin-product-feature', array (
				'productId' => $productId 
		) );
		$item = $linkContainer->add ( 'admin-product-feature', 'Features', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-product-feature-bulk', array (
				'productId' => $productId
		) );
		$item = $linkContainer->add ( 'admin-product-feature-bulk', 'Add Features', $u, 800 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
	
	
}

