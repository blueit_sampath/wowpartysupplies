<?php 
namespace Feature\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminFeature extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminFeature';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-feature-add');
		$item = $linkContainer->add ( 'Add Feature', 'Add Feature', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

