<?php

namespace Feature\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminFeatureOption extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminFeatureOption';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$featureId = Functions::fromRoute ( 'featureId' );
		$u = $url ( 'admin-feature-option-add', array (
				'featureId' => $featureId 
		) );
		$item = $linkContainer->add ( 'admin-feature-option-add', 'Add Option', $u, 1000 );
		$item->setLinkClass ( 'zoombox' );
		
		return $this;
	}
}

