<?php 
namespace Feature\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategoryFeatureLink extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategoryFeature';
	}
	public function link() {
		$categoryId = Functions::fromRoute('categoryId');
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-category-feature-add', array('categoryId' => $categoryId));
		$item = $linkContainer->add ( 'admin-category-feature-add', 'Add Feature', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

