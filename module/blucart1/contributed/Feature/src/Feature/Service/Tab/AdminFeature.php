<?php

namespace Feature\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminFeature extends AbstractTabEvent {
	public function getEventName() {
		return 'adminFeatureAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$featureId = Functions::fromRoute ( 'featureId', null );
		if ($featureId) {
			$u = $url ( 'admin-feature-add', array (
					'featureId' => $featureId 
			) );
			$tabContainer->add ( 'admin-feature-add', 'General Information', $u, 1000 );
		}
		$featureId = Functions::fromRoute ( 'featureId', null );
		if ($featureId) {
			$u = $url ( 'admin-feature-option', array (
					'featureId' => $featureId 
			) );
			$tabContainer->add ( 'admin-feature-option', 'Feature Option', $u, 1000 );
		}
		
		return $this;
	}
}

