<?php

namespace Feature\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategoryFeature extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$categoryId = Functions::fromRoute ( 'categoryId' );
		if ($categoryId) {
			$tabContainer = $this->getTabContainer ();
			$url = Functions::getUrlPlugin ();
			
			$u = $url ( 'admin-category-feature', array (
					'categoryId' => $categoryId 
			) );
			$tabContainer->add ( 'admin-category-feature', 'Features', $u, 800 );
		}
		
		return $this;
	}
}

