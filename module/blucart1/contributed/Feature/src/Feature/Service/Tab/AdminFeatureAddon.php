<?php

namespace Addon\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminFeatureAddon extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$categoryId = Functions::fromRoute ( 'categoryId' );
		if ($categoryId) {
			$tabContainer = $this->getTabContainer ();
			$url = Functions::getUrlPlugin ();
			
			$u = $url ( 'admin-category-addon', array (
					'categoryId' => $categoryId 
			) );
			$tabContainer->add ( 'admin-category-addon', 'Addons', $u, 800 );
		}
		
		return $this;
	}
}

