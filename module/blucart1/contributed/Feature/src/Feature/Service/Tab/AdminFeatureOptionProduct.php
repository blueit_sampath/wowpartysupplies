<?php

namespace Feature\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminFeatureOptionProduct extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId' );
		if ($productId) {
			
			$u = $url ( 'admin-product-feature-add', array (
					'productId' => $productId 
			) );
			$tabContainer->add ( 'admin-product-feature-add', 'Features', $u, 800 );
		}
		return $this;
	}
}

