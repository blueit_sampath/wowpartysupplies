<?php 
namespace Feature\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminFeatureNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'productMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Features',
							'route' => 'admin-feature',
							'id' => 'admin-feature',
							'iconClass' => 'glyphicon-leaf' 
					) 
			) );
		}
	}
}

