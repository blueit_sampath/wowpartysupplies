<?php

namespace Feature\Service\Form;

use Feature\Api\FeatureApi;

use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminProductFeature extends AbstractMainFormEvent {
	protected $_columnKeys = array (
		
	);
	protected $_entity = '\Feature\Entity\FeatureOptionProduct';
	protected $_entityName = 'featureOptionProduct';
	public function getFormName() {
		return 'adminProductFeatureAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		if ($params ['product']) {
			$entity->product = ProductApi::getProductById ( $params ['product'] );
		}
	}
	
	public function save(){
		$form = $this->getForm ();
		$form = $this->getForm ();
		
		$productId = Functions::fromRoute ( 'productId' );
		$features = FeatureApi::getFeatureProductByProductId ( $productId );
		$results = array();
		foreach ( $features as $feature ) {
			$featureProductId = $feature ['featureProductId'];
                        
			$featureProduct = FeatureApi::getFeatureProductById ( $featureProductId );
			$feature = $featureProduct->feature;
			
			$options = FeatureApi::getFeatureOptionProductByProductIdFeatureId($productId, $feature->id);
			
			$temp = $form->get('feature_' . $featureProduct->id)->getValue();
			if(!$temp){
				continue;
			}
			
			$options = explode(',', $temp);
			
			$temp = array();
			foreach ($options as $option){
			$entity = FeatureApi::checkOrSaveFeatureOptionProduct($productId, $option, $feature->id);
			$results[] = $entity->id;
			}
                       
		}
		$this->deleteExcessResults($results, $productId);
	}
	
	public function deleteExcessResults($results, $productId){
		if(!count($results)){
			return true;
		}
		$em = $this->getEntityManager();
		$dql = 'delete from '.$this->_entity.' u where u.id not in ('.implode(',',$results).') and u.product = '.$productId;
		$query = $em->createQuery($dql);
		return $query->getResult();
	}
	
	public function getRecord() {
		$form = $this->getForm ();
		
		$productId = Functions::fromRoute ( 'productId' );
		$features = FeatureApi::getFeatureProductByProductId ( $productId );
		foreach ( $features as $feature ) {
			$featureProductId = $feature ['featureProductId'];
			$featureProduct = FeatureApi::getFeatureProductById ( $featureProductId );
			$feature = $featureProduct->feature;
			
			$options = FeatureApi::getFeatureOptionProductByProductIdFeatureId($productId, $feature->id);
			$temp = array();
			foreach ($options as $optionResult){
				$optionId = $optionResult['featureOptionProductId'];
				$option = FeatureApi::getFeatureOptionProductById($optionId);
				
				$temp[] = $option->featureOption->title;
				
			}
			
			$form->get('feature_' . $featureProduct->id)->setValue(implode(',',$temp));
			
			
		}
	}
}
