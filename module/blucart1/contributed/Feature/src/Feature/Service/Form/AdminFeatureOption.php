<?php

namespace Feature\Service\Form;

use Feature\Api\FeatureApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminFeatureOption extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'title',
			'weight',
			
	);
	protected $_entity = '\Feature\Entity\FeatureOption';
	protected $_entityName = 'featureOption';
	public function getFormName() {
		return 'adminFeatureOptionAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		if ($params ['feature']) {
			$entity->feature = FeatureApi::getFeatureById ( $params ['feature'] );
		}
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$form->get ( 'feature' )->setValue ( Functions::fromRoute ( 'featureId', 0 ) );
	}
        public function afterGetRecord($entity) {
		$form = $this->getForm ();
		
                $form->get ( 'name' )->setAttribute ( 'readonly',true );
	}
}
