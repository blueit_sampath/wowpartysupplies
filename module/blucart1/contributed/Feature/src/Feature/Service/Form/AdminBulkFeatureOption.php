<?php

namespace Feature\Service\Form;

use Feature\Api\FeatureApi;
use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminBulkFeatureOption extends AbstractMainFormEvent {
	protected $_entity = '\Feature\Entity\FeatureOptionProduct';
	protected $_entityName = 'featureOptionProduct';
	public function getFormName() {
		return 'adminBulkFeatureOptionAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$em = Functions::getEntityManager ();
		
		$form = $this->getForm ();
		$productId = Functions::fromRoute ( 'productId' );
		$features_string = $form->get ( 'features' )->getValue ();
		
		$features = explode ( ',', $features_string );
		$product = ProductApi::getProductById ( $productId );
		
		$buildOptions = array ();
		
		foreach ( $features as $featureId ) {
			$feature = FeatureApi::getFeatureById ( $featureId );
			if (! FeatureApi::checkOrGetFeatureByProductIdFeatureId ( $productId, $featureId )) {
				$entity = new \Feature\Entity\FeatureProduct ();
				$entity->feature = $feature;
				$entity->product = $product;
			
				$em->persist ( $entity );
			}
			
			$buildOptions [] = $form->get ( 'feature_' . $featureId )->getValue ();
		}
		
		$this->buildOptions ( $buildOptions, $productId );
		
		$em->flush ();
	}
	
	public function getRecord() {
		$form = $this->getForm ();
		$featureOptions = $form->get ( 'features' )->setValue ( implode ( ',', Functions::fromQuery ( 'features' ) ) );
	}
	public function buildOptions($buildOptions, $productId) {
		$em = $this->getEntityManager ();
		$product = ProductApi::getProductById ( $productId );
		
		foreach($buildOptions as $array){
			foreach ($array as $a){
				if (! ($model = FeatureApi::checkOrGetFeatureOptionByProductIdAndFeatureOptionId($productId, $a))) {
					$model = new $this->_entity ();
					$model->featureOption = FeatureApi::getFeatureOptionById($a);
					$model->product = $product;
					$em->persist ( $model );
					$em->flush ();
				}
				
			}
		}
		return true;
	}
	
}
