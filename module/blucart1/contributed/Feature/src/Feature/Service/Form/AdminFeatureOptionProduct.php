<?php

namespace Feature\Service\Form;

use File\Api\FileApi;
use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminFeatureOptionProduct extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			 
	);
	protected $_entity = '\Feature\Entity\FeatureOptionProduct';
	protected $_entityName = 'featureOptionProduct';
	public function getFormName() {
		return 'adminFeatureOptionProductAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		
		
		if ($params ['product']) {
			$entity->product = ProductApi::getProductById ( $params ['product'] );
		}
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$form->get ( 'product' )->setValue ( Functions::fromRoute ( 'productId', 0 ) );
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		
	}
}
