<?php 
namespace Feature\Service\Form;

use Feature\Entity\FeatureCategory;

use Feature\Api\FeatureApi;

use Category\Api\CategoryApi;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminCategoryFeature extends AbstractMainFormEvent {
	
	protected $_entity = '\Feature\Entity\FeatureCategory';
	protected $_entityName = 'featureCategory';
	public function getFormName() {
		return 'adminCategoryFeature';
	}
	public function getPriority() {
		return 1000;
	}
	
	
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		$categoryId = Functions::fromRoute('categoryId');
	
		if ($categoryId) {
			$category = CategoryApi::getCategoryById ( $categoryId );
				
			if ($category) {
				$results = FeatureApi::getFeatureByCategoryId ( $category->id );
				$array = array ();
				if ($results) {
					foreach ( $results as $result ) {
						$array [] = ( int ) $result ['featureId'];
					}
				}
				foreach ( $params ['features'] as $r ) {
					if (! in_array ( ( int ) $r, $array )) {
						$e = new FeatureCategory ();
						$e->category = $category;
						$e->feature = FeatureApi::getFeatureById ( $r );
						$em->persist ( $e );
					}
				}
	
				$em->flush ();
			}
		}
	
		return true;
	}
	
	
}
