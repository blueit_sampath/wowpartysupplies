<?php 
namespace Feature\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class CategoryFilterProductFeature extends AbstractMainFormEvent {
	
	public function getFormName() {
		return 'categoryFilterProduct';
	}
	public function getPriority() {
		return 800;
	}
	
	public function preInitEvent() {
		$form = $this->getForm();
		$form->add(array(
				'type' => 'Feature\Form\FeatureFilterFieldset',
				
		));
	
	}
}
