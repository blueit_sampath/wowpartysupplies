<?php 
namespace Feature\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class ProductFeature extends AbstractContentEvent {

	protected $_contentTemplate = 'common/product-feature';
	protected $_contentName = 'productFeature';
	public function getEventName() {
		return 'productAdditionalInformation';
	}
	public function getPriority() {
		return null;
	}
	
	
}

