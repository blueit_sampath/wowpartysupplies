<?php

namespace Feature\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;

use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminCategoryFeature extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'type',
			'weight' 
	);
	protected $_entity = '\Feature\Entity\FeatureCategory';
	protected $_entityName = 'featureCategory';
	public function getEventName() {
		return 'adminCategoryFeature';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		$array = array ();
		$grid = $this->getGrid ();
		$grid->addAdditionalParameter ( 'categoryId', $categoryId );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'featureCategory.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Overriden Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'featureCategory.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'featureTitle' );
		$columnItem->setTitle ( 'Feature Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'feature.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'weight' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'featureCategory.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$categoryId = Functions::fromQuery ( 'categoryId', 0 );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem( $this->_entityName . '.category', 'category' );
		$item->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem( $this->_entityName . '.feature', 'feature' );
		$item->addJoin ( $joinItem );
		
		$item = $queryBuilder->addWhere ( 'category.id', 'categoryId' );
		$queryBuilder->addParameter ( 'categoryId', $categoryId );
		
		return true;
	}
}
