<?php

namespace Feature\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;

use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminFeatureOptionProduct extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			
	);
	protected $_entity = 'Feature\Entity\FeatureOptionProduct';
	protected $_entityName = 'featureOptionProduct';
	public function getEventName() {
		return 'adminFeatureOptionProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		$array = array ();
		$grid = $this->getGrid ();
		$grid->setAdditionalParameters ( array (
				'productId' => $productId 
		) );
		
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setIsPrimary ( true );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'featureOptionProduct.id', $columnItem );
		
	
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'featureOptionName' );
		$columnItem->setTitle ( 'Default Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'featureOption.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'featureName' );
		$columnItem->setTitle ( 'Feature' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'feature.name', $columnItem );
		
		
		
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$productId = Functions::fromQuery ( 'productId', 0 );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem( $this->_entityName . '.product', 'product' );
		$item->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem( $this->_entityName . '.featureOption', 'featureOption' );
		$item->addJoin ( $joinItem );
		$joinItem2 = new QueryJoinItem(  'featureOption.feature', 'feature' );
		$joinItem->addJoin ( $joinItem2 );
	
		$item = $queryBuilder->addWhere ( 'product.id', 'productId' );
		$queryBuilder->addParameter ( 'productId', $productId );
	
		return true;
	}
}
