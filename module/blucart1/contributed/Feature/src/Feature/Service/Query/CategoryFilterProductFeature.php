<?php

namespace Feature\Service\Query;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use Feature\Api\FeatureApi;
use Core\Functions;
use QueryBuilder\Option\Event\AbstractQueryBuilderEvent;

class CategoryFilterProductFeature extends AbstractQueryBuilderEvent {
	public function getEventName() {
		return 'ProductCategory\Api\ProductCategoryApi.getProductsByCategoryId';
	}
	public function getPriority() {
		return 500;
	}
	public function preQuery() {
		parent::preQuery ();
		$queryBuilder = $this->getQueryBuilder ();
		
		$sm = Functions::getServiceLocator ();
		$form = $sm->get ( 'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFactory' );
		$form->isValid ();
		$data = $form->getData ();
		
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		
		$results = FeatureApi::getFeatureByCategoryId ( $categoryId );
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhereItem ();
		$where->setLogic(' and ');
		
		
		foreach ( $results as $result ) {
			
			$featureCategory = FeatureApi::getFeatureCategoryById ( $result ['featureCategoryId'] );
			$feature = $featureCategory->feature;
			
			$results2 = FeatureApi::getFeatureOptionByFeatureId ( $feature->id );
			
			if (isset ( $data ['features'] ['feature_' . $feature->id] )) {
				$array = $data ['features'] ['feature_' . $feature->id];
				
				$whereItem = new QueryWhereItem ();
				
				$where->addWhere ( $whereItem, 'feature_' . $feature->id );
				
				foreach ( $array as $value ) {
					$whereItem2 = new QueryWhereItem ();
					$whereItem->addWhere ( $whereItem2, 'feature_' . $feature->id . '_' . $value );
					$where1 = new QueryWhereItem ( 'featureOptionProduct.featureOption', 'featureOption' . $value, ' = ' );
					$queryBuilder->addParameter ( 'featureOption' . $value , $value  );
					$whereItem2->addWhere ( $where1 );
		
				}
			}
		}
		if(count($where->getWheres())){
			$wheres->addWhere ( $where, 'features' );
		}
		
		$item = $queryBuilder->getFrom ( 'productCategory' );
		$joinItem = new QueryJoinItem ( '\Feature\Entity\FeatureOptionProduct', 'featureOptionProduct', 'left join', 'featureOptionProduct.product = productCategory.product' );
		$item->addJoin ( $joinItem );
		
	}
}

