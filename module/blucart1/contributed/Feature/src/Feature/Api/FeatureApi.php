<?php

namespace Feature\Api;

use Product\Api\ProductApi;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Common\Api\Api;

class FeatureApi extends Api {

    protected static $_entity = '\Feature\Entity\Feature';
    protected static $_entityOption = '\Feature\Entity\FeatureOption';
    protected static $_entityOptionProduct = '\Feature\Entity\FeatureOptionProduct';
    protected static $_entityProduct = '\Feature\Entity\FeatureProduct';
    protected static $_entityCategory = '\Feature\Entity\FeatureCategory';
    protected static $_tempArray = array();

    public static function getFeatureByName($name) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
                    'name' => $name
        ));
    }

   

    public static function getFeatureOptionByName($name) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityOption)->findOneBy(array(
                    'name' => $name
        ));
    }

    public static function getFeatureById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getFeatures() {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('feature.id', 'featureId');

        $item = $queryBuilder->addFrom(static::$_entity, 'feature');
        return $queryBuilder->executeQuery();
    }

    public static function getFeatureCategoryById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityCategory, $id);
    }

    public static function getFeatureByCategoryId($categoryId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('featureCategory.id', 'featureCategoryId');

        $queryBuilder->addWhere('featureCategory.category', 'featureCategory');
        $queryBuilder->addParameter('featureCategory', $categoryId);

        $item = $queryBuilder->addFrom(static::$_entityCategory, 'featureCategory');
        $joinItem = new QueryJoinItem('featureCategory.feature', 'feature');
        $item->addJoin($joinItem);
        $queryBuilder->addOrder('featureCategory.weight', 'weight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getFeatureProductByProductId($productId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('featureProduct.id', 'featureProductId');

        $queryBuilder->addWhere('featureProduct.product', 'featureProductProduct');
        $queryBuilder->addParameter('featureProductProduct', $productId);

        $item = $queryBuilder->addFrom(static::$_entityProduct, 'featureProduct');
        $queryBuilder->addOrder('featureProduct.weight', 'weight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getFeatureProductById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityProduct, $id);
    }

    public static function getFeatureOptionProductByProductIdFeatureId($productId, $featureId) {

        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('featureOptionProduct.id', 'featureOptionProductId');

        $queryBuilder->addWhere('featureOptionProduct.product', 'featureOptionProductProduct');
        $queryBuilder->addParameter('featureOptionProductProduct', $productId);

        $queryBuilder->addWhere('feature.id', 'featureId');
        $queryBuilder->addParameter('featureId', $featureId);

        $item = $queryBuilder->addFrom(static::$_entityOptionProduct, 'featureOptionProduct');
        $joinItem = new QueryJoinItem('featureOptionProduct.featureOption', 'featureOption');
        $joinItem2 = new QueryJoinItem('featureOption.feature', 'feature');
        $joinItem->addJoin($joinItem2);
        $item->addJoin($joinItem);
        $queryBuilder->addOrder('featureOption.weight', 'featureOptionWeight', 'desc');


        return $queryBuilder->executeQuery();
    }

    public static function getFeatureOptionProductById($id) {
        $em = Functions::getEntityManager();

        return $em->find(static::$_entityOptionProduct, $id);
    }

    public static function getFeatureOptionProductByProductIdFeatureOptionName($productId, $name) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('featureOptionProduct.id', 'featureOptionProductId');

        $queryBuilder->addWhere('featureOptionProduct.product', 'featureOptionProductProduct');
        $queryBuilder->addParameter('featureOptionProductProduct', $productId);

        $queryBuilder->addWhere('featureOption.name', 'featureOptionName');
        $queryBuilder->addParameter('featureOptionName', $name);

        $item = $queryBuilder->addFrom(static::$_entityOptionProduct, 'featureOptionProduct');
        $joinItem = new QueryJoinItem('featureOptionProduct.featureOption', 'featureOption');
        // $joinItem2 = new QueryJoinItem ( 'featureOption.feature', 'feature'
        // );
        // $joinItem->addJoin ( $joinItem2 );
        $item->addJoin($joinItem);
        return $queryBuilder->executeQuery();
    }

    public static function checkOrSaveFeatureOptionProduct($productId, $name, $featureId) {
        $em = static::getEntityManager();
        $results = static::getFeatureOptionProductByProductIdFeatureOptionName($productId, $name);

        if ($results) {
            if (count($results)) {
                $result = array_pop($results->getArrayCopy());


                return static::getFeatureOptionProductById($result['featureOptionProductId']);
            }
        }

        $featureOption = static::checkOrSaveFeatureOption($featureId, $name);


        $product = ProductApi::getProductById($productId);

        $model = new static::$_entityOptionProduct ();
        $model->featureOption = $featureOption;
        $model->product = $product;
        $em->persist($model);
        $em->flush();
        return $model;
    }

    public static function checkOrSaveFeatureOption($featureId, $name) {
        $em = static::getEntityManager();
        $model = static::getFeatureOptionByFeatureAndName($featureId, $name);
        if ($model) {
            return $model;
        }
        $model = new static::$_entityOption ();
        $model->name = $name;
        $model->title = $name;
        $model->feature = static::getFeatureById($featureId);
        $em->persist($model);
        $em->flush();
        return $model;
    }

    public static function getFeatureOptionById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityOption, $id);
    }

    public static function getFeatureOptionByFeatureAndName($featureId, $name) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityOption)->findOneBy(array(
                    'feature' => $featureId,
                    'name' => $name
        ));
    }

    public static function getFeatureOptionByFeatureId($featureId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('featureOption.id', 'featureOptionId');

        $queryBuilder->addWhere('featureOption.feature', 'featureOptionFeature');
        $queryBuilder->addParameter('featureOptionFeature', $featureId);

        $item = $queryBuilder->addFrom(static::$_entityOption, 'featureOption');
        return $queryBuilder->executeQuery();
    }

    // ---------------------
    public static function checkOrGetFeatureByProductIdFeatureId($productId, $featureId) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityProduct)->findOneBy(array(
                    'product' => $productId,
                    'feature' => $featureId
        ));
    }

    public static function checkOrGetFeatureOptionByProductIdAndFeatureOptionId($productId, $featureOptionId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entityOptionProduct)->findOneBy(array(
                    'product' => $productId,
                    'featureOption' => $featureOptionId
        ));
    }

}
