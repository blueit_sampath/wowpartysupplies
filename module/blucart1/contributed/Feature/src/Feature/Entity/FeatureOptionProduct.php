<?php

namespace Feature\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * FeatureOptionProduct
 *
 * @ORM\Table(name="feature_option_product")
 * @ORM\Entity
 */
class FeatureOptionProduct {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	
	
	/**
	 *
	 * @var \Product @ORM\ManyToOne(targetEntity="\Product\Entity\Product")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="product_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $product;
	
	/**
	 *
	 * @var FeatureOption @ORM\ManyToOne(targetEntity="\Feature\Entity\FeatureOption")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="feature_option_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $featureOption;
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
}
