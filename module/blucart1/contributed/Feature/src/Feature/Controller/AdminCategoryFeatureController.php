<?php 
namespace Feature\Controller;
use Core\Functions;

use Common\MVC\Controller\AbstractAdminController;

class AdminCategoryFeatureController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminCategoryFeature\CategoryFeatureFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-category-feature-add', array (
				'categoryId' => Functions::fromRoute('categoryId')
		) );
	}
	
	
}