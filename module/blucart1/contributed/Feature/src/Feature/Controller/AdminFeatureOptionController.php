<?php

namespace Feature\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminFeatureOptionController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeatureOption\FeatureOptionFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-feature-option-add', array (
				'featureId' => $form->get ( 'feature' )->getValue (),
				'featureOptionId' => $form->get ( 'id' )->getValue () 
		) );
	}
}