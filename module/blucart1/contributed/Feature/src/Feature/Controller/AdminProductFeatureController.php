<?php

namespace Feature\Controller;

use Core\Functions;

use Common\MVC\Controller\AbstractAdminController;

class AdminProductFeatureController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminProductFeature\ProductFeatureFactory' );
	}
	public function getSaveRedirector() {
		$productId = Functions::fromRoute('productId');
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-feature-add', array (
				'productId' => $productId
				
		) );
	}
}