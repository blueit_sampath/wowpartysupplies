<?php

namespace Feature\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminFeatureController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeature\FeatureFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-feature-add', array (
				'featureId' => $form->get ( 'id' )->getValue () 
		) );
	}
}