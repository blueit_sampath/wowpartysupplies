<?php

namespace Feature\Controller;

use Core\Functions;
use Common\MVC\Controller\AbstractAdminController;

class AdminBulkFeatureController extends AbstractAdminController {
	public function featureAction() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminBulkFeature\BulkFeatureFactory' );
		$productId = Functions::fromRoute ( 'productId' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$features = $form->get ( 'features' )->getValue ();
				return $this->redirect ()->toRoute ( 'admin-product-feature-option-bulk', array (
						'productId' => $productId 
				), array (
						'query' => array (
								'features' => $features 
						) 
				) );
			} else {
				$this->notValid ();
			}
		}
		return array (
				'form' => $form 
		);
	}
	public function featureOptionAction() {
		$form = $this->getServiceLocator ()->get ( 'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionFactory' );
		$productId = Functions::fromRoute ( 'productId' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Generated feature options successfully' );
				return $this->redirect ()->toRoute ( 'admin-product-feature-bulk', array (
						'productId' => $productId 
				) );
			} else {
				$this->notValid ();
			}
		}else{
			$form->getRecord();
		}
		
		return array (
				'form' => $form 
		);
	}
	
	
}