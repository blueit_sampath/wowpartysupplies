<?php 
namespace Feature\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminFeatureOptionProductController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-feature-option-add', array (
				'productId' => $form->get ( 'product' )->getValue (),
				'featureOptionProductId' => $form->get ( 'id' )->getValue ()
		) );
	}
	
	
}