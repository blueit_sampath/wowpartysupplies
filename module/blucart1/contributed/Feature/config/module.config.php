<?php
return array (
		'router' => array (
				'routes' => array (
						'admin-feature' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/feature',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminFeature',
												'action' => 'index' 
										) 
								) 
						),
						'admin-feature-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/feature/add[/ajax/:ajax][/:featureId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminFeature',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'featureId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-feature-option' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/feature/option[/ajax/:ajax][/:featureId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminFeatureOption',
												'action' => 'index',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'featureId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-feature-option-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/feature/option/add[/ajax/:ajax][/:featureId][/:featureOptionId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminFeatureOption',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'featureId' => '[0-9]+',
												'featureOptionId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-product-feature-option' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/feature/option[/ajax/:ajax][/:productId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminFeatureOptionProduct',
												'action' => 'index',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'productId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-product-feature-option-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/feature/option/add[/ajax/:ajax][/:productId][/:featureOptionProductId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminFeatureOptionProduct',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'productId' => '[0-9]+',
												'featureOptionProductId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-product-feature' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/feature[/ajax/:ajax][/:productId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminProductFeature',
												'action' => 'index',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'productId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-product-feature-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/feature/add[/ajax/:ajax][/:productId][/:featureProductId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminProductFeature',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'productId' => '[0-9]+',
												'featureProductId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-product-feature-bulk' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/feature/bulk[/ajax/:ajax][/:productId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminBulkFeature',
												'action' => 'feature',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'productId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-product-feature-option-bulk' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/feature/option/bulk[/ajax/:ajax][/:productId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminBulkFeature',
												'action' => 'feature-option',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'productId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-category-feature' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/category/feature[/ajax/:ajax][/:categoryId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminCategoryFeature',
												'action' => 'index',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'categoryId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-category-feature-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/category/feature/add[/ajax/:ajax][/:categoryId][/:featureCategoryId]',
										'defaults' => array (
												'controller' => 'Feature\Controller\AdminCategoryFeature',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'categoryId' => '[0-9]+',
												'featureCategoryId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'Feature\Service\Grid\AdminFeature' => 'Feature\Service\Grid\AdminFeature',
				'Feature\Service\Form\AdminFeature' => 'Feature\Service\Form\AdminFeature',
				'Feature\Service\Link\AdminFeature' => 'Feature\Service\Link\AdminFeature',
				'Feature\Service\Tab\AdminFeature' => 'Feature\Service\Tab\AdminFeature',
				'Feature\Service\Grid\AdminFeatureOption' => 'Feature\Service\Grid\AdminFeatureOption',
				'Feature\Service\Grid\AdminFeatureOptionProduct' => 'Feature\Service\Grid\AdminFeatureOptionProduct',
				'Feature\Service\Form\AdminFeatureOptionProduct' => 'Feature\Service\Form\AdminFeatureOptionProduct',
				'Feature\Service\Tab\AdminFeatureOptionProduct' => 'Feature\Service\Tab\AdminFeatureOptionProduct',
				'Feature\Service\Form\AdminFeatureOption' => 'Feature\Service\Form\AdminFeatureOption',
				'Feature\Service\Link\AdminFeatureOption' => 'Feature\Service\Link\AdminFeatureOption',
				'Feature\Service\Grid\AdminProductFeature' => 'Feature\Service\Grid\AdminProductFeature',
				'Feature\Service\Form\AdminProductFeature' => 'Feature\Service\Form\AdminProductFeature',
				'Feature\Service\Link\AdminProductFeature' => 'Feature\Service\Link\AdminProductFeature',
				'Feature\Service\Form\AdminBulkFeatureOption' => 'Feature\Service\Form\AdminBulkFeatureOption',
				'Feature\Service\Content\AdminFeatureProductView' => 'Feature\Service\Content\AdminFeatureProductView',
				'Feature\Service\Grid\AdminProduct' => 'Feature\Service\Grid\AdminProduct',
				'Feature\Service\Navigation\AdminFeatureNavigation' => 'Feature\Service\Navigation\AdminFeatureNavigation',
				'Feature\Service\Form\AdminCategoryFeature' => 'Feature\Service\Form\AdminCategoryFeature',
				'Feature\Service\Grid\AdminCategoryFeature' => 'Feature\Service\Grid\AdminCategoryFeature',
				'Feature\Service\Tab\AdminCategoryFeature' => 'Feature\Service\Tab\AdminCategoryFeature',
				'Feature\Service\Link\AdminCategoryFeatureLink' => 'Feature\Service\Link\AdminCategoryFeatureLink',
				'Feature\Service\Form\CategoryFilterProductFeature' => 'Feature\Service\Form\CategoryFilterProductFeature',
				'Feature\Service\Query\CategoryFilterProductFeature' => 'Feature\Service\Query\CategoryFilterProductFeature',
				'Feature\Service\Content\ProductFeature' => 'Feature\Service\Content\ProductFeature' 
		),
		'service_manager' => array (
				'invokables' => array (
						'Feature\Service\Grid\AdminFeature' => 'Feature\Service\Grid\AdminFeature',
						'Feature\Form\AdminFeature\FeatureForm' => 'Feature\Form\AdminFeature\FeatureForm',
						'Feature\Form\AdminFeature\FeatureFilter' => 'Feature\Form\AdminFeature\FeatureFilter',
						'Feature\Service\Form\AdminFeature' => 'Feature\Service\Form\AdminFeature',
						'Feature\Service\Link\AdminFeature' => 'Feature\Service\Link\AdminFeature',
						'Feature\Service\Tab\AdminFeature' => 'Feature\Service\Tab\AdminFeature',
						'Feature\Service\Grid\AdminFeatureOption' => 'Feature\Service\Grid\AdminFeatureOption',
						'Feature\Service\Grid\AdminFeatureOptionProduct' => 'Feature\Service\Grid\AdminFeatureOptionProduct',
						'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductForm' => 'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductForm',
						'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductFilter' => 'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductFilter',
						'Feature\Service\Form\AdminFeatureOptionProduct' => 'Feature\Service\Form\AdminFeatureOptionProduct',
						'Feature\Service\Tab\AdminFeatureOptionProduct' => 'Feature\Service\Tab\AdminFeatureOptionProduct',
						'Feature\Form\AdminFeatureOption\FeatureOptionForm' => 'Feature\Form\AdminFeatureOption\FeatureOptionForm',
						'Feature\Form\AdminFeatureOption\FeatureOptionFilter' => 'Feature\Form\AdminFeatureOption\FeatureOptionFilter',
						'Feature\Service\Form\AdminFeatureOption' => 'Feature\Service\Form\AdminFeatureOption',
						'Feature\Service\Link\AdminFeatureOption' => 'Feature\Service\Link\AdminFeatureOption',
						'Feature\Service\Grid\AdminProductFeature' => 'Feature\Service\Grid\AdminProductFeature',
						'Feature\Form\AdminProductFeature\ProductFeatureForm' => 'Feature\Form\AdminProductFeature\ProductFeatureForm',
						'Feature\Form\AdminProductFeature\ProductFeatureFilter' => 'Feature\Form\AdminProductFeature\ProductFeatureFilter',
						'Feature\Service\Form\AdminProductFeature' => 'Feature\Service\Form\AdminProductFeature',
						'Feature\Service\Link\AdminProductFeature' => 'Feature\Service\Link\AdminProductFeature',
						'Feature\Form\AdminBulkFeature\BulkFeatureForm' => 'Feature\Form\AdminBulkFeature\BulkFeatureForm',
						'Feature\Form\AdminBulkFeature\BulkFeatureFilter' => 'Feature\Form\AdminBulkFeature\BulkFeatureFilter',
						'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionForm' => 'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionForm',
						'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionFilter' => 'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionFilter',
						'Feature\Service\Form\AdminBulkFeatureOption' => 'Feature\Service\Form\AdminBulkFeatureOption',
						'Feature\Service\Content\AdminFeatureProductView' => 'Feature\Service\Content\AdminFeatureProductView',
						'Feature\Service\Grid\AdminProduct' => 'Feature\Service\Grid\AdminProduct',
						'Feature\Service\Navigation\AdminFeatureNavigation' => 'Feature\Service\Navigation\AdminFeatureNavigation',
						'Feature\Form\AdminCategoryFeature\CategoryFeatureForm' => 'Feature\Form\AdminCategoryFeature\CategoryFeatureForm',
						'Feature\Form\AdminCategoryFeature\CategoryFeatureFilter' => 'Feature\Form\AdminCategoryFeature\CategoryFeatureFilter',
						'Feature\Service\Form\AdminCategoryFeature' => 'Feature\Service\Form\AdminCategoryFeature',
						'Feature\Service\Grid\AdminCategoryFeature' => 'Feature\Service\Grid\AdminCategoryFeature',
						'Feature\Service\Tab\AdminCategoryFeature' => 'Feature\Service\Tab\AdminCategoryFeature',
						'Feature\Service\Link\AdminCategoryFeatureLink' => 'Feature\Service\Link\AdminCategoryFeatureLink',
						'Feature\Service\Form\CategoryFilterProductFeature' => 'Feature\Service\Form\CategoryFilterProductFeature',
						'Feature\Service\Query\CategoryFilterProductFeature' => 'Feature\Service\Query\CategoryFilterProductFeature',
						'Feature\Service\Content\ProductFeature' => 'Feature\Service\Content\ProductFeature' 
				),
				'factories' => array (
						'Feature\Form\AdminFeature\FeatureFactory' => 'Feature\Form\AdminFeature\FeatureFactory',
						'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductFactory' => 'Feature\Form\AdminFeatureOptionProduct\FeatureOptionProductFactory',
						'Feature\Form\AdminFeatureOption\FeatureOptionFactory' => 'Feature\Form\AdminFeatureOption\FeatureOptionFactory',
						'Feature\Form\AdminProductFeature\ProductFeatureFactory' => 'Feature\Form\AdminProductFeature\ProductFeatureFactory',
						'Feature\Form\AdminBulkFeature\BulkFeatureFactory' => 'Feature\Form\AdminBulkFeature\BulkFeatureFactory',
						'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionFactory' => 'Feature\Form\AdminBulkFeatureOption\BulkFeatureOptionFactory',
						'Feature\Form\AdminCategoryFeature\CategoryFeatureFactory' => 'Feature\Form\AdminCategoryFeature\CategoryFeatureFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'Feature\Controller\AdminFeature' => 'Feature\Controller\AdminFeatureController',
						'Feature\Controller\AdminFeatureOption' => 'Feature\Controller\AdminFeatureOptionController',
						'Feature\Controller\AdminFeatureOptionProduct' => 'Feature\Controller\AdminFeatureOptionProductController',
						'Feature\Controller\AdminProductFeature' => 'Feature\Controller\AdminProductFeatureController',
						'Feature\Controller\AdminBulkFeature' => 'Feature\Controller\AdminBulkFeatureController',
						'Feature\Controller\AdminCategoryFeature' => 'Feature\Controller\AdminCategoryFeatureController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'Feature' => __DIR__.'/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						'Feature_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__.'/../src/Feature/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										'Feature\Entity' => 'Feature_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'Feature' => __DIR__.'/../public' 
						) 
				) 
		) 
);
