<?php

namespace ImportAttribute\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ImportAttributeNavigation extends \Admin\Navigation\Event\AdminNavigationEvent  {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMainImportMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Import Product Attribute',
                    'route' => 'admin-import',
                    'id' => 'admin-import-attribute',
                    'params' => array(
                        'eventName' => 'adminProductAttributeImport' 
                    ),
                    'iconClass' => 'glyphicon-font'
                )
            ));
            
             $page->addPages(array(
                array(
                    'label' => 'Import Product Attribute Options',
                    'route' => 'admin-import',
                    'params' => array(
                        'eventName' => 'adminProductAttributeOptionImport' 
                    ),
                    'id' => 'admin-import-product-attribute-option',
                    'iconClass' => 'glyphicon-wrench'
                )
            ));
        }
    }

}
