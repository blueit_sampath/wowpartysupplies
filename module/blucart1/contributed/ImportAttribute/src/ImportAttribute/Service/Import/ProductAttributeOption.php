<?php

namespace ImportAttribute\Service\Import;

use Import\Option\AbstractImportEvent;
use Product\Api\ProductApi;
use Core\Functions;
use \Attribute\Api\AttributeApi;

class ProductAttributeOption extends AbstractImportEvent {

    public function getEventName() {
        return 'adminProductAttributeOptionImport';
    }

    public function getPriority() {
        return 1000;
    }

    public function loadSchema() {
        $importContainer = $this->getImportContainer();
        $importContainer->setTitle('Import Product Attribute Option');
        

        $importContainer->addColumn('name', 'Attribute Option Name', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 1,
                        'max' => 255
                    )
                )
            )
                ), 990);

        $importContainer->addColumn('psku', 'Product SKU', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 980);

        $importContainer->addColumn('sku', 'Attribute Option SKU', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 970);


        $importContainer->addColumn('adjustmentSellPrice', 'Adj. Sell Price', array(
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
                ), 960);

        $importContainer->addColumn('adjustmentCostPrice', 'Adj. Cost Price', array(
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
                ), 950);

        $importContainer->addColumn('adjustmentListPrice', 'Adj. List Price', array(
            'name' => 'listPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
                ), 940);

        $importContainer->addColumn('isPercentage', 'Is Percentage', array(
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
                ), 930);


        $importContainer->addColumn('image', 'Image', array(
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 920);

        if (Functions::hasModule('Inventory')) {

            $importContainer->addColumn('stockStatus', 'Stock Status', array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Int'
                    )
                )
                    ), 870);

            $importContainer->addColumn('addStock', 'Stock(Add Stock)', array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Int'
                    )
                )
                    ), 860);

            $importContainer->addColumn('resetStock', 'Stock(Reset Stock)', array(
                'required' => false,
                'validators' => array(
                    array(
                        'name' => 'Int'
                    )
                )
                    ), 860);
        }
    }

    public function isValid() {
        $importContainer = $this->getImportContainer();
        $data = $importContainer->getRawData();
        $validationContainer = $this->getImportValidationContainer();

        $product = null;
        if (isset($data['psku'])) {
            $psku = $data['psku'];

            $product = ProductApi::getProductBySku($psku);
            if (!$product) {
                $validationContainer->add('product', 'Product Not Found');
                return;
            }
            
        }
        if (isset($data['name'])) {
            $attributeOptionName = $data['name'];

            if (!\Attribute\Api\AttributeApi::getAttributeOptionByNameAndProductId($attributeOptionName, $product->id)) {
                $validationContainer->add('attribute_'.$attributeOptionName,$attributeOptionName . ': Attribute Option Not Found');
            }
        }
    }

    public function save() {
        $importContainer = $this->getImportContainer();
        $data = $importContainer->getData();
        $columns = array(
            'sku',
            'title',
            'adjustmentListPrice',
            'adjustmentCostPrice',
            'adjustmentSellPrice',
            'isPercentage'
        );
         $psku = $data['psku'];

         $product = ProductApi::getProductBySku($psku);
        $attributeOptionName = $data['name'];
        $entity = \Attribute\Api\AttributeApi::getAttributeOptionByNameAndProductId($attributeOptionName,$product->id);
        foreach ($columns as $column) {
            if (isset($data[$column])) {
                $entity->$column = $data[$column];
            }
        }
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
        $importContainer->addEntity('AttributeOption', $entity);

        $this->saveAttributeOptionImage($data, $product);
        

        if (Functions::hasModule('Inventory')) {
          //  $this->saveStock($data, $product);
        }
        Functions::addSuccessMessage('Saved Successfully');
    }

    public function saveAttributeOptionImage($data, $entity) {

        if (isset($data['image'])) {
            $em = $this->getEntityManager();
           
            if($entity->file){
             \File\Api\FileApi::deleteFile($entity->file->path);
            }
          

            $file = $this->saveImage($data['image'], $entity->name);
            $entity->file = $file;
              $em->flush();
           
        }


    }

    public function saveStock($data, $product) {
        $em = $this->getEntityManager();
        $stockEntity = \Inventory\Api\InventoryApi::getStockByProductId($product->id);
        if (!$stockEntity) {
            $stockEntity = new \Inventory\Entity\Stock();
        }

        if (isset($data['stockStatus']) && $data['stockStatus'] !== '') {
            $stockEntity->status = $data['stockStatus'];
        }


        if (isset($data['addStock'])) {
            $stockEntity->qty = $stockEntity->qty + $data['addStock'];
        }

        if (isset($data['resetStock'])) {
            $stockEntity->qty = $data['resetStock'];
        }
        $stockEntity->product = $product;
        $em->persist($stockEntity);
        $em->flush();
    }

}
