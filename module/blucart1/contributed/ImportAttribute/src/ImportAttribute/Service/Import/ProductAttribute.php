<?php

namespace ImportAttribute\Service\Import;

use Import\Option\AbstractImportEvent;
use Product\Api\ProductApi;
use Core\Functions;
use \Attribute\Api\AttributeApi;

class ProductAttribute extends AbstractImportEvent {

    public function getEventName() {
        return 'adminProductAttributeImport';
    }

    public function getPriority() {
        return 1000;
    }

    public function loadSchema() {
        $importContainer = $this->getImportContainer();
        $importContainer->setTitle('Import Product Attribute');

        $importContainer->addColumn('sku', 'Product SKU', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
                ), 990);

        $importContainer->addColumn('name', 'Attribute Name', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 1,
                        'max' => 255
                    )
                )
            )
                ), 1000);

        $importContainer->addColumn('attributeOption', 'Attribute Option', array(
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 1,
                        'max' => 255
                    )
                )
            )
                ), 1000);

        

    }

    public function isValid() {
        $importContainer = $this->getImportContainer();

        $data = $importContainer->getRawData();
        $validationContainer = $this->getImportValidationContainer();


        if (isset($data['sku'])) {
            $sku = $data['sku'];

            $product = ProductApi::getProductBySku($sku);
            if (!$product) {
                $validationContainer->add('product', 'Product Not Found');
            }
        }

        if (isset($data['name'])) {
            $attributeName = $data['name'];
            $array = explode(';', $attributeName);
            foreach ($array as $a) {
                if (!\Attribute\Api\AttributeApi::getAttributeByName($a)) {
                    $validationContainer->add('attribute_' . $a, $a . ': Attribute Not Found');
                }
            }
        }

        if (isset($data['attributeOption'])) {
            $attributeOption = $data['attributeOption'];
            $array = explode(';', $attributeOption);
            foreach ($array as $a) {
                if (!\Attribute\Api\AttributeApi::getAttributeOptionByName($a)) {
                    $validationContainer->add('attribute_option_' . $a, $a . ': Attribute Option Not Found');
                }
            }
        }
    }

    public function save() {
        $em = $this->getEntityManager();
        $importContainer = $this->getImportContainer();
        $data = $importContainer->getData();
        $sku = $data['sku'];
        $product = ProductApi::getProductBySku($sku);

        $attributeNames = $data['name'];
        $temp = array();
        $array = explode(';', $attributeNames);
        foreach ($array as $a) {
            if ($entity = \Attribute\Api\AttributeApi::getAttributeByName($a)) {
                $temp[] = $entity->id;
            }
        }
        $attributes_string = implode(',', $temp);
        $this->deleteExcessAttributes($attributes_string, $product->id);


        $attributes = explode(',', $attributes_string);

        foreach ($attributes as $attributeId) {
            $attribute = AttributeApi::getAttributeById($attributeId);
            if (!AttributeApi::checkOrGetAttributeByProductIdAttributeId($product->id, $attributeId)) {
                $entity = new \Attribute\Entity\AttributeProduct ();
                $entity->type = $attribute->type;
                $entity->attribute = $attribute;
                $entity->product = $product;
                $em->persist($entity);
            }
        }

        $buildOptions = array();
        if (isset($data['attributeOption'])) {
            $attributeOption = $data['attributeOption'];
            $array = explode(';', $attributeOption);
            foreach ($array as $a) {
                if ($entity = \Attribute\Api\AttributeApi::getAttributeOptionByName($a)) {
                    if (in_array($entity->attribute->id, $attributes)) {
                        $buildOptions[$entity->attribute->id][] = $entity->id;
                    }
                }
            }
        }
        
        if(count($buildOptions) != count($attributes)){
             Functions::addErrorMessage('Minimum 1 option is required per attribute');
             return;
        }
        
        ksort($buildOptions);
        



        $this->buildOptions(array_values($buildOptions), $product->id);

        $em->flush();

        Functions::addSuccessMessage('Saved Successfully');
    }

    public function deleteExcessAttributes($attributes, $productId) {
        $em = Functions::getEntityManager();
        $dql = 'delete from \Attribute\Entity\AttributeProduct  u where u.attribute not in (' . $attributes . ') and u.product = ' . $productId;
        $query = $em->createQuery($dql);
        return $query->getResult();
    }

    
    
    
    

    public function buildOptions($buildOptions, $productId) {
        $em = $this->getEntityManager();
        $product = ProductApi::getProductById($productId);
        $combos = $this->combos($buildOptions);
        $titles = array();

        $nDeleteArray = array();

        foreach ($combos as $combo) {
            $titlesTemp = array();

            $listPrice = 0;
            $sellPrice = 0;
            $costPrice = 0;

            foreach ($combo as $t) {
                $entity = AttributeApi::getAttributeOptionById($t);
                $titlesTemp [$entity->id] = $entity->name . '(' . $entity->attribute->name . ')';
                $listPrice = $listPrice + $entity->defaultAdjustmentListPrice;
                $sellPrice = $sellPrice + $entity->defaultAdjustmentSellPrice;
                $costPrice = $sellPrice + $entity->defaultAdjustmentCostPrice;
            }
            if (count($titlesTemp)) {

                if (!($model = AttributeApi::getAttributeOptionByProductIdAndData($combo, $productId))) {

                    $model = new \Attribute\Entity\AttributeOptionProduct ();
                    $model->attributeOptions = implode(',', $combo);
                    $model->name = implode('-', $titlesTemp);
                    $model->sku = $product->sku . '-' . implode('-', $combo);
                    $model->adjustmentSellPrice = $sellPrice;
                    $model->adjustmentListPrice = $listPrice;
                    $model->adjustmentCostPrice = $costPrice;
                    $model->product = $product;
                    $em->persist($model);
                    $em->flush();
                }
                // $titles[] = $titlesTemp;
                $nDeleteArray [] = $model->id;
            }
        }

        if (count($nDeleteArray)) {

            $dql = 'delete from \Attribute\Entity\AttributeOptionProduct  u where u.id not in (' . implode(',', $nDeleteArray) . ') and u.product = ' . $productId;
            $em->createQuery($dql)->getResult();
        }

        return true;
    }

    public function combos($data, &$all = array(), $group = array(), $val = null, $i = 0) {
        if (isset($val)) {
            array_push($group, $val);
        }

        if ($i >= count($data)) {
            array_push($all, $group);
        } else {
            foreach ($data [$i] as $v) {
                $this->combos($data, $all, $group, $v, $i + 1);
            }
        }

        return $all;
    }

}
