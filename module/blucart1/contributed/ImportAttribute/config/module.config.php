<?php
		
		return array(
    'events' => array(
        'ImportAttribute\Service\Import\ProductAttribute' => 'ImportAttribute\Service\Import\ProductAttribute',
        'ImportAttribute\Service\Import\ProductAttributeOption' => 'ImportAttribute\Service\Import\ProductAttributeOption',
        'ImportAttribute\Service\Navigation\ImportAttributeNavigation' => 'ImportAttribute\Service\Navigation\ImportAttributeNavigation'
        ),
    'service_manager' => array(
        'invokables' => array(
            'ImportAttribute\Service\Import\ProductAttribute' => 'ImportAttribute\Service\Import\ProductAttribute',
            'ImportAttribute\Service\Import\ProductAttributeOption' => 'ImportAttribute\Service\Import\ProductAttributeOption',
            'ImportAttribute\Service\Navigation\ImportAttributeNavigation' => 'ImportAttribute\Service\Navigation\ImportAttributeNavigation'
            ),
        'factories' => array()
        ),
    'controllers' => array('invokables' => array()),
    'view_manager' => array('template_path_stack' => array('ImportAttribute' => __DIR__.'/../view')),
   
    'asset_manager' => array('resolver_configs' => array('paths' => array('ImportAttribute' => __DIR__.'/../public')))
    );
