<?php

return array(
    'events' => array(
        'ExportAttribute\Service\Export\ProductAttribute' => 'ExportAttribute\Service\Export\ProductAttribute',
        'ExportAttribute\Service\Export\ProductAttributeOption' => 'ExportAttribute\Service\Export\ProductAttributeOption',
        'ExportAttribute\Service\Navigation\ExportAttributeNavigation' => 'ExportAttribute\Service\Navigation\ExportAttributeNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'ExportAttribute\Service\Export\ProductAttribute' => 'ExportAttribute\Service\Export\ProductAttribute',
            'ExportAttribute\Service\Export\ProductAttributeOption' => 'ExportAttribute\Service\Export\ProductAttributeOption',
            'ExportAttribute\Service\Navigation\ExportAttributeNavigation' => 'ExportAttribute\Service\Navigation\ExportAttributeNavigation'
        ),
        'factories' => array()
    ),
    'controllers' => array('invokables' => array()),
    'view_manager' => array('template_path_stack' => array('ExportAttribute' => __DIR__ . '/../view')),
    'asset_manager' => array('resolver_configs' => array('paths' => array('ExportAttribute' => __DIR__ . '/../public')))
);
