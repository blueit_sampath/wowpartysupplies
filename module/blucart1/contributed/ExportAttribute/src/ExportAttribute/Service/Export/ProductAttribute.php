<?php

namespace ExportAttribute\Service\Export;

use Export\Option\AbstractExportEvent;
use Product\Api\ProductApi;
use Core\Functions;
use \Attribute\Api\AttributeApi;

class ProductAttribute extends AbstractExportEvent {

    public function getEventName() {
        return 'adminProductAttributeExport';
    }

    public function getPriority() {
        return 1000;
    }

    public function loadSchema() {
        $importContainer = $this->getExportContainer();
        $importContainer->setTitle('Export Product Attribute');

        $importContainer->addColumn('sku', 'Product SKU', 1000);

        $importContainer->addColumn('name', 'Attribute Name', 990);

        $importContainer->addColumn('attributeOption', 'Attribute Option', 980);
    }

    public function fetch() {
        $exportContainer = $this->getExportContainer();

        $products = ProductApi::getAllProducts(null);


        foreach ($products as $product) {
            $temp = array();
            $temp['sku'] = $product->sku;
            $attributeResults = AttributeApi::getAttributeByProductId($product->id);
            $attributes = array();

            foreach ($attributeResults as $attributeResult) {
                $attributeProduct = AttributeApi::getAttributeProductById($attributeResult['attributeProductId']);
                if ($attributeProduct && $attributeProduct->attribute) {
                    $attributes[] = $attributeProduct->attribute->name;
                }
            }
            $temp['name'] = $attributes;
            $attributeOptionResults = AttributeApi::getAttributeOptionProductByProductId($product->id);
         
            $attributesOptions = array();
            foreach ($attributeOptionResults as $attributeResult) {
                $options = $attributeResult->attributeOptions;
                 
                foreach(explode(',',$options) as $optionId){
                    $option = AttributeApi::getAttributeOptionById($optionId);
                  
                    if($option){
                       $attributesOptions[] = $option->name; 
                    }
                }
               
            }
            $temp['attributeOption'] = array_unique($attributesOptions);
            $exportContainer->addDataItem($product->id, $temp);
        }
    }

}
