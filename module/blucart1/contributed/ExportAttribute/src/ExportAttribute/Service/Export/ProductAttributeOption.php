<?php

namespace ExportAttribute\Service\Export;

use Export\Option\AbstractExportEvent;
use Product\Api\ProductApi;
use Core\Functions;
use \Attribute\Api\AttributeApi;

class ProductAttributeOption extends AbstractExportEvent {

    public function getEventName() {
        return 'adminProductAttributeOptionExport';
    }

    public function getPriority() {
        return 1000;
    }

    public function loadSchema() {
        $exportContainer = $this->getExportContainer();
        $exportContainer->setTitle('Export Product Attribute Option');
        $exportContainer->addColumn('psku', 'Product SKU', 1000);
        $exportContainer->addColumn('ptitle', 'Product Title', 1000);
        $exportContainer->addColumn('pSellPrice', 'Product Sell Price', 1000);
        $exportContainer->addColumn('pCostPrice', 'Product Cost Price', 1000);
        $exportContainer->addColumn('pListPrice', 'Product list Price', 1000);
        $exportContainer->addColumn('pStatus', 'Product Status', 1000);
        $exportContainer->addColumn('name', 'Attribute Option Name', 980);
        $exportContainer->addColumn('sku', 'Attribute Option SKU', 970);
        $exportContainer->addColumn('adjustmentSellPrice', 'Adj. Sell Price', 960);
        $exportContainer->addColumn('adjustmentCostPrice', 'Adj. Cost Price', 950);
        $exportContainer->addColumn('adjustmentListPrice', 'Adj. List Price', 940);
        $exportContainer->addColumn('isPercentage', 'Is Percentage', 930);
        $exportContainer->addColumn('image', 'Image', 920);

        if (Functions::hasModule('Inventory')) {
            $exportContainer->addColumn('stockStatus', 'Stock Status', 870);
            $exportContainer->addColumn('stock', 'Stock', 860);
        }
    }

    public function fetch() {

        $exportContainer = $this->getExportContainer();

        $products = ProductApi::getAllProducts(null);

        $selectedColumns = $exportContainer->getSelectedColumns();
        $columns = array(
            'sku',
            'name',
            'adjustmentListPrice',
            'adjustmentCostPrice',
            'adjustmentSellPrice',
            'isPercentage'
        );
        $weight = 10000;
        foreach ($products as $product) {
            $attributeOptionResults = AttributeApi::getAttributeOptionProductByProductId($product->id);


            foreach ($attributeOptionResults as $attributeResult) {
                $temp = array();
                $temp['psku'] = $product->sku;
                $temp['ptitle'] = $product->title;
                $temp['pSellPrice'] = $product->sellPrice;
                $temp['plistPrice'] = $product->listPrice;
                $temp['pCostPrice'] = $product->costPrice;
                $temp['pStatus'] = $product->status;

                foreach ($columns as $column) {
                    if (in_array($column, $selectedColumns)) {
                        $temp[$column] = $attributeResult->$column;
                    }
                }
                if ($attributeResult->file && $attributeResult->file->path) {
                    $temp['image'] = \File\Api\FileApi::getImageUrl($attributeResult->file->path, true);
                }
                if (in_array('stockStatus', $selectedColumns)) {
                    $temp['stockStatus'] = $this->getStockStatus($attributeResult->id);
                }
                if (in_array('stock', $selectedColumns)) {
                    $temp['stock'] = $this->getStockStatus($attributeResult->id);
                }
                $exportContainer->addDataItem($attributeResult->name, $temp, $weight--);
            }
        }
    }

    public function getStockStatus($attributeOptionProductId) {
        $stockEntity = \Inventory\Api\InventoryApi::getStockByAttributeId($attributeOptionProductId);
        if (!$stockEntity) {
            return null;
        }
        return $stockEntity->status;
    }

    public function getStock($attributeOptionProductId) {
        $stockEntity = \Inventory\Api\InventoryApi::getStockByAttributeId($attributeOptionProductId);
        if (!$stockEntity) {
            return null;
        }
        return $stockEntity->qty;
    }

}
