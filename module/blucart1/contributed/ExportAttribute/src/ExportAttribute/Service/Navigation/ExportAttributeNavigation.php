<?php

namespace ExportAttribute\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ExportAttributeNavigation extends \Admin\Navigation\Event\AdminNavigationEvent  {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMainExportMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Export Product Attribute',
                    'route' => 'admin-export',
                    'id' => 'admin-export-attribute',
                    'params' => array(
                        'eventName' => 'adminProductAttributeExport' 
                    ),
                    'iconClass' => 'glyphicon-font'
                )
            ));
            
             $page->addPages(array(
                array(
                    'label' => 'Export Product Attribute Options',
                    'route' => 'admin-export',
                    'params' => array(
                        'eventName' => 'adminProductAttributeOptionExport' 
                    ),
                    'id' => 'admin-export-product-attribute-option',
                    'iconClass' => 'glyphicon-wrench'
                )
            ));
        }
    }

}
