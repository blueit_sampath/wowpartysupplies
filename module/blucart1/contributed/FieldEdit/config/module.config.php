<?php

namespace FieldEdit;

return array (
		'router' => array (
				'routes' => array () 
		),
		'events' => array (),
		'service_manager' => array (
				'invokables' => array (),
				'factories' => array () 
		),
		'controllers' => array (
				'invokables' => array (
						'FieldEdit\Controller\Index' => 'FieldEdit\Controller\IndexController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'FieldEdit' => __DIR__.'/../view' 
				) 
		),
		
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'FieldEdit' => __DIR__.'/../public' 
						) 
				) 
		),
		'view_helpers' => array (
				'invokables' => array (
						'fieldedit' => 'FieldEdit\View\Helper\FieldEdit',
						
				)
		),
);
