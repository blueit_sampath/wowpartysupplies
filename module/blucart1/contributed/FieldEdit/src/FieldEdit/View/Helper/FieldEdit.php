<?php

namespace FieldEdit\View\Helper;

use Common\Option\Content\ContentContainer;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\View\Helper\AbstractHelper;

class FieldEdit extends AbstractHelper implements ServiceLocatorAwareInterface, EventManagerAwareInterface {
	protected $_services;
	protected $_events;
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services->getServiceLocator ();
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (null === $this->_events) {
			$this->setEventManager ( new EventManager () );
		}
		return $this->_events;
	}
	public function __invoke($value, $params = array()) {
		$view = $this->getView ();
		$params ['value'] = $value;
		return $view->render ( 'common/field-edit', 

		$params );
	}
}
