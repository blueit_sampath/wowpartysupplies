<?php
return array(
    'router' => array(
        'routes' => array(
            'paymentsensepayment-send' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/paymentsensepayment/send[/:orderId]',
                    'defaults' => array(
                        'controller' => 'PaymentsensePayment\Controller\Index',
                        'action' => 'send'
                    )
                )
            ),
            'paymentsensepayment-land' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/paymentsensepayment/land',
                    'defaults' => array(
                        'controller' => 'PaymentsensePayment\Controller\Index',
                        'action' => 'land'
                    )
                )
            ),
            'paymentsensepayment-error' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/paymentsensepayment/error',
                    'defaults' => array(
                        'controller' => 'PaymentsensePayment\Controller\Index',
                        'action' => 'error'
                    )
                )
            ),
            'admin-paymentsensepayment-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/paymentsensepayment/setting[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'PaymentsensePayment\Controller\AdminPaymentsensePayment',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            )
        )
    ),
    'events' => array(
        'PaymentsensePayment\Service\Payment\PaymentsensePaymentEvent' => 'PaymentsensePayment\Service\Payment\PaymentsensePaymentEvent',
        'PaymentsensePayment\Service\Form\AdminPaymentsensePaymentSetting' => 'PaymentsensePayment\Service\Form\AdminPaymentsensePaymentSetting',
        'PaymentsensePayment\Service\Tab\AdminPaymentSetting' => 'PaymentsensePayment\Service\Tab\AdminPaymentSetting'
    ),
    'service_manager' => array(
        'invokables' => array(
            'PaymentsensePayment\Service\Payment\PaymentsensePaymentEvent' => 'PaymentsensePayment\Service\Payment\PaymentsensePaymentEvent',
            'PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigForm' => 'PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigForm',
            'PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigFilter' => 'PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigFilter',
            'PaymentsensePayment\Service\Form\AdminPaymentsensePaymentSetting' => 'PaymentsensePayment\Service\Form\AdminPaymentsensePaymentSetting',
            'PaymentsensePayment\Service\Tab\AdminPaymentSetting' => 'PaymentsensePayment\Service\Tab\AdminPaymentSetting'
        ),
        'factories' => array(
            'PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigFactory' => 'PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'PaymentsensePayment\Controller\Index' => 'PaymentsensePayment\Controller\IndexController',
            'PaymentsensePayment\Controller\AdminPaymentsensePayment' => 'PaymentsensePayment\Controller\AdminPaymentsensePaymentController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'PaymentsensePayment' => __DIR__ . '/../view'
        )
    ),
    
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'PaymentsensePayment' => __DIR__ . '/../public'
            )
        )
    )
);
