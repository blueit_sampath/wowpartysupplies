<?php
namespace PaymentsensePayment\Service\Payment;

use Core\Functions;
use Checkout\Option\CartContainer;
use Payment\Option\AbstractPaymentEvent;
use Config\Api\ConfigApi;

class PaymentsensePaymentEvent extends AbstractPaymentEvent
{

    public function getName()
    {
        return 'paymentsensePayment';
    }

    public function getTitle()
    {
        $title = ConfigApi::getConfigByKey('PAYMENT_PAYMENTSENSE_TITLE');
        if (! $title) {
            $title = 'Payment Sense';
        }
        return $title;
    }

    public function getRouteName()
    {
        return 'paymentsensepayment-send';
    }
}
