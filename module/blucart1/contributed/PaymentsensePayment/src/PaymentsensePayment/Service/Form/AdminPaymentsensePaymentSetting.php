<?php
namespace PaymentsensePayment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminPaymentsensePaymentSetting extends AdminConfig
{

    protected $_columnKeys = array(
        'PAYMENT_PAYMENTSENSE_TITLE',
        'PAYMENT_PAYMENTSENSE_MERCHANTID',
         'PAYMENT_PAYMENTSENSE_PASSWORD',
        'PAYMENT_PAYMENTSENSE_PAYMENTPROCESSORDOMAIN',
        'PAYMENT_PAYMENTSENSE_HASHMETHOD',
        'PAYMENT_PAYMENTSENSE_PRESHAREDKEY',
        'PAYMENT_PAYMENTSENSE_RESULTDELIVERYMETHOD'
    );

    public function getFormName()
    {
        return 'adminPaymentsensePaymentConfig';
    }

    public function getPriority()
    {
        return 1000;
    }
}
