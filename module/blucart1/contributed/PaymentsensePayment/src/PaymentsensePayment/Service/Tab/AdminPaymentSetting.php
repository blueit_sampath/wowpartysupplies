<?php
namespace PaymentsensePayment\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminPaymentSetting extends AbstractTabEvent
{

    public function getEventName()
    {
        return 'adminPaymentSetting';
    }

    public function tab()
    {
        $tabContainer = $this->getTabContainer();
        $url = Functions::getUrlPlugin();
        
        $u = $url('admin-paymentsensepayment-setting');
        $tabContainer->add('admin-paymentsensepayment-setting', 'Paymentsense Payment', $u, 500);
        
        return $this;
    }
}

