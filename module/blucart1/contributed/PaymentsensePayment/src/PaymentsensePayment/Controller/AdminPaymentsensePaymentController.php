<?php
namespace PaymentsensePayment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminPaymentsensePaymentController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-paymentsensepayment-setting');
    }
}