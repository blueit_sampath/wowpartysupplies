<?php

namespace PaymentsensePayment\Form\AdminPaymentsensePaymentConfig;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PaymentsensePaymentConfigFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'PAYMENT_PAYMENTSENSE_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYMENTSENSE_MERCHANTID',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYMENTSENSE_PASSWORD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
                , array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYMENTSENSE_PAYMENTPROCESSORDOMAIN',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYMENTSENSE_HASHMETHOD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYMENTSENSE_PRESHAREDKEY',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'PAYMENT_PAYMENTSENSE_RESULTDELIVERYMETHOD',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));
    }

}
