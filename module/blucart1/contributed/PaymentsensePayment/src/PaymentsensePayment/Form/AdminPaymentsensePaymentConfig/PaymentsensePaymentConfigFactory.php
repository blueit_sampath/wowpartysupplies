<?php
namespace PaymentsensePayment\Form\AdminPaymentsensePaymentConfig;

use Common\Form\Option\AbstractFormFactory;

class PaymentsensePaymentConfigFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigForm');
        $form->setName('adminPaymentsensePaymentConfig');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('PaymentsensePayment\Form\AdminPaymentsensePaymentConfig\PaymentsensePaymentConfigFilter');
    }
}
