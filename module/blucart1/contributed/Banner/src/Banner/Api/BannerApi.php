<?php

namespace Banner\Api;

use Core\Functions;
use Common\Api\Api;

class BannerApi extends Api {
	protected static $_entity = '\Banner\Entity\Banner';
	public static function getBannerById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getBanners($status = true) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->setEventName('BannerApi.getBanners');
		$queryBuilder->addColumn ( 'banner.id', 'bannerId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'banner.status', 'bannerStatus' );
			$queryBuilder->addParameter ( 'bannerStatus', $status );
		}
		$queryBuilder->addOrder ( 'banner.weight', 'bannerWeight', 'desc' );
		$queryBuilder->addGroup ( 'banner.id', 'bannerId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'banner' );
		return $queryBuilder->executeQuery ();
	}
	public static function getSortedArray() {
		$array = static::getBanners ( null );
		return static::makeImageArray ( $array );
	}
	public static function makeImageArray($array) {
		$result = array ();
		foreach ( $array as $bannerResults ) {
			$bannerId = $bannerResults ['bannerId'];
			$banner = static::getBannerById ( $bannerId );
			$result [$banner->id] = array (
					'name' => $banner->file->path 
			);
		}
		return $result;
	}
}
