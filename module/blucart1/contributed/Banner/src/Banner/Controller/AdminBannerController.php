<?php

namespace Banner\Controller;

use Common\MVC\Controller\AbstractAdminController;

use Core\Functions;

use Zend\View\Model\ViewModel;

class AdminBannerController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Banner\Form\AdminBanner\BannerFormFactory' );
	}
	
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-banner-add', array (
				'bannerId' => $form->get ( 'id' )->getValue () 
		) );
	}
}