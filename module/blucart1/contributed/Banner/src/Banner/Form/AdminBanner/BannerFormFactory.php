<?php
namespace Banner\Form\AdminBanner;

use Common\Form\Option\AbstractFormFactory;

class BannerFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Banner\Form\AdminBanner\BannerForm' );
		$form->setName ( 'adminBannerAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Banner\Form\AdminBanner\BannerFilter' );
	}
}
