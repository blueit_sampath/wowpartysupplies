<?php

namespace Banner\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use File\Api\FileApi;

class AdminBanner extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'url',
			'status',
			'weight' 
	);
	protected $_entity = '\Banner\Entity\Banner';
	protected $_entityName = 'banner';
	public function getEventName() {
		return 'adminBanner';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'banner.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'file.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'alt' );
		$columnItem->setTitle ( 'Alt' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'file.alt', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'filePath' );
		$columnItem->setTitle ( 'Image' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'file.path', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'url' );
		$columnItem->setTitle ( 'URL' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'banner.url', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 950 );
		$columns->addColumn ( 'banner.status', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Priority' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 940 );
		$columns->addColumn ( 'banner.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 990 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 980 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem ( 'banner.file', 'file' );
		$item->addJoin ( $joinItem );
		return true;
	}
	public function beforeUpdate($params, $entity) {
		$entity->file = FileApi::createOrUpdateFile ( $params ['filePath'], $params ['title'], $params ['alt'] );
	}
	public function afterDestroy($params, $entity) {
		$em = $this->getEntityManager ();
		if ($entity->file && $entity->file->path) {
			FileApi::deleteFile ( $entity->file->path );
		}
		if ($entity->file) {
			$em->remove ( $entity->file );
		}
	}
}
