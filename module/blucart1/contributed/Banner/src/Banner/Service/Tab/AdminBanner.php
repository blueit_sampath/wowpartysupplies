<?php

namespace Banner\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminBanner extends AbstractTabEvent {
	public function getEventName() {
		return 'adminBannerAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$bannerId = Functions::fromRoute ( 'bannerId' );
		if (! $bannerId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-banner-add', array (
				'bannerId' => $bannerId 
		) );
		$tabContainer->add ( 'admin-banner-add', 'General', $u, 1000 );
		
		return $this;
	}
}

