<?php

namespace Banner\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminBanner extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminBanner';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-banner-add' );
		$item = $linkContainer->add ( 'admin-banner-add', 'Add Banner', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-banner-sort' );
		$item = $linkContainer->add ( 'admin-banner-sort', 'Arrange Banners', $u, 990 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

