<?php 
namespace Banner\Service\Navigation;

use Admin\Navigation\Event\AdminNavigationEvent;
use Core\Functions;

class AdminBanner extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'websiteMain' );
	
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Banners',
							'route' => 'admin-banner',
							'id' => 'admin-banner',
							'iconClass' => 'glyphicon-film' 
					) 
			) );
		}
	}
}

