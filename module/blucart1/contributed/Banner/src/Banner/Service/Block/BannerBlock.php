<?php 
namespace Banner\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class BannerBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'banner-block';

	public function getBlockName() {
		return 'bannerBlock';
	}
	public function getPriority() {
		return 1000;
	}
	
}

