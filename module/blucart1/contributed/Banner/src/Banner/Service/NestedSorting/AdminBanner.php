<?php

namespace Banner\Service\NestedSorting;

use Core\Functions;
use Banner\Api\BannerApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminBanner extends AbstractNestedSortingEvent {
	protected $_entityName = '\Banner\Entity\Banner';
	protected $_column = 'path';
	public function getEventName() {
		return 'adminBannerSort';
	}
	public function getPriority() {
		return 1000;
	}
	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (), array (
				'weight' => 'desc' 
		) );
		if ($results) {
			$column = $this->_column;
			foreach ( $results as $result ) {
				if ($result->file) {
					$array [$result->id] = array (
							'name' => $result->file->$column 
					);
				}
			}
		}
		return $array;
	}
}
