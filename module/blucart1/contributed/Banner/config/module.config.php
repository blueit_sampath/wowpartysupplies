<?php
return array (
		'router' => array (
				'routes' => array (
						'admin-banner' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/banner',
										'defaults' => array (
												'controller' => 'Banner\Controller\AdminBanner',
												'action' => 'index' 
										) 
								) 
						),
						'admin-banner-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/banner/add[/ajax/:ajax][/:bannerId]',
										'defaults' => array (
												'controller' => 'Banner\Controller\AdminBanner',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'bannerId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-banner-sort' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/banner/sort[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'Banner\Controller\AdminBanner',
												'action' => 'sort',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'Banner\Service\Grid\AdminBanner' => 'Banner\Service\Grid\AdminBanner',
				'Banner\Service\Form\AdminBanner' => 'Banner\Service\Form\AdminBanner',
				'Banner\Service\Tab\AdminBanner' => 'Banner\Service\Tab\AdminBanner',
				'Banner\Service\Link\AdminBanner' => 'Banner\Service\Link\AdminBanner',
				'Banner\Service\NestedSorting\AdminBanner' => 'Banner\Service\NestedSorting\AdminBanner',
				'Banner\Service\Block\BannerBlock' => 'Banner\Service\Block\BannerBlock',
				'Banner\Service\Navigation\AdminBanner' => 'Banner\Service\Navigation\AdminBanner' 
		),
		'service_manager' => array (
				'invokables' => array (
						'Banner\Service\Grid\AdminBanner' => 'Banner\Service\Grid\AdminBanner',
						'Banner\Service\Form\AdminBanner' => 'Banner\Service\Form\AdminBanner',
						'Banner\Form\AdminBanner\BannerForm' => 'Banner\Form\AdminBanner\BannerForm',
						'Banner\Form\AdminBanner\BannerFilter' => 'Banner\Form\AdminBanner\BannerFilter',
						'Banner\Service\Tab\AdminBanner' => 'Banner\Service\Tab\AdminBanner',
						'Banner\Service\Link\AdminBanner' => 'Banner\Service\Link\AdminBanner',
						'Banner\Service\NestedSorting\AdminBanner' => 'Banner\Service\NestedSorting\AdminBanner',
						'Banner\Service\Block\BannerBlock' => 'Banner\Service\Block\BannerBlock',
						'Banner\Service\Navigation\AdminBanner' => 'Banner\Service\Navigation\AdminBanner' 
				),
				'factories' => array (
						'Banner\Form\AdminBanner\BannerFormFactory' => 'Banner\Form\AdminBanner\BannerFormFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'Banner\Controller\AdminBanner' => 'Banner\Controller\AdminBannerController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'Banner' => __DIR__.'/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						'Banner_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__.'/../src/Banner/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										'Banner\Entity' => 'Banner_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'Banner' => __DIR__.'/../public' 
						) 
				) 
		) 
);
