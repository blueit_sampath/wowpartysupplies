<?php

namespace ProductRecommended\Api;

use Product\Api\ProductApi;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Common\Api\Api;

class ProductRecommendedApi extends Api {
	protected static $_entity = '\ProductRecommended\Entity\ProductRecommended';
	public static function getProductRecommendedById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getRecommendedProducts($productId, $status = true) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'recommendedProduct.id', 'productRecommendRecommendedProductId' );
		$queryBuilder->addColumn ( 'productRecommend.id', 'productRecommendId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'recommendedProduct.status', 'productRecommendRecommendedStatus' );
			$queryBuilder->addParameter ( 'productRecommendRecommendedStatus', $status );
		}
		$queryBuilder->addOrder ( 'productRecommend.weight', 'productRecommendWeight', 'desc' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'productRecommend' );
		$joinItem = new QueryJoinItem ( 'productRecommend.recommendedProduct', 'recommendedProduct' );
		$item->addJoin ( $joinItem );
		
		$item = $queryBuilder->addWhere ( 'productRecommend.product', 'productId' );
		$queryBuilder->addParameter ( 'productId', $productId );
		
		
		
		return $queryBuilder->executeQuery ();
	}
	public static function getSortedArray($productId) {
		$array = static::getRecommendedProducts ( $productId, null );
		return static::makeArray ( $array );
	}
	public static function makeArray($array) {
		$result = array ();
		foreach ( $array as $results ) {
			$productId = $results ['productRecommendRecommendedProductId'];
			$product = ProductApi::getProductById ( $productId );
			$result [$results ['productRecommendId']] = array (
					'name' => $product->title 
			);
		}
		return $result;
	}
}
