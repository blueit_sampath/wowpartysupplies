<?php

namespace ProductRecommended\Controller;

use Common\MVC\Controller\AbstractAdminController;


class AdminProductRecommendedController extends AbstractAdminController{
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'ProductRecommended\Form\AdminProductRecommended\ProductRecommendedFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-recommended-add', array (
						'productId' => $form->get ( 'product' )->getValue () 
				) );
	}
	
}