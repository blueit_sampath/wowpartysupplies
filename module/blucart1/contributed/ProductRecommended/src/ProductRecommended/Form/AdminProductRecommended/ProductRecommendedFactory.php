<?php
namespace ProductRecommended\Form\AdminProductRecommended;

use Common\Form\Option\AbstractFormFactory;

class ProductRecommendedFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ProductRecommended\Form\AdminProductRecommended\ProductRecommendedForm' );
		$form->setName ( 'adminProductRecommendedAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ProductRecommended\Form\AdminProductRecommended\ProductRecommendedFilter' );
	}
}
