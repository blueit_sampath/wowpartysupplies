<?php 
namespace ProductRecommended\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminProductRecommendedProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-product-recommended-product-view';
	protected $_contentName = 'adminProductRecommendedProductView';
	public function getEventName() {
		return 'adminProductViewTop';
	}
	public function getPriority() {
		return ;
	}
	
}

