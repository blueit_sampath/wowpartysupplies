<?php 
namespace ProductRecommended\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProductRecommended extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminProductRecommended';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		if(!$productId){
			return;
		}
		$u = $url ( 'admin-product-recommended-add', array (
				'productId' => $productId
		) );
		$item = $linkContainer->add ( 'admin-product-recommended-add', 'Add Recommended Products', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

