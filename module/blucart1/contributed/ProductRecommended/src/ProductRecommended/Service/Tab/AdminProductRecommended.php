<?php 
namespace ProductRecommended\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminProductRecommended extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		if(!$productId){
			return;
		}
		$u = $url ( 'admin-product-recommended', array (
				'productId' => $productId
		) );
		
		
		$tabContainer->add ( 'admin-product-recommended', 'Recommended Products', $u,null );
		
		return $this;
	}
}

