<?php

namespace ProductRecommended\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use ProductRecommended\Entity\ProductRecommended;
use ProductRecommended\Api\ProductRecommendedApi;
use Product\Api\ProductApi;
use Category\Api\CategoryApi;
use Core\Functions;

class AdminProductRecommended extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight' 
	);
	protected $_entity = '\ProductRecommended\Entity\ProductRecommended';
	protected $_entityName = 'productRecommended';
	public function getFormName() {
		return 'adminProductRecommendedAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		if (isset ( $params ['product'] ) && $params ['product'] && $params ['products']) {
			$product = ProductApi::getProductById ( $params ['product'] );
			
			if ($product) {
				$results = ProductRecommendedApi::getRecommendedProducts ( $product->id );
				$array = array ();
				if ($results) {
					foreach ( $results as $result ) {
						$array [] = ( int ) $result ['productRecommendRecommendedProductId'];
					}
				}
				foreach ( $params ['products'] as $r ) {
					if (! in_array ( ( int ) $r, $array )) {
						$e = new ProductRecommended ();
						$e->product = $product;
						$e->recommendedProduct = ProductApi::getProductById ( $r );
						$em->persist ( $e );
					}
				}
				
				$em->flush ();
			}
		}
		
		return true;
	}
	public function getRecord() {
		$id = Functions::fromRoute ( 'productId' );
		if (! $id) {
			return;
		}
		$form = $this->getForm ();
		$form->get ( 'product' )->setValue ( $id );
		
		return true;
	}
}
