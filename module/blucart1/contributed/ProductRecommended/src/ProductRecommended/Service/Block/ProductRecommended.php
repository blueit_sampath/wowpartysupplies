<?php 
namespace ProductRecommended\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class ProductRecommended extends AbstractBlockEvent {

	protected $_blockTemplate = 'product-recommended';

	public function getBlockName() {
		return 'productRecommended';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'Related Products';
	}
	
}

