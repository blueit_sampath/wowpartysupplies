<?php

namespace ProductRecommended\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;

class AdminProductRecommended extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'weight' 
	);
	protected $_entity = '\ProductRecommended\Entity\ProductRecommended';
	protected $_entityName = 'productRecommended';
	public function getEventName() {
		return 'adminProductRecommended';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		$productId = Functions::fromRoute ( 'productId', 0 );
		$grid = $this->getGrid ();
		
		if ($productId) {
			$grid->addAdditionalParameter ( 'productId', $productId );
		}
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'productRecommended.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Product Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'recommendedProduct.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Priority' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'productRecommended.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 600 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$productId = Functions::fromQuery ( 'productId', 0 );
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		
		$joinItem = new QueryJoinItem ( 'productRecommended.recommendedProduct', 'recommendedProduct' );
		$item->addJoin ( $joinItem );
		
		$queryBuilder->addWhere ( 'productRecommended.product', 'productId' );
		$queryBuilder->addParameter ( 'productId', $productId );
		return true;
	}
}
