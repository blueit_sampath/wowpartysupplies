<?php
		
		return array(
    'router' => array('routes' => array(
            'admin-product-recommended' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/recommended/:productId[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'ProductRecommended\Controller\AdminProductRecommended',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-product-recommended-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/recommended/add[/:productId]',
                    'defaults' => array(
                        'controller' => 'ProductRecommended\Controller\AdminProductRecommended',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-product-recommended-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/recommended/sort[/:productId]',
                    'defaults' => array(
                        'controller' => 'ProductRecommended\Controller\AdminProductRecommended',
                        'action' => 'sort',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                )
            )),
    'events' => array(
        'ProductRecommended\Service\Grid\AdminProductRecommended' => 'ProductRecommended\Service\Grid\AdminProductRecommended',
        'ProductRecommended\Service\Form\AdminProductRecommended' => 'ProductRecommended\Service\Form\AdminProductRecommended',
        'ProductRecommended\Service\Tab\AdminProductRecommended' => 'ProductRecommended\Service\Tab\AdminProductRecommended',
        'ProductRecommended\Service\Link\AdminProductRecommended' => 'ProductRecommended\Service\Link\AdminProductRecommended',
        'ProductRecommended\Service\Content\AdminProductRecommendedProductView' => 'ProductRecommended\Service\Content\AdminProductRecommendedProductView',
        'ProductRecommended\Service\Grid\AdminProduct' => 'ProductRecommended\Service\Grid\AdminProduct',
        'ProductRecommended\Service\Block\ProductRecommended' => 'ProductRecommended\Service\Block\ProductRecommended'
        ),
    'service_manager' => array(
        'invokables' => array(
            'ProductRecommended\Service\Grid\AdminProductRecommended' => 'ProductRecommended\Service\Grid\AdminProductRecommended',
            'ProductRecommended\Service\Form\AdminProductRecommended' => 'ProductRecommended\Service\Form\AdminProductRecommended',
            'ProductRecommended\Form\AdminProductRecommended\ProductRecommendedForm' => 'ProductRecommended\Form\AdminProductRecommended\ProductRecommendedForm',
            'ProductRecommended\Form\AdminProductRecommended\ProductRecommendedFilter' => 'ProductRecommended\Form\AdminProductRecommended\ProductRecommendedFilter',
            'ProductRecommended\Service\Tab\AdminProductRecommended' => 'ProductRecommended\Service\Tab\AdminProductRecommended',
            'ProductRecommended\Service\Link\AdminProductRecommended' => 'ProductRecommended\Service\Link\AdminProductRecommended',
            'ProductRecommended\Service\Content\AdminProductRecommendedProductView' => 'ProductRecommended\Service\Content\AdminProductRecommendedProductView',
            'ProductRecommended\Service\Grid\AdminProduct' => 'ProductRecommended\Service\Grid\AdminProduct',
            'ProductRecommended\Service\Block\ProductRecommended' => 'ProductRecommended\Service\Block\ProductRecommended'
            ),
        'factories' => array('ProductRecommended\Form\AdminProductRecommended\ProductRecommendedFactory' => 'ProductRecommended\Form\AdminProductRecommended\ProductRecommendedFactory')
        ),
    'controllers' => array('invokables' => array('ProductRecommended\Controller\AdminProductRecommended' => 'ProductRecommended\Controller\AdminProductRecommendedController')),
    'view_manager' => array('template_path_stack' => array('ProductRecommended' => __DIR__.'/../view')),
    'doctrine' => array('driver' => array(
            'ProductRecommended_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__.'/../src/ProductRecommended/Entity')
                ),
            'orm_default' => array('drivers' => array('ProductRecommended\Entity' => 'ProductRecommended_driver'))
            )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('ProductRecommended' => __DIR__.'/../public')))
    );
