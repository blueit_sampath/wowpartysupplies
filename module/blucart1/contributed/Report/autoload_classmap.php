<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
  'Report\Api\ReportApi'                                                        => __DIR__ . '/src/Report/Api/ReportApi.php',
  'Report\Service\Content\AdminReportUserView'                                  => __DIR__ . '/src/Report/Service/Content/AdminReportUserView.php',
  'Report\Service\Content\AdminReportProductView'                               => __DIR__ . '/src/Report/Service/Content/AdminReportProductView.php',
  'Report\Service\Navigation\AdminReportNavigation'                             => __DIR__ . '/src/Report/Service/Navigation/AdminReportNavigation.php',
  'Report\Service\Grid\AdminOrderSaleReport\SaleReport'                         => __DIR__ . '/src/Report/Service/Grid/AdminOrderSaleReport/SaleReport.php',
  'Report\Service\Grid\AdminOrderSaleReport\SaleReportMonth'                    => __DIR__ . '/src/Report/Service/Grid/AdminOrderSaleReport/SaleReportMonth.php',
  'Report\Service\Grid\AdminOrderSaleReport\SaleReportWeek'                     => __DIR__ . '/src/Report/Service/Grid/AdminOrderSaleReport/SaleReportWeek.php',
  'Report\Service\Grid\AdminOrderSaleReport\SaleReportYear'                     => __DIR__ . '/src/Report/Service/Grid/AdminOrderSaleReport/SaleReportYear.php',
  'Report\Service\Grid\AdminOrderSaleReport\SaleReportDay'                      => __DIR__ . '/src/Report/Service/Grid/AdminOrderSaleReport/SaleReportDay.php',
  'Report\Service\Grid\AdminProductIndividualReport\ReportDay'                  => __DIR__ . '/src/Report/Service/Grid/AdminProductIndividualReport/ReportDay.php',
  'Report\Service\Grid\AdminProductIndividualReport\ReportCustomer'             => __DIR__ . '/src/Report/Service/Grid/AdminProductIndividualReport/ReportCustomer.php',
  'Report\Service\Grid\AdminProductIndividualReport\ReportYear'                 => __DIR__ . '/src/Report/Service/Grid/AdminProductIndividualReport/ReportYear.php',
  'Report\Service\Grid\AdminProductIndividualReport\ReportMonth'                => __DIR__ . '/src/Report/Service/Grid/AdminProductIndividualReport/ReportMonth.php',
  'Report\Service\Grid\AdminProductIndividualReport\ReportWeek'                 => __DIR__ . '/src/Report/Service/Grid/AdminProductIndividualReport/ReportWeek.php',
  'Report\Service\Grid\AdminOrderPendingReport\PendingReport'                   => __DIR__ . '/src/Report/Service/Grid/AdminOrderPendingReport/PendingReport.php',
  'Report\Service\Grid\AdminOrderPendingReport\PendingReportMonth'              => __DIR__ . '/src/Report/Service/Grid/AdminOrderPendingReport/PendingReportMonth.php',
  'Report\Service\Grid\AdminOrderPendingReport\PendingReportWeek'               => __DIR__ . '/src/Report/Service/Grid/AdminOrderPendingReport/PendingReportWeek.php',
  'Report\Service\Grid\AdminOrderPendingReport\PendingReportDay'                => __DIR__ . '/src/Report/Service/Grid/AdminOrderPendingReport/PendingReportDay.php',
  'Report\Service\Grid\AdminOrderPendingReport\PendingReportYear'               => __DIR__ . '/src/Report/Service/Grid/AdminOrderPendingReport/PendingReportYear.php',
  'Report\Service\Grid\AdminProductReport'                                      => __DIR__ . '/src/Report/Service/Grid/AdminProductReport.php',
  'Report\Service\Grid\AdminUserIndividualReport\ReportDay'                     => __DIR__ . '/src/Report/Service/Grid/AdminUserIndividualReport/ReportDay.php',
  'Report\Service\Grid\AdminUserIndividualReport\ReportProduct'                 => __DIR__ . '/src/Report/Service/Grid/AdminUserIndividualReport/ReportProduct.php',
  'Report\Service\Grid\AdminUserIndividualReport\ReportYear'                    => __DIR__ . '/src/Report/Service/Grid/AdminUserIndividualReport/ReportYear.php',
  'Report\Service\Grid\AdminUserIndividualReport\ReportMonth'                   => __DIR__ . '/src/Report/Service/Grid/AdminUserIndividualReport/ReportMonth.php',
  'Report\Service\Grid\AdminUserIndividualReport\ReportWeek'                    => __DIR__ . '/src/Report/Service/Grid/AdminUserIndividualReport/ReportWeek.php',
  'Report\Service\Grid\AdminCustomerReport'                                     => __DIR__ . '/src/Report/Service/Grid/AdminCustomerReport.php',
  'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReport'      => __DIR__ . '/src/Report/Service/Grid/AdminCustomerRegisterReport/CustomerRegisterReport.php',
  'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportMonth' => __DIR__ . '/src/Report/Service/Grid/AdminCustomerRegisterReport/CustomerRegisterReportMonth.php',
  'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportYear'  => __DIR__ . '/src/Report/Service/Grid/AdminCustomerRegisterReport/CustomerRegisterReportYear.php',
  'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportDay'   => __DIR__ . '/src/Report/Service/Grid/AdminCustomerRegisterReport/CustomerRegisterReportDay.php',
  'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportWeek'  => __DIR__ . '/src/Report/Service/Grid/AdminCustomerRegisterReport/CustomerRegisterReportWeek.php',
  'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportWeek'           => __DIR__ . '/src/Report/Service/Grid/AdminOrderCancelledReport/CancelledReportWeek.php',
  'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportDay'            => __DIR__ . '/src/Report/Service/Grid/AdminOrderCancelledReport/CancelledReportDay.php',
  'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportYear'           => __DIR__ . '/src/Report/Service/Grid/AdminOrderCancelledReport/CancelledReportYear.php',
  'Report\Service\Grid\AdminOrderCancelledReport\CancelledReport'               => __DIR__ . '/src/Report/Service/Grid/AdminOrderCancelledReport/CancelledReport.php',
  'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportMonth'          => __DIR__ . '/src/Report/Service/Grid/AdminOrderCancelledReport/CancelledReportMonth.php',
  'Report\Service\Block\AdminProductReport'                                     => __DIR__ . '/src/Report/Service/Block/AdminProductReport.php',
  'Report\Service\Block\AdminUserReport'                                        => __DIR__ . '/src/Report/Service/Block/AdminUserReport.php',
  'Report\Service\Tab\AdminOrderSaleReport'                                     => __DIR__ . '/src/Report/Service/Tab/AdminOrderSaleReport.php',
  'Report\Service\Tab\AdminCustomerRegisterReport'                              => __DIR__ . '/src/Report/Service/Tab/AdminCustomerRegisterReport.php',
  'Report\Service\Tab\AdminOrderCancelledReport'                                => __DIR__ . '/src/Report/Service/Tab/AdminOrderCancelledReport.php',
  'Report\Service\Tab\AdminOrderPendingReport'                                  => __DIR__ . '/src/Report/Service/Tab/AdminOrderPendingReport.php',
  'Report\Controller\AdminOrderPendingReportController'                         => __DIR__ . '/src/Report/Controller/AdminOrderPendingReportController.php',
  'Report\Controller\AdminCustomerRegisterReportController'                     => __DIR__ . '/src/Report/Controller/AdminCustomerRegisterReportController.php',
  'Report\Controller\AdminOrderSaleReportController'                            => __DIR__ . '/src/Report/Controller/AdminOrderSaleReportController.php',
  'Report\Controller\AdminProductReportController'                              => __DIR__ . '/src/Report/Controller/AdminProductReportController.php',
  'Report\Controller\AdminOrderCancelledReportController'                       => __DIR__ . '/src/Report/Controller/AdminOrderCancelledReportController.php',
  'Report\Controller\AdminCustomerReportController'                             => __DIR__ . '/src/Report/Controller/AdminCustomerReportController.php',
  'Report\Form\AdminCustomerReportSearch'                                       => __DIR__ . '/src/Report/Form/AdminCustomerReportSearch.php',
  'Report\Form\AdminProductIndividualReportSearch'                              => __DIR__ . '/src/Report/Form/AdminProductIndividualReportSearch.php',
  'Report\Form\AdminOrderPendingReportSearch'                                   => __DIR__ . '/src/Report/Form/AdminOrderPendingReportSearch.php',
  'Report\Form\AdminUserReportSearch'                                           => __DIR__ . '/src/Report/Form/AdminUserReportSearch.php',
  'Report\Form\AdminOrderSaleReportSearch'                                      => __DIR__ . '/src/Report/Form/AdminOrderSaleReportSearch.php',
  'Report\Form\AdminOrderCancelledReportSearch'                                 => __DIR__ . '/src/Report/Form/AdminOrderCancelledReportSearch.php',
  'Report\Form\AdminUserIndividualReportSearch'                                 => __DIR__ . '/src/Report/Form/AdminUserIndividualReportSearch.php',
  'Report\Form\AdminProductReportSearch'                                        => __DIR__ . '/src/Report/Form/AdminProductReportSearch.php',
  'Report\Form\AdminCustomerRegisterReportSearch'                               => __DIR__ . '/src/Report/Form/AdminCustomerRegisterReportSearch.php',
  'Report\Module'                                                               => __DIR__ . '/Module.php',
);
