<?php
namespace Report\Form;

use OrderMain\Api\OrderStatusApi;
use Common\Form\Form;
use Zend\Form\Element\Select;

class AdminCustomerReportSearch extends Form {
	public function __construct($name = "adminCustomerReportSearch") {
		parent::__construct ( $name );
		$this->initPreEvent ();
		
		$this->add ( array (
				'name' => 'createdDate',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-gte date',
						'placeholder' => 'Order Start Date' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'createdDate_1',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-lte date element-createdDate',
						'placeholder' => 'Order End Date' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Search'
				)
		), array (
				'priority' => - 1000
		) );
		$this->initPostEvent ();
	}
	
}