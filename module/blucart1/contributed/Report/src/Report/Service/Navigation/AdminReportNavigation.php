<?php 
namespace Report\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminReportNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'reportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Order Report',
							'uri' => '#',
							'id' => 'orderReportMain',
							'iconClass' => 'glyphicon-print' 
					) 
			) );
		}
		$page = $navigation->findOneBy ( 'id', 'orderReportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Order Profit Report',
							'route' => 'admin-order-profit-report',
							'id' => 'admin-order-profit-report',
							'iconClass' => 'glyphicon-signal'
					)
			) );
		}
		$page = $navigation->findOneBy ( 'id', 'orderReportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Sale Report',
							'route' => 'admin-order-sale-report',
							'id' => 'admin-order-sale-report',
							'iconClass' => 'glyphicon-signal'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderReportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Pending Report',
							'route' => 'admin-order-pending-report',
							'id' => 'admin-order-pending-report',
							'iconClass' => 'glyphicon-signal'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderReportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Cancelled Report',
							'route' => 'admin-order-cancelled-report',
							'id' => 'admin-order-cancelled-report',
							'iconClass' => 'glyphicon-signal'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'reportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Product Report',
							'route' => 'admin-product-report',
							'id' => 'admin-product-report',
							'iconClass' => 'glyphicon-signal'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'reportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Customer Report',
							'uri' => '#',
							'id' => 'customerReportMain',
							'iconClass' => 'glyphicon-print'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'customerReportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'User Report',
							'route' => 'admin-customer-register-report',
							'id' => 'admin-customer-register-report',
							'iconClass' => 'glyphicon-signal'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'customerReportMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Sale Report',
							'route' => 'admin-customer-report',
							'id' => 'admin-customer-report',
							'iconClass' => 'glyphicon-signal'
					)
			) );
		}
	}
}

