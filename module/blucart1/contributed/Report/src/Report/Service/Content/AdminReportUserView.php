<?php 
namespace Report\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminReportUserView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-report-user-view';
	protected $_contentName = 'adminReportProductView';
	public function getEventName() {
		return 'adminUserView';
	}
	public function getPriority() {
		return 1000;
	}
	
}

