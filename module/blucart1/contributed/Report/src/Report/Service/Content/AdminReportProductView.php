<?php 
namespace Report\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminReportProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-report-product-view';
	protected $_contentName = 'adminReportProductView';
	public function getEventName() {
		return 'adminProductView';
	}
	public function getPriority() {
		return 1000;
	}
	
}

