<?php

namespace Report\Service\Grid\AdminUserIndividualReport;

use Core\Functions;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use OrderMain\Api\OrderStatusApi;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class ReportDay extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array ();
	protected $_entity = '\OrderMain\Entity\OrderMain';
	protected $_entityName = 'orderMain';
	public function getEventName() {
		return 'adminUserIndividualReportDay';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'orderDate' );
		$columnItem->setTitle ( 'Date' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsGrouped ( true );
		$columns->addColumn ( "DateFormat(orderMain.createdDate, '%D %M, %Y')", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'orders' );
		$columnItem->setTitle ( 'Orders' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:N0}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Count: #=calcOrders()#" );
		$columns->addColumn ( 'count(orderMain.id)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'products' );
		$columnItem->setTitle ( 'Products' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:N0}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Count: #=calcProducts()#" );
		$columns->addColumn ( 'count(product.id)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'amount' );
		$columnItem->setTitle ( 'Revenue' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sum: #=calcAmount()#" );
		$columns->addColumn ( 'sum(orderMain.amount)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Order Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columnItem->setHidden ( true );
		$columns->addColumn ( "orderMain.createdDate", $columnItem );
		
		return $columns;
	}
	public function preRead($e) {
		$userId = Functions::fromQuery ( 'userId' );
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem2 = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem2 );
		$joinItem = new QueryJoinItem ( 'orderMain.user', 'user', 'inner join' );
		$item->addJoin ( $joinItem );
		
		$joinItem = new QueryJoinItem ( '\OrderMain\Entity\OrderProduct', 'orderProduct', 'left join', 'orderMain.id = orderProduct.orderMain' );
		$joinItem2 = new QueryJoinItem ( 'orderProduct.product', 'product' );
		$joinItem->addJoin ( $joinItem2 );
		$item->addJoin($joinItem);
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		$queryBuilder->addWhere ( 'user.id', 'userId' );
		$queryBuilder->setParameter ( 'userId', $userId );
		
		$queryBuilder->addOrder ( 'orderMain.createdDate', 'orderMainCreatedDate', 'desc' );
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		return $queryBuilder;
	}
}
