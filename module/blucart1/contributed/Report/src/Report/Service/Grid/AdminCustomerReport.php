<?php

namespace Report\Service\Grid;

use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use OrderMain\Api\OrderStatusApi;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminCustomerReport extends AbstractMainBlucartGridEvent {
	protected $_entity = 'OrderMain\Entity\OrderProduct';
	protected $_entityName = 'orderProduct';
	public function getEventName() {
		return 'adminProductReport';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productId' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( "product.id", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productTitle' );
		$columnItem->setTitle ( 'Product Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( "product.title", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productSku' );
		$columnItem->setTitle ( 'SKU' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( "product.sku", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'qty' );
		$columnItem->setTitle ( 'Stock Sold' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Stock Sold: #=totalStockSold()#" );
		$columns->addColumn ( "sum(orderProduct.qty)", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'sellPrice' );
		$columnItem->setTitle ( 'Sell Price' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 960 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sell Price: #=totalSellPrice()#" );
		$columns->addColumn ( "sum(orderProduct.sellPrice * orderProduct.qty)", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'costPrice' );
		$columnItem->setTitle ( 'Cost Price' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Cost Price: #=totalCostPrice()#" );
		$columns->addColumn ( "sum(orderProduct.costPrice * orderProduct.qty)", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'amount' );
		$columnItem->setTitle ( 'Amount Paid' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 940 );
		$columnItem->setFooterTemplate ( "Amount Paid: #=totalAmountPaid()#" );
		$columns->addColumn ( "orderProduct.amount", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'profit' );
		$columnItem->setTitle ( 'Profit' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 930 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Profit: #=totalProfit()#" );
		$columns->addColumn ( "(sum(orderProduct.amount)-sum(orderProduct.costPrice * orderProduct.qty))", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Order Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 920 );
		$columnItem->setHidden ( true );
		$columns->addColumn ( "orderMain.createdDate", $columnItem );
		
		return $columns;
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		$joinItem = new QueryJoinItem ( $this->_entityName . '.product', 'product', ' inner join ' );
		$fromItem->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem ( $this->_entityName . '.orderMain', 'orderMain', ' inner join ' );
		$fromItem->addJoin ( $joinItem );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		$queryBuilder->addGroup ( "product.id", 'productId' );
		return $fromItem;
	}
}
