<?php
namespace Report\Service\Grid\AdminOrderSaleReport;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use OrderMain\Api\OrderStatusApi;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class SaleReport extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array ();
	protected $_entity = '\OrderMain\Entity\OrderMain';
	protected $_entityName = 'orderMain';
	public function getEventName() {
		return 'adminOrderReport';
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		$joinItem = new QueryJoinItem ( $this->_entityName . '.orderStatus', 'orderStatus' );
		$fromItem->addJoin ( $joinItem );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
	
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
	
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( $this->_entityName . '.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
				
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		$queryBuilder->addGroup ( "orderDate", 'orderDate' );
		return $fromItem;
	}

}
