<?php

namespace Report\Service\Grid;


use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use OrderMain\Api\OrderStatusApi;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminProductReport extends AbstractMainBlucartGridEvent {
	protected $_entity = '\OrderMain\Entity\OrderMain';
	protected $_entityName = 'orderMain';
	public function getEventName() {
		return 'adminCustomerReport';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'userId' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsGrouped(true);
		$columns->addColumn ( "user.id", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'userFirstName' );
		$columnItem->setTitle ( 'First Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( "user.firstName", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'userLastName' );
		$columnItem->setTitle ( 'Last Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( "user.lastName", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'userLastName' );
		$columnItem->setTitle ( 'Last Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( "user.lastName", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'userEmail' );
		$columnItem->setTitle ( 'Email' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( "user.email", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'amount' );
		$columnItem->setTitle ( 'Revenue' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sum: #=totalAmount()#" );
		$columns->addColumn ( 'sum(orderMain.amount)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Order Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 940 );
		$columnItem->setHidden ( true );
		$columns->addColumn ( "orderMain.createdDate", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'orderStatusId' );
		$columnItem->setTitle ( 'orderStatusId' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 930 );
		$columnItem->setHidden ( true );
		$columns->addColumn ( "orderStatus.id", $columnItem );
		
		return $columns;
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		$joinItem = new QueryJoinItem ( $this->_entityName . '.orderStatus', 'orderStatus' );
		$fromItem->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem ( $this->_entityName . '.user', 'user', 'inner join' );
		$fromItem->addJoin ( $joinItem );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( $this->_entityName . '.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		$queryBuilder->addGroup ( "userId", 'userId' );
		return $fromItem;
	}
}
