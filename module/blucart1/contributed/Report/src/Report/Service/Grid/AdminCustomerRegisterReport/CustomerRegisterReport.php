<?php
namespace Report\Service\Grid\AdminCustomerRegisterReport;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use OrderMain\Api\OrderStatusApi;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class CustomerRegisterReport extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array ();
	protected $_entity = '\User\Entity\User';
	protected $_entityName = 'user';
	public function getEventName() {
		return 'adminCustomerRegisterReport';
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$queryBuilder->addGroup ( "registeredDate", 'registeredDate' );
		return $fromItem;
	}

}
