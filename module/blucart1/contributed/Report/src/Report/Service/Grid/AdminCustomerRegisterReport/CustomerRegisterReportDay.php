<?php
namespace Report\Service\Grid\AdminCustomerRegisterReport;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use OrderMain\Api\OrderStatusApi;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class CustomerRegisterReportDay extends CustomerRegisterReport {
	public function getEventName() {
		return 'adminCustomerRegisterReportDay';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'registeredDate' );
		$columnItem->setTitle ( 'Date' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsGrouped ( true );
		$columns->addColumn ( "DateFormat(user.createdDate, '%D %M, %Y')", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'userCount' );
		$columnItem->setTitle ( 'Users Count' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:N0}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sum: #=mysum()#" );
		$columns->addColumn ( 'count(user.id)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Created Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columnItem->setHidden ( true );
		$columns->addColumn ( "user.createdDate", $columnItem );
		
		return $columns;
	}
	
}
