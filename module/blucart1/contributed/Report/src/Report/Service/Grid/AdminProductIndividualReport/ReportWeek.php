<?php

namespace Report\Service\Grid\AdminProductIndividualReport;

use Core\Functions;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use OrderMain\Api\OrderStatusApi;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class ReportWeek extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array ();
	protected $_entity = '\OrderMain\Entity\OrderProduct';
	protected $_entityName = 'orderProduct';
	public function getEventName() {
		return 'adminProductIndividualReportWeek';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'orderDate' );
		$columnItem->setTitle ( 'Date' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsGrouped ( true );
		$columns->addColumn ( "DateFormat(orderMain.createdDate, '%v, %x')", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'orders' );
		$columnItem->setTitle ( 'Total Orders' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:N0}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Count: #=calcOrders()#" );
		$columns->addColumn ( 'count(orderMain.id)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'qty' );
		$columnItem->setTitle ( 'Units Sold' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:N0}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sum: #=calcQty()#" );
		$columns->addColumn ( 'sum(orderProduct.qty)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'amount' );
		$columnItem->setTitle ( 'Revenue' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sum: #=calcAmount()#" );
		$columns->addColumn ( 'sum(orderProduct.amount)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'costPrice' );
		$columnItem->setTitle ( 'Cost Price' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 960 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sum: #=calcCostPrice()#" );
		$columns->addColumn ( 'sum(orderProduct.costPrice * orderProduct.qty)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'profit' );
		$columnItem->setTitle ( 'Profit' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sum: #=calcProfit()#" );
		$columns->addColumn ( 'sum(orderProduct.amount - (orderProduct.costPrice * orderProduct.qty) )', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Order Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columnItem->setHidden ( true );
		$columns->addColumn ( "orderMain.createdDate", $columnItem );
		
		return $columns;
	}
	public function preRead($e) {
		$productId = Functions::fromQuery ( 'productId' );
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, 'orderProduct' );
		$joinItem = new QueryJoinItem ( 'orderProduct.orderMain', 'orderMain' );
		$item->addJoin ( $joinItem );
		$joinItem2 = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$joinItem->addJoin ( $joinItem2 );
		$joinItem = new QueryJoinItem ( 'orderProduct.product', 'product', 'inner join' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		$queryBuilder->addWhere ( 'product.id', 'productId' );
		$queryBuilder->setParameter ( 'productId', $productId );
		
		$queryBuilder->addOrder('orderMain.createdDate','orderMainCreatedDate','desc');
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		return $queryBuilder;
	}
}
