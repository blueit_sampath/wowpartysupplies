<?php
namespace Report\Service\Grid\AdminOrderSaleReport;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use OrderMain\Api\OrderStatusApi;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class SaleReportDay extends SaleReport {
	public function getEventName() {
		return 'adminOrderSaleReportDay';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'orderDate' );
		$columnItem->setTitle ( 'Date' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsGrouped ( true );
		$columns->addColumn ( "DateFormat(orderMain.createdDate, '%D %M, %Y')", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'amount' );
		$columnItem->setTitle ( 'Revenue' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:C}' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columnItem->setIsHaving ( true );
		$columnItem->setFooterTemplate ( "Sum: #=mysum()#" );
		$columns->addColumn ( 'sum(orderMain.amount)', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Order Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columnItem->setHidden ( true );
		$columns->addColumn ( "orderMain.createdDate", $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'orderStatusId' );
		$columnItem->setTitle ( 'orderStatusId' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columnItem->setHidden ( true );
		$columns->addColumn ( "orderStatus.id", $columnItem );
		
		return $columns;
	}
	
}
