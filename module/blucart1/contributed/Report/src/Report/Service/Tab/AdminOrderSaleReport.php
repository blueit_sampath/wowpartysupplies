<?php 
namespace Report\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminOrderSaleReport extends AbstractTabEvent {
	public function getEventName() {
		return 'adminOrderSaleReport';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-order-sale-report', array('action' => 'day'));
		$tabContainer->add ( 'admin-order-sale-report-day', 'Day', $u,1000 );
		
		$u = $url ( 'admin-order-sale-report', array('action' => 'week'));
		$tabContainer->add ( 'admin-order-sale-report-week', 'Week', $u,900 );
		
		$u = $url ( 'admin-order-sale-report', array('action' => 'month'));
		$tabContainer->add ( 'admin-order-sale-report-month', 'Month', $u,800 );
		
		$u = $url ( 'admin-order-sale-report', array('action' => 'year'));
		$tabContainer->add ( 'admin-order-sale-report-year', 'Year', $u,700 );
		
		return $this;
	}
}

