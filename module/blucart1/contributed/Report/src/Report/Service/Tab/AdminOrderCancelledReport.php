<?php 
namespace Report\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminOrderCancelledReport extends AbstractTabEvent {
	public function getEventName() {
		return 'adminOrderCancelledReport';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-order-cancelled-report', array('action' => 'day'));
		$tabContainer->add ( 'admin-order-cancelled-report-day', 'Day', $u,1000 );
		
		$u = $url ( 'admin-order-cancelled-report', array('action' => 'week'));
		$tabContainer->add ( 'admin-order-cancelled-report-week', 'Week', $u,900 );
		
		$u = $url ( 'admin-order-cancelled-report', array('action' => 'month'));
		$tabContainer->add ( 'admin-order-cancelled-report-month', 'Month', $u,800 );
		
		$u = $url ( 'admin-order-cancelled-report', array('action' => 'year'));
		$tabContainer->add ( 'admin-order-cancelled-report-year', 'Year', $u,700 );
		
		return $this;
	}
}

