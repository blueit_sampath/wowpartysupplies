<?php 
namespace Report\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminOrderPendingReport extends AbstractTabEvent {
	public function getEventName() {
		return 'adminOrderPendingReport';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-order-pending-report', array('action' => 'day'));
		$tabContainer->add ( 'admin-order-pending-report-day', 'Day', $u,1000 );
		
		$u = $url ( 'admin-order-pending-report', array('action' => 'week'));
		$tabContainer->add ( 'admin-order-pending-report-week', 'Week', $u,900 );
		
		$u = $url ( 'admin-order-pending-report', array('action' => 'month'));
		$tabContainer->add ( 'admin-order-pending-report-month', 'Month', $u,800 );
		
		$u = $url ( 'admin-order-pending-report', array('action' => 'year'));
		$tabContainer->add ( 'admin-order-pending-report-year', 'Year', $u,700 );
		
		return $this;
	}
}

