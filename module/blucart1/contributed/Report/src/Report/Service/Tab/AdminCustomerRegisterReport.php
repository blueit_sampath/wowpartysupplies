<?php 
namespace Report\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCustomerRegisterReport extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCustomerRegisterReport';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-customer-register-report', array('action' => 'day'));
		$tabContainer->add ( 'admin-customer-register-report-day', 'Day', $u,1000 );
		
		$u = $url ( 'admin-customer-register-report', array('action' => 'week'));
		$tabContainer->add ( 'admin-customer-register-report-week', 'Week', $u,900 );
		
		$u = $url ( 'admin-customer-register-report', array('action' => 'month'));
		$tabContainer->add ( 'admin-customer-register-report-month', 'Month', $u,800 );
		
		$u = $url ( 'admin-customer-register-report', array('action' => 'year'));
		$tabContainer->add ( 'admin-customer-register-report-year', 'Year', $u,700 );
		
		return $this;
	}
}

