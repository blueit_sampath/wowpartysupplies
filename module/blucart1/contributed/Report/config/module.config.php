<?php

return array(
    'router' => array('routes' => array(
            'admin-order-sale-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/sale/report[/:action]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\AdminOrderSaleReport',
                        'action' => 'month'
                    ),
                    'constraints' => array('action' => '[a-zA-Z][a-zA-Z0-9_-]*')
                )
            ),
            'admin-order-profit-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/profit/report',
                    'defaults' => array(
                        'controller' => 'Report\Controller\AdminOrderProfitReport',
                        'action' => 'index'
                    )
                )
            ),
            'admin-order-pending-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/pending/report[/:action]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\AdminOrderPendingReport',
                        'action' => 'month'
                    ),
                    'constraints' => array('action' => '[a-zA-Z][a-zA-Z0-9_-]*')
                )
            ),
            'admin-order-cancelled-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/cancelled/report[/:action]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\AdminOrderCancelledReport',
                        'action' => 'month'
                    ),
                    'constraints' => array('action' => '[a-zA-Z][a-zA-Z0-9_-]*')
                )
            ),
            'admin-product-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/report[/:action]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\AdminProductReport',
                        'action' => 'index'
                    ),
                    'constraints' => array('action' => '[a-zA-Z][a-zA-Z0-9_-]*')
                )
            ),
            'admin-customer-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/customer/report[/:action]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\AdminCustomerReport',
                        'action' => 'index'
                    ),
                    'constraints' => array('action' => '[a-zA-Z][a-zA-Z0-9_-]*')
                )
            ),
            'admin-customer-register-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/customer/register/report[/:action]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\AdminCustomerRegisterReport',
                        'action' => 'month'
                    ),
                    'constraints' => array('action' => '[a-zA-Z][a-zA-Z0-9_-]*')
                )
            )
        )),
    'events' => array(
        'Report\Service\Grid\AdminOrderSaleReport\SaleReport' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReport',
        'Report\Service\Grid\AdminOrderSaleReport\SaleReportMonth' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReportMonth',
        'Report\Service\Grid\AdminOrderSaleReport\SaleReportDay' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReportDay',
        'Report\Service\Grid\AdminOrderSaleReport\SaleReportYear' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReportYear',
        'Report\Service\Grid\AdminOrderSaleReport\SaleReportWeek' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReportWeek',
        'Report\Service\Tab\AdminOrderSaleReport' => 'Report\Service\Tab\AdminOrderSaleReport',
        'Report\Service\Grid\AdminOrderPendingReport\PendingReport' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReport',
        'Report\Service\Grid\AdminOrderPendingReport\PendingReportMonth' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReportMonth',
        'Report\Service\Grid\AdminOrderPendingReport\PendingReportDay' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReportDay',
        'Report\Service\Grid\AdminOrderPendingReport\PendingReportYear' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReportYear',
        'Report\Service\Grid\AdminOrderPendingReport\PendingReportWeek' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReportWeek',
        'Report\Service\Tab\AdminOrderPendingReport' => 'Report\Service\Tab\AdminOrderPendingReport',
        'Report\Service\Grid\AdminOrderCancelledReport\CancelledReport' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReport',
        'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportMonth' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportMonth',
        'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportDay' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportDay',
        'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportYear' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportYear',
        'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportWeek' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportWeek',
        'Report\Service\Tab\AdminOrderCancelledReport' => 'Report\Service\Tab\AdminOrderCancelledReport',
        'Report\Service\Grid\AdminProductReport' => 'Report\Service\Grid\AdminProductReport',
        'Report\Service\Grid\AdminCustomerReport' => 'Report\Service\Grid\AdminCustomerReport',
        'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReport' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReport',
        'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportMonth' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportMonth',
        'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportDay' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportDay',
        'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportYear' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportYear',
        'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportWeek' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportWeek',
        'Report\Service\Tab\AdminCustomerRegisterReport' => 'Report\Service\Tab\AdminCustomerRegisterReport',
        'Report\Service\Content\AdminReportProductView' => 'Report\Service\Content\AdminReportProductView',
        'Report\Service\Grid\AdminProductIndividualReport\ReportDay' => 'Report\Service\Grid\AdminProductIndividualReport\ReportDay',
        'Report\Service\Grid\AdminProductIndividualReport\ReportWeek' => 'Report\Service\Grid\AdminProductIndividualReport\ReportWeek',
        'Report\Service\Grid\AdminProductIndividualReport\ReportMonth' => 'Report\Service\Grid\AdminProductIndividualReport\ReportMonth',
        'Report\Service\Grid\AdminProductIndividualReport\ReportYear' => 'Report\Service\Grid\AdminProductIndividualReport\ReportYear',
        'Report\Service\Grid\AdminProductIndividualReport\ReportCustomer' => 'Report\Service\Grid\AdminProductIndividualReport\ReportCustomer',
        'Report\Service\Content\AdminReportUserView' => 'Report\Service\Content\AdminReportUserView',
        'Report\Service\Grid\AdminUserIndividualReport\ReportDay' => 'Report\Service\Grid\AdminUserIndividualReport\ReportDay',
        'Report\Service\Grid\AdminUserIndividualReport\ReportWeek' => 'Report\Service\Grid\AdminUserIndividualReport\ReportWeek',
        'Report\Service\Grid\AdminUserIndividualReport\ReportMonth' => 'Report\Service\Grid\AdminUserIndividualReport\ReportMonth',
        'Report\Service\Grid\AdminUserIndividualReport\ReportYear' => 'Report\Service\Grid\AdminUserIndividualReport\ReportYear',
        'Report\Service\Grid\AdminUserIndividualReport\ReportProduct' => 'Report\Service\Grid\AdminUserIndividualReport\ReportProduct',
        'Report\Service\Navigation\AdminReportNavigation' => 'Report\Service\Navigation\AdminReportNavigation',
        'Report\Service\Block\AdminProductReport' => 'Report\Service\Block\AdminProductReport',
        'Report\Service\Block\AdminUserReport' => 'Report\Service\Block\AdminUserReport',
        'Report\Service\Grid\AdminOrderProfitReport' => 'Report\Service\Grid\AdminOrderProfitReport',
    ),
    'service_manager' => array(
        'invokables' => array(
            'Report\Service\Grid\AdminOrderSaleReport\SaleReport' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReport',
            'Report\Form\AdminOrderSaleReportSearch' => 'Report\Form\AdminOrderSaleReportSearch',
            'Report\Service\Grid\AdminOrderSaleReport\SaleReportMonth' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReportMonth',
            'Report\Service\Grid\AdminOrderSaleReport\SaleReportDay' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReportDay',
            'Report\Service\Grid\AdminOrderSaleReport\SaleReportYear' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReportYear',
            'Report\Service\Grid\AdminOrderSaleReport\SaleReportWeek' => 'Report\Service\Grid\AdminOrderSaleReport\SaleReportWeek',
            'Report\Service\Tab\AdminOrderSaleReport' => 'Report\Service\Tab\AdminOrderSaleReport',
            'Report\Service\Grid\AdminOrderPendingReport\PendingReport' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReport',
            'Report\Form\AdminOrderPendingReportSearch' => 'Report\Form\AdminOrderPendingReportSearch',
            'Report\Service\Grid\AdminOrderPendingReport\PendingReportMonth' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReportMonth',
            'Report\Service\Grid\AdminOrderPendingReport\PendingReportDay' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReportDay',
            'Report\Service\Grid\AdminOrderPendingReport\PendingReportYear' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReportYear',
            'Report\Service\Grid\AdminOrderPendingReport\PendingReportWeek' => 'Report\Service\Grid\AdminOrderPendingReport\PendingReportWeek',
            'Report\Service\Tab\AdminOrderPendingReport' => 'Report\Service\Tab\AdminOrderPendingReport',
            'Report\Service\Grid\AdminOrderCancelledReport\CancelledReport' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReport',
            'Report\Form\AdminOrderCancelledReportSearch' => 'Report\Form\AdminOrderCancelledReportSearch',
            'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportMonth' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportMonth',
            'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportDay' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportDay',
            'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportYear' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportYear',
            'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportWeek' => 'Report\Service\Grid\AdminOrderCancelledReport\CancelledReportWeek',
            'Report\Service\Tab\AdminOrderCancelledReport' => 'Report\Service\Tab\AdminOrderCancelledReport',
            'Report\Service\Grid\AdminProductReport' => 'Report\Service\Grid\AdminProductReport',
            'Report\Form\AdminProductReportSearch' => 'Report\Form\AdminProductReportSearch',
            'Report\Service\Grid\AdminCustomerReport' => 'Report\Service\Grid\AdminCustomerReport',
            'Report\Form\AdminCustomerReportSearch' => 'Report\Form\AdminCustomerReportSearch',
            'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReport' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReport',
            'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportMonth' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportMonth',
            'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportDay' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportDay',
            'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportYear' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportYear',
            'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportWeek' => 'Report\Service\Grid\AdminCustomerRegisterReport\CustomerRegisterReportWeek',
            'Report\Service\Tab\AdminCustomerRegisterReport' => 'Report\Service\Tab\AdminCustomerRegisterReport',
            'Report\Form\AdminCustomerRegisterReportSearch' => 'Report\Form\AdminCustomerRegisterReportSearch',
            'Report\Form\AdminProductIndividualReportSearch' => 'Report\Form\AdminProductIndividualReportSearch',
            'Report\Service\Content\AdminReportProductView' => 'Report\Service\Content\AdminReportProductView',
            'Report\Service\Grid\AdminProductIndividualReport\ReportDay' => 'Report\Service\Grid\AdminProductIndividualReport\ReportDay',
            'Report\Service\Grid\AdminProductIndividualReport\ReportWeek' => 'Report\Service\Grid\AdminProductIndividualReport\ReportWeek',
            'Report\Service\Grid\AdminProductIndividualReport\ReportMonth' => 'Report\Service\Grid\AdminProductIndividualReport\ReportMonth',
            'Report\Service\Grid\AdminProductIndividualReport\ReportYear' => 'Report\Service\Grid\AdminProductIndividualReport\ReportYear',
            'Report\Service\Grid\AdminProductIndividualReport\ReportCustomer' => 'Report\Service\Grid\AdminProductIndividualReport\ReportCustomer',
            'Report\Form\AdminUserIndividualReportSearch' => 'Report\Form\AdminUserIndividualReportSearch',
            'Report\Service\Content\AdminReportUserView' => 'Report\Service\Content\AdminReportUserView',
            'Report\Service\Grid\AdminUserIndividualReport\ReportDay' => 'Report\Service\Grid\AdminUserIndividualReport\ReportDay',
            'Report\Service\Grid\AdminUserIndividualReport\ReportWeek' => 'Report\Service\Grid\AdminUserIndividualReport\ReportWeek',
            'Report\Service\Grid\AdminUserIndividualReport\ReportMonth' => 'Report\Service\Grid\AdminUserIndividualReport\ReportMonth',
            'Report\Service\Grid\AdminUserIndividualReport\ReportYear' => 'Report\Service\Grid\AdminUserIndividualReport\ReportYear',
            'Report\Service\Grid\AdminUserIndividualReport\ReportProduct' => 'Report\Service\Grid\AdminUserIndividualReport\ReportProduct',
            'Report\Service\Navigation\AdminReportNavigation' => 'Report\Service\Navigation\AdminReportNavigation',
            'Report\Service\Block\AdminProductReport' => 'Report\Service\Block\AdminProductReport',
            'Report\Service\Block\AdminUserReport' => 'Report\Service\Block\AdminUserReport',
            'Report\Service\Grid\AdminOrderProfitReport' => 'Report\Service\Grid\AdminOrderProfitReport',
            
        ),
        'factories' => array()
    ),
    'controllers' => array('invokables' => array(
            'Report\Controller\AdminOrderSaleReport' => 'Report\Controller\AdminOrderSaleReportController',
            'Report\Controller\AdminOrderPendingReport' => 'Report\Controller\AdminOrderPendingReportController',
            'Report\Controller\AdminOrderCancelledReport' => 'Report\Controller\AdminOrderCancelledReportController',
            'Report\Controller\AdminProductReport' => 'Report\Controller\AdminProductReportController',
            'Report\Controller\AdminCustomerReport' => 'Report\Controller\AdminCustomerReportController',
            'Report\Controller\AdminCustomerRegisterReport' => 'Report\Controller\AdminCustomerRegisterReportController',
            'Report\Controller\AdminOrderProfitReport' => 'Report\Controller\AdminOrderProfitReportController'
        )),
    'view_manager' => array('template_path_stack' => array('Report' => __DIR__ . '/../view')),
    'asset_manager' => array('resolver_configs' => array('paths' => array('Report' => __DIR__ . '/../public')))
);
