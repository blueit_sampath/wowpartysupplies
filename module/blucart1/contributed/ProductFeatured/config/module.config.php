<?php

return array (
		'router' => array (
				'routes' => array (
						'admin-product-featured' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/featured',
										'defaults' => array (
												'controller' => 'ProductFeatured\Controller\AdminProductFeatured',
												'action' => 'index' 
										) 
								) 
						),
						'admin-product-featured-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/featured/add[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'ProductFeatured\Controller\AdminProductFeatured',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-product-featured-sort' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/product/featured/sort[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'ProductFeatured\Controller\AdminProductFeatured',
												'action' => 'sort',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'ProductFeatured\Service\Grid\AdminProductFeatured' => 'ProductFeatured\Service\Grid\AdminProductFeatured',
				'ProductFeatured\Service\Form\AdminProductFeatured' => 'ProductFeatured\Service\Form\AdminProductFeatured',
				'ProductFeatured\Service\Link\AdminProductFeatured' => 'ProductFeatured\Service\Link\AdminProductFeatured',
				'ProductFeatured\Service\NestedSorting\AdminProductFeatured' => 'ProductFeatured\Service\NestedSorting\AdminProductFeatured',
				'ProductFeatured\Service\Content\AdminProductFeaturedProductView' => 'ProductFeatured\Service\Content\AdminProductFeaturedProductView',
				'ProductFeatured\Service\Navigation\AdminProductFeaturedNavigation' => 'ProductFeatured\Service\Navigation\AdminProductFeaturedNavigation',
				'ProductFeatured\Service\Block\ProductFeatured' => 'ProductFeatured\Service\Block\ProductFeatured' 
		),
		'service_manager' => array (
				'invokables' => array (
						'ProductFeatured\Service\Grid\AdminProductFeatured' => 'ProductFeatured\Service\Grid\AdminProductFeatured',
						'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedForm' => 'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedForm',
						'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedFilter' => 'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedFilter',
						'ProductFeatured\Service\Form\AdminProductFeatured' => 'ProductFeatured\Service\Form\AdminProductFeatured',
						'ProductFeatured\Service\Link\AdminProductFeatured' => 'ProductFeatured\Service\Link\AdminProductFeatured',
						'ProductFeatured\Service\NestedSorting\AdminProductFeatured' => 'ProductFeatured\Service\NestedSorting\AdminProductFeatured',
						'ProductFeatured\Service\Content\AdminProductFeaturedProductView' => 'ProductFeatured\Service\Content\AdminProductFeaturedProductView',
						'ProductFeatured\Service\Navigation\AdminProductFeaturedNavigation' => 'ProductFeatured\Service\Navigation\AdminProductFeaturedNavigation',
						'ProductFeatured\Service\Block\ProductFeatured' => 'ProductFeatured\Service\Block\ProductFeatured' 
				),
				'factories' => array (
						'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedFactory' => 'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'ProductFeatured\Controller\AdminProductFeatured' => 'ProductFeatured\Controller\AdminProductFeaturedController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						'ProductFeatured' => __DIR__ . '/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						'ProductFeatured_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__ . '/../src/ProductFeatured/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										'ProductFeatured\Entity' => 'ProductFeatured_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'ProductFeatured' => __DIR__ . '/../public' 
						) 
				) 
		) 
);
