<?php 
namespace ProductFeatured\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminProductFeaturedController extends AbstractAdminController {
	
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-featured-add' );
	}
	
}