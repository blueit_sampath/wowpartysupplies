<?php
namespace ProductFeatured\Form\AdminProductFeatured;

use Common\Form\Option\AbstractFormFactory;

class ProductFeaturedFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedForm' );
		$form->setName ( 'adminProductFeaturedAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ProductFeatured\Form\AdminProductFeatured\ProductFeaturedFilter' );
	}
}
