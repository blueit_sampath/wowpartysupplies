<?php

namespace ProductFeatured\Form\AdminProductFeatured;

use Product\Api\ProductApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class ProductFeaturedForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$select = new Select ( 'products' );
		$select->setLabel ( 'Products' );
		$select->setAttribute ( 'multiple', true );
		$select->setValueOptions ( $this->getProducts () );
		$this->add ( $select, array (
				'priority' => 950 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getProducts() {
		$results = ProductApi::getAllProducts ();
		$array = array ();
		if ($results) {
			foreach ( $results as $result ) {
				$array [$result->id] = $result->title;
			}
		}
		return $array;
	}
}