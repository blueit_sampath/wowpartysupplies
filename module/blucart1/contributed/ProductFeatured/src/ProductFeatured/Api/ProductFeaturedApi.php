<?php

namespace ProductFeatured\Api;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Common\Api\Api;

class ProductFeaturedApi extends Api {
	protected static $_entity = '\ProductFeatured\Entity\ProductFeatured';
	public static function getProductFeaturedById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getFeaturedProducts($skip = 0, $take = PHP_INT_MAX, $status = true) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( 'productFeatured.id', 'productFeaturedId' );
		$queryBuilder->addColumn ( 'product.id', 'productId' );
		
		if ($status !== null) {
			$item = $queryBuilder->addWhere ( 'product.status', 'productStatus' );
			$queryBuilder->addParameter ( 'productStatus', $status );
		}
		
		$queryBuilder->addOrder ( 'productFeatured.weight', 'productFeaturedWeight', 'desc' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'productFeatured' );
		$joinItem = new QueryJoinItem ( 'productFeatured.product', 'product' );
		$item->addJoin ( $joinItem );
		
		return $queryBuilder->executeQuery ( $skip, $take );
	}
	public static function getProductFeaturedBProductyId($productId) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findOneBy ( array (
				'product' => $productId 
		) );
	}
}
