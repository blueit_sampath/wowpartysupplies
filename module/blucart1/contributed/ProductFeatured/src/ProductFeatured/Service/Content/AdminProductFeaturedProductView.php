<?php 
namespace ProductFeatured\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminProductFeaturedProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-product-featured-product-view';
	protected $_contentName = 'adminProductFeaturedProductView';
	public function getEventName() {
		return 'adminProductViewTop';
	}
	public function getPriority() {
		return 600;
	}
	
}

