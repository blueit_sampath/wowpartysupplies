<?php 
namespace ProductFeatured\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class ProductFeatured extends AbstractBlockEvent {

	protected $_blockTemplate = 'product-featured';

	public function getBlockName() {
		return 'productFeatured';
	}
	public function getPriority() {
		return 1000;
	}
	
}

