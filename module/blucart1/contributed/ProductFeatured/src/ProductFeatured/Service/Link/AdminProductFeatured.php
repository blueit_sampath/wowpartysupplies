<?php

namespace ProductFeatured\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProductFeatured extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminProductFeatured';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-product-featured-add' );
		$item = $linkContainer->add ( 'admin-product-featured-add', 'Add Products', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-product-featured-sort' );
		$item = $linkContainer->add ( 'admin-product-featured-sort', 'Arrange Products', $u, 900 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

