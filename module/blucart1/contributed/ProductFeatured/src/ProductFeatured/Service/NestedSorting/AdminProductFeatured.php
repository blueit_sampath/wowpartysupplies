<?php

namespace ProductFeatured\Service\NestedSorting;

use Core\Functions;
use ProductFeatured\Api\ProductFeaturedApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminProductFeatured extends AbstractNestedSortingEvent {
	protected $_entityName = '\ProductFeatured\Entity\ProductFeatured';
	public function getEventName() {
		return 'adminProductFeaturedSort';
	}
	public function getPriority() {
		return 1000;
	}
	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (), array (
				'weight' => 'desc' 
		) );
		if ($results) {
			
			foreach ( $results as $result ) {
				
				$array [$result->id] = array (
						'name' => $result->product->title 
				);
			}
		}
		return $array;
	}
}
