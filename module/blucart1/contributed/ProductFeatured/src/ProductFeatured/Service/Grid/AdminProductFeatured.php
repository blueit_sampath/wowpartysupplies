<?php

namespace ProductFeatured\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminProductFeatured extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'weight' 
	);
	protected $_entity = 'ProductFeatured\Entity\ProductFeatured';
	protected $_entityName = 'productFeatured';
	public function getEventName() {
		return 'adminProductFeatured';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'productFeatured.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productTitle' );
		$columnItem->setTitle ( 'Product Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'product.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Weight' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'productFeatured.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		
		$joinItem = new QueryJoinItem ( 'productFeatured.product', 'product' );
		$item->addJoin ( $joinItem );
		
		return true;
	}
}
