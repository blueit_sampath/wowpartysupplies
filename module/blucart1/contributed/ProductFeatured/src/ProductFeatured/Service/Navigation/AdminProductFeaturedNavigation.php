<?php 
namespace ProductFeatured\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminProductFeaturedNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'productMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Featured Products',
							'route' => 'admin-product-featured',
							'id' => 'admin-product-featured',
							'iconClass' => 'glyphicon glyphicon-star' 
					) 
			) );
		}
	}
}

