<?php

namespace ProductFeatured\Service\Form;

use ProductFeatured\Api\ProductFeaturedApi;
use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminProductFeatured extends AbstractMainFormEvent {
	protected $_columnKeys = array ();
	protected $_entity = '\ProductFeatured\Entity\ProductFeatured';
	protected $_entityName = 'productFeatured';
	public function getFormName() {
		return 'adminProductFeaturedAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		$results = ProductFeaturedApi::getFeaturedProducts ();
		$array = array ();
		if ($results) {
			foreach ( $results as $result ) {
				$array [] = ( int ) $result ['productId'];
			}
		}
		foreach ( $params ['products'] as $r ) {
			if (! in_array ( ( int ) $r, $array )) {
				$e = new $this->_entity;
				$e->product = ProductApi::getProductById ( $r );
				
				$em->persist ( $e );
				$em->flush ();
				
			}
		}
		
		
		
		return true;
	}
	public function getRecord() {
		return true;
	}
}
