<?php

namespace Search\Api;

use Common\Api\Api;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;

class SearchApi extends Api {

    protected static $_entity = 'Product\Entity\Product';

    public static function getProductsByString($string, $page = 1, $perPage = 10) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('product.id', 'productId');
        $item = $queryBuilder->addFrom(static::$_entity, 'product');

        $whereItem = $queryBuilder->addWhere('product.title', 'productTitle', 'like');
        $queryBuilder->addParameter('productTitle', '%' . $string . '%');
        $whereItem->setLogic(' or ');
        $item = new QueryWhereItem('product.sku', 'productSku', 'like');
        $queryBuilder->addParameter('productSku', '%' . $string . '%');
        $whereItem->addWhere($item);
        $whereItem->setLogic(' or ');
        $item = new QueryWhereItem('product.shortDescription', 'productShortDescription', 'like');
        $queryBuilder->addParameter('productShortDescription', '%' . $string . '%');
        $whereItem->addWhere($item);
        $item = new QueryWhereItem('product.description', 'productDescription', 'like');
        $queryBuilder->addParameter('productDescription', '%' . $string . '%');

        $item = new QueryWhereItem('product.searchKeywords', 'productSearchKeywords', 'like');
        $queryBuilder->addParameter('productSearchKeywords', '%' . $string . '%');

        $whereItem->addWhere($item);

        $whereItem = $queryBuilder->addWhere('product.status', 'productStatus');
        $queryBuilder->addParameter('productStatus', 1);

        $queryBuilder->addOrder('product.weight', 'productWeight', 'desc');
        $queryBuilder->addOrder('product.id', 'productId', 'desc');

        $queryBuilder->addGroup('product.id', 'productId');

        $results = $queryBuilder->executeQuery(($page - 1) * $perPage, $perPage);


        return $results;
    }

}
