<?php
namespace Search\Form\SearchForm;

use Common\Form\Form;

class SearchForm extends Form

{

    public function init()
    {
        $this->setAttribute('method', 'get');
        
        $this->add(array(
            'name' => 'query',
            'attributes' => array(
                'type' => 'text',
                'placeholder' => 'Search..'
            )
        ), array(
            'priority' => 1000
        ));
        
        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search'
            )
        ), array(
            'priority' => - 100
        ));
    }
}