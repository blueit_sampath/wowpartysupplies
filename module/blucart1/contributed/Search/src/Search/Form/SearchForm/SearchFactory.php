<?php
namespace Search\Form\SearchForm;

use Common\Form\Option\AbstractFormFactory;

class SearchFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Search\Form\SearchForm\SearchForm' );
		$form->setName ( 'searchForm' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Search\Form\SearchForm\SearchFilter' );
	}
}
