<?php

namespace Search\Service\Navigation;

use Common\Navigation\Event\NavigationEvent;

class SearchNavigation extends NavigationEvent {

    public function breadcrumb() {
        parent::breadcrumb();
        $page = array(
            'label' => 'Search Results',
            'route' => 'search',
            'id' => 'search'
        );

        $this->addDefaultMenu($page);
    }

}
