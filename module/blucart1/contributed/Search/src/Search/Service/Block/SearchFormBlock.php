<?php 
namespace Search\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class SearchFormBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'search-form-block';

	public function getBlockName() {
		return 'searchFormBlock';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'Search Form';
	}
	
}

