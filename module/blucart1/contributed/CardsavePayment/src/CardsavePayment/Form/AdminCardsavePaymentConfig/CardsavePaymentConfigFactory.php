<?php
namespace CardsavePayment\Form\AdminCardsavePaymentConfig;

use Common\Form\Option\AbstractFormFactory;

class CardsavePaymentConfigFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigForm');
        $form->setName('adminCardsavePaymentConfig');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigFilter');
    }
}
