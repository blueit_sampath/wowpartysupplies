<?php
namespace CardsavePayment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminCardsavePaymentController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-cardsavepayment-setting');
    }
}