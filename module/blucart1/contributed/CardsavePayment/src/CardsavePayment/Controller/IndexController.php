<?php
namespace CardsavePayment\Controller;

use Core\Functions;
use Payment\Option\PaymentController;
use Zend\View\Model\ViewModel;
use Config\Api\ConfigApi;
use CardsavePayment\Option\PaymentFormHelper;

class IndexController extends PaymentController {

    const GATEWAY_NAME = 'cardsavePayment';

    public function sendAction() {
        if (!$this->validatePath()) {
            return $this->redirect()->toRoute('checkout');
        }
        $orderId = Functions::fromRoute('orderId');
        $viewModel = new ViewModel(array(
            'orderId' => $orderId
        ));
        return $viewModel;
    }

    public function landAction() {
        $cart = $this->getCart();
        $MerchantID = ConfigApi::getConfigByKey('PAYMENT_CARDSAVE_MERCHANTID');
        $Password = ConfigApi::getConfigByKey('PAYMENT_CARDSAVE_PASSWORD');
        $PreSharedKey = ConfigApi::getConfigByKey('PAYMENT_CARDSAVE_PRESHAREDKEY');
        $HashMethod = ConfigApi::getConfigByKey('PAYMENT_CARDSAVE_HASHMETHOD');
        $PaymentProcessorDomain = ConfigApi::getConfigByKey('PAYMENT_CARDSAVE_PAYMENTPROCESSORDOMAIN');
        $ResultDeliveryMethod = ConfigApi::getConfigByKey('PAYMENT_CARDSAVE_RESULTDELIVERYMETHOD');

        $DuplicateTransaction = false;
        $PreviousTransactionMessage = "";

        switch ($ResultDeliveryMethod) {
            case "POST":
                // the results will be delivered via POST variables to this
                // page
                $boResultValidationSuccessful = PaymentFormHelper::validateTransactionResult_POST($MerchantID, $Password, $PreSharedKey, $HashMethod, $_POST, $trTransactionResult, $szValidateErrorMessage);
                // the results need to be stored here as this is the first time
                // they will have touched this system
                if ($boResultValidationSuccessful) {
                    if (!PaymentFormHelper::reportTransactionResults($trTransactionResult, $szOutputMessage)) {
                        // handle the case where the results aren't stored correctly
                        $orderId = $trTransactionResult->getOrderID();
                        $transactionId = $trTransactionResult->getCrossReference();
                        $viewModel = $this->error($orderId, $transactionId, $Message);
                        return $viewModel;
                    }
                }
                break;
            case "SERVER":
                // the results have already been delivered via a server-to-server
                // call from the payment form to the ServerResultURL
                // need to query these transaction results to display
                $boResultValidationSuccessful = PaymentFormHelper::validateTransactionResult_SERVER($MerchantID, $Password, $PreSharedKey, $HashMethod, $_GET, $trTransactionResult, $szValidateErrorMessage);
                break;
            case "SERVER_PULL":
                // need to query the results from the payment form using the passed
                // cross reference
                $szPaymentFormResultHandler = $PaymentProcessorDomain;

                $boResultValidationSuccessful = PaymentFormHelper::validateTransactionResult_SERVER_PULL($MerchantID, $Password, $PreSharedKey, $HashMethod, $_GET, $szPaymentFormResultHandler, $trTransactionResult, $szValidateErrorMessage);
                // the results need to be stored here as this is the first time
                // they will have touched this system
                if ($boResultValidationSuccessful) {
                    if (!PaymentFormHelper::reportTransactionResults($trTransactionResult, $szOutputMessage)) {
                        // handle the case where the results aren't stored correctly
                        $orderId = $trTransactionResult->getOrderID();
                        $transactionId = $trTransactionResult->getCrossReference();
                        $viewModel = $this->error($orderId, $transactionId, $Message);
                        return $viewModel;
                    }
                }
                break;
        }

        // display an error message if the transaction result couldn't be validated
        if (!$boResultValidationSuccessful) {
            $MessageClass = "ErrorMessage";
            $Message = $szValidateErrorMessage;
        } else {
            switch ($trTransactionResult->getStatusCode()) {
                case 0:
                    $MessageClass = "SuccessMessage";
                    // return $this->processPayment();
                    $orderId = $trTransactionResult->getOrderID();
                    $transactionId = $trTransactionResult->getCrossReference();
                    $viewModel = $this->complete($orderId, $transactionId);
                   $cart->clearCart();
                    return $viewModel;
                    break;
                case 4:
                    $MessageClass = "ErrorMessage";
                    break;
                case 5:
                    $MessageClass = "ErrorMessage";
                    break;
                case 20:
                    $DuplicateTransaction = true;
                    if ($trTransactionResult->getPreviousStatusCode() == 0) {
                        $MessageClass = "SuccessMessage";
                    } else {
                        $MessageClass = "ErrorMessage";
                    }
                    $PreviousTransactionMessage = $trTransactionResult->getPreviousMessage();
                    break;
                case 30:
                    $MessageClass = "ErrorMessage";
                    break;
                default:
                    $MessageClass = "ErrorMessage";
                    break;
            }


            $Message = $trTransactionResult->getMessage();
        }
        if ($Message) {
            $orderId = $trTransactionResult->getOrderID();
           
            $transactionId = $trTransactionResult->getCrossReference();
            $viewModel = $this->error($orderId, $transactionId, $Message);
            return $viewModel;
        }
        if ($PreviousTransactionMessage) {
            $orderId = $trTransactionResult->getOrderID();
            $transactionId = $trTransactionResult->getCrossReference();
            $viewModel = $this->error($orderId, $transactionId, $PreviousTransactionMessage);
            return $viewModel;
        }
        return $this->redirect()->toRoute('home');
    }

}
