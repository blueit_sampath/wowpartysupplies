<?php
namespace CardsavePayment\Service\Payment;

use Core\Functions;
use Checkout\Option\CartContainer;
use Payment\Option\AbstractPaymentEvent;
use Config\Api\ConfigApi;

class CardsavePaymentEvent extends AbstractPaymentEvent
{

    public function getName()
    {
        return 'cardsavePayment';
    }

    public function getTitle()
    {
        $title = ConfigApi::getConfigByKey('PAYMENT_CARDSAVE_TITLE');
        if (! $title) {
            $title = 'Card Save';
        }
        return $title;
    }

    public function getRouteName()
    {
        return 'cardsavepayment-send';
    }
}
