<?php
namespace CardsavePayment\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminPaymentSetting extends AbstractTabEvent
{

    public function getEventName()
    {
        return 'adminPaymentSetting';
    }

    public function tab()
    {
        $tabContainer = $this->getTabContainer();
        $url = Functions::getUrlPlugin();
        
        $u = $url('admin-cardsavepayment-setting');
        $tabContainer->add('admin-cardsavepayment-setting', 'Cardsave Payment', $u, 500);
        
        return $this;
    }
}

