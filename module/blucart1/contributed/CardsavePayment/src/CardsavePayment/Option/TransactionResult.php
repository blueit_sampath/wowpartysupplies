<?php
namespace CardsavePayment\Option;

class TransactionResult
{
	private $m_nStatusCode;
	private $m_szMessage;
	private $m_nPreviousStatusCode;
	private $m_szPreviousMessage;
	private $m_szCrossReference;
	private $m_nAmount;
	private $m_nCurrencyCode;
	private $m_szOrderID;
	private $m_szTransactionType;
	private $m_szTransactionDateTime;
	private $m_szOrderDescription;
	private $m_szCustomerName;
	private $m_szAddress1;
	private $m_szAddress2;
	private $m_szAddress3;
	private $m_szAddress4;
	private $m_szCity;
	private $m_szState;
	private $m_szPostCode;
	private $m_nCountryCode;

	public function getStatusCode()
	{
		return $this->m_nStatusCode;
	}
	public function setStatusCode($nStatusCode)
	{
		$this->m_nStatusCode = $nStatusCode;
	}
	public function getMessage()
	{
		return $this->m_szMessage;
	}
	public function setMessage($szMessage)
	{
		$this->m_szMessage = $szMessage;
	}
	public function getPreviousStatusCode()
	{
		return $this->m_nPreviousStatusCode;
	}
	public function setPreviousStatusCode($nPreviousStatusCode)
	{
		$this->m_nPreviousStatusCode = $nPreviousStatusCode;
	}
	public function getPreviousMessage()
	{
		return $this->m_szPreviousMessage;
	}
	public function setPreviousMessage($szPreviousMessage)
	{
		$this->m_szPreviousMessage = $szPreviousMessage;
	}
	public function getCrossReference()
	{
		return $this->m_szCrossReference;
	}
	public function setCrossReference($szCrossReference)
	{
		$this->m_szCrossReference = $szCrossReference;
	}
	public function getAmount()
	{
		return $this->m_nAmount;
	}
	public function setAmount($nAmount)
	{
		$this->m_nAmount = $nAmount;
	}
	public function getCurrencyCode()
	{
		return $this->m_nCurrencyCode;
	}
	public function setCurrencyCode($nCurrencyCode)
	{
		$this->m_nCurrencyCode = $nCurrencyCode;
	}
	public function getOrderID()
	{
		return $this->m_szOrderID;
	}
	public function setOrderID($szOrderID)
	{
		$this->m_szOrderID = $szOrderID;
	}
	public function getTransactionType()
	{
		return $this->m_szTransactionType;
	}
	public function setTransactionType($szTransactionType)
	{
		$this->m_szTransactionType = $szTransactionType;
	}
	public function getTransactionDateTime()
	{
		return $this->m_szTransactionDateTime;
	}
	public function setTransactionDateTime($szTransactionDateTime)
	{
		$this->m_szTransactionDateTime = $szTransactionDateTime;
	}
	public function getOrderDescription()
	{
		return $this->m_szOrderDescription;
	}
	public function setOrderDescription($szOrderDescription)
	{
		$this->m_szOrderDescription = $szOrderDescription;
	}
	public function getCustomerName()
	{
		return $this->m_szCustomerName;
	}
	public function setCustomerName($szCustomerName)
	{
		$this->m_szCustomerName = $szCustomerName;
	}
	public function getAddress1()
	{
		return $this->m_szAddress1;
	}
	public function setAddress1($szAddress1)
	{
		$this->m_szAddress1 = $szAddress1;
	}
	public function getAddress2()
	{
		return $this->m_szAddress2;
	}
	public function setAddress2($szAddress2)
	{
		$this->m_szAddress2 = $szAddress2;
	}
	public function getAddress3()
	{
		return $this->m_szAddress3;
	}
	public function setAddress3($szAddress3)
	{
		$this->m_szAddress3 = $szAddress3;
	}
	public function getAddress4()
	{
		return $this->m_szAddress4;
	}
	public function setAddress4($szAddress4)
	{
		$this->m_szAddress4 = $szAddress4;
	}
	public function getCity()
	{
		return $this->m_szCity;
	}
	public function setCity($szCity)
	{
		$this->m_szCity = $szCity;
	}
	public function getState()
	{
		return $this->m_szState;
	}
	public function setState($szState)
	{
		$this->m_szState = $szState;
	}
	public function getPostCode()
	{
		return $this->m_szPostCode;
	}
	public function setPostCode($szPostCode)
	{
		$this->m_szPostCode = $szPostCode;
	}
	public function getCountryCode()
	{
		return $this->m_nCountryCode;
	}
	public function setCountryCode($nCountryCode)
	{
		$this->m_nCountryCode = $nCountryCode;
	}
}
