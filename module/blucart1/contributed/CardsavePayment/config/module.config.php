<?php
return array(
    'router' => array(
        'routes' => array(
            'cardsavepayment-send' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cardsavepayment/send[/:orderId]',
                    'defaults' => array(
                        'controller' => 'CardsavePayment\Controller\Index',
                        'action' => 'send'
                    )
                )
            ),
            'cardsavepayment-land' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cardsavepayment/land',
                    'defaults' => array(
                        'controller' => 'CardsavePayment\Controller\Index',
                        'action' => 'land'
                    )
                )
            ),
            'cardsavepayment-error' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cardsavepayment/error',
                    'defaults' => array(
                        'controller' => 'CardsavePayment\Controller\Index',
                        'action' => 'error'
                    )
                )
            ),
            'admin-cardsavepayment-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/cardsavepayment/setting[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'CardsavePayment\Controller\AdminCardsavePayment',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            )
        )
    ),
    'events' => array(
        'CardsavePayment\Service\Payment\CardsavePaymentEvent' => 'CardsavePayment\Service\Payment\CardsavePaymentEvent',
        'CardsavePayment\Service\Form\AdminCardsavePaymentSetting' => 'CardsavePayment\Service\Form\AdminCardsavePaymentSetting',
        'CardsavePayment\Service\Tab\AdminPaymentSetting' => 'CardsavePayment\Service\Tab\AdminPaymentSetting'
    ),
    'service_manager' => array(
        'invokables' => array(
            'CardsavePayment\Service\Payment\CardsavePaymentEvent' => 'CardsavePayment\Service\Payment\CardsavePaymentEvent',
            'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigForm' => 'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigForm',
            'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigFilter' => 'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigFilter',
            'CardsavePayment\Service\Form\AdminCardsavePaymentSetting' => 'CardsavePayment\Service\Form\AdminCardsavePaymentSetting',
            'CardsavePayment\Service\Tab\AdminPaymentSetting' => 'CardsavePayment\Service\Tab\AdminPaymentSetting'
        ),
        'factories' => array(
            'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigFactory' => 'CardsavePayment\Form\AdminCardsavePaymentConfig\CardsavePaymentConfigFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'CardsavePayment\Controller\Index' => 'CardsavePayment\Controller\IndexController',
            'CardsavePayment\Controller\AdminCardsavePayment' => 'CardsavePayment\Controller\AdminCardsavePaymentController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'CardsavePayment' => __DIR__ . '/../view'
        )
    ),
    
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'CardsavePayment' => __DIR__ . '/../public'
            )
        )
    )
);
