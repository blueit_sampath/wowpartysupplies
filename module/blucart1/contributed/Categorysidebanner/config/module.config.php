<?php

return array(
    'router' => array(
        'routes' => array(
            'admin-categorysidebanner' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/categorysidebanner[/ajax/:ajax][/category/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Categorysidebanner\Controller\AdminCategorysidebanner',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-categorysidebanner-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/categorysidebanner/add[/ajax/:ajax][/category/:categoryId][/:categorysidebannerId]',
                    'defaults' => array(
                        'controller' => 'Categorysidebanner\Controller\AdminCategorysidebanner',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categorysidebannerId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-categorysidebanner-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/categorysidebanner/sort[/ajax/:ajax][/category/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Categorysidebanner\Controller\AdminCategorysidebanner',
                        'action' => 'sort',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Categorysidebanner\Service\Grid\AdminCategorysidebanner' => 'Categorysidebanner\Service\Grid\AdminCategorysidebanner',
        'Categorysidebanner\Service\Form\AdminCategorysidebanner' => 'Categorysidebanner\Service\Form\AdminCategorysidebanner',
        'Categorysidebanner\Service\Tab\AdminCategorysidebanner' => 'Categorysidebanner\Service\Tab\AdminCategorysidebanner',
        'Categorysidebanner\Service\Link\AdminCategorysidebanner' => 'Categorysidebanner\Service\Link\AdminCategorysidebanner',
        'Categorysidebanner\Service\NestedSorting\AdminCategorysidebanner' => 'Categorysidebanner\Service\NestedSorting\AdminCategorysidebanner',
        'Categorysidebanner\Service\Block\CategorysidebannerBlock' => 'Categorysidebanner\Service\Block\CategorysidebannerBlock',
        'Categorysidebanner\Service\Tab\AdminCategorysidebannerConfig' => 'Categorysidebanner\Service\Tab\AdminCategorysidebannerConfig',
    ),
    'service_manager' => array(
        'invokables' => array(
            'Categorysidebanner\Service\Grid\AdminCategorysidebanner' => 'Categorysidebanner\Service\Grid\AdminCategorysidebanner',
            'Categorysidebanner\Service\Form\AdminCategorysidebanner' => 'Categorysidebanner\Service\Form\AdminCategorysidebanner',
            'Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerForm' => 'Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerForm',
            'Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerFilter' => 'Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerFilter',
            'Categorysidebanner\Service\Tab\AdminCategorysidebanner' => 'Categorysidebanner\Service\Tab\AdminCategorysidebanner',
            'Categorysidebanner\Service\Link\AdminCategorysidebanner' => 'Categorysidebanner\Service\Link\AdminCategorysidebanner',
            'Categorysidebanner\Service\NestedSorting\AdminCategorysidebanner' => 'Categorysidebanner\Service\NestedSorting\AdminCategorysidebanner',
            'Categorysidebanner\Service\Block\CategorysidebannerBlock' => 'Categorysidebanner\Service\Block\CategorysidebannerBlock',
            'Categorysidebanner\Service\Tab\AdminCategorysidebannerConfig' => 'Categorysidebanner\Service\Tab\AdminCategorysidebannerConfig',
        ),
        'factories' => array(
            'Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerFormFactory' => 'Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerFormFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Categorysidebanner\Controller\AdminCategorysidebanner' => 'Categorysidebanner\Controller\AdminCategorysidebannerController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Categorysidebanner' => __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'Categorysidebanner_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Categorysidebanner/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Categorysidebanner\Entity' => 'Categorysidebanner_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Categorysidebanner' => __DIR__ . '/../public'
            )
        )
    )
);
