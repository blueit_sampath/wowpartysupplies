<?php
namespace Categorysidebanner\Form\AdminCategorysidebanner;

use Common\Form\Option\AbstractFormFactory;

class CategorysidebannerFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerForm' );
		$form->setName ( 'adminCategorysidebannerAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerFilter' );
	}
}
