<?php

namespace Categorysidebanner\Controller;

use Common\MVC\Controller\AbstractAdminController;
use Core\Functions;
use Zend\View\Model\ViewModel;

class AdminCategorysidebannerController extends AbstractAdminController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('Categorysidebanner\Form\AdminCategorysidebanner\CategorysidebannerFormFactory');
    }

    public function getSaveRedirector() {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-categorysidebanner-add', array(
                    'categoryId' => $form->get('category')->getValue(),
                    'categorysidebannerId' => $form->get('id')->getValue()
                ));
    }

}
