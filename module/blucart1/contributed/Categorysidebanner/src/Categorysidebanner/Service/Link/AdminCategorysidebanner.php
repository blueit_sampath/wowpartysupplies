<?php

namespace Categorysidebanner\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategorysidebanner extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategorysidebanner';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		
		$url = Functions::getUrlPlugin ();
		$categoryId = Functions::fromRoute('categoryId',0);
		$u = $url ( 'admin-categorysidebanner-add',array('categoryId' => $categoryId) );
		$item = $linkContainer->add ( 'admin-categorysidebanner-add', 'Add Side Banner', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
//		$u = $url ( 'admin-categorysidebanner-sort',array('categoryId' => $categoryId) );
//		$item = $linkContainer->add ( 'admin-categorysidebanner-sort', 'Arrange Banners', $u, 990 );
//		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

