<?php

namespace Categorysidebanner\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategorysidebannerConfig extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryConfigAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-categorysidebanner');
		
                $tabContainer->add ( 'admin-categorysidebanner', 'Side Banners', $u, 900 );
		
		
		return $this;
	}
}

