<?php
namespace SeoCategory\Form\AdminSeo;

use Common\Form\Option\AbstractFormFactory;

class SeoFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'SeoCategory\Form\AdminSeo\SeoForm' );
		$form->setName ( 'adminSeoCategoryAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'SeoCategory\Form\AdminSeo\SeoFilter' );
	}
}
