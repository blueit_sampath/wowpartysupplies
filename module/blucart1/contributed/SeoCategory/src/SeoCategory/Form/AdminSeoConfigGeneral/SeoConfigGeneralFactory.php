<?php
namespace SeoCategory\Form\AdminSeoConfigGeneral;

use Common\Form\Option\AbstractFormFactory;

class SeoConfigGeneralFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm' );
		$form->setName ( 'adminSeoCategoryConfigGeneral' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter' );
	}
}
