<?php

namespace SeoCategory\Form\AdminSeoConfigGeneral;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class SeoConfigGeneralForm extends Form {

    public function init() {
        $this->add(array(
            'name' => 'SEO_CATEGORY_URL',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Seo Page Url'
            )
                ), array(
            'priority' => 1000
        ));
        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_URL',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Seo Page Url'
            )
                ), array(
            'priority' => 1000
        ));
        $this->add(array(
            'name' => 'SEO_CATEGORY_TITLE',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Seo Default Title'
            )
                ), array(
            'priority' => 1000
        ));
        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_TITLE',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Seo Default Pagination Title'
            )
                ), array(
            'priority' => 1000
        ));


        $this->add(array(
            'name' => 'SEO_CATEGORY_KEYWORDS',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Seo Default Keywords'
            )
                ), array(
            'priority' => 990
        ));

        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_KEYWORDS',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Seo Default Pagination Keywords'
            )
                ), array(
            'priority' => 990
        ));

        $this->add(array(
            'name' => 'SEO_CATEGORY_DESCRIPTION',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Seo Default Description'
            )
                ), array(
            'priority' => 980
        ));


        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_DESCRIPTION',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Seo Default Pagination Description'
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'name' => 'SEO_CATEGORY_ROBOTS',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Seo Default Robots'
            )
                ), array(
            'priority' => 970
        ));

        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_ROBOTS',
            'attributes' => array(
                'type' => 'textarea',
            ),
            'options' => array(
                'label' => 'Seo Default Pagination Robots'
            )
                ), array(
            'priority' => 970
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'forceBuild',
            'options' => array(
                'label' => 'Force Build',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 960
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save & Build'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
