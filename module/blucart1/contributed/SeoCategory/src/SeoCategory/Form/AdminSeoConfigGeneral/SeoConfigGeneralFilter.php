<?php

namespace SeoCategory\Form\AdminSeoConfigGeneral;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SeoConfigGeneralFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'SEO_CATEGORY_URL',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_URL',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_CATEGORY_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );

        $this->add(array(
            'name' => 'SEO_CATEGORY_DESCRIPTION',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_DESCRIPTION',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_CATEGORY_KEYWORDS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_KEYWORDS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_CATEGORY_ROBOTS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_CATEGORY_PAGINATION_ROBOTS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'forceBuild',
            'required' => false,
        ));
    }

}
