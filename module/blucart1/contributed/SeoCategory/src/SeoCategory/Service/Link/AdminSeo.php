<?php

namespace SeoCategory\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminSeo extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminSeo';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-seo-config-category' );
		$item = $linkContainer->add ( 'admin-seo-config-category', 'Build Category Urls', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

