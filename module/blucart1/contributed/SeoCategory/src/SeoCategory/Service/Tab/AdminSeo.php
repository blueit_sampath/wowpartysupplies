<?php

namespace SeoCategory\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminSeo extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$categoryId = Functions::fromRoute ( 'categoryId' );
		if (! $categoryId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-seo-category-add', array (
				'categoryId' => $categoryId 
		) );
		$tabContainer->add ( 'admin-seo-category-add', 'SEO', $u, 100 );
		
		return $this;
	}
}

