<?php

namespace SeoCategory\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;

class AdminSeo extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'alias',
        'pageTitle',
        'metaKeywords',
        'metaDescription',
        'robots',
        'aliasPagination',
        'pageTitlePagination',
        'metaKeywordsPagination',
        'metaDescriptionPagination',
        'robotsPagination'
    );
    protected $_entity = '\Seo\Entity\Seo';
    protected $_entityName = 'seo';

    public function getFormName() {
        return 'adminSeoCategoryAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function save() {
        $em = $this->getEntityManager();
        $categoryId = \Core\Functions::fromRoute('categoryId', 0);
        $form = $this->getForm();
        $params = $form->getData();
        if (!$categoryId) {
            return;
        }
        $categoryUrl = \SeoCategory\Api\SeoCategoryApi::getCategoryUrl($categoryId);
        $entity = \Seo\Api\SeoApi::getByUrl($categoryUrl);
        if (!$entity) {
            $entity = new \Seo\Entity\Seo();
        }
        $entity->url = $categoryUrl;
        $entity->alias = \Seo\Api\SeoApi::createSlug($params['alias']);
        $entity->pageTitle = $params['pageTitle'];
        $entity->metaKeywords = $params['metaKeywords'];
        $entity->metaDescription = $params['metaDescription'];
        $entity->robots = $params['robots'];
        $em->persist($entity);
        $em->flush();
        $resultContainer = $this->getFormResultContainer();
        $resultContainer->add($this->_entityName, $entity);

        //pagination category

        $categoryUrl = \SeoCategory\Api\SeoCategoryApi::getCategoryUrl($categoryId) . '/*';
        $entity = \Seo\Api\SeoApi::getByUrl($categoryUrl);
        if (!$entity) {
            $entity = new \Seo\Entity\Seo();
        }
        $entity->url = $categoryUrl;
        $entity->alias = \Seo\Api\SeoApi::createSlug($params['aliasPagination']);
        $entity->pageTitle = $params['pageTitlePagination'];
        $entity->metaKeywords = $params['metaKeywordsPagination'];
        $entity->metaDescription = $params['metaDescriptionPagination'];
        $entity->robots = $params['robotsPagination'];
        $em->persist($entity);
        $em->flush();
        $resultContainer = $this->getFormResultContainer();
        $resultContainer->add($this->_entityName . 'Pagination', $entity);

        return $entity;
    }

    public function getRecord() {
        $categoryId = \Core\Functions::fromRoute('categoryId', 0);
        if (!$categoryId) {
            return;
        }
        $form = $this->getForm();
        $categoryUrl = \SeoCategory\Api\SeoCategoryApi::getCategoryUrl($categoryId);
        $entity = \Seo\Api\SeoApi::getByUrl($categoryUrl);
        if (!$entity) {
            return;
        }
        $form->get('alias')->setValue($entity->alias);
        $form->get('pageTitle')->setValue($entity->pageTitle);
        $form->get('metaKeywords')->setValue($entity->metaKeywords);
        $form->get('metaDescription')->setValue($entity->metaDescription);
        $form->get('robots')->setValue($entity->robots);

        $categoryUrl = \SeoCategory\Api\SeoCategoryApi::getCategoryUrl($categoryId).'/*';
        $entity = \Seo\Api\SeoApi::getByUrl($categoryUrl);
        if (!$entity) {
            return;
        }
        $form->get('aliasPagination')->setValue($entity->alias);
        $form->get('pageTitlePagination')->setValue($entity->pageTitle);
        $form->get('metaKeywordsPagination')->setValue($entity->metaKeywords);
        $form->get('metaDescriptionPagination')->setValue($entity->metaDescription);
        $form->get('robotsPagination')->setValue($entity->robots);
    }

}
