<?php

namespace SeoCategory\Service\Form;

use Config\Service\Form\AdminConfig;

class AdminSeoConfigGeneral extends AdminConfig {

    protected $_columnKeys = array(
        'SEO_CATEGORY_TITLE',
        'SEO_CATEGORY_KEYWORDS',
        'SEO_CATEGORY_DESCRIPTION',
        'SEO_CATEGORY_ROBOTS',
        'SEO_CATEGORY_URL',
        'SEO_CATEGORY_PAGINATION_TITLE',
        'SEO_CATEGORY_PAGINATION_KEYWORDS',
        'SEO_CATEGORY_PAGINATION_DESCRIPTION',
        'SEO_CATEGORY_PAGINATION_ROBOTS',
        'SEO_CATEGORY_PAGINATION_URL'
    );

    public function getFormName() {
        return 'adminSeoCategoryConfigGeneral';
    }

    public function getPriority() {
        return 1000;
    }

    public function save() {
        parent::save();
        $form = $this->getForm();
        $params = $form->getData();

        $forceBuild = false;
        if (isset($params['forceBuild'])) {
            $forceBuild = true;
        }
        \SeoCategory\Api\SeoCategoryApi::buildUrls($forceBuild);
    }

}
