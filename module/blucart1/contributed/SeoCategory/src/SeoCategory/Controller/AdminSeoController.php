<?php

namespace SeoCategory\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminSeoController extends AbstractAdminController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('SeoCategory\Form\AdminSeo\SeoFormFactory');
    }

    public function getSaveRedirector() {
        return $this->redirect()->toRoute('admin-seo-category-add', array(
                    'categoryId' => \Core\Functions::fromRoute('categoryId')
        ));
    }

}
