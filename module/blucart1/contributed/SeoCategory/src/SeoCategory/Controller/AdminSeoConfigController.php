<?php

namespace SeoCategory\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminSeoConfigController extends AbstractAdminController {

   	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-seo-config-category' );
	}
}
