<?php

namespace SeoCategory\Api;

use Core\Functions;
use Common\Api\Api;
use \Config\Api\ConfigApi;

class SeoCategoryApi extends Api {

    protected static $_entity = '\Seo\Entity\Seo';

    public static function buildUrls($forceBuild) {

        $em = Functions::getEntityManager();
        if ($forceBuild) {
            $dql = 'delete from ' . static::$_entity . ' u where u.url like \'/category/%\'';
            $query = $em->createQuery($dql);
            $query->getResult();
        }
        $categories = \Category\Api\CategoryApi::getAllCategories(null);
        foreach ($categories as $categoryResult) {
            $category = \Category\Api\CategoryApi::getCategoryById($categoryResult['categoryId']);
            $params = array('category' => $category);
            $url = static::getCategoryUrl($category->id);
            if (\Seo\Api\SeoApi::getByUrl($url)) {
                continue;
            }

            $pageTitle = '';

            if ($value = ConfigApi::getConfigByKey('SEO_CATEGORY_TITLE')) {
                $pageTitle = $value;
            }
            $pageTitle = static::substitute($pageTitle, $params);
            $array['pageTitle'] = $pageTitle;

            $keywords = '';

            if ($value = ConfigApi::getConfigByKey('SEO_CATEGORY_KEYWORDS')) {
                $keywords = $value;
            }

            $keywords = static::substitute($keywords, $params);
            $array['metaKeywords'] = $keywords;


            if ($value = ConfigApi::getConfigByKey('SEO_CATEGORY_DESCRIPTION')) {
                $description = $value;
            }
            $description = static::substitute($description, $params);
            $array['metaDescription'] = $description;

            $robots = '';

            if ($value = \Config\Api\ConfigApi::getConfigByKey('SEO_CATEGORY_ROBOTS')) {
                $robots = $value;
            }



            $robots = static::substitute($robots, $params);
            $array['robots'] = $robots;
            $alias = $category->title;

            if ($value = ConfigApi::getConfigByKey('SEO_CATEGORY_URL')) {
                $alias = $value;
            }

            $array['alias'] = static::substitute($alias, $params);
            $array['url'] = $url;
            \Seo\Api\SeoApi::insertSeoWithAlias($url, $array);
            // CATEGORY WITH URL /*
             $url = static::getCategoryUrl($category->id).'/*';
            if (\Seo\Api\SeoApi::getByUrl($url)) {
                continue;
            }

            $pageTitle = '';

            if ($value = ConfigApi::getConfigByKey('SEO_CATEGORY_PAGINATION_TITLE')) {
                $pageTitle = $value;
            }
            $pageTitle = static::substitute($pageTitle, $params);
            $array['pageTitle'] = $pageTitle;

            $keywords = '';

            if ($value = ConfigApi::getConfigByKey('SEO_CATEGORY_PAGINATION_KEYWORDS')) {
                $keywords = $value;
            }

            $keywords = static::substitute($keywords, $params);
            $array['metaKeywords'] = $keywords;


            if ($value = ConfigApi::getConfigByKey('SEO_CATEGORY_PAGINATION_DESCRIPTION')) {
                $description = $value;
            }
            $description = static::substitute($description, $params);
            $array['metaDescription'] = $description;

            $robots = '';

            if ($value = \Config\Api\ConfigApi::getConfigByKey('SEO_CATEGORY_PAGINATION_ROBOTS')) {
                $robots = $value;
            }



            $robots = static::substitute($robots, $params);
            $array['robots'] = $robots;
            $alias = $category->title.'/*';

            if ($value = ConfigApi::getConfigByKey('SEO_CATEGORY_PAGINATION_URL')) {
                $alias = $value;
            }

            $array['alias'] = static::substitute($alias, $params);
            $array['url'] = $url;
            \Seo\Api\SeoApi::insertSeoWithAlias($url, $array);
        }
    }

    public static function getCategoryUrl($categoryId){
        return '/category/'.$categoryId;
    }
    public static function substitute($string, $params) {
        $twigPlugin = Functions::getViewHelperManager()->get('twig');
        $string = $twigPlugin($string, $params);
        $string = str_replace('\{', '{', $string);
        $string = str_replace('\}', '}', $string);
        return $string;
    }

}
