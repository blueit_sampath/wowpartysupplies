<?php

namespace SeoCategory;

return array(
    'router' => array(
        'routes' => array(
            'admin-seo-config-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo/config/category[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'SeoCategory\Controller\AdminSeoConfig',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            ),
            'admin-seo-category-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo/category/add[/ajax/:ajax][/:categoryId]',
                    'defaults' => array(
                        'controller' => 'SeoCategory\Controller\AdminSeo',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
        )
    ),
    'events' => array(
        'SeoCategory\Service\Form\AdminSeoConfigGeneral' => 'SeoCategory\Service\Form\AdminSeoConfigGeneral',
        'SeoCategory\Service\Link\AdminSeo' => 'SeoCategory\Service\Link\AdminSeo',
        'SeoCategory\Service\Tab\AdminSeo' => 'SeoCategory\Service\Tab\AdminSeo',
        'SeoCategory\Service\Form\AdminSeo' => 'SeoCategory\Service\Form\AdminSeo',
    ),
    'service_manager' => array(
        'invokables' => array(
            'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm' => 'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm',
            'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter' => 'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter',
            'SeoCategory\Service\Form\AdminSeoConfigGeneral' => 'SeoCategory\Service\Form\AdminSeoConfigGeneral',
            'SeoCategory\Service\Link\AdminSeo' => 'SeoCategory\Service\Link\AdminSeo',
            'SeoCategory\Service\Form\AdminSeo' => 'SeoCategory\Service\Form\AdminSeo',
            'SeoCategory\Service\Tab\AdminSeo' => 'SeoCategory\Service\Tab\AdminSeo',
            'SeoCategory\Form\AdminSeo\SeoFilter' => 'SeoCategory\Form\AdminSeo\SeoFilter',
            'SeoCategory\Form\AdminSeo\SeoForm' => 'SeoCategory\Form\AdminSeo\SeoForm',
          
        ),
        'factories' => array(
            'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory' => 'SeoCategory\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory',
             'SeoCategory\Form\AdminSeo\SeoFormFactory' => 'SeoCategory\Form\AdminSeo\SeoFormFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'SeoCategory\Controller\AdminSeoConfig' => 'SeoCategory\Controller\AdminSeoConfigController',
            'SeoCategory\Controller\AdminSeo' => 'SeoCategory\Controller\AdminSeoController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view'
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __NAMESPACE__ => __DIR__ . '/../public'
            )
        )
    )
);
