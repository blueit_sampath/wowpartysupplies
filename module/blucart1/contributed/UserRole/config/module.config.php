<?php

namespace UserRole;

return array (
		'router' => array (
				'routes' => array (
						'admin-user-role' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/user/role',
										'defaults' => array (
												'controller' => 'UserRole\Controller\AdminUserRole',
												'action' => 'index' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'UserRole\Service\Grid\AdminUserRole' => 'UserRole\Service\Grid\AdminUserRole',
				'UserRole\Service\Navigation\AdminUserRoleNavigation' => 'UserRole\Service\Navigation\AdminUserRoleNavigation' 
		),
		'service_manager' => array (
				'invokables' => array (
						'UserRole\Service\Grid\AdminUserRole' => 'UserRole\Service\Grid\AdminUserRole',
						'UserRole\Service\Navigation\AdminUserRoleNavigation' => 'UserRole\Service\Navigation\AdminUserRoleNavigation' 
				),
				'factories' => array () 
		),
		'controllers' => array (
				'invokables' => array (
						'UserRole\Controller\AdminUserRole' => 'UserRole\Controller\AdminUserRoleController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						__NAMESPACE__ => __DIR__ . '/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						__NAMESPACE__ . '_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								__NAMESPACE__ => __DIR__ . '/../public' 
						) 
				) 
		) 
);
