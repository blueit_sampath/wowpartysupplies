<?php 
namespace UserRole\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminUserRoleNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'customerMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Role',
							'route' => 'admin-user-role',
							'id' => 'admin-user-role',
							'iconClass' => 'glyphicon-hand-right' 
					) 
			) );
		}
	}
}

