<?php

namespace UserRole\Service\Grid;


use BlucartGrid\Option\ColumnItem;

use BlucartGrid\Event\AbstractMainBlucartGridEvent;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;

class AdminUserRole extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'accessAdmin',
			'displayAtRegistration' 
	);
	protected $_entity = '\UserRole\Entity\UserRole';
	protected $_entityName = 'userRole';
	public function getEventName() {
		return 'adminUserRole';
	}
	
	public function getPriority(){
		return 1000;
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'Id' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary(true);
		$columns->addColumn ( 'userRole.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'userRole.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'accessAdmin' );
		$columnItem->setTitle ( 'Admin Access' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'userRole.accessAdmin', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'displayAtRegistration' );
		$columnItem->setTitle ( 'Display At Registration' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'userRole.displayAtRegistration', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'create' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'create', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 700 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	
	
	
}
