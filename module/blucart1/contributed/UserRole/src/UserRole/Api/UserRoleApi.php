<?php

namespace UserRole\Api;

use Common\Api\Api;

class UserRoleApi extends Api {
	protected static $_entity = '\UserRole\Entity\UserRole';
	public static function getRoles() {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'userRole.id', 'userRoleId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'userRole' );
		return $queryBuilder->executeQuery ();
	}
	public static function getRoleById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	
	public static function getRolesAtRegistration() {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findBy ( array (
				'displayAtRegistration' => 1
		) );
	}
	
}
