<?php

namespace OrderMain\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminOrderStatusController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderStatus\OrderStatusFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-order-status-add', array (
				'orderStatusId' => $form->get ( 'id' )->getValue () 
		) );
	}
}