<?php

namespace OrderMain\Controller;

use Common\MVC\Controller\AbstractFrontController;
use Core\Functions;
use OrderMain\Api\OrderApi;
use Zend\View\Model\ViewModel;
use User\Api\UserApi;
use Pdf\Option\Pdf;

class UserOrderController extends AbstractFrontController {

    public function indexAction() {
        if (!UserApi::getLoggedInUser()) {
            return $this->accessDenied();
        }
        $viewModel = new ViewModel ();
        return $viewModel;
    }

    public function viewAction() {
        $orderId = Functions::fromRoute('orderId');
        $order = OrderApi::getOrderById($orderId);
        $user = UserApi::getLoggedInUser();
        if (!($user && $order && ($order->user->id == $user->id))) {
            return $this->accessDenied();
        }
        $viewModel = new ViewModel ();
        return $viewModel;
    }

    public function printAction() {
        $orderId = Functions::fromRoute('orderId');
        $order = OrderApi::getOrderById($orderId);
        $user = UserApi::getLoggedInUser();
        if (!($user && $order && ($order->user->id == $user->id))) {
            return $this->accessDenied();
        }
        $viewModel = new ViewModel ();
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function commentAction() {
        $form = $this->getServiceLocator()->get('OrderMain\Form\UserOrderComment\CommentFactory');

        $orderId = Functions::fromRoute('orderId');
        $order = OrderApi::getOrderById($orderId);
        $user = UserApi::getLoggedInUser();
        if (!($user && $order && ($order->user->id == $user->id))) {
            return $this->accessDenied();
        }
        $orderCommentId = Functions::fromRoute('orderCommentId', 0);
        $comment = OrderApi::getOrderCommentById($orderCommentId);
        if ($comment) {
            if ($comment->orderMain->id != $order->id) {
                return $this->accessDenied();
            }
        }

        if ($this->getRequest()->isPost()) {
            $form->setData($this->params()->fromPost());
            if ($form->isValid()) {
                $form->save();
                Functions::addSuccessMessage('Saved Successfully');
                return $this->redirect()->toRoute('account-order-comment', array(
                            'orderId' => $form->get('order')->getValue(),
                            'orderCommentId' => $form->get('id')->getValue()
                        ));
            }
        } else {

            $form->getRecord();
        }
        return array(
            'form' => $form
        );
    }

    public function commentdeleteAction() {
        $em = Functions::getEntityManager();
        $orderId = Functions::fromRoute('orderId', 0);
        $orderCommentId = Functions::fromRoute('orderCommentId', 0);

        $order = OrderApi::getOrderById($orderId);
        $user = UserApi::getLoggedInUser();
        if (!($user && $order && ($order->user->id == $user->id))) {
            return $this->accessDenied();
        }
        $comment = OrderApi::getOrderCommentById($orderCommentId);
        if ($comment) {
            if ($comment->orderMain->id == $order->id) {
                $em->remove($comment);
                $em->flush();
                Functions::addSuccessMessage('Deleted Comment Successfully');
            }
        }

        return $this->redirect()->toRoute('account-order-view', array(
                    'orderId' => Functions::fromRoute('orderId')
                ));
    }

    public function downloadAction() {
        $orderId = Functions::fromRoute('orderId');
        $order = OrderApi::getOrderById($orderId);
        $user = UserApi::getLoggedInUser();
        if (!($user && $order && ($order->user->id == $user->id))) {
            return $this->accessDenied();
        }

        $renderer = Functions::getServiceLocator()->get('viewrenderer');

        $string = $renderer->render('common/order-invoice', array(
            'orderId' => $orderId
        ));
        $pdf = new Pdf();
        $pdf->WriteHTML($string);
        $pdf->sendToBrowserAsDownload("order_" . $orderId . ".pdf");

        exit();
    }

}
