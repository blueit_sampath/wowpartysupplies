<?php

namespace OrderMain\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminOrderConfigAdminNotificationController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-order-config-admin-notification' );
	}
}