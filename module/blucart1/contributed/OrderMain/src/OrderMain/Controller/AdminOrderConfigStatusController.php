<?php

namespace OrderMain\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminOrderConfigStatusController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-order-config-status' );
	}
}