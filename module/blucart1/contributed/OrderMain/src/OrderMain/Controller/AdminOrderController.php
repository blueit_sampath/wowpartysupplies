<?php

namespace OrderMain\Controller;

use Config\Api\ConfigApi;
use Common\Option\Token\TokenContainer;
use Core\Functions;
use OrderMain\Api\OrderApi;
use Common\MVC\Controller\AbstractAdminController;

class AdminOrderController extends AbstractAdminController {
	
	public function commentAction() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderComment\CommentFactory' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				return $this->redirect ()->toRoute ( 'admin-order-comment', array (
						'orderId' => $form->get ( 'order' )->getValue (),
						'orderCommentId' => $form->get ( 'id' )->getValue () 
				) );
			}
		} else {
			
			$form->getRecord ();
		}
		return array (
				'form' => $form 
		);
	}
	public function commentdeleteAction() {
		$em = Functions::getEntityManager ();
		$orderId = Functions::fromRoute ( 'orderId', 0 );
		$orderCommentId = Functions::fromRoute ( 'orderCommentId', 0 );
		
		$comment = OrderApi::getOrderCommentById ( $orderCommentId );
		if ($comment) {
			$em->remove ( $comment );
			$em->flush ();
		}
		Functions::addSuccessMessage ( 'Deleted Comment Successfully' );
		return $this->redirect ()->toRoute ( 'admin-order-view', array (
				'orderId' => Functions::fromRoute ( 'orderId' ) 
		) );
	}
	public function sendinvoiceAction() {
		$orderId = Functions::fromRoute ( 'orderId', 0 );
		$order = OrderApi::getOrderById ( $orderId );
		$tokenContainer = $this->parseInvoiceEmail ();
		
		$this->getRequest ()->getQuery ()->set ( 'subject', $tokenContainer->getSubject () );
		$this->getRequest ()->getQuery ()->set ( 'htmlMessage', $tokenContainer->getBodyHtml () );
		$this->getRequest ()->getQuery ()->set ( 'plainMessage', $tokenContainer->getBodyText () );
		$this->getRequest ()->getQuery ()->set ( 'fromEmail', ConfigApi::getConfigByKey ( 'ORDER_EMAIL' ) );
		$this->getRequest ()->getQuery ()->set ( 'fromName', ConfigApi::getConfigByKey ( 'ORDER_NAME' ) );
		$this->getRequest ()->getQuery ()->set ( 'toEmails', $order->email );
		$this->getRequest ()->getQuery ()->set ( 'toNames', $order->firstName . ' ' . $order->lastName );
		$this->getRequest ()->getQuery ()->set ( 'redirectUrl', $this->url ()->fromRoute ( 'admin-order-view', array (
				'orderId' => $orderId 
		) ) );
		
		return $this->forward ()->dispatch ( 'Mail\Controller\AdminMail', array (
				'action' => 'add' 
		) );
	}
	public function parseInvoiceEmail() {
		$orderId = Functions::fromRoute ( 'orderId', 0 );
		$order = OrderApi::getOrderById ( $orderId );
		$tokenContainer = $this->getTokenContainer ();
		$tokenContainer->addParam ( 'orderId', $order->id );
		$tokenContainer->setSubject ( ConfigApi::getConfigByKey ( 'ORDER_INVOICE_SUBJECT' ) );
		$tokenContainer->setBodyHtml ( ConfigApi::getConfigByKey ( 'ORDER_INVOICE' ) );
		$tokenContainer->setBodyText ( 'dfsa' );
		$tokenContainer->prepare ( 'adminOrderMail' );
		
		return $tokenContainer;
	}
	public function statusAction() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderViewStatus\StatusFactory' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				
				if ($form->get ( 'notifyCustomer' )->getValue ()) {
					$orderId = Functions::fromRoute ( 'orderId', 0 );
					$order = OrderApi::getOrderById ( $orderId );
					$tokenContainer = $this->parseEmail ();
					$this->getRequest ()->getQuery ()->set ( 'subject', $tokenContainer->getSubject () );
					$this->getRequest ()->getQuery ()->set ( 'htmlMessage', $tokenContainer->getBodyHtml () );
					$this->getRequest ()->getQuery ()->set ( 'plainMessage', $tokenContainer->getBodyText () );
					$this->getRequest ()->getQuery ()->set ( 'fromEmail', ConfigApi::getConfigByKey ( 'ORDER_EMAIL' ) );
					$this->getRequest ()->getQuery ()->set ( 'fromName', ConfigApi::getConfigByKey ( 'ORDER_NAME' ) );
					$this->getRequest ()->getQuery ()->set ( 'toEmails', $order->email );
					$this->getRequest ()->getQuery ()->set ( 'toNames', $order->firstName . ' ' . $order->lastName );
					$this->getRequest ()->getQuery ()->set ( 'redirectUrl', $this->url ()->fromRoute ( 'admin-order-view-status', array (
							'orderId' => $form->get ( 'id' )->getValue () 
					) ) );
					return $this->forward ()->dispatch ( 'Mail\Controller\AdminMail', array (
							'action' => 'add' 
					) );
				} else {
					return $this->redirect ()->toRoute ( 'admin-order-view-status', array (
							'orderId' => $form->get ( 'id' )->getValue () 
					) );
				}
				;
			}
		} else {
			
			$form->getRecord ();
		}
		return array (
				'form' => $form 
		);
	}
	
	/**
	 *
	 * @return \Common\Option\Token\TokenContainer
	 */
	public function parseEmail() {
		$orderId = Functions::fromRoute ( 'orderId', 0 );
		$order = OrderApi::getOrderById ( $orderId );
		$tokenContainer = $this->getTokenContainer ();
		$tokenContainer->addParam ( 'orderId', $order->id );
		$tokenContainer->setSubject ( $order->orderStatus->subject );
		$tokenContainer->setBodyHtml ( $order->orderStatus->bodyHtml );
		$tokenContainer->setBodyText ( $order->orderStatus->bodyPlain );
		$tokenContainer->prepare ( 'adminOrderMail' );
		
		return $tokenContainer;
	}
	
	/**
	 *
	 * @return TokenContainer
	 */
	public function getTokenContainer() {
		return $this->getServiceLocator ()->get ( 'TokenContainer' );
	}
}