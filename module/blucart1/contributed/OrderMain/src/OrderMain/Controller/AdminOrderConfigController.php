<?php

namespace OrderMain\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminOrderConfigController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfig\OrderConfigFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-order-config' );
	}
}