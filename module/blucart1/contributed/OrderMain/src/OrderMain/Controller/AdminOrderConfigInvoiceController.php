<?php

namespace OrderMain\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminOrderConfigInvoiceController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-order-config-invoice' );
	}
}