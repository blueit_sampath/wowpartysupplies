<?php

namespace OrderMain\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminOrderConfig extends AbstractTabEvent {
	public function getEventName() {
		return 'adminOrderConfig';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-order-config-general' );
		$tabContainer->add ( 'admin-order-config-general', 'General', $u, 1000 );
		
		$u = $url ( 'admin-order-config-invoice' );
		$tabContainer->add ( 'admin-order-config-Invoice', 'Invoice', $u, 990 );
		
// 		$u = $url ( 'admin-order-config-status' );
// 		$tabContainer->add ( 'admin-order-config-status', 'Order Status', $u, 980 );
		
		$u = $url ( 'admin-order-config-customer-notification' );
		$tabContainer->add ( 'admin-order-config-customer-notification', 'Customer Notification', $u, 970 );
		
		$u = $url ( 'admin-order-config-admin-notification' );
		$tabContainer->add ( 'admin-order-config-admin-notification', 'Admin Notification', $u, 960 );
		
		$u = $url ( 'admin-order-config-comment-customer-notification' );
		$tabContainer->add ( 'admin-order-config-comment-customer-notification', 'Comment Customer Notification', $u, 950 );
		
		$u = $url ( 'admin-order-config-comment-admin-notification' );
		$tabContainer->add ( 'admin-order-config-comment-admin-notification', 'Comment Admin Notification', $u, 940 );
		
		return $this;
	}
}

