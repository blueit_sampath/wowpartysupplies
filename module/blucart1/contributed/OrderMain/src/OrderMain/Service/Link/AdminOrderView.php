<?php

namespace OrderMain\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminOrderView extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminOrderView';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$orderId = Functions::fromRoute ( 'orderId' );
		
		$u = $url ( 'admin-order-comment', array (
				'orderId' => $orderId
		) );
		$item = $linkContainer->add ( 'admin-order-comment', 'Add Comment', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-order-view-sendinvoice', array (
				'orderId' => $orderId
		) );
		$item = $linkContainer->add ( 'admin-order-view-sendinvoice', 'Send Invoice', $u, 1000 );
		
		$u = $url ( 'admin-order-view-status', array (
				'orderId' => $orderId 
		) );
		$item = $linkContainer->add ( 'admin-order-view-status', 'Change Order Status', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}
