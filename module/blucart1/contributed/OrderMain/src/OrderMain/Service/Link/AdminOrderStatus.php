<?php 
namespace OrderMain\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminOrderStatus extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminOrderStatus';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-order-status-add');
		$item = $linkContainer->add ( 'admin-order-status-add', 'Add Order Status', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

