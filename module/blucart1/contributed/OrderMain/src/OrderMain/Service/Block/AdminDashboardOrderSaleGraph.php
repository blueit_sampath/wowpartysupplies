<?php 
namespace OrderMain\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardOrderSaleGraph extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-order-sale-graph';

	public function getBlockName() {
		return 'adminDashboardOrderSaleGraph';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardOrderSaleGraph';
	}
	
}

