<?php 
namespace OrderMain\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardSimpleOrderSaleStat extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-simple-order-sale-stat';

	public function getBlockName() {
		return 'AdminDashboardSimpleOrderSaleStat';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardSimpleOrderSaleStat';
	}
	
}

