<?php 
namespace OrderMain\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminOrderSimpleStat extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-order-simple-stat';

	public function getBlockName() {
		return 'adminOrderSimpleStat';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'AdminOrderSimpleStat';
	}
	
}

