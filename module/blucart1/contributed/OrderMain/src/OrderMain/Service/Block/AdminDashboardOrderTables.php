<?php 
namespace OrderMain\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardOrderTables extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-order-tables';

	public function getBlockName() {
		return 'AdminDashboardOrderTables';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardOrderTables';
	}
	
}

