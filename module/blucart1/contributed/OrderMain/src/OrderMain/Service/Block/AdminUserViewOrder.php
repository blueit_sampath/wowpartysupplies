<?php 
namespace OrderMain\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminUserViewOrder extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-user-view-order';

	public function getBlockName() {
		return 'AdminUserViewOrder';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminUserViewOrder';
	}
	
}

