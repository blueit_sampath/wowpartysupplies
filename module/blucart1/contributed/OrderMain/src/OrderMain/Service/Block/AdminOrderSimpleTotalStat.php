<?php 
namespace OrderMain\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminOrderSimpleTotalStat extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-order-simple-total-stat';

	public function getBlockName() {
		return 'adminOrderSimpleTotalStat';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminOrderSimpleTotalStat';
	}
	
}

