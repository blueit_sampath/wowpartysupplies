<?php

namespace OrderMain\Service\Token;

use OrderMain\Api\OrderApi;
use Core\Functions;
use Common\Option\Token\AbstractTokenEvent;

class AdminOrderStatusChange extends AbstractTokenEvent {
	public function getEventName() {
		return 'adminOrderMail';
	}
	public function token() {
		$tokenContainer = $this->getTokenContainer ();
		$orderId = $tokenContainer->getParam ( 'orderId' );
		$order = OrderApi::getOrderById ( $orderId );
		$item = $tokenContainer->add ( 'order', $order, '', 1000 );
		$entity = OrderApi::getOrderBillingAddressByOrderId ( $orderId );
		$item = $tokenContainer->add ( 'billingAddress', $entity, '', 1000 );
		
		$helper = Functions::getServiceLocator ()->get ( 'viewrenderer' );
		$order->invoiceHtml = $helper->render ( 'common/invoice-table', array (
				'order' => $order 
		) );
		
		return $this;
	}
}

