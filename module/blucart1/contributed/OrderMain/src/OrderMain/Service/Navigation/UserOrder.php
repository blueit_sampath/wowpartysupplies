<?php

namespace OrderMain\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;
use User\Api\UserApi;

class UserOrder extends NavigationEvent {

    public function account() {
        $navigation = $this->getNavigation();
        $user = UserApi::getLoggedInUser();
        if ($user) {
            $navigation->addPages(array(
                array(
                    'label' => 'My Orders',
                    'route' => 'account-order',
                    'id' => 'account-order',
                    'order' => - 500
                )
            ));
        }
    }

    public function breadcrumb() {
        parent::breadcrumb();


        $page = array(
            'label' => 'My Orders',
            'route' => 'account-order',
            'id' => 'account-order'
        );

        $this->findPagesAndAddMenuByUrl($this->getUrl('account'), $page);
        $routeName = Functions::getMatchedRouteName();
        if ($routeName != 'account-order-view') {
            return false;
        }
        $orderId = Functions::fromRoute('orderId', 0);

        $page = array(
            'label' => 'Order Id: #'.$orderId,
            'route' => 'account-order-view',
            'params' => array(
                'orderId' => $orderId
            ),
            'id' => 'order_' . $orderId
        );
        $this->findPagesAndAddMenuByUrl($this->getUrl('account-order'), $page);
    }

}
