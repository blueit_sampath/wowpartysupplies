<?php

namespace OrderMain\Service\Navigation;

use OrderMain\Api\OrderApi;
use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;

class AdminOrderMenu extends AdminNavigationEvent {
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'orderMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Orders',
							'route' => 'admin-order',
							'id' => 'orderMainOrder',
							'iconClass' => 'glyphicon-folder-open'
					)
			) );
		}
		$page = $navigation->findOneBy ( 'id', 'orderMainOrder' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Orders',
							'route' => 'admin-order',
							'id' => 'admin-order',
							'iconClass' => 'glyphicon-folder-open' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderMainOrder' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Order status',
							'route' => 'admin-order-status',
							'id' => 'admin-order-status',
							'iconClass' => 'glyphicon-folder-open' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderMainOrder' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Settings',
							'route' => 'admin-order-config-general',
							'id' => 'orderConfigMain',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderConfigMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'General',
							'route' => 'admin-order-config-general',
							'id' => 'admin-order-config-general',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderConfigMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Invoice',
							'route' => 'admin-order-config-invoice',
							'id' => 'admin-order-config-invoice',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox' 
					) 
			) );
		}
		
// 		$page = $navigation->findOneBy ( 'id', 'orderConfigMain' );
// 		if ($page) {
// 			$page->addPages ( array (
// 					array (
// 							'label' => 'Status',
// 							'route' => 'admin-order-config-status',
// 							'id' => 'admin-order-config-status',
// 							'iconClass' => 'icon-wrench',
// 							'aClass' => 'zoombox' 
// 					) 
// 			) );
// 		}
		
		$page = $navigation->findOneBy ( 'id', 'orderConfigMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Customer Notification',
							'route' => 'admin-order-config-customer-notification',
							'id' => 'admin-order-config-customer-notification',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderConfigMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Admin Notification',
							'route' => 'admin-order-config-admin-notification',
							'id' => 'admin-order-config-admin-notification',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderConfigMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Comment Customer Notification',
							'route' => 'admin-order-config-comment-customer-notification',
							'id' => 'admin-order-config-comment-customer-notification',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'orderConfigMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Comment Admin Notification',
							'route' => 'admin-order-config-comment-admin-notification',
							'id' => 'admin-order-config-comment-admin-notification',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox' 
					) 
			) );
		}
	}
	public function breadcrumb() {
		parent::breadcrumb ();
		$routeName = Functions::getMatchedRouteName ();
		
		if ($routeName === 'admin-order-view') {
			$order = OrderApi::getOrderById ( Functions::fromRoute ( 'orderId' ) );
			$navigation = $this->getNavigation ();
			$page = $navigation->findOneBy ( 'id', 'admin-order' );
			$page->addPages ( array (
					array (
							'label' => $order->id . ' ',
							'route' => 'admin-order-view',
							'params' => array (
									'orderId' => $order->id 
							),
							'id' => 'admin-order-view' 
					) 
			) );
		}
	}
}

