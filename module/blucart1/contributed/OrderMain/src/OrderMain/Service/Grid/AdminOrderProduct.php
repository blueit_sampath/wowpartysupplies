<?php

namespace OrderMain\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminOrderProduct extends AdminOrder {
	public function getEventName() {
		return 'adminOrderProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		$productId = Functions::fromQuery ( 'productId', 0 );
		
		
		
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		
		
		$joinItem = new QueryJoinItem ( '\OrderMain\Entity\OrderProduct', 'orderProduct', 'inner join', 'orderMain.id = orderProduct.orderMain' );
		$joinItem2 = new QueryJoinItem ( 'orderProduct.product', 'product' );
		$joinItem->addJoin ( $joinItem2 );
		$fromItem->addJoin($joinItem);
		
		$joinItem = new QueryJoinItem ( 'orderMain.user', 'user' );
		$fromItem->addJoin ( $joinItem );
		$item = $queryBuilder->addWhere ( 'product.id', 'productId' );
		$queryBuilder->addParameter ( 'productId', $productId );
		return true;
	}
}
