<?php

namespace OrderMain\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminOrderStatus extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'isSale',
			'isPending',
			'isCancelled',
			'weight' 
	);
	protected $_entity = '\OrderMain\Entity\OrderStatus';
	protected $_entityName = 'orderStatus';
	public function getEventName() {
		return 'adminOrderStatus';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'orderStatus.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'orderStatus.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'isPending' );
		$columnItem->setTitle ( 'Is Pending' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'orderStatus.isPending', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'isSale' );
		$columnItem->setTitle ( 'Is Sale' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'orderStatus.isSale', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'isCancelled' );
		$columnItem->setTitle ( 'Is Cancelled' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'orderStatus.isCancelled', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Priority' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 950 );
		$columns->addColumn ( 'orderStatus.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
	
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 700 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	
	
}
