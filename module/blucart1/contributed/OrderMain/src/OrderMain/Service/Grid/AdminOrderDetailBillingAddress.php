<?php 
namespace OrderMain\Service\Grid;

use BlucartGrid\Event\AbstractBlucartGridEvent;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;



class AdminOrderDetailBillingAddress extends AbstractBlucartGridEvent {
	
	protected $_detailTemplateName =  'adminOrderDetailBillingAddress';
	protected $_detailTemplate = 'common/admin-order-detail-billing-address';
	public function getEventName() {
		return 'adminOrder';
	}
	public function details($e) {
		parent::details ( $e );
	
		if ($this->_detailTemplate) {
			$detailContainer = $this->getGrid ()->getDetailContainer ();
			$renderer = $this->getServiceLocator ()->get ( 'viewrenderer' );
			$resultObject = $this->getGrid ()->getResultObject ();
			$params = $resultObject->getResults ();
			$item = $detailContainer->add ( $this->_detailTemplateName );
			$renderer->render ( $this->_detailTemplate, array (
					'item' => $item,
					'params' => $params
			) );
		}
	}
	
	
}
