<?php

namespace OrderMain\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminOrderUser extends AdminOrder {
	public function getEventName() {
		return 'adminOrderUser';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		$columns->removeColumn ( 'orderMain.firstName' );
		$columns->removeColumn ( 'orderMain.lastName' );
		$columns->removeColumn ( 'orderMain.email' );
		$columns->removeColumn ( 'user.id' );
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		$userId = Functions::fromQuery ( 'userId', 0 );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		
		$item = $queryBuilder->addWhere ( 'user.id', 'userId' );
		$queryBuilder->addParameter ( 'userId', $userId );
		return true;
	}
}
