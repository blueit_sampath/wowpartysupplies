<?php

namespace OrderMain\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminOrder extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'firstName',
			'lastName',
			'email',
			'amount',
			'orderStatusName' 
	);
	protected $_entity = '\OrderMain\Entity\OrderMain';
	protected $_entityName = 'orderMain';
	public function getEventName() {
		return 'adminOrder';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'Order ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'orderMain.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'firstName' );
		$columnItem->setTitle ( 'First Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'orderMain.firstName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'lastName' );
		$columnItem->setTitle ( 'Last Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'orderMain.lastName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'email' );
		$columnItem->setTitle ( 'Email' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'orderMain.email', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'userId' );
		$columnItem->setTitle ( 'User ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'user.id', $columnItem );
		
		
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'amount' );
		$columnItem->setTitle ( 'Amount' );
		$columnItem->setType ( 'number' );
		$columnItem->setFormat ( '{0:c}' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 950 );
		$columns->addColumn ( 'orderMain.amount', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'orderStatusName' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 940 );
		$columns->addColumn ( 'orderStatus.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 900 );
		$columnItem->setTemplate('#= kendo.toString(createdDate,"g") #');
		$columns->addColumn ( 'orderMain.createdDate', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
	}
	public function preRead($e) {
		$fromItem = parent::preRead ( $e );
		
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$fromItem->addJoin ( $joinItem );
		
		$joinItem = new QueryJoinItem ( 'orderMain.user', 'user' );
		$fromItem->addJoin ( $joinItem );
                $grid = $this->getGrid();
                $queryBuilder = $grid->getQueryBuilder();
                $queryBuilder->addOrder('orderMain.id', 'id','desc');
		
		return $fromItem;
	}
}
