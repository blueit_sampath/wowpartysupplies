<?php 
namespace OrderMain\Service\Form;

use Config\Service\Form\AdminConfig;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderConfig extends AdminConfig {
	
	protected $_columnKeys = array (
			'ORDER_NAME',
			'ORDER_NOTIFICATION_NAME',
			'ORDER_NOTIFICATION_EMAIL',
			'ORDER_EMAIL',
			'ORDER_STATUS_CHECKOUT',
			'ORDER_INVOICE',
			'ORDER_INVOICE_SUBJECT'
	);
	
	
	public function getFormName() {
		return 'adminOrderConfig';
	}
	public function getPriority() {
		return 1000;
	}
	
}
