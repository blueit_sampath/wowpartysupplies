<?php

namespace OrderMain\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Checkout\Option\CartContainer;
use User\Api\UserApi;
use Config\Api\ConfigApi;
use OrderMain\Api\OrderStatusApi;
use OrderMain\Entity\OrderProduct;
use Checkout\Option\CartItem;
use Product\Api\ProductApi;

class CheckoutOrderSave extends AbstractMainFormEvent {
	public function getFormName() {
		return 'cartCheckout';
	}
	public function getPriority() {
		return 800;
	}
	public function save() {
		$form = $this->getForm ();
		$user = UserApi::getLoggedInUser ();
		$cart = $this->getCart ();
		$em = Functions::getEntityManager ();
		
		$billingAddress = $cart->getParam ( 'billingAddress' );
		
		$orderMain = new \OrderMain\Entity\OrderMain ();
		$orderMain->firstName = $billingAddress ['firstName'];
		$orderMain->lastName = $billingAddress ['lastName'];
		$orderMain->email = $billingAddress ['email'];
		$orderMain->amount = $cart->getTotalPrice ();
		if ($user) {
			$orderMain->user = $user;
		}
		$status = ConfigApi::getConfigByKey ( 'ORDER_STATUS_CHECKOUT' );
		if ($status) {
			$s = OrderStatusApi::getOrderStatusById ( $status );
			if ($s) {
				$orderMain->orderStatus = $s;
			}
		}
		$em->persist ( $orderMain );
		
		/* Order Billing	 */
		$orderBilling = new \OrderMain\Entity\OrderBillingAddress ();
		$array = array (
				'firstName',
				'lastName',
				'email',
				'addressLine1',
				'addressLine2',
				'city',
				'state',
				'country',
				'postcode',
				'contactNumber' 
		);
		foreach($array as $key){
			$orderBilling->$key = $billingAddress[$key];
		}
		$orderBilling->orderMain = $orderMain;
		$em->persist($orderBilling);
		
		/* Order Product Save */
		$cartItems = $cart->getCartItems ();
		if (count ( $cartItems )) {
			foreach ( $cartItems as $cartItem ) {
				$this->saveProduct ( $cartItem, $orderMain );
			}
		}
		/* Order In Items */
		$weight = 1000;
		$inItems = $cart->getInItems ();
		
		foreach ( $inItems as $inItem ) {
			$orderLineItem = new \OrderMain\Entity\OrderLineitem ();
			$orderLineItem->title = $inItem->getTitle ();
			$orderLineItem->value = $inItem->getValue ();
			$orderLineItem->orderMain = $orderMain;
			$orderLineItem->weight = $weight;
			$em->persist ( $orderLineItem );
			$weight --;
		}
		/* Order SubTotal */
		$orderLineItem = new \OrderMain\Entity\OrderLineitem ();
		$orderLineItem->title = 'Sub Total';
		$orderLineItem->value = $cart->getSubTotalPrice ();
		$orderLineItem->weight = $weight;
		$orderLineItem->orderMain = $orderMain;
		$em->persist ( $orderLineItem );
		$weight --;
		
		/* Order Out Items */
		$outItems = $cart->getOutItems ();
		foreach ( $outItems as $outItem ) {
			$orderLineItem = new \OrderMain\Entity\OrderLineitem ();
			$orderLineItem->title = $outItem->getTitle ();
			$orderLineItem->value = $outItem->getValue ();
			$orderLineItem->orderMain = $orderMain;
			$orderLineItem->weight = $weight;
			$em->persist ( $orderLineItem );
			$weight --;
		}
		/* Order Comment */
		$params = $form->getData ();
		if (isset ( $params ['comment'] ) && $params ['comment']) {
			$comment = new \OrderMain\Entity\OrderComment ();
			$comment->comment = $params ['comment'];
			$comment->displayFrontend = 1;
			if ($user) {
				$comment->user = $user;
			}
			$comment->orderMain = $orderMain;
			$em->persist ( $comment );
		}
		$em->flush ();
		$orderId = $orderMain->id;
		$cart->addParam ( 'orderId', $orderId );
	}
	public function saveProduct(CartItem $cartItem, $orderMain) {
		$cart = $this->getCart ();
		$em = Functions::getEntityManager ();
		$orderProduct = new \OrderMain\Entity\OrderProduct ();
		$orderProduct->costPrice = $cartItem->getCostPrice ();
		$orderProduct->listPrice = $cartItem->getListPrice ();
		$orderProduct->sellPrice = $cartItem->getSellPrice ();
		$orderProduct->amount = $cartItem->getTotalPrice ();
		$orderProduct->qty = $cartItem->getQuantity ();
		$orderProduct->productTitle = $cartItem->getTitle ();
		$orderProduct->weight = $cartItem->getWeight ();
		
		$productId = $cartItem->getProductId ();
		if ($productId) {
			$product = ProductApi::getProductById ( $productId );
			if ($product) {
				$orderProduct->product = $product;
			}
		}
		$orderProduct->orderMain = $orderMain;
		$em->persist ( $orderProduct );
		
		$inItems = $cartItem->getInItems ();
		$weight = 1;
		foreach ( $inItems as $inItem ) {
			$orderProductLineItem = new \OrderMain\Entity\OrderProductLineitem ();
			$orderProductLineItem->title = $inItem->getTitle ();
			$orderProductLineItem->value = $inItem->getValue ();
			$orderProductLineItem->orderProduct = $orderProduct;
			$orderProductLineItem->weight = $weight;
			$em->persist ( $orderProductLineItem );
			$weight ++;
		}
		$outItems = $cartItem->getOutItems ();
		foreach ( $outItems as $outItem ) {
			$orderProductLineItem = new \OrderMain\Entity\OrderProductLineitem ();
			$orderProductLineItem->title = $outItem->getTitle ();
			$orderProductLineItem->value = $outItem->getValue ();
			$orderProductLineItem->orderProduct = $orderProduct;
			$orderProductLineItem->weight = $weight;
			$em->persist ( $orderProductLineItem );
			$weight ++;
		}
		$em->flush();
		$cartItem->addParam('orderProductId', $orderProduct->id);
		return $orderProduct;
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	protected function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
