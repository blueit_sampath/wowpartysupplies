<?php

namespace OrderMain\Service\Form;

use Config\Service\Form\AdminConfig;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderConfigGeneral extends AdminConfig {
	protected $_columnKeys = array (
			'ORDER_NAME',
			'ORDER_EMAIL',
			'ORDER_NOTIFICATION_EMAIL',
			'ORDER_NOTIFICATION_NAME',
			'ORDER_TC',
			'ORDER_STATUS_CHECKOUT'
			 
	);
	
	public function getFormName() {
		return 'adminOrderConfigGeneral';
	}
	public function getPriority() {
		return 1000;
	}
}
