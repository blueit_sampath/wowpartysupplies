<?php
namespace OrderMain\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderConfigAdminNotification extends AdminConfig
{

    protected $_columnKeys = array(
        'ORDER_ADMIN_NOTIFICATION_PLAINMESSAGE',
        'ORDER_ADMIN_NOTIFICATION_HTMLMESSAGE',
        'ORDER_ADMIN_NOTIFICATION_SUBJECT',
        'ORDER_ADMIN_NOTIFICATION_ATTACH_INVOICE'
    );

    public function getFormName()
    {
        return 'adminOrderConfigAdminNotification';
    }

    public function getPriority()
    {
        return 1000;
    }
}
