<?php 
namespace OrderMain\Service\Form;

use Config\Service\Form\AdminConfig;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderConfigCommentAdminNotification extends AdminConfig {
	protected $_columnKeys = array (
			'ORDER_COMMENT_ADMIN_NOTIFICATION_PLAINMESSAGE',
			'ORDER_COMMENT_ADMIN_NOTIFICATION_HTMLMESSAGE',
			'ORDER_COMMENT_ADMIN_NOTIFICATION_SUBJECT' 
	);
	
	public function getFormName() {
		return 'adminOrderConfigCommentAdminNotification';
	}
	public function getPriority() {
		return 1000;
	}
	
}
