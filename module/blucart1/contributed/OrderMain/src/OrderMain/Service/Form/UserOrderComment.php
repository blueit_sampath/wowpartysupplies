<?php

namespace OrderMain\Service\Form;

use User\Api\UserApi;
use OrderMain\Api\OrderApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class UserOrderComment extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'comment'
	);
	protected $_entity = '\OrderMain\Entity\OrderComment';
	protected $_entityName = 'orderComment';
	public function getFormName() {
		return 'userOrderCommentAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		if (isset ( $params ['order'] )) {
			$entity->orderMain = OrderApi::getOrderById ( $params ['order'] );
		}
		$entity->displayFrontend = 1;
		$entity->user = UserApi::getLoggedInUser ();
	}
	public function getId() {
		return Functions::fromRoute ( 'orderCommentId' );
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$orderId = Functions::fromRoute ( 'orderId' );
		
		$orderCommentId = Functions::fromRoute ( 'orderCommentId' );
		$form->get ( 'order' )->setValue ( $orderId );
		
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		if ($entity->orderMain) {
			$form->get ( 'order' )->setValue ( $entity->orderMain->id );
		}
	}
}
