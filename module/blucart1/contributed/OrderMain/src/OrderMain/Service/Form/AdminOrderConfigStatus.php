<?php

namespace OrderMain\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderConfigStatus extends AdminConfig {
	protected $_columnKeys = array (
			'ORDER_STATUS_DEFAULT_SUBJECT',
			'ORDER_STATUS_DEFAULT_HTMLMESSAGE',
			'ORDER_STATUS_DEFAULT_PLAINMESSAGE',
			'ORDER_STATUS_CHECKOUT' 
	);
	public function getFormName() {
		return 'adminOrderConfigStatus';
	}
	public function getPriority() {
		return 1000;
	}
}
