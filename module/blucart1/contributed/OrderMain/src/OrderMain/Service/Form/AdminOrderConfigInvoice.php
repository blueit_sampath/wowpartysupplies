<?php 
namespace OrderMain\Service\Form;

use Config\Service\Form\AdminConfig;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderConfigInvoice extends AdminConfig {
	protected $_columnKeys = array (
			'ORDER_INVOICE',
			'ORDER_INVOICE_SUBJECT'
				);
	
	public function getFormName() {
		return 'adminOrderConfigInvoice';
	}
	public function getPriority() {
		return 1000;
	}
	
}
