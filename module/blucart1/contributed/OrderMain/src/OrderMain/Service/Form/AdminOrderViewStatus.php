<?php

namespace OrderMain\Service\Form;

use OrderMain\Api\OrderStatusApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderViewStatus extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id' 
	);
	protected $_entity = '\OrderMain\Entity\OrderMain';
	protected $_entityName = 'orderMain';
	public function getFormName() {
		
		return 'adminOrderViewStatus';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		
		if (isset ( $params ['orderStatus'] )) {
			$entity->orderStatus = OrderStatusApi::getOrderStatusById ( $params ['orderStatus'] );
		}
	}
	
	public function getId(){
		
		return Functions::fromRoute('orderId');
	}
	
	public function afterGetRecord($entity) {
		
		$form = $this->getForm ();
		if ($entity->orderStatus) {
			$form->get ( 'orderStatus' )->setValue ( $entity->orderStatus->id );
		}
	}
}
