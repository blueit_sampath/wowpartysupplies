<?php 
namespace OrderMain\Service\Form;

use Config\Service\Form\AdminConfig;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderConfigCommentCustomerNotification extends AdminConfig {
	protected $_columnKeys = array (
			'ORDER_COMMENT_CUSTOMER_NOTIFICATION_PLAINMESSAGE',
			'ORDER_COMMENT_CUSTOMER_NOTIFICATION_HTMLMESSAGE',
			'ORDER_COMMENT_CUSTOMER_NOTIFICATION_SUBJECT',
	  
	);
	
	public function getFormName() {
		return 'adminOrderConfigCommentCustomerNotification';
	}
	public function getPriority() {
		return 1000;
	}
	
}
