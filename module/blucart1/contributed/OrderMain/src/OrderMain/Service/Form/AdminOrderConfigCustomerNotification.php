<?php
namespace OrderMain\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderConfigCustomerNotification extends AdminConfig
{

    protected $_columnKeys = array(
        'ORDER_CUSTOMER_NOTIFICATION_PLAINMESSAGE',
        'ORDER_CUSTOMER_NOTIFICATION_HTMLMESSAGE',
        'ORDER_CUSTOMER_NOTIFICATION_SUBJECT',
        'ORDER_CUSTOMER_NOTIFICATION_ATTACH_INVOICE'
    );

    public function getFormName()
    {
        return 'adminOrderConfigCustomerNotification';
    }

    public function getPriority()
    {
        return 1000;
    }
}
