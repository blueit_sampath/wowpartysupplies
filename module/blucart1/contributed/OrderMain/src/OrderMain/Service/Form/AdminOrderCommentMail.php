<?php

namespace OrderMain\Service\Form;

use Config\Api\ConfigApi;
use Common\Option\Token\TokenContainer;
use Common\Form\Option\AbstractFrontMainFormEvent;

class AdminOrderCommentMail extends AbstractFrontMainFormEvent {
	public function getFormName() {
		return 'adminOrderCommentAdd';
	}
	public function getPriority() {
		return null;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		
		$resultContainer = $this->getFormResultContainer ();
		$orderComment = $resultContainer->get ( 'orderComment' )->getValue ();
		
		if ($orderComment) {
			
			$this->sendEmail ( $orderComment );
		}
		return true;
	}
	
	/**
	 *
	 * @return \Common\Option\Token\TokenContainer
	 */
	public function sendEmail($orderComment) {
		$subject = ConfigApi::getConfigByKey ( 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_SUBJECT' );
		$bodyHtml = ConfigApi::getConfigByKey ( 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_HTMLMESSAGE' );
		$bodyPlain = ConfigApi::getConfigByKey ( 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_PLAINMESSAGE' );
		
		$fromName = ConfigApi::getConfigByKey ( 'ORDER_NAME', '' );
		$fromEmail = ConfigApi::getConfigByKey ( 'ORDER_EMAIL' );
		
		if (! $fromEmail) {
			return;
		}
		
		if ($subject && ($bodyHtml || $bodyPlain) && ($orderComment->displayFrontend)) {
			
			$tokenContainer = $this->getTokenContainer ();
			$tokenContainer->add ( 'orderComment', $orderComment );
			$tokenContainer->addParam ( 'order', $orderComment->orderMain );
			$tokenContainer->addParam ( 'orderId', $orderComment->orderMain->id );
			$tokenContainer->setSubject ( $subject );
			$tokenContainer->setBodyHtml ( $bodyHtml );
			$tokenContainer->setBodyText ( $bodyPlain );
			$tokenContainer->setFromEmail ( $fromEmail );
			$tokenContainer->setFromName ( $fromName );
			
			$tokenContainer->setToEmail ( $orderComment->orderMain->user->email );
			$tokenContainer->setToName ( $orderComment->orderMain->user->firstName . ' ' . $orderComment->orderMain->user->lastName );
			
			$tokenContainer->prepare ( 'adminOrderMail' );
			
			$tokenContainer->sendMail ();
		}
		
		$subject = ConfigApi::getConfigByKey ( 'ORDER_COMMENT_ADMIN_NOTIFICATION_SUBJECT' );
		$bodyHtml = ConfigApi::getConfigByKey ( 'ORDER_COMMENT_ADMIN_NOTIFICATION_HTMLMESSAGE' );
		$bodyPlain = ConfigApi::getConfigByKey ( 'ORDER_COMMENT_ADMIN_NOTIFICATION_PLAINMESSAGE' );
		$toName = ConfigApi::getConfigByKey ( 'ORDER_NOTIFICATION_NAME' );
		$toEmail = ConfigApi::getConfigByKey ( 'ORDER_NOTIFICATION_EMAIL' );
		
		if ($subject && ($bodyHtml || $bodyPlain) && $toEmail) {
			
			$tokenContainer = $this->getTokenContainer ();
			$tokenContainer->add ( 'orderComment', $orderComment );
			$tokenContainer->addParam ( 'order', $orderComment->orderMain );
			$tokenContainer->addParam ( 'orderId', $orderComment->orderMain->id );
			$tokenContainer->setSubject ( $subject );
			$tokenContainer->setBodyHtml ( $bodyHtml );
			$tokenContainer->setBodyText ( $bodyPlain );
			$tokenContainer->setFromEmail ( $fromEmail );
			$tokenContainer->setFromName ( $fromName );
			
			$tokenContainer->setToEmail ( $toEmail );
			$tokenContainer->setToName ( $toName  );
			
			$tokenContainer->prepare ( 'adminOrderMail' );
			$tokenContainer->sendMail ();
		}
		
		return $tokenContainer;
	}
	
	/**
	 *
	 * @return TokenContainer
	 */
	public function getTokenContainer() {
		return $this->getServiceLocator ()->get ( 'TokenContainer' );
	}
}
