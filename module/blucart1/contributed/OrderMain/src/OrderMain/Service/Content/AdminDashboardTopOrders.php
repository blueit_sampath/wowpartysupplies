<?php 
namespace OrderMain\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardTopOrders extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-top-orders';
	protected $_contentName = 'adminDashboardTopOrders';
	public function getEventName() {
		return 'adminDashboardTopStastics';
	}
	public function getPriority() {
		return 990;
	}
	
}

