<?php 
namespace OrderMain\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class UserDashboardOrder extends AbstractContentEvent {

	protected $_contentTemplate = 'common/user-dashboard-order';
	protected $_contentName = 'userDashboardOrder';
	public function getEventName() {
		return 'userDashboard';
	}
	public function getPriority() {
		return 800;
	}
	
}

