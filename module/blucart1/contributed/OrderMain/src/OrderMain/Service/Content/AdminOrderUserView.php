<?php 
namespace OrderMain\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminOrderUserView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-order-user-view';
	protected $_contentName = 'adminOrderUserView';
	public function getEventName() {
		return 'adminUserView';
	}
	public function getPriority() {
		return 1000;
	}
	
}

