<?php 
namespace OrderMain\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardMainContentOrder extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-main-content-order';
	protected $_contentName = 'adminDashboardMainContentOrder';
	public function getEventName() {
		return 'adminDashboardMainContent';
	}
	public function getPriority() {
		return 1000;
	}
	
}

