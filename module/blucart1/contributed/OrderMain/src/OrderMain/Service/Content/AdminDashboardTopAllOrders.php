<?php 
namespace OrderMain\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardTopAllOrders extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-top-all-orders';
	protected $_contentName = 'adminDashboardTopAllOrders';
	public function getEventName() {
		return 'adminDashboardTopStastics';
	}
	public function getPriority() {
		return 980;
	}
	
}

