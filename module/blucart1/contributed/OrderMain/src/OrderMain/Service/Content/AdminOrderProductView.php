<?php 
namespace OrderMain\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminOrderProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-order-product-view';
	protected $_contentName = 'adminOrderProductView';
	public function getEventName() {
		return 'adminProductView';
	}
	public function getPriority() {
		return 800;
	}
	
}

