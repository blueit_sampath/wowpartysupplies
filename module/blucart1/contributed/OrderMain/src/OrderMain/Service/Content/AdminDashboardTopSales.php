<?php 
namespace OrderMain\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardTopSales extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-top-sales';
	protected $_contentName = 'admin-dashboard-top-sales';
	public function getEventName() {
		return 'adminDashboardTopStastics';
	}
	public function getPriority() {
		return 1000;
	}
	
}

