<?php
namespace OrderMain\Form\AdminOrderConfigCommentAdminNotification;

use Common\Form\Option\AbstractFormFactory;

class OrderConfigCommentAdminNotificationFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationForm' );
		$form->setName ( 'adminOrderConfigCommentAdminNotification' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationFilter' );
	}
}
