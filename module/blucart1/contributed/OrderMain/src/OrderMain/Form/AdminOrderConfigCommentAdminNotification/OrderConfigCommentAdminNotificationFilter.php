<?php

namespace OrderMain\Form\AdminOrderConfigCommentAdminNotification;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class OrderConfigCommentAdminNotificationFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'ORDER_COMMENT_ADMIN_NOTIFICATION_SUBJECT',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_COMMENT_ADMIN_NOTIFICATION_HTMLMESSAGE',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_COMMENT_ADMIN_NOTIFICATION_PLAINMESSAGE',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				) 
		) );
	}
} 