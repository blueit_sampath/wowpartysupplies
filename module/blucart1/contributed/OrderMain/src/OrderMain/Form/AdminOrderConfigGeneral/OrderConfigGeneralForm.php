<?php

namespace OrderMain\Form\AdminOrderConfigGeneral;

use OrderMain\Api\OrderStatusApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class OrderConfigGeneralForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'ORDER_NAME',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Order From Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_EMAIL',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Order From Email' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_NOTIFICATION_NAME',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'Notification Name'
				)
		), array (
				'priority' => 980
		) );
		
		$this->add ( array (
				'name' => 'ORDER_NOTIFICATION_EMAIL',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'Notification Email'
				)
		), array (
				'priority' => 970
		) );
		
		$select = new Select ( 'ORDER_STATUS_CHECKOUT' );
		$select->setLabel ( 'Order status in checkout' );
		$select->setValueOptions ( $this->getOrderStatus () );
		$this->add ( $select, array (
				'priority' => 960
		) );
		
		$this->add ( array (
				'name' => 'ORDER_TC',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_mini'
				),
				'options' => array (
						'label' => 'Terms & Condition in Checkout page'
				)
		), array (
				'priority' => 950
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
	public function getOrderStatus() {
		$array = array ();
	
		$statuses = OrderStatusApi::getOrderStatuses ();
		foreach ( $statuses as $statusId ) {
			$status = OrderStatusApi::getOrderStatusById ( $statusId ['orderStatusId'] );
			$array [$status->id] = $status->name;
		}
		return $array;
	}
	
}