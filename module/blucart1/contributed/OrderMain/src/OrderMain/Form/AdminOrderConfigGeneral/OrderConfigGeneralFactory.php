<?php
namespace OrderMain\Form\AdminOrderConfigGeneral;

use Common\Form\Option\AbstractFormFactory;

class OrderConfigGeneralFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralForm' );
		$form->setName ( 'adminOrderConfigGeneral' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralFilter' );
	}
}
