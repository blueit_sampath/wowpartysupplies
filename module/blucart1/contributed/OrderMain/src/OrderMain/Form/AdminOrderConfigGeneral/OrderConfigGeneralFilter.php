<?php

namespace OrderMain\Form\AdminOrderConfigGeneral;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class OrderConfigGeneralFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'ORDER_EMAIL',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'EmailAddress' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_NAME',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_NOTIFICATION_EMAIL',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'EmailAddress' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_NOTIFICATION_NAME',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'ORDER_TC',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags',
								'options' => array (
										'allowTags' => array('a','span', 'em','strong'),
										'allowAttribs' => array('style','href')
								) 
						)
						 
				) 
		) );
	}
} 