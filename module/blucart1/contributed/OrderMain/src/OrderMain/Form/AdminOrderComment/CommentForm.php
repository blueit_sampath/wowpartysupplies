<?php

namespace OrderMain\Form\AdminOrderComment;

use Zend\Form\Element\Radio;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CommentForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'order',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'user',
				'attributes' => array (
						'type' => 'hidden'
				)
		) );
		
		$this->add ( array (
				'name' => 'comment',
				'attributes' => array (
						'type' => 'textarea' 
				)
				,
				'options' => array (
						'label' => 'Comment' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'displayFrontend',
				'options' => array (
						'label' => 'Display To Customer',
						'use_hidden_element' => true
				)
		), array (
				'priority' => 990
		) );
		
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}