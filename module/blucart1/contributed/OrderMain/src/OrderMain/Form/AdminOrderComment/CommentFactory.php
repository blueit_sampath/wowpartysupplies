<?php
namespace OrderMain\Form\AdminOrderComment;

use Common\Form\Option\AbstractFormFactory;

class CommentFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderComment\CommentForm' );
		$form->setName ( 'adminOrderCommentAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderComment\CommentFilter' );
	}
}
