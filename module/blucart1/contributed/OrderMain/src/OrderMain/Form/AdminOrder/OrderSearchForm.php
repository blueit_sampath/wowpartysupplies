<?php

namespace OrderMain\Form\AdminOrder;

use Common\Form\Form;
use Zend\Form\Element\Select;

class OrderSearchForm extends Form {
	public function __construct($name = "adminOrderSearch") {
		parent::__construct ( $name );
		$this->initPreEvent ();
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-eq',
						'placeholder' => 'Order ID' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'firstName',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'First Name' 
				) 
		), array (
				'priority' => 990 
		) );
		$this->add ( array (
				'name' => 'lastName',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Last Name' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'email',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Email' 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->add ( array (
				'name' => 'userId',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-eq',
						'placeholder' => 'User ID' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'amount',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-eq',
						'placeholder' => 'Amount' 
				) 
		), array (
				'priority' => 950 
		) );
		
		$this->add ( array (
				'name' => 'orderStatusName',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Status' 
				) 
		), array (
				'priority' => 940 
		) );
		
		$this->add ( array (
				'name' => 'createdDate',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-gte date',
						'placeholder' => 'Order Date' 
				) 
		), array (
				'priority' => 900 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Search' 
				) 
		), array (
				'priority' => - 1000 
		) );
		$this->initPostEvent ();
	}
}