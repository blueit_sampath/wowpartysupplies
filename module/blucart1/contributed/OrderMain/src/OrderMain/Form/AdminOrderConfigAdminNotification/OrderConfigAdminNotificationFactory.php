<?php
namespace OrderMain\Form\AdminOrderConfigAdminNotification;

use Common\Form\Option\AbstractFormFactory;

class OrderConfigAdminNotificationFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationForm' );
		$form->setName ( 'adminOrderConfigAdminNotification' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationFilter' );
	}
}
