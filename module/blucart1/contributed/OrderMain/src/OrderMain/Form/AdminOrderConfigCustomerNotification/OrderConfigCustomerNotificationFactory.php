<?php
namespace OrderMain\Form\AdminOrderConfigCustomerNotification;

use Common\Form\Option\AbstractFormFactory;

class OrderConfigCustomerNotificationFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationForm' );
		$form->setName ( 'adminOrderConfigCustomerNotification' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationFilter' );
	}
}
