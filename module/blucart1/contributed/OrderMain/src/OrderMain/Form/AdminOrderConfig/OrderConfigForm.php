<?php

namespace OrderMain\Form\AdminOrderConfig;

use OrderMain\Api\OrderStatusApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class OrderConfigForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'ORDER_NAME',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Order From Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_EMAIL',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Order Email' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$select = new Select ( 'ORDER_STATUS_CHECKOUT' );
		$select->setLabel ( 'Order status in checkout' );
		$select->setValueOptions ( $this->getOrderStatus () );
		$this->add ( $select, array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_INVOICE_SUBJECT',
				'attributes' => array (
						'type' => 'text',
		
				),
				'options' => array (
						'label' => 'Invoice Subject'
				)
		), array (
				'priority' => 970
		) );
		$this->add ( array (
				'name' => 'ORDER_INVOICE',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full'
				),
				'options' => array (
						'label' => 'Invoice Template'
				)
		), array (
				'priority' => 960
		) );
		
	
		
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getOrderStatus() {
		$array = array (
				'' => '' 
		);
		
		$statuses = OrderStatusApi::getOrderStatuses ();
		foreach ( $statuses as $statusId ) {
			$status = OrderStatusApi::getOrderStatusById ( $statusId ['orderStatusId'] );
			$array [$status->id] = $status->name;
		}
		return $array;
	}
}