<?php
namespace OrderMain\Form\AdminOrderConfig;

use Common\Form\Option\AbstractFormFactory;

class OrderConfigFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfig\OrderConfigForm' );
		$form->setName ( 'adminOrderConfig' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfig\OrderConfigFilter' );
	}
}
