<?php

namespace OrderMain\Form\AdminOrderConfig;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class OrderConfigFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'ORDER_STATUS_CHECKOUT',
				'required' => true 
		)
		 );
		$this->add ( array (
				'name' => 'ORDER_EMAIL',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'EmailAddress' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_NAME',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				) 
		)
		 );
		
		$this->add ( array (
				'name' => 'ORDER_INVOICE',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				) 
		)
		 );
		$this->add ( array (
				'name' => 'ORDER_INVOICE_SUBJECT',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						)
				)
		)
		);
	}
} 