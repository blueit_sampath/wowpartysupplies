<?php
namespace OrderMain\Form\AdminOrderViewStatusEmail;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  OrderViewStatusEmailFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'description',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						)
				)
		) );
		$this->add ( array (
				'name' => 'weight',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		
		
		
		$this->add ( array (
				'name' => 'status',
				'required' => true,
				'validators' => array (
						array (
								'name' => 'NotEmpty' 
						),
						array (
								'name' => 'Int' 
						) 
				) 
		) );
	}
} 