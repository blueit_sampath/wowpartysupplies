<?php
namespace OrderMain\Form\AdminOrderViewStatusEmail;

use Common\Form\Option\AbstractFormFactory;

class OrderViewStatusEmailFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailForm' );
		$form->setName ( 'adminOrderViewStatusEmail' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailFilter' );
	}
}
