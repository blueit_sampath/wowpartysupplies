<?php

namespace OrderMain\Form\AdminOrderViewStatusEmail;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class OrderViewStatusEmailForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'subject',
				'attributes' => array (
						'type' => 'text' 
				)
				,
				'options' => array (
						'label' => 'Subject' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'bodyHtml',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'Description' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'bodyPlain',
				'attributes' => array (
						'type' => 'textarea' 
				),
				'options' => array (
						'label' => 'Description' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'fromName',
				'attributes' => array (
						'type' => 'hidden' 
				)
				 
		) );
		
		$this->add ( array (
				'name' => 'fromEmail',
				'attributes' => array (
						'type' => 'hidden' 
				)
				 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Send' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}