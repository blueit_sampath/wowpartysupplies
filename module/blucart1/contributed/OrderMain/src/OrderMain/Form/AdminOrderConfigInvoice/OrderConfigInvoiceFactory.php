<?php
namespace OrderMain\Form\AdminOrderConfigInvoice;

use Common\Form\Option\AbstractFormFactory;

class OrderConfigInvoiceFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceForm' );
		$form->setName ( 'adminOrderConfigInvoice' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceFilter' );
	}
}
