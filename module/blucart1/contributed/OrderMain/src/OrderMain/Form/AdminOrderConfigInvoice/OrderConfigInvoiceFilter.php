<?php
namespace OrderMain\Form\AdminOrderConfigInvoice;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  OrderConfigInvoiceFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'ORDER_INVOICE',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						
				) 
		)
		 );
		$this->add ( array (
				'name' => 'ORDER_INVOICE_SUBJECT',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim',
						),
						array (
								'name' => 'StripTags'
						)
				)
			)
		);
	}
} 