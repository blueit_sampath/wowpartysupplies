<?php

namespace OrderMain\Form\AdminOrderConfigInvoice;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class OrderConfigInvoiceForm extends Form 

{
	public function init() {
		
		
		$this->add ( array (
				'name' => 'ORDER_INVOICE_SUBJECT',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Invoice Subject' 
				) 
		), array (
				'priority' => 1000 
		) );
		$this->add ( array (
				'name' => 'ORDER_INVOICE',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'Invoice Template' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}