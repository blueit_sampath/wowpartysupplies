<?php

namespace OrderMain\Form\AdminOrderStatus;

use Zend\Form\Element\Select;
use Config\Api\ConfigApi;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class MailFieldset extends Fieldset implements InputFilterProviderInterface {
	public function __construct($name = 'mailFieldset') {
		parent::__construct ( $name );
		
		$this->add ( array (
				'name' => 'subject',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Subject' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'bodyHtml',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'HTML Message' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'bodyPlain',
				'attributes' => array (
						'type' => 'textarea' 
				)
				,
				'options' => array (
						'label' => 'Plain Message' 
				) 
		), array (
				'priority' => 980 
		) );
	}
	
	/**
	 * Should return an array specification compatible with
	 * {@link Zend\InputFilter\Factory::createInputFilter()}.
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		return array (
				'subject' => array (
						'required' => false 
				)
				,
				'bodyHtml' => array (
						'required' => false 
				)
				,
				'bodyPlain' => array (
						'required' => false 
				) 
		)
		;
	}
}
