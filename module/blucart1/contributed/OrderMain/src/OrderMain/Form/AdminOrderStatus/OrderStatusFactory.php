<?php
namespace OrderMain\Form\AdminOrderStatus;

use Common\Form\Option\AbstractFormFactory;

class OrderStatusFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderStatus\OrderStatusForm' );
		$form->setName ( 'adminOrderStatus' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderStatus\OrderStatusFilter' );
	}
}
