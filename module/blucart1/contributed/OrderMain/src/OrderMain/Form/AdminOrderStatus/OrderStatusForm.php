<?php

namespace OrderMain\Form\AdminOrderStatus;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class OrderStatusForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'isSale',
				'options' => array (
						'label' => 'Is Sale',
						'use_hidden_element' => true 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'isPending',
				'options' => array (
						'label' => 'Is Pending',
						'use_hidden_element' => true 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'isCancelled',
				'options' => array (
						'label' => 'Is Cancelled',
						'use_hidden_element' => true 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Priority' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'mailFieldset',
				'type' => 'OrderMain\Form\AdminOrderStatus\MailFieldset',
				'options' => array (
						'legend' => 'Mail Settings' 
				) 
		), 

		array (
				'priority' => 940 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}