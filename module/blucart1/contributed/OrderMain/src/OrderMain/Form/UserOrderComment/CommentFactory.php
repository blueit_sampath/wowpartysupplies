<?php
namespace OrderMain\Form\UserOrderComment;

use Common\Form\Option\AbstractFormFactory;

class CommentFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\UserOrderComment\CommentForm' );
		$form->setName ( 'userOrderCommentAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\UserOrderComment\CommentFilter' );
	}
}
