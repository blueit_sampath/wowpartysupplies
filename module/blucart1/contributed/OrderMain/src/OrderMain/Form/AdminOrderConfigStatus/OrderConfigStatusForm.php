<?php
namespace OrderMain\Form\AdminOrderConfigStatus;

use OrderMain\Api\OrderStatusApi;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class OrderConfigStatusForm extends Form 

{
	public function init() {
		
		$select = new Select ( 'ORDER_STATUS_CHECKOUT' );
		$select->setLabel ( 'Order status in checkout' );
		$select->setValueOptions ( $this->getOrderStatus () );
		$this->add ( $select, array (
				'priority' => 1000
		) );
		
		$this->add ( array (
				'name' => 'ORDER_STATUS_DEFAULT_SUBJECT',
				'attributes' => array (
						'type' => 'text'
				)
				,
				'options' => array (
						'label' => 'Default Subject'
				)
		), array (
				'priority' => 1000
		) );
		$this->add ( array (
				'name' => 'ORDER_STATUS_DEFAULT_HTMLMESSAGE',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full'
				),
				'options' => array (
						'label' => 'Default Html Message'
				)
		), array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'ORDER_STATUS_DEFAULT_PLAINMESSAGE',
				'attributes' => array (
						'type' => 'textarea',
						
				),
				'options' => array (
						'label' => 'Default Plain Message'
				)
		), array (
				'priority' => 990
		) );
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
	public function getOrderStatus() {
		$array = array ();
	
		$statuses = OrderStatusApi::getOrderStatuses ();
		foreach ( $statuses as $statusId ) {
			$status = OrderStatusApi::getOrderStatusById ( $statusId ['orderStatusId'] );
			$array [$status->id] = $status->name;
		}
		return $array;
	}
	
}