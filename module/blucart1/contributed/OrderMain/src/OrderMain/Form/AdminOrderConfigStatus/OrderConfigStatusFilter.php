<?php

namespace OrderMain\Form\AdminOrderConfigStatus;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class OrderConfigStatusFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'ORDER_STATUS_CHECKOUT',
				'required' => true
		) );
		$this->add ( array (
				'name' => 'ORDER_STATUS_DEFAULT_SUBJECT',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				)
				 
		) );
		$this->add ( array (
				'name' => 'ORDER_STATUS_DEFAULT_HTMLMESSAGE',
				'required' => TRUE,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				)
				 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_STATUS_DEFAULT_PLAINMESSAGE',
				'required' => TRUE,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags'
						)
				)
				 
		) );
	}
} 