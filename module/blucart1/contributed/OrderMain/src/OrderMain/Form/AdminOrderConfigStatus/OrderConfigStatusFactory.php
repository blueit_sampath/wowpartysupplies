<?php
namespace OrderMain\Form\AdminOrderConfigStatus;

use Common\Form\Option\AbstractFormFactory;

class OrderConfigStatusFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusForm' );
		$form->setName ( 'adminOrderConfigStatus' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusFilter' );
	}
}
