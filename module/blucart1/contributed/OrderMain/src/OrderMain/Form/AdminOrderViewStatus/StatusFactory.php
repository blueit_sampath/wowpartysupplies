<?php
namespace OrderMain\Form\AdminOrderViewStatus;

use Common\Form\Option\AbstractFormFactory;

class StatusFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderViewStatus\StatusForm' );
		$form->setName ( 'adminOrderViewStatus' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderViewStatus\StatusFilter' );
	}
}
