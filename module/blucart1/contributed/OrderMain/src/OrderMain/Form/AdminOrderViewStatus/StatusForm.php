<?php
namespace OrderMain\Form\AdminOrderViewStatus;

use OrderMain\Api\OrderStatusApi;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class StatusForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		
		$select = new Select ( 'orderStatus' );
		$select->setLabel ( 'Order Status' );
		$select->setValueOptions ( $this->getOrderStatus () );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'notifyCustomer',
				'options' => array (
						'label' => 'Notify Customer',
						'use_hidden_element' => true
				)
		), array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
	public function getOrderStatus(){
		$array = array('' => '');

		$statuses = OrderStatusApi::getOrderStatuses();
		foreach($statuses as $statusId){
			$status = OrderStatusApi::getOrderStatusById($statusId['orderStatusId']);
			$array[$status->id] = $status->name;
		}
		return $array;
	}
	
}