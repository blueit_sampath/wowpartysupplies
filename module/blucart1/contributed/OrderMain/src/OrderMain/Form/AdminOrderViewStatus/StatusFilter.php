<?php
namespace OrderMain\Form\AdminOrderViewStatus;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  StatusFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'orderStatus',
				'required' => true,
				
		) );
		$this->add ( array (
				'name' => 'notifyCustomer',
				'required' => false,
				
		) );
		
	
	}
} 