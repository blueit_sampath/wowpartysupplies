<?php
namespace OrderMain\Form\AdminOrderConfigCommentCustomerNotification;

use Common\Form\Option\AbstractFormFactory;

class OrderConfigCommentCustomerNotificationFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationForm' );
		$form->setName ( 'adminOrderConfigCommentCustomerNotification' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationFilter' );
	}
}
