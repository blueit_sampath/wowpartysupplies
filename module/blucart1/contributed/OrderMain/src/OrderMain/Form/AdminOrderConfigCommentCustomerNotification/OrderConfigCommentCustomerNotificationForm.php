<?php

namespace OrderMain\Form\AdminOrderConfigCommentCustomerNotification;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class OrderConfigCommentCustomerNotificationForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_SUBJECT',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Comment Customer Notification Subject' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_HTMLMESSAGE',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'Comment Customer Notification HTML Message' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_PLAINMESSAGE',
				'attributes' => array (
						'type' => 'textarea' 
				)
				,
				'options' => array (
						'label' => 'Comment Customer Notification Plain Message' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}