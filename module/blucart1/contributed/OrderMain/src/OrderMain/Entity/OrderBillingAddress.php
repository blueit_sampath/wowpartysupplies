<?php

namespace OrderMain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * OrderBillingAddress
 *
 * @ORM\Table(name="order_billing_address")
 * @ORM\Entity
 */
class OrderBillingAddress {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="first_name", type="string", length=255,
	 *      nullable=true)
	 */
	private $firstName;
	
	/**
	 *
	 * @var string @ORM\Column(name="last_name", type="string", length=255,
	 *      nullable=true)
	 */
	private $lastName;
	/**
	 *
	 * @var string @ORM\Column(name="email", type="string", length=255,
	 *      nullable=true)
	 */
	private $email;
	
	/**
	 *
	 * @var string @ORM\Column(name="address_line1", type="string", length=255,
	 *      nullable=true)
	 */
	private $addressLine1;
	
	/**
	 *
	 * @var string @ORM\Column(name="address_line2", type="string", length=255,
	 *      nullable=true)
	 */
	private $addressLine2;
	
	/**
	 *
	 * @var string @ORM\Column(name="city", type="string", length=255,
	 *      nullable=true)
	 */
	private $city;
	
	/**
	 *
	 * @var string @ORM\Column(name="state", type="string", length=255,
	 *      nullable=true)
	 */
	private $state;
	
	/**
	 *
	 * @var string @ORM\Column(name="country", type="string", length=255,
	 *      nullable=true)
	 */
	private $country;
	
	/**
	 *
	 * @var string @ORM\Column(name="postcode", type="string", length=255,
	 *      nullable=true)
	 */
	private $postcode;
	
	/**
	 *
	 * @var string @ORM\Column(name="contact_number", type="string", length=255,
	 *      nullable=true)
	 */
	private $contactNumber;
	
	/**
	 *
	 * @var \OrderMain @ORM\ManyToOne(targetEntity="OrderMain")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="order_main_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $orderMain;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
	public function __isset($name){
		return true;
	}
}
