<?php

namespace OrderMain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * OrderProduct
 *
 * @ORM\Table(name="order_product")
 * @ORM\Entity
 */
class OrderProduct {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var float @ORM\Column(name="cost_price", type="float", nullable=true)
	 */
	private $costPrice;
	
	/**
	 *
	 * @var float @ORM\Column(name="list_price", type="float", nullable=true)
	 */
	private $listPrice;
	
	/**
	 *
	 * @var float @ORM\Column(name="sell_price", type="float", nullable=true)
	 */
	private $sellPrice;
	
	/**
	 *
	 * @var float @ORM\Column(name="amount", type="float", nullable=true)
	 */
	private $amount;
	
	/**
	 *
	 * @var integer @ORM\Column(name="weight", type="integer", nullable=true)
	 */
	private $weight;
	
	/**
	 *
	 * @var integer @ORM\Column(name="qty", type="integer", nullable=true)
	 */
	private $qty;
	
	/**
	 *
	 * @var string @ORM\Column(name="product_title", type="string", length=255,
	 *      nullable=true)
	 */
	private $productTitle;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	
	/**
	 *
	 * @var \OrderMain @ORM\ManyToOne(targetEntity="OrderMain")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="order_main_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $orderMain;
	
	/**
	 *
	 * @var \Product @ORM\ManyToOne(targetEntity="\Product\Entity\Product")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="product_id", referencedColumnName="id",nullable=true,onDelete="SET NULL")
	 *      })
	 */
	private $product;
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
	
	public function __isset($name){
		return true;
	}
}
