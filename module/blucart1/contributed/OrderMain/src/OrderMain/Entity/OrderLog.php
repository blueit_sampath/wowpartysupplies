<?php

namespace OrderMain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * OrderLog
 *
 * @ORM\Table(name="order_log")
 * @ORM\Entity
 */
class OrderLog {

    /**
     *
     * @var integer @ORM\Column(name="id", type="integer", nullable=false)
     *      @ORM\Id
     *      @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var string @ORM\Column(name="log", type="text", nullable=true)
     */
    private $log;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="create")
     */
    private $createdDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="update")
     */
    private $updatedDate;

    /**
     *
     * @var \OrderMain @ORM\ManyToOne(targetEntity="OrderMain")
     *      @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="order_main_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     *      })
     */
    private $orderMain;

    public function __get($property) {
        if (\method_exists($this, "get" . \ucfirst($property))) {
            $method_name = "get" . \ucfirst($property);
            return $this->$method_name();
        } else {

            if (is_object($this->$property)) {
                // return $this->$property->id;
            }
            return $this->$property;
        }
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property        	
     * @param mixed $value        	
     *
     */
    public function __set($property, $value) {
        if (\method_exists($this, "set" . \ucfirst($property))) {
            $method_name = "set" . \ucfirst($property);

            $this->$method_name($value);
        } else
            $this->$property = $value;
    }

    public function __isset($name) {
        return true;
    }

}
