<?php

namespace OrderMain\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * OrderProductLineitem
 *
 * @ORM\Table(name="order_product_lineitem")
 * @ORM\Entity
 */
class OrderProductLineitem {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="title", type="string", length=255,
	 *      nullable=true)
	 */
	private $title;
	
	
	/**
	 *
	 * @var string @ORM\Column(name="value", type="string", length=255, nullable=true)
	 */
	private $value;
	
	/**
	 *
	 * @var integer @ORM\Column(name="weight", type="integer", nullable=true)
	 */
	private $weight;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	
	/**
	 *
	 * @var \OrderProduct @ORM\ManyToOne(targetEntity="OrderProduct")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="order_product_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $orderProduct;
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
	
	public function __isset($name){
		return true;
	}
}
