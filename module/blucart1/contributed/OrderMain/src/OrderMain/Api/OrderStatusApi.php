<?php

namespace OrderMain\Api;

use Common\Api\Api;

class OrderStatusApi extends Api {
	protected static $_entity = '\OrderMain\Entity\OrderStatus';
	public static function getOrderStatusById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getOrderStatuses() {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'orderStatus.id', 'orderStatusId' );
		$queryBuilder->addOrder ( 'orderStatus.weight', 'orderStatusWeight', 'desc' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderStatus' );
		return $queryBuilder->executeQuery ();
	}
	public static function getPendingOrderStatus() {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findBy ( array (
				'isPending' => 1 
		), array (
				'weight' => 'desc' 
		) );
	}
	public static function getSaleOrderStatus() {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findBy ( array (
				'isSale' => 1 
		), array (
				'weight' => 'desc' 
		) );
	}
	public static function getCancelledOrderStatus() {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findBy ( array (
				'isCancelled' => 1 
		), array (
				'weight' => 'desc' 
		) );
	}
}
