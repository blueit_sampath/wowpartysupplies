<?php

namespace OrderMain\Api;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use Common\Api\Api;

class OrderReportApi extends Api {
	protected static $_entity = '\OrderMain\Entity\OrderMain';
	public static function getTotalSales($startDate, $endDate) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		
		return $queryBuilder->executeSingleScalarQuery ( 'sum' );
	}
	public static function getTotalCompletedOrders($startDate, $endDate) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		
		return $queryBuilder->executeSingleScalarQuery ( 'sum' );
	}
	public static function getTotalAllOrders($startDate, $endDate) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		
		return $queryBuilder->executeSingleScalarQuery ( 'sum' );
	}
	public static function getOrderSalesByDay($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%D %M, %Y')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getOrderSalesByWeek($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%v Week,%x')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getOrderSalesByMonth($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%M,%Y')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getOrderSalesByYear($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%Y')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getAllOrderSalesByDay($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%D %M, %Y')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getAllOrderSalesByWeek($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%v,%x')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getAllOrderSalesByMonth($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%M,%Y')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getAllOrderSalesByYear($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%Y')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getPendingOrders($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( 'orderMain.id', 'orderMainId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$statuses = OrderStatusApi::getPendingOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $statuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		
		$queryBuilder->addOrder ( 'orderMain.id', 'orderMainId', 'desc' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getCancelledOrders($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( 'orderMain.id', 'orderMainId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$statuses = OrderStatusApi::getCancelledOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $statuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		
		$queryBuilder->addOrder ( 'orderMain.id', 'orderMainId', 'desc' );
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getSaleOrders($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( 'orderMain.id', 'orderMainId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		
		$statuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $statuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		
		$queryBuilder->addOrder ( 'orderMain.id', 'orderMainId', 'desc' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getBestCustomers($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "user.id", 'userId' );
		$queryBuilder->addColumn ( 'sum(orderMain.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderMain' );
		$joinItem = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$item->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem ( 'orderMain.user', 'user', 'inner join' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'userId', 'userId' );
		$queryBuilder->addOrder ( 'sumAmount', 'sumAmount', 'desc' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getBestProducts($startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "product.id", 'productId' );
		$queryBuilder->addColumn ( "product.title", 'productTitle' );
		$queryBuilder->addColumn ( 'sum(orderProduct.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'sum(orderProduct.amount-(orderProduct.costPrice * orderProduct.qty))', 'profit' );
		$queryBuilder->addColumn ( 'count(orderProduct.id)', 'countOrders' );
		$queryBuilder->addColumn ( 'sum(orderProduct.qty)', 'unitsSold' );
		
		$item = $queryBuilder->addFrom ( '\OrderMain\Entity\OrderProduct', 'orderProduct' );
		$joinItem = new QueryJoinItem ( 'orderProduct.orderMain', 'orderMain' );
		$item->addJoin ( $joinItem );
		$joinItem2 = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$joinItem->addJoin ( $joinItem2 );
		$joinItem = new QueryJoinItem ( 'orderProduct.product', 'product', 'inner join' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'productId', 'productId' );
		$queryBuilder->addOrder ( 'profit', 'profit', 'desc' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
}
