<?php

namespace OrderMain\Api;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use Common\Api\Api;

class OrderApi extends Api {

    protected static $_entity = '\OrderMain\Entity\OrderMain';
    protected static $_billingEntity = '\OrderMain\Entity\OrderBillingAddress';
    protected static $_commentEntity = '\OrderMain\Entity\OrderComment';
    protected static $_productEntity = '\OrderMain\Entity\OrderProduct';
    protected static $_productLineItemEntity = '\OrderMain\Entity\OrderProductLineitem';
    protected static $_orderLineItemEntity = '\OrderMain\Entity\OrderLineitem';
    protected static $_logEntity = '\OrderMain\Entity\OrderLog';

    public static function getOrderById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getOrders() {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('orderMain.id', 'orderMainId');
        $item = $queryBuilder->addFrom(static::$_entity, 'orderMain');
        return $queryBuilder->executeQuery();
    }

    public static function getOrderBillingAddressByOrderId($orderId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_billingEntity)->findOneBy(array(
                    'orderMain' => $orderId
        ));
    }

    public static function getOrderProductById($orderProductId) {
        $em = static::getEntityManager();
        return $em->find(static::$_productEntity, $orderProductId);
    }

    public static function getOrderProductsByOrderId($orderId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_productEntity)->findBy(array(
                    'orderMain' => $orderId
                        ), array(
                    'weight' => 'desc'
        ));
    }

    public static function getOrderProductLineItemsByOrderProductId($orderProductId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_productLineItemEntity)->findBy(array(
                    'orderProduct' => $orderProductId
                        ), array(
                    'weight' => 'desc'
        ));
    }

    public static function getOrderCommentById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_commentEntity, $id);
    }

    public static function getOrderCommentsByOrderId($orderId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_commentEntity)->findBy(array(
                    'orderMain' => $orderId
                        ), array(
                    'id' => 'desc'
        ));
    }

    public static function getFrontOrderCommentsByOrderId($orderId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_commentEntity)->findBy(array(
                    'orderMain' => $orderId,
                    'displayFrontend' => 1
                        ), array(
                    'id' => 'desc'
        ));
    }

    public static function getOrderLineItemByOrderId($orderId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_orderLineItemEntity)->findBy(array(
                    'orderMain' => $orderId
                        ), array(
                    'weight' => 'desc'
        ));
    }

    public static function getOrdersByUserId($userId, $page = 1, $perPage = PHP_INT_MAX) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('orderMain.id', 'orderMainId');
        $item = $queryBuilder->addFrom(static::$_entity, 'orderMain');
        $joinItem = new QueryJoinItem('orderMain.user', 'user');
        $item->addJoin($joinItem);

        $whereItem = $queryBuilder->addWhere('user.id', 'userId');
        $queryBuilder->addParameter('userId', $userId);

        $queryBuilder->addOrder('orderMain.id', 'orderMainId', 'desc');
        $queryBuilder->addGroup('orderMain.id', 'orderMainId');
        $results = $queryBuilder->executeQuery(($page - 1) * $perPage, $perPage);
        return $results;
    }

    public static function saveLog($orderId, $log) {
        $em = static::getEntityManager();
        $order = static::getOrderById($orderId);
       
        if ($order) {
            $orderLog = new static::$_logEntity();
            $orderLog->orderMain = $order;
            $orderLog->log = $log;
            $em->persist($orderLog);
            $em->flush();
            return $orderLog;
        }
        return false;
    }

    public static function getLogsByOrderId($orderId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_logEntity)->findBy(array(
                    'orderMain' => $orderId
                        ), array(
                    'id' => 'asc'
        ));
    }

}
