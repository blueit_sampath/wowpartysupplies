<?php

namespace OrderMain\Api;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use Common\Api\Api;

class OrderProductReportApi extends Api {
	protected static $_entity = '\OrderMain\Entity\OrderProduct';
	public static function getProductOrderSalesByDay($productId, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(orderMain.createdDate, '%D %M, %Y')", 'orderDate' );
		$queryBuilder->addColumn ( 'sum(orderProduct.amount)', 'sumAmount' );
		$queryBuilder->addColumn ( 'count(orderMain.id)', 'countOrders' );
		$queryBuilder->addColumn ( 'sum(orderProduct.costPrice * orderProduct.qty )', 'costPrice' );
		$queryBuilder->addColumn ( 'sum(orderProduct.amount - (orderProduct.costPrice * orderProduct.qty) )', 'profit' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'orderProduct' );
		$joinItem = new QueryJoinItem ( 'orderProduct.orderMain', 'orderMain' );
		$item->addJoin ( $joinItem );
		$joinItem2 = new QueryJoinItem ( 'orderMain.orderStatus', 'orderStatus' );
		$joinItem->addJoin ( $joinItem2 );
		$joinItem = new QueryJoinItem ( 'orderProduct.product', 'product', 'inner join' );
		$item->addJoin ( $joinItem );
		
		$saleStatuses = OrderStatusApi::getSaleOrderStatus ();
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhere ();
		$where->setLogic ( ' OR ' );
		foreach ( $saleStatuses as $status ) {
			$whereItem = new QueryWhereItem ( 'orderMain.orderStatus', 'orderStatus_' . $status->id, '=' );
			$where->addWhere ( $whereItem );
			
			$queryBuilder->setParameter ( 'orderStatus_' . $status->id, $status->id );
		}
		$wheres->addWhereGroup ( 'orderStatus', $where );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$queryBuilder->addWhere ( 'orderMain.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		if ($productId) {
			$queryBuilder->addWhere ( 'product.id', 'productId' );
			$queryBuilder->setParameter ( 'productId', $productId );
		}
		$queryBuilder->addGroup ( 'orderDate', 'orderDate' );
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
}
