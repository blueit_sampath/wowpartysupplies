<?php
		
		return array(
    'router' => array('routes' => array(
            'admin-order' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrder',
                        'action' => 'index'
                        )
                    )
                ),
            'account-order' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/order',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\UserOrder',
                        'action' => 'index'
                        )
                    )
                ),
            'account-order-comment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/order/comment[/ajax/:ajax][/:orderId][/:orderCommentId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\UserOrder',
                        'action' => 'comment',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderId' => '[0-9]+',
                        'orderCommentId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'account-order-commentdelete' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/order/comment/delete[/:orderId][/:orderCommentId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\UserOrder',
                        'action' => 'commentdelete'
                        ),
                    'constraints' => array(
                        'orderId' => '[0-9]+',
                        'orderCommentId' => '[0-9]+'
                        )
                    )
                ),
            'account-order-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/order/view[/:orderId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\UserOrder',
                        'action' => 'view'
                        ),
                    'constraints' => array('orderId' => '[0-9]+')
                    )
                ),
            'account-order-print' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/order/print[/:orderId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\UserOrder',
                        'action' => 'print'
                        ),
                    'constraints' => array('orderId' => '[0-9]+')
                    )
                ),
            'account-order-download' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/order/download[/:orderId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\UserOrder',
                        'action' => 'download'
                        ),
                    'constraints' => array('orderId' => '[0-9]+')
                    )
                ),
            'admin-order-comment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/comment[/ajax/:ajax][/:orderId][/:orderCommentId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrder',
                        'action' => 'comment',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderId' => '[0-9]+',
                        'orderCommentId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-commentdelete' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/comment/delete[/:orderId][/:orderCommentId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrder',
                        'action' => 'commentdelete'
                        ),
                    'constraints' => array(
                        'orderId' => '[0-9]+',
                        'orderCommentId' => '[0-9]+'
                        )
                    )
                ),
            'admin-order-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/view[/:orderId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrder',
                        'action' => 'view'
                        )
                    )
                ),
            'admin-order-view-status' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/view/status[/ajax/:ajax][/:orderId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrder',
                        'action' => 'status',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-view-sendinvoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/view/sendinvoice[/ajax/:ajax][/:orderId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrder',
                        'action' => 'sendinvoice',
                        'ajax' => 'false'
                        ),
                    'constraints' => array(
                        'orderId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-status' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/status',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderStatus',
                        'action' => 'index'
                        )
                    )
                ),
            'admin-order-status-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/status/add[/ajax/:ajax][/:orderStatusId]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderStatus',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-config' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/config',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderConfig',
                        'action' => 'add'
                        )
                    )
                ),
            'admin-order-config-general' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/config/general[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderConfigGeneral',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-config-invoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/config/invoice[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderConfigInvoice',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-config-status' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/config/status[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderConfigStatus',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-config-customer-notification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/config/customer/notification[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderConfigCustomerNotification',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-config-admin-notification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/config/admin/notification[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderConfigAdminNotification',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-config-comment-customer-notification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/config/comment/customer/notification[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderConfigCommentCustomerNotification',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-order-config-comment-admin-notification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/config/comment/admin/notification',
                    'defaults' => array(
                        'controller' => 'OrderMain\Controller\AdminOrderConfigCommentAdminNotification',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'orderStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                )
            )),
    'events' => array(
        'OrderMain\Service\Grid\AdminOrderStatus' => 'OrderMain\Service\Grid\AdminOrderStatus',
        'OrderMain\Service\Grid\AdminOrderPaymentStatus' => 'OrderMain\Service\Grid\AdminOrderPaymentStatus',
        'OrderMain\Service\Grid\AdminOrder' => 'OrderMain\Service\Grid\AdminOrder',
        'OrderMain\Service\Form\AdminOrderStatus' => 'OrderMain\Service\Form\AdminOrderStatus',
        'OrderMain\Service\Link\AdminOrderStatus' => 'OrderMain\Service\Link\AdminOrderStatus',
        'OrderMain\Service\Form\AdminOrderViewStatus' => 'OrderMain\Service\Form\AdminOrderViewStatus',
        'OrderMain\Service\Link\AdminOrderView' => 'OrderMain\Service\Link\AdminOrderView',
        'OrderMain\Service\Token\AdminOrderStatusChange' => 'OrderMain\Service\Token\AdminOrderStatusChange',
        'OrderMain\Service\Form\AdminOrderConfig' => 'OrderMain\Service\Form\AdminOrderConfig',
        'OrderMain\Service\Tab\AdminOrderConfig' => 'OrderMain\Service\Tab\AdminOrderConfig',
        'OrderMain\Service\Form\AdminOrderComment' => 'OrderMain\Service\Form\AdminOrderComment',
        'OrderMain\Service\Content\AdminDashboardTopSales' => 'OrderMain\Service\Content\AdminDashboardTopSales',
        'OrderMain\Service\Content\AdminDashboardTopOrders' => 'OrderMain\Service\Content\AdminDashboardTopOrders',
        'OrderMain\Service\Content\AdminDashboardTopAllOrders' => 'OrderMain\Service\Content\AdminDashboardTopAllOrders',
        'OrderMain\Service\Content\AdminDashboardMainContentOrder' => 'OrderMain\Service\Content\AdminDashboardMainContentOrder',
        'OrderMain\Service\Content\AdminOrderUserView' => 'OrderMain\Service\Content\AdminOrderUserView',
        'OrderMain\Service\Grid\AdminOrderUser' => 'OrderMain\Service\Grid\AdminOrderUser',
        'OrderMain\Service\Grid\AdminUser' => 'OrderMain\Service\Grid\AdminUser',
        'OrderMain\Service\Content\AdminOrderProductView' => 'OrderMain\Service\Content\AdminOrderProductView',
        'OrderMain\Service\Grid\AdminOrderProduct' => 'OrderMain\Service\Grid\AdminOrderProduct',
        'OrderMain\Service\Grid\AdminProduct' => 'OrderMain\Service\Grid\AdminProduct',
        'OrderMain\Service\Grid\AdminOrderDetailProduct' => 'OrderMain\Service\Grid\AdminOrderDetailProduct',
        'OrderMain\Service\Grid\AdminOrderDetailBillingAddress' => 'OrderMain\Service\Grid\AdminOrderDetailBillingAddress',
        'OrderMain\Service\Navigation\AdminOrderMenu' => 'OrderMain\Service\Navigation\AdminOrderMenu',
        'OrderMain\Service\Form\AdminOrderConfigGeneral' => 'OrderMain\Service\Form\AdminOrderConfigGeneral',
        'OrderMain\Service\Form\AdminOrderConfigInvoice' => 'OrderMain\Service\Form\AdminOrderConfigInvoice',
        'OrderMain\Service\Form\AdminOrderConfigStatus' => 'OrderMain\Service\Form\AdminOrderConfigStatus',
        'OrderMain\Service\Form\AdminOrderConfigCustomerNotification' => 'OrderMain\Service\Form\AdminOrderConfigCustomerNotification',
        'OrderMain\Service\Form\AdminOrderConfigAdminNotification' => 'OrderMain\Service\Form\AdminOrderConfigAdminNotification',
        'OrderMain\Service\Form\AdminOrderConfigCommentCustomerNotification' => 'OrderMain\Service\Form\AdminOrderConfigCommentCustomerNotification',
        'OrderMain\Service\Form\AdminOrderConfigCommentAdminNotification' => 'OrderMain\Service\Form\AdminOrderConfigCommentAdminNotification',
        'OrderMain\Service\Content\UserDashboardOrder' => 'OrderMain\Service\Content\UserDashboardOrder',
        'OrderMain\Service\Form\UserOrderComment' => 'OrderMain\Service\Form\UserOrderComment',
        'OrderMain\Service\Form\AdminOrderCommentMail' => 'OrderMain\Service\Form\AdminOrderCommentMail',
        'OrderMain\Service\Form\UserOrderCommentMail' => 'OrderMain\Service\Form\UserOrderCommentMail',
        'OrderMain\Service\Navigation\UserOrder' => 'OrderMain\Service\Navigation\UserOrder',
        'OrderMain\Service\Form\CheckoutOrderSave' => 'OrderMain\Service\Form\CheckoutOrderSave',
        'OrderMain\Service\Block\AdminOrderSimpleStat' => 'OrderMain\Service\Block\AdminOrderSimpleStat',
        'OrderMain\Service\Block\AdminOrderSimpleTotalStat' => 'OrderMain\Service\Block\AdminOrderSimpleTotalStat',
        'OrderMain\Service\Block\AdminDashboardSimpleOrderSaleStat' => 'OrderMain\Service\Block\AdminDashboardSimpleOrderSaleStat',
        'OrderMain\Service\Block\AdminDashboardOrderSaleGraph' => 'OrderMain\Service\Block\AdminDashboardOrderSaleGraph',
        'OrderMain\Service\Block\AdminDashboardOrderTables' => 'OrderMain\Service\Block\AdminDashboardOrderTables',
        'OrderMain\Service\Block\AdminUserViewOrder' => 'OrderMain\Service\Block\AdminUserViewOrder'
        ),
    'service_manager' => array(
        'invokables' => array(
            'OrderMain\Service\Grid\AdminOrderStatus' => 'OrderMain\Service\Grid\AdminOrderStatus',
            'OrderMain\Service\Grid\AdminOrderPaymentStatus' => 'OrderMain\Service\Grid\AdminOrderPaymentStatus',
            'OrderMain\Service\Grid\AdminOrder' => 'OrderMain\Service\Grid\AdminOrder',
            'OrderMain\Form\AdminOrderStatus\OrderStatusForm' => 'OrderMain\Form\AdminOrderStatus\OrderStatusForm',
            'OrderMain\Form\AdminOrderStatus\OrderStatusFilter' => 'OrderMain\Form\AdminOrderStatus\OrderStatusFilter',
            'OrderMain\Service\Form\AdminOrderStatus' => 'OrderMain\Service\Form\AdminOrderStatus',
            'OrderMain\Service\Link\AdminOrderStatus' => 'OrderMain\Service\Link\AdminOrderStatus',
            'OrderMain\Form\AdminOrderViewStatus\StatusForm' => 'OrderMain\Form\AdminOrderViewStatus\StatusForm',
            'OrderMain\Form\AdminOrderViewStatus\StatusFilter' => 'OrderMain\Form\AdminOrderViewStatus\StatusFilter',
            'OrderMain\Service\Form\AdminOrderViewStatus' => 'OrderMain\Service\Form\AdminOrderViewStatus',
            'OrderMain\Service\Link\AdminOrderView' => 'OrderMain\Service\Link\AdminOrderView',
            'OrderMain\Service\Token\AdminOrderStatusChange' => 'OrderMain\Service\Token\AdminOrderStatusChange',
            'OrderMain\Form\AdminOrderConfig\OrderConfigForm' => 'OrderMain\Form\AdminOrderConfig\OrderConfigForm',
            'OrderMain\Form\AdminOrderConfig\OrderConfigFilter' => 'OrderMain\Form\AdminOrderConfig\OrderConfigFilter',
            'OrderMain\Service\Form\AdminOrderConfig' => 'OrderMain\Service\Form\AdminOrderConfig',
            'OrderMain\Service\Tab\AdminOrderConfig' => 'OrderMain\Service\Tab\AdminOrderConfig',
            'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailForm' => 'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailForm',
            'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailFilter' => 'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailFilter',
            'OrderMain\Form\AdminOrderComment\CommentForm' => 'OrderMain\Form\AdminOrderComment\CommentForm',
            'OrderMain\Form\AdminOrderComment\CommentFilter' => 'OrderMain\Form\AdminOrderComment\CommentFilter',
            'OrderMain\Service\Form\AdminOrderComment' => 'OrderMain\Service\Form\AdminOrderComment',
            'OrderMain\Service\Content\AdminDashboardTopSales' => 'OrderMain\Service\Content\AdminDashboardTopSales',
            'OrderMain\Service\Content\AdminDashboardTopOrders' => 'OrderMain\Service\Content\AdminDashboardTopOrders',
            'OrderMain\Service\Content\AdminDashboardTopAllOrders' => 'OrderMain\Service\Content\AdminDashboardTopAllOrders',
            'OrderMain\Service\Content\AdminDashboardMainContentOrder' => 'OrderMain\Service\Content\AdminDashboardMainContentOrder',
            'OrderMain\Service\Content\AdminOrderUserView' => 'OrderMain\Service\Content\AdminOrderUserView',
            'OrderMain\Service\Grid\AdminOrderUser' => 'OrderMain\Service\Grid\AdminOrderUser',
            'OrderMain\Service\Grid\AdminUser' => 'OrderMain\Service\Grid\AdminUser',
            'OrderMain\Service\Content\AdminOrderProductView' => 'OrderMain\Service\Content\AdminOrderProductView',
            'OrderMain\Service\Grid\AdminOrderProduct' => 'OrderMain\Service\Grid\AdminOrderProduct',
            'OrderMain\Service\Grid\AdminProduct' => 'OrderMain\Service\Grid\AdminProduct',
            'OrderMain\Service\Grid\AdminOrderDetailProduct' => 'OrderMain\Service\Grid\AdminOrderDetailProduct',
            'OrderMain\Service\Grid\AdminOrderDetailBillingAddress' => 'OrderMain\Service\Grid\AdminOrderDetailBillingAddress',
            'OrderMain\Form\AdminOrder\OrderSearchForm' => 'OrderMain\Form\AdminOrder\OrderSearchForm',
            'OrderMain\Service\Navigation\AdminOrderMenu' => 'OrderMain\Service\Navigation\AdminOrderMenu',
            'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralForm' => 'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralForm',
            'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralFilter' => 'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralFilter',
            'OrderMain\Service\Form\AdminOrderConfigGeneral' => 'OrderMain\Service\Form\AdminOrderConfigGeneral',
            'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceForm' => 'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceForm',
            'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceFilter' => 'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceFilter',
            'OrderMain\Service\Form\AdminOrderConfigInvoice' => 'OrderMain\Service\Form\AdminOrderConfigInvoice',
            'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusForm' => 'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusForm',
            'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusFilter' => 'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusFilter',
            'OrderMain\Service\Form\AdminOrderConfigStatus' => 'OrderMain\Service\Form\AdminOrderConfigStatus',
            'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationForm' => 'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationForm',
            'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationFilter' => 'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationFilter',
            'OrderMain\Service\Form\AdminOrderConfigCustomerNotification' => 'OrderMain\Service\Form\AdminOrderConfigCustomerNotification',
            'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationForm' => 'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationForm',
            'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationFilter' => 'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationFilter',
            'OrderMain\Service\Form\AdminOrderConfigAdminNotification' => 'OrderMain\Service\Form\AdminOrderConfigAdminNotification',
            'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationForm' => 'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationForm',
            'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationFilter' => 'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationFilter',
            'OrderMain\Service\Form\AdminOrderConfigCommentCustomerNotification' => 'OrderMain\Service\Form\AdminOrderConfigCommentCustomerNotification',
            'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationForm' => 'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationForm',
            'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationFilter' => 'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationFilter',
            'OrderMain\Service\Form\AdminOrderConfigCommentAdminNotification' => 'OrderMain\Service\Form\AdminOrderConfigCommentAdminNotification',
            'OrderMain\Service\Content\UserDashboardOrder' => 'OrderMain\Service\Content\UserDashboardOrder',
            'OrderMain\Service\Form\UserOrderComment' => 'OrderMain\Service\Form\UserOrderComment',
            'OrderMain\Form\UserOrderComment\CommentForm' => 'OrderMain\Form\UserOrderComment\CommentForm',
            'OrderMain\Form\UserOrderComment\CommentFilter' => 'OrderMain\Form\UserOrderComment\CommentFilter',
            'OrderMain\Service\Form\AdminOrderCommentMail' => 'OrderMain\Service\Form\AdminOrderCommentMail',
            'OrderMain\Service\Form\UserOrderCommentMail' => 'OrderMain\Service\Form\UserOrderCommentMail',
            'OrderMain\Service\Navigation\UserOrder' => 'OrderMain\Service\Navigation\UserOrder',
            'OrderMain\Service\Form\CheckoutOrderSave' => 'OrderMain\Service\Form\CheckoutOrderSave',
            'OrderMain\Service\Block\AdminOrderSimpleStat' => 'OrderMain\Service\Block\AdminOrderSimpleStat',
            'OrderMain\Service\Block\AdminOrderSimpleTotalStat' => 'OrderMain\Service\Block\AdminOrderSimpleTotalStat',
            'OrderMain\Service\Block\AdminDashboardSimpleOrderSaleStat' => 'OrderMain\Service\Block\AdminDashboardSimpleOrderSaleStat',
            'OrderMain\Service\Block\AdminDashboardOrderSaleGraph' => 'OrderMain\Service\Block\AdminDashboardOrderSaleGraph',
            'OrderMain\Service\Block\AdminDashboardOrderTables' => 'OrderMain\Service\Block\AdminDashboardOrderTables',
            'OrderMain\Service\Block\AdminUserViewOrder' => 'OrderMain\Service\Block\AdminUserViewOrder'
            ),
        'factories' => array(
            'OrderMain\Form\AdminOrderStatus\OrderStatusFactory' => 'OrderMain\Form\AdminOrderStatus\OrderStatusFactory',
            'OrderMain\Form\AdminOrderViewStatus\StatusFactory' => 'OrderMain\Form\AdminOrderViewStatus\StatusFactory',
            'OrderMain\Form\AdminOrderConfig\OrderConfigFactory' => 'OrderMain\Form\AdminOrderConfig\OrderConfigFactory',
            'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailFactory' => 'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailFactory',
            'OrderMain\Form\AdminOrderComment\CommentFactory' => 'OrderMain\Form\AdminOrderComment\CommentFactory',
            'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralFactory' => 'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralFactory',
            'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceFactory' => 'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceFactory',
            'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusFactory' => 'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusFactory',
            'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationFactory' => 'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationFactory',
            'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationFactory' => 'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationFactory',
            'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationFactory' => 'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationFactory',
            'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationFactory' => 'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationFactory',
            'OrderMain\Form\UserOrderComment\CommentFactory' => 'OrderMain\Form\UserOrderComment\CommentFactory'
            )
        ),
    'controllers' => array('invokables' => array(
            'OrderMain\Controller\AdminOrderStatus' => 'OrderMain\Controller\AdminOrderStatusController',
            'OrderMain\Controller\AdminOrderPaymentStatus' => 'OrderMain\Controller\AdminOrderPaymentStatusController',
            'OrderMain\Controller\AdminOrder' => 'OrderMain\Controller\AdminOrderController',
            'OrderMain\Controller\AdminOrderConfig' => 'OrderMain\Controller\AdminOrderConfigController',
            'OrderMain\Controller\AdminOrderConfigGeneral' => 'OrderMain\Controller\AdminOrderConfigGeneralController',
            'OrderMain\Controller\AdminOrderConfigInvoice' => 'OrderMain\Controller\AdminOrderConfigInvoiceController',
            'OrderMain\Controller\AdminOrderConfigStatus' => 'OrderMain\Controller\AdminOrderConfigStatusController',
            'OrderMain\Controller\AdminOrderConfigCustomerNotification' => 'OrderMain\Controller\AdminOrderConfigCustomerNotificationController',
            'OrderMain\Controller\AdminOrderConfigAdminNotification' => 'OrderMain\Controller\AdminOrderConfigAdminNotificationController',
            'OrderMain\Controller\AdminOrderConfigCommentCustomerNotification' => 'OrderMain\Controller\AdminOrderConfigCommentCustomerNotificationController',
            'OrderMain\Controller\AdminOrderConfigCommentAdminNotification' => 'OrderMain\Controller\AdminOrderConfigCommentAdminNotificationController',
            'OrderMain\Controller\UserOrder' => 'OrderMain\Controller\UserOrderController'
            )),
    'view_manager' => array('template_path_stack' => array('OrderMain' => __DIR__.'/../view')),
    'doctrine' => array('driver' => array(
            'OrderMain_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__.'/../src/OrderMain/Entity')
                ),
            'orm_default' => array('drivers' => array('OrderMain\Entity' => 'OrderMain_driver'))
            )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('OrderMain' => __DIR__.'/../public')))
    );
