<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
  'OrderMain\Api\OrderReportApi'                                                                             => __DIR__ . '/src/OrderMain/Api/OrderReportApi.php',
  'OrderMain\Api\OrderProductReportApi'                                                                      => __DIR__ . '/src/OrderMain/Api/OrderProductReportApi.php',
  'OrderMain\Api\OrderStatusApi'                                                                             => __DIR__ . '/src/OrderMain/Api/OrderStatusApi.php',
  'OrderMain\Api\OrderApi'                                                                                   => __DIR__ . '/src/OrderMain/Api/OrderApi.php',
  'OrderMain\Entity\OrderProduct'                                                                            => __DIR__ . '/src/OrderMain/Entity/OrderProduct.php',
  'OrderMain\Entity\OrderComment'                                                                            => __DIR__ . '/src/OrderMain/Entity/OrderComment.php',
  'OrderMain\Entity\OrderStatus'                                                                             => __DIR__ . '/src/OrderMain/Entity/OrderStatus.php',
  'OrderMain\Entity\OrderProductLineitem'                                                                    => __DIR__ . '/src/OrderMain/Entity/OrderProductLineitem.php',
  'OrderMain\Entity\OrderLineitem'                                                                           => __DIR__ . '/src/OrderMain/Entity/OrderLineitem.php',
  'OrderMain\Entity\OrderMain'                                                                               => __DIR__ . '/src/OrderMain/Entity/OrderMain.php',
  'OrderMain\Entity\OrderBillingAddress'                                                                     => __DIR__ . '/src/OrderMain/Entity/OrderBillingAddress.php',
  'OrderMain\Service\Content\AdminOrderUserView'                                                             => __DIR__ . '/src/OrderMain/Service/Content/AdminOrderUserView.php',
  'OrderMain\Service\Content\UserDashboardOrder'                                                             => __DIR__ . '/src/OrderMain/Service/Content/UserDashboardOrder.php',
  'OrderMain\Service\Content\AdminDashboardTopAllOrders'                                                     => __DIR__ . '/src/OrderMain/Service/Content/AdminDashboardTopAllOrders.php',
  'OrderMain\Service\Content\AdminDashboardTopSales'                                                         => __DIR__ . '/src/OrderMain/Service/Content/AdminDashboardTopSales.php',
  'OrderMain\Service\Content\AdminOrderProductView'                                                          => __DIR__ . '/src/OrderMain/Service/Content/AdminOrderProductView.php',
  'OrderMain\Service\Content\AdminDashboardTopOrders'                                                        => __DIR__ . '/src/OrderMain/Service/Content/AdminDashboardTopOrders.php',
  'OrderMain\Service\Content\AdminDashboardMainContentOrder'                                                 => __DIR__ . '/src/OrderMain/Service/Content/AdminDashboardMainContentOrder.php',
  'OrderMain\Service\Navigation\AdminOrderMenu'                                                              => __DIR__ . '/src/OrderMain/Service/Navigation/AdminOrderMenu.php',
  'OrderMain\Service\Navigation\UserOrder'                                                                   => __DIR__ . '/src/OrderMain/Service/Navigation/UserOrder.php',
  'OrderMain\Service\Grid\AdminOrderDetailBillingAddress'                                                    => __DIR__ . '/src/OrderMain/Service/Grid/AdminOrderDetailBillingAddress.php',
  'OrderMain\Service\Grid\AdminProduct'                                                                      => __DIR__ . '/src/OrderMain/Service/Grid/AdminProduct.php',
  'OrderMain\Service\Grid\AdminUser'                                                                         => __DIR__ . '/src/OrderMain/Service/Grid/AdminUser.php',
  'OrderMain\Service\Grid\AdminOrderUser'                                                                    => __DIR__ . '/src/OrderMain/Service/Grid/AdminOrderUser.php',
  'OrderMain\Service\Grid\AdminOrderStatus'                                                                  => __DIR__ . '/src/OrderMain/Service/Grid/AdminOrderStatus.php',
  'OrderMain\Service\Grid\AdminOrderPaymentStatus'                                                           => __DIR__ . '/src/OrderMain/Service/Grid/AdminOrderPaymentStatus.php',
  'OrderMain\Service\Grid\AdminOrderDetailProduct'                                                           => __DIR__ . '/src/OrderMain/Service/Grid/AdminOrderDetailProduct.php',
  'OrderMain\Service\Grid\AdminOrderProduct'                                                                 => __DIR__ . '/src/OrderMain/Service/Grid/AdminOrderProduct.php',
  'OrderMain\Service\Grid\AdminOrder'                                                                        => __DIR__ . '/src/OrderMain/Service/Grid/AdminOrder.php',
  'OrderMain\Service\Block\AdminUserViewOrder'                                                               => __DIR__ . '/src/OrderMain/Service/Block/AdminUserViewOrder.php',
  'OrderMain\Service\Block\AdminDashboardOrderTables'                                                        => __DIR__ . '/src/OrderMain/Service/Block/AdminDashboardOrderTables.php',
  'OrderMain\Service\Block\AdminDashboardSimpleOrderSaleStat'                                                => __DIR__ . '/src/OrderMain/Service/Block/AdminDashboardSimpleOrderSaleStat.php',
  'OrderMain\Service\Block\AdminOrderSimpleStat'                                                             => __DIR__ . '/src/OrderMain/Service/Block/AdminOrderSimpleStat.php',
  'OrderMain\Service\Block\AdminDashboardOrderSaleGraph'                                                     => __DIR__ . '/src/OrderMain/Service/Block/AdminDashboardOrderSaleGraph.php',
  'OrderMain\Service\Block\AdminOrderSimpleTotalStat'                                                        => __DIR__ . '/src/OrderMain/Service/Block/AdminOrderSimpleTotalStat.php',
  'OrderMain\Service\Link\AdminOrderStatus'                                                                  => __DIR__ . '/src/OrderMain/Service/Link/AdminOrderStatus.php',
  'OrderMain\Service\Link\AdminOrderView'                                                                    => __DIR__ . '/src/OrderMain/Service/Link/AdminOrderView.php',
  'OrderMain\Service\Form\AdminOrderConfigStatus'                                                            => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderConfigStatus.php',
  'OrderMain\Service\Form\AdminOrderConfig'                                                                  => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderConfig.php',
  'OrderMain\Service\Form\CheckoutOrderSave'                                                                 => __DIR__ . '/src/OrderMain/Service/Form/CheckoutOrderSave.php',
  'OrderMain\Service\Form\AdminOrderConfigCommentAdminNotification'                                          => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderConfigCommentAdminNotification.php',
  'OrderMain\Service\Form\UserOrderCommentMail'                                                              => __DIR__ . '/src/OrderMain/Service/Form/UserOrderCommentMail.php',
  'OrderMain\Service\Form\AdminOrderConfigCommentCustomerNotification'                                       => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderConfigCommentCustomerNotification.php',
  'OrderMain\Service\Form\AdminOrderConfigCustomerNotification'                                              => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderConfigCustomerNotification.php',
  'OrderMain\Service\Form\AdminOrderStatus'                                                                  => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderStatus.php',
  'OrderMain\Service\Form\AdminOrderComment'                                                                 => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderComment.php',
  'OrderMain\Service\Form\AdminOrderConfigInvoice'                                                           => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderConfigInvoice.php',
  'OrderMain\Service\Form\UserOrderComment'                                                                  => __DIR__ . '/src/OrderMain/Service/Form/UserOrderComment.php',
  'OrderMain\Service\Form\AdminOrderViewStatus'                                                              => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderViewStatus.php',
  'OrderMain\Service\Form\AdminOrderCommentMail'                                                             => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderCommentMail.php',
  'OrderMain\Service\Form\AdminOrderConfigGeneral'                                                           => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderConfigGeneral.php',
  'OrderMain\Service\Form\AdminOrderConfigAdminNotification'                                                 => __DIR__ . '/src/OrderMain/Service/Form/AdminOrderConfigAdminNotification.php',
  'OrderMain\Service\Tab\AdminOrderConfig'                                                                   => __DIR__ . '/src/OrderMain/Service/Tab/AdminOrderConfig.php',
  'OrderMain\Service\Token\AdminOrderStatusChange'                                                           => __DIR__ . '/src/OrderMain/Service/Token/AdminOrderStatusChange.php',
  'OrderMain\Controller\AdminOrderConfigInvoiceController'                                                   => __DIR__ . '/src/OrderMain/Controller/AdminOrderConfigInvoiceController.php',
  'OrderMain\Controller\AdminOrderConfigCommentAdminNotificationController'                                  => __DIR__ . '/src/OrderMain/Controller/AdminOrderConfigCommentAdminNotificationController.php',
  'OrderMain\Controller\AdminOrderConfigCommentCustomerNotificationController'                               => __DIR__ . '/src/OrderMain/Controller/AdminOrderConfigCommentCustomerNotificationController.php',
  'OrderMain\Controller\AdminOrderConfigGeneralController'                                                   => __DIR__ . '/src/OrderMain/Controller/AdminOrderConfigGeneralController.php',
  'OrderMain\Controller\AdminOrderConfigCustomerNotificationController'                                      => __DIR__ . '/src/OrderMain/Controller/AdminOrderConfigCustomerNotificationController.php',
  'OrderMain\Controller\UserOrderController'                                                                 => __DIR__ . '/src/OrderMain/Controller/UserOrderController.php',
  'OrderMain\Controller\AdminOrderConfigController'                                                          => __DIR__ . '/src/OrderMain/Controller/AdminOrderConfigController.php',
  'OrderMain\Controller\AdminOrderConfigAdminNotificationController'                                         => __DIR__ . '/src/OrderMain/Controller/AdminOrderConfigAdminNotificationController.php',
  'OrderMain\Controller\AdminOrderConfigStatusController'                                                    => __DIR__ . '/src/OrderMain/Controller/AdminOrderConfigStatusController.php',
  'OrderMain\Controller\AdminOrderController'                                                                => __DIR__ . '/src/OrderMain/Controller/AdminOrderController.php',
  'OrderMain\Controller\AdminOrderStatusController'                                                          => __DIR__ . '/src/OrderMain/Controller/AdminOrderStatusController.php',
  'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusForm'                                              => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigStatus/OrderConfigStatusForm.php',
  'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusFactory'                                           => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigStatus/OrderConfigStatusFactory.php',
  'OrderMain\Form\AdminOrderConfigStatus\OrderConfigStatusFilter'                                            => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigStatus/OrderConfigStatusFilter.php',
  'OrderMain\Form\UserOrderComment\CommentForm'                                                              => __DIR__ . '/src/OrderMain/Form/UserOrderComment/CommentForm.php',
  'OrderMain\Form\UserOrderComment\CommentFactory'                                                           => __DIR__ . '/src/OrderMain/Form/UserOrderComment/CommentFactory.php',
  'OrderMain\Form\UserOrderComment\CommentFilter'                                                            => __DIR__ . '/src/OrderMain/Form/UserOrderComment/CommentFilter.php',
  'OrderMain\Form\AdminOrderComment\CommentForm'                                                             => __DIR__ . '/src/OrderMain/Form/AdminOrderComment/CommentForm.php',
  'OrderMain\Form\AdminOrderComment\CommentFactory'                                                          => __DIR__ . '/src/OrderMain/Form/AdminOrderComment/CommentFactory.php',
  'OrderMain\Form\AdminOrderComment\CommentFilter'                                                           => __DIR__ . '/src/OrderMain/Form/AdminOrderComment/CommentFilter.php',
  'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationFilter'                      => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigAdminNotification/OrderConfigAdminNotificationFilter.php',
  'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationFactory'                     => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigAdminNotification/OrderConfigAdminNotificationFactory.php',
  'OrderMain\Form\AdminOrderConfigAdminNotification\OrderConfigAdminNotificationForm'                        => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigAdminNotification/OrderConfigAdminNotificationForm.php',
  'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationForm'          => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCommentAdminNotification/OrderConfigCommentAdminNotificationForm.php',
  'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationFactory'       => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCommentAdminNotification/OrderConfigCommentAdminNotificationFactory.php',
  'OrderMain\Form\AdminOrderConfigCommentAdminNotification\OrderConfigCommentAdminNotificationFilter'        => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCommentAdminNotification/OrderConfigCommentAdminNotificationFilter.php',
  'OrderMain\Form\AdminOrderConfig\OrderConfigFilter'                                                        => __DIR__ . '/src/OrderMain/Form/AdminOrderConfig/OrderConfigFilter.php',
  'OrderMain\Form\AdminOrderConfig\OrderConfigForm'                                                          => __DIR__ . '/src/OrderMain/Form/AdminOrderConfig/OrderConfigForm.php',
  'OrderMain\Form\AdminOrderConfig\OrderConfigFactory'                                                       => __DIR__ . '/src/OrderMain/Form/AdminOrderConfig/OrderConfigFactory.php',
  'OrderMain\Form\AdminOrderViewStatus\StatusForm'                                                           => __DIR__ . '/src/OrderMain/Form/AdminOrderViewStatus/StatusForm.php',
  'OrderMain\Form\AdminOrderViewStatus\StatusFactory'                                                        => __DIR__ . '/src/OrderMain/Form/AdminOrderViewStatus/StatusFactory.php',
  'OrderMain\Form\AdminOrderViewStatus\StatusFilter'                                                         => __DIR__ . '/src/OrderMain/Form/AdminOrderViewStatus/StatusFilter.php',
  'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationFilter'                => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCustomerNotification/OrderConfigCustomerNotificationFilter.php',
  'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationFactory'               => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCustomerNotification/OrderConfigCustomerNotificationFactory.php',
  'OrderMain\Form\AdminOrderConfigCustomerNotification\OrderConfigCustomerNotificationForm'                  => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCustomerNotification/OrderConfigCustomerNotificationForm.php',
  'OrderMain\Form\AdminOrderStatus\OrderStatusForm'                                                          => __DIR__ . '/src/OrderMain/Form/AdminOrderStatus/OrderStatusForm.php',
  'OrderMain\Form\AdminOrderStatus\OrderStatusFilter'                                                        => __DIR__ . '/src/OrderMain/Form/AdminOrderStatus/OrderStatusFilter.php',
  'OrderMain\Form\AdminOrderStatus\OrderStatusFactory'                                                       => __DIR__ . '/src/OrderMain/Form/AdminOrderStatus/OrderStatusFactory.php',
  'OrderMain\Form\AdminOrderStatus\MailFieldset'                                                             => __DIR__ . '/src/OrderMain/Form/AdminOrderStatus/MailFieldset.php',
  'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationFactory' => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCommentCustomerNotification/OrderConfigCommentCustomerNotificationFactory.php',
  'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationFilter'  => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCommentCustomerNotification/OrderConfigCommentCustomerNotificationFilter.php',
  'OrderMain\Form\AdminOrderConfigCommentCustomerNotification\OrderConfigCommentCustomerNotificationForm'    => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigCommentCustomerNotification/OrderConfigCommentCustomerNotificationForm.php',
  'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceFilter'                                          => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigInvoice/OrderConfigInvoiceFilter.php',
  'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceFactory'                                         => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigInvoice/OrderConfigInvoiceFactory.php',
  'OrderMain\Form\AdminOrderConfigInvoice\OrderConfigInvoiceForm'                                            => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigInvoice/OrderConfigInvoiceForm.php',
  'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailForm'                                        => __DIR__ . '/src/OrderMain/Form/AdminOrderViewStatusEmail/OrderViewStatusEmailForm.php',
  'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailFactory'                                     => __DIR__ . '/src/OrderMain/Form/AdminOrderViewStatusEmail/OrderViewStatusEmailFactory.php',
  'OrderMain\Form\AdminOrderViewStatusEmail\OrderViewStatusEmailFilter'                                      => __DIR__ . '/src/OrderMain/Form/AdminOrderViewStatusEmail/OrderViewStatusEmailFilter.php',
  'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralForm'                                            => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigGeneral/OrderConfigGeneralForm.php',
  'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralFactory'                                         => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigGeneral/OrderConfigGeneralFactory.php',
  'OrderMain\Form\AdminOrderConfigGeneral\OrderConfigGeneralFilter'                                          => __DIR__ . '/src/OrderMain/Form/AdminOrderConfigGeneral/OrderConfigGeneralFilter.php',
  'OrderMain\Form\AdminOrder\OrderSearchForm'                                                                => __DIR__ . '/src/OrderMain/Form/AdminOrder/OrderSearchForm.php',
  'OrderMain\Module'                                                                                         => __DIR__ . '/Module.php',
);
