<?php
return array(
    'router' => array(
        'routes' => array(
            'sagepaypayment-send' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sagepaypayment/send[/:orderId]',
                    'defaults' => array(
                        'controller' => 'SagepayPayment\Controller\Index',
                        'action' => 'send'
                    )
                )
            ),
            'sagepaypayment-land' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sagepaypayment/land',
                    'defaults' => array(
                        'controller' => 'SagepayPayment\Controller\Index',
                        'action' => 'land'
                    )
                )
            ),
            'sagepaypayment-error' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sagepaypayment/error',
                    'defaults' => array(
                        'controller' => 'SagepayPayment\Controller\Index',
                        'action' => 'error'
                    )
                )
            ),
            'admin-sagepaypayment-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/sagepaypayment/setting[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'SagepayPayment\Controller\AdminSagepayPayment',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            )
        )
    ),
    'events' => array(
        'SagepayPayment\Service\Payment\SagepayPaymentEvent' => 'SagepayPayment\Service\Payment\SagepayPaymentEvent',
        'SagepayPayment\Service\Form\AdminSagepayPaymentSetting' => 'SagepayPayment\Service\Form\AdminSagepayPaymentSetting',
        'SagepayPayment\Service\Tab\AdminPaymentSetting' => 'SagepayPayment\Service\Tab\AdminPaymentSetting'
    ),
    'service_manager' => array(
        'invokables' => array(
            'SagepayPayment\Service\Payment\SagepayPaymentEvent' => 'SagepayPayment\Service\Payment\SagepayPaymentEvent',
            'SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigForm' => 'SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigForm',
            'SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigFilter' => 'SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigFilter',
            'SagepayPayment\Service\Form\AdminSagepayPaymentSetting' => 'SagepayPayment\Service\Form\AdminSagepayPaymentSetting',
            'SagepayPayment\Service\Tab\AdminPaymentSetting' => 'SagepayPayment\Service\Tab\AdminPaymentSetting'
        ),
        'factories' => array(
            'SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigFactory' => 'SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'SagepayPayment\Controller\Index' => 'SagepayPayment\Controller\IndexController',
            'SagepayPayment\Controller\AdminSagepayPayment' => 'SagepayPayment\Controller\AdminSagepayPaymentController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'SagepayPayment' => __DIR__ . '/../view'
        )
    ),
    
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'SagepayPayment' => __DIR__ . '/../public'
            )
        )
    )
);
