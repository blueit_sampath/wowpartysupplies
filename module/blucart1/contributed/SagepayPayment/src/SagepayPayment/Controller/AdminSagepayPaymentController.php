<?php
namespace SagepayPayment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminSagepayPaymentController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-sagepaypayment-setting');
    }
}