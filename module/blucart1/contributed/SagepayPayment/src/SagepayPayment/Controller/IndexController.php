<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace SagepayPayment\Controller;

use Core\Functions;
use Payment\Option\PaymentController;
use SagepayPayment\Option\Sagepay;
use Zend\View\Model\ViewModel;

class IndexController extends PaymentController {

    const GATEWAY_NAME = 'sagepayPayment';

    public function sendAction() {
        if (!$this->validatePath()) {
            return $this->redirect()->toRoute('checkout');
        }

        $orderId = Functions::fromRoute('orderId');

        $viewModel = new ViewModel(array(
            'orderId' => $orderId
        ));
        return $viewModel;
    }

    public function landAction() {
        $sagePay = $this->getSagePay();

        $crypt = $this->getRequest()->getQuery('crypt');
        if (!$crypt) {
            return $this->redirect()->toRoute('home');
        }
        $strDecoded = $sagePay->decodeAndDecrypt($crypt);
        $values = $sagePay->getToken($strDecoded);
        if (is_array($values)) {

            $orderId = $values['VendorTxCode'];
            $transactionId = $values['VPSTxId'];
            $cart = $this->getCart();

            $viewModel = $this->complete($orderId, $transactionId);
            $cart->clearCart();

            return $viewModel;
        }
        return $this->redirect()->toRoute('home');
    }

    /**
     *
     * @return \SagepayPayment\Option\Sagepay
     */
    public function getSagePay() {
        $sagePay = new Sagepay();
        return $sagePay;
    }

    public function errorAction() {
        $sagepay = $this->getSagePay();
        $strDecoded = $sagepay->decodeAndDecrypt($this->getRequest()
                        ->getQuery('crypt'));
        $values = $sagepay->getToken($strDecoded);
        $orderId = $values['VendorTxCode'];
        $transactionId = $values['VPSTxId'];
        $message = $values['StatusDetail'];

        $viewModel = $this->error($orderId, $transactionId, $message);
        return $viewModel;
    }

}
