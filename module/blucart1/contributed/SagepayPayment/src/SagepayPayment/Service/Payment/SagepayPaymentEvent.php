<?php
namespace SagepayPayment\Service\Payment;

use Core\Functions;
use Checkout\Option\CartContainer;
use Payment\Option\AbstractPaymentEvent;
use Config\Api\ConfigApi;

class SagepayPaymentEvent extends AbstractPaymentEvent
{

    public function getName()
    {
        return 'sagepayPayment';
    }

    public function getTitle()
    {
        $title = ConfigApi::getConfigByKey('PAYMENT_SAGEPAY_TITLE');
        if (! $title) {
            $title = 'SagePay';
        }
        return $title;
    }

    public function getRouteName()
    {
        return 'sagepaypayment-send';
    }
}
