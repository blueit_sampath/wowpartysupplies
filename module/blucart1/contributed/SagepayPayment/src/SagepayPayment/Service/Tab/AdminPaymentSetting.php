<?php
namespace SagepayPayment\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminPaymentSetting extends AbstractTabEvent
{

    public function getEventName()
    {
        return 'adminPaymentSetting';
    }

    public function tab()
    {
        $tabContainer = $this->getTabContainer();
        $url = Functions::getUrlPlugin();
        
        $u = $url('admin-sagepaypayment-setting');
        $tabContainer->add('admin-sagepaypayment-setting', 'Sagepay Payment', $u, 500);
        
        return $this;
    }
}

