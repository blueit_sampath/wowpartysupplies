<?php
namespace SagepayPayment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminSagepayPaymentSetting extends AdminConfig
{

    protected $_columnKeys = array(
        'PAYMENT_SAGEPAY_TITLE',
        'PAYMENT_SAGEPAY_ENV',
        'PAYMENT_SAGEPAY_VENDOR_NAME',
        'PAYMENT_SAGEPAY_VENDOR_PASSWORD',
        'PAYMENT_SAGEPAY_SENDEMAIL',
        'PAYMENT_SAGEPAY_TRANSACTION_TYPE',
        'PAYMENT_SAGEPAY_VENDOR_EMAIL',
        'PAYMENT_SAGEPAY_ENCRYPTION_TYPE',
        'PAYMENT_SAGEPAY_PROTOCOL',
        'PAYMENT_SAGEPAY_PARTNERID',
        'PAYMENT_SAGEPAY_DESCRIPTION'
    );

    public function getFormName()
    {
        return 'adminSagepayPaymentConfig';
    }

    public function getPriority()
    {
        return 1000;
    }
}
