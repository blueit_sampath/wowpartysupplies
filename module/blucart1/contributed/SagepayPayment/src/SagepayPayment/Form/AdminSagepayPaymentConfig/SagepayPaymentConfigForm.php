<?php
namespace SagepayPayment\Form\AdminSagepayPaymentConfig;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class SagepayPaymentConfigForm extends Form

{

    public function init()
    {
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_TITLE',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Sagepay Payment Label'
            )
        ), array(
            'priority' => 1000
        ));
        
        $select = new Select('PAYMENT_SAGEPAY_ENV');
        $select->setValueOptions(array(
            'TEST' => 'Test',
            'LIVE' => 'Live',
            'SIMULATOR' => 'Simulator'
        ));
        $select->setLabel('Enivornment');
        $this->add($select, array(
            'priority' => 990
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_VENDOR_NAME',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Vendor Name'
            )
        ), array(
            'priority' => 980
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_VENDOR_PASSWORD',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Vendor Password'
            )
        ), array(
            'priority' => 970
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_VENDOR_EMAIL',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Vendor Email'
            )
        ), array(
            'priority' => 960
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_PARTNERID',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Partner Id'
            )
        ), array(
            'priority' => 950
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_ENCRYPTION_TYPE',
            'attributes' => array(
                'type' => 'text',
                'value' => 'AES'
            ),
            'options' => array(
                'label' => 'Encryption Type'
            )
        ), array(
            'priority' => 940
        ));
        
        $select = new Select('PAYMENT_SAGEPAY_SENDEMAIL');
        $select->setValueOptions(array(
            '1' => 'Yes',
            '0' => 'No'
        ));
        $select->setLabel('Send Email');
        $this->add($select, array(
            'priority' => 930
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_TRANSACTION_TYPE',
            'attributes' => array(
                'type' => 'text',
                'value' => 'PAYMENT'
            ),
            'options' => array(
                'label' => 'Transaction Type'
            )
        ), array(
            'priority' => 920
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_PROTOCOL',
            'attributes' => array(
                'type' => 'text',
                'value' => '2.23'
            ),
            'options' => array(
                'label' => 'Protocol'
            )
        ), array(
            'priority' => 910
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_SAGEPAY_DESCRIPTION',
            'attributes' => array(
                'type' => 'textarea'
            ),
            'options' => array(
                'label' => 'Description'
            )
        ), array(
            'priority' => 900
        ));
        
        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }
}