<?php
namespace SagepayPayment\Form\AdminSagepayPaymentConfig;

use Common\Form\Option\AbstractFormFactory;

class SagepayPaymentConfigFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigForm');
        $form->setName('adminSagepayPaymentConfig');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('SagepayPayment\Form\AdminSagepayPaymentConfig\SagepayPaymentConfigFilter');
    }
}
