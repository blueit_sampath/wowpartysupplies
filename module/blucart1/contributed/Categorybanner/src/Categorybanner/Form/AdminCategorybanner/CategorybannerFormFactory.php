<?php
namespace Categorybanner\Form\AdminCategorybanner;

use Common\Form\Option\AbstractFormFactory;

class CategorybannerFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Categorybanner\Form\AdminCategorybanner\CategorybannerForm' );
		$form->setName ( 'adminCategorybannerAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Categorybanner\Form\AdminCategorybanner\CategorybannerFilter' );
	}
}
