<?php

namespace Categorybanner\Service\Form;

use File\Api\FileApi;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminCategorybanner extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'weight',
        'status',
        'url'
    );
    protected $_entity = '\Categorybanner\Entity\Categorybanner';
    protected $_entityName = 'categorybanner';

    public function getFormName() {
        return 'adminCategorybannerAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function parseSaveEntity($params, $entity) {
        FileApi::deleteFiles(Functions::fromPost('image_delete', ''));

        if (isset($params ['category'])) {
            if ($params ['category']) {
                $category = CategoryApi::getCategoryById($params['category']);
                if ($category) {
                    $entity->category = $category;
                }
            }
        }


        $image = $params ['image'];


        $imageEntity = null;
        if ($image) {
            $array = explode(',', $image);
            foreach ($array as $file) {
                $imageEntity = FileApi::createOrUpdateFile($file, $params ['title'], $params ['alt']);
            }
        }

        $entity->file = $imageEntity;
    }

     public function beforeGetRecord() {
        $form = $this->getForm();
        $form->get('category')->setValue(Functions::fromRoute('categoryId', 0));
    }
    public function afterGetRecord($entity) {
        $form = $this->getForm();
        if ($entity->file) {
            $form->get('title')->setValue($entity->file->title);
            $form->get('alt')->setValue($entity->file->alt);
            $form->get('image')->setValue($entity->file->path);
        }
        if ($entity->category) {
            $form->get('category')->setValue($entity->category->id);
        }
    }

}
