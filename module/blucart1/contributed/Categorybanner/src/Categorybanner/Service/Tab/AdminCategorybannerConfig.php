<?php

namespace Categorybanner\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategorybannerConfig extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryConfigAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-categorybanner');
		
                $tabContainer->add ( 'admin-categorybanner', 'Banners', $u, 900 );
		
		
		return $this;
	}
}

