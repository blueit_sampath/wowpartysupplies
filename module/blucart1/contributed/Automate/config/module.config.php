<?php

namespace Automate;

return array (
		'controllers' => array (
				'invokables' => array (
						__NAMESPACE__.'\Controller\Index' => __NAMESPACE__.'\Controller\IndexController',
						
				) 
		),
		'router' => array (
				'routes' => array (
						'automate' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/automate[/:action][/:limit]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												
										),
										'defaults' => array (
												'controller' => __NAMESPACE__ . '\Controller\Index',
												'action' => 'index' 
										) 
								) 
						) 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						__NAMESPACE__ => __DIR__ . '/../view' 
				) 
		),
		
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								__NAMESPACE__ => __DIR__ . '/../public' 
						) 
				) 
		) 
); 

