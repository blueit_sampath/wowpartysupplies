<?php

namespace Automate\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class AutomateNavigationForm extends Form 

{
	public function __construct($name = '') {
		parent::__construct ( $name );
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'moduleName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Module Name eg.User' 
				),
				'options' => array (
						'label' => 'Module Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'className',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Class Name' 
				),
				'options' => array (
						'label' => 'Class Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'parentId',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'productMain'
				),
				'options' => array (
						'label' => 'Parent Id'
				)
		) );
		
		$this->add ( array (
				'name' => 'routeName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Route Name'
				),
				'options' => array (
						'label' => 'Route Name'
				)
		) );
		
		$this->add ( array (
				'name' => 'label',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Products'
				),
				'options' => array (
						'label' => 'Label'
				)
		) );
		
		$this->add ( array (
				'name' => 'iconClass',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'glyphicon-briefcase'
				),
				'options' => array (
						'label' => 'Icon Class'
				)
		) );
		
		
		
		$this->add ( array (
				'name' => 'submit',
				'type' => 'submit',
				'attributes' => array (
						'value' => 'Submit' 
				) 
		) );
	}
}