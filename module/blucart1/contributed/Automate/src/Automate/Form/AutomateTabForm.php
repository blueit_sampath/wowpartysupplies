<?php

namespace Automate\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class AutomateTabForm extends Form 

{
	public function __construct($name = '') {
		parent::__construct ( $name );
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'moduleName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Module Name eg.User' 
				),
				'options' => array (
						'label' => 'Module Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'className',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Class Name' 
				),
				'options' => array (
						'label' => 'Class Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'eventName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Event Name'
				),
				'options' => array (
						'label' => 'Event Name'
				)
		) );
		
		$this->add ( array (
				'name' => 'routeName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Route Name'
				),
				'options' => array (
						'label' => 'Route Name'
				)
		) );
		
		$this->add ( array (
				'name' => 'tabName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Tab Name'
				),
				'options' => array (
						'label' => 'Tab Name'
				)
		) );
		
		$this->add ( array (
				'name' => 'tabTitle',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Tab Title'
				),
				'options' => array (
						'label' => 'Tab Title'
				)
		) );
		
		$this->add ( array (
				'name' => 'priority',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'priority'
				),
				'options' => array (
						'label' => 'Priority'
				)
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'type' => 'submit',
				'attributes' => array (
						'value' => 'Submit' 
				) 
		) );
	}
}