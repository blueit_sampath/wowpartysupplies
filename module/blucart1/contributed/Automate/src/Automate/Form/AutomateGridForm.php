<?php

namespace Automate\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class AutomateGridForm extends Form 

{
	protected $_limit = 5;
	public function __construct($limit = 5) {
		parent::__construct();
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'ModuleName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'ModuleName eg.User' 
				),
				'options' => array (
						'label' => 'Module Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ControllerName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'ControllerName' 
				),
				'options' => array (
						'label' => 'Controller Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ServiceName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'ServiceName' 
				),
				'options' => array (
						'label' => 'Service Name' 
				) 
		) );
		$this->add ( array (
				'name' => 'EventName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'EventName' 
				),
				'options' => array (
						'label' => 'Event Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'Entity',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Entity' 
				),
				'options' => array (
						'label' => 'Entity' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'EntityName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'EntityName' 
				),
				'options' => array (
						'label' => 'Entity Name' 
				) 
		) );
		
		$this->createColumns ( $limit );
		
		$this->add ( array (
				'name' => 'toolbar',
				'type' => 'Zend\Form\Element\Select',
				'attributes' => array (
						'placeholder' => '',
						'multiple'=>true
						
				),
				'options' => array (
							'label' => 'Toolbar',
						'value_options' => array (
								
								'create' => 'create',
								'save' => 'save',
								'cancel' => 'cancel',
								'destroy' => 'destroy'
						)
				)
		) );
		
		
		$this->add ( array (
				'name' => 'submit',
				'type' => 'submit',
				'attributes' => array (
						'value' => 'Submit' 
				) 
		) );
	}
	public function createColumns($limit) {
		for($i = 0; $i < $limit; $i ++) {
			
			$this->add ( array (
					'name' => 'ColumnName_' . $i,
					'type' => 'Zend\Form\Element\Text',
					'attributes' => array (
							'placeholder' => 'ColumnName eg.name' 
					),
					'options' => array (
							'label' => 'Column Name' 
					) 
			)
			 );
			
			$this->add ( array (
					'name' => 'Key_' . $i,
					'type' => 'Zend\Form\Element\Text',
					'attributes' => array (
							'placeholder' => 'Key eg.user.name' 
					) 
			) );
			
			$this->add ( array (
					'name' => 'Title_' . $i,
					'type' => 'Zend\Form\Element\Text',
					'attributes' => array (
							'placeholder' => 'Title eg.Name' 
					) 
			) );
			$this->add ( array (
					'name' => 'Type_' . $i,
					'type' => 'Zend\Form\Element\Select',
					'attributes' => array (
							'placeholder' => '' 
					),
					'options' => array (
							
							'value_options' => array (
									'string' => 'string',
									'number' => 'number',
									'boolean' => 'boolean',
									'date' => 'date' 
							) 
					) 
			) );
			
			$this->add ( array (
					'name' => 'Editable_' . $i,
					'type' => 'Zend\Form\Element\Select',
					'attributes' => array (
							'value' => 'true' 
					),
					'options' => array (
							
							'value_options' => array (
									'true' => 'Editable',
									'false' => 'Not Editable' 
							) 
					) 
			) );
		}
	}
}