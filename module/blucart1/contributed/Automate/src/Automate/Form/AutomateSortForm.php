<?php

namespace Automate\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class AutomateSortForm extends Form 

{
	public function __construct($name = '') {
		parent::__construct ( $name );
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'moduleName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Module Name eg.Category' 
				),
				'options' => array (
						'label' => 'Module Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'className',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'AdminCategory' 
				),
				'options' => array (
						'label' => 'Class Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'eventName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'adminCategorySort'
				),
				'options' => array (
						'label' => 'Event Name'
				)
		) );
		
		$this->add ( array (
				'name' => 'entity',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => '\Category\Entity\Category'
				),
				'options' => array (
						'label' => 'Entity'
				)
		) );
		
		
		
		$this->add ( array (
				'name' => 'priority',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => '1000'
				),
				'options' => array (
						'label' => 'Priority'
				)
		) );
		
		
		
		$this->add ( array (
				'name' => 'submit',
				'type' => 'submit',
				'attributes' => array (
						'value' => 'Submit' 
				) 
		) );
	}
}