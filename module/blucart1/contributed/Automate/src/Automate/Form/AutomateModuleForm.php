<?php

namespace Automate\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class AutomateModuleForm extends Form 

{
	public function __construct($name = '') {
		parent::__construct ( $name );
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'ModuleName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'ModuleName eg.User' 
				),
				'options' => array (
						'label' => 'Module Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'ControllerName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'ControllerName' 
				),
				'options' => array (
						'label' => 'Controller Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'type' => 'submit',
				'attributes' => array (
						'value' => 'Submit' 
				) 
		) );
	}
}