<?php

namespace Automate\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class TestForm extends Form 

{
	public function __construct($name = '') {
		parent::__construct ( $name );
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'moduleName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Module Name eg.User' 
				),
				'options' => array (
						'label' => 'Module Name' 
				) 
		) );
		
		
		
		$this->add ( array (
				'name' => 'image',
				'type' => 'File\Form\Element\FileUpload',
				'options' => array (
						'label' => 'Event Name'
				)
		) );
		
		
		
		$this->add ( array (
				'name' => 'submit',
				'type' => 'submit',
				'attributes' => array (
						'value' => 'Submit' 
				) 
		) );
	}
}