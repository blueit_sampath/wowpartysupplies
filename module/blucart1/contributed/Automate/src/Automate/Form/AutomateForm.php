<?php

namespace Automate\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class AutomateForm extends Form 

{
	public function __construct($name = '') {
		parent::__construct ( $name );
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'moduleName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'Module Name eg.Category' 
				),
				'options' => array (
						'label' => 'Module Name' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'formCategory',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'AdminCategory' 
				),
				'options' => array (
						'label' => 'Form Category' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'formClass',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'CategoryForm'
				),
				'options' => array (
						'label' => 'Form Class'
				)
		) );
		
		$this->add ( array (
				'name' => 'filterClass',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'CategoryFormFilter'
				),
				'options' => array (
						'label' => 'Filter Class'
				)
		) );
		
		$this->add ( array (
				'name' => 'factoryClass',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'CategoryFormFactory'
				),
				'options' => array (
						'label' => 'Factory Class'
				)
		) );
		
		$this->add ( array (
				'name' => 'formName',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array (
						'placeholder' => 'adminCategoryAdd'
				),
				'options' => array (
						'label' => 'Form Name'
				)
		) );
		
		
		
		$this->add ( array (
				'name' => 'submit',
				'type' => 'submit',
				'attributes' => array (
						'value' => 'Submit' 
				) 
		) );
	}
}