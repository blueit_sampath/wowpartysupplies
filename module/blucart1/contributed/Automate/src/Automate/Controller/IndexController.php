<?php

namespace Automate\Controller;

use Automate\Form\AutomateQueryForm;

use Automate\Form\AutomateBlockForm;

use Common\MVC\Controller\AbstractAdminController;

use Automate\Form\AutomateNavigationForm;

use Automate\Form\AutomateTokenForm;

use Automate\Form\AutomateContentForm;

use Automate\Form\AutomateSortForm;

use Automate\Form\AutomateForm;

use Automate\Form\TestForm;

use Automate\Form\AutomateFormEvent;

use Automate\Form\AutomateModuleForm;
use Automate\Form\AutomateLinkForm;
use Automate\Form\AutomateTabForm;
use ZFTool\Model\Skeleton;
use Automate\Form\AutomateGridForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractAdminController {
	public function indexAction() {
	}
	public function createModuleAction() {
		$form = new AutomateModuleForm ();
		$message = '';
		$request = $this->getRequest ();
		
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
			
			\Automate\Model\Skeleton::createModule ( $this->getServiceLocator (), $form );
			
			$message = 'Module created successfully';
			$form = new AutomateModuleForm ();
		}
		
		return array (
				'form' => $form,
				'message' => $message 
		);
	}
	public function createControllerAction() {
	}
	public function createGridAction() {
		$limit = $this->params ()->fromRoute ( 'limit', 5 );
		$form = new AutomateGridForm ( $limit );
		
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
			
			\Automate\Model\Skeleton::createGrid ( $this->getServiceLocator (), $form, $limit );
			$message = 'Service created successfully';
			$form = new AutomateGridForm ( $limit );
		}
		
		return array (
				'form' => $form,
				'limit' => $limit,
				'message' => $message 
		);
	}
	
	public function createTabAction() {
		
		$form = new AutomateTabForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
				
			\Automate\Model\Skeleton::createTab ( $form );
			$message = 'Tab created successfully';
			$form = new AutomateTabForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
	public function createLinkAction() {
	
		$form = new AutomateLinkForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createLink ( $form );
			$message = 'Link created successfully';
			$form = new AutomateTabForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	public function createFormEventAction() {
	
		$form = new AutomateFormEvent();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createFormEvent ( $form );
			$message = 'Form Event created successfully';
			$form = new AutomateTabForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
	public function createFormAction() {
	
		$form = new AutomateForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createForm ( $form );
			$message = 'Form created successfully';
			$form = new AutomateForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
	public function createSortAction() {
	
		$form = new AutomateSortForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createSort ( $form );
			$message = 'Form created successfully';
			$form = new AutomateSortForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
	public function testAction() {
		$form = new TestForm();
		return array (
				'form' => $form,
		);
	}
	
	public function createContentAction() {
	
		$form = new AutomateContentForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createContent ( $form );
			$message = 'Content created successfully';
			$form = new AutomateContentForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
	public function createBlockAction() {
	
		$form = new AutomateBlockForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createBlock ( $form );
			$message = 'Block created successfully';
			$form = new AutomateBlockForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
	public function createQueryAction() {
	
		$form = new AutomateQueryForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createQuery ( $form );
			$message = 'Query created successfully';
			$form = new AutomateQueryForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
	public function createTokenAction() {
	
		$form = new AutomateTokenForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createToken ( $form );
			$message = 'Token created successfully';
			$form = new AutomateTokenForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
	public function createNavigationAction() {
	
		$form = new AutomateNavigationForm();
	
		$request = $this->getRequest ();
		$message = '';
		if ($request->isPost ()) {
			$form->setData ( $request->getPost () );
			$form->isValid ();
	
			\Automate\Model\Skeleton::createNavigation ( $form );
			$message = 'Navigation created successfully';
			$form = new AutomateNavigationForm();
		}
	
		return array (
				'form' => $form,
				'message' => $message
		);
	}
	
}