<?php

namespace Automate\Model;

use Core\Functions;
use ZFTool\Model\Skeleton as ZFSkeleton;
use Zend\View\Model\ViewModel;

class Skeleton {
    
        public static function getModulePath(){
            return 'module/project_specific/';
        }
	public static function createModule($sm, $form) {
		$moduleName = $form->get ( 'ModuleName' )->getValue ();
		$controllerName = $form->get ( 'ControllerName' )->getValue ();
		$basePath = static::getModulePath();
		$paths = array (
				$moduleName,
				$moduleName . '/config',
				$moduleName . '/src',
				$moduleName . '/src/' . $moduleName,
				$moduleName . '/src/' . $moduleName . '/Controller',
				$moduleName . '/src/' . $moduleName . '/Api',
				$moduleName . '/src/' . $moduleName . '/Service',
				$moduleName . '/src/' . $moduleName . '/Service/Grid',
				$moduleName . '/src/' . $moduleName . '/Form',
				$moduleName . '/src/' . $moduleName . '/Entity',
				$moduleName . '/view',
				$moduleName . '/view/' . lcfirst ( $moduleName ),
				$moduleName . '/view/' . lcfirst ( $moduleName ) . '/' . lcfirst ( $controllerName ) 
		);
		
		foreach ( $paths as $path ) {
			
			@mkdir ( $basePath . $path );
		}
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$model->setTemplate ( 'skeleton/module_config' );
		$html = $renderer->render ( $model );
		
		file_put_contents ( $basePath . $moduleName . '/config/module.config.php', $html );
		
		$model->setTemplate ( 'skeleton/module' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/Module.php', $html );
		
		if ($controllerName) {
			static::createController ( $sm, $moduleName, $controllerName, $form );
		}
		
		$model->setTemplate ( 'skeleton/api' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Api/' . $moduleName . 'Api.php', $html );
		
		return false;
		
		$application = require "config/application.config.php";
		if (! in_array ( $moduleName, $application ['modules'] )) {
			$application ['modules'] [] = $moduleName;
			copy ( "config/application.config.php", "config/application.config.old" );
			$content = <<<EOD
			<?php

EOD;
			
			$content .= 'return ' . ZFSkeleton::exportConfig ( $application ) . ";\n";
			file_put_contents ( "config/application.config.php", $content );
		}
	}
	public static function createController($sm, $moduleName, $controllerName, $form, $templateName = 'skeleton/index-controller') {
		$basePath = static::getModulePath();
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$renderer = $sm->get ( 'viewrenderer' );
		$model->setTemplate ( 'skeleton/controller' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Controller/' . $controllerName . 'Controller.php', $html );
		$path = $basePath . $moduleName . '/view/' . lcfirst ( $moduleName ) . '/' . lcfirst ( $controllerName );
		@mkdir ( $path, 0777, true );
		$model->setTemplate ( $templateName );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/view/' . lcfirst ( $moduleName ) . '/' . lcfirst ( $controllerName ) . '/index.phtml', $html );
		
		$config = static::getModuleConfig ( $moduleName );
		copy ( static::getModulePath()."/$moduleName/config/module.config.php", static::getModulePath()."/$moduleName/config/module.config.old" );
		$config ['controllers'] ['invokables'] [$moduleName . '\\Controller\\' . $controllerName] = $moduleName . '\\Controller\\' . $controllerName . 'Controller';
		
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createGrid($sm, $form, $limit) {
		$moduleName = $form->get ( 'ModuleName' )->getValue ();
		$controllerName = $form->get ( 'ControllerName' )->getValue ();
		$serviceName = $form->get ( 'ServiceName' )->getValue ();
		$basePath = static::getModulePath();
		
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData (),
				'limit' => $limit 
		) );
		
		if ($controllerName) {
			static::createController ( $sm, $moduleName, $controllerName, $form, 'skeleton/service-index' );
		}
		$config = static::getModuleConfig ( $moduleName );
		$model->setTemplate ( 'skeleton/service' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Grid/' . $serviceName . '.php', $html );
		
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Grid\\' . $serviceName] = $moduleName . '\\Service\\Grid\\' . $serviceName;
		$config ['events'] [$moduleName . '\\Service\\Grid\\' . $serviceName] = $moduleName . '\\Service\\Grid\\' . $serviceName;
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createTab($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/Tab';
		@mkdir ( $basePath . $path );
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$model->setTemplate ( 'skeleton/tab' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Tab/' . $className . '.php', $html );
		
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Tab\\' . $className] = $moduleName . '\\Service\\Tab\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\Tab\\' . $className] = $moduleName . '\\Service\\Tab\\' . $className;
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createLink($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/Link';
		@mkdir ( $basePath . $path );
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$model->setTemplate ( 'skeleton/link' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Link/' . $className . '.php', $html );
		
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Link\\' . $className] = $moduleName . '\\Service\\Link\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\Link\\' . $className] = $moduleName . '\\Service\\Link\\' . $className;
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createToken($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/Token';
		@mkdir ( $basePath . $path );
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$model->setTemplate ( 'skeleton/token' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Token/' . $className . '.php', $html );
		
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Token\\' . $className] = $moduleName . '\\Service\\Token\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\Token\\' . $className] = $moduleName . '\\Service\\Token\\' . $className;
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createFormEvent($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/Form';
		@mkdir ( $basePath . $path );
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$model->setTemplate ( 'skeleton/form-event' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Form/' . $className . '.php', $html );
		
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Form\\' . $className] = $moduleName . '\\Service\\Form\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\Form\\' . $className] = $moduleName . '\\Service\\Form\\' . $className;
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createForm($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$formCategory = $form->get ( 'formCategory' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Form/' . $formCategory;
		@mkdir ( $basePath . $path, 0777, true );
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		
		$formClass = $form->get ( 'formClass' )->getValue ();
		$model->setTemplate ( 'skeleton/form' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $path . '/' . $formClass . '.php', $html );
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Form\\' . $formCategory . '\\' . $formClass] = $moduleName . '\\Form\\' . $formCategory . '\\' . $formClass;
		
		$filterClass = $form->get ( 'filterClass' )->getValue ();
		$model->setTemplate ( 'skeleton/form-filter' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $path . '/' . $filterClass . '.php', $html );
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Form\\' . $formCategory . '\\' . $filterClass] = $moduleName . '\\Form\\' . $formCategory . '\\' . $filterClass;
		
		$factoryClass = $form->get ( 'factoryClass' )->getValue ();
		$model->setTemplate ( 'skeleton/form-factory' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $path . '/' . $factoryClass . '.php', $html );
		$config ['service_manager'] ['factories'] [$moduleName . '\\Form\\' . $formCategory . '\\' . $factoryClass] = $moduleName . '\\Form\\' . $formCategory . '\\' . $factoryClass;
		
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createContent($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		$template = $form->get ( 'template' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/Content';
		@mkdir ( $basePath . $path );
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$model->setTemplate ( 'skeleton/content' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Content/' . $className . '.php', $html );
		
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Content\\' . $className] = $moduleName . '\\Service\\Content\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\Content\\' . $className] = $moduleName . '\\Service\\Content\\' . $className;
		
		if ($template) {
			$model->setTemplate ( 'skeleton/content-view' );
			$html = $renderer->render ( $model );
			$path = $basePath . $moduleName . '/view/common';
			@mkdir ( $path, 0777, true );
			
			file_put_contents ( $basePath . $moduleName . '/view/common/' . $template . '.phtml', $html );
		}
		static::writeModuleConfig ( $moduleName, $config );
	}
	
	public static function createBlock($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		$template = $form->get ( 'template' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/Block';
		@mkdir ( $basePath . $path );
	
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData ()
		) );
		$model->setTemplate ( 'skeleton/block' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Block/' . $className . '.php', $html );
	
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Block\\' . $className] = $moduleName . '\\Service\\Block\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\Block\\' . $className] = $moduleName . '\\Service\\Block\\' . $className;
	
		if ($template) {
			$model->setTemplate ( 'skeleton/block-view' );
			$html = $renderer->render ( $model );
			$path = $basePath . $moduleName . '/view/block';
			@mkdir ( $path, 0777, true );
				
			file_put_contents ( $basePath . $moduleName . '/view/block/' . $template . '.phtml', $html );
		}
		static::writeModuleConfig ( $moduleName, $config );
	}
	
	public static function createQuery($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/Query';
		@mkdir ( $basePath . $path );
	
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData ()
		) );
		$model->setTemplate ( 'skeleton/query' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Query/' . $className . '.php', $html );
	
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Query\\' . $className] = $moduleName . '\\Service\\Query\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\Query\\' . $className] = $moduleName . '\\Service\\Query\\' . $className;
	
		
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createSort($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/NestedSorting';
		@mkdir ( $basePath . $path );
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$model->setTemplate ( 'skeleton/nestedsorting' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/NestedSorting/' . $className . '.php', $html );
		
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\NestedSorting\\' . $className] = $moduleName . '\\Service\\NestedSorting\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\NestedSorting\\' . $className] = $moduleName . '\\Service\\NestedSorting\\' . $className;
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function createNavigation($form) {
		$sm = Functions::getServiceLocator ();
		$moduleName = $form->get ( 'moduleName' )->getValue ();
		$className = $form->get ( 'className' )->getValue ();
		$basePath = static::getModulePath();
		$config = static::getModuleConfig ( $moduleName );
		if (! is_dir ( $basePath . $moduleName )) {
			throw new \Exception ( 'Module doesnot exists' );
		}
		$path = $moduleName . '/src/' . $moduleName . '/Service/Navigation';
		@mkdir ( $basePath . $path );
		
		$renderer = $sm->get ( 'viewrenderer' );
		$model = new ViewModel ( array (
				'data' => $form->getData () 
		) );
		$model->setTemplate ( 'skeleton/navigation' );
		$html = $renderer->render ( $model );
		file_put_contents ( $basePath . $moduleName . '/src/' . $moduleName . '/Service/Navigation/' . $className . '.php', $html );
		
		$config ['service_manager'] ['invokables'] [$moduleName . '\\Service\\Navigation\\' . $className] = $moduleName . '\\Service\\Navigation\\' . $className;
		$config ['events'] [$moduleName . '\\Service\\Navigation\\' . $className] = $moduleName . '\\Service\\Navigation\\' . $className;
		static::writeModuleConfig ( $moduleName, $config );
	}
	public static function getModuleConfig($moduleName) {
		$array = require static::getModulePath()."/$moduleName/config/module.config.php";
		return $array;
	}
	public static function writeModuleConfig($moduleName, $array) {
		$content = <<<EOD
<?php
		
		
EOD;
		
		$content .= 'return ' . ZFSkeleton::exportConfig ( $array ) . ";\n";
		file_put_contents ( static::getModulePath()."/$moduleName/config/module.config.php", $content );
	}
}
