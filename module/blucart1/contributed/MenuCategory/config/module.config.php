<?php

return array(
    'router' => array('routes' => array()),
    'events' => array(
        'MenuCategory\Service\Form\AdminMenuCategory' => 'MenuCategory\Service\Form\AdminMenuCategory',
        'MenuCategory\Service\Form\AdminMenuCategoryCategoryAdd' => 'MenuCategory\Service\Form\AdminMenuCategoryCategoryAdd',
        'MenuCategory\Service\Grid\AdminCategoryGrid' => 'MenuCategory\Service\Grid\AdminCategoryGrid',
        'MenuCategory\Service\NestedSorting\AdminNestedSortingCategory' => 'MenuCategory\Service\NestedSorting\AdminNestedSortingCategory'
    ),
    'service_manager' => array(
        'invokables' => array(
            'MenuCategory\Service\Form\AdminMenuCategory' => 'MenuCategory\Service\Form\AdminMenuCategory',
            'MenuCategory\Service\Form\AdminMenuCategoryCategoryAdd' => 'MenuCategory\Service\Form\AdminMenuCategoryCategoryAdd',
            'MenuCategory\Service\Grid\AdminCategoryGrid' => 'MenuCategory\Service\Grid\AdminCategoryGrid',
            'MenuCategory\Service\NestedSorting\AdminNestedSortingCategory' => 'MenuCategory\Service\NestedSorting\AdminNestedSortingCategory'
        ),
        'factories' => array()
    ),
    'controllers' => array('invokables' => array()),
    'view_manager' => array('template_path_stack' => array('MenuCategory' => __DIR__ . '/../view')),
    'doctrine' => array('driver' => array(
            'MenuCategory_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/MenuCategory/Entity')
            ),
            'orm_default' => array('drivers' => array('MenuCategory\Entity' => 'MenuCategory_driver'))
        )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('MenuCategory' => __DIR__ . '/../public')))
);
