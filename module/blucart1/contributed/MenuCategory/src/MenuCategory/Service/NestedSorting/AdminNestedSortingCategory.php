<?php

namespace MenuCategory\Service\NestedSorting;

use Core\Functions;
use Category\Api\CategoryApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminNestedSortingCategory extends AbstractNestedSortingEvent {
	public function getEventName() {
		return 'adminCategorySort';
	}
	public function getPriority() {
		return 500;
	}
	public function formList($e) {
           
	}
	public function saveList($e) {
       
            \MenuCategory\Api\MenuCategoryApi::rebuildAllMenus(true);
	}
	
}
