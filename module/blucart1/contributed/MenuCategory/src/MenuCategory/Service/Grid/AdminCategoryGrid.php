<?php

namespace MenuCategory\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminCategoryGrid extends \BlucartGrid\Event\AbstractBlucartGridEvent {

    public function getEventName() {
        return 'adminCategory';
    }

    public function postCreate($e) {
        parent::postCreate($e);
        $grid = $this->getGrid();
        $resultContainer = $this->getGrid()->getResultContainer();
        $category = $resultContainer->get('category');
        if (!$category) {
            return;
        }

        $category = $category->getValue();
        \MenuCategory\Api\MenuCategoryApi::syncAllMenus($category->id, true);
    }

    public function postDestroy($e) {

        parent::postDestroy($e);
        $resultObject = $this->getGrid()->getResultObject();
        $resultContainer = $this->getGrid()->getResultContainer();
        $params = $resultObject->getResults();
        if ($params['id']) {
            \MenuCategory\Api\MenuCategoryApi::removeCategoryInAllMenus($params['id']);
        }
    }

    public function postUpdate($e) {
        parent::postUpdate($e);

        $resultContainer = $this->getGrid()->getResultContainer();
        $category = $resultContainer->get('category');
        if (!$category) {
            return;
        }
        $category = $category->getValue();
        \MenuCategory\Api\MenuCategoryApi::syncAllMenus($category->id, true);
    }

}
