<?php

namespace MenuCategory\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminMenuCategory extends \Common\Form\Option\AbstractFormEvent {

    public function getFormName() {
        return 'adminMenuManagerAdd';
    }

    public function getPriority() {
        return 500;
    }

    public function postInitEvent() {
        $form = $this->getForm();

        $form->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'syncCategory',
            'options' => array(
                'label' => 'Sync Category',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 880
        ));
        $select = new \Zend\Form\Element\Select('parentMenuOption');
        $select->setLabel('Parent Menu Option');
        $select->setValueOptions($this->getMenuOptions());
        $form->add($select, array(
            'priority' => 870
        ));

        $form->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'forceBuild',
            'options' => array(
                'label' => 'Force Build',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 860
        ));

        $inputFilter = $form->getInputFilter();
        $inputFilter->add(array(
            'name' => 'syncCategory',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));
        $inputFilter->add(array(
            'name' => 'parentMenuOption',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));

        $inputFilter->add(array(
            'name' => 'forceBuild',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));

        return $this;
    }

    public function getMenuOptions() {
        $array = array(
            '' => ''
        );
        $menuId = \Core\Functions::fromRoute('menuId');

        $results = \MenuManager\Api\MenuManagerApi::getChildrenMenuOptions($menuId, null);

        $result = $this->formOptions($results, $menuId);
        return $array + $result;
    }

    public function formOptions($array, $menuId, $level = 0) {
        $result = array();

        foreach ($array as $res) {
            $menuOptionId = $res ['menuOptionId'];

            $menuOption = \MenuManager\Api\MenuManagerApi::getMenuOptionById($menuOptionId);
            $space = '';

            $name = '';
            $parent = $menuOption;
            while ($parent->parent) {
                $name = $parent->parent->name . ' > ' . $name;
                $parent = $parent->parent;
            }

            if (!$name) {
                $result [$menuOption->id] = $menuOption->name;
            } else {
                $result [$menuOption->id] = $name . $menuOption->name;
            }

            $children = \MenuManager\Api\MenuManagerApi::getChildrenMenuOptions($menuId, $menuOption->id, null);
            if (count($children)) {
                $result = $result + $this->formOptions($children, $menuId, ($level + 1));
            }
        }

        return $result;
    }

    public function save() {
        $resultContainer = $this->getFormResultContainer();
        $form = $this->getForm();
        $params = $form->getData();
        $em = $this->getEntityManager();

        $menu = $resultContainer->get('menu');


        if ($menu) {
            $menu = $menu->getValue();
            $entity = \MenuCategory\Api\MenuCategoryApi::isCategoryEnabledForMenu($menu->id);

            if ($params ['syncCategory']) {

                if (!$entity) {
                    $entity = new \MenuCategory\Entity\MenuCategory();
                    $entity->menu = $menu;
                    $menuOption = null;
                    if ($params ['parentMenuOption']) {
                        $menuOption = \MenuManager\Api\MenuManagerApi::getMenuOptionById($params ['parentMenuOption']);
                    }

                    $entity->menuOption = $menuOption ? $menuOption : null;
                    $em->persist($entity);
                    $em->flush();
                    if ($params['forceBuild']) {

                        \MenuCategory\Api\MenuCategoryApi::removeAllCategories($menu->id);
                        \MenuCategory\Api\MenuCategoryApi::synAllCategories($menu->id, $params ['parentMenuOption'], true);
                    } else {
                        \MenuCategory\Api\MenuCategoryApi::removeAllCategories($menu->id);
                        \MenuCategory\Api\MenuCategoryApi::synAllCategories($menu->id, $params ['parentMenuOption']);
                    }
                } else {

                    $menuOption = null;
                    if ($params ['parentMenuOption']) {
                        $menuOption = \MenuManager\Api\MenuManagerApi::getMenuOptionById($params ['parentMenuOption']);
                    }

                    $entity->menuOption = $menuOption ? $menuOption : null;
                    $em->persist($entity);
                    $em->flush();
                    if ($params['forceBuild']) {
                        \MenuCategory\Api\MenuCategoryApi::removeAllCategories($menu->id);
                       
                        \MenuCategory\Api\MenuCategoryApi::synAllCategories($menu->id, $params ['parentMenuOption'], true);
                    }
                }
            } else {
                if ($entity) {
                    $em->remove($entity);
                    $em->flush();
                }
            }
        }

        return true;
    }

    public function getRecord() {
        $menuId = Functions::fromRoute('menuId');
        if (!$menuId) {
            return;
        }

        $form = $this->getForm();
        $em = $this->getEntityManager();

        $entity = \MenuCategory\Api\MenuCategoryApi::isCategoryEnabledForMenu($menuId);
        if (!$entity) {

            return;
        }

        $form->get('syncCategory')->setValue(1);
        if ($entity->menuOption) {
            $form->get('parentMenuOption')->setValue($entity->menuOption->id);
        }

        return true;
    }

}
