<?php

namespace MenuCategory\Service\Form;

use Core\Functions;

class AdminMenuCategoryCategoryAdd extends \Common\Form\Option\AbstractFormEvent {

    public function getFormName() {
        return 'adminCategory';
    }

    public function getPriority() {
        return;
    }

    public function save() {
        $resultContainer = $this->getFormResultContainer();
      
        $category = $resultContainer->get('category');


        if ($category) {
            $category = $category->getValue();
            \MenuCategory\Api\MenuCategoryApi::syncAllMenus($category->id,true);
        }
    }

}
