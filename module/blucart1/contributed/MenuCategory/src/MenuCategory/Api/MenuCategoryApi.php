<?php

namespace MenuCategory\Api;

class MenuCategoryApi extends \Common\Api\Api {

    protected static $_entity = '\MenuCategory\Entity\MenuCategory';
    protected static $_entityMenuOption = '\MenuManager\Entity\MenuOption';

    public static function getMenuCategoryById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function isCategoryEnabledForMenu($menuId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array('menu' => $menuId));
    }

    public static function removeAllCategories($menuId = 0) {
        $em = \Core\Functions::getEntityManager();
        $dql = "delete from " . static::$_entityMenuOption . " u where u.uri like '/category/%' and u.menu = " . $menuId;

        $query = $em->createQuery($dql);
        $query->getResult();

        return false;
    }

    public static function getMenuOptionByCategoryId($categoryId = 0, $menuId = 0, $route = 'category') {
        $em = \Core\Functions::getEntityManager();
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn("menuOption.id", 'menuOptionId');
        $item = $queryBuilder->addFrom(static::$_entityMenuOption, 'menuOption');
        $queryBuilder->addWhere('menuOption.menu', 'menuOptionMenu');
        $queryBuilder->setParameter('menuOptionMenu', $menuId);
        $queryBuilder->addWhere('menuOption.uri', 'menuOptionUri', ' like ');
        $queryBuilder->setParameter('menuOptionUri', '/category/' . $categoryId);
        $results = $queryBuilder->executeQuery();
        if (count($results)) {
            $array = (array) $results;
            $result = array_pop($array);
            return \MenuManager\Api\MenuManagerApi::getMenuOptionById($result['menuOptionId']);
        }
        return false;
    }

    public static function syncCategory($categoryId, $menuId, $parentMenuOptionId = null, $isForce = false) {
        $em = \Core\Functions::getEntityManager();
        $router = \Core\Functions::getServiceLocator()->get('Router');
        $category = \Category\Api\CategoryApi::getCategoryById($categoryId);
        if (!$category) {
            return;
        }

        $menu = \MenuManager\Api\MenuManagerApi::getMenuById($menuId);
        if (!$menu) {
            return;
        }

        $parentOption = null;
        if ($parentMenuOptionId) {
            $parentOption = \MenuManager\Api\MenuManagerApi::getMenuOptionById($parentMenuOptionId);
        }

        if ($category->parent) {
            $parentOption = static::getMenuOptionByCategoryId($category->parent->id, $menuId);
        }
        $menuOption = static::getMenuOptionByCategoryId($categoryId, $menuId);
        $url = $router->assemble(array('categoryId' => $categoryId), array('name' => 'category'));
        $url = static::removeBasePath($url);
        if ($menuOption) {
            $menuOption->title = $category->title;
            $menuOption->name = 'category_' . $category->id;
            $menuOption->status = $category->status;
            $menuOption->weight = $category->weight;
            if ($isForce) {
                $menuOption->uri = $url;
                $menuOption->status = $category->status;
                $menuOption->parent = $parentOption ? $parentOption : null;
                $menuOption->menu = $menu;
            }
            $em->persist($menuOption);
        } else {
            $menuOption = new static::$_entityMenuOption;
            $menuOption->title = $category->title;
            $menuOption->name = 'category_' . $category->id;
            $menuOption->uri = $url;
            $menuOption->status = $category->status;
            $menuOption->parent = $parentOption ? $parentOption : null;
            $menuOption->menu = $menu;
            $menuOption->weight = $category->weight;
            $em->persist($menuOption);
        }
        $em->flush();

        return $menuOption;
    }

    public static function removeBasePath($url) {
        $basePathPlugin = \Core\Functions::getViewHelperManager()->get('basePath');
        $basePath = $basePathPlugin('/');
        $str = str_replace($basePath, '/', $url);
        return $str;
    }

    public static function syncAllMenus($categoryId, $isForce = false) {
        $em = static::getEntityManager();
        $results = $em->getRepository(static::$_entity)->findAll();

        foreach ($results as $result) {
            $menuId = $result->menu->id;
            $menuOptionId = null;

            if ($result->menuOption) {
                $menuOptionId = $result->menuOption->id;
            }
            static::syncCategory($categoryId, $menuId, $menuOptionId, $isForce);
        }
    }

    public static function removeCategoryInAllMenus($categoryId) {
        $em = static::getEntityManager();
        $results = $em->getRepository(static::$_entity)->findAll();

        foreach ($results as $result) {
            $menuId = $result->menu->id;

            $menuOption = static::getMenuOptionByCategoryId($categoryId, $menuId);
            if ($menuOption) {
                $em->remove($menuOption);
                $em->flush();
            }
        }
    }

    public static function synAllCategories($menuId, $parentOptionId, $isForce = false) {
        $categories = \Category\Api\CategoryApi::getChildrenCategories(null, null);

        static::recursiveSyncCategories($categories, $menuId, $parentOptionId, $isForce);

        return;
    }

    public static function recursiveSyncCategories($array, $menuId, $parentOptionId, $isForce) {

        foreach ($array as $categoryResults) {
            $catId = $categoryResults ['categoryId'];
            static::syncCategory($catId, $menuId, $parentOptionId, $isForce);
            $children = \Category\Api\CategoryApi::getChildrenCategories($catId, null);
            if (count($children)) {
                static::recursiveSyncCategories($children, $menuId, $parentOptionId, $isForce);
            }
        }
    }

    public static function rebuildAllMenus($isForce = false) {
        $em = static::getEntityManager();
        $results = $em->getRepository(static::$_entity)->findAll();

        foreach ($results as $result) {
            if (!$result->menu) {
                continue;
            }
            $menuId = $result->menu->id;

            static::removeAllCategories($menuId);
            $menuOption = null;
            if ($result->menuOption) {
                $menuOption = $result->menuOption->id;
            }
            static::synAllCategories($menuId, $menuOption, $isForce);
        }
    }

}
