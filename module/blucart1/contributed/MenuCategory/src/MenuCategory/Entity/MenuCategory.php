<?php

namespace MenuCategory\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * MenuCategory
 *
 * @ORM\Table(name="menu_category")
 * @ORM\Entity
 */
class MenuCategory {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="create")
     */
    private $createdDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="update")
     */
    private $updatedDate;

    /**
     * @var \Menu
     *
     * @ORM\ManyToOne(targetEntity="\MenuManager\Entity\Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $menu;
    
    /**
     * @var \Menu
     *
     * @ORM\ManyToOne(targetEntity="\MenuManager\Entity\MenuOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_option_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $menuOption;

   

    public function __get($property) {
        if (\method_exists($this, "get" . \ucfirst($property))) {
            $method_name = "get" . \ucfirst($property);
            return $this->$method_name();
        } else {

            if (is_object($this->$property)) {
                // return $this->$property->id;
            }
            return $this->$property;
        }
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     *
     */
    public function __set($property, $value) {
        if (\method_exists($this, "set" . \ucfirst($property))) {
            $method_name = "set" . \ucfirst($property);

            $this->$method_name($value);
        } else
            $this->$property = $value;
    }

}
