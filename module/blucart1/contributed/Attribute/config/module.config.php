<?php
		
		return array(
    'router' => array('routes' => array(
            'admin-attribute' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/attribute',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminAttribute',
                        'action' => 'index'
                        )
                    )
                ),
            'admin-attribute-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/attribute/add[/ajax/:ajax][/:attributeId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminAttribute',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'attributeId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-attribute-option' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/attribute/option[/ajax/:ajax][/:attributeId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminAttributeOption',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'attributeId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-attribute-option-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/attribute/option/add[/ajax/:ajax][/:attributeId][/:attributeOptionId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminAttributeOption',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'attributeId' => '[0-9]+',
                        'attributeOptionId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-product-attribute-option' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/attribute/option[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminAttributeOptionProduct',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-product-attribute-option-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/attribute/option/add[/ajax/:ajax][/:productId][/:attributeOptionProductId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminAttributeOptionProduct',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'attributeOptionProductId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-product-attribute' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/attribute[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminProductAttribute',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-product-attribute-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/attribute/add[/ajax/:ajax][/:productId][/:attributeProductId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminProductAttribute',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'attributeProductId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-product-attribute-bulk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/attribute/bulk[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminBulkAttribute',
                        'action' => 'attribute',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-product-attribute-option-bulk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/attribute/option/bulk[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminBulkAttribute',
                        'action' => 'attribute-option',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-category-attribute' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/attribute[/ajax/:ajax][/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminCategoryAttribute',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-category-attribute-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/attribute/add[/ajax/:ajax][/:categoryId][/:attributeCategoryId]',
                    'defaults' => array(
                        'controller' => 'Attribute\Controller\AdminCategoryAttribute',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'attributeCategoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                )
            )),
    'events' => array(
        'Attribute\Service\Grid\AdminAttribute' => 'Attribute\Service\Grid\AdminAttribute',
        'Attribute\Service\Form\AdminAttribute' => 'Attribute\Service\Form\AdminAttribute',
        'Attribute\Service\Link\AdminAttribute' => 'Attribute\Service\Link\AdminAttribute',
        'Attribute\Service\Tab\AdminAttribute' => 'Attribute\Service\Tab\AdminAttribute',
        'Attribute\Service\Grid\AdminAttributeOption' => 'Attribute\Service\Grid\AdminAttributeOption',
        'Attribute\Service\Grid\AdminAttributeOptionProduct' => 'Attribute\Service\Grid\AdminAttributeOptionProduct',
        'Attribute\Service\Form\AdminAttributeOptionProduct' => 'Attribute\Service\Form\AdminAttributeOptionProduct',
        'Attribute\Service\Tab\AdminAttributeOptionProduct' => 'Attribute\Service\Tab\AdminAttributeOptionProduct',
        'Attribute\Service\Form\AdminAttributeOption' => 'Attribute\Service\Form\AdminAttributeOption',
        'Attribute\Service\Link\AdminAttributeOption' => 'Attribute\Service\Link\AdminAttributeOption',
        'Attribute\Service\Grid\AdminProductAttribute' => 'Attribute\Service\Grid\AdminProductAttribute',
        'Attribute\Service\Form\AdminProductAttribute' => 'Attribute\Service\Form\AdminProductAttribute',
        'Attribute\Service\Link\AdminProductAttribute' => 'Attribute\Service\Link\AdminProductAttribute',
        'Attribute\Service\Form\AdminBulkAttributeOption' => 'Attribute\Service\Form\AdminBulkAttributeOption',
        'Attribute\Service\Content\AdminAttributeProductView' => 'Attribute\Service\Content\AdminAttributeProductView',
        'Attribute\Service\Grid\AdminProduct' => 'Attribute\Service\Grid\AdminProduct',
        'Attribute\Service\Navigation\AdminAttributeNavigation' => 'Attribute\Service\Navigation\AdminAttributeNavigation',
        'Attribute\Service\Form\AdminCategoryAttribute' => 'Attribute\Service\Form\AdminCategoryAttribute',
        'Attribute\Service\Grid\AdminCategoryAttribute' => 'Attribute\Service\Grid\AdminCategoryAttribute',
        'Attribute\Service\Tab\AdminCategoryAttribute' => 'Attribute\Service\Tab\AdminCategoryAttribute',
        'Attribute\Service\Link\AdminCategoryAttributeLink' => 'Attribute\Service\Link\AdminCategoryAttributeLink',
        'Attribute\Service\Form\CategoryFilterProductAttribute' => 'Attribute\Service\Form\CategoryFilterProductAttribute',
        'Attribute\Service\Query\CategoryFilterProductAttribute' => 'Attribute\Service\Query\CategoryFilterProductAttribute',
        'Attribute\Service\Form\ProductAddToCartAttribute' => 'Attribute\Service\Form\ProductAddToCartAttribute',
        'Attribute\Service\Cart\CartAttribute' => 'Attribute\Service\Cart\CartAttribute',
        'Attribute\Service\Form\CartAttributeSave' => 'Attribute\Service\Form\CartAttributeSave',
        'Attribute\Service\Block\AdminProductViewAttribute' => 'Attribute\Service\Block\AdminProductViewAttribute'
        ),
    'service_manager' => array(
        'invokables' => array(
            'Attribute\Service\Grid\AdminAttribute' => 'Attribute\Service\Grid\AdminAttribute',
            'Attribute\Form\AdminAttribute\AttributeForm' => 'Attribute\Form\AdminAttribute\AttributeForm',
            'Attribute\Form\AdminAttribute\AttributeFilter' => 'Attribute\Form\AdminAttribute\AttributeFilter',
            'Attribute\Service\Form\AdminAttribute' => 'Attribute\Service\Form\AdminAttribute',
            'Attribute\Service\Link\AdminAttribute' => 'Attribute\Service\Link\AdminAttribute',
            'Attribute\Service\Tab\AdminAttribute' => 'Attribute\Service\Tab\AdminAttribute',
            'Attribute\Service\Grid\AdminAttributeOption' => 'Attribute\Service\Grid\AdminAttributeOption',
            'Attribute\Service\Grid\AdminAttributeOptionProduct' => 'Attribute\Service\Grid\AdminAttributeOptionProduct',
            'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductForm' => 'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductForm',
            'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductFilter' => 'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductFilter',
            'Attribute\Service\Form\AdminAttributeOptionProduct' => 'Attribute\Service\Form\AdminAttributeOptionProduct',
            'Attribute\Service\Tab\AdminAttributeOptionProduct' => 'Attribute\Service\Tab\AdminAttributeOptionProduct',
            'Attribute\Form\AdminAttributeOption\AttributeOptionForm' => 'Attribute\Form\AdminAttributeOption\AttributeOptionForm',
            'Attribute\Form\AdminAttributeOption\AttributeOptionFilter' => 'Attribute\Form\AdminAttributeOption\AttributeOptionFilter',
            'Attribute\Service\Form\AdminAttributeOption' => 'Attribute\Service\Form\AdminAttributeOption',
            'Attribute\Service\Link\AdminAttributeOption' => 'Attribute\Service\Link\AdminAttributeOption',
            'Attribute\Service\Grid\AdminProductAttribute' => 'Attribute\Service\Grid\AdminProductAttribute',
            'Attribute\Form\AdminProductAttribute\productAttributeForm' => 'Attribute\Form\AdminProductAttribute\productAttributeForm',
            'Attribute\Form\AdminProductAttribute\productAttributeFilter' => 'Attribute\Form\AdminProductAttribute\productAttributeFilter',
            'Attribute\Service\Form\AdminProductAttribute' => 'Attribute\Service\Form\AdminProductAttribute',
            'Attribute\Service\Link\AdminProductAttribute' => 'Attribute\Service\Link\AdminProductAttribute',
            'Attribute\Form\AdminBulkAttribute\BulkAttributeForm' => 'Attribute\Form\AdminBulkAttribute\BulkAttributeForm',
            'Attribute\Form\AdminBulkAttribute\BulkAttributeFilter' => 'Attribute\Form\AdminBulkAttribute\BulkAttributeFilter',
            'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionForm' => 'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionForm',
            'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionFilter' => 'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionFilter',
            'Attribute\Service\Form\AdminBulkAttributeOption' => 'Attribute\Service\Form\AdminBulkAttributeOption',
            'Attribute\Service\Content\AdminAttributeProductView' => 'Attribute\Service\Content\AdminAttributeProductView',
            'Attribute\Service\Grid\AdminProduct' => 'Attribute\Service\Grid\AdminProduct',
            'Attribute\Service\Navigation\AdminAttributeNavigation' => 'Attribute\Service\Navigation\AdminAttributeNavigation',
            'Attribute\Form\AdminCategoryAttribute\CategoryAttributeForm' => 'Attribute\Form\AdminCategoryAttribute\CategoryAttributeForm',
            'Attribute\Form\AdminCategoryAttribute\CategoryAttributeFilter' => 'Attribute\Form\AdminCategoryAttribute\CategoryAttributeFilter',
            'Attribute\Service\Form\AdminCategoryAttribute' => 'Attribute\Service\Form\AdminCategoryAttribute',
            'Attribute\Service\Grid\AdminCategoryAttribute' => 'Attribute\Service\Grid\AdminCategoryAttribute',
            'Attribute\Service\Tab\AdminCategoryAttribute' => 'Attribute\Service\Tab\AdminCategoryAttribute',
            'Attribute\Service\Link\AdminCategoryAttributeLink' => 'Attribute\Service\Link\AdminCategoryAttributeLink',
            'Attribute\Service\Form\CategoryFilterProductAttribute' => 'Attribute\Service\Form\CategoryFilterProductAttribute',
            'Attribute\Service\Query\CategoryFilterProductAttribute' => 'Attribute\Service\Query\CategoryFilterProductAttribute',
            'Attribute\Service\Form\ProductAddToCartAttribute' => 'Attribute\Service\Form\ProductAddToCartAttribute',
            'Attribute\Service\Cart\CartAttribute' => 'Attribute\Service\Cart\CartAttribute',
            'Attribute\Service\Form\CartAttributeSave' => 'Attribute\Service\Form\CartAttributeSave',
            'Attribute\Service\Block\AdminProductViewAttribute' => 'Attribute\Service\Block\AdminProductViewAttribute'
            ),
        'factories' => array(
            'Attribute\Form\AdminAttribute\AttributeFactory' => 'Attribute\Form\AdminAttribute\AttributeFactory',
            'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductFactory' => 'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductFactory',
            'Attribute\Form\AdminAttributeOption\AttributeOptionFactory' => 'Attribute\Form\AdminAttributeOption\AttributeOptionFactory',
            'Attribute\Form\AdminProductAttribute\productAttributeFactory' => 'Attribute\Form\AdminProductAttribute\productAttributeFactory',
            'Attribute\Form\AdminBulkAttribute\BulkAttributeFactory' => 'Attribute\Form\AdminBulkAttribute\BulkAttributeFactory',
            'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionFactory' => 'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionFactory',
            'Attribute\Form\AdminCategoryAttribute\CategoryAttributeFactory' => 'Attribute\Form\AdminCategoryAttribute\CategoryAttributeFactory'
            )
        ),
    'controllers' => array('invokables' => array(
            'Attribute\Controller\AdminAttribute' => 'Attribute\Controller\AdminAttributeController',
            'Attribute\Controller\AdminAttributeOption' => 'Attribute\Controller\AdminAttributeOptionController',
            'Attribute\Controller\AdminAttributeOptionProduct' => 'Attribute\Controller\AdminAttributeOptionProductController',
            'Attribute\Controller\AdminProductAttribute' => 'Attribute\Controller\AdminProductAttributeController',
            'Attribute\Controller\AdminBulkAttribute' => 'Attribute\Controller\AdminBulkAttributeController',
            'Attribute\Controller\AdminCategoryAttribute' => 'Attribute\Controller\AdminCategoryAttributeController'
            )),
    'view_manager' => array('template_path_stack' => array('Attribute' => __DIR__.'/../view')),
    'doctrine' => array('driver' => array(
            'Attribute_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__.'/../src/Attribute/Entity')
                ),
            'orm_default' => array('drivers' => array('Attribute\Entity' => 'Attribute_driver'))
            )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('Attribute' => __DIR__.'/../public')))
    );
