<?php
namespace Attribute\Form\AdminBulkAttributeOption;

use Common\Form\Option\AbstractFormFactory;

class BulkAttributeOptionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionForm' );
		$form->setName ( 'adminBulkAttributeOptionAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionFilter' );
	}
}
