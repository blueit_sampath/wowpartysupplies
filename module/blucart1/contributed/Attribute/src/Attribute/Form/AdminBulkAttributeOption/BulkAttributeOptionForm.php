<?php

namespace Attribute\Form\AdminBulkAttributeOption;

use Attribute\Api\AttributeApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class BulkAttributeOptionForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->formAttributeOptions ();
		
		$this->add ( array (
				'name' => 'attributes',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Generate' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function formAttributeOptions() {
		$attributes = Functions::fromPost ( 'attributes',Functions::fromQuery('attributes') );
		if (is_string ( $attributes )) {
			$attributes = explode ( ',', $attributes );
		}
		
		foreach ( $attributes as $attributeId ) {
			$attribute = AttributeApi::getAttributeById ( $attributeId );
			$options = AttributeApi::getAttributeOptionByAttributeId ( $attributeId );
			$array = array ();
			foreach ( $options as $result ) {
				$option = AttributeApi::getAttributeOptionById ( $result ['attributeOptionId'] );
				$array [$option->id] = $option->name;
			}
			$select = new Select ( 'attribute_' . $attributeId );
			$select->setLabel ( $attribute->name . ' Options' );
			$select->setAttribute ( 'multiple', 'multiple' );
			$select->setValueOptions ( $array );
			$select->setValue ( array_keys ( $array ) );
			$this->add ( $select, array (
					'priority' => 1000 
			) );
		}
	}
}