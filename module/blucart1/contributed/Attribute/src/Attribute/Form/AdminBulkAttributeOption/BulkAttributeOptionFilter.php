<?php

namespace Attribute\Form\AdminBulkAttributeOption;

use Attribute\Api\AttributeApi;
use Core\Functions;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BulkAttributeOptionFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->formAttributeOptions();
	}
	public function formAttributeOptions() {
	$attributes = Functions::fromPost ( 'attributes',Functions::fromQuery('attributes') );
		if (is_string ( $attributes )) {
			$attributes = explode ( ',', $attributes );
		}
		foreach ( $attributes as $attributeId ) {
			$attribute = AttributeApi::getAttributeById ( $attributeId );
			$options = AttributeApi::getAttributeOptionByAttributeId ( $attributeId );
			$array = array ();
			
			$this->add ( array (
					'name' => 'attribute_' . $attributeId,
					'required' => true,
			) );
		}
	}
} 