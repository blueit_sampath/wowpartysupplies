<?php
namespace Attribute\Form\AdminProductAttribute;

use Common\Form\Option\AbstractFormFactory;

class productAttributeFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminProductAttribute\productAttributeForm' );
		$form->setName ( 'adminProductAttributeAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminProductAttribute\productAttributeFilter' );
	}
}
