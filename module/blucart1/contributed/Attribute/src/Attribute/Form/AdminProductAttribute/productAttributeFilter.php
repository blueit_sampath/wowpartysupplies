<?php
namespace Attribute\Form\AdminProductAttribute;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  productAttributeFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'title',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						),
						array (
								'name' => 'StripTags'
						),
						
				)
		) );
		
		$this->add ( array (
				'name' => 'type',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						),
						array (
								'name' => 'StripTags'
						),
		
				)
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		
		
	}
} 