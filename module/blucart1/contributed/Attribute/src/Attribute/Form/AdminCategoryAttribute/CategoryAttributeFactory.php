<?php
namespace Attribute\Form\AdminCategoryAttribute;

use Common\Form\Option\AbstractFormFactory;

class CategoryAttributeFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminCategoryAttribute\CategoryAttributeForm' );
		$form->setName ( 'adminCategoryAttribute' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminCategoryAttribute\CategoryAttributeFilter' );
	}
}
