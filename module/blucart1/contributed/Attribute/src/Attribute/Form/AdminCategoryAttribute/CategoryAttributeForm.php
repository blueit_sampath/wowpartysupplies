<?php

namespace Attribute\Form\AdminCategoryAttribute;

use Attribute\Api\AttributeApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CategoryAttributeForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'attributes' );
		$select->setLabel ( 'Attributes' );
		$select->setValueOptions ( $this->getCartAttributes () );
		$select->setAttribute ( 'multiple', 'true' );
		$this->add ( $select, array (
				'priority' => 950 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCartAttributes() {
		$array = array (
				'' => '' 
		);
		$results = AttributeApi::getAttributes ();
		
		foreach ( $results as $result ) {
			$attribute = AttributeApi::getAttributeById ( $result ['attributeId'] );
			$array [$attribute->id] = $attribute->title;
		}
		return $array;
	}
}