<?php
namespace Attribute\Form\AdminBulkAttribute;

use Common\Form\Option\AbstractFormFactory;

class BulkAttributeFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminBulkAttribute\BulkAttributeForm' );
		$form->setName ( 'adminBulkAttributeAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminBulkAttribute\BulkAttributeFilter' );
	}
}
