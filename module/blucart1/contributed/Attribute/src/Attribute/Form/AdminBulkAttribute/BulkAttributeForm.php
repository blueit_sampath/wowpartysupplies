<?php

namespace Attribute\Form\AdminBulkAttribute;

use Attribute\Api\AttributeApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class BulkAttributeForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'attributes' );
		$select->setLabel ( 'Attributes' );
		$select->setAttribute('multiple', 'multiple');
		$select->setValueOptions ( $this->getCartAttributes () );
		$this->add ( $select, array (
				'priority' => 950 
		) );
		
		
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Next >' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCartAttributes() {
		$array = array ();
		$results = AttributeApi::getAttributes ();
		foreach ( $results as $result ) {
			$attributeId = $result ['attributeId'];
			$attribute = AttributeApi::getAttributeById ( $attributeId );
			$array [$attribute->id] = $attribute->name;
		}
		return $array;
	}
}