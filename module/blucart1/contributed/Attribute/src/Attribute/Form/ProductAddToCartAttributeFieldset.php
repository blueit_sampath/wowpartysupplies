<?php

namespace Attribute\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Radio;
use Attribute\Api\AttributeApi;
use Core\Functions;
use Zend\Form\Element\MultiCheckbox;
use Config\Api\ConfigApi;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use File\Api\FileApi;

class ProductAddToCartAttributeFieldset extends Fieldset implements InputFilterProviderInterface {
	protected $_inputs = array ();
	protected $_productId = '';
	public function __construct($productId = 0, $name = 'attributes') {
		$this->_productId = $productId;
		parent::__construct ( $name );
		
		$this->addAttributeElements ();
	}
	public function addAttributeElements() {
		$productId = $this->_productId;
		
		$results = AttributeApi::getAttributeByProductId ( $productId );
	
		foreach ( $results as $result ) {
		    $attributeProduct = AttributeApi::getAttributeProductById ( $result ['attributeProductId'] );
			$attribute = $attributeProduct->attribute;
			
			$results2 = AttributeApi::getAttributeOptionByAttributeId ( $attribute->id );
			$array = array ();
			foreach ( $results2 as $result2 ) {
				if (! AttributeApi::checkOrGetAttributeOptionByProductId ( $productId, $result2 ['attributeOptionId'] )) {
					continue;
				}
				$option = AttributeApi::getAttributeOptionById ( $result2 ['attributeOptionId'] );
				if(!$option){
					continue;
				}
				$link = '';
				if($option->file){
				   $link =  FileApi::formImage($option->file->path, 100, 100,array(),true);
				}
				$array [$option->name] = array(
				'label'=>    $option->title,
				'value' => $option->id,
				    'attributes' => array(
				    		'data-image' => $link,
				    ),
				);
			}
			
			if(count($array)){
				$this->addAttributeElement($attribute, $array, $attributeProduct->type, $attributeProduct->title);
			}
		}
	}
	public function addAttributeElement($attribute, $options, $type = '',$title='') {
		if ($type == 'radio') {
			$element = new Radio( 'attribute_' . $attribute->id );
		} else {
			$element = new Select ( 'attribute_' . $attribute->id );
                        $array = array('' => 'Please Select');
                        $options = $array + $options;
		}
		
		$element->setLabel ( $title ? $title : $attribute->title );
		$element->setValueOptions ( $options );
                $element->setOptions(array('inline' => true));
		$this->add ( $element );
	
		$this->_inputs [] = $element->getName ();
	}
	
	
	
	/**
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		$array = array ();
		foreach ( $this->_inputs as $input ) {
			$array [$input] = array (
					'required' => true 
			);
		}
		
		return $array;
	}
	
	
}
