<?php
namespace Attribute\Form\AdminAttributeOption;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class AttributeOptionForm extends Form

{

    public function init()
    {
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
        
        $this->add(array(
            'name' => 'attribute',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Name'
            )
        ), array(
            'priority' => 1000
        ));
        
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Title'
            )
        ), array(
            'priority' => 990
        ));
        
        $this->add(array(
            'name' => 'defaultAdjustmentSellPrice',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Default Adj. Sell Price'
            )
        ), array(
            'priority' => 980
        ));
        
        $this->add(array(
            'name' => 'defaultAdjustmentListPrice',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Default Adj. List Price'
            )
        ), array(
            'priority' => 960
        ));
        
        $this->add(array(
            'name' => 'defaultAdjustmentCostPrice',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Default Adj. Cost Price'
            )
        ), array(
            'priority' => 950
        ));
        
        $this->add(array(
            'name' => 'file',
            'type' => 'File\Form\Element\FileUpload'
        ), array(
            'priority' => 955
        ));
        
        $this->add(array(
            'name' => 'weight',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Priority'
            )
        ), array(
            'priority' => 940
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }
}