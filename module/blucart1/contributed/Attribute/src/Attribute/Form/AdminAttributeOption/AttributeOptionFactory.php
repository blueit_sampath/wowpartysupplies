<?php
namespace Attribute\Form\AdminAttributeOption;

use Common\Form\Option\AbstractFormFactory;

class AttributeOptionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttributeOption\AttributeOptionForm' );
		$form->setName ( 'adminAttributeOptionAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttributeOption\AttributeOptionFilter' );
	}
}
