<?php
namespace Attribute\Form\AdminAttributeOptionProduct;

use Common\Form\Option\AbstractFormFactory;

class AttributeOptionProductFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductForm' );
		$form->setName ( 'adminAttributeOptionProductAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductFilter' );
	}
}
