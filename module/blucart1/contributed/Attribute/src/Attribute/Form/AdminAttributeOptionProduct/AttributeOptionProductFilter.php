<?php

namespace Attribute\Form\AdminAttributeOptionProduct;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class AttributeOptionProductFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
              
        ));

        $this->add(array(
            'name' => 'sku',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'image',
            'required' => false
                )
        );
        $this->add(array(
            'name' => 'adjustmentCostPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));

        $this->add(array(
            'name' => 'adjustmentListPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));
        $this->add(array(
            'name' => 'adjustmentSellPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));

        $this->add(array(
            'name' => 'isPercentage',
            'required' => false,
        ));
    }

}
