<?php

namespace Attribute\Form\AdminAttributeOptionProduct;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class AttributeOptionProductForm extends Form {

    public function init() {
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'name' => 'product',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'readonly' => true
            ),
            'options' => array(
                'label' => 'Name'
            )
                ), array(
            'priority' => 1000
        ));

       

        $this->add(array(
            'name' => 'sku',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'SKU'
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'name' => 'adjustmentSellPrice',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Adj. Sell Price'
            )
                ), array(
            'priority' => 970
        ));

        $this->add(array(
            'name' => 'adjustmentListPrice',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Adj. List Price'
            )
                ), array(
            'priority' => 960
        ));

        $this->add(array(
            'name' => 'adjustmentCostPrice',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Adj. Cost Price'
            )
                ), array(
            'priority' => 950
        ));

        $this->add(array(
            'name' => 'image',
            'type' => 'File\Form\Element\FileUpload'
                ), array(
            'priority' => 940
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isPercentage',
            'options' => array(
                'label' => 'Is Percentage?',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 930
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
