<?php

namespace Attribute\Form;

use Attribute\Api\AttributeApi;
use Core\Functions;
use Zend\Form\Element\MultiCheckbox;
use Config\Api\ConfigApi;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class AttributeFilterFieldset extends Fieldset implements InputFilterProviderInterface {
	protected $_inputs = array ();
	public function __construct() {
		parent::__construct ( 'attributes' );
		
		$this->addAttributeElements ();
	}
	public function addAttributeElements() {
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		
		$results = AttributeApi::getAttributeByCategoryId ( $categoryId );
		
		foreach ( $results as $result ) {
			
			$attributeCategory = AttributeApi::getAttributeCategoryById ( $result ['attributeCategoryId'] );
			$attribute = $attributeCategory->attribute;
			
			$results2 = AttributeApi::getAttributeOptionByAttributeId ( $attribute->id );
			$array = array ();
			foreach ( $results2 as $result2 ) {
				
				
				$option = AttributeApi::getAttributeOptionById ( $result2 ['attributeOptionId'] );
				$array [$option->id] = $option->title;
			}
			
			
			if (count ( $array )) {
				$this->_inputs ['attribute_' . $attribute->id] = array (
						'required' => false 
				);
				$multiCheckbox = new MultiCheckbox ( 'attribute_' . $attribute->id );
				$multiCheckbox->setLabel ( $attributeCategory->title ? $attributeCategory->title : $attribute->title );
				$multiCheckbox->setValueOptions (
						$array 
				 );
				$this->add ( $multiCheckbox );
			}
		}
	}
	
	/**
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		return $this->_inputs;
		
	}
}
