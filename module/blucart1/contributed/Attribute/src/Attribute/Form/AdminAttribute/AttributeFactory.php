<?php
namespace Attribute\Form\AdminAttribute;

use Common\Form\Option\AbstractFormFactory;

class AttributeFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttribute\AttributeForm' );
		$form->setName ( 'adminAttributeAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttribute\AttributeFilter' );
	}
}
