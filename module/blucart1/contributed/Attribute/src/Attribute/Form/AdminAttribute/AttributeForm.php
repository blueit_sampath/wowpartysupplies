<?php

namespace Attribute\Form\AdminAttribute;

use Zend\Form\Element\Radio;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class AttributeForm extends Form {

    public function init() {
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Name'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Title'
            )
                ), array(
            'priority' => 990
        ));



        $radio = new Radio('type');
        $radio->setLabel('Type');
        $radio->setValueOptions(array(
            'radio' => 'Radio',
            'select' => 'Select'
        ));
        $this->add($radio, array(
            'priority' => 980
        ));


        $this->add(array(
            'name' => 'image',
            'type' => 'File\Form\Element\FileUpload'
                ), array(
            'priority' => 970
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
