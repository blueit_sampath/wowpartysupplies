<?php

namespace Attribute\Api;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Common\Api\Api;

class AttributeApi extends Api {

    protected static $_entity = '\Attribute\Entity\Attribute';
    protected static $_entityOption = '\Attribute\Entity\AttributeOption';
    protected static $_entityOptionProduct = '\Attribute\Entity\AttributeOptionProduct';
    protected static $_entityProduct = '\Attribute\Entity\AttributeProduct';
    protected static $_entityCategory = '\Attribute\Entity\AttributeCategory';
    protected static $_tempArray = array();

    public static function getAttributeById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getAttributeByName($name) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
                    'name' => $name
        ));
    }

    public static function getAttributeOptionByNameAndProductId($name, $productId) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityOptionProduct)->findOneBy(array(
                    'name' => $name,
                    'product' => $productId
        ));
    }

    public static function getAttributeOptionByName($name) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityOption)->findOneBy(array(
                    'name' => $name
        ));
    }

    public static function getAttributes() {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('attribute.id', 'attributeId');

        $item = $queryBuilder->addFrom(static::$_entity, 'attribute');
        return $queryBuilder->executeQuery();
    }

    public static function getAttributeCategoryById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityCategory, $id);
    }

    public static function getAttributeByCategoryId($categoryId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('attributeCategory.id', 'attributeCategoryId');

        $queryBuilder->addWhere('attributeCategory.category', 'attributeCategory');
        $queryBuilder->addParameter('attributeCategory', $categoryId);

        $item = $queryBuilder->addFrom(static::$_entityCategory, 'attributeCategory');
        $joinItem = new QueryJoinItem('attributeCategory.attribute', 'attribute');
        $item->addJoin($joinItem);
        $queryBuilder->addOrder('attributeCategory.weight', 'weight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getAttributeProductById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityProduct, $id);
    }

    public static function getAttributeByProductId($productId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('attributeProduct.id', 'attributeProductId');

        $queryBuilder->addWhere('attributeProduct.product', 'attributeProduct');
        $queryBuilder->addParameter('attributeProduct', $productId);

        $item = $queryBuilder->addFrom(static::$_entityProduct, 'attributeProduct');
        $joinItem = new QueryJoinItem('attributeProduct.attribute', 'attribute');
        $item->addJoin($joinItem);
        $queryBuilder->addOrder('attributeProduct.weight', 'weight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getAttributeOptionById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityOption, $id);
    }

    public static function getAttributeOptionByAttributeId($attributeId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('attributeOption.id', 'attributeOptionId');

        $queryBuilder->addWhere('attributeOption.attribute', 'attributeOptionAttribute');
        $queryBuilder->addParameter('attributeOptionAttribute', $attributeId);

        $item = $queryBuilder->addFrom(static::$_entityOption, 'attributeOption');
        $queryBuilder->addOrder('attributeOption.weight', 'weight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getAttributeOptionProductById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityOptionProduct, $id);
    }

    public static function getAttributeOptionProductByProductId($productId) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityOptionProduct)->findBy(array('product' => $productId));
    }

    // ---------------------
    public static function checkOrGetAttributeByProductIdAttributeId($productId, $attributeId) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityProduct)->findOneBy(array(
                    'product' => $productId,
                    'attribute' => $attributeId
        ));
    }

    public static function checkOrGetAttributeOptionByProductId($productId, $optionId) {
        $dql = 'select u from ' . static::$_entityOptionProduct . ' u  where u.product = ' . $productId . ' and (u.attributeOptions like \'%,' . $optionId . ',%\' or u.attributeOptions like \'' . $optionId . ',%\' or u.attributeOptions like \'%,' . $optionId . '\' or u.attributeOptions like \'' . $optionId . '\'  )';

        $em = static::getEntityManager();
        $query = $em->createQuery($dql);

        return $query->getResult();
    }

    public static function getAttributeOptionByProductIdAndData($options, $productId) {
        static::$_tempArray = array();
        static::pc_permute(array_values($options));
        if (!count(static::$_tempArray)) {
            return;
        }
        $dql = 'select u from ' . static::$_entityOptionProduct . ' u  where u.product = ' . $productId . ' and u.attributeOptions in (\'' . implode("','", static::$_tempArray) . '\')';
        $em = static::getEntityManager();
        $query = $em->createQuery($dql);
        $result = $query->getResult();
        if ($result) {
            return array_pop($result);
        }
        return false;
    }

    public static function pc_permute($items, $perms = array()) {
        if (empty($items)) {
            static::$_tempArray [] = join(',', $perms);
        } else {
            for ($i = count($items) - 1; $i >= 0; --$i) {
                $newitems = $items;
                $newperms = $perms;
                list ( $foo ) = array_splice($newitems, $i, 1);
                array_unshift($newperms, $foo);
                static::pc_permute($newitems, $newperms);
            }
        }
    }

    public static function getAttributeOptionsByProductId($productId) {
        $dql = 'select u from ' . static::$_entityOptionProduct . ' u  where u.product = ' . $productId;

        $em = static::getEntityManager();
        $query = $em->createQuery($dql);

        return $query->getResult();
    }

}
