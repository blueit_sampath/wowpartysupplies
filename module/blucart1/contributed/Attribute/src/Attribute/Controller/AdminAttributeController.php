<?php

namespace Attribute\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminAttributeController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttribute\AttributeFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-attribute-add', array (
				'attributeId' => $form->get ( 'id' )->getValue () 
		) );
	}
}