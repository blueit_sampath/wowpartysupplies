<?php

namespace Attribute\Controller;

use Core\Functions;
use Common\MVC\Controller\AbstractAdminController;

class AdminBulkAttributeController extends AbstractAdminController {
	public function attributeAction() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminBulkAttribute\BulkAttributeFactory' );
		$productId = Functions::fromRoute ( 'productId' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$attributes = $form->get ( 'attributes' )->getValue ();
				return $this->redirect ()->toRoute ( 'admin-product-attribute-option-bulk', array (
						'productId' => $productId 
				), array (
						'query' => array (
								'attributes' => $attributes 
						) 
				) );
			} else {
				$this->notValid ();
			}
		}
		return array (
				'form' => $form 
		);
	}
	public function attributeOptionAction() {
		$form = $this->getServiceLocator ()->get ( 'Attribute\Form\AdminBulkAttributeOption\BulkAttributeOptionFactory' );
		$productId = Functions::fromRoute ( 'productId' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Generated attribute options successfully' );
				return $this->redirect ()->toRoute ( 'admin-product-attribute-bulk', array (
						'productId' => $productId 
				) );
			} else {
				$this->notValid ();
			}
		}else{
			$form->getRecord();
		}
		
		return array (
				'form' => $form 
		);
	}
	
	
}