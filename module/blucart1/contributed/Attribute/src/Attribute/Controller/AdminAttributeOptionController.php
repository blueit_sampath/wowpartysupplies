<?php

namespace Attribute\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminAttributeOptionController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttributeOption\AttributeOptionFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-attribute-option-add', array (
				'attributeId' => $form->get ( 'attribute' )->getValue (),
				'attributeOptionId' => $form->get ( 'id' )->getValue () 
		) );
	}
}