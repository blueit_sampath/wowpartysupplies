<?php 
namespace Attribute\Controller;
use Core\Functions;

use Common\MVC\Controller\AbstractAdminController;

class AdminCategoryAttributeController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminCategoryAttribute\CategoryAttributeFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-category-attribute-add', array (
				'categoryId' => Functions::fromRoute('categoryId')
		) );
	}
	
	
}