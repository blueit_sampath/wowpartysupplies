<?php 
namespace Attribute\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminAttributeOptionProductController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminAttributeOptionProduct\AttributeOptionProductFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-attribute-option-add', array (
				'productId' => $form->get ( 'product' )->getValue (),
				'attributeOptionProductId' => $form->get ( 'id' )->getValue ()
		) );
	}
	
	
}