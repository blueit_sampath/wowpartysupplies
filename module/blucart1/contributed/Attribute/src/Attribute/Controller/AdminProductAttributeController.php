<?php

namespace Attribute\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminProductAttributeController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Attribute\Form\AdminProductAttribute\ProductAttributeFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-attribute-add', array (
				'productId' => $form->get ( 'product' )->getValue (),
				'attributeProductId' => $form->get ( 'id' )->getValue () 
		) );
	}
}