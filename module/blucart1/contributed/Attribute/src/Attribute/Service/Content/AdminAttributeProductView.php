<?php 
namespace Attribute\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminAttributeProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-attribute-product-view';
	protected $_contentName = 'adminAttributeProductView';
	public function getEventName() {
		return 'adminProductView';
	}
	public function getPriority() {
		return 800;
	}
	
}

