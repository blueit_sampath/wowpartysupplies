<?php

namespace Attribute\Service\Query;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use Attribute\Api\AttributeApi;
use Core\Functions;
use QueryBuilder\Option\Event\AbstractQueryBuilderEvent;

class CategoryFilterProductAttribute extends AbstractQueryBuilderEvent {
	public function getEventName() {
		return 'ProductCategory\Api\ProductCategoryApi.getProductsByCategoryId';
	}
	public function getPriority() {
		return 500;
	}
	public function preQuery() {
		parent::preQuery ();
		$queryBuilder = $this->getQueryBuilder ();
		
		$sm = Functions::getServiceLocator ();
		$form = $sm->get ( 'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFactory' );
		$form->isValid ();
		$data = $form->getData ();
		
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		
		$results = AttributeApi::getAttributeByCategoryId ( $categoryId );
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhereItem ();
		$where->setLogic(' and ');
		
		
		foreach ( $results as $result ) {
			
			$attributeCategory = AttributeApi::getAttributeCategoryById ( $result ['attributeCategoryId'] );
			$attribute = $attributeCategory->attribute;
			
			$results2 = AttributeApi::getAttributeOptionByAttributeId ( $attribute->id );
			
			if (isset ( $data ['attributes'] ['attribute_' . $attribute->id] )) {
				$array = $data ['attributes'] ['attribute_' . $attribute->id];
				
				$whereItem = new QueryWhereItem ();
				
				$where->addWhere ( $whereItem, 'attribute_' . $attribute->id );
				
				foreach ( $array as $value ) {
					$whereItem2 = new QueryWhereItem ();
					$whereItem->addWhere ( $whereItem2, 'attribute_' . $attribute->id . '_' . $value );
					// (u.attributeOptions like \'%,' . $optionId . ',%\' or
					// u.attributeOptions like \'' . $optionId . ',%\' or
					// u.attributeOptions like \'%,' . $optionId . '\' or
					// u.attributeOptions like \'' . $optionId . '\' )';
					$where1 = new QueryWhereItem ( 'attributeOptionProduct.attributeOptions', 'attributeOptions' . $value . '_1', ' like ' );
					$queryBuilder->addParameter ( 'attributeOptions' . $value . '_1', $value . ',%' );
					$whereItem2->addWhere ( $where1 );
					$where2 = new QueryWhereItem ( 'attributeOptionProduct.attributeOptions', 'attributeOptions' . $value . '_2', ' like ' );
					$queryBuilder->addParameter ( 'attributeOptions' . $value . '_2', '%,' . $value );
					$whereItem2->addWhere ( $where2 );
					$where3 = new QueryWhereItem ( 'attributeOptionProduct.attributeOptions', 'attributeOptions' . $value . '_3', ' like ' );
					$queryBuilder->addParameter ( 'attributeOptions' . $value . '_3', '%,' . $value . '%,' );
					$whereItem2->addWhere ( $where3 );
					$where4 = new QueryWhereItem ( 'attributeOptionProduct.attributeOptions', 'attributeOptions' . $value . '_4', ' like ' );
					$queryBuilder->addParameter ( 'attributeOptions' . $value . '_4', $value );
					$whereItem2->addWhere ( $where4 );
					
					
				}
			}
		}
		if(count($where->getWheres())){
			$wheres->addWhere ( $where, 'attributes' );
		}
		
		$item = $queryBuilder->getFrom ( 'productCategory' );
		$joinItem = new QueryJoinItem ( '\Attribute\Entity\AttributeOptionProduct', 'attributeOptionProduct', 'left join', 'attributeOptionProduct.product = productCategory.product' );
		$item->addJoin ( $joinItem );
		
	}
}

