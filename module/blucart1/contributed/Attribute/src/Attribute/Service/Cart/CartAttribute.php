<?php

namespace Attribute\Service\Cart;

use Core\Functions;
use Checkout\Option\CartEvent;
use Product\Api\ProductApi;
use Attribute\Api\AttributeApi;

class CartAttribute extends CartEvent {

    public function getPriority() {
        return 800;
    }

    public function validateCartItem() {
        $cart = $this->getCart();
        $cartItem = $this->getCartItem();

        $validateCartContainer = $this->getValidationContainer();
        $productId = $cartItem->getProductId();

        if (!$productId) {
            return;
        }
        $product = ProductApi::getProductById($productId);
        if (!$product) {
            return;
        }

        $results = AttributeApi::getAttributeByProductId($productId);
        if (!$results) {
            return;
        }
        $formData = $cartItem->getParam('formData');
        foreach ($results as $result) {

            $attributeProduct = AttributeApi::getAttributeProductById($result ['attributeProductId']);
            $attribute = $attributeProduct->attribute;

            if (!isset($formData ['attributes'] ['attribute_' . $attribute->id])) {
                if ($cart->getCartType() != \Checkout\Option\CartContainer::CART_PREVIEW) {
                    $validateCartContainer->add('attribute_' . $attribute->id, 'Attribute ' . $attribute->title . ' is missing');
                }
            }
        }

        return true;
    }

    public function prepareCartItem() {
        $cart = $this->getCart();
        $cartItem = $this->getCartItem();

        $productId = $cartItem->getProductId();

        if (!$productId) {
            return;
        }
        $product = ProductApi::getProductById($productId);
        if (!$product) {
            return;
        }

        $results = AttributeApi::getAttributeByProductId($productId);
        if (!$results) {
            return;
        }
        $formData = $cartItem->getParam('formData');
        if (!isset($formData ['attributes'])) {
            return;
        }
        $result = AttributeApi::getAttributeOptionByProductIdAndData($formData ['attributes'], $productId);
        if (!$result) {
            return;
        }

        if ($cartItem->getInItem('attribute')) {
            return;
        }
        if ($result->file && $result->file->path) {
            $cartItem->setImage($result->file->path);
        }

        if ($result->sku) {
            $cartItem->setSku($result->sku);
        }

       
        if ($result->adjustmentListPrice) {
            $price = $cartItem->getListPrice();

            $s = $result->adjustmentListPrice;
            if ($result->isPercentage) {
                $s = ($price * $s) / 100;
            }

            $cartItem->setListPrice($price + $s);
        }
        if ($result->adjustmentCostPrice) {
            $price = $cartItem->getCostPrice();
            $s = $result->adjustmentCostPrice;
            if ($result->isPercentage) {
                $s = ($price * $s) / 100;
            }
            $cartItem->setCostPrice($price + $s);
        }
        if ($result->adjustmentSellPrice) {
            $price = $cartItem->getSellPrice();

            $s = $result->adjustmentSellPrice;
            if ($result->isPercentage) {
                $s = ($price * $s) / 100;
            }

            $cartItem->setSellPrice($price + $s);
        }
        $title = $result->title;
        if (!$title) {
            $options = $result->attributeOptions;
            $array = explode(',', $options);
            $temp = array();
            foreach ($array as $option) {
                $op = AttributeApi::getAttributeOptionById($option);
                $p = AttributeApi::checkOrGetAttributeByProductIdAttributeId($productId, $op->attribute->id);

                $t = '';
                if ($p) {
                    $t = $p->title ? $p->title : $p->attribute->title;
                    $t .= ': ';
                }
                $temp[] = $t . $op->title;
            }

            $title = implode(' | ', $temp);
        }
        $inItem = $cartItem->addQuickInItem('attribute', $title, null, array(
            'attributeOptionProductId' => $result->id
        ));
        $inItem->setIsCurrency(false);
        $inItem->setIncludePrice(false);

        return true;
    }

}
