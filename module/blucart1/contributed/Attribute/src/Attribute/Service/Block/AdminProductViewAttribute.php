<?php 
namespace Attribute\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminProductViewAttribute extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-product-view-attribute';

	public function getBlockName() {
		return 'AdminProductViewAttribute';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminProductViewAttribute';
	}
	
}

