<?php 
namespace Attribute\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminAttribute extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminAttribute';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-attribute-add');
		$item = $linkContainer->add ( 'Add Attribute', 'Add Attribute', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

