<?php

namespace Attribute\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProductAttribute extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminAttributeOptionProduct';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$productId = Functions::fromRoute ( 'productId' );
		$u = $url ( 'admin-product-attribute', array (
				'productId' => $productId 
		) );
		$item = $linkContainer->add ( 'admin-product-attribute', 'Attributes', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-product-attribute-bulk', array (
				'productId' => $productId
		) );
		$item = $linkContainer->add ( 'admin-product-attribute-bulk', 'Generate Attributes', $u, 800 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
	
	
}

