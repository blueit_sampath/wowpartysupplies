<?php

namespace Attribute\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminAttributeOption extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminAttributeOption';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$attributeId = Functions::fromRoute ( 'attributeId' );
		$u = $url ( 'admin-attribute-option-add', array (
				'attributeId' => $attributeId 
		) );
		$item = $linkContainer->add ( 'admin-attribute-option-add', 'Add Option', $u, 1000 );
		$item->setLinkClass ( 'zoombox' );
		
		return $this;
	}
}

