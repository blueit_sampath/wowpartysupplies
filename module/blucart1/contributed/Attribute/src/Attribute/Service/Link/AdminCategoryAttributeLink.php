<?php 
namespace Attribute\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategoryAttributeLink extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategoryAttribute';
	}
	public function link() {
		$categoryId = Functions::fromRoute('categoryId');
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-category-attribute-add', array('categoryId' => $categoryId));
		$item = $linkContainer->add ( 'admin-category-attribute-add', 'Add Attribute', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

