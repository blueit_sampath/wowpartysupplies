<?php

namespace Attribute\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminAttributeOption extends AbstractMainBlucartGridEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'title',
        'weight',
        'defaultAdjustmentSellPrice',
        'defaultAdjustmentListPrice',
        'defaultAdjustmentCostPrice'
    );
    protected $_entity = '\Attribute\Entity\AttributeOption';
    protected $_entityName = 'attributeOption';

    public function getEventName() {
        return 'adminAttributeOption';
    }

    public function preSchema($e) {
        parent::preSchema($e);

        $array = array();

        $grid = $this->getGrid();

        $attributeId = Functions::fromRoute('attributeId', 0);
        $array = array();
        $grid = $this->getGrid();
        $grid->setAdditionalParameters(array(
            'attributeId' => $attributeId
        ));

        $columns = $grid->getColumns();

        $columnItem = new ColumnItem ();
        $columnItem->setField('id');
        $columnItem->setTitle('ID');
        $columnItem->setType('number');
        $columnItem->setIsPrimary(true);
        $columnItem->setEditable(false);
        $columnItem->setWeight(1000);
        $columns->addColumn('attributeOption.id', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('name');
        $columnItem->setTitle('Name');
        $columnItem->setType('string');
        $columnItem->setEditable(false);
        $columnItem->setWeight(990);
        $columns->addColumn('attributeOption.name', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('title');
        $columnItem->setTitle('Title');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('attributeOption.title', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('weight');
        $columnItem->setTitle('Priority');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(970);
        $columns->addColumn('attributeOption.weight', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('defaultAdjustmentSellPrice');
        $columnItem->setTitle('Adj. Sell Price');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columnItem->setFormat('{0:n2}');
        $columns->addColumn('attributeOption.defaultAdjustmentSellPrice', $columnItem);
        $columnItem = new ColumnItem ();
        $columnItem->setField('defaultAdjustmentListPrice');
        $columnItem->setTitle('Adj. List Price');
        $columnItem->setType('number');
        $columnItem->setFormat('{0:n2}');
        $columnItem->setEditable(true);
        $columnItem->setWeight(950);
        $columns->addColumn('attributeOption.defaultAdjustmentListPrice', $columnItem);
        $columnItem = new ColumnItem ();
        $columnItem->setField('defaultAdjustmentCostPrice');
        $columnItem->setTitle('Adj. Cost Price');
        $columnItem->setType('number');
        $columnItem->setFormat('{0:n2}');
        $columnItem->setEditable(true);
        $columnItem->setWeight(940);
        $columns->addColumn('attributeOption.defaultAdjustmentCostPrice', $columnItem);

        $this->formToolbar();

        return $columns;
    }

    public function formToolbar() {
        $grid = $this->getGrid();

        $toolbar = $grid->getToolbar();
        if (!$toolbar) {
            $toolbar = new Toolbar ();
            $grid->setToolbar($toolbar);
        }



        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('save', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('cancel', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(700);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }

    public function preRead($e) {
        parent::preRead($e);
        $attributeId = Functions::fromQuery('attributeId', 0);
        $grid = $this->getGrid();
        $queryBuilder = $this->getGrid()->getQueryBuilder();
        $item = $queryBuilder->addFrom($this->_entity, $this->_entityName);
        $joinItem = new QueryJoinItem($this->_entityName . '.attribute', 'attribute');
        $item->addJoin($joinItem);

        $item = $queryBuilder->addWhere('attribute.id', 'attributeId');
        $queryBuilder->addParameter('attributeId', $attributeId);

        return true;
    }

}
