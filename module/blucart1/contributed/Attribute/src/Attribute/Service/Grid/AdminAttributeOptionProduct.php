<?php

namespace Attribute\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminAttributeOptionProduct extends AbstractMainBlucartGridEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'adjustmentSellPrice',
        'adjustmentListPrice',
        'adjustmentCostPrice',
        'sku',
        'isPercentage'
    );
    protected $_entity = 'Attribute\Entity\AttributeOptionProduct';
    protected $_entityName = 'attributeOptionProduct';

    public function getEventName() {
        return 'adminAttributeOptionProduct';
    }

    public function preSchema($e) {
        parent::preSchema($e);

        $array = array();

        $grid = $this->getGrid();

        $productId = Functions::fromRoute('productId', 0);
        $array = array();
        $grid = $this->getGrid();
        if ($productId) {
            $grid->addAdditionalParameter('productId', $productId);
        }

        $columns = $grid->getColumns();

        $columnItem = new ColumnItem ();
        $columnItem->setField('id');
        $columnItem->setTitle('ID');
        $columnItem->setType('number');
        $columnItem->setIsPrimary(true);
        $columnItem->setEditable(false);
        $columnItem->setWeight(1000);
        $columns->addColumn('attributeOptionProduct.id', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('name');
        $columnItem->setTitle('Name');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(990);
        $columnItem->setEditable(false);
        $columns->addColumn('attributeOptionProduct.name', $columnItem);



        $columnItem = new ColumnItem ();
        $columnItem->setField('sku');
        $columnItem->setTitle('SKU');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('attributeOptionProduct.sku', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('adjustmentSellPrice');
        $columnItem->setTitle('Adj. Sell Price');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columnItem->setFormat('{0:n2}');
        $columns->addColumn('attributeOptionProduct.adjustmentSellPrice', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('adjustmentListPrice');
        $columnItem->setTitle('Adj. List Price');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(950);
        $columnItem->setFormat('{0:n2}');
        $columns->addColumn('attributeOptionProduct.adjustmentListPrice', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('adjustmentCostPrice');
        $columnItem->setTitle('Adj. Cost Price');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(940);
        $columnItem->setFormat('{0:n2}');
        $columns->addColumn('attributeOptionProduct.adjustmentCostPrice', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('isPercentage');
        $columnItem->setTitle('Is Percentage');
        $columnItem->setType('boolean');
        $columnItem->setEditable(true);
        $columnItem->setWeight(940);
        $columns->addColumn('attributeOptionProduct.isPercentage', $columnItem);

        $this->formToolbar();

        return $columns;
    }

    public function formToolbar() {
        $grid = $this->getGrid();

        $toolbar = $grid->getToolbar();
        if (!$toolbar) {
            $toolbar = new Toolbar ();
            $grid->setToolbar($toolbar);
        }

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(1000);
        $toolbar->addToolbar('save', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('cancel', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }

    public function preRead($e) {
        parent::preRead($e);
        $productId = Functions::fromQuery('productId', 0);
        $grid = $this->getGrid();
        $queryBuilder = $this->getGrid()->getQueryBuilder();
        $item = $queryBuilder->addFrom($this->_entity, $this->_entityName);
        $joinItem = new QueryJoinItem($this->_entityName . '.product', 'product');
        $item->addJoin($joinItem);

        $item = $queryBuilder->addWhere('product.id', 'productId');
        $queryBuilder->addParameter('productId', $productId);

        return true;
    }

}
