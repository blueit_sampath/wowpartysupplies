<?php

namespace Attribute\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminProductAttribute extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'type',
			'weight' 
	);
	protected $_entity = '\Attribute\Entity\AttributeProduct';
	protected $_entityName = 'attributeProduct';
	public function getEventName() {
		return 'adminProductAttribute';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		$array = array ();
		$grid = $this->getGrid ();
		$grid->addAdditionalParameter ( 'productId', $productId );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'attributeProduct.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'attributeTitle' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'attribute.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Overriden Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'attributeProduct.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Overriden Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'attributeProduct.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'type' );
		$columnItem->setTitle ( 'Type' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columnItem->setValues ( array (
				array(
						'value' => 'radio',
						'text' => 'Radio',
						
						),
				array(
						'value' => 'select',
						'text' => 'Dropdown',
				
				),
				
				
		) );
		$columns->addColumn ( 'attributeProduct.type', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'weight' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'attributeProduct.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 700 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$productId = Functions::fromQuery ( 'productId', 0 );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem ( $this->_entityName . '.attribute', 'attribute' );
		$item->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem ( $this->_entityName . '.product', 'product' );
		$item->addJoin ( $joinItem );
		
		$item = $queryBuilder->addWhere ( 'product.id', 'productId' );
		$queryBuilder->addParameter ( 'productId', $productId );
		
		return true;
	}
}
