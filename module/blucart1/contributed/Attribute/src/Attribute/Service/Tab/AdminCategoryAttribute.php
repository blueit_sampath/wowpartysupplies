<?php

namespace Attribute\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategoryAttribute extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$categoryId = Functions::fromRoute ( 'categoryId' );
		if ($categoryId) {
			$tabContainer = $this->getTabContainer ();
			$url = Functions::getUrlPlugin ();
			
			$u = $url ( 'admin-category-attribute', array (
					'categoryId' => $categoryId 
			) );
			$tabContainer->add ( 'admin-category-attribute', 'Attributes', $u, 800 );
		}
		
		return $this;
	}
}

