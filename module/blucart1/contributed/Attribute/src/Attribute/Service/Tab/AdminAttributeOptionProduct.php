<?php

namespace Attribute\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminAttributeOptionProduct extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId' );
		if ($productId) {
			
			$u = $url ( 'admin-product-attribute-option', array (
					'productId' => $productId 
			) );
			$tabContainer->add ( 'admin-product-attribute-option', 'Attributes', $u, 800 );
		}
		return $this;
	}
}

