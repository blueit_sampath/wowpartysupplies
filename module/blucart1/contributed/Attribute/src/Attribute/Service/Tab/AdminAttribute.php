<?php

namespace Attribute\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminAttribute extends AbstractTabEvent {
	public function getEventName() {
		return 'adminAttributeAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$attributeId = Functions::fromRoute ( 'attributeId', null );
		if ($attributeId) {
			$u = $url ( 'admin-attribute-add', array (
					'attributeId' => $attributeId 
			) );
			$tabContainer->add ( 'admin-attribute-add', 'General Information', $u, 1000 );
		}
		$attributeId = Functions::fromRoute ( 'attributeId', null );
		if ($attributeId) {
			$u = $url ( 'admin-attribute-option', array (
					'attributeId' => $attributeId 
			) );
			$tabContainer->add ( 'admin-attribute-option', 'Attribute Option', $u, 1000 );
		}
		
		return $this;
	}
}

