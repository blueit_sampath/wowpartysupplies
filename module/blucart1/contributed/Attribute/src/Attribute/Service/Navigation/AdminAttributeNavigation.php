<?php 
namespace Attribute\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminAttributeNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'productMain' );
		$page->addPages ( array (
				array (
						'label' => 'Attribute',
						'route' => 'admin-attribute',
						'id' => 'admin-attribute',
						'iconClass' => 'glyphicon-font' 
				) 
		) );
	}
}

