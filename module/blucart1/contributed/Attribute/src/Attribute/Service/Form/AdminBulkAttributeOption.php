<?php

namespace Attribute\Service\Form;

use Attribute\Api\AttributeApi;
use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminBulkAttributeOption extends AbstractMainFormEvent {
	protected $_entity = '\Attribute\Entity\AttributeOptionProduct';
	protected $_entityName = 'attributeOptionProduct';
	public function getFormName() {
		return 'adminBulkAttributeOptionAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$em = Functions::getEntityManager ();
		
		$form = $this->getForm ();
		$productId = Functions::fromRoute ( 'productId' );
		$attributes_string = $form->get ( 'attributes' )->getValue ();
		$this->deleteExcessAttributes ( $attributes_string, $productId );
		$attributes = explode ( ',', $attributes_string );
		$product = ProductApi::getProductById ( $productId );
		
		$buildOptions = array ();
		
		foreach ( $attributes as $attributeId ) {
			$attribute = AttributeApi::getAttributeById ( $attributeId );
			if (! AttributeApi::checkOrGetAttributeByProductIdAttributeId ( $productId, $attributeId )) {
				$entity = new \Attribute\Entity\AttributeProduct ();
				$entity->type = $attribute->type;
				$entity->attribute = $attribute;
				$entity->product = $product;
				$em->persist ( $entity );
			}
			
			$buildOptions [] = $form->get ( 'attribute_' . $attributeId )->getValue ();
		}
		
		$this->buildOptions ( $buildOptions, $productId );
		
		$em->flush ();
	}
	public function deleteExcessAttributes($attributes, $productId) {
		$em = Functions::getEntityManager ();
		$dql = 'delete from \Attribute\Entity\AttributeProduct  u where u.attribute not in (' . $attributes . ') and u.product = ' . $productId;
		$query = $em->createQuery ( $dql );
		return $query->getResult ();
	}
	public function getRecord() {
		$form = $this->getForm ();
		$attributeOptions = $form->get ( 'attributes' )->setValue ( implode ( ',', Functions::fromQuery ( 'attributes' ) ) );
	}
	public function buildOptions($buildOptions, $productId) {
		$em = $this->getEntityManager ();
		$product = ProductApi::getProductById ( $productId );
		$combos = $this->combos ( $buildOptions );
		$titles = array ();
		
		$nDeleteArray = array ();
		
		foreach ( $combos as $combo ) {
			$titlesTemp = array ();
			
			$listPrice = 0;
			$sellPrice = 0;
			$costPrice = 0;
			
			foreach ( $combo as $t ) {
				$entity = AttributeApi::getAttributeOptionById ( $t );
				$titlesTemp [$entity->id] = $entity->name . '(' . $entity->attribute->name . ')';
				$listPrice = $listPrice + $entity->defaultAdjustmentListPrice;
				$sellPrice = $sellPrice + $entity->defaultAdjustmentSellPrice;
				$costPrice = $sellPrice + $entity->defaultAdjustmentCostPrice;
			}
			if (count ( $titlesTemp )) {
				
				if (! ($model = AttributeApi::getAttributeOptionByProductIdAndData ( $combo, $productId ))) {
					
					$model = new $this->_entity ();
					$model->attributeOptions = implode ( ',', $combo );
					$model->name = implode ( '-', $titlesTemp );
					$model->sku = $product->sku . '-' . implode ( '-', $combo );
					$model->adjustmentSellPrice = $sellPrice;
					$model->adjustmentListPrice = $listPrice;
					$model->adjustmentCostPrice = $costPrice;
					$model->product = $product;
					$em->persist ( $model );
					$em->flush ();
				}
				// $titles[] = $titlesTemp;
				$nDeleteArray [] = $model->id;
			}
		}
		
		if (count ( $nDeleteArray )) {
			
			$dql = 'delete from \Attribute\Entity\AttributeOptionProduct  u where u.id not in (' . implode ( ',', $nDeleteArray ) . ') and u.product = ' . $productId;
			$em->createQuery ( $dql )->getResult ();
		}
		
		return true;
	}
	public function combos($data, &$all = array(), $group = array(), $val = null, $i = 0) {
		if (isset ( $val )) {
			array_push ( $group, $val );
		}
		
		if ($i >= count ( $data )) {
			array_push ( $all, $group );
		} else {
			foreach ( $data [$i] as $v ) {
				$this->combos ( $data, $all, $group, $v, $i + 1 );
			}
		}
		
		return $all;
	}
}
