<?php

namespace Attribute\Service\Form;

use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminProductAttribute extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'type',
			'weight' 
	);
	protected $_entity = '\Attribute\Entity\AttributeProduct';
	protected $_entityName = 'attributeProduct';
	public function getFormName() {
		return 'adminProductAttributeAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		if ($params ['product']) {
			$entity->product = ProductApi::getProductById ( $params ['product'] );
		}
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$form->get ( 'product' )->setValue ( Functions::fromRoute ( 'productId', 0 ) );
	}
}
