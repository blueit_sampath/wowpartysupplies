<?php 
namespace Attribute\Service\Form;

use Attribute\Entity\AttributeCategory;

use Attribute\Api\AttributeApi;

use Category\Api\CategoryApi;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminCategoryAttribute extends AbstractMainFormEvent {
	
	protected $_entity = '\Attribute\Entity\AttributeCategory';
	protected $_entityName = 'attributeCategory';
	public function getFormName() {
		return 'adminCategoryAttribute';
	}
	public function getPriority() {
		return 1000;
	}
	
	
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		$categoryId = Functions::fromRoute('categoryId');
	
		if ($categoryId) {
			$category = CategoryApi::getCategoryById ( $categoryId );
				
			if ($category) {
				$results = AttributeApi::getAttributeByCategoryId ( $category->id );
				$array = array ();
				if ($results) {
					foreach ( $results as $result ) {
						$array [] = ( int ) $result ['attributeId'];
					}
				}
				foreach ( $params ['attributes'] as $r ) {
					if (! in_array ( ( int ) $r, $array )) {
						$e = new AttributeCategory ();
						$e->category = $category;
						$e->attribute = AttributeApi::getAttributeById ( $r );
						$em->persist ( $e );
					}
				}
	
				$em->flush ();
			}
		}
	
		return true;
	}
	
	
}
