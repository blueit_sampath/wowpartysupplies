<?php

namespace Attribute\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;
use Checkout\Option\CartContainer;
use Attribute\Api\AttributeApi;
use OrderMain\Api\OrderApi;
use Attribute\Entity\AttributeOptionProductOrder;

class CartAttributeSave extends AbstractMainFormEvent {
	public function getFormName() {
		return 'cartCheckout';
	}
	public function getPriority() {
		return 500;
	}
	public function save() {
		$form = $this->getForm ();
		$cart = $this->getCart ();
		$em = Functions::getEntityManager ();
		/* Order Product Save */
		$cartItems = $cart->getCartItems ();
		if (count ( $cartItems )) {
			foreach ( $cartItems as $cartItem ) {
				$attributeItem = $cartItem->getInItem ( 'attribute' );
				if ($attributeItem) {
					$option = AttributeApi::getAttributeOptionProductById ( $attributeItem->getParam ( 'attributeOptionProductId' ) );
					if ($option) {
						$orderProductId = $cartItem->getParam ( 'orderProductId' );
						if ($orderProductId) {
							$orderProduct = OrderApi::getOrderProductById ( $orderProductId );
							$attributeOrder = new AttributeOptionProductOrder ();
							$attributeOrder->attributeOptionProduct = $option;
							$attributeOrder->orderProduct = $orderProduct;
							$em->persist ( $attributeOrder );
							$em->flush ();
							return $attributeOrder;
						}
					}
				}
			}
		}
		
		return;
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	protected function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
