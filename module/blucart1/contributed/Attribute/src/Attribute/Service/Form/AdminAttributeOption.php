<?php

namespace Attribute\Service\Form;

use Attribute\Api\AttributeApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use File\Api\FileApi;

class AdminAttributeOption extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'title',
        'weight',
        'defaultAdjustmentSellPrice' => array('type' => 'float'),
        'defaultAdjustmentListPrice' => array('type' => 'float'),
        'defaultAdjustmentCostPrice' => array('type' => 'float'),
    );
    protected $_entity = '\Attribute\Entity\AttributeOption';
    protected $_entityName = 'attributeOption';

    public function getFormName() {
        return 'adminAttributeOptionAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function beforeGetRecord() {
        $form = $this->getForm();
        $form->get('attribute')->setValue(Functions::fromRoute('attributeId', 0));
    }

    public function parseSaveEntity($params, $entity) {
        FileApi::deleteFiles(Functions::fromPost('file_delete', ''));

        $image = $params ['file'];

        if ($image) {
            $array = explode(',', $image);
            foreach ($array as $file) {
                $imageEntity = FileApi::createOrUpdateFile($file, $params ['title'], $params ['title']);
                $entity->file = $imageEntity;
            }
        } else {
            $entity->file = null;
        }

        if ($params ['attribute']) {
            $entity->attribute = AttributeApi::getAttributeById($params ['attribute']);
        }
    }

    public function afterGetRecord($entity) {
        $form = $this->getForm();
        if ($entity->file) {
            $form->get('file')->setValue($entity->file->path);
        }
        $form->get('name')->setAttribute('readonly', true);
    }

}
