<?php 
namespace Attribute\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class CategoryFilterProductAttribute extends AbstractMainFormEvent {
	
	public function getFormName() {
		return 'categoryFilterProduct';
	}
	public function getPriority() {
		return 800;
	}
	
	public function preInitEvent() {
		$form = $this->getForm();
		$form->add(array(
				'type' => 'Attribute\Form\AttributeFilterFieldset',
				
		));
	
	}
}
