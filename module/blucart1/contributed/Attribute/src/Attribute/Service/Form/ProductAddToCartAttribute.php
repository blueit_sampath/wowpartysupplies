<?php

namespace Attribute\Service\Form;

use Attribute\Form\ProductAddToCartAttributeFieldset;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class ProductAddToCartAttribute extends AbstractMainFormEvent {

    public function getFormName() {
        return 'productAddToCartForm';
    }

    public function getPriority() {
        return 800;
    }

    public function postInitEvent() {
      
    }

    public function preSetDataEvent() {
        $this->attachFormElements();
    }

    public function attachFormElements() {

        $form = $this->getForm();
        $product = $form->get('product')->getValue();

        if (!$form->has('attributes')) {

            $attributes = new ProductAddToCartAttributeFieldset($product, 'attributes');


            if ($attributes->getElements()) {

                $form->add($attributes,array(
                    'priority' => 800
                ));
            }
        }
    }
    

}
