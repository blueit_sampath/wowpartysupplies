<?php
return array(
    'router' => array(
        'routes' => array(
            'admin-payment-status' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/payment/status',
                    'defaults' => array(
                        'controller' => 'Payment\Controller\AdminPaymentStatus',
                        'action' => 'index'
                    )
                )
            ),
            'admin-payment-status-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/payment/status/add[/ajax/:ajax][/:paymentStatusId]',
                    'defaults' => array(
                        'controller' => 'Payment\Controller\AdminPaymentStatus',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'paymentStatusId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-payment-config' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/payment/config',
                    'defaults' => array(
                        'controller' => 'Payment\Controller\AdminPaymentConfig',
                        'action' => 'add'
                    )
                )
            ),
            'admin-order-view-payment-status' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/order/view/payment/status[/ajax/:ajax][/:orderId]',
                    'defaults' => array(
                        'controller' => 'Payment\Controller\AdminOrderViewPaymentStatus',
                        'action' => 'status',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'orderId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-payment-config-general' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/payment/config/general[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Payment\Controller\AdminPaymentConfigGeneral',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-payment-config-status' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/payment/config/status[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Payment\Controller\AdminPaymentConfigStatus',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-payment-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/payment/setting[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Payment\Controller\AdminPaymentSetting',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Payment\Service\Grid\AdminPaymentStatus' => 'Payment\Service\Grid\AdminPaymentStatus',
        'Payment\Service\Form\AdminPaymentStatus' => 'Payment\Service\Form\AdminPaymentStatus',
        'Payment\Service\Link\AdminPaymentStatus' => 'Payment\Service\Link\AdminPaymentStatus',
        'Payment\Service\Content\AdminOrderPaymentContentTop' => 'Payment\Service\Content\AdminOrderPaymentContentTop',
        'Payment\Service\Form\AdminPaymentConfig' => 'Payment\Service\Form\AdminPaymentConfig',
        'Payment\Service\Tab\AdminPaymentConfig' => 'Payment\Service\Tab\AdminPaymentConfig',
        'Payment\Service\Link\AdminOrderViewPaymentStatus' => 'Payment\Service\Link\AdminOrderViewPaymentStatus',
        'Payment\Service\Form\AdminOrderViewPaymentStatus' => 'Payment\Service\Form\AdminOrderViewPaymentStatus',
        'Payment\Service\Token\AdminOrderPaymentStatusChange' => 'Payment\Service\Token\AdminOrderPaymentStatusChange',
        'Payment\Service\Grid\AdminPaymentOrder' => 'Payment\Service\Grid\AdminPaymentOrder',
        'Payment\Service\Grid\AdminPaymentOrderUser' => 'Payment\Service\Grid\AdminPaymentOrderUser',
        'Payment\Service\Grid\AdminOrderDetailPayment' => 'Payment\Service\Grid\AdminOrderDetailPayment',
        'Payment\Service\Form\AdminOrderSearch' => 'Payment\Service\Form\AdminOrderSearch',
        'Payment\Service\Navigation\AdminPaymentNavigation' => 'Payment\Service\Navigation\AdminPaymentNavigation',
        'Payment\Service\Form\AdminPaymentConfigGeneral' => 'Payment\Service\Form\AdminPaymentConfigGeneral',
        'Payment\Service\Form\AdminPaymentConfigStatus' => 'Payment\Service\Form\AdminPaymentConfigStatus',
        'Payment\Service\Content\UserOrderPaymentContentTop' => 'Payment\Service\Content\UserOrderPaymentContentTop',
        'Payment\Service\Form\CartCheckoutPayment' => 'Payment\Service\Form\CartCheckoutPayment',
        'Payment\Service\Form\CartPaymentSave' => 'Payment\Service\Form\CartPaymentSave',
        'Payment\Service\Transaction\OrderTransactionSave' => 'Payment\Service\Transaction\OrderTransactionSave',
        'Payment\Service\Transaction\PaymentOrderMail' => 'Payment\Service\Transaction\PaymentOrderMail',
        'Payment\Service\Form\AdminPaymentSetting' => 'Payment\Service\Form\AdminPaymentSetting',
        'Payment\Service\Tab\AdminPaymentSetting' => 'Payment\Service\Tab\AdminPaymentSetting'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Payment\Service\Grid\AdminPaymentStatus' => 'Payment\Service\Grid\AdminPaymentStatus',
            'Payment\Form\AdminPaymentStatus\PaymentStatusForm' => 'Payment\Form\AdminPaymentStatus\PaymentStatusForm',
            'Payment\Form\AdminPaymentStatus\PaymentStatusFilter' => 'Payment\Form\AdminPaymentStatus\PaymentStatusFilter',
            'Payment\Service\Form\AdminPaymentStatus' => 'Payment\Service\Form\AdminPaymentStatus',
            'Payment\Service\Link\AdminPaymentStatus' => 'Payment\Service\Link\AdminPaymentStatus',
            'Payment\Service\Content\AdminOrderPaymentContentTop' => 'Payment\Service\Content\AdminOrderPaymentContentTop',
            'Payment\Form\AdminPaymentConfig\PaymentConfigForm' => 'Payment\Form\AdminPaymentConfig\PaymentConfigForm',
            'Payment\Form\AdminPaymentConfig\PaymentConfigFilter' => 'Payment\Form\AdminPaymentConfig\PaymentConfigFilter',
            'Payment\Service\Form\AdminPaymentConfig' => 'Payment\Service\Form\AdminPaymentConfig',
            'Payment\Service\Tab\AdminPaymentConfig' => 'Payment\Service\Tab\AdminPaymentConfig',
            'Payment\Service\Link\AdminOrderViewPaymentStatus' => 'Payment\Service\Link\AdminOrderViewPaymentStatus',
            'Payment\Form\AdminOrderViewPaymentStatus\StatusForm' => 'Payment\Form\AdminOrderViewPaymentStatus\StatusForm',
            'Payment\Form\AdminOrderViewPaymentStatus\StatusFilter' => 'Payment\Form\AdminOrderViewPaymentStatus\StatusFilter',
            'Payment\Service\Form\AdminOrderViewPaymentStatus' => 'Payment\Service\Form\AdminOrderViewPaymentStatus',
            'Payment\Service\Token\AdminOrderPaymentStatusChange' => 'Payment\Service\Token\AdminOrderPaymentStatusChange',
            'Payment\Service\Grid\AdminPaymentOrder' => 'Payment\Service\Grid\AdminPaymentOrder',
            'Payment\Service\Grid\AdminPaymentOrderUser' => 'Payment\Service\Grid\AdminPaymentOrderUser',
            'Payment\Service\Grid\AdminOrderDetailPayment' => 'Payment\Service\Grid\AdminOrderDetailPayment',
            'Payment\Service\Form\AdminOrderSearch' => 'Payment\Service\Form\AdminOrderSearch',
            'Payment\Service\Navigation\AdminPaymentNavigation' => 'Payment\Service\Navigation\AdminPaymentNavigation',
            'Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralForm' => 'Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralForm',
            'Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralFilter' => 'Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralFilter',
            'Payment\Service\Form\AdminPaymentConfigGeneral' => 'Payment\Service\Form\AdminPaymentConfigGeneral',
            'Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusForm' => 'Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusForm',
            'Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusFilter' => 'Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusFilter',
            'Payment\Service\Form\AdminPaymentConfigStatus' => 'Payment\Service\Form\AdminPaymentConfigStatus',
            'Payment\Service\Content\UserOrderPaymentContentTop' => 'Payment\Service\Content\UserOrderPaymentContentTop',
            'PaymentContainer' => 'Payment\Option\PaymentContainer',
            'Payment\Service\Form\CartCheckoutPayment' => 'Payment\Service\Form\CartCheckoutPayment',
            'Payment\Service\Form\CartPaymentSave' => 'Payment\Service\Form\CartPaymentSave',
            'Payment\Service\Transaction\OrderTransactionSave' => 'Payment\Service\Transaction\OrderTransactionSave',
            'Payment\Service\Transaction\PaymentOrderMail' => 'Payment\Service\Transaction\PaymentOrderMail',
            'Payment\Form\AdminPaymentSetting\PaymentSettingForm' => 'Payment\Form\AdminPaymentSetting\PaymentSettingForm',
            'Payment\Form\AdminPaymentSetting\PaymentSettingFilter' => 'Payment\Form\AdminPaymentSetting\PaymentSettingFilter',
            'Payment\Service\Form\AdminPaymentSetting' => 'Payment\Service\Form\AdminPaymentSetting',
            'Payment\Service\Tab\AdminPaymentSetting' => 'Payment\Service\Tab\AdminPaymentSetting'
        ),
        'factories' => array(
            'Payment\Form\AdminPaymentStatus\PaymentStatusFactory' => 'Payment\Form\AdminPaymentStatus\PaymentStatusFactory',
            'Payment\Form\AdminPaymentConfig\PaymentConfigFactory' => 'Payment\Form\AdminPaymentConfig\PaymentConfigFactory',
            'Payment\Form\AdminOrderViewPaymentStatus\StatusFactory' => 'Payment\Form\AdminOrderViewPaymentStatus\StatusFactory',
            'Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralFactory' => 'Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralFactory',
            'Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusFactory' => 'Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusFactory',
            'Payment\Form\AdminPaymentSetting\PaymentSettingFactory' => 'Payment\Form\AdminPaymentSetting\PaymentSettingFactory'
        ),
        'shared' => array(
            'PaymentContainer' => false
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Payment\Controller\AdminOrderViewPaymentStatus' => 'Payment\Controller\AdminOrderViewPaymentStatusController',
            'Payment\Controller\AdminPaymentStatus' => 'Payment\Controller\AdminPaymentStatusController',
            'Payment\Controller\AdminPaymentConfig' => 'Payment\Controller\AdminPaymentConfigController',
            'Payment\Controller\AdminPaymentConfigGeneral' => 'Payment\Controller\AdminPaymentConfigGeneralController',
            'Payment\Controller\AdminPaymentConfigStatus' => 'Payment\Controller\AdminPaymentConfigStatusController',
            'Payment\Controller\AdminPaymentSetting' => 'Payment\Controller\AdminPaymentSettingController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Payment' => __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'Payment_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Payment/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Payment\Entity' => 'Payment_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Payment' => __DIR__ . '/../public'
            )
        )
    )
);
