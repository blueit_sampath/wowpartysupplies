<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Payment\Option;

use Common\MVC\Controller\AbstractFrontController;
use Core\Functions;
use Zend\View\Model\ViewModel;
use Checkout\Option\CartContainer;
use Payment\Option\TransactionEvent;

class PaymentController extends AbstractFrontController {

    const GATEWAY_NAME = 'testPayment';

    public function beforeSend($orderId = null, $transactionId = null, $message = '', $params = array()) {
        $data = array();
        $data['name'] = self::GATEWAY_NAME;
        $data['orderId'] = $orderId;
        $data['transactionId'] = $transactionId;
        $data['message'] = $message;
        $data['params'] = $params;
        $this->getEventManager()->trigger(TransactionEvent::PAYMENT_BEFORE_SENDING, $message = '', $this, $data);
    }

    public function afterSend($orderId = null, $transactionId = null, $message = '', $params = array()) {
        $data = array();
        $data['name'] = self::GATEWAY_NAME;
        $data['orderId'] = $orderId;
        $data['transactionId'] = $transactionId;
        $data['params'] = $params;
        $data['message'] = $message;
        $this->getEventManager()->trigger(TransactionEvent::PAYMENT_AFTER_SENDING, $this, $data);
    }

    public function complete($orderId = null, $transactionId = null, $message = '', $params = array()) {
        $data = array();
        $data['name'] = self::GATEWAY_NAME;
        $data['orderId'] = $orderId;
        $data['transactionId'] = $transactionId;
        $data['params'] = $params;
        $data['message'] = $message;
        if ($orderId) {
            $result = \Payment\Api\PaymentApi::getPaymentOrderByOrderId($orderId);
            if ($result && $result->paymentRefNo && ($result->paymentRefNo === $transactionId)) {
                return $this->redirect()->toRoute('home');
            }
        }
        $this->getEventManager()->trigger(TransactionEvent::PAYMENT_PAID, $this, $data);

        $viewModel = new ViewModel(array(
            'orderId' => $orderId,
            'data' => $data
        ));
        $viewModel->setTemplate('payment/payment/complete');
        return $viewModel;
    }

    public function pending($orderId = null, $transactionId = null, $message = '', $params = array()) {
        $data = array();
        $data['name'] = self::GATEWAY_NAME;
        $data['orderId'] = $orderId;
        $data['transactionId'] = $transactionId;
        $data['params'] = $params;
        $data['message'] = $message;
        $this->getEventManager()->trigger(TransactionEvent::PAYMENT_PENDING, $this, $data);

        $viewModel = new ViewModel(array(
            'orderId' => $orderId,
            'data' => $data
        ));
        $viewModel->setTemplate('payment/payment/pending');
        return $viewModel;
    }

    public function cancelled($orderId = null, $transactionId = null, $message = '', $params = array()) {
        $data = array();
        $data['name'] = self::GATEWAY_NAME;
        $data['orderId'] = $orderId;
        $data['transactionId'] = $transactionId;
        $data['params'] = $params;
        $data['message'] = $message;
        $this->getEventManager()->trigger(TransactionEvent::PAYMENT_CANCELLED, $this, $data);

        $viewModel = new ViewModel(array(
            'orderId' => $orderId,
            'data' => $data
        ));
        $viewModel->setTemplate('payment/payment/cancelled');
        return $viewModel;
    }

    public function error($orderId = null, $transactionId = null, $message = '', $params = array()) {
        $data = array();
        $data['name'] = self::GATEWAY_NAME;
        $data['orderId'] = $orderId;
        $data['transactionId'] = $transactionId;
        $data['params'] = $params;
        $data['message'] = $message;
        $this->getEventManager()->trigger(TransactionEvent::PAYMENT_CANCELLED, $this, $data);

        $viewModel = new ViewModel(array(
            'orderId' => $orderId,
            'data' => $data,
            'message' => $message
        ));
        $viewModel->setTemplate('payment/payment/error');
        return $viewModel;
    }

    /**
     *
     * @return CartContainer
     */
    public function getCart() {
        $sm = $this->getServiceLocator();

        return $sm->get('Cart');
    }

    public function validatePath() {
        $referer = $this->getRequest()->getHeader('referer');
        if (!$referer) {
            return false;
        }
        if (!$referer->getUri()) {
            return false;
        }

        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\RendererInterface');
        $path = $renderer->url('checkout');

        $uri = $referer->getUri();
        if (!fnmatch('*' . $path, $referer->getUri())) {
            return false;
        }

        return true;
    }

}
