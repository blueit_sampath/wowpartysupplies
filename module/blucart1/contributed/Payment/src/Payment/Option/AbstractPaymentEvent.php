<?php
namespace Payment\Option;

use Checkout\Option\PaymentItem;
use Checkout\Option\CartContainer;
use Checkout\Option\Validation\ValidationContainer;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;
use Payment;
use Core\Functions;

abstract class AbstractPaymentEvent extends AbstractEvent
{

    protected $paymentContainer = null;

    protected $paymentItem = null;

    abstract public function getName();

    public function getTitle()
    {
        return $this->getName();
    }

    public function getPriority()
    {
        return null;
    }

    public function attach(EventManagerInterface $events)
    {
        $priority = $this->getPriority();
        $sharedEventManager = $events->getSharedManager();
        
        $this->_listeners[] = $sharedEventManager->attach('*', PaymentContainer::PAYMENT_GET_METHOD, array(
            $this,
            'prePaymentGetMethod'
        ), $priority);
        
        return $this;
    }

    public function prePaymentGetMethod($e)
    {
        $paymentContainer = $e->getTarget();
        $this->setPaymentContainer($paymentContainer);
        return $this->paymentGetMethod();
    }

    protected function paymentGetMethod()
    {
        $paymentContainer = $this->getPaymentContainer();
        $item = $paymentContainer->add($this->getName(), $this->getTitle(), $this->getUrl());
        return $item;
    }

    public function getUrl()
    {
        $routeName = $this->getRouteName();
        $cart = $this->getCart();
        if ($routeName) {
            $url = Functions::getUrlPlugin();
            return $url($this->getRouteName(), array(
                'orderId' => $cart->getParam('orderId')
            ));
        }
        return '';
    }

    public function getRouteName()
    {
        return '';
    }

    /**
     *
     * @return PaymentContainer
     */
    public function getPaymentContainer()
    {
        return $this->paymentContainer;
    }

    /**
     *
     * @return PaymentItem
     */
    public function getPaymentItem()
    {
        return $this->paymentItem;
    }

    /**
     *
     * @param field_type $paymentContainer            
     * @param
     *            class_name
     */
    public function setPaymentContainer($paymentContainer)
    {
        $this->paymentContainer = $paymentContainer;
        return $this;
    }

    /**
     *
     * @param field_type $paymentItem            
     * @param
     *            class_name
     */
    public function setPaymentItem($paymentItem)
    {
        $this->paymentItem = $paymentItem;
        return $this;
    }

    /**
     *
     * @return CartContainer
     */
    public function getCart()
    {
        $sm = Functions::getServiceLocator();
        return $sm->get('Cart');
    }
}
