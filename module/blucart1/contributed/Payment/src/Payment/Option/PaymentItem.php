<?php
namespace Payment\Option;

use Core\Item\Item;

class PaymentItem extends Item
{

    protected $_title;

    protected $_url;

    protected $_params = array();

    /**
     *
     * @return string $_title
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     *
     * @param field_type $_title            
     * @param
     *            class_name
     */
    public function setTitle($_title)
    {
        $this->_title = $_title;
        return $this;
    }

    /**
     *
     * @return string $_value
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     *
     * @param field_type $_value            
     * @param
     *            class_name
     */
    public function setUrl($_url)
    {
        $this->_url = $_url;
        return $this;
    }

    /**
     *
     * @return string $_params
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     *
     * @param multitype: $_params            
     * @param
     *            class_name
     */
    public function setParams($_params)
    {
        $this->_params = $_params;
        return $this;
    }

    public function addParam($key, $value)
    {
        $this->_params[$key] = $value;
        
        return $this;
    }

    public function getParam($key)
    {
        if (isset($this->_params[$key])) {
            return $this->_params[$key];
        }
        return false;
    }

    public function removeParam($key)
    {
        if (isset($this->_params[$key])) {
            unset($this->_params[$key]);
            return true;
        }
        return false;
    }
}
