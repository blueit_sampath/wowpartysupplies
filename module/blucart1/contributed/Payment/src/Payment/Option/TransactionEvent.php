<?php
namespace Payment\Option;

use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;
use Payment;

class TransactionEvent extends AbstractEvent
{

    protected $_orderId = null;

    protected $_transactionId = null;

    protected $_message = '';

    const PAYMENT_PAID = 'payment-paid';

    const PAYMENT_PENDING = 'payment-pending';

    const PAYMENT_CANCELLED = 'payment-cancelled';

    const PAYMENT_BEFORE_SENDING = 'payment-before-sending';

    const PAYMENT_AFTER_SENDING = 'payment-after-sending';

    const PAYMENT_ERROR = 'payment-error';

    public function getPriority()
    {
        return null;
    }

    public function attach(EventManagerInterface $events)
    {
        $priority = $this->getPriority();
        $sharedEventManager = $events->getSharedManager();
        
        $this->_listeners[] = $sharedEventManager->attach('*', self::PAYMENT_PAID, array(
            $this,
            'prePaymentPaid'
        ), $priority);
        
        $this->_listeners[] = $sharedEventManager->attach('*', self::PAYMENT_PENDING, array(
            $this,
            'prePaymentPending'
        ), $priority);
        
        $this->_listeners[] = $sharedEventManager->attach('*', self::PAYMENT_CANCELLED, array(
            $this,
            'prePaymentCancelled'
        ), $priority);
        
        $this->_listeners[] = $sharedEventManager->attach('*', self::PAYMENT_BEFORE_SENDING, array(
            $this,
            'prePaymentBeforeSending'
        ), $priority);
        
        $this->_listeners[] = $sharedEventManager->attach('*', self::PAYMENT_AFTER_SENDING, array(
            $this,
            'prePaymentAfterSending'
        ), $priority);
        
        $this->_listeners[] = $sharedEventManager->attach('*', self::PAYMENT_ERROR, array(
            $this,
            'prePaymentError'
        ), $priority);
        
        return $this;
    }

    public function prePaymentPaid($e)
    {
        $orderId = $e->getParam('orderId');
        $this->setOrderId($orderId);
        $transactionId = $e->getParam('transactionId');
        $this->setTransactionId($transactionId);
        $message = $e->getParam('message');
        $this->setMessage($message);
        $this->setEvent($e);
        return $this->paymentPaid();
    }

    public function paymentPaid()
    {}

    public function prePaymentPending($e)
    {
        $orderId = $e->getParam('orderId');
        $this->setOrderId($orderId);
        $transactionId = $e->getParam('transactionId');
        $this->setTransactionId($transactionId);
        $message = $e->getParam('message');
        $this->setMessage($message);
        $this->setEvent($e);
        return $this->paymentPending();
    }

    public function paymentPending()
    {}

    public function prePaymentCancelled($e)
    {
        $orderId = $e->getParam('orderId');
        $this->setOrderId($orderId);
        $transactionId = $e->getParam('transactionId');
        $this->setTransactionId($transactionId);
        $message = $e->getParam('message');
        $this->setMessage($message);
        $this->setEvent($e);
        return $this->paymentCancelled();
    }

    public function paymentCancelled()
    {}

    public function prePaymentBeforeSending($e)
    {
        $orderId = $e->getParam('orderId');
        $this->setOrderId($orderId);
        $transactionId = $e->getParam('transactionId');
        $this->setTransactionId($transactionId);
        $message = $e->getParam('message');
        $this->setMessage($message);
        $this->setEvent($e);
        return $this->paymentBeforeSending();
    }

    public function paymentBeforeSending()
    {}

    public function prePaymentAfterSending($e)
    {
        $orderId = $e->getParam('orderId');
        $this->setOrderId($orderId);
        $transactionId = $e->getParam('transactionId');
        $this->setTransactionId($transactionId);
        $message = $e->getParam('message');
        $this->setMessage($message);
        $this->setEvent($e);
        return $this->paymentAfterSending();
    }

    public function paymentAfterSending()
    {}

    public function prePaymentError($e)
    {
        $orderId = $e->getParam('orderId');
        $this->setOrderId($orderId);
        $transactionId = $e->getParam('transactionId');
        $this->setTransactionId($transactionId);
        $message = $e->getParam('message');
        $this->setMessage($message);
        $this->setEvent($e);
        return $this->paymentError();
    }

    public function paymentError()
    {}

    /**
     *
     * @return string $_orderId
     */
    public function getOrderId()
    {
        return $this->_orderId;
    }

    /**
     *
     * @param field_type $_orderId            
     * @param
     *            class_name
     */
    public function setOrderId($_orderId)
    {
        $this->_orderId = $_orderId;
        return $this;
    }

    /**
     *
     * @return string $_transactionId
     */
    public function getTransactionId()
    {
        return $this->_transactionId;
    }

    /**
     *
     * @param field_type $_transactionId            
     * @param
     *            class_name
     */
    public function setTransactionId($_transactionId)
    {
        $this->_transactionId = $_transactionId;
        return $this;
    }

    /**
     *
     * @return the $_message
     */
    public function getMessage()
    {
        return $this->_message;
    }

    /**
     *
     * @param string $_message            
     */
    public function setMessage($_message)
    {
        $this->_message = $_message;
        return $this;
    }
}
