<?php
namespace Payment\Option;

use Core\Item\Container\ItemContainer;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Payment\Option\PaymentItem;

class PaymentContainer extends ItemContainer implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{

    protected $_services;

    protected $_events;

    const PAYMENT_GET_METHOD = 'payment-get-method';

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->_services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->_services->getServiceLocator();
    }

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    public function getEventManager()
    {
        if (null === $this->_events) {
            $this->setEventManager(new EventManager());
        }
        return $this->_events;
    }

    public function prepare($params = array())
    {
        $this->getEventManager()->trigger(self::PAYMENT_GET_METHOD, $this, $params);
    }

    /**
     *
     * @param string $name            
     * @param string $title            
     * @param string $url            
     * @param multitype $array            
     * @param string $weight            
     * @return \Payment\Option\PaymentItem
     */
    public function add($name, $title, $url = '', $array = array(), $weight = null)
    {
        $item = new PaymentItem($name);
        $item->setTitle($title);
        $item->setUrl($url);
        $item->setParams($array);
        $item->setWeight($weight);
        $this->addItem($item);
        return $item;
    }
}
