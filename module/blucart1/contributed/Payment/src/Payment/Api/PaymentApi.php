<?php
namespace Payment\Api;

use Common\Api\Api;
use Config\Api\ConfigApi;

class PaymentApi extends Api
{

    protected static $_orderEntity = '\Payment\Entity\PaymentOrder';

    public static function getPaymentOrderByOrderId($orderId)
    {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_orderEntity)->findOneBy(array(
            'orderMain' => $orderId
        ));
    }

    public static function hasPaymentModuleEnabled($moduleName)
    {
        $value = ConfigApi::getConfigByKey('PAYMENT_METHOD', null);
        
        if ($value) {
            $value = explode(',', $value);
            if (in_array($moduleName, $value)) {
                return true;
            }
        }
        return false;
    }
}
