<?php
namespace Payment\Api;

use Common\Api\Api;

class PaymentStatusApi extends Api
{

    protected static $_entity = '\Payment\Entity\PaymentStatus';

    public static function getPaymentStatusById($id)
    {
        $em = static::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getPaymentStatusByOrderId($orderId)
    {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
            'orderMain' => $orderId
        ));
    }

    public static function getPaymentStatuses()
    {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('paymentStatus.id', 'paymentStatusId');
        $queryBuilder->addOrder('paymentStatus.weight', 'paymentStatusWeight', 'desc');
        $item = $queryBuilder->addFrom(static::$_entity, 'paymentStatus');
        return $queryBuilder->executeQuery();
    }
}
