<?php
namespace Payment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminPaymentSettingController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentSetting\PaymentSettingFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-payment-setting');
    }
}