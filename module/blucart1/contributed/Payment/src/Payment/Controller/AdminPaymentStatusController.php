<?php
namespace Payment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminPaymentStatusController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentStatus\PaymentStatusFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-payment-status-add', array(
            'paymentStatusId' => $form->get('id')
                ->getValue()
        ));
    }
}