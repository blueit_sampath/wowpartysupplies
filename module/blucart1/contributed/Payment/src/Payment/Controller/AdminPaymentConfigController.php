<?php
namespace Payment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminPaymentConfigController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfig\PaymentConfigFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-payment-config');
    }
}