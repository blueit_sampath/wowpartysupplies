<?php
namespace Payment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminPaymentConfigStatusController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-payment-config-status');
    }
}