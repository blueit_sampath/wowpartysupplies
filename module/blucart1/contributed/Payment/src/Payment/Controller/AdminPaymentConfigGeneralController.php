<?php
namespace Payment\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminPaymentConfigGeneralController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-payment-config-general');
    }
}