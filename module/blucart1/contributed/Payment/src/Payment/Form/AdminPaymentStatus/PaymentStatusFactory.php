<?php
namespace Payment\Form\AdminPaymentStatus;

use Common\Form\Option\AbstractFormFactory;

class PaymentStatusFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('Payment\Form\AdminPaymentStatus\PaymentStatusForm');
        $form->setName('adminPaymentStatusAdd');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentStatus\PaymentStatusFilter');
    }
}
