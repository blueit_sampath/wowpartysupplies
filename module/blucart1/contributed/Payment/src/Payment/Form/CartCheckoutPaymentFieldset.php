<?php
namespace Payment\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Form\Element\Radio;
use Core\Functions;
use Payment\Option\PaymentContainer;
use Checkout\Option\CartContainer;
use Zend\Form\Element\Text;
use Payment\Api\PaymentApi;

class CartCheckoutPaymentFieldset extends Fieldset implements InputFilterProviderInterface
{

    public function __construct($name = 'cartCheckoutPayment')
    {
        parent::__construct($name);
        
        $array = $this->getPaymentGateways();
        if (count($array) && count($array) > 1) {
            
            $radio = new Radio('payment');
            $radio->setLabel('Please choose a payment type');
            $radio->setValueOptions($array);
            $this->add($radio, array(
                'priority' => 1000
            ));
        }
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array \
     */
    public function getInputFilterSpecification()
    {
        $array = $this->getPaymentGateways();
        if (count($array)) {
            return array(
                'payment' => array(
                    'required' => true
                )
            );
        }
        return array();
    }

    /**
     *
     * @return multitype: multitype:string
     */
    public function getPaymentGateways()
    {
        $array = array();
        $cart = $this->getCart();
        $paymentContainer = $this->getPaymentContainer();
        $paymentContainer->prepare();
        $items = $paymentContainer->getItems();
        foreach ($items as $item) {
            if (PaymentApi::hasPaymentModuleEnabled($item->getId())) {
                $array[$item->getId()] = $item->getTitle();
            }
        }
        
        return $array;
    }

    /**
     *
     * @return PaymentContainer
     */
    public function getPaymentContainer()
    {
        $sm = Functions::getServiceLocator();
        return $sm->get('PaymentContainer');
    }

    /**
     *
     * @return CartContainer
     */
    public function getCart()
    {
        $sm = Functions::getServiceLocator();
        return $sm->get('Cart');
    }
}
