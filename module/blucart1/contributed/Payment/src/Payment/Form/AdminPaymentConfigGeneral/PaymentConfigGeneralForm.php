<?php
namespace Payment\Form\AdminPaymentConfigGeneral;

use OrderMain\Api\OrderStatusApi;
use Payment\Api\PaymentStatusApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class PaymentConfigGeneralForm extends Form

{

    public function init()
    {
        $this->setAttribute('method', 'post');
        
        $array = $this->getPaymentStatus();
        
        $select = new Select('PAYMENT_STATUS_CHECKOUT');
        $select->setLabel('Payment status in checkout');
        $select->setValueOptions($array);
        $this->add($select, array(
            'priority' => 1000
        ));
        
        $select = new Select('PAYMENT_STATUS_PENDING');
        $select->setLabel('Payment status on pending');
        $select->setValueOptions($array);
        $this->add($select, array(
            'priority' => 1000
        ));
        
        $select = new Select('PAYMENT_STATUS_CONFIRMED');
        $select->setLabel('Payment status on confirmed');
        $select->setValueOptions($array);
        $this->add($select, array(
            'priority' => 1000
        ));
        
        $select = new Select('PAYMENT_STATUS_CANCELLED');
        $select->setLabel('Payment status on cancelled');
        $select->setValueOptions($array);
        $this->add($select, array(
            'priority' => 1000
        ));
        
        $array = $this->getOrderStatus();
        
        $select = new Select('ORDER_PAYMENT_STATUS_PENDING');
        $select->setLabel('Order status on payment pending');
        $select->setValueOptions($array);
        $this->add($select, array(
            'priority' => 1000
        ));
        
        $select = new Select('ORDER_PAYMENT_STATUS_CONFIRMED');
        $select->setLabel('Order status on payment confirmed');
        $select->setValueOptions($array);
        $this->add($select, array(
            'priority' => 1000
        ));
        
        $select = new Select('ORDER_PAYMENT_STATUS_CANCELLED');
        $select->setLabel('Order status on payment cancelled');
        $select->setValueOptions($array);
        $this->add($select, array(
            'priority' => 1000
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }

    public function getPaymentStatus()
    {
        $array = array(
            '' => ''
        );
        
        $statuses = PaymentStatusApi::getPaymentStatuses();
        foreach ($statuses as $statusId) {
            $status = PaymentStatusApi::getPaymentStatusById($statusId['paymentStatusId']);
            
            $array[$status->id] = $status->name;
        }
        return $array;
    }

    public function getOrderStatus()
    {
        $array = array(
            '' => ''
        );
        
        $statuses = OrderStatusApi::getOrderStatuses();
        foreach ($statuses as $statusId) {
            $status = OrderStatusApi::getOrderStatusById($statusId['orderStatusId']);
            $array[$status->id] = $status->name;
        }
        return $array;
    }
}