<?php
namespace Payment\Form\AdminPaymentConfigGeneral;

use Common\Form\Option\AbstractFormFactory;

class PaymentConfigGeneralFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralForm');
        $form->setName('adminPaymentConfigGeneral');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfigGeneral\PaymentConfigGeneralFilter');
    }
}
