<?php
namespace Payment\Form\AdminPaymentSetting;

use Zend\InputFilter\InputFilter;

class PaymentSettingFilter extends InputFilter

{

    protected $inputFilter;

    public function __construct()
    {
        $this->add(array(
            'name' => 'PAYMENT_METHOD',
            'required' => false
        ));
    }
} 