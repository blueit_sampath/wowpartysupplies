<?php
namespace Payment\Form\AdminPaymentSetting;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use Zend\Form\Element\MultiCheckbox;

class PaymentSettingForm extends Form

{

    public function init()
    {
        $array = $this->getPaymentMethods();
        if (count($array)) {
            $element = new MultiCheckbox('PAYMENT_METHOD');
            $element->setLabel('Payment Modules');
            $element->setValueOptions($array);
            $this->add($element, array(
                'priority' => 1000
            ));
        }
        
        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }

    public function getPaymentMethods()
    {
        $array = array();
        $sm = Functions::getServiceLocator();
        $paymentContainer = $sm->get('PaymentContainer');
        $paymentContainer->prepare();
        $items = $paymentContainer->getItems();
        foreach ($items as $item) {
            $array[$item->getId()] = $item->getTitle() . ' ( ' . $item->getId() . ' ) ';
        }
        return $array;
    }
}