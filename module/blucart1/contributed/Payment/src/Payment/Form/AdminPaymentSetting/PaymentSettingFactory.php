<?php
namespace Payment\Form\AdminPaymentSetting;

use Common\Form\Option\AbstractFormFactory;

class PaymentSettingFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('Payment\Form\AdminPaymentSetting\PaymentSettingForm');
        $form->setName('adminPaymentSetting');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentSetting\PaymentSettingFilter');
    }
}
