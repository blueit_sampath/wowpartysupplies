<?php
namespace Payment\Form\AdminPaymentConfigStatus;

use Common\Form\Option\AbstractFormFactory;

class PaymentConfigStatusFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusForm');
        $form->setName('adminPaymentConfigStatus');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfigStatus\PaymentConfigStatusFilter');
    }
}
