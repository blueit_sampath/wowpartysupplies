<?php
namespace Payment\Form\AdminPaymentConfigStatus;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class PaymentConfigStatusForm extends Form

{

    public function init()
    {
        $this->add(array(
            'name' => 'PAYMENT_STATUS_DEFAULT_SUBJECT',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Default Subject'
            )
        ), array(
            'priority' => 1000
        ));
        $this->add(array(
            'name' => 'PAYMENT_STATUS_DEFAULT_HTMLMESSAGE',
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'wysiwg_full'
            ),
            'options' => array(
                'label' => 'Default Html Message'
            )
        ), array(
            'priority' => 990
        ));
        
        $this->add(array(
            'name' => 'PAYMENT_STATUS_DEFAULT_PLAINMESSAGE',
            'attributes' => array(
                'type' => 'textarea'
            )
            ,
            'options' => array(
                'label' => 'Default Plain Message'
            )
        ), array(
            'priority' => 990
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }
}