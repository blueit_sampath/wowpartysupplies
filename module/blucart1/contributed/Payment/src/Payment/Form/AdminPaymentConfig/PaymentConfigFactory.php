<?php
namespace Payment\Form\AdminPaymentConfig;

use Common\Form\Option\AbstractFormFactory;

class PaymentConfigFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfig\PaymentConfigForm');
        $form->setName('adminPaymentConfig');
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminPaymentConfig\PaymentConfigFilter');
    }
}
