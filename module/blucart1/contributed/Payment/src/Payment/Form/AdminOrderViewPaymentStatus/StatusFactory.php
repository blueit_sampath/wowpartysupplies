<?php
namespace Payment\Form\AdminOrderViewPaymentStatus;

use Common\Form\Option\AbstractFormFactory;

class StatusFactory extends AbstractFormFactory
{

    public function getForm()
    {
        $form = $this->getServiceLocator()->get('Payment\Form\AdminOrderViewPaymentStatus\StatusForm');
        $form->setName('adminOrderViewPaymentStatus');
        
        return $form;
    }

    public function getFormFilter()
    {
        return $this->getServiceLocator()->get('Payment\Form\AdminOrderViewPaymentStatus\StatusFilter');
    }
}
