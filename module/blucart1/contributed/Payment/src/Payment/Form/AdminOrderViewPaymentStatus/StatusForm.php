<?php
namespace Payment\Form\AdminOrderViewPaymentStatus;

use Payment\Api\PaymentStatusApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class StatusForm extends Form

{

    public function init()
    {
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'order',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
        
        $select = new Select('paymentStatus');
        $select->setLabel('Payment Status');
        $select->setValueOptions($this->getPaymentStatus());
        $this->add($select, array(
            'priority' => 1000
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'notifyCustomer',
            'options' => array(
                'label' => 'Notify Customer',
                'use_hidden_element' => true
            )
        ), array(
            'priority' => 990
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }

    public function getPaymentStatus()
    {
        $array = array(
            '' => ''
        );
        
        $statuses = PaymentStatusApi::getPaymentStatuses();
        foreach ($statuses as $statusId) {
            $status = PaymentStatusApi::getPaymentStatusById($statusId['paymentStatusId']);
            $array[$status->id] = $status->name;
        }
        return $array;
    }
}