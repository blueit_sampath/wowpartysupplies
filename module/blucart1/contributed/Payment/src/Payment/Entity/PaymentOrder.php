<?php
namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PaymentOrder
 *
 * @ORM\Table(name="payment_order")
 * @ORM\Entity
 */
class PaymentOrder
{

    /**
     *
     * @var integer @ORM\Column(name="id", type="integer", nullable=false)
     *      @ORM\Id
     *      @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var float @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     *
     * @var string @ORM\Column(name="payment_type", type="string", length=255, nullable=true)
     */
    private $paymentType;

    /**
     *
     * @var string @ORM\Column(name="payment_ref_no", type="string", length=255, nullable=true)
     */
    private $paymentRefNo;

    /**
     *
     * @var \PaymentStatus @ORM\ManyToOne(targetEntity="PaymentStatus")
     *      @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="payment_status_id", referencedColumnName="id",nullable=true,onDelete="SET NULL")
     *      })
     */
    private $paymentStatus;

    /**
     *
     * @var \OrderMain @ORM\ManyToOne(targetEntity="\OrderMain\Entity\OrderMain")
     *      @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="order_main_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     *      })
     */
    private $orderMain;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="create")
     */
    private $createdDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="update")
     */
    private $updatedDate;

    public function __get($property)
    {
        if (\method_exists($this, "get" . \ucfirst($property))) {
            $method_name = "get" . \ucfirst($property);
            return $this->$method_name();
        } else {
            
            if (is_object($this->$property)) {
                // return $this->$property->id;
            }
            return $this->$property;
        }
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property            
     * @param mixed $value            
     *
     */
    public function __set($property, $value)
    {
        if (\method_exists($this, "set" .\ucfirst($property))) {
            $method_name = "set" .\ucfirst($property);
            
            $this->$method_name($value);
        } else
            $this->$property = $value;
    }

    public function __isset($name)
    {
        return true;
    }
}
