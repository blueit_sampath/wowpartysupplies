<?php
namespace Payment\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminOrderViewPaymentStatus extends AbstractLinkEvent
{

    public function getEventName()
    {
        return 'adminOrderView';
    }

    public function link()
    {
        $linkContainer = $this->getLinkContainer();
        $url = Functions::getUrlPlugin();
        $orderId = Functions::fromRoute('orderId');
        
        $u = $url('admin-order-view-payment-status', array(
            'orderId' => $orderId
        ));
        $item = $linkContainer->add('admin-order-view-payment-status', 'Change Payment Status', $u, 900);
        $item->setLinkClass('zoombox ');
        
        return $this;
    }
}

