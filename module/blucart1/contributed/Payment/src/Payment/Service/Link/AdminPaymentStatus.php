<?php
namespace Payment\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminPaymentStatus extends AbstractLinkEvent
{

    public function getEventName()
    {
        return 'adminPaymentStatus';
    }

    public function link()
    {
        $linkContainer = $this->getLinkContainer();
        $url = Functions::getUrlPlugin();
        
        $u = $url('admin-payment-status-add');
        $item = $linkContainer->add('admin-payment-status-add', 'Add Payment Status', $u, 1000);
        $item->setLinkClass('zoombox ');
        
        return $this;
    }
}

