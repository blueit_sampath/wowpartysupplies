<?php

namespace Payment\Service\Transaction;

use OrderMain\Api\OrderApi;
use Payment\Option\TransactionEvent;
use Config\Api\ConfigApi;
use Common\Option\Token\TokenItem;
use Common\Option\Token\TokenContainer;
use Common\Form\Option\AbstractFrontMainFormEvent;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Pdf\Option\Pdf;

class PaymentOrderMail extends TransactionEvent {

    public function getPriority() {
        return 100;
    }

    public function paymentPaid() {
        $em = Functions::getEntityManager();
        $orderId = $this->getOrderId();
        if (!$orderId) {
            return;
        }
        $orderMain = OrderApi::getOrderById($orderId);
        if ($orderMain) {
            $this->sendEmail($orderMain);
        }
        return true;
    }

    public function paymentPending() {
        $em = Functions::getEntityManager();
        $orderId = $this->getOrderId();
        if (!$orderId) {
            return;
        }
        $orderMain = OrderApi::getOrderById($orderId);
        if ($orderMain) {
            $this->sendEmail($orderMain);
        }
        return true;
    }

    /**
     *
     * @return \Common\Option\Token\TokenContainer
     */
    public function sendEmail($orderMain) {
        $subject = ConfigApi::getConfigByKey('ORDER_CUSTOMER_NOTIFICATION_SUBJECT');
        $bodyHtml = ConfigApi::getConfigByKey('ORDER_CUSTOMER_NOTIFICATION_HTMLMESSAGE');
        $bodyPlain = ConfigApi::getConfigByKey('ORDER_CUSTOMER_NOTIFICATION_PLAINMESSAGE');

        $fromName = ConfigApi::getConfigByKey('ORDER_NAME', '');
        $fromEmail = ConfigApi::getConfigByKey('ORDER_EMAIL');

        if (!$fromEmail) {
            return;
        }

        if ($subject && ($bodyHtml || $bodyPlain)) {

            $tokenContainer = $this->getTokenContainer();
            $tokenContainer->addParam('order', $orderMain);
            $tokenContainer->addParam('orderId', $orderMain->id);
            $tokenContainer->setSubject($subject);
            $tokenContainer->setBodyHtml($bodyHtml);
            $tokenContainer->setBodyText($bodyPlain);
            $tokenContainer->setFromEmail($fromEmail);
            $tokenContainer->setFromName($fromName);
            $tokenContainer->setToEmail($orderMain->email);
            $tokenContainer->setToName($orderMain->firstName . ' ' . $orderMain->lastName);

            $mail = $tokenContainer->prepare('adminOrderMail');
        
          
            
            if (ConfigApi::getConfigByKey('ORDER_CUSTOMER_NOTIFICATION_ATTACH_INVOICE')) {
                $renderer = Functions::getServiceLocator()->get('viewrenderer');
                $invoice = $renderer->render('common/order-invoice', array(
                    'orderId' => $orderMain->id
                ));

                $pdf = new Pdf();
                $pdf->WriteHTML($invoice);
                $invoice = $pdf->getContent();
             
                $attachment = $mail->addAttachment('order_' . $orderMain->id, $invoice, 'application/pdf', 'order_' . $orderMain->id);
            }
            $tokenContainer->sendMail();
        }

        $subject = ConfigApi::getConfigByKey('ORDER_ADMIN_NOTIFICATION_SUBJECT');
        $bodyHtml = ConfigApi::getConfigByKey('ORDER_ADMIN_NOTIFICATION_HTMLMESSAGE');
        $bodyPlain = ConfigApi::getConfigByKey('ORDER_ADMIN_NOTIFICATION_PLAINMESSAGE');

        $toName = ConfigApi::getConfigByKey('ORDER_NOTIFICATION_NAME');
        $toEmail = ConfigApi::getConfigByKey('ORDER_NOTIFICATION_EMAIL');

        if ($subject && ($bodyHtml || $bodyPlain) && $toEmail) {

            $tokenContainer = $this->getTokenContainer();
            $tokenContainer->addParam('order', $orderMain);
            $tokenContainer->addParam('orderId', $orderMain->id);
            $tokenContainer->setSubject($subject);
            $tokenContainer->setBodyHtml($bodyHtml);
            $tokenContainer->setBodyText($bodyPlain);
            $tokenContainer->setFromEmail($fromEmail);
            $tokenContainer->setFromName($fromName);

            $tokenContainer->setToEmail($toEmail);
            $tokenContainer->setToName($toName);

            $mail = $tokenContainer->prepare('adminOrderMail');
            if (ConfigApi::getConfigByKey('ORDER_ADMIN_NOTIFICATION_ATTACH_INVOICE')) {
                $renderer = Functions::getServiceLocator()->get('viewrenderer');
                $invoice = $renderer->render('common/order-invoice', array(
                    'orderId' => $orderMain->id
                ));
                $pdf = new Pdf();
                $pdf->WriteHTML($invoice);
                $invoice = $pdf->getContent();
            
                $mail->addAttachment('order_' . $orderMain->id, $invoice, 'application/pdf', 'order_' . $orderMain->id);
            }
            $tokenContainer->sendMail();
        }

        return $tokenContainer;
    }

    /**
     *
     * @return TokenContainer
     */
    public function getTokenContainer() {
        return $this->getServiceLocator()->get('TokenContainer');
    }

}
