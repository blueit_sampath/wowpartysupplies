<?php
namespace Payment\Service\Transaction;

use Payment\Option\TransactionEvent;
use OrderMain\Api\OrderApi;
use DoctrineORMModule\Proxy\__CG__\OrderMain\Entity\OrderMain;
use Config\Api\ConfigApi;
use OrderMain\Api\OrderStatusApi;
use Core\Functions;
use Payment\Api\PaymentStatusApi;
use Payment\Api\PaymentApi;

class OrderTransactionSave extends TransactionEvent
{

    public function getPriority()
    {
        return 1000;
    }

    public function paymentPaid()
    {
        return $this->process('ORDER_PAYMENT_STATUS_CONFIRMED', 'PAYMENT_STATUS_CONFIRMED');
    }

    public function paymentPending()
    {
        return $this->process('ORDER_PAYMENT_STATUS_PENDING', 'PAYMENT_STATUS_PENDING');
    }

    public function paymentCancelled()
    {
        return $this->process('ORDER_PAYMENT_STATUS_CANCELLED', 'PAYMENT_STATUS_CANCELLED');
    }

    public function paymentError()
    {
        return $this->process('ORDER_PAYMENT_STATUS_CANCELLED', 'PAYMENT_STATUS_CANCELLED');
    }

    public function process($orderStatusKey, $paymentStatusKey)
    {
        $em = Functions::getEntityManager();
        $orderId = $this->getOrderId();
        
        if (! $orderId) {
            return;
        }
        
        $orderMain = OrderApi::getOrderById($orderId);
        if ($orderMain) {
            $message = $this->getMessage();
            if ($message) {
              
                if (is_array($message)) {
                    foreach ($message as $mg) {
                        OrderApi::saveLog($orderMain->id, $mg);
                    }
                } else {
                    OrderApi::saveLog($orderMain->id, $message);
                }
            }
            
            $statusId = ConfigApi::getConfigByKey($orderStatusKey);
            
            if ($statusId) {
                $status = OrderStatusApi::getOrderStatusById($statusId);
                if ($status) {
                    $orderMain->orderStatus = $status;
                    $em->persist($orderMain);
                }
            }
            $paymentOrder = PaymentApi::getPaymentOrderByOrderId($orderId);
            if ($paymentOrder) {
                $transactionId = $this->getTransactionId();
                if ($transactionId) {
                    $paymentOrder->paymentRefNo = $transactionId;
                }
                
                $statusId = ConfigApi::getConfigByKey($paymentStatusKey);
                if ($statusId) {
                    $status = PaymentStatusApi::getPaymentStatusById($statusId);
                    if ($status) {
                        $paymentOrder->paymentStatus = $status;
                        $em->persist($paymentOrder);
                    }
                }
            }
            $em->flush();
        }
    }
}
