<?php
namespace Payment\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminPaymentOrderUser extends AdminPaymentOrder
{

    public function getEventName()
    {
        return 'adminOrderUser';
    }
}
