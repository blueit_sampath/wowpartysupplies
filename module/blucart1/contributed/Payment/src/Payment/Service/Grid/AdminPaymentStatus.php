<?php
namespace Payment\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminPaymentStatus extends AbstractMainBlucartGridEvent
{

    protected $_columnKeys = array(
        'id',
        'name',
        'weight'
    );

    protected $_entity = '\Payment\Entity\PaymentStatus';

    protected $_entityName = 'paymentStatus';

    public function getEventName()
    {
        return 'adminPaymentStatus';
    }

    public function preSchema($e)
    {
        parent::preSchema($e);
        
        $array = array();
        
        $grid = $this->getGrid();
        $columns = $grid->getColumns();
        
        $columnItem = new ColumnItem();
        $columnItem->setField('id');
        $columnItem->setTitle('ID');
        $columnItem->setType('number');
        $columnItem->setEditable(false);
        $columnItem->setWeight(1000);
        $columnItem->setIsPrimary(true);
        $columns->addColumn('paymentStatus.id', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('name');
        $columnItem->setTitle('Name');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(990);
        $columns->addColumn('paymentStatus.name', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('weight');
        $columnItem->setTitle('Priority');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('paymentStatus.weight', $columnItem);
        
        $this->formToolbar();
        
        return $columns;
    }

    public function formToolbar()
    {
        $grid = $this->getGrid();
        
        $toolbar = $grid->getToolbar();
        if (! $toolbar) {
            $toolbar = new Toolbar();
            $grid->setToolbar($toolbar);
        }
        
        $toolBarItem = new ToolbarItem();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('save', $toolBarItem);
        
        $toolBarItem = new ToolbarItem();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('cancel', $toolBarItem);
        
        $toolBarItem = new ToolbarItem();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(700);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }
}
