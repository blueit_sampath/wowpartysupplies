<?php
namespace Payment\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use BlucartGrid\Event\AbstractBlucartGridEvent;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;

class AdminPaymentOrder extends AbstractBlucartGridEvent
{

    protected $_entity = 'Payment\Entity\PaymentOrder';

    protected $_entityName = 'paymentOrder';

    public function getEventName()
    {
        return 'adminOrder';
    }

    public function getPriority()
    {
        return 900;
    }

    public function preSchema($e)
    {
        parent::preSchema($e);
        
        $array = array();
        
        $grid = $this->getGrid();
        $columns = $grid->getColumns();
        
        $columnItem = new ColumnItem();
        $columnItem->setField('paymentStatusName');
        $columnItem->setTitle('Payment Status');
        $columnItem->setType('string');
        $columnItem->setEditable(false);
        $columnItem->setWeight(940);
        
        $columns->addColumn('paymentStatus.name', $columnItem);
        
        return $columns;
    }

    public function preRead($e)
    {
        parent::preRead($e);
        $grid = $this->getGrid();
        $queryBuilder = $this->getGrid()->getQueryBuilder();
        
        $orderFrom = $queryBuilder->getFrom('orderMain');
        $joinItem = new QueryJoinItem($this->_entity, 'paymentOrder', 'left join', 'orderMain.id = paymentOrder.orderMain');
        $joinItem2 = new QueryJoinItem('paymentOrder.paymentStatus', 'paymentStatus');
        $joinItem->addJoin($joinItem2);
        $orderFrom->addJoin($joinItem);
        
        $queryBuilder->addGroup('orderMain.id', 'orderMainId');
        return true;
    }
}
