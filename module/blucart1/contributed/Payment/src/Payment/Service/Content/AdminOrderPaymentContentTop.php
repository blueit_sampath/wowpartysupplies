<?php
namespace Payment\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminOrderPaymentContentTop extends AbstractContentEvent
{

    protected $_contentTemplate = 'common/admin-order-payment-content-top';

    protected $_contentName = 'admin-order-payment-content-top';

    public function getEventName()
    {
        return 'adminOrderContentTop';
    }

    public function getPriority()
    {
        return 1000;
    }
}

