<?php
namespace Payment\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class UserOrderPaymentContentTop extends AbstractContentEvent
{

    protected $_contentTemplate = 'common/user-order-payment-content-top';

    protected $_contentName = 'user-order-payment-content-top';

    public function getEventName()
    {
        return 'userOrderContentTop';
    }

    public function getPriority()
    {
        return 1000;
    }
}

