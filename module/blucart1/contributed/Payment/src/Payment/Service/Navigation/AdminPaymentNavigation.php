<?php
namespace Payment\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;

class AdminPaymentNavigation extends AdminNavigationEvent
{

    public function common()
    {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'orderMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Payment',
                    'uri' => '#',
                    'id' => 'orderMainPaymentMain',
                    'iconClass' => 'glyphicon glyphicon-gift'
                )
            ));
        }
        $page = $navigation->findOneBy('id', 'orderMainPaymentMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Setting',
                    'route' => 'admin-payment-setting',
                    'id' => 'orderMainPaymentSetting',
                    'iconClass' => 'glyphicon glyphicon-wrench',
                    'aClass' => 'zoombox'
                )
            ));
        }
        $page = $navigation->findOneBy('id', 'orderMainPaymentMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Payment Status',
                    'route' => 'admin-payment-status',
                    'id' => 'admin-payment-status',
                    'iconClass' => 'glyphicon glyphicon-ok'
                )
            ));
        }
        
        $page = $navigation->findOneBy('id', 'orderMainPaymentMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Payment Status',
                    'route' => 'admin-payment-config-general',
                    'id' => 'orderMainPaymentMainSettingMain',
                    'iconClass' => 'glyphicon glyphicon-wrench',
                    'aClass' => 'zoombox'
                )
            ));
        }
        
        $page = $navigation->findOneBy('id', 'orderMainPaymentMainSettingMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'General',
                    'route' => 'admin-payment-config-general',
                    'id' => 'admin-payment-config-general',
                    'iconClass' => 'glyphicon glyphicon-wrench',
                    'aClass' => 'zoombox'
                )
            ));
        }
        // $page = $navigation->findOneBy ( 'id', 'orderMainPaymentMainSettingMain' );
        // if($page){
        // $page->addPages ( array (
        // array (
        // 'label' => 'Status',
        // 'route' => 'admin-payment-config-status',
        // 'id' => 'admin-payment-config-status',
        // 'iconClass' => 'icon-wrench',
        // 'aClass' => 'zoombox'
        // )
        // ) );
        // }
    }
}

