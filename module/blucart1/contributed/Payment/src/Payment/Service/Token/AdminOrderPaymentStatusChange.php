<?php
namespace Payment\Service\Token;

use Payment\Api\PaymentApi;
use OrderMain\Api\OrderApi;
use Core\Functions;
use Common\Option\Token\AbstractTokenEvent;

class AdminOrderPaymentStatusChange extends AbstractTokenEvent
{

    public function getEventName()
    {
        return 'adminOrderMail';
    }

    public function token()
    {
        $tokenContainer = $this->getTokenContainer();
        $orderId = $tokenContainer->getParam('orderId');
        $entity = PaymentApi::getPaymentOrderByOrderId($orderId);
        $item = $tokenContainer->add('payment', $entity, '', 900);
        return $this;
    }
}
