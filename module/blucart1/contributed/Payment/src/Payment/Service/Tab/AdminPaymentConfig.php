<?php
namespace Payment\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminPaymentConfig extends AbstractTabEvent
{

    public function getEventName()
    {
        return 'adminPaymentConfig';
    }

    public function tab()
    {
        $tabContainer = $this->getTabContainer();
        $url = Functions::getUrlPlugin();
        
        $u = $url('admin-payment-config-general');
        $tabContainer->add('admin-payment-config-general', 'General', $u, 1000);
        
        // $u = $url ( 'admin-payment-config-status');
        // $tabContainer->add ( 'admin-payment-config-status', 'Status', $u,980 );
        
        return $this;
    }
}

