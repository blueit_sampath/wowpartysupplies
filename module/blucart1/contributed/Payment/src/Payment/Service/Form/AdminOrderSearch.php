<?php
namespace Payment\Service\Form;

use Product\Api\ProductApi;
use ProductCategory\Api\ProductCategoryApi;
use Zend\Form\Element\Select;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractFormEvent;

class AdminOrderSearch extends AbstractFormEvent
{

    public function getFormName()
    {
        return 'adminOrderSearch';
    }

    public function postInitEvent()
    {
        $form = $this->getForm();
        
        $form->add(array(
            'name' => 'paymentStatusName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'op-contains',
                'placeholder' => 'Payment Status'
            )
        ), array(
            'priority' => 930
        ));
    }
}
