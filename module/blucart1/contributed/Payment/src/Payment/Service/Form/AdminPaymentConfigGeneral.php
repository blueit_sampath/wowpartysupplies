<?php
namespace Payment\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminPaymentConfigGeneral extends AdminConfig
{

    protected $_columnKeys = array(
        'PAYMENT_STATUS_CHECKOUT',
        'PAYMENT_STATUS_PENDING',
        'PAYMENT_STATUS_CONFIRMED',
        'PAYMENT_STATUS_CANCELLED',
        'ORDER_PAYMENT_STATUS_PENDING',
        'ORDER_PAYMENT_STATUS_CONFIRMED',
        'ORDER_PAYMENT_STATUS_CANCELLED'
    )
    ;

    public function getFormName()
    {
        return 'adminPaymentConfigGeneral';
    }

    public function getPriority()
    {
        return 1000;
    }
}
