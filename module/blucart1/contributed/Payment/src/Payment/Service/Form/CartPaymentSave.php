<?php
namespace Payment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Payment\Entity\PaymentOrder;
use Checkout\Option\CartContainer;
use OrderMain\Api\OrderApi;
use Payment\Api\PaymentStatusApi;
use Config\Api\ConfigApi;
use Payment\Option\PaymentContainer;

class CartPaymentSave extends AbstractMainFormEvent
{

    public function getFormName()
    {
        return 'cartCheckout';
    }

    public function getPriority()
    {
        return -100;
    }

    public function save()
    {
        $form = $this->getForm();
        $cart = $this->getCart();
        $em = Functions::getEntityManager();
        
        $orderId = $cart->getParam('orderId');
        if (! $orderId) {
            return;
        }
        $data = $form->getData();
        
        $paymentContainer = $this->getPaymentContainer();
        $paymentContainer->prepare();
        $items = $this->getPaymentGateways();
       
        if (count($items) && count($items) > 1) {
            $payment = $data['cartCheckoutPayment']['payment'];
            $item = $paymentContainer->getItem($payment);
        } elseif (count($items)) {
            $item = array_shift($items);
        } else {
            $item = false;
        }
        
        if ($item) {
            $paymentOrder = new PaymentOrder();
            $paymentOrder->orderMain = OrderApi::getOrderById($orderId);
            $paymentOrder->paymentType = $item->getTitle();
            $paymentOrder->paymentStatus = PaymentStatusApi::getPaymentStatusById(ConfigApi::getConfigByKey('PAYMENT_STATUS_CHECKOUT'));
            $paymentOrder->amount = $cart->getTotalPrice();
            $em->persist($paymentOrder);
            $em->flush();
            
            if ($item->getUrl()) {
                header('Location: ' . $item->getUrl());
                exit();
            }
            return $paymentOrder;
        }
        
        return;
    }

    /**
     *
     * @return CartContainer
     */
    protected function getCart()
    {
        $sm = Functions::getServiceLocator();
        return $sm->get('Cart');
    }
    
    public function getPaymentGateways()
    {
        $array = array();
        $cart = $this->getCart();
        $paymentContainer = $this->getPaymentContainer();
        $paymentContainer->prepare();
        $items = $paymentContainer->getItems();
        foreach ($items as $item) {
            if (\Payment\Api\PaymentApi::hasPaymentModuleEnabled($item->getId())) {
                $array[$item->getId()] = $item;
            }
        }
        
        return $array;
    }

    /**
     *
     * @return PaymentContainer
     */
    protected function getPaymentContainer()
    {
        $sm = Functions::getServiceLocator();
        return $sm->get('PaymentContainer');
    }
}
