<?php
namespace Payment\Service\Form;

use Payment\Api\PaymentStatusApi;
use Payment\Entity\PaymentStatus;
use OrderMain\Api\OrderApi;
use Payment\Api\PaymentApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminOrderViewPaymentStatus extends AbstractMainFormEvent
{

    protected $_columnKeys = array();

    protected $_entity = '\Payment\Entity\PaymentOrder';

    protected $_entityName = 'paymentOrder';

    public function getFormName()
    {
        return 'adminOrderViewPaymentStatus';
    }

    public function getPriority()
    {
        return 1000;
    }

    public function save()
    {
        $form = $this->getForm();
        $params = $form->getData();
        $params = $this->parseSaveParams($params);
        $em = $this->getEntityManager();
        
        if (isset($params['order']) && $params['order']) {
            $entity = PaymentApi::getPaymentOrderByOrderId($params['order']);
        } else {
            $entity = new $this->_entity();
            $entity->order = OrderApi::getOrderById($params['order']);
        }
        $entity->paymentStatus = PaymentStatusApi::getPaymentStatusById($params['paymentStatus']);
        $em->persist($entity);
        $em->flush();
        return true;
    }

    public function getRecord()
    {
        $form = $this->getForm();
        $orderId = Functions::fromRoute('orderId');
        $form->get('order')->setValue($orderId);
        
        $entity = PaymentApi::getPaymentOrderByOrderId($orderId);
        
        if ($entity->paymentStatus) {
            $form->get('paymentStatus')->setValue($entity->paymentStatus->id);
        }
    }
}
