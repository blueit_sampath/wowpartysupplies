<?php
namespace Payment\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminPaymentConfigStatus extends AdminConfig
{

    protected $_columnKeys = array(
        'PAYMENT_STATUS_DEFAULT_SUBJECT',
        'PAYMENT_STATUS_DEFAULT_HTMLMESSAGE',
        'PAYMENT_STATUS_DEFAULT_PLAINMESSAGE'
    );

    public function getFormName()
    {
        return 'adminPaymentConfigStatus';
    }

    public function getPriority()
    {
        return 1000;
    }
}
