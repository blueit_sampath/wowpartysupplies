<?php
namespace Payment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Payment\Form\CartCheckoutPaymentFieldset;

class CartCheckoutPayment extends AbstractMainFormEvent
{

    public function getFormName()
    {
        return 'cartCheckout';
    }

    public function getPriority()
    {
        return 500;
    }

    public function preInitEvent()
    {
        $form = $this->getForm();
        $paymentFieldset = new CartCheckoutPaymentFieldset('cartCheckoutPayment');
        if ($paymentFieldset->has('payment')) {
            $paymentFieldset->setOptions(array(
                'legend' => 'Payment'
            ));
            $form->add($paymentFieldset, array(
                'priority' => 900
            ));
        }
    }
}
