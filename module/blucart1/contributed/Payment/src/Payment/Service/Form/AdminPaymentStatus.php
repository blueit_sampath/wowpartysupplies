<?php
namespace Payment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminPaymentStatus extends AbstractMainFormEvent
{

    protected $_columnKeys = array(
        'id',
        'name',
        'weight'
    );

    protected $_entity = '\Payment\Entity\PaymentStatus';

    protected $_entityName = 'paymentStatus';

    public function getFormName()
    {
        return 'adminPaymentStatusAdd';
    }

    public function getPriority()
    {
        return 1000;
    }

    public function parseSaveEntity($params, $entity)
    {
        $mailParams = $params['mailFieldset'];
        if (isset($mailParams['subject'])) {
            $entity->subject = $mailParams['subject'];
        }
        if (isset($mailParams['bodyHtml'])) {
            $entity->bodyHtml = $mailParams['bodyHtml'];
        }
        
        if (isset($mailParams['bodyPlain'])) {
            $entity->bodyPlain = $mailParams['bodyPlain'];
        }
    }

    public function afterGetRecord($entity)
    {
        $form = $this->getForm();
        $form->get('mailFieldset')
            ->get('subject')
            ->setValue($entity->subject);
        $form->get('mailFieldset')
            ->get('bodyHtml')
            ->setValue($entity->bodyHtml);
        $form->get('mailFieldset')
            ->get('bodyPlain')
            ->setValue($entity->bodyPlain);
    }
}
