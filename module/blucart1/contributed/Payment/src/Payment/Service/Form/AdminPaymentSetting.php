<?php
namespace Payment\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Api\ConfigApi;

class AdminPaymentSetting extends AbstractMainFormEvent
{

    protected $_columnKeys = array(
        'PAYMENT_METHOD'
    )
    ;

    public function getFormName()
    {
        return 'adminPaymentSetting';
    }

    public function getPriority()
    {
        return 1000;
    }

    public function save()
    {
        $resultContainer = $this->getFormResultContainer();
        $form = $this->getForm();
        $params = $form->getData();
        $paymentValue = '';
        if (isset($params['PAYMENT_METHOD'])) {
            $paymentValue = implode(',', $params['PAYMENT_METHOD']);
        }
        ConfigApi::createOrUpdateKey('PAYMENT_METHOD', $paymentValue);
        
        return true;
    }

    public function getRecord()
    {
        $form = $this->getForm();
        
        if ($form->has('PAYMENT_METHOD')) {
            $value = ConfigApi::getConfigByKey('PAYMENT_METHOD', null);
            
            if ($value) {
                $value = explode(',', $value);
                $form->get('PAYMENT_METHOD')->setValue($value);
            }
        }
        return true;
    }
}
