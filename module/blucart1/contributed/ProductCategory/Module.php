<?php

namespace ProductCategory;

use Zend\Mvc\ModuleRouteListener;

class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
                    $this,
                    'initializeForms'
        ));
    }

    public function initializeForms($e) {
        $app = $e->getApplication();
        $sm = $app->getServiceManager();
        $sm->get('ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductFactory');
        $sm->get('ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFactory');
        $sm->get('ProductCategory\Form\CategorySortProduct\CategorySortProductFactory');
    }

}
