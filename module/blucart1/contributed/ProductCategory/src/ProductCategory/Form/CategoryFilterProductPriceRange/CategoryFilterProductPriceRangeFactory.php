<?php
namespace ProductCategory\Form\CategoryFilterProductPriceRange;

use Core\Functions;
use Common\Form\Option\AbstractFormFactory;
use Common\Form\Form;
use Traversable;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;

class CategoryFilterProductPriceRangeFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeForm' );
		$form->setName ( 'categoryFilterProductPriceRange' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeFilter' );
	}
	
	public function createService(ServiceLocatorInterface $services) {
		$this->setServiceLocator ( $services );
		$filter = $this->getFormFilter ();
		$form = $this->getForm ();
		$form->initPreEvent ();
		$form->init ();
		if ($filter) {
			$form->setInputFilter ( $filter );
		}
	
		if (isset ( $_POST ['priceRangeForm'] ) && $_POST ['priceRangeForm']) {
			$encoded = strtr(base64_encode(addslashes(gzcompress(serialize($_POST),9))), '+/=', '-_,');
			Functions::setRouteParam ( 'priceRange', $encoded );
			$form->setData ( $_POST );
			$form->isValid();
		}
	
		elseif ($priceRange = Functions::fromRoute ( 'priceRange' )) {
			$string= unserialize(gzuncompress(stripslashes(base64_decode(strtr($priceRange, '-_,', '+/=')))));
			$form->setData ( $string );
			$form->isValid();
		}
	
		else{
			$form->setData ( array() );
			$form->isValid();
		}
	
		$form->initPostEvent ();
		return $form;
	}
}
