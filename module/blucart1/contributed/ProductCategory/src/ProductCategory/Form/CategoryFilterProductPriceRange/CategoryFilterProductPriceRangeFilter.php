<?php

namespace ProductCategory\Form\CategoryFilterProductPriceRange;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CategoryFilterProductPriceRangeFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {

        $this->add(array(
            'name' => 'minPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));
        
         $this->add(array(
            'name' => 'maxPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));
    }

}
