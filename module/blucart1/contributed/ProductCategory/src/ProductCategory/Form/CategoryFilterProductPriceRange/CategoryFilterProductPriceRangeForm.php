<?php

namespace ProductCategory\Form\CategoryFilterProductPriceRange;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CategoryFilterProductPriceRangeForm extends Form {

    public function init() {
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'priceRangeForm',
            'attributes' => array(
                'type' => 'hidden',
                'value' => 1
            )
        ));

        $this->add(array(
            'name' => 'minPrice',
            'options' => array(
                'label' => 'Min Price'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'maxPrice',
            'options' => array(
                'label' => 'Max Price'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
