<?php

namespace ProductCategory\Form\CategoryAdditionalProduct;

use Core\Functions;
use Common\Form\Option\AbstractFormFactory;
use Common\Form\Form;
use Traversable;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;

class CategoryAdditionalProductFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductForm' );
		$form->setName ( 'categoryAdditionalProduct' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductFilter' );
	}
	public function createService(ServiceLocatorInterface $services) {
		$this->setServiceLocator ( $services );
		$filter = $this->getFormFilter ();
		$form = $this->getForm ();
		$form->initPreEvent ();
		$form->init ();
		if ($filter) {
			$form->setInputFilter ( $filter );
		}
		
		if (isset ( $_POST ['additionalForm'] ) && $_POST ['additionalForm']) {
			Functions::setRouteParam ( 'perPage', $_POST ['perPage'] );
			$form->setData ( $_POST );
			$form->isValid ();
		} 

		elseif ($perPage = Functions::fromRoute ( 'perPage' )) {
			
			$form->setData ( array (
					'perPage' => $perPage 
			) );
			$form->isValid ();
		} 

		else {
			$form->setData ( array () );
			$form->isValid ();
		}
		
		$form->initPostEvent ();
		return $form;
	}
}
