<?php
namespace ProductCategory\Form\CategoryAdditionalProduct;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class CategoryAdditionalProductForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'additionalForm',
				'attributes' => array (
						'type' => 'hidden',
						'value' => 1
				)
		) );
		
		$select = new Select ( 'perPage' );
		$select->setValueOptions ( array (
				'12' => '12',
				'24' => '24',
				'36' => '36',
				'48' => '48',
                                '60' => '60',
                                '120' => '120',
				'100000' => 'All',
		) );
		$this->add ( $select, array (
				'priority' => 1000
		) );
		
		
	}
	
}