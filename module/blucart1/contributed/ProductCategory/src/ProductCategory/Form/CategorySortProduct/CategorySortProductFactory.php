<?php
namespace ProductCategory\Form\CategorySortProduct;

use Core\Functions;
use Common\Form\Option\AbstractFormFactory;
use Common\Form\Form;
use Traversable;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;

class CategorySortProductFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ProductCategory\Form\CategorySortProduct\CategorySortProductForm' );
		$form->setName ( 'categorySortProduct' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ProductCategory\Form\CategorySortProduct\CategorySortProductFilter' );
	}
	
	public function createService(ServiceLocatorInterface $services) {
		$this->setServiceLocator ( $services );
		$filter = $this->getFormFilter ();
		$form = $this->getForm ();
		$form->initPreEvent ();
		$form->init ();
		if ($filter) {
			$form->setInputFilter ( $filter );
		}
	
		if (isset ( $_POST ['sortForm'] ) && $_POST ['sortForm']) {
			$encoded = strtr(base64_encode(addslashes(gzcompress(serialize($_POST),9))), '+/=', '-_,');
			Functions::setRouteParam ( 'sort', $encoded );
			$form->setData ( $_POST );
			$form->isValid();
		}
	
		elseif ($sort = Functions::fromRoute ( 'sort' )) {
			$string= unserialize(gzuncompress(stripslashes(base64_decode(strtr($sort, '-_,', '+/=')))));
			
			$form->setData ( $string );
			$form->isValid();
		}
	
		else{
			$form->setData ( array() );
			$form->isValid();
		}
	
		$form->initPostEvent ();
		return $form;
	}
}
