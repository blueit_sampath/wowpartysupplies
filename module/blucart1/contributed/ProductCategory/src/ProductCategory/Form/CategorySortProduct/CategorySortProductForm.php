<?php

namespace ProductCategory\Form\CategorySortProduct;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CategorySortProductForm extends Form {

    public function init() {
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'sortForm',
            'attributes' => array(
                'type' => 'hidden',
                'value' => 1
            )
        ));

        $select = new Select('sortBy');
        $select->setLabel('Sort By');
        $select->setValueOptions(array(
            '' => 'Position',
            'sellPrice.asc' => 'Price: Lowest first',
            'sellPrice.desc' => 'Price: Highest first',
            'title.asc' => 'Product Name: A to Z',
            'title.desc' => 'Product Name: Z to A'
        ));
        $this->add($select, array(
            'priority' => 1000
        ));
    }

}
