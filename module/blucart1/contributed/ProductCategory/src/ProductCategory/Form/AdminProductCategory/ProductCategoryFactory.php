<?php
namespace ProductCategory\Form\AdminProductCategory;

use Common\Form\Option\AbstractFormFactory;

class ProductCategoryFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ProductCategory\Form\AdminProductCategory\ProductCategoryForm' );
		$form->setName ( 'adminProductCategoryAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ProductCategory\Form\AdminProductCategory\ProductCategoryFilter' );
	}
}
