<?php

namespace ProductCategory\Form\AdminProductCategory;

use Category\Api\CategoryApi;
use Product\Api\ProductApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class ProductCategoryForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'product',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'category' );
		$select->setLabel ( 'Categories' );
		$select->setAttribute ( 'multiple', true );
		$select->setValueOptions ( $this->getCategories () );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
	public function getCategories() {
		$array = array ();
		$result = $this->formOptions ( CategoryApi::getChildrenCategories ( null, null ), Functions::fromRoute ( 'categoryId' ) );
		return $array + $result;
	}
	public function formOptions($array, $id, $level = 0) {
		$result = array ();
		
		foreach ( $array as $res ) {
			$categoryId = $res ['categoryId'];
			
			if ($id == $categoryId) {
				continue;
			}
			$category = CategoryApi::getCategoryById ( $categoryId );
			$space = '';
			
			$name = '';
			$parent = $category;
			while ( $parent->parent ) {
				$name = $parent->parent->name . ' > ' . $name;
				$parent = $parent->parent;
			}
			
			if (! $name) {
				$result [$category->id] = $category->name;
			} else {
				$result [$category->id] = $name . $category->name;
			}
			
			$children = CategoryApi::getChildrenCategories ( $category->id, null );
			if (count ( $children )) {
				
				$result = $result + $this->formOptions ( $children, $id, ($level + 1) );
			}
		}
		
		return $result;
	}
}