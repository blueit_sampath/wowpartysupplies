<?php
namespace ProductCategory\Form\CategoryFilterProduct;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class CategoryFilterProductForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		

		$this->add ( array (
				'name' => 'filterForm',
				'attributes' => array (
						'type' => 'hidden',
						'value' => '1'
				)
		), array (
				'priority' => - 100
		) );
		
		
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}