<?php

namespace ProductCategory\Form\CategoryFilterProduct;

use Core\Functions;
use Common\Form\Option\AbstractFormFactory;
use Common\Form\Form;
use Traversable;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;

class CategoryFilterProductFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductForm' );
		$form->setName ( 'categoryFilterProduct' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFilter' );
	}
	public function createService(ServiceLocatorInterface $services) {
		$this->setServiceLocator ( $services );
		$filter = $this->getFormFilter ();
		$form = $this->getForm ();
		$form->initPreEvent ();
		$form->init ();
		if ($filter) {
			$form->setInputFilter ( $filter );
		}
		
		if (isset ( $_POST ['filterForm'] ) && $_POST ['filterForm']) {
			$encoded = strtr(base64_encode(addslashes(gzcompress(serialize($_POST),9))), '+/=', '-_,');
			Functions::setRouteParam ( 'filter',$encoded );
			$form->setData ( $_POST );
		}
		
		elseif ($filter = Functions::fromRoute ( 'filter' )) {
			$string= unserialize(gzuncompress(stripslashes(base64_decode(strtr($filter, '-_,', '+/=')))));
			$form->setData ($string);
		}
		
		else{
			$form->setData ( array() );
		}
		
		$form->initPostEvent ();
		return $form;
	}
}
