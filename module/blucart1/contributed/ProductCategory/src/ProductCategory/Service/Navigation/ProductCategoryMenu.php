<?php

namespace ProductCategory\Service\Navigation;

use Category\Api\CategoryApi;
use ProductCategory\Api\ProductCategoryApi;
use Product\Api\ProductApi;
use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ProductCategoryMenu extends NavigationEvent {

    protected $_navigation = null;

    public function breadcrumb() {

        $routeName = Functions::getMatchedRouteName();
        if ($routeName != 'product') {
            return false;
        }
        $productId = Functions::fromRoute('productId', 0);
        $product = ProductApi::getProductById($productId);
        if (!$product) {
            return false;
        }
        $navigation = $this->getNavigation();
        $page = array(
            'label' => $product->title,
            'route' => 'product',
            'params' => array(
                'productId' => $product->id
            ),
            'id' => 'productId_' . $product->id
        );
        $categories = ProductCategoryApi::getCategoryByProductId($productId);
        if (count($categories)) {
            $categoryId = array_shift($categories->getArrayCopy());
            if ($categoryId) {
                $url = $this->getUrl('category', array('categoryId' => $categoryId['categoryId']));
                $this->findPagesAndAddMenuByUrl($url, $page);
            }
        }
        
    }

}
