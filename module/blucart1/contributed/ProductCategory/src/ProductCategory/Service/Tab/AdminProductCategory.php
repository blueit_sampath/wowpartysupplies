<?php

namespace ProductCategory\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminProductCategory extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		if (! $productId) {
			return;
		}
		$u = $url ( 'admin-product-category', array (
				'productId' => $productId 
		) );
		
		$tabContainer->add ( 'admin-product-category', 'Categories', $u, 985 );
		
		return $this;
	}
}

