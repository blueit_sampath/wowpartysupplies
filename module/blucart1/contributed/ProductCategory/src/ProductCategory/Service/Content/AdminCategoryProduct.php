<?php 
namespace ProductCategory\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminCategoryProduct extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-category-product';
	protected $_contentName = 'adminCategoryProduct';
	public function getEventName() {
		return 'adminCategoryView';
	}
	public function getPriority() {
		return 1000;
	}
	
}

