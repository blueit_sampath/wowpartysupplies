<?php 
namespace ProductCategory\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminProductCategoryProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-product-category-product-view';
	protected $_contentName = 'adminProductCategoryProductView';
	public function getEventName() {
		return 'adminProductViewTop';
	}
	public function getPriority() {
		return 1000;
	}
	
}

