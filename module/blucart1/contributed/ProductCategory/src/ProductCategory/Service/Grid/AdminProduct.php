<?php

namespace ProductCategory\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractBlucartGridEvent;

class AdminProduct extends AbstractBlucartGridEvent {
	protected $_entity = 'ProductCategory\Entity\ProductCategory';
	protected $_entityName = 'ProductCategory';
	public function getEventName() {
		return 'adminProduct';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'categories' );
		$columnItem->setTitle ( 'Categories' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 995 );
		$columnItem->setIsHaving ( true );
		$columnItem->setBelongsToEntity ( 'category' );
		$columns->addColumn ( 'GroupConcat(Distinct category.name)', $columnItem );
		
		return $columns;
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		
		$productFrom = $queryBuilder->getFrom ( 'product' );
		$joinItem = new QueryJoinItem ( '\ProductCategory\Entity\ProductCategory', 'productCategory', 'left join', 'product.id = productCategory.product' );
		$joinItem2 = new QueryJoinItem ( 'productCategory.category', 'category' );
		$joinItem->addJoin ( $joinItem2 );
		
		$productFrom->addJoin ( $joinItem );
		return true;
	}
	public function postCreate($e) {
	}
	public function postUpdate($e) {
	}
	public function postDestroy($e) {
	}
}
