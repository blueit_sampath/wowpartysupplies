<?php

namespace ProductCategory\Service\Query;

use Core\Functions;
use QueryBuilder\Option\Event\AbstractQueryBuilderEvent;

class CategorySortProduct extends AbstractQueryBuilderEvent {
	public function getEventName() {
		return 'ProductCategory\Api\ProductCategoryApi.getProductsByCategoryId';
	}
	public function getPriority() {
		return 500;
	}
	public function preQuery() {
		parent::preQuery ();
		$queryBuilder = $this->getQueryBuilder ();
		
		$sm = Functions::getServiceLocator ();
		$form = $sm->get ( 'ProductCategory\Form\CategorySortProduct\CategorySortProductFactory' );
		
		$data = $form->getData ();
		
		if (isset ( $data ['sortBy'] ) && $data ['sortBy']) {
			
			switch ($data ['sortBy']) {
				case 'sellPrice.desc' :
					$orderItem = $queryBuilder->addOrder ( 'product.sellPrice', 'sellPrice', 'desc' );
					$orderItem->setWeight ( 1000 );
					break;
				case 'sellPrice.asc' :
					$orderItem = $queryBuilder->addOrder ( 'product.sellPrice', 'sellPrice', 'asc' );
					$orderItem->setWeight ( 1000 );
					break;
				case 'title.asc' :
					$orderItem = $queryBuilder->addOrder ( 'product.title', 'title', 'asc' );
					$orderItem->setWeight ( 1000 );
					break;
				case 'title.desc' :
					$orderItem = $queryBuilder->addOrder ( 'product.title', 'title', 'desc' );
					$orderItem->setWeight ( 1000 );
					break;
			}
		}
	}
}

