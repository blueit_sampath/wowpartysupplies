<?php

namespace ProductCategory\Service\Query;

use Core\Functions;
use QueryBuilder\Option\Event\AbstractQueryBuilderEvent;

class CategoryFilterProductPriceRange extends AbstractQueryBuilderEvent {
	public function getEventName() {
		return 'ProductCategory\Api\ProductCategoryApi.getProductsByCategoryId';
	}
	public function getPriority() {
		return 500;
	}
	public function preQuery() {
		parent::preQuery ();
		$queryBuilder = $this->getQueryBuilder ();
		
		$sm = Functions::getServiceLocator();
		$form = $sm->get('ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeFactory');
		$form->isValid();
		$data = $form->getData();
		
		
		
		if($data['minPrice']){
			$whereItem = $queryBuilder->addWhere ( 'product.sellPrice', 'sellPriceMin' , ' >= ' );
			$queryBuilder->addParameter ( 'sellPriceMin', $data['minPrice'] );
		}
		if($data['maxPrice']){
			$whereItem = $queryBuilder->addWhere ( 'product.sellPrice', 'sellPriceMax' , ' <= ' );
			$queryBuilder->addParameter ( 'sellPriceMax', $data['maxPrice'] );
		}
		
		
	
	}
}

