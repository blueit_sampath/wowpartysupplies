<?php 
namespace ProductCategory\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class CategoryFilterProductBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'category-filter-product';

	public function getBlockName() {
		return 'categoryFilterProduct';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'Category Filter Product';
	}
	
}

