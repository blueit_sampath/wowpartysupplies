<?php 
namespace ProductCategory\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class CategorySortProductBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'category-sort-product';

	public function getBlockName() {
		return 'categorySortProduct';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'Category Sort Product';
	}
	
}

