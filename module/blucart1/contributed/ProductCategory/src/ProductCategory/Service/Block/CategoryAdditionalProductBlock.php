<?php 
namespace ProductCategory\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class CategoryAdditionalProductBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'category-additional-product';

	public function getBlockName() {
		return 'categoryAdditionalProduct';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'Category Additional Product';
	}
	
}

