<?php 
namespace ProductCategory\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminCategoryViewProduct extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-category-view-product';

	public function getBlockName() {
		return 'AdminCategoryViewProduct';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminCategoryViewProduct';
	}
	
}

