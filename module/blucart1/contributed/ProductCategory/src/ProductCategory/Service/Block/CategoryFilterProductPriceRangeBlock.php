<?php 
namespace ProductCategory\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class CategoryFilterProductPriceRangeBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'category-filter-product-price-range';

	public function getBlockName() {
		return 'categoryFilterProductPriceRange';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'Category Filter Product By Price Range';
	}
	
}

