<?php 
namespace ProductCategory\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProductCategory extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminProductCategory';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		if(!$productId){
			return;
		}
		$u = $url ( 'admin-product-category-add', array (
				'productId' => $productId
		) );
		$item = $linkContainer->add ( 'admin-product-category-add', 'Add Category Products', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

