<?php

namespace ProductCategory\Service\Form;

use Product\Api\ProductApi;
use ProductCategory\Api\ProductCategoryApi;
use Zend\Form\Element\Select;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractFormEvent;

class AdminProduct extends AbstractFormEvent {
	protected $_columnKeys = array ();
	protected $_entityName = '\ProductCategory\Entity\ProductCategory';
	public function getFormName() {
		return 'adminProductAdd';
	}
	public function postInitEvent() {
		$form = $this->getForm ();
		
		$select = new Select ( 'productCategory' );
		$select->setLabel ( 'Categories' );
		$select->setAttribute ( 'multiple', true );
		$select->setValueOptions ( $this->getCategories () );
		$form->add ( $select, array (
				'priority' => 925 
		) );
		
		$inputFilter = $form->getInputFilter ();
		$inputFilter->add ( array (
				'name' => 'productCategory',
				'required' => false 
		) );
	}
	public function getCategories() {
		$array = array ();
		$result = $this->formOptions ( CategoryApi::getChildrenCategories ( null, null ), Functions::fromRoute ( 'categoryId' ) );
		return $array + $result;
	}
	public function formOptions($array, $id, $level = 0) {
		$result = array ();
		
		foreach ( $array as $res ) {
			$categoryId = $res ['categoryId'];
			
			if ($id == $categoryId) {
				continue;
			}
			$category = CategoryApi::getCategoryById ( $categoryId );
			$space = '';
			
			$name = '';
			$parent = $category;
			while ( $parent->parent ) {
				$name = $parent->parent->name . ' > ' . $name;
				$parent = $parent->parent;
			}
			
			if (! $name) {
				$result [$category->id] = $category->name;
			} else {
				$result [$category->id] = $name . $category->name;
			}
			
			$children = CategoryApi::getChildrenCategories ( $category->id, null );
			if (count ( $children )) {
				
				$result = $result + $this->formOptions ( $children, $id, ($level + 1) );
			}
		}
		
		return $result;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		$product = $resultContainer->get ( 'product' );
		
		if (isset ( $params ['productCategory'] ) || $params ['productCategory'] === null) {
			
			if ($product) {
				$product = $product->getValue ();
				if (! ($params ['productCategory']) || ! count ( $params ['productCategory'] )) {
					
					$dql = ' delete from ' . $this->_entityName . ' u where u.product = ' . $product->id;
					$em->createQuery ( $dql )->execute ();
					$resultContainer->add ( 'productCategory', null );
				} else {
					$results = ProductCategoryApi::getCategoryByProductId ( $product->id );
					$categories = array ();
					if ($results) {
						foreach ( $results as $result ) {
							$categories [] = $result ['categoryId'];
						}
					}
					$temp = array_diff ( $params ['productCategory'], $categories );
					if ($temp && count ( 'temp' )) {
						$temp1 = array ();
						foreach ( $temp as $value ) {
							$entity = new $this->_entityName ();
							$entity->product = $product;
							$entity->category = CategoryApi::getCategoryById ( $value );
							$em->persist ( $entity );
						}
					}
					$em->flush ();
					$temp = array_diff ( $categories, $params ['productCategory'] );
					if ($temp && count ( 'temp' )) {
						$dql = ' delete from ' . $this->_entityName . ' u where u.category in (' . implode ( ',', $temp ) . ')';
						$em->createQuery ( $dql )->execute ();
					}
				}
			}
		}
		
		return true;
	}
	public function getRecord() {
		$form = $this->getForm ();
		$productId = Functions::fromRoute ( 'productId' );
		if (! $productId) {
			return;
		}
		
		$results = ProductCategoryApi::getCategoryByProductId ( $productId,null );
		$temp = array ();
		if ($results) {
			
			foreach ( $results as $result ) {
				$temp [] = $result ['categoryId'];
			}
		}
		
		$form->get ( 'productCategory' )->setValue ( $temp );
		
		return true;
	}
}
