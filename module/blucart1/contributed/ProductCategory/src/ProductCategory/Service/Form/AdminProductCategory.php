<?php

namespace ProductCategory\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use ProductCategory\Entity\ProductCategory;
use ProductCategory\Api\ProductCategoryApi;
use Product\Api\ProductApi;
use Category\Api\CategoryApi;
use Core\Functions;

class AdminProductCategory extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight' 
	);
	protected $_entity = '\ProductCategory\Entity\ProductCategory';
	protected $_entityName = 'productCategory';
	public function getFormName() {
		return 'adminProductCategoryAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		if (isset ( $params ['product'] ) && $params ['product'] && $params ['category']) {
			$product = ProductApi::getProductById ( $params ['product'] );
			
			if ($product) {
				$results = ProductCategoryApi::getCategoryByProductId ( $product->id, null );
				$array = array ();
				if ($results) {
					foreach ( $results as $result ) {
						$array [] = ( int ) $result ['categoryId'];
					}
				}
				foreach ( $params ['category'] as $r ) {
					if (! in_array ( ( int ) $r, $array )) {
						$e = new ProductCategory ();
						$e->product = $product;
						$e->category = CategoryApi::getCategoryById ( $r );
						$em->persist ( $e );
					}
				}
				
				$em->flush ();
			}
		}
		
		return true;
	}
	public function getRecord() {
		$id = Functions::fromRoute ( 'productId' );
		if (! $id) {
			return;
		}
		$form = $this->getForm ();
		$form->get ( 'product' )->setValue ( $id );
		
		return true;
	}
}
