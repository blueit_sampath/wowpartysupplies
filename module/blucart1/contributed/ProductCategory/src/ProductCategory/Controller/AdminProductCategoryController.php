<?php

namespace ProductCategory\Controller;

use Common\MVC\Controller\AbstractAdminController;


class AdminProductCategoryController extends AbstractAdminController{
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'ProductCategory\Form\AdminProductCategory\ProductCategoryFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-category-add', array (
						'productId' => $form->get ( 'product' )->getValue () 
				) );
	}
	
}