<?php

namespace ProductCategory\Api;

use DoctrineExtensions\Paginate\Paginate;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use Category\Api\CategoryApi;
use Common\Api\Api;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;

class ProductCategoryApi extends Api {

    protected static $_entity = 'ProductCategory\Entity\ProductCategory';

    public static function getProductCatetoryById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getProductCategoryByProductId($productId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('productCategory.id', 'productCategoryId');
        $item = $queryBuilder->addFrom(static::$_entity, 'productCategory');
        $joinItem = new QueryJoinItem('productCategory.category', 'category');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('productCategory.product', 'product');
        $item->addJoin($joinItem);
        $item = $queryBuilder->addWhere('product.id', 'productId');
        $queryBuilder->addParameter('productId', $productId);
        $queryBuilder->addOrder('productCategory.weight', 'productCategoryWeight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getCategoryByProductId($productId, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('category.id', 'categoryId');
        $item = $queryBuilder->addFrom(static::$_entity, 'productCategory');
        $joinItem = new QueryJoinItem('productCategory.category', 'category');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('productCategory.product', 'product');
        $item->addJoin($joinItem);
        $item = $queryBuilder->addWhere('product.id', 'productId');
        $queryBuilder->addParameter('productId', $productId);
        if ($status) {
            $item = $queryBuilder->addWhere('category.status', 'categoryStatus');
            $queryBuilder->addParameter('categoryStatus', 1);
        }

        $queryBuilder->addOrder('productCategory.weight', 'productCategoryWeight', 'desc');
        $queryBuilder->addOrder('category.weight', 'categoryWeight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getProductsByCategoryId($categoryId, $page = 1, $perPage = PHP_INT_MAX, $orderBy = array('product.weight' => array(
            'name' => 'productWeight',
            'order' => 'desc'
        ),
        'product.id' => array(
            'name' => 'productId',
            'order' => 'desc'
        )
    )) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('product.id', 'productId');
        $item = $queryBuilder->addFrom(static::$_entity, 'productCategory');
        $joinItem = new QueryJoinItem('productCategory.category', 'category');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('productCategory.product', 'product');
        $item->addJoin($joinItem);

        $children = static::getInCategories($categoryId);
        $whereItem = $queryBuilder->addWhere('category.id', 'categoryId');
        $queryBuilder->addParameter('categoryId', $categoryId);
        $whereItem->setLogic(' or ');

        foreach ($children as $value) {

            $item = new QueryWhereItem('category.id', 'categoryId' . $value);
            $whereItem->addWhere($item);
            $queryBuilder->addParameter('categoryId' . $value, $value);
        }

        $whereItem = $queryBuilder->addWhere('product.status', 'productStatus');
        $queryBuilder->addParameter('productStatus', 1);

        if ($orderBy) {
            foreach ($orderBy as $key => $value) {
                if (isset($value['order'])) {
                    $queryBuilder->addOrder($key, $value['name'], $value['order']);
                }
            }
        }
        $queryBuilder->addGroup('product.id', 'productId');

        $results = $queryBuilder->executeQuery(($page - 1) * $perPage, $perPage);

        return $results;
    }

    public static function getInCategories($categoryId, &$result = array()) {
        $result[] = $categoryId;
        $array = CategoryApi::getChildrenCategories($categoryId);

        foreach ($array as $a) {

            static::getInCategories($a['categoryId'], $result);
        }
        return $result;
    }

    public static function productHasCategory($productId, $categoryId) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
                    'product' => $productId,
                    'category' => $categoryId
        ));
    }

    public static function getMinProductPriceByCategoryId($categoryId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('min(product.sellPrice)', 'productSellPrice');
        $item = $queryBuilder->addFrom(static::$_entity, 'productCategory');
        $joinItem = new QueryJoinItem('productCategory.category', 'category');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('productCategory.product', 'product');
        $item->addJoin($joinItem);

        $children = static::getInCategories($categoryId);
        $whereItem = $queryBuilder->addWhere('category.id', 'categoryId');
        $queryBuilder->addParameter('categoryId', $categoryId);
        $whereItem->setLogic(' or ');

        foreach ($children as $value) {

            $item = new QueryWhereItem('category.id', 'categoryId' . $value);
            $whereItem->addWhere($item);
            $queryBuilder->addParameter('categoryId' . $value, $value);
        }

        $whereItem = $queryBuilder->addWhere('product.status', 'productStatus');
        $queryBuilder->addParameter('productStatus', 1);

        $results = $queryBuilder->executeQuery();

        if(count($results)){
            $array = (array)$results;
            $array = array_shift($array);
            return $array['productSellPrice'];
        }
        return false;
    }
    
    public static function getMaxProductPriceByCategoryId($categoryId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('max(product.sellPrice)', 'productSellPrice');
        $item = $queryBuilder->addFrom(static::$_entity, 'productCategory');
        $joinItem = new QueryJoinItem('productCategory.category', 'category');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('productCategory.product', 'product');
        $item->addJoin($joinItem);

        $children = static::getInCategories($categoryId);
        $whereItem = $queryBuilder->addWhere('category.id', 'categoryId');
        $queryBuilder->addParameter('categoryId', $categoryId);
        $whereItem->setLogic(' or ');

        foreach ($children as $value) {

            $item = new QueryWhereItem('category.id', 'categoryId' . $value);
            $whereItem->addWhere($item);
            $queryBuilder->addParameter('categoryId' . $value, $value);
        }

        $whereItem = $queryBuilder->addWhere('product.status', 'productStatus');
        $queryBuilder->addParameter('productStatus', 1);

        $results = $queryBuilder->executeQuery();

        if(count($results)){
            $array = (array)$results;
            $array = array_shift($array);
            return $array['productSellPrice'];
        }
        return false;
    }

}
