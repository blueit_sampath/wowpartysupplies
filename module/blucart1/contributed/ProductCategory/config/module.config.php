<?php

return array(
    'router' => array('routes' => array(
            'category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/category[/:categoryId][/page/:page][/perPage/:perPage][/sort/:sort][/filter/:filter][/priceRange/:priceRange]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\Index',
                        'action' => 'index',
                        'page' => 1,
                        'perPage' => 36
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'page' => '[0-9]+',
                        'perPage' => '[0-9]+'
                    )
                )
            ),
            'admin-product-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/category/[/product/:productId][/productCategory/:productCategoryId][/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'ProductCategory\Controller\AdminProductCategory',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'productCategoryId' => '[0-9]+'
                    )
                )
            ),
            'admin-product-category-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/category/add[/product/:productId][/productCategory/:productCategoryId][/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'ProductCategory\Controller\AdminProductCategory',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'productCategoryId' => '[0-9]+'
                    )
                )
            )
        )),
    'events' => array(
        'ProductCategory\Service\Grid\AdminProduct' => 'ProductCategory\Service\Grid\AdminProduct',
        'ProductCategory\Service\Form\AdminProduct' => 'ProductCategory\Service\Form\AdminProduct',
        'ProductCategory\Service\Content\AdminProductCategoryProductView' => 'ProductCategory\Service\Content\AdminProductCategoryProductView',
        'ProductCategory\Service\Grid\AdminProductCategory' => 'ProductCategory\Service\Grid\AdminProductCategory',
        'ProductCategory\Service\Form\AdminProductCategory' => 'ProductCategory\Service\Form\AdminProductCategory',
        'ProductCategory\Service\Tab\AdminProductCategory' => 'ProductCategory\Service\Tab\AdminProductCategory',
        'ProductCategory\Service\Link\AdminProductCategory' => 'ProductCategory\Service\Link\AdminProductCategory',
        'ProductCategory\Service\Content\AdminCategoryProduct' => 'ProductCategory\Service\Content\AdminCategoryProduct',
        'ProductCategory\Service\Grid\AdminProductCategoryProduct' => 'ProductCategory\Service\Grid\AdminProductCategoryProduct',
        'ProductCategory\Service\Form\CategoryFilterProduct' => 'ProductCategory\Service\Form\CategoryFilterProduct',
        'ProductCategory\Service\Block\CategoryFilterProductBlock' => 'ProductCategory\Service\Block\CategoryFilterProductBlock',
        'ProductCategory\Service\Query\CategoryFilterProductPriceRange' => 'ProductCategory\Service\Query\CategoryFilterProductPriceRange',
        'ProductCategory\Service\Query\CategorySortProduct' => 'ProductCategory\Service\Query\CategorySortProduct',
        'ProductCategory\Service\Navigation\ProductCategoryMenu' => 'ProductCategory\Service\Navigation\ProductCategoryMenu',
        'ProductCategory\Service\Block\AdminCategoryViewProduct' => 'ProductCategory\Service\Block\AdminCategoryViewProduct',
        'ProductCategory\Service\Block\CategorySortProductBlock' => 'ProductCategory\Service\Block\CategorySortProductBlock',
        'ProductCategory\Service\Block\CategoryAdditionalProductBlock' => 'ProductCategory\Service\Block\CategoryAdditionalProductBlock',
        'ProductCategory\Service\Block\CategoryFilterProductPriceRangeBlock' => 'ProductCategory\Service\Block\CategoryFilterProductPriceRangeBlock',
    ),
    'service_manager' => array(
        'invokables' => array(
            'ProductCategory\Service\Grid\AdminProduct' => 'ProductCategory\Service\Grid\AdminProduct',
            'ProductCategory\Service\Form\AdminProduct' => 'ProductCategory\Service\Form\AdminProduct',
            'ProductCategory\Service\Content\AdminProductCategoryProductView' => 'ProductCategory\Service\Content\AdminProductCategoryProductView',
            'ProductCategory\Service\Grid\AdminProductCategory' => 'ProductCategory\Service\Grid\AdminProductCategory',
            'ProductCategory\Service\Form\AdminProductCategory' => 'ProductCategory\Service\Form\AdminProductCategory',
            'ProductCategory\Form\AdminProductCategory\ProductCategoryForm' => 'ProductCategory\Form\AdminProductCategory\ProductCategoryForm',
            'ProductCategory\Form\AdminProductCategory\ProductCategoryFilter' => 'ProductCategory\Form\AdminProductCategory\ProductCategoryFilter',
            'ProductCategory\Service\Tab\AdminProductCategory' => 'ProductCategory\Service\Tab\AdminProductCategory',
            'ProductCategory\Service\Link\AdminProductCategory' => 'ProductCategory\Service\Link\AdminProductCategory',
            'ProductCategory\Service\Content\AdminCategoryProduct' => 'ProductCategory\Service\Content\AdminCategoryProduct',
            'ProductCategory\Service\Grid\AdminCategory' => 'ProductCategory\Service\Grid\AdminCategory',
            'ProductCategory\Service\Grid\AdminProductCategoryProduct' => 'ProductCategory\Service\Grid\AdminProductCategoryProduct',
            'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductForm' => 'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductForm',
            'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFilter' => 'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFilter',
            'ProductCategory\Service\Form\CategoryFilterProduct' => 'ProductCategory\Service\Form\CategoryFilterProduct',
            'ProductCategory\Service\Block\CategoryFilterProductBlock' => 'ProductCategory\Service\Block\CategoryFilterProductBlock',
            'ProductCategory\Service\Query\CategoryFilterProductPriceRange' => 'ProductCategory\Service\Query\CategoryFilterProductPriceRange',
            'ProductCategory\Form\CategorySortProduct\CategorySortProductForm' => 'ProductCategory\Form\CategorySortProduct\CategorySortProductForm',
            'ProductCategory\Form\CategorySortProduct\CategorySortProductFilter' => 'ProductCategory\Form\CategorySortProduct\CategorySortProductFilter',
            'ProductCategory\Service\Query\CategorySortProduct' => 'ProductCategory\Service\Query\CategorySortProduct',
            'ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductForm' => 'ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductForm',
            'ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductFilter' => 'ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductFilter',
            'ProductCategory\Service\Navigation\ProductCategoryMenu' => 'ProductCategory\Service\Navigation\ProductCategoryMenu',
            'ProductCategory\Service\Block\AdminCategoryViewProduct' => 'ProductCategory\Service\Block\AdminCategoryViewProduct',
            'ProductCategory\Service\Block\CategorySortProductBlock' => 'ProductCategory\Service\Block\CategorySortProductBlock',
            'ProductCategory\Service\Block\CategoryAdditionalProductBlock' => 'ProductCategory\Service\Block\CategoryAdditionalProductBlock',
            'ProductCategory\Service\Block\CategoryFilterProductPriceRangeBlock' => 'ProductCategory\Service\Block\CategoryFilterProductPriceRangeBlock',
            'ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeForm' => 'ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeForm',
            'ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeFilter' => 'ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeFilter',
        ),
        'factories' => array(
            'ProductCategory\Form\AdminProductCategory\ProductCategoryFactory' => 'ProductCategory\Form\AdminProductCategory\ProductCategoryFactory',
            'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFactory' => 'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFactory',
            'ProductCategory\Form\CategorySortProduct\CategorySortProductFactory' => 'ProductCategory\Form\CategorySortProduct\CategorySortProductFactory',
            'ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductFactory' => 'ProductCategory\Form\CategoryAdditionalProduct\CategoryAdditionalProductFactory',
            'ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeFactory' => 'ProductCategory\Form\CategoryFilterProductPriceRange\CategoryFilterProductPriceRangeFactory',
        )
    ),
    'controllers' => array('invokables' => array('ProductCategory\Controller\AdminProductCategory' => 'ProductCategory\Controller\AdminProductCategoryController')),
    'view_manager' => array('template_path_stack' => array('ProductCategory' => __DIR__ . '/../view')),
    'doctrine' => array('driver' => array(
            'ProductCategory_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/ProductCategory/Entity')
            ),
            'orm_default' => array('drivers' => array('ProductCategory\Entity' => 'ProductCategory_driver'))
        )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('ProductCategory' => __DIR__ . '/../public')))
);
