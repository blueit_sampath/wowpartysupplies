<?php 
namespace StaticPage\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminStaticPage extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminStaticPage';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-static-page-add');
		$item = $linkContainer->add ( 'admin-static-page-add', 'Add Page', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

