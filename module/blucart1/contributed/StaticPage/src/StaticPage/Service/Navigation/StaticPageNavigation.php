<?php
namespace StaticPage\Service\Navigation;


use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class StaticPageNavigation extends NavigationEvent {

    protected $_navigation = null;

    public function breadcrumb() {

        $routeName = Functions::getMatchedRouteName();
        if ($routeName != 'static-page') {
            return false;
        }
        $staticPageId = Functions::fromRoute('staticPageId', 0);
        $staticPage = \StaticPage\Api\StaticPageApi::getStaticPageById($staticPageId);
        if (!$staticPage) {
            return false;
        }
        $page = array(
            'label' => $staticPage->title,
            'route' => 'static-page',
            'params' => array(
                'staticPageId' => $staticPageId
            ),
            'id' => 'staticPage_' . $staticPage->id
        );
        $this->addDefaultMenu($page);
    }

}
