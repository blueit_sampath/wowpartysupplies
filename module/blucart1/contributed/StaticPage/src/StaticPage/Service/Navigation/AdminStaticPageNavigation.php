<?php 
namespace StaticPage\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminStaticPageNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'websiteMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Content Pages',
							'route' => 'admin-static-page',
							'id' => 'admin-static-page',
							'iconClass' => 'glyphicon-bullhorn' 
					) 
			) );
		}
	}
	
	
}

