<?php

namespace StaticPage\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminStaticPage extends AbstractTabEvent {
	public function getEventName() {
		return 'adminStaticPageAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$staticPageId = Functions::fromRoute ( 'staticPageId' );
		if(!$staticPageId){
			return;
		}
		$u = $url ( 'admin-static-page-add', array (
				'staticPageId' => $staticPageId 
		) );
		$tabContainer->add ( 'admin-static-page-add', 'General Information', $u, 1000 );
		
		return $this;
	}
}

