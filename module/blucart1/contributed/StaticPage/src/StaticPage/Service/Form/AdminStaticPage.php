<?php

namespace StaticPage\Service\Form;

use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminStaticPage extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'description' 
	);
	protected $_entity = '\StaticPage\Entity\StaticPage';
	protected $_entityName = 'staticPage';
	public function getFormName() {
		return 'adminStaticPageAdd';
	}
	public function getPriority() {
		return 1000;
	}
	
}
