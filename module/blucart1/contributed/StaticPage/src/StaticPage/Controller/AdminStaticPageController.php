<?php 
namespace StaticPage\Controller;
use Common\MVC\Controller\AbstractAdminController;


class AdminStaticPageController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'StaticPage\Form\AdminStaticPage\StaticPageFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-static-page-add', array (
						'staticPageId' => $form->get ( 'id' )->getValue ()
				) );
	}
	
	
}