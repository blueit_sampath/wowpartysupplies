<?php
namespace StaticPage\Form\AdminStaticPage;

use Common\Form\Option\AbstractFormFactory;

class StaticPageFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'StaticPage\Form\AdminStaticPage\StaticPageForm' );
		$form->setName ( 'adminStaticPageAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'StaticPage\Form\AdminStaticPage\StaticPageFilter' );
	}
}
