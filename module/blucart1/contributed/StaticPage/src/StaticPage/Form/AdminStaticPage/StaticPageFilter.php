<?php
namespace StaticPage\Form\AdminStaticPage;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  StaticPageFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'title',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags'
						),
						array (
								'name' => 'StringTrim'
						)
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255
								)
						)
				)
		) );
		$this->add ( array (
				'name' => 'description',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						)
				)
		) );
		
	}
} 