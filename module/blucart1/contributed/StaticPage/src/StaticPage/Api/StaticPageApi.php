<?php

namespace StaticPage\Api;

use Core\Functions;
use Common\Api\Api;

class StaticPageApi extends Api {

    protected static $_entity = '\StaticPage\Entity\StaticPage';

    public static function getStaticPageById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getAllStaticPages($status = true) {
        $em = Functions::getEntityManager();
        $array = array();
        $orderArray = array();
        if ($status !== null) {
            $array['status'] = $status;
        }
      

        return $em->getRepository(static::$_entity)->findBy($array, $orderArray);
    }

    public static function getStaticPages($status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('staticPage.id', 'staticPageId');

        if ($status !== null) {
            $queryBuilder->addWhere('staticPage.status', 'staticPageStatus');
            $queryBuilder->addParameter('staticPageStatus', $status);
        }
        $item = $queryBuilder->addFrom(static::$_entity, 'staticPage');
        return $queryBuilder->executeQuery();
    }

}
