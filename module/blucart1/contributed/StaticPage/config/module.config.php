<?php

namespace StaticPage;

return array(
    'router' => array(
        'routes' => array(
            'static-page' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/static-page[/:staticPageId]',
                    'defaults' => array(
                        'controller' => 'StaticPage\Controller\Index',
                        'action' => 'index'
                    ),
                    'constraints' => array(
                        'staticPageId' => '[0-9]+'
                    )
                )
            ),
            'admin-static-page' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/static/page',
                    'defaults' => array(
                        'controller' => 'StaticPage\Controller\AdminStaticPage',
                        'action' => 'index'
                    )
                )
            ),
            'admin-static-page-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/static/page/add[/ajax/:ajax][/:staticPageId]',
                    'defaults' => array(
                        'controller' => 'StaticPage\Controller\AdminStaticPage',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            )
        )
    ),
    'events' => array(
        'StaticPage\Service\Grid\AdminStaticPage' => 'StaticPage\Service\Grid\AdminStaticPage',
        'StaticPage\Service\Link\AdminStaticPage' => 'StaticPage\Service\Link\AdminStaticPage',
        'StaticPage\Service\Tab\AdminStaticPage' => 'StaticPage\Service\Tab\AdminStaticPage',
        'StaticPage\Service\Form\AdminStaticPage' => 'StaticPage\Service\Form\AdminStaticPage',
        'StaticPage\Service\Navigation\StaticPageNavigation' => 'StaticPage\Service\Navigation\StaticPageNavigation',
        'StaticPage\Service\Navigation\AdminStaticPageNavigation' => 'StaticPage\Service\Navigation\AdminStaticPageNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'StaticPage\Service\Grid\AdminStaticPage' => 'StaticPage\Service\Grid\AdminStaticPage',
            'StaticPage\Service\Link\AdminStaticPage' => 'StaticPage\Service\Link\AdminStaticPage',
            'StaticPage\Service\Tab\AdminStaticPage' => 'StaticPage\Service\Tab\AdminStaticPage',
            'StaticPage\Form\AdminStaticPage\StaticPageForm' => 'StaticPage\Form\AdminStaticPage\StaticPageForm',
            'StaticPage\Form\AdminStaticPage\StaticPageFilter' => 'StaticPage\Form\AdminStaticPage\StaticPageFilter',
            'StaticPage\Service\Form\AdminStaticPage' => 'StaticPage\Service\Form\AdminStaticPage',
            'StaticPage\Service\Navigation\AdminStaticPageNavigation' => 'StaticPage\Service\Navigation\AdminStaticPageNavigation',
            'StaticPage\Service\Navigation\StaticPageNavigation' => 'StaticPage\Service\Navigation\StaticPageNavigation',
        ),
        'factories' => array(
            'StaticPage\Form\AdminStaticPage\StaticPageFactory' => 'StaticPage\Form\AdminStaticPage\StaticPageFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'StaticPage\Controller\AdminStaticPage' => 'StaticPage\Controller\AdminStaticPageController',
            'StaticPage\Controller\Index' => 'StaticPage\Controller\IndexController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __NAMESPACE__ => __DIR__ . '/../public'
            )
        )
    )
);
