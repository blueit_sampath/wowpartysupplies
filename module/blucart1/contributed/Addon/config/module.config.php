<?php

return array(
    'router' => array('routes' => array(
            'admin-addon' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/addon',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminAddon',
                        'action' => 'index'
                    )
                )
            ),
            'admin-addon-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/addon/add[/ajax/:ajax][/:addonId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminAddon',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'addonId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-addon-option' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/addon/option[/ajax/:ajax][/:addonId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminAddonOption',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'addonId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-addon-option-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/addon/option/add[/ajax/:ajax][/:addonId][/:addonOptionId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminAddonOption',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'addonId' => '[0-9]+',
                        'addonOptionId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-addon-option' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/addon/option[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminAddonOptionProduct',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-addon-option-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/addon/option/add[/ajax/:ajax][/:productId][/:addonOptionProductId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminAddonOptionProduct',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'addonOptionProductId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-addon' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/addon[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminProductAddon',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-addon-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/addon/add[/ajax/:ajax][/:productId][/:addonProductId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminProductAddon',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'addonProductId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-addon-bulk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/addon/bulk[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminBulkAddon',
                        'action' => 'addon',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-addon-option-bulk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/addon/option/bulk[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminBulkAddon',
                        'action' => 'addon-option',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-category-addon' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/addon[/ajax/:ajax][/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminCategoryAddon',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-category-addon-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/addon/add[/ajax/:ajax][/:categoryId][/:addonCategoryId]',
                    'defaults' => array(
                        'controller' => 'Addon\Controller\AdminCategoryAddon',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'addonCategoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            )
        )),
    'events' => array(
        'Addon\Service\Grid\AdminAddon' => 'Addon\Service\Grid\AdminAddon',
        'Addon\Service\Form\AdminAddon' => 'Addon\Service\Form\AdminAddon',
        'Addon\Service\Link\AdminAddon' => 'Addon\Service\Link\AdminAddon',
        'Addon\Service\Tab\AdminAddon' => 'Addon\Service\Tab\AdminAddon',
        'Addon\Service\Grid\AdminAddonOption' => 'Addon\Service\Grid\AdminAddonOption',
        'Addon\Service\Grid\AdminAddonOptionProduct' => 'Addon\Service\Grid\AdminAddonOptionProduct',
        'Addon\Service\Form\AdminAddonOptionProduct' => 'Addon\Service\Form\AdminAddonOptionProduct',
        'Addon\Service\Tab\AdminAddonOptionProduct' => 'Addon\Service\Tab\AdminAddonOptionProduct',
        'Addon\Service\Form\AdminAddonOption' => 'Addon\Service\Form\AdminAddonOption',
        'Addon\Service\Link\AdminAddonOption' => 'Addon\Service\Link\AdminAddonOption',
        'Addon\Service\Grid\AdminProductAddon' => 'Addon\Service\Grid\AdminProductAddon',
        'Addon\Service\Form\AdminProductAddon' => 'Addon\Service\Form\AdminProductAddon',
        'Addon\Service\Link\AdminProductAddon' => 'Addon\Service\Link\AdminProductAddon',
        'Addon\Service\Form\AdminBulkAddonOption' => 'Addon\Service\Form\AdminBulkAddonOption',
        'Addon\Service\Content\AdminAddonProductView' => 'Addon\Service\Content\AdminAddonProductView',
        'Addon\Service\Grid\AdminProduct' => 'Addon\Service\Grid\AdminProduct',
        'Addon\Service\Navigation\AdminAddonNavigation' => 'Addon\Service\Navigation\AdminAddonNavigation',
        'Addon\Service\Form\AdminCategoryAddon' => 'Addon\Service\Form\AdminCategoryAddon',
        'Addon\Service\Grid\AdminCategoryAddon' => 'Addon\Service\Grid\AdminCategoryAddon',
        'Addon\Service\Tab\AdminCategoryAddon' => 'Addon\Service\Tab\AdminCategoryAddon',
        'Addon\Service\Link\AdminCategoryAddonLink' => 'Addon\Service\Link\AdminCategoryAddonLink',
        'Addon\Service\Form\CategoryFilterProductAddon' => 'Addon\Service\Form\CategoryFilterProductAddon',
        'Addon\Service\Query\CategoryFilterProductAddon' => 'Addon\Service\Query\CategoryFilterProductAddon',
        'Addon\Service\Form\ProductAddToCartAddon' => 'Addon\Service\Form\ProductAddToCartAddon',
        'Addon\Service\Cart\CartAddon' => 'Addon\Service\Cart\CartAddon',
        'Addon\Service\Form\CartAddonSave' => 'Addon\Service\Form\CartAddonSave',
        'Addon\Service\Block\AdminProductViewAddon' => 'Addon\Service\Block\AdminProductViewAddon'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Addon\Service\Grid\AdminAddon' => 'Addon\Service\Grid\AdminAddon',
            'Addon\Form\AdminAddon\AddonForm' => 'Addon\Form\AdminAddon\AddonForm',
            'Addon\Form\AdminAddon\AddonFilter' => 'Addon\Form\AdminAddon\AddonFilter',
            'Addon\Service\Form\AdminAddon' => 'Addon\Service\Form\AdminAddon',
            'Addon\Service\Link\AdminAddon' => 'Addon\Service\Link\AdminAddon',
            'Addon\Service\Tab\AdminAddon' => 'Addon\Service\Tab\AdminAddon',
            'Addon\Service\Grid\AdminAddonOption' => 'Addon\Service\Grid\AdminAddonOption',
            'Addon\Service\Grid\AdminAddonOptionProduct' => 'Addon\Service\Grid\AdminAddonOptionProduct',
            'Addon\Form\AdminAddonOptionProduct\AddonOptionProductForm' => 'Addon\Form\AdminAddonOptionProduct\AddonOptionProductForm',
            'Addon\Form\AdminAddonOptionProduct\AddonOptionProductFilter' => 'Addon\Form\AdminAddonOptionProduct\AddonOptionProductFilter',
            'Addon\Service\Form\AdminAddonOptionProduct' => 'Addon\Service\Form\AdminAddonOptionProduct',
            'Addon\Service\Tab\AdminAddonOptionProduct' => 'Addon\Service\Tab\AdminAddonOptionProduct',
            'Addon\Form\AdminAddonOption\AddonOptionForm' => 'Addon\Form\AdminAddonOption\AddonOptionForm',
            'Addon\Form\AdminAddonOption\AddonOptionFilter' => 'Addon\Form\AdminAddonOption\AddonOptionFilter',
            'Addon\Service\Form\AdminAddonOption' => 'Addon\Service\Form\AdminAddonOption',
            'Addon\Service\Link\AdminAddonOption' => 'Addon\Service\Link\AdminAddonOption',
            'Addon\Service\Grid\AdminProductAddon' => 'Addon\Service\Grid\AdminProductAddon',
            'Addon\Form\AdminProductAddon\ProductAddonForm' => 'Addon\Form\AdminProductAddon\ProductAddonForm',
            'Addon\Form\AdminProductAddon\ProductAddonFilter' => 'Addon\Form\AdminProductAddon\ProductAddonFilter',
            'Addon\Service\Form\AdminProductAddon' => 'Addon\Service\Form\AdminProductAddon',
            'Addon\Service\Link\AdminProductAddon' => 'Addon\Service\Link\AdminProductAddon',
            'Addon\Form\AdminBulkAddon\BulkAddonForm' => 'Addon\Form\AdminBulkAddon\BulkAddonForm',
            'Addon\Form\AdminBulkAddon\BulkAddonFilter' => 'Addon\Form\AdminBulkAddon\BulkAddonFilter',
            'Addon\Form\AdminBulkAddonOption\BulkAddonOptionForm' => 'Addon\Form\AdminBulkAddonOption\BulkAddonOptionForm',
            'Addon\Form\AdminBulkAddonOption\BulkAddonOptionFilter' => 'Addon\Form\AdminBulkAddonOption\BulkAddonOptionFilter',
            'Addon\Service\Form\AdminBulkAddonOption' => 'Addon\Service\Form\AdminBulkAddonOption',
            'Addon\Service\Content\AdminAddonProductView' => 'Addon\Service\Content\AdminAddonProductView',
            'Addon\Service\Grid\AdminProduct' => 'Addon\Service\Grid\AdminProduct',
            'Addon\Service\Navigation\AdminAddonNavigation' => 'Addon\Service\Navigation\AdminAddonNavigation',
            'Addon\Form\AdminCategoryAddon\CategoryAddonForm' => 'Addon\Form\AdminCategoryAddon\CategoryAddonForm',
            'Addon\Form\AdminCategoryAddon\CategoryAddonFilter' => 'Addon\Form\AdminCategoryAddon\CategoryAddonFilter',
            'Addon\Service\Form\AdminCategoryAddon' => 'Addon\Service\Form\AdminCategoryAddon',
            'Addon\Service\Grid\AdminCategoryAddon' => 'Addon\Service\Grid\AdminCategoryAddon',
            'Addon\Service\Tab\AdminCategoryAddon' => 'Addon\Service\Tab\AdminCategoryAddon',
            'Addon\Service\Link\AdminCategoryAddonLink' => 'Addon\Service\Link\AdminCategoryAddonLink',
            'Addon\Service\Form\CategoryFilterProductAddon' => 'Addon\Service\Form\CategoryFilterProductAddon',
            'Addon\Service\Query\CategoryFilterProductAddon' => 'Addon\Service\Query\CategoryFilterProductAddon',
            'Addon\Service\Form\ProductAddToCartAddon' => 'Addon\Service\Form\ProductAddToCartAddon',
            'Addon\Service\Cart\CartAddon' => 'Addon\Service\Cart\CartAddon',
            'Addon\Service\Form\CartAddonSave' => 'Addon\Service\Form\CartAddonSave',
            'Addon\Service\Block\AdminProductViewAddon' => 'Addon\Service\Block\AdminProductViewAddon'
        ),
        'factories' => array(
            'Addon\Form\AdminAddon\AddonFactory' => 'Addon\Form\AdminAddon\AddonFactory',
            'Addon\Form\AdminAddonOptionProduct\AddonOptionProductFactory' => 'Addon\Form\AdminAddonOptionProduct\AddonOptionProductFactory',
            'Addon\Form\AdminAddonOption\AddonOptionFactory' => 'Addon\Form\AdminAddonOption\AddonOptionFactory',
            'Addon\Form\AdminProductAddon\ProductAddonFactory' => 'Addon\Form\AdminProductAddon\ProductAddonFactory',
            'Addon\Form\AdminBulkAddon\BulkAddonFactory' => 'Addon\Form\AdminBulkAddon\BulkAddonFactory',
            'Addon\Form\AdminBulkAddonOption\BulkAddonOptionFactory' => 'Addon\Form\AdminBulkAddonOption\BulkAddonOptionFactory',
            'Addon\Form\AdminCategoryAddon\CategoryAddonFactory' => 'Addon\Form\AdminCategoryAddon\CategoryAddonFactory'
        )
    ),
    'controllers' => array('invokables' => array(
            'Addon\Controller\AdminAddon' => 'Addon\Controller\AdminAddonController',
            'Addon\Controller\AdminAddonOption' => 'Addon\Controller\AdminAddonOptionController',
            'Addon\Controller\AdminAddonOptionProduct' => 'Addon\Controller\AdminAddonOptionProductController',
            'Addon\Controller\AdminProductAddon' => 'Addon\Controller\AdminProductAddonController',
            'Addon\Controller\AdminBulkAddon' => 'Addon\Controller\AdminBulkAddonController',
            'Addon\Controller\AdminCategoryAddon' => 'Addon\Controller\AdminCategoryAddonController'
        )),
    'view_manager' => array('template_path_stack' => array('Addon' => __DIR__ . '/../view')),
    'doctrine' => array('driver' => array(
            'Addon_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Addon/Entity')
            ),
            'orm_default' => array('drivers' => array('Addon\Entity' => 'Addon_driver'))
        )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('Addon' => __DIR__ . '/../public')))
);
