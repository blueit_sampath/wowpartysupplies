<?php 
namespace Addon\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminAddon extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminAddon';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-addon-add');
		$item = $linkContainer->add ( 'Add Addon', 'Add Addon', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

