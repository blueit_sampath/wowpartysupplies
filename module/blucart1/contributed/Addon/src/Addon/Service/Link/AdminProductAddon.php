<?php

namespace Addon\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProductAddon extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminAddonOptionProduct';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$productId = Functions::fromRoute ( 'productId' );
		$u = $url ( 'admin-product-addon', array (
				'productId' => $productId 
		) );
		$item = $linkContainer->add ( 'admin-product-addon', 'Addons', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-product-addon-bulk', array (
				'productId' => $productId
		) );
		$item = $linkContainer->add ( 'admin-product-addon-bulk', 'Add Addons', $u, 800 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
	
	
}

