<?php

namespace Addon\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminAddonOption extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminAddonOption';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$addonId = Functions::fromRoute ( 'addonId' );
		$u = $url ( 'admin-addon-option-add', array (
				'addonId' => $addonId 
		) );
		$item = $linkContainer->add ( 'admin-addon-option-add', 'Add Option', $u, 1000 );
		$item->setLinkClass ( 'zoombox' );
		
		return $this;
	}
}

