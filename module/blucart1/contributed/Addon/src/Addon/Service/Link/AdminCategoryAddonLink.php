<?php 
namespace Addon\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategoryAddonLink extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategoryAddon';
	}
	public function link() {
		$categoryId = Functions::fromRoute('categoryId');
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-category-addon-add', array('categoryId' => $categoryId));
		$item = $linkContainer->add ( 'admin-category-addon-add', 'Add Addon', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

