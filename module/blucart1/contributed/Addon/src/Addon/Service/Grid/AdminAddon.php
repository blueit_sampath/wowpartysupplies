<?php

namespace Addon\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminAddon extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'title',
			'type' 
	)
	;
	protected $_entity = '\Addon\Entity\Addon';
	protected $_entityName = 'addon';
	public function getEventName() {
		return 'adminAddon';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'addon.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'addon.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'addon.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'type' );
		$columnItem->setTitle ( 'Type' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'addon.type', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
	
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 700 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
}
