<?php

namespace Addon\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminAddonOption extends AbstractMainBlucartGridEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'title',
        'weight',
        'defaultAdjustmentSellPrice',
        'defaultAdjustmentListPrice',
        'defaultAdjustmentCostPrice'
    );
    protected $_entity = '\Addon\Entity\AddonOption';
    protected $_entityName = 'addonOption';

    public function getEventName() {
        return 'adminAddonOption';
    }

    public function preSchema($e) {
        parent::preSchema($e);

        $array = array();

        $grid = $this->getGrid();

        $addonId = Functions::fromRoute('addonId', 0);
        $array = array();
        $grid = $this->getGrid();
        $grid->setAdditionalParameters(array(
            'addonId' => $addonId
        ));

        $columns = $grid->getColumns();

        $columnItem = new ColumnItem ();
        $columnItem->setField('id');
        $columnItem->setTitle('ID');
        $columnItem->setType('number');
        $columnItem->setIsPrimary(true);
        $columnItem->setEditable(false);
        $columnItem->setWeight(1000);
        $columns->addColumn('addonOption.id', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('name');
        $columnItem->setTitle('Name');
        $columnItem->setType('string');
        $columnItem->setEditable(false);
        $columnItem->setWeight(990);

        $columns->addColumn('addonOption.name', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('title');
        $columnItem->setTitle('Title');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('addonOption.title', $columnItem);


        $columnItem = new ColumnItem ();
        $columnItem->setField('defaultAdjustmentSellPrice');
        $columnItem->setTitle('Adj. Sell Price');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columnItem->setFormat('{0:n2}');
        $columns->addColumn('addonOption.defaultAdjustmentSellPrice', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('defaultAdjustmentListPrice');
        $columnItem->setTitle('Adj. Sell Price');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columnItem->setFormat('{0:n2}');
        $columns->addColumn('addonOption.defaultAdjustmentListPrice', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('defaultAdjustmentCostPrice');
        $columnItem->setTitle('Adj. Sell Price');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columnItem->setFormat('{0:n2}');
        $columns->addColumn('addonOption.defaultAdjustmentCostPrice', $columnItem);

        
        $columnItem = new ColumnItem ();
        $columnItem->setField('weight');
        $columnItem->setTitle('Priority');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(950);
        $columns->addColumn('addonOption.weight', $columnItem);
        
        $this->formToolbar();

        return $columns;
    }

    public function formToolbar() {
        $grid = $this->getGrid();

        $toolbar = $grid->getToolbar();
        if (!$toolbar) {
            $toolbar = new Toolbar ();
            $grid->setToolbar($toolbar);
        }



        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('save', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('cancel', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(700);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }

    public function preRead($e) {
        parent::preRead($e);
        $addonId = Functions::fromQuery('addonId', 0);
        $grid = $this->getGrid();
        $queryBuilder = $this->getGrid()->getQueryBuilder();
        $item = $queryBuilder->addFrom($this->_entity, $this->_entityName);
        $joinItem = new QueryJoinItem($this->_entityName . '.addon', 'addon');
        $item->addJoin($joinItem);

        $item = $queryBuilder->addWhere('addon.id', 'addonId');
        $queryBuilder->addParameter('addonId', $addonId);

        return true;
    }

}
