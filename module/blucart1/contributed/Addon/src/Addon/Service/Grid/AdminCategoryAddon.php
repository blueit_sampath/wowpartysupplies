<?php

namespace Addon\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;

use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminCategoryAddon extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'type',
			'weight' 
	);
	protected $_entity = '\Addon\Entity\AddonCategory';
	protected $_entityName = 'addonCategory';
	public function getEventName() {
		return 'adminCategoryAddon';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		$array = array ();
		$grid = $this->getGrid ();
		$grid->addAdditionalParameter ( 'categoryId', $categoryId );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary ( true );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'addonCategory.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Overriden Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'addonCategory.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'addonTitle' );
		$columnItem->setTitle ( 'Addon Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'addon.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'weight' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'addonCategory.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$categoryId = Functions::fromQuery ( 'categoryId', 0 );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem( $this->_entityName . '.category', 'category' );
		$item->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem( $this->_entityName . '.addon', 'addon' );
		$item->addJoin ( $joinItem );
		
		$item = $queryBuilder->addWhere ( 'category.id', 'categoryId' );
		$queryBuilder->addParameter ( 'categoryId', $categoryId );
		
		return true;
	}
}
