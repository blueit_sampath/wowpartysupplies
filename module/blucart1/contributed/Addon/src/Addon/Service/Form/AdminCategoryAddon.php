<?php 
namespace Addon\Service\Form;

use Addon\Entity\AddonCategory;

use Addon\Api\AddonApi;

use Category\Api\CategoryApi;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminCategoryAddon extends AbstractMainFormEvent {
	
	protected $_entity = '\Addon\Entity\AddonCategory';
	protected $_entityName = 'addonCategory';
	public function getFormName() {
		return 'adminCategoryAddon';
	}
	public function getPriority() {
		return 1000;
	}
	
	
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		$categoryId = Functions::fromRoute('categoryId');
	
		if ($categoryId) {
			$category = CategoryApi::getCategoryById ( $categoryId );
				
			if ($category) {
				$results = AddonApi::getAddonByCategoryId ( $category->id );
				$array = array ();
				if ($results) {
					foreach ( $results as $result ) {
						$array [] = ( int ) $result ['addonId'];
					}
				}
				foreach ( $params ['addons'] as $r ) {
					if (! in_array ( ( int ) $r, $array )) {
						$e = new AddonCategory ();
						$e->category = $category;
						$e->addon = AddonApi::getAddonById ( $r );
						$em->persist ( $e );
					}
				}
	
				$em->flush ();
			}
		}
	
		return true;
	}
	
	
}
