<?php

namespace Addon\Service\Form;

use File\Api\FileApi;
use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminAddonOptionProduct extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'adjustmentSellPrice' => array('type' => 'float'),
        'adjustmentListPrice' => array('type' => 'float'),
        'adjustmentCostPrice' => array('type' => 'float'),
        'isPercentage',
    );
    protected $_entity = '\Addon\Entity\AddonOptionProduct';
    protected $_entityName = 'addonOptionProduct';

    public function getFormName() {
        return 'adminAddonOptionProductAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function parseSaveEntity($params, $entity) {
        FileApi::deleteFiles(Functions::fromPost('image_delete', ''));

        $image = $params ['image'];

        if ($image) {
            $array = explode(',', $image);
            foreach ($array as $file) {
                $imageEntity = FileApi::createOrUpdateFile($file, '', '');
                $entity->file = $imageEntity;
            }
        }


        if ($params ['product']) {
            $entity->product = ProductApi::getProductById($params ['product']);
        }
    }

    public function beforeGetRecord() {
        $form = $this->getForm();
        $form->get('product')->setValue(Functions::fromRoute('productId', 0));
    }

    public function afterGetRecord($entity) {
        $form = $this->getForm();
        if ($entity->file) {
            $form->get('image')->setValue($entity->file->path);
        }
    }

}
