<?php

namespace Addon\Service\Form;

use Addon\Form\ProductAddToCartAddonFieldset;
use Common\Form\Option\AbstractMainFormEvent;

class ProductAddToCartAddon extends AbstractMainFormEvent {

    public function getFormName() {
        return 'productAddToCartForm';
    }

    public function getPriority() {
        return 800;
    }

    public function preSetDataEvent() {
        $this->attachFormElements();
    }

    public function attachFormElements() {

        $form = $this->getForm();
        $product = $form->get('product')->getValue();

        if (!$form->has('addons')) {

            $attributes = new ProductAddToCartAddonFieldset($product, 'addons');


            if ($attributes->getElements()) {

                $form->add($attributes,array(
                    'priority' => 500
                ));
            }
        }
    }

}
