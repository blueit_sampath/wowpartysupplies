<?php

namespace Addon\Service\Form;

use Addon\Api\AddonApi;
use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminBulkAddonOption extends AbstractMainFormEvent {
	protected $_entity = '\Addon\Entity\AddonOptionProduct';
	protected $_entityName = 'addonOptionProduct';
	public function getFormName() {
		return 'adminBulkAddonOptionAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$em = Functions::getEntityManager ();
		
		$form = $this->getForm ();
		$productId = Functions::fromRoute ( 'productId' );
		$addons_string = $form->get ( 'addons' )->getValue ();
		
		$addons = explode ( ',', $addons_string );
		$product = ProductApi::getProductById ( $productId );
		
		$buildOptions = array ();
		
		foreach ( $addons as $addonId ) {
			$addon = AddonApi::getAddonById ( $addonId );
			if (! AddonApi::checkOrGetAddonByProductIdAddonId ( $productId, $addonId )) {
				$entity = new \Addon\Entity\AddonProduct ();
				$entity->type = $addon->type;
				$entity->addon = $addon;
				$entity->product = $product;
				$em->persist ( $entity );
			}
			
			$buildOptions [] = $form->get ( 'addon_' . $addonId )->getValue ();
		}
		
		$this->buildOptions ( $buildOptions, $productId );
		
		$em->flush ();
	}
	
	public function getRecord() {
		$form = $this->getForm ();
		$addonOptions = $form->get ( 'addons' )->setValue ( implode ( ',', Functions::fromQuery ( 'addons' ) ) );
	}
	public function buildOptions($buildOptions, $productId) {
		$em = $this->getEntityManager ();
		$product = ProductApi::getProductById ( $productId );
		
		foreach($buildOptions as $array){
			foreach ($array as $a){
				if (! ($model = AddonApi::checkOrGetAddonOptionByProductIdAndAddonOptionId($productId, $a))) {
					$model = new $this->_entity ();
					$model->addonOption = AddonApi::getAddonOptionById($a);
					$model->product = $product;
					$em->persist ( $model );
					$em->flush ();
				}
				
			}
		}
		return true;
	}
	
}
