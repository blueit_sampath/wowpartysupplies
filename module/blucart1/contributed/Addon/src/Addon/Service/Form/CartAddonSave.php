<?php 
namespace Addon\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Checkout\Option\CartContainer;
use Addon\Api\AddonApi;
use OrderMain\Api\OrderApi;
use Addon\Entity\AddonOptionProductOrder;

class CartAddonSave extends AbstractMainFormEvent {

	public function getFormName() {
		return 'cartCheckout';
	}
	public function getPriority() {
		return 500;
	}
	public function save() {
		$form = $this->getForm ();
		$cart = $this->getCart ();
		$em = Functions::getEntityManager ();
		/* Order Product Save */
		$cartItems = $cart->getCartItems ();
		if (count ( $cartItems )) {
			foreach ( $cartItems as $cartItem ) {
				
				$inItems = $cartItem->getInItems();
				
				foreach($inItems as $addonItem){
					$id = $addonItem->getId();
					
					if(fnmatch('addon_*', $id)){
						
						if ($addonItem) {
							$option = AddonApi::getAddonOptionProductById ( $addonItem->getParam ( 'addonOptionProductId' ) );
							if ($option) {
								$orderProductId = $cartItem->getParam ( 'orderProductId' );
								if ($orderProductId) {
									$orderProduct = OrderApi::getOrderProductById ( $orderProductId );
									$addonOrder = new AddonOptionProductOrder ();
									$addonOrder->addonOptionProduct = $option;
									$addonOrder->orderProduct = $orderProduct;
									$em->persist ( $addonOrder );
									$em->flush ();
									return $addonOrder;
								}
							}
						}
					}
				}
				
			}
		}
	
		return;
	}
	
	/**
	 *
	 * @return CartContainer
	 */
	protected function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
}
