<?php

namespace Addon\Service\Form;

use Addon\Api\AddonApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminAddonOption extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'title',
        'weight',
        'defaultAdjustmentSellPrice' => array('type' => 'float'),
        'defaultAdjustmentListPrice' => array('type' => 'float'),
        'defaultAdjustmentCostPrice' => array('type' => 'float')
    );
    protected $_entity = '\Addon\Entity\AddonOption';
    protected $_entityName = 'addonOption';

    public function getFormName() {
        return 'adminAddonOptionAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function parseSaveEntity($params, $entity) {
        if ($params ['addon']) {
            $entity->addon = AddonApi::getAddonById($params ['addon']);
        }
    }

    public function beforeGetRecord() {
        $form = $this->getForm();
        $form->get('addon')->setValue(Functions::fromRoute('addonId', 0));
    }

    public function afterGetRecord($entity) {
        $form = $this->getForm();
        $form->get('name')->setAttribute('readonly', true);
    }

}
