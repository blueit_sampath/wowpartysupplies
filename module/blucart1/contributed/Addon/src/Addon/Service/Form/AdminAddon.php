<?php

namespace Addon\Service\Form;

use File\Api\FileApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminAddon extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'title',
        'type',
        'description'
    );
    protected $_entity = '\Addon\Entity\Addon';
    protected $_entityName = 'addon';

    public function getFormName() {
        return 'adminAddonAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function parseSaveEntity($params, $entity) {
        FileApi::deleteFiles(Functions::fromPost('image_delete', ''));

        $image = $params ['image'];

        if ($image) {
            $array = explode(',', $image);
            foreach ($array as $file) {
                $imageEntity = FileApi::createOrUpdateFile($file, $params ['title'], '');
                $entity->file = $imageEntity;
            }
        }
    }

    public function afterGetRecord($entity) {
        $form = $this->getForm();
        if ($entity->file) {
            $form->get('image')->setValue($entity->file->path);
        }
        $form->get('name')->setAttribute('readonly', true);
    }

}
