<?php

namespace Addon\Service\Form;

use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminProductAddon extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'type',
			'weight',
                        'isRequired'
	);
	protected $_entity = '\Addon\Entity\AddonProduct';
	protected $_entityName = 'addonProduct';
	public function getFormName() {
		return 'adminProductAddonAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		if ($params ['product']) {
			$entity->product = ProductApi::getProductById ( $params ['product'] );
		}
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$form->get ( 'product' )->setValue ( Functions::fromRoute ( 'productId', 0 ) );
	}
}
