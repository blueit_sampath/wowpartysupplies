<?php 
namespace Addon\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminProductViewAddon extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-product-view-addon';

	public function getBlockName() {
		return 'AdminProductViewAddon';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminProductViewAddon';
	}
	
}

