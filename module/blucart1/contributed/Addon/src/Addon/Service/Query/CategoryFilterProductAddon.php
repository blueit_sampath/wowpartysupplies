<?php

namespace Addon\Service\Query;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use Addon\Api\AddonApi;
use Core\Functions;
use QueryBuilder\Option\Event\AbstractQueryBuilderEvent;

class CategoryFilterProductAddon extends AbstractQueryBuilderEvent {
	public function getEventName() {
		return 'ProductCategory\Api\ProductCategoryApi.getProductsByCategoryId';
	}
	public function getPriority() {
		return 500;
	}
	public function preQuery() {
		parent::preQuery ();
		$queryBuilder = $this->getQueryBuilder ();
		
		$sm = Functions::getServiceLocator ();
		$form = $sm->get ( 'ProductCategory\Form\CategoryFilterProduct\CategoryFilterProductFactory' );
		$form->isValid ();
		$data = $form->getData ();
		
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		
		$results = AddonApi::getAddonByCategoryId ( $categoryId );
		
		$wheres = $queryBuilder->getWheres ();
		$where = new QueryWhereItem ();
		$where->setLogic(' and ');
		
		
		foreach ( $results as $result ) {
			
			$addonCategory = AddonApi::getAddonCategoryById ( $result ['addonCategoryId'] );
			$addon = $addonCategory->addon;
			
			$results2 = AddonApi::getAddonOptionByAddonId ( $addon->id );
			
			if (isset ( $data ['addons'] ['addon_' . $addon->id] )) {
				$array = $data ['addons'] ['addon_' . $addon->id];
				
				$whereItem = new QueryWhereItem ();
				
				$where->addWhere ( $whereItem, 'addon_' . $addon->id );
				
				foreach ( $array as $value ) {
					$whereItem2 = new QueryWhereItem ();
					$whereItem->addWhere ( $whereItem2, 'addon_' . $addon->id . '_' . $value );
					$where1 = new QueryWhereItem ( 'addonOptionProduct.addonOption', 'addonOption' . $value, ' = ' );
					$queryBuilder->addParameter ( 'addonOption' . $value , $value  );
					$whereItem2->addWhere ( $where1 );
		
				}
			}
		}
		if(count($where->getWheres())){
			$wheres->addWhere ( $where, 'addons' );
		}
		
		$item = $queryBuilder->getFrom ( 'productCategory' );
		$joinItem = new QueryJoinItem ( '\Addon\Entity\AddonOptionProduct', 'addonOptionProduct', 'left join', 'addonOptionProduct.product = productCategory.product' );
		$item->addJoin ( $joinItem );
		
	}
}

