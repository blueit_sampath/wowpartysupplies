<?php 
namespace Addon\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminAddonProductView extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-addon-product-view';
	protected $_contentName = 'adminAddonProductView';
	public function getEventName() {
		return 'adminProductView';
	}
	public function getPriority() {
		return 1000;
	}
	
}

