<?php 
namespace Addon\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminAddonNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'productMain' );
		$page->addPages ( array (
				array (
						'label' => 'Addon',
						'route' => 'admin-addon',
						'id' => 'admin-addon',
						'iconClass' => 'glyphicon-plus' 
				) 
		) );
	}
}

