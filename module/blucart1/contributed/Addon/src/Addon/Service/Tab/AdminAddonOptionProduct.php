<?php

namespace Addon\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminAddonOptionProduct extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$productId = Functions::fromRoute ( 'productId' );
		if ($productId) {
			
			$u = $url ( 'admin-product-addon-option', array (
					'productId' => $productId 
			) );
			$tabContainer->add ( 'admin-product-addon-option', 'Addons', $u, 800 );
		}
		return $this;
	}
}

