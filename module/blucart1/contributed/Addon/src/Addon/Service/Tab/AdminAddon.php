<?php

namespace Addon\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminAddon extends AbstractTabEvent {
	public function getEventName() {
		return 'adminAddonAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$addonId = Functions::fromRoute ( 'addonId', null );
		if ($addonId) {
			$u = $url ( 'admin-addon-add', array (
					'addonId' => $addonId 
			) );
			$tabContainer->add ( 'admin-addon-add', 'General Information', $u, 1000 );
		}
		$addonId = Functions::fromRoute ( 'addonId', null );
		if ($addonId) {
			$u = $url ( 'admin-addon-option', array (
					'addonId' => $addonId 
			) );
			$tabContainer->add ( 'admin-addon-option', 'Addon Option', $u, 1000 );
		}
		
		return $this;
	}
}

