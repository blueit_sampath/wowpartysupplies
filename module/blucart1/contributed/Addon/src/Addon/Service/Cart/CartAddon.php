<?php

namespace Addon\Service\Cart;

use Core\Functions;
use Checkout\Option\CartEvent;
use Product\Api\ProductApi;
use Addon\Api\AddonApi;

class CartAddon extends CartEvent {

    public function getPriority() {
        return 500;
    }

    public function prepareCartItem() {
        $cart = $this->getCart();
        $cartItem = $this->getCartItem();

        $productId = $cartItem->getProductId();

        if (!$productId) {
            return;
        }
        $product = ProductApi::getProductById($productId);
        if (!$product) {
            return;
        }

        $results = AddonApi::getAddonByProductId($productId);
        if (!$results) {
            return;
        }
        $formData = $cartItem->getParam('formData');
        if (!isset($formData ['addons'])) {
            return;
        }

        foreach ($formData['addons'] as $addonOptionId) {

            $result = AddonApi::checkOrGetAddonOptionByProductIdAndAddonOptionId($productId, $addonOptionId);
            if (!$result) {
                continue;
            }

            if ($cartItem->getInItem('addon_' . $result->addonOption->id)) {
                return;
            }

            if ($result->file && $result->file->path) {
                $cartItem->setImage($result->file->path);
            }


            if ($result->adjustmentListPrice) {
                $price = $cartItem->getListPrice();

                $s = $result->adjustmentListPrice;
                if ($result->isPercentage) {
                    $s = ($price * $s) / 100;
                }

                $cartItem->setListPrice($price + $s);
            }
            if ($result->adjustmentCostPrice) {
                $price = $cartItem->getCostPrice();
                $s = $result->adjustmentCostPrice;
                if ($result->isPercentage) {
                    $s = ($price * $s) / 100;
                }
                $cartItem->setCostPrice($price + $s);
            }
            if ($result->adjustmentSellPrice) {
                $price = $cartItem->getSellPrice();

                $s = $result->adjustmentSellPrice;

                if ($result->isPercentage) {
                    $s = ($price * $s) / 100;
                }

                $cartItem->setSellPrice($price + $s);
            }

            $title = $result->name;
            if (!$title) {

                $title = $result->addonOption->title;
            }

            $addonTitle = $result->addonOption->addon->title;
            $addonResult = AddonApi::checkOrGetAddonByProductIdAddonId($productId, $result->addonOption->addon->id);
            if ($addonResult) {
                if ($addonResult->title) {
                    $addonTitle = $addonResult->title;
                }
            }
            $inItem = $cartItem->addQuickInItem('addon_' . $result->addonOption->id, $addonTitle, $title, array(
                'addonOptionProductId' => $result->id
            ));

            $inItem->setIsCurrency(false);
            $inItem->setIncludePrice(false);
        }

        return true;
    }

}
