<?php

namespace Addon\Api;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Common\Api\Api;

class AddonApi extends Api {

    protected static $_entity = '\Addon\Entity\Addon';
    protected static $_entityOption = '\Addon\Entity\AddonOption';
    protected static $_entityOptionProduct = '\Addon\Entity\AddonOptionProduct';
    protected static $_entityProduct = '\Addon\Entity\AddonProduct';
    protected static $_entityCategory = '\Addon\Entity\AddonCategory';
    protected static $_tempArray = array();

    public static function getAddonById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getAddonByName($name) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
                    'name' => $name
        ));
    }

    public static function getAddonOptionByName($name) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityOption)->findOneBy(array(
                    'name' => $name
        ));
    }

    public static function getAddons() {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('addon.id', 'addonId');

        $item = $queryBuilder->addFrom(static::$_entity, 'addon');
        return $queryBuilder->executeQuery();
    }

    public static function getAddonCategoryById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityCategory, $id);
    }

    public static function getAddonByCategoryId($categoryId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('addonCategory.id', 'addonCategoryId');

        $queryBuilder->addWhere('addonCategory.category', 'addonCategory');
        $queryBuilder->addParameter('addonCategory', $categoryId);

        $item = $queryBuilder->addFrom(static::$_entityCategory, 'addonCategory');
        $joinItem = new QueryJoinItem('addonCategory.addon', 'addon');
        $item->addJoin($joinItem);
        $queryBuilder->addOrder('addonCategory.weight', 'weight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getAddonOptionById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityOption, $id);
    }

    public static function getAddonProductById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityProduct, $id);
    }

    public static function getAddonOptionProductById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entityOptionProduct, $id);
    }

    public static function getAddonByProductId($productId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('addonProduct.id', 'addonProductId');

        $queryBuilder->addWhere('addonProduct.product', 'addonProduct');
        $queryBuilder->addParameter('addonProduct', $productId);

        $item = $queryBuilder->addFrom(static::$_entityProduct, 'addonProduct');
        $joinItem = new QueryJoinItem('addonProduct.addon', 'addon');
        $item->addJoin($joinItem);
        $queryBuilder->addOrder('addonProduct.weight', 'weight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getAddonOptionByAddonId($addonId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('addonOption.id', 'addonOptionId');

        $queryBuilder->addWhere('addonOption.addon', 'addonOptionAddon');
        $queryBuilder->addParameter('addonOptionAddon', $addonId);

        $item = $queryBuilder->addFrom(static::$_entityOption, 'addonOption');
        return $queryBuilder->executeQuery();
    }

    // ---------------------
    public static function checkOrGetAddonByProductIdAddonId($productId, $addonId) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entityProduct)->findOneBy(array(
                    'product' => $productId,
                    'addon' => $addonId
        ));
    }

    public static function checkOrGetAddonOptionByProductIdAndAddonOptionId($productId, $addonOptionId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entityOptionProduct)->findOneBy(array(
                    'product' => $productId,
                    'addonOption' => $addonOptionId
        ));
    }

}
