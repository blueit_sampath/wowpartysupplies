<?php 
namespace Addon\Controller;
use Core\Functions;

use Common\MVC\Controller\AbstractAdminController;

class AdminCategoryAddonController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminCategoryAddon\CategoryAddonFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-category-addon-add', array (
				'categoryId' => Functions::fromRoute('categoryId')
		) );
	}
	
	
}