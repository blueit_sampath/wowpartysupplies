<?php

namespace Addon\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminAddonController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminAddon\AddonFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-addon-add', array (
				'addonId' => $form->get ( 'id' )->getValue () 
		) );
	}
}