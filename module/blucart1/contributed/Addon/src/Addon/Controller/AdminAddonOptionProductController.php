<?php 
namespace Addon\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminAddonOptionProductController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminAddonOptionProduct\AddonOptionProductFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-addon-option-add', array (
				'productId' => $form->get ( 'product' )->getValue (),
				'addonOptionProductId' => $form->get ( 'id' )->getValue ()
		) );
	}
	
	
}