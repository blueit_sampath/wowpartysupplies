<?php

namespace Addon\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminProductAddonController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminProductAddon\ProductAddonFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-addon-add', array (
				'productId' => $form->get ( 'product' )->getValue (),
				'addonProductId' => $form->get ( 'id' )->getValue () 
		) );
	}
}