<?php

namespace Addon\Controller;

use Core\Functions;
use Common\MVC\Controller\AbstractAdminController;

class AdminBulkAddonController extends AbstractAdminController {
	public function addonAction() {
		$form = $this->getServiceLocator ()->get ( 'Addon\Form\AdminBulkAddon\BulkAddonFactory' );
		$productId = Functions::fromRoute ( 'productId' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$addons = $form->get ( 'addons' )->getValue ();
				return $this->redirect ()->toRoute ( 'admin-product-addon-option-bulk', array (
						'productId' => $productId 
				), array (
						'query' => array (
								'addons' => $addons 
						) 
				) );
			} else {
				$this->notValid ();
			}
		}
		return array (
				'form' => $form 
		);
	}
	public function addonOptionAction() {
		$form = $this->getServiceLocator ()->get ( 'Addon\Form\AdminBulkAddonOption\BulkAddonOptionFactory' );
		$productId = Functions::fromRoute ( 'productId' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Generated addon options successfully' );
				return $this->redirect ()->toRoute ( 'admin-product-addon-bulk', array (
						'productId' => $productId 
				) );
			} else {
				$this->notValid ();
			}
		}else{
			$form->getRecord();
		}
		
		return array (
				'form' => $form 
		);
	}
	
	
}