<?php

namespace Addon\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminAddonOptionController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminAddonOption\AddonOptionFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-addon-option-add', array (
				'addonId' => $form->get ( 'addon' )->getValue (),
				'addonOptionId' => $form->get ( 'id' )->getValue () 
		) );
	}
}