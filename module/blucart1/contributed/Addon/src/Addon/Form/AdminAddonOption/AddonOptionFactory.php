<?php
namespace Addon\Form\AdminAddonOption;

use Common\Form\Option\AbstractFormFactory;

class AddonOptionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Addon\Form\AdminAddonOption\AddonOptionForm' );
		$form->setName ( 'adminAddonOptionAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminAddonOption\AddonOptionFilter' );
	}
}
