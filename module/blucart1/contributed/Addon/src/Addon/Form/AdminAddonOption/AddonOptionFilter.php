<?php

namespace Addon\Form\AdminAddonOption;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class AddonOptionFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'regex',
                    'options' => array(
                        'pattern' => "/^[a-zA-Z0-9_]+$/",
                        'messages' => array(
                            \Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers or _'
                        )
                    )
                ),
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => array(
                            $this,
                            'checkAddonOption'
                        ),
                        'messages' => array(
                            \Zend\Validator\Callback::INVALID_VALUE => "Addon Option Name already exists",
                        )
                    )
                )
            )
            
        ));

        $this->add(array(
            'name' => 'title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
        ));

        $this->add(array(
            'name' => 'weight',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));
        $this->add(array(
            'name' => 'defaultAdjustmentCostPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));

        $this->add(array(
            'name' => 'defaultAdjustmentListPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));
        $this->add(array(
            'name' => 'defaultAdjustmentSellPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));
    }

    public function checkAddonOption($value, $data = array()) {
       
        $entity = \Addon\Api\AddonApi::getAddonOptionByName($value);
        if (!$entity) {
            return true;
        }
        if ($entity->id == $data['id']) {
            return true;
        }
        return false;
    }

}
