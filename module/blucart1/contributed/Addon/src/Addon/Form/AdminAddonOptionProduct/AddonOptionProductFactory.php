<?php

namespace Addon\Form\AdminAddonOptionProduct;

use Common\Form\Option\AbstractFormFactory;

class AddonOptionProductFactory extends AbstractFormFactory {

    public function getForm() {
        $form = $this->getServiceLocator()->get('Addon\Form\AdminAddonOptionProduct\AddonOptionProductForm');
        $form->setName('adminAddonOptionProductAdd');
        return $form;
    }

    public function getFormFilter() {
        return $this->getServiceLocator()->get('Addon\Form\AdminAddonOptionProduct\AddonOptionProductFilter');
    }

}
