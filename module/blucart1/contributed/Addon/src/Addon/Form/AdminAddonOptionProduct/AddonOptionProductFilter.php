<?php

namespace Addon\Form\AdminAddonOptionProduct;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class AddonOptionProductFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'name',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            ),
// 				'validators' => array (
// 						array (
// 								'name' => 'Regex',
// 								'options' => array (
// 										'pattern' => '/^[A-Za-z0-9]+$/' 
// 								) 
// 						) 
// 				) 
        ));


      
        $this->add(array(
            'name' => 'image',
            'required' => false,
        ));
        $this->add(array(
            'name' => 'adjustmentCostPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));

        $this->add(array(
            'name' => 'adjustmentListPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));
        $this->add(array(
            'name' => 'adjustmentSellPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));

        $this->add(array(
            'name' => 'isPercentage',
            'required' => false,
        ));

        $this->add(array(
            'name' => 'isRequired',
            'required' => false,
        ));
    }

}
