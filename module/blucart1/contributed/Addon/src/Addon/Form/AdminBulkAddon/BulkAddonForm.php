<?php

namespace Addon\Form\AdminBulkAddon;

use Addon\Api\AddonApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class BulkAddonForm extends Form 

{
	public function init() {
	
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$select = new Select ( 'addons' );
		$select->setLabel ( 'Addons' );
		$select->setAttribute('multiple', 'multiple');
		$select->setValueOptions ( $this->getCartAddons () );
		$this->add ( $select, array (
				'priority' => 950 
		) );
		
		
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Next >' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCartAddons() {
		$array = array ();
		$results = AddonApi::getAddons ();
		foreach ( $results as $result ) {
			$addonId = $result ['addonId'];
			$addon = AddonApi::getAddonById ( $addonId );
			$array [$addon->id] = $addon->name;
		}
		return $array;
	}
}