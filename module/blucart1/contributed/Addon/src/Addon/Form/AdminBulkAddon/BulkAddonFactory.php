<?php
namespace Addon\Form\AdminBulkAddon;

use Common\Form\Option\AbstractFormFactory;

class BulkAddonFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Addon\Form\AdminBulkAddon\BulkAddonForm' );
		$form->setName ( 'adminBulkAddonAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminBulkAddon\BulkAddonFilter' );
	}
}
