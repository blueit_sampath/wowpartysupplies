<?php
namespace Addon\Form\AdminProductAddon;

use Common\Form\Option\AbstractFormFactory;

class ProductAddonFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Addon\Form\AdminProductAddon\ProductAddonForm' );
		$form->setName ( 'adminProductAddonAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminProductAddon\ProductAddonFilter' );
	}
}
