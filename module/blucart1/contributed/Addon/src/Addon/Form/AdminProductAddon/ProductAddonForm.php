<?php

namespace Addon\Form\AdminProductAddon;

use Zend\Form\Element\Select;
use Common\Form\Form;

class ProductAddonForm extends Form {

    public function init() {
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
        $this->add(array(
            'name' => 'product',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Override Title'
            )
                ), array(
            'priority' => 1000
        ));
        $select = new Select('type');
        $select->setLabel('Type');
        $select->setValueOptions(array(
            'radio' => 'Radio',
            'select' => 'Select'
        ));
        $this->add($select, array(
            'priority' => 990
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isRequired',
            'options' => array(
                'label' => 'Is Required?',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'name' => 'weight',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Priority'
            )
                ), array(
            'priority' => 970
        ));



        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
