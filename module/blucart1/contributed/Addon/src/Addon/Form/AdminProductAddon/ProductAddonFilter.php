<?php
namespace Addon\Form\AdminProductAddon;

use Zend\InputFilter\InputFilter;

class  ProductAddonFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'title',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						),
						array (
								'name' => 'StripTags'
						),
						
				)
		) );
		
		$this->add ( array (
				'name' => 'type',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						),
						array (
								'name' => 'StripTags'
						),
		
				)
		) );
		
                $this->add ( array (
				'name' => 'isRequired',
				'required' => false,
                    ));
		$this->add ( array (
				'name' => 'weight',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		
		
	}
} 