<?php

namespace Addon\Form;

use Addon\Api\AddonApi;
use Core\Functions;
use Zend\Form\Element\MultiCheckbox;
use Config\Api\ConfigApi;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class AddonFilterFieldset extends Fieldset implements InputFilterProviderInterface {
	protected $_inputs = array ();
	public function __construct() {
		parent::__construct ( 'addons' );
		
		$this->addAddonElements ();
	}
	public function addAddonElements() {
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		
		$results = AddonApi::getAddonByCategoryId ( $categoryId );
		
		foreach ( $results as $result ) {
			
			$addonCategory = AddonApi::getAddonCategoryById ( $result ['addonCategoryId'] );
			$addon = $addonCategory->addon;
			
			$results2 = AddonApi::getAddonOptionByAddonId ( $addon->id );
			$array = array ();
			foreach ( $results2 as $result2 ) {
				
				
				$option = AddonApi::getAddonOptionById ( $result2 ['addonOptionId'] );
				$array [$option->id] = $option->title;
			}
			
			
			if (count ( $array )) {
				$this->_inputs ['addon_' . $addon->id] = array (
						'required' => false 
				);
				$multiCheckbox = new MultiCheckbox ( 'addon_' . $addon->id );
				$multiCheckbox->setLabel ( $addonCategory->title ? $addonCategory->title : $addon->title );
				$multiCheckbox->setValueOptions (
						$array 
				 );
				$this->add ( $multiCheckbox );
			}
		}
	}
	
	/**
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		return $this->_inputs;
		
	}
}
