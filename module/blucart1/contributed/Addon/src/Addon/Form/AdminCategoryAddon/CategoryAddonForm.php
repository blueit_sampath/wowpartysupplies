<?php

namespace Addon\Form\AdminCategoryAddon;

use Addon\Api\AddonApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CategoryAddonForm extends Form 

{
	public function init() {
		
		
	
		
		$select = new Select ( 'addons' );
		$select->setLabel ( 'Addons' );
		$select->setValueOptions ( $this->getCartAddons () );
		$select->setAttribute ( 'multiple', 'true' );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCartAddons() {
		$array = array (
				'' => '' 
		);
		$results = AddonApi::getAddons ();
		
		foreach ( $results as $result ) {
			$addon = AddonApi::getAddonById ( $result ['addonId'] );
			$array [$addon->id] = $addon->title;
		}
		return $array;
	}
}