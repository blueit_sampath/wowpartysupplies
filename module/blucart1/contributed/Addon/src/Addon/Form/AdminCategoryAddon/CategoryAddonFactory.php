<?php
namespace Addon\Form\AdminCategoryAddon;

use Common\Form\Option\AbstractFormFactory;

class CategoryAddonFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Addon\Form\AdminCategoryAddon\CategoryAddonForm' );
		$form->setName ( 'adminCategoryAddon' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminCategoryAddon\CategoryAddonFilter' );
	}
}
