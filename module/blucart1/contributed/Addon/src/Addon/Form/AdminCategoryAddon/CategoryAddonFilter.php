<?php
namespace Addon\Form\AdminCategoryAddon;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  CategoryAddonFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'addons',
				'required' => true,
				
		) );
		
	}
} 