<?php

namespace Addon\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Radio;
use Core\Functions;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Addon\Api\AddonApi;

class ProductAddToCartAddonFieldset extends Fieldset implements InputFilterProviderInterface {
	protected $_inputs = array ();
	protected $_productId = '';
	public function __construct($productId = 0, $name = 'addons') {
		$this->_productId = $productId;
		parent::__construct ( $name );
		
		$this->addAddonElements ();
	}
	public function addAddonElements() {
		$productId = $this->_productId;
		
		$results = AddonApi::getAddonByProductId ( $productId );
		
		foreach ( $results as $result ) {
			
			$addonProduct = AddonApi::getAddonProductById ( $result ['addonProductId'] );
			$addon = $addonProduct->addon;
			
			$results2 = AddonApi::getAddonOptionByAddonId ( $addon->id );
			$array = array ();
			foreach ( $results2 as $result2 ) {
				
				if (! ($result = AddonApi::checkOrGetAddonOptionByProductIdAndAddonOptionId ( $productId, $result2 ['addonOptionId'] ))) {
					continue;
				}
				
				$option = $result->addonOption;
				$array [$option->id] = $result->name ? $result->name : $option->title;
			}
			if (count ( $array )) {
				$this->addAddonElement ( $addon, $array, $addonProduct->type, $addonProduct->title,$addonProduct->isRequired );
			}
		}
	}
	public function addAddonElement($addon, $options, $type = '', $title = '',$isRequired = false) {
		if ($type == 'radio') {
			$element = new Radio ( 'addon_' . $addon->id );
		} else {
			$element = new Select ( 'addon_' . $addon->id );
                         $array = array('' => 'Please Select');
                        $options = $array + $options;
		}
		
		$element->setLabel ( $title ? $title : $addon->title );
		$element->setValueOptions ( $options );
		$this->add ( $element );
                if(!$isRequired){
                    $this->_inputs [] = $element->getName ();
                }
		
	}
	
	/**
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		$array = array ();
		foreach ( $this->_inputs as $input ) {
			$array [$input] = array (
					'required' => false 
			);
		}
		
		return $array;
	}
}
