<?php
namespace Addon\Form\AdminAddon;

use Common\Form\Option\AbstractFormFactory;

class AddonFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Addon\Form\AdminAddon\AddonForm' );
		$form->setName ( 'adminAddonAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminAddon\AddonFilter' );
	}
}
