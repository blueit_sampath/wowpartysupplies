<?php

namespace Addon\Form\AdminBulkAddonOption;

use Addon\Api\AddonApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class BulkAddonOptionForm extends Form 

{
	public function init() {
		
		$this->formAddonOptions ();
		
		$this->add ( array (
				'name' => 'addons',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Generate' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function formAddonOptions() {
		$addons = Functions::fromPost ( 'addons',Functions::fromQuery('addons') );
		if (is_string ( $addons )) {
			$addons = explode ( ',', $addons );
		}
		
		foreach ( $addons as $addonId ) {
			$addon = AddonApi::getAddonById ( $addonId );
			$options = AddonApi::getAddonOptionByAddonId ( $addonId );
			$array = array ();
			foreach ( $options as $result ) {
				$option = AddonApi::getAddonOptionById ( $result ['addonOptionId'] );
				$array [$option->id] = $option->name;
			}
			$select = new Select ( 'addon_' . $addonId );
			$select->setLabel ( $addon->name . ' Options' );
			$select->setAttribute ( 'multiple', 'multiple' );
			$select->setValueOptions ( $array );
			$select->setValue ( array_keys ( $array ) );
			$this->add ( $select, array (
					'priority' => 1000 
			) );
		}
	}
}