<?php

namespace Addon\Form\AdminBulkAddonOption;

use Addon\Api\AddonApi;
use Core\Functions;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BulkAddonOptionFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->formAddonOptions();
	}
	public function formAddonOptions() {
	$addons = Functions::fromPost ( 'addons',Functions::fromQuery('addons') );
		if (is_string ( $addons )) {
			$addons = explode ( ',', $addons );
		}
		foreach ( $addons as $addonId ) {
			$addon = AddonApi::getAddonById ( $addonId );
			$options = AddonApi::getAddonOptionByAddonId ( $addonId );
			$array = array ();
			
			$this->add ( array (
					'name' => 'addon_' . $addonId,
					'required' => true,
			) );
		}
	}
} 