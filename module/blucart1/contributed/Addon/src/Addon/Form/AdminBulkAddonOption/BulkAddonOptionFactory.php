<?php
namespace Addon\Form\AdminBulkAddonOption;

use Common\Form\Option\AbstractFormFactory;

class BulkAddonOptionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Addon\Form\AdminBulkAddonOption\BulkAddonOptionForm' );
		$form->setName ( 'adminBulkAddonOptionAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Addon\Form\AdminBulkAddonOption\BulkAddonOptionFilter' );
	}
}
