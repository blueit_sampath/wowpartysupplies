<?php

namespace Addon\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AddonOptionProduct
 *
 * @ORM\Table(name="addon_option_product")
 * @ORM\Entity
 */
class AddonOptionProduct {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="name", type="string",
	 *      length=255, nullable=true)
	 */
	private $name;
	
	
	/**
	 *
	 * @var string @ORM\Column(name="adjustment_sell_price", type="string",
	 *      length=255, nullable=true)
	 */
	private $adjustmentSellPrice;
	
	/**
	 *
	 * @var string @ORM\Column(name="adjustment_list_price", type="string",
	 *      length=255, nullable=true)
	 */
	private $adjustmentListPrice;
	
	/**
	 *
	 * @var string @ORM\Column(name="adjustment_cost_price", type="string",
	 *      length=255, nullable=true)
	 */
	private $adjustmentCostPrice;
        
         /**
	 *
	 * @var string @ORM\Column(name="is_percentage", type="boolean", nullable=true)
	 */
	private $isPercentage;
        
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	
	/**
	 *
	 * @var \File @ORM\ManyToOne(targetEntity="\File\Entity\File")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="file_id", referencedColumnName="id",nullable=true,onDelete="SET NULL")
	 *      })
	 */
	private $file;
	
	/**
	 *
	 * @var \Product @ORM\ManyToOne(targetEntity="\Product\Entity\Product")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="product_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $product;
	
	/**
	 *
	 * @var AddonOption @ORM\ManyToOne(targetEntity="\Addon\Entity\AddonOption")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="addon_option_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $addonOption;
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
}
