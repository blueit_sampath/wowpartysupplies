<?php 
namespace Brand\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;



class AdminBrandNavigation extends \Admin\Navigation\Event\AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'productMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Brands',
							'route' => 'admin-brand',
							'id' => 'admin-brand',
							'iconClass' => 'glyphicon-font',
                                                       
					) 
			) );
		}
	}
}

