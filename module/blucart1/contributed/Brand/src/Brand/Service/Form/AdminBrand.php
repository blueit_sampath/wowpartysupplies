<?php

namespace Brand\Service\Form;

use File\Api\FileApi;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminBrand extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight',
			'status',
			'title' 
	);
	protected $_entity = '\Brand\Entity\Brand';
	protected $_entityName = 'brand';
	public function getFormName() {
		return 'adminBrandAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		FileApi::deleteFiles ( Functions::fromPost ( 'image_delete', '' ) );
		
		$image = $params ['image'];
		
		if ($image) {
			$array = explode ( ',', $image );
			foreach ( $array as $file ) {
				$imageEntity = FileApi::createOrUpdateFile ( $file, $params ['title'], $params ['title'] );
			}
                        $entity->file = $imageEntity;
		}
		
		
	}
	
	public function afterGetRecord($entity){
		$form = $this->getForm();
                if($entity->file){
		$form->get ( 'image' )->setValue ( $entity->file->path );
                }
	}
	
}
