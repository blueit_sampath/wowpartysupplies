<?php 
namespace Brand\Service\Form;

use Brand\Entity\BrandCategory;

use Brand\Api\BrandApi;

use Category\Api\CategoryApi;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminCategoryBrand extends AbstractMainFormEvent {
	
	protected $_entity = '\Brand\Entity\BrandCategory';
	protected $_entityName = 'brandCategory';
	public function getFormName() {
		return 'adminCategoryBrand';
	}
	public function getPriority() {
		return 1000;
	}
	
	
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		$categoryId = Functions::fromRoute('categoryId');
	
		if ($categoryId) {
			$category = CategoryApi::getCategoryById ( $categoryId );
				
			if ($category) {
				$results = BrandApi::getBrandByCategoryId ( $category->id );
				$array = array ();
				if ($results) {
					foreach ( $results as $result ) {
						$array [] = ( int ) $result ['brandId'];
					}
				}
				foreach ( $params ['brands'] as $r ) {
					if (! in_array ( ( int ) $r, $array )) {
						$e = new BrandCategory ();
						$e->category = $category;
						$e->brand = BrandApi::getBrandById ( $r );
						$em->persist ( $e );
					}
				}
	
				$em->flush ();
			}
		}
	
		return true;
	}
	
	
}
