<?php
namespace Brand\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminBrand extends AbstractTabEvent {
	public function getEventName() {
		return 'adminBrandAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$brandId = Functions::fromRoute ( 'brandId' );
		if (! $brandId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-brand-add', array (
				'brandId' => $brandId 
		) );
		$tabContainer->add ( 'admin-brand-add', 'General', $u, 1000 );
		
		return $this;
	}
}

