<?php

namespace Brand\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategoryBrand extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$categoryId = Functions::fromRoute ( 'categoryId' );
		if ($categoryId) {
			$tabContainer = $this->getTabContainer ();
			$url = Functions::getUrlPlugin ();
			
			$u = $url ( 'admin-category-brand', array (
					'categoryId' => $categoryId 
			) );
			$tabContainer->add ( 'admin-category-brand', 'Brands', $u, 800 );
		}
		
		return $this;
	}
}

