<?php

namespace Brand\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminBrand extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminBrand';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-brand-add' );
		$item = $linkContainer->add ( 'admin-brand-add', 'Add Brand', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-brand-sort' );
		$item = $linkContainer->add ( 'admin-brand-sort', 'Arrange Brands', $u, 990 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

