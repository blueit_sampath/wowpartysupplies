<?php 
namespace Brand\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategoryBrandLink extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategoryBrand';
	}
	public function link() {
		$categoryId = Functions::fromRoute('categoryId');
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-category-brand-add', array('categoryId' => $categoryId));
		$item = $linkContainer->add ( 'admin-category-brand-add', 'Add Brand', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

