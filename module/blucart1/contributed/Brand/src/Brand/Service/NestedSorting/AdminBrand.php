<?php

namespace Brand\Service\NestedSorting;

use Core\Functions;
use Brand\Api\BrandApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminBrand extends AbstractNestedSortingEvent {
	protected $_entityName = '\Brand\Entity\Brand';
	protected $_column = 'title';
	public function getEventName() {
		return 'adminBrandSort';
	}
	public function getPriority() {
		return 1000;
	}
	
}
