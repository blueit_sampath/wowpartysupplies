<?php

namespace Brand\Form\AdminCategoryBrand;

use Brand\Api\BrandApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CategoryBrandForm extends Form 

{
	public function init() {
		
		$select = new Select ( 'brands' );
		$select->setLabel ( 'Brands' );
		$select->setValueOptions ( $this->getCartBrands () );
		$select->setAttribute ( 'multiple', 'true' );
		$this->add ( $select, array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCartBrands() {
		$array = array (
				'' => '' 
		);
		$results = BrandApi::getBrands ();
		
		foreach ( $results as $result ) {
			$brand = BrandApi::getBrandById ( $result ['brandId'] );
			$array [$brand->id] = $brand->title;
		}
		return $array;
	}
}