<?php
namespace Brand\Form\AdminCategoryBrand;

use Common\Form\Option\AbstractFormFactory;

class CategoryBrandFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Brand\Form\AdminCategoryBrand\CategoryBrandForm' );
		$form->setName ( 'adminCategoryBrand' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Brand\Form\AdminCategoryBrand\CategoryBrandFilter' );
	}
}
