<?php
namespace Brand\Form\AdminBrand;

use Common\Form\Option\AbstractFormFactory;

class BrandFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Brand\Form\AdminBrand\BrandForm' );
		$form->setName ( 'adminBrandAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Brand\Form\AdminBrand\BrandFilter' );
	}
}
