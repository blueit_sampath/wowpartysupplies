<?php

namespace Brand\Api;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;
use Common\Api\Api;

class BrandApi extends Api {

    protected static $_entity = '\Brand\Entity\Brand';
    protected static $_entityCategory = '\Brand\Entity\BrandCategory';

    public static function getBrandById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getBrandByCategoryId($categoryId) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('brand.id', 'brandId');

        $queryBuilder->addWhere('brandCategory.category', 'brandCategoryCategory');
        $queryBuilder->addParameter('brandCategoryCategory', $categoryId);

        $item = $queryBuilder->addFrom(static::$_entityCategory, 'brandCategory');
        $joinItem = new QueryJoinItem('brandCategory.brand', 'brand');
        $item->addJoin($joinItem);
        return $queryBuilder->executeQuery();
    }

    public static function getBrands($status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->setEventName('BrandApi.getBrands');
        $queryBuilder->addColumn('brand.id', 'brandId');

        if ($status !== null) {
            $queryBuilder->addWhere('brand.status', 'brandStatus');
            $queryBuilder->addParameter('brandStatus', $status);
        }
        $queryBuilder->addOrder('brand.weight', 'brandWeight', 'desc');
        $queryBuilder->addGroup('brand.id', 'brandId');
        $item = $queryBuilder->addFrom(static::$_entity, 'brand');
        return $queryBuilder->executeQuery();
    }

  

}
