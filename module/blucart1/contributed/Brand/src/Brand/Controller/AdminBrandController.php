<?php

namespace Brand\Controller;

use Common\MVC\Controller\AbstractAdminController;

use Core\Functions;

use Zend\View\Model\ViewModel;

class AdminBrandController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Brand\Form\AdminBrand\BrandFormFactory' );
	}
	
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-brand-add', array (
				'brandId' => $form->get ( 'id' )->getValue () 
		) );
	}
}