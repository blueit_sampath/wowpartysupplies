<?php 
namespace Brand\Controller;
use Core\Functions;

use Common\MVC\Controller\AbstractAdminController;

class AdminCategoryBrandController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Brand\Form\AdminCategoryBrand\CategoryBrandFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-category-brand-add', array (
				'categoryId' => Functions::fromRoute('categoryId')
		) );
	}
	
	
}