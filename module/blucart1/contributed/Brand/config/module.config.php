<?php
		
		return array(
    'router' => array('routes' => array(
            'admin-brand' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/brand',
                    'defaults' => array(
                        'controller' => 'Brand\Controller\AdminBrand',
                        'action' => 'index'
                        )
                    )
                ),
            'admin-brand-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/brand/add[/ajax/:ajax][/:brandId]',
                    'defaults' => array(
                        'controller' => 'Brand\Controller\AdminBrand',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'brandId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-brand-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/brand/sort[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Brand\Controller\AdminBrand',
                        'action' => 'sort',
                        'ajax' => 'true'
                        ),
                    'constraints' => array('ajax' => 'true|false')
                    )
                ),
            'admin-category-brand' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/brand[/ajax/:ajax][/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Brand\Controller\AdminCategoryBrand',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-category-brand-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/brand/add[/ajax/:ajax][/:categoryId][/:brandCategoryId]',
                    'defaults' => array(
                        'controller' => 'Brand\Controller\AdminCategoryBrand',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'brandCategoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                )
            )),
    'events' => array(
        'Brand\Service\Grid\AdminBrand' => 'Brand\Service\Grid\AdminBrand',
        'Brand\Service\Form\AdminBrand' => 'Brand\Service\Form\AdminBrand',
        'Brand\Service\Tab\AdminBrand' => 'Brand\Service\Tab\AdminBrand',
        'Brand\Service\Link\AdminBrand' => 'Brand\Service\Link\AdminBrand',
        'Brand\Service\NestedSorting\AdminBrand' => 'Brand\Service\NestedSorting\AdminBrand',
        'Brand\Service\Form\AdminCategoryBrand' => 'Brand\Service\Form\AdminCategoryBrand',
        'Brand\Service\Grid\AdminCategoryBrand' => 'Brand\Service\Grid\AdminCategoryBrand',
        'Brand\Service\Tab\AdminCategoryBrand' => 'Brand\Service\Tab\AdminCategoryBrand',
        'Brand\Service\Link\AdminCategoryBrandLink' => 'Brand\Service\Link\AdminCategoryBrandLink',
        'Brand\Service\Navigation\AdminBrandNavigation' => 'Brand\Service\Navigation\AdminBrandNavigation'
        ),
    'service_manager' => array(
        'invokables' => array(
            'Brand\Service\Grid\AdminBrand' => 'Brand\Service\Grid\AdminBrand',
            'Brand\Service\Form\AdminBrand' => 'Brand\Service\Form\AdminBrand',
            'Brand\Form\AdminBrand\BrandForm' => 'Brand\Form\AdminBrand\BrandForm',
            'Brand\Form\AdminBrand\BrandFilter' => 'Brand\Form\AdminBrand\BrandFilter',
            'Brand\Service\Tab\AdminBrand' => 'Brand\Service\Tab\AdminBrand',
            'Brand\Service\Link\AdminBrand' => 'Brand\Service\Link\AdminBrand',
            'Brand\Service\NestedSorting\AdminBrand' => 'Brand\Service\NestedSorting\AdminBrand',
            'Brand\Form\AdminCategoryBrand\CategoryBrandForm' => 'Brand\Form\AdminCategoryBrand\CategoryBrandForm',
            'Brand\Form\AdminCategoryBrand\CategoryBrandFilter' => 'Brand\Form\AdminCategoryBrand\CategoryBrandFilter',
            'Brand\Service\Form\AdminCategoryBrand' => 'Brand\Service\Form\AdminCategoryBrand',
            'Brand\Service\Grid\AdminCategoryBrand' => 'Brand\Service\Grid\AdminCategoryBrand',
            'Brand\Service\Tab\AdminCategoryBrand' => 'Brand\Service\Tab\AdminCategoryBrand',
            'Brand\Service\Link\AdminCategoryBrandLink' => 'Brand\Service\Link\AdminCategoryBrandLink',
            'Brand\Service\Navigation\AdminBrandNavigation' => 'Brand\Service\Navigation\AdminBrandNavigation'
            ),
        'factories' => array(
            'Brand\Form\AdminBrand\BrandFormFactory' => 'Brand\Form\AdminBrand\BrandFormFactory',
            'Brand\Form\AdminCategoryBrand\CategoryBrandFactory' => 'Brand\Form\AdminCategoryBrand\CategoryBrandFactory'
            )
        ),
    'controllers' => array('invokables' => array(
            'Brand\Controller\AdminBrand' => 'Brand\Controller\AdminBrandController',
            'Brand\Controller\AdminCategoryBrand' => 'Brand\Controller\AdminCategoryBrandController'
            )),
    'view_manager' => array('template_path_stack' => array('Brand' => __DIR__.'/../view')),
    'doctrine' => array('driver' => array(
            'Brand_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__.'/../src/Brand/Entity')
                ),
            'orm_default' => array('drivers' => array('Brand\Entity' => 'Brand_driver'))
            )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('Brand' => __DIR__.'/../public')))
    );
