<?php

namespace ShippingAmount\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ShippingAmount
 *
 * @ORM\Table(name="shipping_amount")
 * @ORM\Entity
 */
class ShippingAmount {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 * @var boolen
	 *
	 * @ORM\Column(name="status", type="boolean",  nullable=true)
	 */
	private $status;
	/**
	 *
	 * @var float @ORM\Column(name="min_amount", type="float", nullable=true)
	 */
	private $minAmount;
	
	/**
	 *
	 * @var float @ORM\Column(name="max_amount", type="float", nullable=true)
	 */
	private $maxAmount;
	
	/**
	 *
	 * @var string @ORM\Column(name="title", type="string", length=255,
	 *      nullable=true)
	 */
	private $title;
	
	/**
	 *
	 * @var string @ORM\Column(name="name", type="string", length=255,
	 *      nullable=true)
	 */
	private $name;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $property        	
	 * @return mixed
	 *
	 */
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
}
