<?php
namespace ShippingAmount\Form\AdminShippingAmountCountry;

use Common\Form\Option\AbstractFormFactory;

class ShippingCountryFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryForm' );
		$form->setName ( 'adminShippingAmountCountryAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryFilter' );
	}
}
