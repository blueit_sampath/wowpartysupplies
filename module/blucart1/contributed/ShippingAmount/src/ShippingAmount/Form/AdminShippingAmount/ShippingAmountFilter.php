<?php
namespace ShippingAmount\Form\AdminShippingAmount;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  ShippingAmountFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'name',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StripTags'
						),
						array (
								'name' => 'StringTrim'
						)
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255
								)
						)
				)
		) );
		
		$this->add ( array (
				'name' => 'title',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'minAmount',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Float' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'maxAmount',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Float'
						)
				)
		) );
		
		
		$this->add ( array (
				'name' => 'status',
				'required' => true,
				'validators' => array (
						
						array (
								'name' => 'Int' 
						) 
				) 
		) );
	}
} 