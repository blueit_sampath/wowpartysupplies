<?php

namespace ShippingAmount\Form\AdminShippingAmount;

use Config\Api\ConfigApi;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class ShippingAmountForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text' 
				)
				,
				'options' => array (
						'label' => 'Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'title',
				'attributes' => array (
						'type' => 'text' 
				)
				,
				'options' => array (
						'label' => 'Title' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'minAmount',
				'attributes' => array (
						'type' => 'text' 
				)
				,
				'options' => array (
						'label' => 'Minimum Amount',
						'prependText' => ConfigApi::getCurrencySymbol () 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'maxAmount',
				'attributes' => array (
						'type' => 'text' 
				)
				,
				'options' => array (
						'label' => 'Maximum Amount',
						'prependText' => ConfigApi::getCurrencySymbol () 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->getStatus ( 960 );
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}