<?php
namespace ShippingAmount\Form\AdminShippingAmount;

use Common\Form\Option\AbstractFormFactory;

class ShippingAmountFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'ShippingAmount\Form\AdminShippingAmount\ShippingAmountForm' );
		$form->setName ( 'adminShippingAmountAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'ShippingAmount\Form\AdminShippingAmount\ShippingAmountFilter' );
	}
}
