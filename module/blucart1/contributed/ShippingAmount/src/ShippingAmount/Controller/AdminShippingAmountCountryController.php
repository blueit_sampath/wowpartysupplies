<?php 
namespace ShippingAmount\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminShippingAmountCountryController extends AbstractAdminController {
	
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryFactory' );
	}
	
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		
		return $this->redirect ()->toRoute ( 'admin-shipping-amount-country-add', array (
				'shippingAmountId' => $form->get ( 'shippingAmount' )->getValue (),
				'shippingAmountCountryId' => $form->get ( 'id' )->getValue ()
		) );
	}
	
}