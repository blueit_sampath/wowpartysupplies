<?php 
namespace ShippingAmount\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminShippingAmountController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'ShippingAmount\Form\AdminShippingAmount\ShippingAmountFactory' );
	}
	
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		
		return $this->redirect ()->toRoute ( 'admin-shipping-amount-add', array (
				'shippingAmountId' => $form->get ( 'id' )->getValue ()
		) );
	}
	
	
}