<?php

namespace ShippingAmount\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminShippingAmount extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'title',
			'minAmount',
			'maxAmount',
			'status' 
	);
	protected $_entity = '\ShippingAmount\Entity\ShippingAmount';
	protected $_entityName = 'shippingAmount';
	
	protected $_detailTemplateName =  'adminShippingAmountCountry';
	protected $_detailTemplate = 'common/admin-shipping-amount-country';
	
	public function getEventName() {
		return 'adminShippingAmount';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		$array = array ();
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary(true);
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'shippingAmount.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'shippingAmount.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'shippingAmount.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'minAmount' );
		$columnItem->setTitle ( 'Minimum Amount' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columnItem->setFormat ( '{0:c}' );
		$columns->addColumn ( 'shippingAmount.minAmount', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'maxAmount' );
		$columnItem->setTitle ( 'Maximum Amount' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columnItem->setFormat ( '{0:c}' );
		$columns->addColumn ( 'shippingAmount.maxAmount', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'shippingAmount.status', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
}
