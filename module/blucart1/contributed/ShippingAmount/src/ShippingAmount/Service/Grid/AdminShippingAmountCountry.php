<?php

namespace ShippingAmount\Service\Grid;

use Core\Functions;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminShippingAmountCountry extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			
			'country',
			
			'amount' 
	);
	protected $_entity = 'ShippingAmount\Entity\ShippingAmountCountry';
	protected $_entityName = 'shippingAmountCountry';
	public function getEventName() {
		return 'adminShippingAmountCountry';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		$shippingAmountId = Functions::fromRoute ( 'shippingAmountId', 0 );
		$array = array ();
		$grid = $this->getGrid ();
		$grid->setAdditionalParameters ( array (
				'shippingAmountId' => $shippingAmountId 
		) );
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'shippingAmountCountry.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'country' );
		$columnItem->setTitle ( 'Country' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'shippingAmountCountry.country', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'amount' );
		$columnItem->setTitle ( 'Amount' );
		$columnItem->setFormat ( '{0:c}' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'shippingAmountCountry.amount', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		$shippingAmountId = Functions::fromQuery ( 'shippingAmountId', 0 );
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		
		$item = $queryBuilder->addWhere ( 'shippingAmountCountry.shippingAmount', 'shippingAmountId' );
		$queryBuilder->addParameter ( 'shippingAmountId', $shippingAmountId );
		return true;
	}
}
