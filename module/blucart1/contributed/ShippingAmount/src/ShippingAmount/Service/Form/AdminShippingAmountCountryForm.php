<?php

namespace ShippingAmount\Service\Form;

use ShippingAmount\Api\ShippingAmountApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminShippingAmountCountryForm extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'country',
			'amount' 
	);
	protected $_entity = 'ShippingAmount\Entity\ShippingAmountCountry';
	protected $_entityName = 'shippingAmountCountry';
	public function getFormName() {
		return 'adminShippingAmountCountryAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		if ($params ['shippingAmount']) {
			$entity->shippingAmount = ShippingAmountApi::getShippingAmountById ( $params ['shippingAmount'] );
		}
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$form->get ( 'shippingAmount' )->setValue ( Functions::fromRoute ( 'shippingAmountId', 0 ) );
	}
}
