<?php

namespace ShippingAmount\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminShippingEvent extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'title',
			'minAmount',
			'maxAmount',
			'status' 
	);
	protected $_entity = '\ShippingAmount\Entity\ShippingAmount';
	protected $_entityName = 'shippingAmount';
	public function getFormName() {
		return 'adminShippingAmountAdd';
	}
	public function getPriority() {
		return 1000;
	}
	
	
}
