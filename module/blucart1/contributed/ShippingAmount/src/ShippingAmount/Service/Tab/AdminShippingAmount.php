<?php

namespace ShippingAmount\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminShippingAmount extends AbstractTabEvent {
	public function getEventName() {
		return 'adminShippingAmountAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$shippingAmountId = Functions::fromRoute ( 'shippingAmountId', null );
		if (! $shippingAmountId) {
			return;
		}
		$u = $url ( 'admin-shipping-amount-add', array (
				'shippingAmountId' => $shippingAmountId 
		) );
		$tabContainer->add ( 'admin-shipping-amount-add', 'General', $u, 1000 );
		
		$u = $url ( 'admin-shipping-amount-country', array (
				'shippingAmountId' => $shippingAmountId 
		) );
		$tabContainer->add ( 'admin-shipping-amount-country', 'Countries', $u, 1000 );
		
		return $this;
	}
}

