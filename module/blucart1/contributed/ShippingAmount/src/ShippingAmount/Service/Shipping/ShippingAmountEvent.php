<?php

namespace ShippingAmount\Service\Shipping;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Shipping\Option\AbstractShippingEvent;
use Checkout\Option\CartContainer;
use ShippingAmount;
use ShippingAmount\Api\ShippingAmountApi;

class ShippingAmountEvent extends AbstractShippingEvent {
	public function getName() {
		return 'shippingAmount';
	}
	public function getTitle() {
		return 'Shipping By Amount';
	}
	/**
	 *
	 * @return CartContainer
	 */
	public function getCart() {
		$sm = Functions::getServiceLocator ();
		return $sm->get ( 'Cart' );
	}
	public function shippingQuote() {
		$cart = $this->getCart ();
		$shippingContainer = $this->getShippingContainer ();
		$country = $shippingContainer->getCountry ();
		$quotes = ShippingAmountApi::getQuotes ( $cart->getSubTotalPrice (), $country );
		foreach ( $quotes as $key => $value ) {
			$record = ShippingAmountApi::getShippingAmountById ( $key );
			$shippingContainer->add ( $this->getName () . '_' . $record->id, $record->title, $value, array (
					'shippingAmountId' => $record->id 
			) );
		}
		return;
	}
}
