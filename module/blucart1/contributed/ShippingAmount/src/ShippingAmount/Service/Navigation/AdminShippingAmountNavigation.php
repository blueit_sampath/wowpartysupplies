<?php 
namespace ShippingAmount\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminShippingAmountNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'orderMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Shipping By Amount',
							'route' => 'admin-shipping-amount',
							'id' => 'admin-shipping-amount',
							'iconClass' => 'glyphicon glyphicon-road' 
					) 
			) );
		}
	}
}

