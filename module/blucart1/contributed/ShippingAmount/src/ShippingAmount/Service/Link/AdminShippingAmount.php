<?php 
namespace ShippingAmount\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminShippingAmount extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminShippingAmount';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-shipping-amount-add');
		$item = $linkContainer->add ( 'admin-shipping-amount-add', 'Add New Shipping Amount', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

