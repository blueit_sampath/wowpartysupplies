<?php

namespace ShippingAmount\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminShippingAmountCountry extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminShippingAmountCountry';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$shippingAmountId = Functions::fromRoute ( 'shippingAmountId', null );
		if (! $shippingAmountId) {
			return;
		}
		$u = $url ( 'admin-shipping-amount-country-add', array (
				'shippingAmountId' => $shippingAmountId 
		) );
		$item = $linkContainer->add ( 'admin-shipping-amount-country-add', 'Add Country', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

