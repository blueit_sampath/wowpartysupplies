<?php

namespace ShippingAmount\Api;

use Common\Api\Api;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;

class ShippingAmountApi extends Api {
	protected static $_entity = '\ShippingAmount\Entity\ShippingAmount';
	protected static $_entityCountry = '\ShippingAmount\Entity\ShippingAmountCountry';
	public static function getShippingAmountById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getShippingAmountCountryById($id) {
		$em = static::getEntityManager ();
		return $em->find ( static::$_entityCountry, $id );
	}
	public static function getShippingAmountCountryByCountryAndShippingAmountId($country,$shippingAmountId) {
		$em = static::getEntityManager ();
		return $em->getRepository ( static::$_entityCountry )->findOneBy ( array (
				'country' => $country,
				'shippingAmount' => $shippingAmountId
		) );
	}
	public static function getShippingAmounts() {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'shippingAmount.id', 'shippingAmountId' );
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'shippingAmount' );
		return $queryBuilder->executeQuery ();
	}
	
	public static function getShippingAmountsByPrice($amount) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'shippingAmount.id', 'shippingAmountId' );
	
		$whereItem = $queryBuilder->addWhere ( 'shippingAmount.minAmount', 'shippingAmountMinAmount', ' <= ' );
		$whereItem2 = new QueryWhereItem( 'shippingAmount.minAmount', 'shippingAmountMinAmount2', null);
		$whereItem->addWhere( $whereItem2,'shippingAmountMinAmount2');
		$queryBuilder->setParameter ( 'shippingAmountMinAmount', $amount );
		
		$whereItem = $queryBuilder->addWhere ( 'shippingAmount.maxAmount', 'shippingAmountMaxAmount', ' >= ' );
		$whereItem2 = new QueryWhereItem( 'shippingAmount.maxAmount', 'shippingAmountMaxAmount2', null);
		$whereItem->addWhere( $whereItem2, 'shippingAmountMaxAmount2');
		$queryBuilder->setParameter ( 'shippingAmountMaxAmount', $amount );
		
		$whereItem = $queryBuilder->addWhere ( 'shippingAmount.status', 'shippingAmountStatus', ' = ' );
		$queryBuilder->setParameter ( 'shippingAmountStatus', 1 );
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'shippingAmount' );
		
		return $queryBuilder->executeQuery ();
	}
	
	public static function getQuotes($amount, $country = 'all') {
		
		$array = array();
		$results = static::getShippingAmountsByPrice($amount);
		
		foreach($results as $result){
			
			$t = static::getShippingAmountCountryByCountryAndShippingAmountId($country, $result['shippingAmountId']);
			if($t){
				$array[$result['shippingAmountId']] = $t->amount;
			}else{
				$t = static::getShippingAmountCountryByCountryAndShippingAmountId('all', $result['shippingAmountId']);
				if($t){
					$array[$result['shippingAmountId']] = $t->amount;
				}
			}
			
		}
		
		return $array;
	}
	
	
}
