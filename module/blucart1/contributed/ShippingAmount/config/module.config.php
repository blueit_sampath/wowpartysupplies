<?php

namespace ShippingAmount;

return array (
		'router' => array (
				'routes' => array (
						'admin-shipping-amount' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/amount',
										'defaults' => array (
												'controller' => 'ShippingAmount\Controller\AdminShippingAmount',
												'action' => 'index' 
										) 
								) 
						),
						'admin-shipping-amount-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/amount/add[/ajax/:ajax][/:shippingAmountId]',
										'defaults' => array (
												'controller' => 'ShippingAmount\Controller\AdminShippingAmount',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'shippingAmountId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						),
						'admin-shipping-amount-country' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/amount/country/:shippingAmountId[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'ShippingAmount\Controller\AdminShippingAmountCountry',
												'action' => 'index',
												'ajax' => 'true' 
										) 
								) 
						),
						'admin-shipping-amount-country-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/shipping/amount/country/add[/ajax/:ajax][/:shippingAmountId][/:shippingAmountCountryId]',
										'defaults' => array (
												'controller' => 'ShippingAmount\Controller\AdminShippingAmountCountry',
												'action' => 'add',
												'ajax' => 'true' 
										),
										'constraints' => array (
												'shippingAmountId' => '[0-9]+',
												'shippingAmountCountryId' => '[0-9]+',
												'ajax' => 'true|false' 
										) 
								) 
						) 
				) 
		),
		'events' => array (
				'ShippingAmount\Service\Grid\AdminShippingAmount' => 'ShippingAmount\Service\Grid\AdminShippingAmount',
				'ShippingAmount\Service\Link\AdminShippingAmount' => 'ShippingAmount\Service\Link\AdminShippingAmount',
				'ShippingAmount\Service\Tab\AdminShippingAmount' => 'ShippingAmount\Service\Tab\AdminShippingAmount',
				'ShippingAmount\Service\Form\AdminShippingEvent' => 'ShippingAmount\Service\Form\AdminShippingEvent',
				'ShippingAmount\Service\Grid\AdminShippingAmountCountry' => 'ShippingAmount\Service\Grid\AdminShippingAmountCountry',
				'ShippingAmount\Service\Form\AdminShippingAmountCountryForm' => 'ShippingAmount\Service\Form\AdminShippingAmountCountryForm',
				'ShippingAmount\Service\Link\AdminShippingAmountCountry' => 'ShippingAmount\Service\Link\AdminShippingAmountCountry',
				'ShippingAmount\Service\Navigation\AdminShippingAmountNavigation' => 'ShippingAmount\Service\Navigation\AdminShippingAmountNavigation',
				'ShippingAmount\Service\Shipping\ShippingAmountEvent' => 'ShippingAmount\Service\Shipping\ShippingAmountEvent',
		),
		'service_manager' => array (
				'invokables' => array (
						'ShippingAmount\Service\Grid\AdminShippingAmount' => 'ShippingAmount\Service\Grid\AdminShippingAmount',
						'ShippingAmount\Service\Link\AdminShippingAmount' => 'ShippingAmount\Service\Link\AdminShippingAmount',
						'ShippingAmount\Service\Tab\AdminShippingAmount' => 'ShippingAmount\Service\Tab\AdminShippingAmount',
						'ShippingAmount\Form\AdminShippingAmount\ShippingAmountForm' => 'ShippingAmount\Form\AdminShippingAmount\ShippingAmountForm',
						'ShippingAmount\Form\AdminShippingAmount\ShippingAmountFilter' => 'ShippingAmount\Form\AdminShippingAmount\ShippingAmountFilter',
						'ShippingAmount\Service\Form\AdminShippingEvent' => 'ShippingAmount\Service\Form\AdminShippingEvent',
						'ShippingAmount\Service\Grid\AdminShippingAmountCountry' => 'ShippingAmount\Service\Grid\AdminShippingAmountCountry',
						'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryForm' => 'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryForm',
						'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryFilter' => 'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryFilter',
						'ShippingAmount\Service\Form\AdminShippingAmountCountryForm' => 'ShippingAmount\Service\Form\AdminShippingAmountCountryForm',
						'ShippingAmount\Service\Link\AdminShippingAmountCountry' => 'ShippingAmount\Service\Link\AdminShippingAmountCountry',
						'ShippingAmount\Service\Navigation\AdminShippingAmountNavigation' => 'ShippingAmount\Service\Navigation\AdminShippingAmountNavigation',
						'ShippingAmount\Service\Shipping\ShippingAmountEvent' => 'ShippingAmount\Service\Shipping\ShippingAmountEvent'
				),
				'factories' => array (
						'ShippingAmount\Form\AdminShippingAmount\ShippingAmountFactory' => 'ShippingAmount\Form\AdminShippingAmount\ShippingAmountFactory',
						'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryFactory' => 'ShippingAmount\Form\AdminShippingAmountCountry\ShippingCountryFactory' 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'ShippingAmount\Controller\AdminShippingAmount' => 'ShippingAmount\Controller\AdminShippingAmountController',
						'ShippingAmount\Controller\AdminShippingAmountCountry' => 'ShippingAmount\Controller\AdminShippingAmountCountryController' 
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						__NAMESPACE__ => __DIR__ . '/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						__NAMESPACE__ . '_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								__NAMESPACE__ => __DIR__ . '/../public' 
						) 
				) 
		) 
);
