<?php

return array(
    'events' => array(
        'ExportProduct\Service\Export\Product' => 'ExportProduct\Service\Export\Product',
        'ExportProduct\Service\Navigation\ExportProductNavigation' => 'ExportProduct\Service\Navigation\ExportProductNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'ExportProduct\Service\Export\Product' => 'ExportProduct\Service\Export\Product',
            'ExportProduct\Service\Navigation\ExportProductNavigation' => 'ExportProduct\Service\Navigation\ExportProductNavigation'
        ),
        'factories' => array()
    ),
    'controllers' => array('invokables' => array()),
    'view_manager' => array('template_path_stack' => array('ExportProduct' => __DIR__ . '/../view')),
    'asset_manager' => array('resolver_configs' => array('paths' => array('ExportProduct' => __DIR__ . '/../public')))
);
