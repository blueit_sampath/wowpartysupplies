<?php

namespace ExportProduct\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ExportProductNavigation extends \Admin\Navigation\Event\AdminNavigationEvent {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMainExportMain');
       
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Export Products',
                    'route' => 'admin-export',
                    'params' => array(
                        'eventName' => 'adminProductExport' 
                    ),
                    'id' => 'admin-export-product',
                    'iconClass' => 'glyphicon-briefcase'
                )
            ));
        }
    }

}
