<?php

namespace ExportProduct\Service\Export;

use Export\Option\AbstractExportEvent;
use Product\Api\ProductApi;
use Core\Functions;

class Product extends AbstractExportEvent {

    public function getEventName() {
        return 'adminProductExport';
    }

    public function getPriority() {
        return 1000;
    }

    public function loadSchema() {
        $exportContainer = $this->getExportContainer();
        $exportContainer->setTitle('Export Product');
        $exportContainer->addColumn('title', 'Title', 1000);

        $exportContainer->addColumn('sku', 'SKU', 990);

        $exportContainer->addColumn('shortDescription', 'Short Description', 980);

        $exportContainer->addColumn('description', 'Description', 970);

        $exportContainer->addColumn('sellPrice', 'Sell Price', 960);

        $exportContainer->addColumn('costPrice', 'Cost Price', 950);

        $exportContainer->addColumn('listPrice', 'List Price', 940);

        $exportContainer->addColumn('weight', 'Priority', 930);

        $exportContainer->addColumn('status', 'Status', 920);

        $exportContainer->addColumn('images', 'Images', 910);


        if (Functions::hasModule('ProductCategory')) {

            $exportContainer->addColumn('categories', 'Categories', 890);
        }
        if (Functions::hasModule('Inventory')) {

            $exportContainer->addColumn('stockStatus', 'Stock Status', 870);

            $exportContainer->addColumn('stock', 'Stock', 860);
        }

        if (Functions::hasModule('Vat')) {
            $exportContainer->addColumn('isVat', 'Is Vat?', 860);
        }
    }

    public function fetch() {

        
        $products = ProductApi::getAllProducts(null);

        $exportContainer = $this->getExportContainer();
        $columns = array(
            'sku',
            'title',
            'shortDescription',
            'description',
            'listPrice',
            'costPrice',
            'sellPrice',
            'weight',
            'status'
        );
        $selectColumns = $exportContainer->getSelectedColumns();

      
        foreach ($products as $product) {
            $temp = array();
            foreach ($columns as $column) {
                if (in_array($column, $selectColumns)) {
                    $temp[$column] = $product->$column;
                }
            }
            if (in_array('images', $selectColumns)) {
                $temp['images'] = $this->getProductImages($product->id);
            }
            if (in_array('categories', $selectColumns)) {
                $temp['categories'] = $this->getProductCategories($product->id);
            }

            if (in_array('isVat', $selectColumns)) {
                $temp['isVat'] = $this->getProductVat($product->id);
            }

            if (in_array('stock', $selectColumns)) {
                $temp['stock'] = $this->getStock($product->id);
            }

            if (in_array('stockStatus', $selectColumns)) {
                $temp['stockStatus'] = $this->getStockStatus($product->id);
            }
           
        
           
            $exportContainer->addDataItem($product->id, $temp);
        }
    }

    public function getProductImages($productId) {
        $productImages = ProductApi::getProductImageByProductId($productId, null);
        $temp = array();
        foreach ($productImages as $productImageArray) {
            $productImage = ProductApi::getProductImageById($productImageArray['productImageId']);
            if ($productImage->file->path) {
                $temp[] = \File\Api\FileApi::getImageUrl($productImage->file->path, true);
            }
        }
        return $temp;
    }

    public function getProductCategories($productId) {
        $productCategories = \ProductCategory\Api\ProductCategoryApi::getProductCategoryByProductId($productId);
        $temp = array();
        foreach ($productCategories as $productCategoryArray) {
            $productCategory = \ProductCategory\Api\ProductCategoryApi::getProductCatetoryById($productCategoryArray['productCategoryId']);
            $temp[] = $productCategory->name;
        }
        return $temp;
    }

    public function getStock($productId) {

        $stockEntity = \Inventory\Api\InventoryApi::getStockByProductId($productId);
        if ($stockEntity) {
            return $stockEntity->qty;
        }
        return null;
    }

    public function getStockStatus($productId) {

        $stockEntity = \Inventory\Api\InventoryApi::getStockByProductId($productId);
        if ($stockEntity) {
            return $stockEntity->status;
        }
        return null;
    }

    public function getProductVat($productId) {
        $entity = \Vat\Api\VatApi::getVatByProductId($productId);


        if ($entity) {
            return $entity->status;
        }
        return null;
    }

}
