<?php

namespace Newsletter\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminNewsletterController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletter\NewsletterFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-newsletter-add');
	}
}