<?php

namespace Newsletter\Controller;

use Common\MVC\Controller\AbstractFrontController;

class NewsletterController extends AbstractFrontController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Newsletter\Form\NewsletterForm\NewsletterFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'newsletter' );
	}
	public function indexAction() {
		return $this->addAction ();
	}
}