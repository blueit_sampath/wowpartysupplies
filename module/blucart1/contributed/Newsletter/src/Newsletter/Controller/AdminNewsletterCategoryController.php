<?php

namespace Newsletter\Controller;

use Common\MVC\Controller\AbstractAdminController;



class AdminNewsletterCategoryController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-newsletter-category-add', array (
				'newsletterCategoryId' => $form->get ( 'id' )->getValue () 
		) );
	}
}