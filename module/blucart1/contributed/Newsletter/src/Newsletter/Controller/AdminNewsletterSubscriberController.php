<?php

namespace Newsletter\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminNewsletterSubscriberController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-newsletter-subscriber-add', array (
				'newsletterCategoryId' => $form->get ( 'newsletterCategory' )->getValue (),
				'newsletterSubscriberId' => $form->get ( 'id' )->getValue () 
		) );
	}
}