<?php

namespace Newsletter\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Newsletter
 *
 * @ORM\Table(name="newsletter")
 * @ORM\Entity
 */
class Newsletter {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="body", type="text", nullable=true)
	 */
	private $body;
	
	/**
	 *
	 * @var string @ORM\Column(name="subject", type="string", length=1024,
	 *      nullable=true)
	 */
	private $subject;
	
	/**
	 *
	 * @var string @ORM\Column(name="body_plain", type="text", nullable=true)
	 */
	private $bodyPlain;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	
	/**
	 *
	 * @var \NewsletterCategory
	 *      @ORM\ManyToOne(targetEntity="NewsletterCategory")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="newsletter_category_id",
	 *      referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $newsletterCategory;
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $property        	
	 * @return mixed
	 *
	 */
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
}
