<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * NewsletterUnsubscriber
 *
 * @ORM\Table(name="newsletter_unsubscriber")
 * @ORM\Entity
 */
class NewsletterUnsubscriber
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="string", length=255, nullable=true)
     */
    private $secret;

    /**
     * @var string
     *
     * @ORM\Column(name="created_date", type="string", length=255, nullable=true)
     */
    private $createdDate;

    /**
     * @var string
     *
     * @ORM\Column(name="updated_date", type="string", length=255, nullable=true)
     */
    private $updatedDate;

    /**
     * @var \NewsletterSubscriber
     *
     * @ORM\ManyToOne(targetEntity="NewsletterSubscriber")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="newsletter_subscriber_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $newsletterSubscriber;


}
