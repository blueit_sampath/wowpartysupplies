<?php

namespace Newsletter\Api;

use Core\Functions;
use Common\Api\Api;

class NewsletterApi extends Api {
	protected static $_entity = '\Newsletter\Entity\Newsletter';
	protected static $_entityCategory = '\Newsletter\Entity\NewsletterCategory';
	protected static $_entitySubscriber = '\Newsletter\Entity\NewsletterSubscriber';
	public static function getNewsletterById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getNewsletterCategoryById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entityCategory, $id );
	}
	public static function getNewsletterCategories($status = true) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'newsletterCategory.id', 'newsletterCategoryId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'newsletterCategory.status', 'newsletterCategoryStatus' );
			$queryBuilder->addParameter ( 'newsletterCategoryStatus', $status );
		}
		$queryBuilder->addOrder ( 'newsletterCategory.weight', 'newsletterCategoryWeight', 'desc' );
		$item = $queryBuilder->addFrom ( static::$_entityCategory, 'newsletterCategory' );
		return $queryBuilder->executeQuery ();
	}
	public static function getNewsletterCategorySortedArray() {
		$array = static::getNewsletterCategories ( null );
		return static::makeNewletterCategoryArray ( $array );
	}
	public static function makeNewletterCategoryArray($array) {
		$result = array ();
		foreach ( $array as $results ) {
			$newsletterCategoryId = $results ['newsletterCategoryId'];
			$newsletterCategory = static::getNewsletterCategoryById ( $newsletterCategoryId );
			$result [$newsletterCategory->id] = array (
					'name' => $newsletterCategory->name 
			);
		}
		return $result;
	}
	public static function getNewsletterSubscriberById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entitySubscriber, $id );
	}
	public static function getNewsletterSubscribers($newsletterCategoryId) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'newsletterSubscriber.id', 'newsletterSubscriberId' );
		
		$queryBuilder->addWhere ( 'newsletterSubscriber.newsletterCategory', 'newsletterCategoryId' );
		$queryBuilder->addParameter ( 'newsletterCategoryId', $newsletterCategoryId );
		
		$item = $queryBuilder->addFrom ( static::$_entitySubscriber, 'newsletterSubscriber' );
		
		return $queryBuilder->executeQuery ();
	}
	public static function getNewsletterSubscriberByEmailAndNewsletterCategoryId($email, $newsletterCategoryId) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entitySubscriber )->findOneBy ( array (
				'email' => trim ( $email ),
				'newsletterCategory' => $newsletterCategoryId 
		) );
	}
	public static function removeNewsletterSubscriberByEmailAndNewsletterCategoryId($email, $newsletterCategoryId) {
		$em = Functions::getEntityManager ();
		$result = static::getNewsletterSubscriberByEmailAndNewsletterCategoryId ( $email, $newsletterCategoryId );
		$em->remove ( $result );
		$em->flush ();
		return;
	}
	public static function saveNewsletterSubscriberByEmailAndNewsletterCategoryId($email, $newsletterCategoryId, $firstName = "", $lastName = "") {
		$em = Functions::getEntityManager ();
		
		if (! $entity = static::getNewsletterSubscriberByEmailAndNewsletterCategoryId ( $email, $newsletterCategoryId )) {
			$entity = new static::$_entitySubscriber ();
			$entity->email = $email;
			$entity->newsletterCategory = static::getNewsletterCategoryById ( $newsletterCategoryId );
			$entity->firstName = $firstName;
			$entity->lastName = $lastName;
			$em->persist ( $entity );
			$em->flush ();
		} else {
			$entity->firstName = $firstName;
			$entity->lastName = $lastName;
			
			$em->flush ();
		}
		
		return $entity;
	}
	public static function getNewsletterSubscriberByEmail($email) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entitySubscriber )->findBy ( array (
				'email' => trim ( $email ) 
		) );
	}
}
