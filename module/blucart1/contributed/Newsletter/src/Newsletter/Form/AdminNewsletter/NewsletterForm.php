<?php

namespace Newsletter\Form\AdminNewsletter;

use Newsletter\Entity\NewsletterCategory;
use Newsletter\Api\NewsletterApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class NewsletterForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'subject',
				'attributes' => array (
						'type' => 'text' 
				)
				,
				'options' => array (
						'label' => 'Subject' 
				) 
		), array (
				'priority' => 1000 
		) );
		$this->add ( array (
				'name' => 'body',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'Message' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'bodyPlain',
				'attributes' => array (
						'type' => 'textarea' 
				),
				'options' => array (
						'label' => 'Plain Message' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$select = new Select ( 'newsletterCategory' );
		$select->setLabel ( 'Newsletter Category' );
		$select->setValueOptions ( $this->getNewsletterCategories () );
		$this->add ( $select, array (
				'priority' => 970 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getNewsletterCategories() {
		$newsletterCategories = NewsletterApi::getNewsletterCategories ( null );
		$array = array (
				'' => 'Please select' 
		);
		foreach ( $newsletterCategories as $newsletterCategoryId ) {
			$newsletterCategory = NewsletterApi::getNewsletterCategoryById ( $newsletterCategoryId ['newsletterCategoryId'] );
			$array [$newsletterCategory->id] = $newsletterCategory->name;
		}
		return $array;
	}
}