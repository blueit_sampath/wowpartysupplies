<?php
namespace Newsletter\Form\AdminNewsletter;

use Common\Form\Option\AbstractFormFactory;

class NewsletterFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletter\NewsletterForm' );
		$form->setName ( 'adminNewsletterAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletter\NewsletterFilter' );
	}
}
