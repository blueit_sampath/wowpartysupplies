<?php

namespace Newsletter\Form\AdminNewsletter;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class NewsletterFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'subject',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'body',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'newsletterCategory',
				'required' => true 
		) );
		$this->add ( array (
				'name' => 'bodyPlain',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
	}
} 