<?php

namespace Newsletter\Form\AdminNewsletterCategory;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class NewsletterCategoryFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'name',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'fromName',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'fromEmail',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						),
						array (
								'name' => 'EmailAddress' 
						)
						 
				) 
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'status',
				'required' => true,
				'validators' => array (
						array (
								'name' => 'NotEmpty' 
						),
						array (
								'name' => 'Int' 
						) 
				) 
		) );
	}
} 