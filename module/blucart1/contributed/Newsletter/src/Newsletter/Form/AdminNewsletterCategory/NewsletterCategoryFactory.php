<?php
namespace Newsletter\Form\AdminNewsletterCategory;

use Common\Form\Option\AbstractFormFactory;

class NewsletterCategoryFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryForm' );
		$form->setName ( 'adminNewsletterCategoryAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryFilter' );
	}
}
