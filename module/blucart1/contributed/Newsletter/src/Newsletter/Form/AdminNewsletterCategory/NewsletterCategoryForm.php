<?php
namespace Newsletter\Form\AdminNewsletterCategory;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class NewsletterCategoryForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text',
				),
				'options' => array (
						'label' => 'Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'fromEmail',
				'attributes' => array (
						'type' => 'text',
				),
				'options' => array (
						'label' => 'From Email'
				)
		), array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'fromName',
				'attributes' => array (
						'type' => 'text',
				),
				'options' => array (
						'label' => 'From Name'
				)
		), array (
				'priority' => 980
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'attributes' => array (
						'type' => 'text',
						'title' => 'Please enter priority' 
				),
				'options' => array (
						'label' => 'Priority' 
				) 
		), array (
				'priority' => 970 
		) );
		
		
		
		$this->getStatus ( 960 );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}