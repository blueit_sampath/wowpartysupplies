<?php
namespace Newsletter\Form\NewsletterForm;

use Common\Form\Option\AbstractFormFactory;

class NewsletterFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Newsletter\Form\NewsletterForm\NewsletterForm' );
		$form->setName ( 'newsletterForm' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Newsletter\Form\NewsletterForm\NewsletterFilter' );
	}
}
