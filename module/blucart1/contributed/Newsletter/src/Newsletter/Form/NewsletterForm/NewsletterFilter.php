<?php

namespace Newsletter\Form\NewsletterForm;

use Zend\InputFilter\InputFilter;
use Zend\Validator\Regex;


class NewsletterFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'firstName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => "/^[a-zA-Z ,.'-]+$/",
										'messages' => array (
												Regex::NOT_MATCH => 'Use Letters & periods' 
										) 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => "/^[a-zA-Z ,.'-]+$/",
										'messages' => array (
												Regex::NOT_MATCH => 'Use Letters & periods' 
										) 
								) 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'email',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'EmailAddress' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'newsletter',
				'required' => true 
		) );
	}
} 