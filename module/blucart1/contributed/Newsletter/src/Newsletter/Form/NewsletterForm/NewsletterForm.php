<?php

namespace Newsletter\Form\NewsletterForm;

use Common\Form\Form;
use Zend\Form\Element\MultiCheckbox;
use Newsletter\Api\NewsletterApi;

class NewsletterForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'firstName',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'First Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		$this->add ( array (
				'name' => 'lastName',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Last Name' 
				) 
		), array (
				'priority' => 990 
		) );
		$this->add ( array (
				'name' => 'email',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Email' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$array = array ();
		$results = NewsletterApi::getNewsletterCategories ();
		foreach ( $results as $result ) {
			$newsletterCategory = NewsletterApi::getNewsletterCategoryById ( $result ['newsletterCategoryId'] );
			$array [$newsletterCategory->id] = $newsletterCategory->name;
		}
		if (count ( $array )) {
			if (count ( $array ) == 1) {
				$this->add ( array (
						'name' => 'newsletter',
						'attributes' => array (
								'type' => 'hidden',
								//'value' => array_shift ( array_keys ( $array ) ) 
						) 
				), array (
						'priority' => 970 
				) );
			} else {
				$multiCheckbox = new MultiCheckbox ( 'newsletter' );
				$multiCheckbox->setLabel ( 'Newsletter Subscription' );
				$multiCheckbox->setValueOptions ( $array );
				$this->add ( $multiCheckbox, array (
						'priority' => 970 
				) );
			}
		}
		$this->addCaptcha ();
		$this->addCsrf ();
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}