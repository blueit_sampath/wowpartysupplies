<?php
namespace Newsletter\Form\AdminNewsletterSubscriber;

use Common\Form\Option\AbstractFormFactory;

class NewsletterSubscriberFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberForm' );
		$form->setName ( 'adminNewsletterSubscriberAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberFilter' );
	}
}
