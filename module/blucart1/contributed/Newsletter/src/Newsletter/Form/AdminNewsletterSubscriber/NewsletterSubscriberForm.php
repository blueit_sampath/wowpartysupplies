<?php

namespace Newsletter\Form\AdminNewsletterSubscriber;

use Common\Functions;
use Common\Form\Form;

class NewsletterSubscriberForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		$this->add ( array (
				'name' => 'newsletterCategory',
				'attributes' => array (
						'type' => 'hidden'
				)
		) );
		
		$this->add ( array (
				'name' => 'firstName',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'First Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Last Name' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'email',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Email' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}