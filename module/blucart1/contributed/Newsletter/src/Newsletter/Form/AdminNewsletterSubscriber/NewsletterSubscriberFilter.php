<?php

namespace Newsletter\Form\AdminNewsletterSubscriber;

use Newsletter\Api\NewsletterApi;

use Zend\Validator\Callback;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class NewsletterSubscriberFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'firstName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						)
						
				)
				 
		) );
		$this->add ( array (
				'name' => 'email',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						),
						array (
								'name' => 'EmailAddress' 
						),
						array (
								'name' => 'Callback',
								'options' => array (
										'callback' => array (
												$this,
												'checkSubscription'
										),
										'messages' => array (
												Callback::INVALID_VALUE => "Email already subscribed",
													
										)
								)
						)
						 
				) 
		) );
	}
	public function checkSubscription($value,$context){
		$entity = NewsletterApi::getNewsletterSubscriberByEmailAndNewsletterCategoryId($value, $context['newsletterCategory']);
		if($entity){
			if(!$context['id']){
				return false;
			}
			if($context['id'] == $entity->id){
				return true;
			}
			return false;
			
		}
		return true;
	}
} 