<?php

namespace Newsletter\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminNewsletterSubscriber extends AbstractTabEvent {
	public function getEventName() {
		return 'adminNewsletterCategoryAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$newsletterCategoryId = Functions::fromRoute ( 'newsletterCategoryId' );
		if (! $newsletterCategoryId) {
			return false;
		}
		
		$u = $url ( 'admin-newsletter-subscriber', array (
				'newsletterCategoryId' => $newsletterCategoryId 
		) );
		$tabContainer->add ( 'admin-newsletter-subscriber', 'Subscribers', $u, 900 );
		
		return $this;
	}
}

