<?php

namespace Newsletter\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminNewsletterCategory extends AbstractTabEvent {
	public function getEventName() {
		return 'adminNewsletterCategoryAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$newsletterCategoryId = Functions::fromRoute ( 'newsletterCategoryId' );
		if (! $newsletterCategoryId) {
			return false;
		}
		
		$u = $url ( 'admin-newsletter-category-add', array (
				'newsletterCategoryId' => $newsletterCategoryId 
		) );
		$tabContainer->add ( 'admin-newsletter-category-add', 'General Information', $u, 1000 );
		
		return $this;
	}
}

