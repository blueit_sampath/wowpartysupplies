<?php 
namespace Newsletter\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminUserViewNewsletter extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-user-view-newsletter';

	public function getBlockName() {
		return 'AdminUserViewNewsletter';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminUserViewNewsletter';
	}
	
}

