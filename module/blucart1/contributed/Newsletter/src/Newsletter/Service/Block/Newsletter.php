<?php 
namespace Newsletter\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class Newsletter extends AbstractBlockEvent {

	protected $_blockTemplate = 'newsletter-block';

	public function getBlockName() {
		return 'newsletterBlock';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'Newsletter Block';
	}
	
}

