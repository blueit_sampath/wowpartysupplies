<?php

namespace Newsletter\Service\Form;

use Newsletter\Api\NewsletterApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminNewsletterSubscriber extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'email',
			'firstName',
			'lastName' 
	);
	protected $_entity = '\Newsletter\Entity\NewsletterSubscriber';
	protected $_entityName = 'newsletterSubscriber';
	
	public function getFormName() {
		return 'adminNewsletterSubscriberAdd';
	}
	public function getPriority() {
		return 1000;
	}
	
	public function parseSaveEntity($params, $entity){
		if (isset ( $params ['newsletterCategory'] )) {
			$entity->newsletterCategory = NewsletterApi::getNewsletterCategoryById ( $params ['newsletterCategory'] );
		}
	}
	
	public function beforeGetRecord(){
		$form = $this->getForm ();
		$newsletterCategoryId = Functions::fromRoute ( 'newsletterCategoryId' );
		if ($newsletterCategoryId) {
			$form->get ( 'newsletterCategory' )->setValue ( $newsletterCategoryId );
		}
	}
	
}
