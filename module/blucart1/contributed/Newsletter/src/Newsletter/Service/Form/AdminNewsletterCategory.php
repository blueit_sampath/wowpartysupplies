<?php

namespace Newsletter\Service\Form;


use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminNewsletterCategory extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight',
			'status',
			'name',
			'fromEmail',
			'fromName' 
	);
	protected $_entity = '\Newsletter\Entity\NewsletterCategory';
	protected $_entityName = 'newsletterCategory';
	public function getFormName() {
		return 'adminNewsletterCategoryAdd';
	}
	public function getPriority() {
		return 1000;
	}
	
}
