<?php

namespace Newsletter\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use Newsletter\Api\NewsletterApi;

class NewsletterForm extends AbstractMainFormEvent {
	public function getFormName() {
		return 'newsletterForm';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		$email = $params ['email'];
		$firstName = $params ['firstName'];
		$lastName = $params ['lastName'];
		if (isset ( $params ['newsletter'] )) {
			
			if (is_array ( $params ['newsletter'] )) {
				foreach ( $params ['newsletter'] as $newsletterId ) {
					
					NewsletterApi::saveNewsletterSubscriberByEmailAndNewsletterCategoryId ( $email, $newsletterId, $firstName, $lastName );
				}
			} else {
				NewsletterApi::saveNewsletterSubscriberByEmailAndNewsletterCategoryId ( $email, $params ['newsletter'], $firstName, $lastName );
			}
		}
		
		return true;
	}
}
