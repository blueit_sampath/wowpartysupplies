<?php

namespace Newsletter\Service\Form;

use Common\Form\Option\AbstractFrontMainFormEvent;
use Zend\Form\Element\MultiCheckbox;
use Newsletter\Api\NewsletterApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;

class UserEditProfileNewsletter extends AbstractFrontMainFormEvent {
	protected $_columnKeys = array ();
	protected $_entity = '\Newsletter\Entity\NewsletterCategory';
	protected $_entityName = 'newsletterCategory';
	public function getFormName() {
		return 'userEditProfile';
	}
	public function getPriority() {
		return 500;
	}
	public function preInitEvent() {
		parent::preInitEvent ();
		$form = $this->getForm ();
		$inputFilter = $form->getInputFilter ();
		$array = array ();
		
		$results = NewsletterApi::getNewsletterCategories ();
		foreach ( $results as $result ) {
			$newsletterCategory = NewsletterApi::getNewsletterCategoryById ( $result ['newsletterCategoryId'] );
			$array [$newsletterCategory->id] = $newsletterCategory->name;
		}
		if (count ( $array )) {

			
			$multiCheckbox = new MultiCheckbox ( 'newsletter' );
			$multiCheckbox->setLabel ( 'Newsletter Subscription' );
			$multiCheckbox->setValueOptions ( $array );
			$form->add ( $multiCheckbox, array (
					'priority' => 200 
			) );
	
			
			$inputFilter->add ( array (
					'name' => 'newsletter',
					'required' => false,
			) );
			
	
			
		}
	}
	public function getRecord() {
		$form = $this->getForm ();
		$user = UserApi::getLoggedInUser ();
		if (! $user) {
			return false;
		}
		$results = NewsletterApi::getNewsletterSubscriberByEmail ( $user->email );
		$array = array ();
		foreach ( $results as $result ) {
			$array [] = $result->newsletterCategory->id;
		}
		if (count ( $array )) {
			$form->get ( 'newsletter' )->setValue ( $array );
		}
	}
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		$user = $resultContainer->get ( 'user' )->getValue ();
		
		$results = NewsletterApi::getNewsletterSubscriberByEmail ( $user->email );
		$array = array ();
		foreach ( $results as $result ) {
			$array [$result->newsletterCategory->id] = $result->newsletterCategory->id;
		}
		
		if (isset ( $params ['newsletter'] ) && $params ['newsletter']) {
			
			foreach ( $params ['newsletter'] as $newsletterId ) {
				
				NewsletterApi::saveNewsletterSubscriberByEmailAndNewsletterCategoryId ( $user->email, $newsletterId, $user->firstName, $user->lastName );
				unset ( $array [$newsletterId] );
			}
		}
		
		foreach ( $array as $key => $value ) {
			NewsletterApi::removeNewsletterSubscriberByEmailAndNewsletterCategoryId ( $user->email, $value );
		}
		
		return true;
	}
}
