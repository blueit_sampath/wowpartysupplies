<?php

namespace Newsletter\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use Newsletter\Api\NewsletterApi;
use Category\Api\CategoryApi;
use Core\Functions;

class AdminNewsletter extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'body',
			'bodyPlain',
			'subject' 
	);
	protected $_entity = '\Newsletter\Entity\Newsletter';
	protected $_entityName = 'newsletter';
	public function getFormName() {
		return 'adminNewsletterAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveParams($params){
		$params['id'] = '';
		return $params;
	}
	public function parseSaveEntity($params, $entity) {
		$entity->id = null;
		if (isset ( $params ['newsletterCategory'] )) {
			$entity->newsletterCategory = NewsletterApi::getNewsletterCategoryById ( $params ['newsletterCategory'] );
		}
	}
	public function afterSaveEntity($entity) {
		$this->sendNewsletter ( $entity );
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		if ($entity->newsletterCategory) {
			$form->get ( 'newsletterCategory' )->setValue ( $entity->newsletterCategory->id );
		}
	}
	public function sendNewsletter($entity) {
		$subscribers = NewsletterApi::getNewsletterSubscribers ( $entity->newsletterCategory->id );
		
		if ($subscribers) {
			$mail = Functions::getMail ();
			$mail->addFrom ( $entity->newsletterCategory->fromEmail, $entity->newsletterCategory->fromName );
			$mail->setSubject ( $entity->subject );
			$mail->setHtmlMessage ( $entity->body );
			$mail->setTextMessage ( $entity->bodyPlain );
			foreach ( $subscribers as $subscriber ) {
				$newsletterSubscriber = NewsletterApi::getNewsletterSubscriberById ( $subscriber ['newsletterSubscriberId'] );
				$mail->addTo ( $newsletterSubscriber->email, $newsletterSubscriber->firstName . ' ' . $newsletterSubscriber->lastName );
			}
			$mail->formMessage ();
			$mail->sendMail ();
			
			Functions::addSuccessMessage ( 'Mails sent successfully!' );
		}
		return true;
	}
}
