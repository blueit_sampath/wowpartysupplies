<?php

namespace Newsletter\Service\Form;

use Common\Form\Option\AbstractFrontMainFormEvent;
use Zend\Form\Element\MultiCheckbox;
use Newsletter\Api\NewsletterApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class RegisterNewsletter extends AbstractFrontMainFormEvent {

    protected $_columnKeys = array();
    protected $_entity = '\Newsletter\Entity\NewsletterCategory';
    protected $_entityName = 'newsletterCategory';

    public function getFormName() {
        return 'userRegister';
    }

    public function getPriority() {
        return 500;
    }

    public function preInitEvent() {
        parent::preInitEvent();
        $form = $this->getForm();

        $array = array();

        $results = NewsletterApi::getNewsletterCategories();
        foreach ($results as $result) {
            $newsletterCategory = NewsletterApi::getNewsletterCategoryById($result ['newsletterCategoryId']);
            $array [$newsletterCategory->id] = $newsletterCategory->name;
        }
        if (count($array)) {
            $multiCheckbox = new MultiCheckbox('newsletter');
            $multiCheckbox->setLabel('Newsletter Subscription');
            $multiCheckbox->setValueOptions($array);
            $form->add($multiCheckbox, array(
                'priority' => 200
            ));

            $inputFilter = $form->getInputFilter();
            $inputFilter->add(array(
                'name' => 'newsletter',
                'required' => false
            ));
        }
    }

    public function save() {
        $resultContainer = $this->getFormResultContainer();
        $form = $this->getForm();
        $params = $form->getData();
        $em = $this->getEntityManager();

        $user = $resultContainer->get('user')->getValue();

        // if (isset($params ['newsletter']) || $params ['newsletter'] === null) {
        if (isset($params ['newsletter'])) {
            if (is_array($params ['newsletter'])) {
                foreach ($params ['newsletter'] as $newsletterId) {

                    NewsletterApi::saveNewsletterSubscriberByEmailAndNewsletterCategoryId($user->email, $newsletterId, $user->firstName, $user->lastName);
                }
            }
        }

        return true;
    }

}
