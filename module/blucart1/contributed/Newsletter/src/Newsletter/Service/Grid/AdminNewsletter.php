<?php

namespace Newsletter\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;

use Kendo\Lib\Grid\Option\Toolbar\Toolbar;

use BlucartGrid\Option\ColumnItem;

use BlucartGrid\Event\AbstractMainBlucartGridEvent;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;


class AdminNewsletter extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'subject',
			'createdDate' 
	)
	;
	protected $_entity = '\Newsletter\Entity\Newsletter';
	protected $_entityName = 'newsletter';
	public function getEventName() {
		return 'adminNewsletter';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary(true);
		$columns->addColumn ( 'newsletter.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'subject' );
		$columnItem->setTitle ( 'Subject' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'newsletter.subject', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'categoryName' );
		$columnItem->setTitle ( 'Category' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'newsletterCategory.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Created Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columnItem->setFormat ( '{0:g}' );
		$columns->addColumn ( 'newsletter.createdDate', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$fromItem = $queryBuilder->addFrom ( $this->_entity, $this->_entityName);
		$joinItem = new QueryJoinItem ( 'newsletter.newsletterCategory', 'newsletterCategory' );
		$fromItem->addJoin ( $joinItem );
		return true;
	}
	
}
