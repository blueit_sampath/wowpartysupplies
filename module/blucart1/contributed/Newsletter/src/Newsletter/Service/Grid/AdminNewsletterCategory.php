<?php

namespace Newsletter\Service\Grid;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;


use BlucartGrid\Event\AbstractMainBlucartGridEvent;

use BlucartGrid\Option\ColumnItem;
use Kendo\Lib\Option\Aggregate\Aggregates;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;

class AdminNewsletterCategory extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id','name','fromEmail','fromName','status','priority' 
	)
	;
	protected $_entity = '\Newsletter\Entity\NewsletterCategory';
	protected $_entityName = 'newsletterCategory';
	
	public function getEventName() {
		return 'adminNewsletterCategory';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setIsPrimary(true);
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'newsletterCategory.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'newsletterCategory.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'fromEmail' );
		$columnItem->setTitle ( 'From Email' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'newsletterCategory.fromEmail', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'fromName' );
		$columnItem->setTitle ( 'From Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'newsletterCategory.fromName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'subscribers' );
		$columnItem->setTitle ( 'Subscribers' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 960 );
		$columnItem->setIsHaving(true);
		$columns->addColumn ( 'count(newsletterSubscriber.email)', $columnItem );
		
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'newsletterCategory.status', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Priority' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 940 );
		$columns->addColumn ( 'newsletterCategory.weight', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function postSchema($e) {
		parent::postSchema ( $e );
		$grid = $this->getGrid ();
		$aggregate = $grid->getDataSource ()->getAggregate ();
		if (! $aggregate) {
			$aggregate = new Aggregates ();
			$grid->getDataSource ()->setAggregate ( $aggregate );
		}
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$fromItem = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem( '\Newsletter\Entity\NewsletterSubscriber', 'newsletterSubscriber', 'left join', 'newsletterCategory.id = newsletterSubscriber.newsletterCategory' );
		$fromItem->addJoin($joinItem);
		$queryBuilder->addGroup ( 'newsletterCategory.id', 'newsletterCategoryId' );
		return true;
	}
	
}
