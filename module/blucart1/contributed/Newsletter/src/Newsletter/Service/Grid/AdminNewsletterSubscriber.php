<?php

namespace Newsletter\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;

use Kendo\Lib\Grid\Option\Toolbar\Toolbar;

use BlucartGrid\Option\ColumnItem;

use Core\Functions;

use BlucartGrid\Event\AbstractMainBlucartGridEvent;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;


class AdminNewsletterSubscriber extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'email',
			'firstName',
			'lastName' 
	);
	protected $_entity = '\Newsletter\Entity\NewsletterSubscriber';
	protected $_entityName = 'newsletterSubscriber';
	public function getEventName() {
		return 'adminNewsletterSubscriber';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$newsletterCategoryId = Functions::fromRoute ( 'newsletterCategoryId', 0 );
		
		$grid = $this->getGrid ();
		$grid->setAdditionalParameters ( array (
				'newsletterCategoryId' => $newsletterCategoryId 
		) );
		
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary(true);
		$columns->addColumn ( 'newsletterSubscriber.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'newsletterCategoryId' );
		$columnItem->setTitle ( 'newsletterCategoryId' );
		$columnItem->setType ( 'number' );
		$columnItem->setHidden ( true );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( - 1000 );
		$columns->addColumn ( 'newsletterCategory.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'email' );
		$columnItem->setTitle ( 'Email' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'newsletterSubscriber.email', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'firstName' );
		$columnItem->setTitle ( 'First Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'newsletterSubscriber.firstName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'lastName' );
		$columnItem->setTitle ( 'Last Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'newsletterSubscriber.lastName', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$newsletterCategoryId = Functions::fromQuery ( 'newsletterCategoryId', 0 );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$fromItem = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem ( 'newsletterSubscriber.newsletterCategory', 'newsletterCategory' );
		$fromItem->addJoin ( $joinItem );
		$item = $queryBuilder->addWhere ( 'newsletterCategory.id', 'newsletterCategoryId' );
		$queryBuilder->addParameter ( 'newsletterCategoryId', $newsletterCategoryId );
		return true;
	}
	
}
