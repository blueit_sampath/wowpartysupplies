<?php 
namespace Newsletter\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminNewsletterUser extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-newsletter-user-view';
	protected $_contentName = 'adminNewsletterUser';
	public function getEventName() {
		return 'adminUserView';
	}
	public function getPriority() {
		return 800;
	}
	
}

