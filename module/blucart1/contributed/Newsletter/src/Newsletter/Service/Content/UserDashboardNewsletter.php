<?php 
namespace Newsletter\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class UserDashboardNewsletter extends AbstractContentEvent {

	protected $_contentTemplate = 'common/user-dashboard-newsletter';
	protected $_contentName = 'userDashboardNewsletter';
	public function getEventName() {
		return 'userDashboard';
	}
	public function getPriority() {
		return 1000;
	}
	
}

