<?php 
namespace Newsletter\Service\NestedSorting;

use Core\Functions;
use Newsletter\Api\NewsletterApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminNewsletterCategory extends AbstractNestedSortingEvent {
	protected $_entityName = '\Newsletter\Entity\NewsletterCategory';
	public function getEventName() {
		return 'adminNewsletterCategorySort';
	}
	public function getPriority() {
		return 1000;
	}
	
}
