<?php

namespace Newsletter\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;

class AdminNewsletterNavigation extends AdminNavigationEvent {
	public function common() {
		$navigation = $this->getNavigation ();
		
		$page = $navigation->findOneBy ( 'id', 'marketingMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Newsletter',
							'uri' => '#',
							'id' => 'newsletterMain',
							'iconClass' => 'glyphicon-envelope' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'newsletterMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Newsletter',
							'route' => 'admin-newsletter',
							'id' => 'admin-newsletter',
							'iconClass' => 'glyphicon-envelope' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'newsletterMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Newsletter Category',
							'route' => 'admin-newsletter-category',
							'id' => 'admin-newsletter-category',
							'iconClass' => 'glyphicon-envelope' 
					) 
			) );
		}
	}
}

