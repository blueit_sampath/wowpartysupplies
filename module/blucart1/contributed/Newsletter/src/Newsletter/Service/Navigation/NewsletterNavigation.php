<?php

namespace Newsletter\Service\Navigation;

use Common\Navigation\Event\NavigationEvent;

class NewsletterNavigation extends NavigationEvent {

    public function breadcrumb() {
        parent::breadcrumb();
        $page = array(
            'label' => 'Newsletter Subscription',
            'route' => 'newsletter',
            'id' => 'newsletter'
        );

        $this->addDefaultMenu($page);
    }

}
