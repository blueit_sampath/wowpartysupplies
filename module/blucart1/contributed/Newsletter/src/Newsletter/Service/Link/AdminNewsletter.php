<?php

namespace Newsletter\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminNewsletter extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminNewsletter';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-newsletter-add' );
		$item = $linkContainer->add ( 'admin-newsletter-add', 'Create Newsletter', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

