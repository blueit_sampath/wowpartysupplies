<?php

namespace Newsletter\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminNewsletterSubscriber extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminNewsletterSubscriber';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$newsletterCategoryId = Functions::fromRoute ( 'newsletterCategoryId' );
		if (! $newsletterCategoryId) {
			return false;
		}
		$u = $url ( 'admin-newsletter-subscriber-add', array (
				'newsletterCategoryId' => $newsletterCategoryId 
		) );
		$item = $linkContainer->add ( 'admin-newsletter-subscriber-add', 'Add New Subscriber', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

