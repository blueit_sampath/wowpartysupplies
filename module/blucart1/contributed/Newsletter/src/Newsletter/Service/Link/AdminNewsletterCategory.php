<?php

namespace Newsletter\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminNewsletterCategory extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminNewsletterCategory';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-newsletter-category-add' );
		$item = $linkContainer->add ( 'admin-newsletter-category-add', 'Add Newsletter Category', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-newsletter-category-sort' );
		$item = $linkContainer->add ( 'admin-newsletter-category-sort', 'Arrange Newsletters', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		$item->setImageClass ( 'splashy-slider_no_pointy_thing' );
		
		return $this;
	}
}

