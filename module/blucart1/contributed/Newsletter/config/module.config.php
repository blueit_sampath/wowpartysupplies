<?php

return array(
    'router' => array('routes' => array(
            'admin-newsletter' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/newsletter',
                    'defaults' => array(
                        'controller' => 'Newsletter\Controller\AdminNewsletter',
                        'action' => 'index'
                    )
                )
            ),
            'newsletter' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/newsletter',
                    'defaults' => array(
                        'controller' => 'Newsletter\Controller\Newsletter',
                        'action' => 'index'
                    )
                )
            ),
            'admin-newsletter-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/newsletter/add[/ajax/:ajax][/:newsletterId]',
                    'defaults' => array(
                        'controller' => 'Newsletter\Controller\AdminNewsletter',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'newsletterId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-newsletter-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/newsletter/category',
                    'defaults' => array(
                        'controller' => 'Newsletter\Controller\AdminNewsletterCategory',
                        'action' => 'index'
                    )
                )
            ),
            'admin-newsletter-category-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/newsletter/category/add[/ajax/:ajax][/:newsletterCategoryId]',
                    'defaults' => array(
                        'controller' => 'Newsletter\Controller\AdminNewsletterCategory',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'newsletterCategoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-newsletter-category-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/newsletter/category/sort[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Newsletter\Controller\AdminNewsletterCategory',
                        'action' => 'sort',
                        'ajax' => 'true'
                    ),
                    'constraints' => array('ajax' => 'true|false')
                )
            ),
            'admin-newsletter-subscriber' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/newsletter/subscriber[/ajax/:ajax][/:newsletterCategoryId]',
                    'defaults' => array(
                        'controller' => 'Newsletter\Controller\AdminNewsletterSubscriber',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'newsletterCategoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-newsletter-subscriber-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/newsletter/subscriber/add[/ajax/:ajax][/newsletterCategoryId/:newsletterCategoryId][/newsletterSubscriberId/:newsletterSubscriberId]',
                    'defaults' => array(
                        'controller' => 'Newsletter\Controller\AdminNewsletterSubscriber',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'newsletterCategoryId' => '[0-9]+',
                        'newsletterSubscriberId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            )
        )),
    'events' => array(
        'Newsletter\Service\Grid\AdminNewsletterCategory' => 'Newsletter\Service\Grid\AdminNewsletterCategory',
        'Newsletter\Service\Link\AdminNewsletterCategory' => 'Newsletter\Service\Link\AdminNewsletterCategory',
        'Newsletter\Service\Form\AdminNewsletterCategory' => 'Newsletter\Service\Form\AdminNewsletterCategory',
        'Newsletter\Service\Tab\AdminNewsletterCategory' => 'Newsletter\Service\Tab\AdminNewsletterCategory',
        'Newsletter\Service\NestedSorting\AdminNewsletterCategory' => 'Newsletter\Service\NestedSorting\AdminNewsletterCategory',
        'Newsletter\Service\Grid\AdminNewsletterSubscriber' => 'Newsletter\Service\Grid\AdminNewsletterSubscriber',
        'Newsletter\Service\Link\AdminNewsletterSubscriber' => 'Newsletter\Service\Link\AdminNewsletterSubscriber',
        'Newsletter\Service\Tab\AdminNewsletterSubscriber' => 'Newsletter\Service\Tab\AdminNewsletterSubscriber',
        'Newsletter\Service\Form\AdminNewsletterSubscriber' => 'Newsletter\Service\Form\AdminNewsletterSubscriber',
        'Newsletter\Service\Grid\AdminNewsletter' => 'Newsletter\Service\Grid\AdminNewsletter',
        'Newsletter\Service\Form\AdminNewsletter' => 'Newsletter\Service\Form\AdminNewsletter',
        'Newsletter\Service\Link\AdminNewsletter' => 'Newsletter\Service\Link\AdminNewsletter',
        'Newsletter\Service\Content\AdminNewsletterUser' => 'Newsletter\Service\Content\AdminNewsletterUser',
        'Newsletter\Service\Grid\AdminNewsletterUser' => 'Newsletter\Service\Grid\AdminNewsletterUser',
        'Newsletter\Service\Navigation\AdminNewsletterNavigation' => 'Newsletter\Service\Navigation\AdminNewsletterNavigation',
        'Newsletter\Service\Form\RegisterNewsletter' => 'Newsletter\Service\Form\RegisterNewsletter',
        'Newsletter\Service\Form\UserEditProfileNewsletter' => 'Newsletter\Service\Form\UserEditProfileNewsletter',
        'Newsletter\Service\Content\UserDashboardNewsletter' => 'Newsletter\Service\Content\UserDashboardNewsletter',
        'Newsletter\Service\Form\NewsletterForm' => 'Newsletter\Service\Form\NewsletterForm',
        'Newsletter\Service\Block\Newsletter' => 'Newsletter\Service\Block\Newsletter',
        'Newsletter\Service\Block\AdminUserViewNewsletter' => 'Newsletter\Service\Block\AdminUserViewNewsletter',
        'Newsletter\Service\Navigation\NewsletterNavigation' => 'Newsletter\Service\Navigation\NewsletterNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Newsletter\Service\Grid\AdminNewsletterCategory' => 'Newsletter\Service\Grid\AdminNewsletterCategory',
            'Newsletter\Service\Link\AdminNewsletterCategory' => 'Newsletter\Service\Link\AdminNewsletterCategory',
            'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryForm' => 'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryForm',
            'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryFilter' => 'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryFilter',
            'Newsletter\Service\Form\AdminNewsletterCategory' => 'Newsletter\Service\Form\AdminNewsletterCategory',
            'Newsletter\Service\Tab\AdminNewsletterCategory' => 'Newsletter\Service\Tab\AdminNewsletterCategory',
            'Newsletter\Service\NestedSorting\AdminNewsletterCategory' => 'Newsletter\Service\NestedSorting\AdminNewsletterCategory',
            'Newsletter\Service\Grid\AdminNewsletterSubscriber' => 'Newsletter\Service\Grid\AdminNewsletterSubscriber',
            'Newsletter\Service\Link\AdminNewsletterSubscriber' => 'Newsletter\Service\Link\AdminNewsletterSubscriber',
            'Newsletter\Service\Tab\AdminNewsletterSubscriber' => 'Newsletter\Service\Tab\AdminNewsletterSubscriber',
            'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberForm' => 'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberForm',
            'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberFilter' => 'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberFilter',
            'Newsletter\Service\Form\AdminNewsletterSubscriber' => 'Newsletter\Service\Form\AdminNewsletterSubscriber',
            'Newsletter\Service\Grid\AdminNewsletter' => 'Newsletter\Service\Grid\AdminNewsletter',
            'Newsletter\Form\AdminNewsletter\NewsletterForm' => 'Newsletter\Form\AdminNewsletter\NewsletterForm',
            'Newsletter\Form\AdminNewsletter\NewsletterFilter' => 'Newsletter\Form\AdminNewsletter\NewsletterFilter',
            'Newsletter\Service\Form\AdminNewsletter' => 'Newsletter\Service\Form\AdminNewsletter',
            'Newsletter\Service\Link\AdminNewsletter' => 'Newsletter\Service\Link\AdminNewsletter',
            'Newsletter\Service\Content\AdminNewsletterUser' => 'Newsletter\Service\Content\AdminNewsletterUser',
            'Newsletter\Service\Grid\AdminNewsletterUser' => 'Newsletter\Service\Grid\AdminNewsletterUser',
            'Newsletter\Service\Navigation\AdminNewsletterNavigation' => 'Newsletter\Service\Navigation\AdminNewsletterNavigation',
            'Newsletter\Service\Form\RegisterNewsletter' => 'Newsletter\Service\Form\RegisterNewsletter',
            'Newsletter\Service\Content\UserDashboardNewsletter' => 'Newsletter\Service\Content\UserDashboardNewsletter',
            'Newsletter\Service\Form\UserEditProfileNewsletter' => 'Newsletter\Service\Form\UserEditProfileNewsletter',
            'Newsletter\Form\NewsletterForm\NewsletterForm' => 'Newsletter\Form\NewsletterForm\NewsletterForm',
            'Newsletter\Form\NewsletterForm\NewsletterFilter' => 'Newsletter\Form\NewsletterForm\NewsletterFilter',
            'Newsletter\Service\Form\NewsletterForm' => 'Newsletter\Service\Form\NewsletterForm',
            'Newsletter\Service\Block\Newsletter' => 'Newsletter\Service\Block\Newsletter',
            'Newsletter\Service\Block\AdminUserViewNewsletter' => 'Newsletter\Service\Block\AdminUserViewNewsletter',
            'Newsletter\Service\Navigation\NewsletterNavigation' => 'Newsletter\Service\Navigation\NewsletterNavigation'
        ),
        'factories' => array(
            'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryFactory' => 'Newsletter\Form\AdminNewsletterCategory\NewsletterCategoryFactory',
            'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberFactory' => 'Newsletter\Form\AdminNewsletterSubscriber\NewsletterSubscriberFactory',
            'Newsletter\Form\AdminNewsletter\NewsletterFactory' => 'Newsletter\Form\AdminNewsletter\NewsletterFactory',
            'Newsletter\Form\NewsletterForm\NewsletterFactory' => 'Newsletter\Form\NewsletterForm\NewsletterFactory'
        )
    ),
    'controllers' => array('invokables' => array(
            'Newsletter\Controller\AdminNewsletterCategory' => 'Newsletter\Controller\AdminNewsletterCategoryController',
            'Newsletter\Controller\AdminNewsletterSubscriber' => 'Newsletter\Controller\AdminNewsletterSubscriberController',
            'Newsletter\Controller\AdminNewsletter' => 'Newsletter\Controller\AdminNewsletterController',
            'Newsletter\Controller\Newsletter' => 'Newsletter\Controller\NewsletterController'
        )),
    'view_manager' => array('template_path_stack' => array('Newsletter' => __DIR__ . '/../view')),
    'doctrine' => array('driver' => array(
            'Newsletter_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Newsletter/Entity')
            ),
            'orm_default' => array('drivers' => array('Newsletter\Entity' => 'Newsletter_driver'))
        )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('Newsletter' => __DIR__ . '/../public')))
);
