<?php 
namespace Contact\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use Config\Service\Form\AdminConfig;

class AdminContactConfigAdmin extends AdminConfig {
    protected $_columnKeys = array (
        'CONTACT_ADMIN_FROM_EMAIL',
        'CONTACT_ADMIN_FROM_NAME',
        'CONTACT_ADMIN_NOTIFY',
        'CONTACT_ADMIN_SUBJECT',
    		'CONTACT_ADMIN_SUBJECT',
    		'CONTACT_ADMIN_BODYHTML',
    		'CONTACT_ADMIN_BODYPLAIN'
    );
	public function getFormName() {
		return 'adminContactConfigAdmin';
	}
	public function getPriority() {
		return 1000;
	}
	
}
