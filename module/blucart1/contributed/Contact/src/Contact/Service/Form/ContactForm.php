<?php
namespace Contact\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class ContactForm extends AbstractMainFormEvent
{

    protected $_columnKeys = array(
        'message',
        'email',
        'name',
        'contactNumber'
    );

    protected $_entity = '\Contact\Entity\Contact';

    protected $_entityName = 'contact';

    public function getFormName()
    {
        return 'contactForm';
    }

    public function getPriority()
    {
        return 1000;
    }
}
