<?php

namespace Search\Controller;

use Common\MVC\Controller\AbstractFrontController;
use Search\Form\SearchForm\SearchFactory;
use Zend\View\Model\ViewModel;
use Search\Api\SearchApi;

class IndexController extends AbstractFrontController {
	public function indexAction() {
		$query = $this->getRequest ()->getQuery ( 'query', '' );
                $page = $this->params()->fromRoute('page');
                $perPage = $this->params()->fromRoute('perPage');
                
		$results = SearchApi::getProductsByString ( $query,$page,$perPage );
		$form = $this->getForm ();
		$form->setData ( $this->getRequest ()->getQuery () );
		return array (
				'form' => $form,
				'results' => $results 
		);
	}
	public function queryAction() {
		$query = $this->getRequest ()->getQuery ( 'query', '' );
		$results = SearchApi::getProductsByString ( $query );
		$this->getResponse ()->getHeaders ()->addHeaderLine ( 'Content-Type', 'application/json' );
		$viewModel = new ViewModel ( array (
				'results' => $results 
		) );
		$viewModel->setTerminal ( true );
		return $viewModel;
	}
	/**
	 *
	 * @return SearchFactory
	 */
	public function getForm() {
		return $this->getServiceLocator ()->get ( 'Search\Form\SearchForm\SearchFactory' );
	}
}