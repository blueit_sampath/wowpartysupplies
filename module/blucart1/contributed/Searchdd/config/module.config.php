<?php
return array(
    'router' => array(
        'routes' => array(
            'search' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search[/page/:page][/perPage/:perPage]',
                    'defaults' => array(
                        'controller' => 'Search\Controller\Index',
                        'action' => 'index',
                        'page' => 1,
                        'perPage' => 12
                    ),
                    'constraints' => array(
                        'page' => '[0-9]+',
                        'perPage' => '[0-9]+'
                    )
                )
            ),
            'search-query' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search/query[/page/:page][/perPage/:perPage]',
                    'defaults' => array(
                        'controller' => 'Search\Controller\Index',
                        'action' => 'query',
                        'page' => 1,
                        'perPage' => 12
                    ),
                    'constraints' => array(
                        'page' => '[0-9]+',
                        'perPage' => '[0-9]+'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Search\Service\Block\SearchFormBlock' => 'Search\Service\Block\SearchFormBlock',
        'Search\Service\Navigation\SearchNavigation' => 'Search\Service\Navigation\SearchNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Search\Form\SearchForm\SearchForm' => 'Search\Form\SearchForm\SearchForm',
            'Search\Form\SearchForm\SearchFilter' => 'Search\Form\SearchForm\SearchFilter',
            'Search\Service\Block\SearchFormBlock' => 'Search\Service\Block\SearchFormBlock',
             'Search\Service\Navigation\SearchNavigation' => 'Search\Service\Navigation\SearchNavigation'
        ),
        'factories' => array(
            'Search\Form\SearchForm\SearchFactory' => 'Search\Form\SearchForm\SearchFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Search\Controller\Index' => 'Search\Controller\IndexController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Search' => __DIR__.'/../view'
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Search' => __DIR__.'/../public'
            )
        )
    )
);
