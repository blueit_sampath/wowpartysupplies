<?php

namespace Seo;

return array(
    'router' => array(
        'routes' => array(
            'admin-seo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo',
                    'defaults' => array(
                        'controller' => 'Seo\Controller\AdminSeo',
                        'action' => 'index'
                    )
                )
            ),
            'admin-seo-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo/add[/ajax/:ajax][/:seoId]',
                    'defaults' => array(
                        'controller' => 'Seo\Controller\AdminSeo',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-seo-config-general' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/seo/config/general[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Seo\Controller\AdminSeoConfigGeneral',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Seo\Service\Grid\AdminSeo' => 'Seo\Service\Grid\AdminSeo',
        'Seo\Service\Navigation\AdminSeoNavigation' => 'Seo\Service\Navigation\AdminSeoNavigation',
        'Seo\Service\Form\AdminSeoConfigGeneral' => 'Seo\Service\Form\AdminSeoConfigGeneral',
        'Seo\Service\Form\AdminSeo' => 'Seo\Service\Form\AdminSeo',
        'Seo\Service\Tab\AdminSeo' => 'Seo\Service\Tab\AdminSeo',
        'Seo\Service\Link\AdminSeo' => 'Seo\Service\Link\AdminSeo',
       
    ),
    'service_manager' => array(
        'invokables' => array(
            'Seo\Service\Grid\AdminSeo' => 'Seo\Service\Grid\AdminSeo',
            'Seo\Service\Navigation\AdminSeoNavigation' => 'Seo\Service\Navigation\AdminSeoNavigation',
            'Seo\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm' => 'Seo\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm',
            'Seo\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter' => 'Seo\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter',
            'Seo\Service\Form\AdminSeoConfigGeneral' => 'Seo\Service\Form\AdminSeoConfigGeneral',
            'Seo\Service\Form\AdminSeo' => 'Seo\Service\Form\AdminSeo',
            'Seo\Form\AdminSeo\SeoForm' => 'Seo\Form\AdminSeo\SeoForm',
            'Seo\Form\AdminSeo\SeoFilter' => 'Seo\Form\AdminSeo\SeoFilter',
            'Seo\Service\Tab\AdminSeo' => 'Seo\Service\Tab\AdminSeo',
            'Seo\Service\Link\AdminSeo' => 'Seo\Service\Link\AdminSeo',
            'Seo\Service\Navigation\AdminSeo' => 'Seo\Service\Navigation\AdminSeo',
        ),
        'factories' => array(
            'Seo\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory' => 'Seo\Form\AdminSeoConfigGeneral\SeoConfigGeneralFactory',
            'Seo\Form\AdminSeo\SeoFormFactory' => 'Seo\Form\AdminSeo\SeoFormFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Seo\Controller\AdminSeo' => 'Seo\Controller\AdminSeoController',
            'Seo\Controller\AdminSeoConfigGeneral' => 'Seo\Controller\AdminSeoConfigGeneralController'
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'seo' => 'Seo\View\Helper\Seo',
            'url' => 'Seo\View\Helper\SeoUrl'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __NAMESPACE__ => __DIR__ . '/../public'
            )
        )
    )
);
