<?php
namespace Seo\Form\AdminSeoConfigGeneral;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class SeoConfigGeneralForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'SEO_DEFAULT_TITLE',
				'attributes' => array (
						'type' => 'text',
						 
				),
				'options' => array (
						'label' => 'Seo Default Title' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'SEO_DEFAULT_KEYWORDS',
				'attributes' => array (
						'type' => 'textarea',
							
				),
				'options' => array (
						'label' => 'Seo Default Keywords'
				)
		), array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'SEO_DEFAULT_DESCRIPTION',
				'attributes' => array (
						'type' => 'textarea',
							
				),
				'options' => array (
						'label' => 'Seo Default Description'
				)
		), array (
				'priority' => 980
		) );
		
		$this->add ( array (
				'name' => 'SEO_DEFAULT_ROBOTS',
				'attributes' => array (
						'type' => 'textarea',
							
				),
				'options' => array (
						'label' => 'Seo Default Robots'
				)
		), array (
				'priority' => 970
		) );
		
		$this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'SEO_ENABLE',
				'options' => array (
						'label' => 'Enable SEO',
						'use_hidden_element' => true
				)
		), array (
				'priority' => 960
		) );
                
                $this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'SEO_ENABLE_URLS',
				'options' => array (
						'label' => 'Enable SEO Friendly URLS',
						'use_hidden_element' => true
				)
		), array (
				'priority' => 950
		) );
                
                 $this->add ( array (
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'SEO_ENABLE_301_REDIRECTION',
				'options' => array (
						'label' => 'Enable 301 Redirection',
						'use_hidden_element' => true
				)
		), array (
				'priority' => 940
		) );
		
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}