<?php

namespace Seo\Form\AdminSeoConfigGeneral;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SeoConfigGeneralFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'SEO_DEFAULT_TITLE',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_DEFAULT_DESCRIPTION',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_DEFAULT_KEYWORDS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_DEFAULT_ROBOTS',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            )
                )
        );
        $this->add(array(
            'name' => 'SEO_ENABLE',
            'required' => false,
        ));

        $this->add(array(
            'name' => 'SEO_ENABLE_URLS',
            'required' => false,
        ));
        $this->add(array(
            'name' => 'SEO_ENABLE_301_REDIRECTION',
            'required' => false,
        ));
    }

}
