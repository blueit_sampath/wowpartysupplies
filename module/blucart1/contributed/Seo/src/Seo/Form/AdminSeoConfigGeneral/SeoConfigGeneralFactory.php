<?php
namespace Seo\Form\AdminSeoConfigGeneral;

use Common\Form\Option\AbstractFormFactory;

class SeoConfigGeneralFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Seo\Form\AdminSeoConfigGeneral\SeoConfigGeneralForm' );
		$form->setName ( 'adminSeoConfigGeneral' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Seo\Form\AdminSeoConfigGeneral\SeoConfigGeneralFilter' );
	}
}
