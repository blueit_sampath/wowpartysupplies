<?php
namespace Seo\Form\AdminSeo;

use Common\Form\Option\AbstractFormFactory;

class SeoFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Seo\Form\AdminSeo\SeoForm' );
		$form->setName ( 'adminSeoAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Seo\Form\AdminSeo\SeoFilter' );
	}
}
