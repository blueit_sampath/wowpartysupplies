<?php

namespace Seo\Form\AdminSeo;

use Zend\Validator\Regex;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SeoFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {

        $this->add(array(
            'name' => 'url',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => array(
                            $this,
                            'checkUrl'
                        ),
                        'messages' => array(
                            \Zend\Validator\Callback::INVALID_VALUE => "URL already exists",
                        )
                    )
                )
            )
        ));


        $this->add(array(
            'name' => 'alias',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
                array(
                    'name' => 'StripTags'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => array(
                            $this,
                            'checkAlias'
                        ),
                        'messages' => array(
                            \Zend\Validator\Callback::INVALID_VALUE => "Alias Url already exists",
                        )
                    )
                )
            )
        ));


        $this->add(array(
            'name' => 'pageTitle',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'maxLength' => 255
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'metaKeywords',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            ),
        ));

        $this->add(array(
            'name' => 'metaDescription',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            ),
        ));

        $this->add(array(
            'name' => 'robots',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            ),
        ));
    }

    public function checkUrl($value, $data = array()) {
        $entity = \Seo\Api\SeoApi::getByUrl($value);
        if (!$entity) {
            return true;
        }
        if ($entity->id == $data['id']) {
            return true;
        }
        return false;
    }

    public function checkAlias($value, $data = array()) {
        $entity = \Seo\Api\SeoApi::getByAlias($value);
        if (!$entity) {
            return true;
        }
        if ($entity->url == $data['url']) {
            return true;
        }
        return false;
    }

}
