<?php

/**
 * Product
 * 
 * @author
 * @version 
 */

namespace Seo\View\Helper;

use Config\Api\ConfigApi;
use Zend\View\Helper\AbstractHelper;

/**
 * View Helper
 */
class Seo extends AbstractHelper {

    public function __invoke() {
        
    }

    public function setSeo($seo) {
        $view = $this->getView();

        if (!\Seo\Api\SeoApi::checkIfSeoEnabled()) {
            return;
        }
        $params = \Core\Functions::getRouteParams();
        $pageTitle = '';
        if ($seo && $seo->pageTitle) {
            $pageTitle = $seo->pageTitle;
        } else {
            if ($value = ConfigApi::getConfigByKey('SEO_DEFAULT_TITLE')) {
                $pageTitle = $value;
            }
        }
        $pageTitle = $this->substitute($pageTitle, $params);
        $view->headTitle($pageTitle);

        $keywords = '';
        if ($seo && $seo->metaKeywords) {
            $keywords = $seo->metaKeywords;
        } else {
            if ($value = ConfigApi::getConfigByKey('SEO_DEFAULT_KEYWORDS')) {
                $keywords = $value;
            }
        }
        $keywords = $this->substitute($keywords, $params);
        $view->headMeta()->setName('keywords', $keywords);

        $description = '';
        if ($seo && $seo->metaDescription) {
            $description = $seo->metaDescription;
        } else {
            if ($value = ConfigApi::getConfigByKey('SEO_DEFAULT_DESCRIPTION')) {
                $description = $value;
            }
        }
        $description = $this->substitute($description, $params);
        $view->headMeta()->setName('description', $description);

        $robots = '';
        if ($seo && $seo->robots) {
            $robots = $seo->robots;
        } else {
            if ($value = ConfigApi::getConfigByKey('SEO_DEFAULT_ROBOTS')) {
                $robots = $value;
            }
        }
        $robots = $this->substitute($robots, $params);
        $view->headMeta()->setName('robots', $robots);
    }

    public function substitute($value, $params = array()) {

        return $this->getView()->twig($value, $params);
    }

}
