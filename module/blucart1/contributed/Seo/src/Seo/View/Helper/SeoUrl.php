<?php

/**
 * Product
 * 
 * @author
 * @version 
 */

namespace Seo\View\Helper;

use Core\Functions;
use Seo\Api\SeoApi;
use Zend\View\Helper\Url;
use Zend\View\Helper\AbstractHelper;

/**
 * View Helper
 */
class SeoUrl extends Url {

    public function __construct() {
        $sm = Functions::getServiceLocator();
        $this->router = $sm->get('Router');
    }

    public function __invoke($name = null, array $params = array(), $options = array(), $reuseMatchedParams = false) {
        $url = parent::__invoke($name, $params, $options, $reuseMatchedParams);
        if (is_array($options) && isset($options ['force_canonical']) && $options ['force_canonical']) {
            return $url;
        }

        $alias = SeoApi::getAliasByUrl($url);
      
        return $alias;
    }

}
