<?php

namespace Seo\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminSeoController extends AbstractAdminController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('Seo\Form\AdminSeo\SeoFormFactory');
    }

    public function getSaveRedirector() {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-seo-add', array(
                    'seoId' => $form->get('id')->getValue()
                ));
    }

}
