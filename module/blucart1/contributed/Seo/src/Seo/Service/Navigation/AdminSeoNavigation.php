<?php

namespace Seo\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;

class AdminSeoNavigation extends AdminNavigationEvent {
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'marketingMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Seo',
							'route' => 'admin-seo',
							'id' => 'marketingMainSeoMain',
							'iconClass' => 'glyphicon-eye-open' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'marketingMainSeoMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Seo',
							'route' => 'admin-seo',
							'id' => 'admin-seo',
							'iconClass' => 'glyphicon-eye-open' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'marketingMainSeoMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Settings',
							'route' => 'admin-seo-config-general',
							'id' => 'admin-seo-config-general',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox' 
					) 
			) );
		}
	}
}

