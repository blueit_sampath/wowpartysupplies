<?php

namespace Seo\Service\Grid;

use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;

class AdminSeo extends AbstractMainBlucartGridEvent {

    protected $_columnKeys = array(
        'id',
        'url',
        'alias',
        'pageTitle',
        'metaDescription',
        'metaKeywords',
        'robots'
    );
    protected $_entity = '\Seo\Entity\Seo';
    protected $_entityName = 'seo';

    public function getEventName() {
        return 'adminSeo';
    }

    public function preSchema($e) {
        parent::preSchema($e);

        $array = array();

        $grid = $this->getGrid();
        $columns = $grid->getColumns();

        $columnItem = new ColumnItem ();
        $columnItem->setField('id');
        $columnItem->setTitle('ID');
        $columnItem->setType('number');
        $columnItem->setEditable(false);
        $columnItem->setIsPrimary(true);
        $columnItem->setWeight(1000);
        $columns->addColumn('seo.id', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('url');
        $columnItem->setTitle('URL');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(990);
        $columns->addColumn('seo.url', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('alias');
        $columnItem->setTitle('Alias');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('seo.alias', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('pageTitle');
        $columnItem->setTitle('Page Title');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(970);
        $columns->addColumn('seo.pageTitle', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('metaKeywords');
        $columnItem->setTitle('Meta Keywords');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columns->addColumn('seo.metaKeywords', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('metaDescription');
        $columnItem->setTitle('Meta Description');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columns->addColumn('seo.metaDescription', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('robots');
        $columnItem->setTitle('Robots');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(950);
        $columns->addColumn('seo.robots', $columnItem);

        $this->formToolbar();

        return $columns;
    }

    public function formToolbar() {
        $grid = $this->getGrid();

        $toolbar = $grid->getToolbar();
        if (!$toolbar) {
            $toolbar = new Toolbar ();
            $grid->setToolbar($toolbar);
        }

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('create');
        $toolBarItem->setWeight(1000);
        $toolbar->addToolbar('create', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('save', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('cancel', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(700);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }

    public function preCreate($e) {
        parent::preCreate($e);
        $this->checkValidation();
    }

    public function preUpdate($e) {
        parent::preCreate($e);
        $this->checkValidation();
    }

    public function checkValidation() {
        $grid = $this->getGrid();

        $resultObject = $this->getGrid()->getResultObject();
        $params = $resultObject->getResults();
        $validationContainer = $grid->getValidateContainer();

        $url = $params['url'];
        $alias = $params['alias'];

        if (!$url) {
            $validationContainer->add('urlRequired', 'Url is required');
        }
        if (!$this->checkUrl($url, $params)) {
            $validationContainer->add('urlExists', 'Url Already Exists');
        }


        if ($alias && (!$this->checkAlias($alias, $params))) {
            $validationContainer->add('aliasExists', 'Alias Already Exists');
        }
    }

    public function checkUrl($value, $data = array()) {
        $entity = \Seo\Api\SeoApi::getByUrl($value);
        if (!$entity) {
            return true;
        }
        if ($entity->id == $data['id']) {
            return true;
        }
        return false;
    }

    public function checkAlias($value, $data = array()) {
        $entity = \Seo\Api\SeoApi::getByAlias($value);
        if (!$entity) {
            return true;
        }
        if ($entity->url == $data['url']) {
            return true;
        }
        return false;
    }

}
