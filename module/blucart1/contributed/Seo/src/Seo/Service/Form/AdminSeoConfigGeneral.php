<?php

namespace Seo\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminSeoConfigGeneral extends AdminConfig {

    protected $_columnKeys = array(
        'SEO_DEFAULT_TITLE',
        'SEO_DEFAULT_KEYWORDS',
        'SEO_DEFAULT_DESCRIPTION',
        'SEO_DEFAULT_ROBOTS',
        'SEO_ENABLE',
        'SEO_ENABLE_301_REDIRECTION',
        'SEO_ENABLE_URLS'
    );

    public function getFormName() {
        return 'adminSeoConfigGeneral';
    }

    public function getPriority() {
        return 1000;
    }

}
