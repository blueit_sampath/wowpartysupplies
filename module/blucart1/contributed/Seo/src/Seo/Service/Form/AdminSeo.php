<?php
namespace Seo\Service\Form;


use Common\Form\Option\AbstractMainFormEvent;

class AdminSeo extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'url',
        'alias',
        'pageTitle',
        'metaKeywords',
        'metaDescription',
        'robots'
    );
    protected $_entity = '\Seo\Entity\Seo';
    protected $_entityName = 'seo';

    public function getFormName() {
        return 'adminSeoAdd';
    }

    public function getPriority() {
        return 1000;
    }

}
