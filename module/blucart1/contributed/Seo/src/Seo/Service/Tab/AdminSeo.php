<?php

namespace Seo\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminSeo extends AbstractTabEvent {
	public function getEventName() {
		return 'adminSeoAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$seoId = Functions::fromRoute ( 'seoId' );
		if (! $seoId) {
			return;
		}
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-seo-add', array (
				'seoId' => $seoId 
		) );
		$tabContainer->add ( 'admin-seo-add', 'General', $u, 1000 );
		
		return $this;
	}
}

