<?php

namespace Seo\Api;

use Core\Functions;
use Common\Api\Api;

class SeoApi extends Api {

    protected static $_entity = '\Seo\Entity\Seo';

    public static function getBasePath($url = "") {
        $sm = Functions::getServiceLocator();
        $plugins = $sm->get('ViewHelperManager');
        $plugin = $plugins->get('basepath');

        $basePath = $plugin('/' . $url);

        return $basePath;
    }

    public static function parseUrl($url) {
        $basePath = static::getBasePath('/');

        $url = static::strReplaceOnce($basePath, '/', $url);
        if ($url == '') {
            $url = '/';
        }
        return $url;
    }

    public static function strReplaceOnce($needle, $replace, $haystack) {

        $pos = strpos($haystack, $needle);
        if ($pos === false) {
            return $haystack;
        }
        return substr_replace($haystack, $replace, $pos, strlen($needle));
    }

    public static function getSeoById($id) {

        $em = Functions::getEntityManager();

        return $em->find(static::$_entity, $id);
    }

    public static function getByUrl($url) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array('url' => $url));
    }

    public static function getByAlias($alias) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array('alias' => $alias));
    }

    public static function checkAliasExits($alias, $url = '') {
        $em = Functions::getEntityManager();
        $entity = $em->getRepository(static::$_entity)->findOneBy(array('alias' => $alias));
        if (!$entity) {
            return false;
        }
        if ($url) {
            if ($entity->url == $url) {
                return false;
            }
        }
        return $entity;
    }

    public static function createSlug($string) {
        $slug = preg_replace('/[^A-Za-z0-9-\/\*]+/', '-', $string);
        return strtolower($slug);
    }

    public static function checkIfSeoEnabled() {
        return \Config\Api\ConfigApi::getConfigByKey('SEO_ENABLE');
    }

    public static function checkIfSeoFriendlyUrlsEnabled() {
        return \Config\Api\ConfigApi::getConfigByKey('SEO_ENABLE_URLS');
    }

    public static function checkIfRedirectionEnabled() {
        return \Config\Api\ConfigApi::getConfigByKey('SEO_ENABLE_301_REDIRECTION');
    }

    public static function getAliasByUrl($rawUrl, $returnEntity = false) {

        if (!static::checkIfSeoEnabled()) {
            if ($returnEntity) {
                return false;
            }
            return $rawUrl;
        }

        if (!static::checkIfSeoFriendlyUrlsEnabled()) {
            if (!$returnEntity) {
                return $rawUrl;
            }
        }
        $url = static::parseUrl($rawUrl);
        $em = Functions::getEntityManager();

        $result = $em->getRepository(static::$_entity)->findOneBy(array(
            'url' => $url
        ));
        if ($result) {
            if ($returnEntity) {
                return $result;
            }
            return static::getBasePath($result->alias);
        }

        $dql = 'select LOCATE(substring(u.url,1,length(u.url)-1) ,\'' . $url . '\') As matchedUrl,  u.alias As alias,u.url as url, length(u.url) as urllength, u.id as seoId  from \Seo\Entity\Seo  u where u.url like \'%*\'  having matchedUrl = 1   order by urllength desc';
        $query = $em->createQuery($dql);
        $results = $query->getResult();
        if ($results) {
            foreach ($results as $result) {

                if ($returnEntity) {
                    return static::getSeoById($result['seoId']);
                }
                $alias = substr($result['alias'], 0, -1);
                $alias = str_replace(substr($result['url'], 0, -1), $alias, $url);
                return static::getBasePath($alias);
            }
        }
        if ($returnEntity) {
            return false;
        }
        return $rawUrl;
    }

    public static function getUrlByAlias($rawAlias, $returnEntity = false) {


        if (!static::checkIfSeoEnabled()) {
            if ($returnEntity) {
                return false;
            }
            return $rawAlias;
        }

        if (!static::checkIfSeoFriendlyUrlsEnabled()) {
            if (!$returnEntity) {
                return $rawAlias;
            }
        }
        $alias = static::parseUrl($rawAlias);

        $em = Functions::getEntityManager();
        $result = $em->getRepository(static::$_entity)->findOneBy(array(
            'alias' => $alias
        ));
        if ($result) {
            if ($returnEntity) {
                return $result;
            }
            return static::getBasePath($result->url);
        }

        $dql = 'select  LOCATE(substring(u.alias,1,length(u.alias)-1),\'' . $alias . '\') matchedUrl, u.alias As alias,u.url as url, length(u.alias) as urllength, u.id as seoId from \Seo\Entity\Seo  u where u.alias like \'%*\' HAVING  matchedUrl = 1 order by urllength desc';
        $query = $em->createQuery($dql);
        $results = $query->getResult();

        if ($results) {
            foreach ($results as $result) {
                if ($returnEntity) {
                    return static::getSeoById($result['seoId']);
                }
                $url = substr($result['url'], 0, -1);
                $url = str_replace(substr($result['alias'], 0, -1), $url, $alias);
                return static::getBasePath($url);
            }
        }
        if ($returnEntity) {
            return false;
        }
        return $rawAlias;
    }

    public static function insertSeoWithAlias($url, $array = array(), $appendString = '') {
        $em = static::getEntityManager();
        $entity = static::getByUrl($url);
        $columns = array('pageTitle', 'metaDescription', 'metaKeywords', 'robots');
        if (!$entity) {
            $entity = new static::$_entity;
        }
        foreach ($columns as $column) {
            if (isset($array[$column])) {
                $entity->$column = $array[$column];
            }
        }
        if (isset($array['alias']) && $array['alias']) {
            if ($array['alias'] && (strpos($array['alias'], '/') !== 0)) {
                $array['alias'] = '/' . $array['alias'];
            }

            $slug = $tempSlug = static::createSlug($array['alias']);
            $i = 1;
            while (static::checkAliasExits($tempSlug . $appendString, $url)) {
                $tempSlug = $slug . '-' . $i;
                $i++;
            }
            $entity->alias = $tempSlug . $appendString;
        }
        $entity->url = $url;

        $em->persist($entity);
        $em->flush();
        return $entity;
    }

}
