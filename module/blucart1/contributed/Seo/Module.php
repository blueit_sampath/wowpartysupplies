<?php

namespace Seo;

use Seo\Api\SeoApi;

class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onSeo($e) {
        $serviceManager = \Core\Functions::getServiceLocator();
        $request = $serviceManager->get('Request');
        $uri = $request->getUri();
        $path = $uri->getPath();

        $result = SeoApi::getAliasByUrl($path, true);
        $serviceManager->get('viewhelpermanager')->get('seo')->setSeo($result);
    }

    public function onRoute($e) {
        $serviceManager = $e->getApplication()->getServiceManager();
        $request = $serviceManager->get('Request');
        $uri = $request->getUri();
        $path = $uri->getPath();


        $result = SeoApi::getUrlByAlias($path);

        if ($result) {
            $uri->setPath($result);
            $request->setUri($uri);
        }
    }

    public function checkAndRedirect($e) {
        if(!SeoApi::checkIfRedirectionEnabled()){
            return;
        }
        $serviceManager = $e->getApplication()->getServiceManager();
        $request = $serviceManager->get('Request');
        $uri = $request->getUri();
        $path = $uri->getPath();
        $result = SeoApi::getAliasByUrl($path);
        if ($result && $result != $path) {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . $result);
            exit;
        }
    }

    public function onBootstrap($e) {

        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach('route', array(
            $this,
            'checkAndRedirect'
                ), 1000);
        $eventManager->attach('route', array(
            $this,
            'onRoute'
                ), 1000);

        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
                    $this,
                    'onSeo'
        ));
    }

}
