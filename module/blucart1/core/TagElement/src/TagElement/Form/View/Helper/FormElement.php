<?php

namespace TagElement\Form\View\Helper;

use TagElement\Form\Element\Tag;

use Core\Functions;
use DluTwBootstrap\Form\View\Helper\FormElementTwb;
use File\Form\Element;
use Zend\Form\View\Helper\FormElement as BaseFormElement;
use Zend\Form\ElementInterface;

class FormElement extends \TwbBundle\Form\View\Helper\TwbBundleFormElement {
	public function render(ElementInterface $element) {
		$renderer = $this->getView ();
		
		if (! method_exists ( $renderer, 'plugin' )) {
			// Bail early if renderer is not pluggable
			return '';
		}
		
		if (Functions::hasModule ( 'File' )) {
			if ($element instanceof Element\FileUpload) {
				$helper = $renderer->plugin ( 'form_file_upload' );
				return $helper ( $element );
			}
		}
		
		if ($element instanceof Tag) {
			$helper = $renderer->plugin ( 'form_tag' );
			return $helper ( $element );
		}
		
		
		return parent::render ( $element );
	}
}

