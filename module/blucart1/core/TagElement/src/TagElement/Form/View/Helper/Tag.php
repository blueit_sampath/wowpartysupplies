<?php

namespace TagElement\Form\View\Helper;

use FileUpload\Form\View\Helper\FormFile;
use TagElement\Form\Element\Tag as Tagele;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;

class Tag extends \TwbBundle\Form\View\Helper\TwbBundleFormElement {

    public function render(\Zend\Form\ElementInterface $oElement) {
        //Add form-controll class
        $sElementType = $oElement->getAttribute('type');

        if ($element instanceof Tagele) {
            $name = $element->getName();
            if ($name === null || $name === '') {
                throw new Exception\DomainException(sprintf('%s requires that the element has an assigned name; none discovered', __METHOD__));
            }
        }

        if (!in_array($sElementType, array('file', 'checkbox', 'radio', 'submit', 'multi_checkbox', 'static', 'button'))) {
            if ($sElementClass = $oElement->getAttribute('class')) {
                if (!preg_match('/(\s|^)form-control(\s|$)/', $sElementClass))
                    $oElement->setAttribute('class', trim($sElementClass . ' form-control'));
            } else
                $oElement->setAttribute('class', 'form-control');
        }

        $sMarkup = $this->getView()->render('common/tag-element', array(
            'element' => $oElement));

        //Addon prepend
        if ($aAddOnPrepend = $oElement->getOption('add-on-prepend'))
            $sMarkup = $this->renderAddOn($aAddOnPrepend) . $sMarkup;

        //Addon append
        if ($aAddOnAppend = $oElement->getOption('add-on-append'))
            $sMarkup .= $this->renderAddOn($aAddOnAppend);

        if ($aAddOnAppend || $aAddOnPrepend) {
            $sSpecialClass = '';
            //Input size
            if ($sElementClass = $oElement->getAttribute('class')) {
                if (preg_match('/(\s|^)input-lg(\s|$)/', $sElementClass))
                    $sSpecialClass .= ' input-group-lg';
                elseif (preg_match('/(\s|^)input-sm(\s|$)/', $sElementClass))
                    $sSpecialClass .= ' input-group-sm';
            }
            return sprintf(
                    self::$inputGroupFormat, trim($sSpecialClass), $sMarkup
            );
        }
        return $sMarkup;
    }

}
