<?php
namespace TagElement\Form\Element;

use Zend\Form\Form;
use Zend\Form\Element\Hidden;

class Tag extends Hidden {
	protected $_url = '';
	protected $_multiOptions = array();
	
	
	public function getUrl() {
		return $this->_url;
	}
	
	/**
	 *
	 * @param string $_url        	
	 */
	public function setUrl($_url) {
		$this->_url = $_url;
	}
	/**
	 * @return the $_multiOptions
	 */
	public function getMultiOptions() {
		return $this->_multiOptions;
	}

	/**
	 * @param multitype: $_multiOptions
	 */
	public function setMultiOptions($_multiOptions) {
		$this->_multiOptions = $_multiOptions;
	}

}