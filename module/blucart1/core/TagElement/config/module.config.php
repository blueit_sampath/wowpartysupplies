<?php

namespace TagElement;

return array (
		
		'view_manager' => array (
				'template_path_stack' => array (
						'TagElement' => __DIR__ . '/../view' 
				),
				
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'TagElement' => __DIR__ . '/../public' 
						) 
				) 
		),
		
		
)
;
