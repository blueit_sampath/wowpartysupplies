<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/FileUpload for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace TagElement;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;


class Module implements AutoloaderProviderInterface {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onBootstrap($e) {
        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();
        $viewHelperPluginManager = $serviceManager->get('view_helper_manager');
        $viewHelperPluginManager->setInvokableClass('formelement', 'TagElement\Form\View\Helper\FormElement');
        $viewHelperPluginManager->setInvokableClass('formTag', 'TagElement\Form\View\Helper\Tag');
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}
