<?php
namespace User\Form\AdminUserConfigGeneral;

use Common\Form\Option\AbstractFormFactory;

class UserConfigGeneralFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigGeneral\UserConfigGeneralForm' );
		$form->setName ( 'adminUserConfigGeneral' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigGeneral\UserConfigGeneralFilter' );
	}
}
