<?php

namespace User\Form\AdminUserConfigGeneral;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class UserConfigGeneralForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'USER_FROM_NAME',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'From Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'USER_FROM_EMAIL',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'From Email' 
				) 
		), array (
				'priority' => 990 
		) );
		
		
		$this->add ( array (
				'name' => 'USER_TC_TEXT',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'Terms & Condition Text' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}