<?php
namespace User\Form\UserAddress;

use Common\Form\Option\AbstractFormFactory;

class UserAddressFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserAddress\UserAddressForm' );
		$form->setName ( 'userAddressAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserAddress\UserAddressFilter' );
	}
}
