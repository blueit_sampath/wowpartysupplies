<?php

namespace User\Form\AdminUser;

use Common\Form\Form;
use Zend\Form\Element\Select;

class UserSearchForm extends Form {
	public function __construct($name = "adminUserSearch") {
		parent::__construct ( $name );
		$this->initPreEvent();
		$this->add ( array (
				'name' => 'firstName',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'First Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Last Name'
				)
		), array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'email',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Email'
				)
		), array (
				'priority' => 980
		) );
		
		$this->add ( array (
				'name' => 'contactNumber',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Contact Number'
				)
		), array (
				'priority' => 970
		) );
		
		$this->add ( array (
				'name' => 'userRoleTitle',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Role'
				)
		), array (
				'priority' => 970
		) );
	
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Search' 
				) 
		), array (
				'priority' => - 1000 
		) );
		$this->initPostEvent();
	}
}