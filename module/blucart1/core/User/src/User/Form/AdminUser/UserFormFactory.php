<?php
namespace User\Form\AdminUser;

use Common\Form\Option\AbstractFormFactory;

class UserFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminUser\UserForm' );
		$form->setName ( 'adminUserAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUser\UserFormFilter' );
	}
}
