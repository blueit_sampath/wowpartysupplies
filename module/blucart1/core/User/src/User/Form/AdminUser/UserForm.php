<?php

namespace User\Form\AdminUser;

use UserRole\Api\UserRoleApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class UserForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'firstName',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'First Name' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Last Name' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'email',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Email' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'password',
				'attributes' => array (
						'type' => 'password' 
				),
				'options' => array (
						'label' => 'Password' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'confirmPassword',
				'attributes' => array (
						'type' => 'password' 
				),
				'options' => array (
						'label' => 'Re-enter Password' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'contactNumber',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Contact Number' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'avatar',
				'type' => 'File\Form\Element\FileUpload' 
		), array (
				'priority' => 950 
		) );
		
		$select = new Select ( 'userRole' );
		$select->setLabel ( 'User Role' );
		$select->setValueOptions ( $this->getRoles () );
		$this->add ( $select, array (
				'priority' => 940 
		) );
		
		$this->getStatus ( 930 );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getRoles() {
		$roles = UserRoleApi::getRoles ();
		$temp = array ();
		if ($roles) {
			foreach ( $roles as $roleArray ) {
				$role = UserRoleApi::getRoleById ( $roleArray ['userRoleId'] );
				$temp [$role->id] = $role->title;
			}
		}
		return $temp;
	}
}