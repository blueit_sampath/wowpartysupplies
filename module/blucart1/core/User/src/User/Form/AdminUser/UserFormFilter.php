<?php

namespace User\Form\AdminUser;

use Zend\Validator\Callback;

use User\Api\UserApi;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UserFormFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'firstName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 45 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 45 
								) 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'email',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 255 
								) 
						),
						array (
								'name' => 'EmailAddress' 
						),
						array (
								'name' => 'Callback',
								'options' => array (
										'callback' => array (
												$this,
												'checkEmailAddress' 
										),
										'messages' => array (
												Callback::INVALID_VALUE => "Email already exists",
												 
										) 
								) 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'password',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 32 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'confirmPassword',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 32 
								) 
						),
						array (
								'name' => 'Identical',
								'options' => array (
										'token' => 'password' 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'contactNumber',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 32 
								) 
						),
						array (
								'name' => 'Regex',
								
								'options' => array (
										'pattern' => '/^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/' 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'userRole',
				'required' => true,
				'validators' => array (
						
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'avatar',
				'required' => false,
				
		) );
		$this->add ( array (
				'name' => 'status',
				'required' => true,
				'validators' => array (
						
						array (
								'name' => 'Int' 
						) 
				) 
		) );
	}
	public function checkEmailAddress($value) {
		if (UserApi::getUserByEmail ( $value )) {
			return false;
		}
		return true;
	}
} 