<?php
namespace User\Form\UserEditProfile;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use UserRole\Api\UserRoleApi;
use Zend\Form\Element\Radio;


class EditProfileForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'firstName',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'First Name'
				)
		), array (
				'priority' => 1000
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'Last Name'
				)
		), array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'contactNumber',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'Phone Number'
				)
		), array (
				'priority' => 980
		) );
		
		$this->addUserRoles ( 970 );
		$this->addCsrf();
		
	
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Save Profile'
				)
		), array (
				'priority' => - 100
		) );
	}
	
	protected function addUserRoles($priority) {
		$roles = UserRoleApi::getRolesAtRegistration ();
	
		$array = array ();
		foreach ( $roles as $role ) {
			$array [$role->id] = $role->title;
		}
	
		if (count ( $array ) > 1) {
				
			$radio = new Radio ( 'userRole' );
			$radio->setLabel ( 'Role' );
			$radio->setValueOptions ( $array );
			$this->add ( $radio, array (
					'priority' => $priority
			) );
		}
	}
	
}