<?php

namespace User\Form\UserEditProfile;

use User\Api\UserApi;
use Zend\Validator\Callback;
use Config\Api\ConfigApi;
use UserRole\Api\UserRoleApi;
use Zend\Validator\Regex;
use Zend\InputFilter\InputFilter;

class EditProfileFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'firstName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => "/^[a-zA-Z ,.'-]+$/",
										'messages' => array (
												Regex::NOT_MATCH => 'Use Letters & periods' 
										) 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'fileupload',
				'required' => false,
				));
		$this->add ( array (
				'name' => 'lastName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => "/^[a-zA-Z ,.'-]+$/",
										'messages' => array (
												Regex::NOT_MATCH => 'Use Letters & periods' 
										) 
								) 
						) 
				) 
		) );
		
		
		
		$this->add ( array (
				'name' => 'contactNumber',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => '/^(?!.*-.*-.*-)(?=(?:\d{8,13}$)|(?:(?=.{9,13}$)[^-]*-[^-]*$)|(?:(?=.{10,13}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/',
										'messages' => array (
												Regex::NOT_MATCH => 'Invalid Phone number' 
										) 
								) 
						) 
				) 
		) );
		
		$roles = UserRoleApi::getRolesAtRegistration ();
		if (count ( $roles ) > 1) {
			$this->add ( array (
					'name' => 'userRole',
					'required' => true 
			) );
		}
		
	}
	public function checkEmailAddress($value) {
		if (UserApi::getUserByEmail ( $value )) {
			return false;
		}
		return true;
	}
} 