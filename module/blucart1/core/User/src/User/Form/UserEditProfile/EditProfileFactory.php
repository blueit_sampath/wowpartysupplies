<?php
namespace User\Form\UserEditProfile;

use Common\Form\Option\AbstractFormFactory;

class EditProfileFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserEditProfile\EditProfileForm' );
		$form->setName ( 'userEditProfile' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserEditProfile\EditProfileFilter' );
	}
}
