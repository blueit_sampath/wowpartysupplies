<?php
namespace User\Form\AdminUserAddress;

use Common\Form\Option\AbstractFormFactory;

class UserAddressFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminUserAddress\UserAddressForm' );
		$form->setName ( 'adminUserAddressAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUserAddress\UserAddressFilter' );
	}
	
	
}
