<?php
namespace User\Form\AdminUserAddress;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use LocaleElement\Form\Element\CountryPicker;
use LocaleElement\Form\Element\StatePicker;
use LocaleElement\Form\Element\CheckboxHtmlText;


class UserAddressForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'firstName',
				'attributes' => array (
						'type' => 'text'
				)
				,
				'options' => array (
						'label' => 'First Name'
				)
		), array (
				'priority' => 1000
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'attributes' => array (
						'type' => 'text'
				)
				,
				'options' => array (
						'label' => 'Last Name'
				)
		), array (
				'priority' => 990
		) );
		
		$this->add ( array (
				'name' => 'email',
				'attributes' => array (
						'type' => 'text'
				)
				,
				'options' => array (
						'label' => 'Email'
				)
		), array (
				'priority' => 980
		) );
		
		$this->add ( array (
				'name' => 'contactNumber',
				'attributes' => array (
						'type' => 'text'
				)
				,
				'options' => array (
						'label' => 'Contact Number'
				)
		), array (
				'priority' => 970
		) );
		
		$this->add ( array (
				'name' => 'addressLine1',
				'attributes' => array (
						'type' => 'text'
				)
				,
				'options' => array (
						'label' => 'Address Line 1'
				)
		), array (
				'priority' => 960
		) );
		
		$this->add ( array (
				'name' => 'addressLine2',
				'attributes' => array (
						'type' => 'text'
				)
				,
				'options' => array (
						'label' => 'Address Line 2'
				)
		), array (
				'priority' => 950
		) );
		
		$this->add ( array (
				'name' => 'city',
				'attributes' => array (
						'type' => 'text'
				)
				,
				'options' => array (
						'label' => 'City'
				)
		), array (
				'priority' => 940
		) );
		
		$country = new CountryPicker ( 'country' );
		$country->setLabel ( 'Country' );
		$this->add ( $country, array (
				'priority' => 930
		));
		
		$state = new StatePicker( 'state' );
		$state->setLabel ( 'State' );
		$state->setCountry ( 'country' );
		$this->add ( $state, array (
				'priority' => 920
		) );
		
		$this->add ( array (
				'name' => 'postcode',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'Postcode'
				)
		), array (
				'priority' => 910
		) );
		
		$checkbox = new CheckboxHtmlText ( 'isDefault' );
		$checkbox->setHtmlText ( 'Make As Default Address' );
		$this->add ( $checkbox, array (
				'priority' => 900
		) );
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit'
				)
		), array (
				'priority' => - 100
		) );
		
		
	}
	
}