<?php

namespace User\Form\UserForgotPassword;

use Zend\Validator\Callback;
use User\Api\UserApi;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ForgotPasswordFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'email',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'EmailAddress' 
						),
						array (
								'name' => 'Callback',
								'options' => array (
										'callback' => array (
												$this,
												'checkEmailAddress' 
										),
										'messages' => array (
												Callback::INVALID_VALUE => "Email doesnot exist" 
										) 
								) 
						)
						 
				) 
		) );
	}
	public function checkEmailAddress($value) {
		if (UserApi::getUserByEmail ( $value )) {
			return true;
		}
		return false;
	}
} 