<?php
namespace User\Form\UserForgotPassword;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class ForgotPasswordForm extends Form 

{
	public function init() {
		
		$this->add ( array (
				'name' => 'email',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'E-Mail'
				)
		), array (
				'priority' => 1000
		) );
		
		$this->addCaptcha();
		$this->addCsrf();
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}