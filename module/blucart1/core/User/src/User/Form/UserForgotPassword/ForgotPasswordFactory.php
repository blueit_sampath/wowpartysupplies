<?php
namespace User\Form\UserForgotPassword;

use Common\Form\Option\AbstractFormFactory;

class ForgotPasswordFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserForgotPassword\ForgotPasswordForm' );
		$form->setName ( 'userForgotPassword' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserForgotPassword\ForgotPasswordFilter' );
	}
}
