<?php

namespace User\Form\AdminChangePassword;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class ChangePasswordForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'password',
				'attributes' => array (
						'type' => 'password' 
				),
				'options' => array (
						'label' => 'Password' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'confirmPassword',
				'attributes' => array (
						'type' => 'password' 
				),
				'options' => array (
						'label' => 'Re-enter Password' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}