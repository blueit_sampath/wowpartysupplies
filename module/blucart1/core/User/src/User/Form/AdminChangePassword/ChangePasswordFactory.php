<?php
namespace User\Form\AdminChangePassword;

use Common\Form\Option\AbstractFormFactory;

class ChangePasswordFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminChangePassword\ChangePasswordForm' );
		$form->setName ( 'adminChangePasswordAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminChangePassword\ChangePasswordFilter' );
	}
}
