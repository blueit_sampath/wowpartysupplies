<?php
namespace User\Form\AdminUserConfigAdminNotification;

use Common\Form\Option\AbstractFormFactory;

class UserConfigAdminNotificationFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigAdminNotification\UserConfigAdminNotificationForm' );
		$form->setName ( 'adminUserConfigAdminNotification' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigAdminNotification\UserConfigAdminNotificationFilter' );
	}
}
