<?php

namespace User\Form\AdminUserConfigAdminNotification;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UserConfigAdminNotificationFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'USER_ADMIN_NOTIFICATION_NOTIFY',
				'required' => false 
		) );
		$this->add ( array (
				'name' => 'USER_ADMIN_NOTIFICATION_SUBJECT',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'min' => 1,
										'max' => 255 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'USER_ADMIN_NOTIFICATION_BODYHTML',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				) 
		)
		 );
		
		$this->add ( array (
				'name' => 'USER_ADMIN_NOTIFICATION_BODYPLAIN',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'USER_NOTIFICATION_FROM_EMAIL',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'EmailAddress' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'USER_NOTIFICATION_FROM_NAME',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'min' => 1,
										'max' => 255 
								) 
						) 
				) 
		) );
	}
} 