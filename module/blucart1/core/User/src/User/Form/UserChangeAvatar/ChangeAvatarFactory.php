<?php
namespace User\Form\UserChangeAvatar;

use Common\Form\Option\AbstractFormFactory;

class ChangeAvatarFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserChangeAvatar\ChangeAvatarForm' );
		$form->setName ( 'userChangeAvatar' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserChangeAvatar\ChangeAvatarFilter' );
	}
}
