<?php

namespace User\Form\UserChangeAvatar;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use Zend\Form\Element\File;
use File\Form\Element\FileUpload;


class ChangeAvatarForm extends Form 

{
	public function init() {
		
		
		$file = new FileUpload('avatar');
		$file->setMaxNumberOfFiles(1);
		$file->setButtonText('Select Avatar');
	
		$this->add ($file, array (
				'priority' => 1000
		) );
// 		$myFile = new File( 'avatar' );
// 		$myFile->setOptions ( array () );
// 		$this->add ( $myFile );
		
		
		
		
		
		$this->add ( array (
				'name' => 'btnsubmit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Change Avatar' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}