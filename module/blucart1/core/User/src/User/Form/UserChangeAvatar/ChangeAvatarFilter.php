<?php
namespace User\Form\UserChangeAvatar;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  ChangeAvatarFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'avatar',
				'required' => false,
				
		) );
		
	}
} 