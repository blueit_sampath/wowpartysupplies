<?php

namespace User\Form\AdminUserConfigPasswordRecovery;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UserConfigPasswordRecoveryFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'USER_PASSWORD_RECOVERY_SUBJECT',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'min' => 1,
										'max' => 255 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'USER_PASSWORD_RECOVERY_BODYHTML',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				)
				 
		) );
		
		$this->add ( array (
				'name' => 'USER_PASSWORD_RECOVERY_BODYPLAIN',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				) 
		) );
	}
} 