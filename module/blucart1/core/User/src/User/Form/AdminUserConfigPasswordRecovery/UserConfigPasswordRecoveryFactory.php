<?php
namespace User\Form\AdminUserConfigPasswordRecovery;

use Common\Form\Option\AbstractFormFactory;

class UserConfigPasswordRecoveryFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryForm' );
		$form->setName ( 'adminUserConfigPasswordRecovery' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryFilter' );
	}
}
