<?php
namespace User\Form\AdminUserConfigRegistration;

use Common\Form\Option\AbstractFormFactory;

class UserConfigRegistrationFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigRegistration\UserConfigRegistrationForm' );
		$form->setName ( 'adminUserConfigRegistration' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigRegistration\UserConfigRegistrationFilter' );
	}
}
