<?php
namespace User\Form\UserLogin;

use Common\Form\Option\AbstractFormFactory;

class LoginFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserLogin\LoginForm' );
		$form->setName ( 'userLogin' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserLogin\LoginFilter' );
	}
}
