<?php
namespace User\Form\UserChangePassword;

use Common\Form\Option\AbstractFormFactory;

class ChangePasswordFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserChangePassword\ChangePasswordForm' );
		$form->setName ( 'userChangePassword' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserChangePassword\ChangePasswordFilter' );
	}
}
