<?php

namespace User\Form\UserChangePassword;


use Zend\InputFilter\InputFilter;


class ChangePasswordFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'password',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 6,
										'max' => 255 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'confirmPassword',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 6,
										'max' => 255 
								) 
						),
						array (
								'name' => 'Identical',
								'options' => array (
										
										'token' => 'password' 
								) 
						) 
				) 
		) );
	}
} 