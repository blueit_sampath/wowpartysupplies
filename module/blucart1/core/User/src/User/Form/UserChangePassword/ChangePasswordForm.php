<?php

namespace User\Form\UserChangePassword;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class ChangePasswordForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'password',
				'attributes' => array (
						'type' => 'password' 
				),
				'options' => array (
						'label' => 'Password' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'confirmPassword',
				'attributes' => array (
						'type' => 'password' 
				),
				'options' => array (
						'label' => 'Re-Type Password' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Save' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}