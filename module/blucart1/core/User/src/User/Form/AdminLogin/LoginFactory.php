<?php
namespace User\Form\AdminLogin;

use Common\Form\Option\AbstractFormFactory;

class LoginFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminLogin\LoginForm' );
		$form->setName ( 'adminLogin' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminLogin\LoginFilter' );
	}
}
