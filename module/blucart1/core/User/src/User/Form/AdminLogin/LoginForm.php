<?php

namespace User\Form\AdminLogin;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class LoginForm extends Form 

{
	public function init() {
		$this->add ( array (
				'name' => 'email',
				'attributes' => array (
						'type' => 'email',
				),
				'options' => array (
						'label' => 'Email Address' 
				) 
		) );
		$this->add ( array (
				'name' => 'password',
				'attributes' => array (
						'type' => 'password',
				),
				'options' => array (
						'label' => 'Password' 
				) 
		) );
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'class' => 'button',
						'type' => 'submit',
						'value' => 'Login' 
				) 
		)
		 );
	}
}