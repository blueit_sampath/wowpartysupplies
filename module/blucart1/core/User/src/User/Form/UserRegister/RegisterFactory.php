<?php
namespace User\Form\UserRegister;

use Common\Form\Option\AbstractFormFactory;

class RegisterFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserRegister\RegisterForm' );
		$form->setName ( 'userRegister' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserRegister\RegisterFilter' );
	}
}
