<?php

namespace User\Form\UserRegister;

use User\Api\UserApi;

use Zend\Validator\Callback;

use Config\Api\ConfigApi;
use UserRole\Api\UserRoleApi;
use Zend\Validator\Regex;
use Zend\Validator\Hostname;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class RegisterFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'firstName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => "/^[a-zA-Z ,.'-]+$/",
										'messages' => array (
												Regex::NOT_MATCH => 'Use Letters & periods' 
										) 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'lastName',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						),
						array (
								'name' => 'StripTags' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => "/^[a-zA-Z ,.'-]+$/",
										'messages' => array (
												Regex::NOT_MATCH => 'Use Letters & periods' 
										) 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'email',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'EmailAddress' 
						),
						array (
								'name' => 'Callback',
								'options' => array (
										'callback' => array (
												$this,
												'checkEmailAddress'
										),
										'messages' => array (
												Callback::INVALID_VALUE => "Email already exists",
													
										)
								)
						)
				) 
		) );
		
		$this->add ( array (
				'name' => 'password',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 6,
										'max' => 255 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'confirmPassword',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 6,
										'max' => 255 
								) 
						),
						array (
								'name' => 'Identical',
								'options' => array (
										
										'token' => 'password' 
								) 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'contactNumber',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'regex',
								'options' => array (
										'pattern' => '/^(?!.*-.*-.*-)(?=(?:\d{8,13}$)|(?:(?=.{9,13}$)[^-]*-[^-]*$)|(?:(?=.{10,13}$)[^-]*-[^-]*-[^-]*$)  )[\d-]+$/',
										'messages' => array (
												Regex::NOT_MATCH => 'Invalid Phone number' 
										) 
								) 
						) 
				) 
		) );
		
		$roles = UserRoleApi::getRolesAtRegistration ();
		if (count ( $roles ) > 1) {
			$this->add ( array (
					'name' => 'userRole',
					'required' => true 
			) );
		}
		$tc = ConfigApi::getConfigByKey ( 'USER_TC_TEXT' );
		if ($tc) {
			$this->add ( array (
					'name' => 'agree',
					'required' => true 
			) );
		}
	}
	public function checkEmailAddress($value) {
		
		if (UserApi::getUserByEmail ( $value )) {
			return false;
		}
		return true;
	}
} 