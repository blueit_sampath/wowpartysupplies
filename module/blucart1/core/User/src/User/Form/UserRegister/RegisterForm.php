<?php

namespace User\Form\UserRegister;

use Zend\Form\Element\Hidden;
use Zend\Form\Element\Radio;
use UserRole\Api\UserRoleApi;
use LocaleElement\Form\Element\CheckboxHtmlText;
use Zend\Form\Element\MultiCheckbox;
use Config\Api\ConfigApi;
use LocaleElement\Form\Element\StatePicker;
use LocaleElement\Form\Element\CountryPicker;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class RegisterForm extends Form {

    public function init() {
        $this->add(array(
            'name' => 'firstName',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'First Name'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'lastName',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Last Name'
            )
                ), array(
            'priority' => 990
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'E-Mail'
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password'
            ),
            'options' => array(
                'label' => 'Password'
            )
                ), array(
            'priority' => 970
        ));

        $this->add(array(
            'name' => 'confirmPassword',
            'attributes' => array(
                'type' => 'password'
            ),
            'options' => array(
                'label' => 'Re-Type Password'
            )
                ), array(
            'priority' => 960
        ));

        $this->add(array(
            'name' => 'contactNumber',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Phone Number'
            )
                ), array(
            'priority' => 950
        ));

        $this->addUserRoles(940);
        $this->addCaptcha();
        $this->addCsrf();

        // $country = new CountryPicker('country');
        // $country->setLabel('Country');
        // $country->setValue(ConfigApi::getCountryCodeByCountryName());
        // $this->add($country, array('priority' => 940));
        // $state = new StatePicker('state');
        // $state->setLabel('State');
        // $state->setCountry('country');
        // $this->add($state, array('priority' => 930));

        $tc = ConfigApi::getConfigByKey('USER_TC_TEXT');
        if ($tc) {
            $checkbox = new CheckboxHtmlText('agree');
            $checkbox->setHtmlText($tc);
            $this->add($checkbox, array(
                'priority' => 50
            ));
        }
        $this->add(array(
            'name' => 'redirectUrl',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Register'
            )
                ), array(
            'priority' => - 100
        ));
    }

    protected function addUserRoles($priority) {
        $roles = UserRoleApi::getRolesAtRegistration();

        $array = array();
        foreach ($roles as $role) {
            $array [$role->id] = $role->title;
        }

        if (count($array) > 1) {

            $radio = new Radio('userRole');
            $radio->setLabel('Role');
            $radio->setValueOptions($array);
            $this->add($radio, array(
                'priority' => $priority
            ));
        }
    }

}
