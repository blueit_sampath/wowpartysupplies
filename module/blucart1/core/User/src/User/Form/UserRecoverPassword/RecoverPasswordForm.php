<?php
namespace User\Form\UserRecoverPassword;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;


class RecoverPasswordForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'password',
				'attributes' => array (
						'type' => 'password'
				),
				'options' => array (
						'label' => 'Password'
				)
		), array (
				'priority' => 1000
		) );
		
		$this->add ( array (
				'name' => 'confirmPassword',
				'attributes' => array (
						'type' => 'password'
				),
				'options' => array (
						'label' => 'Re-Type Password'
				)
		), array (
				'priority' => 990
		) );
		
		$this->addCaptcha();
		$this->addCsrf();
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}