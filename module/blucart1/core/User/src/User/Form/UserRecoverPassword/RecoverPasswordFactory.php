<?php
namespace User\Form\UserRecoverPassword;

use Common\Form\Option\AbstractFormFactory;

class RecoverPasswordFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserRecoverPassword\RecoverPasswordForm' );
		$form->setName ( 'userRecoverPassword' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserRecoverPassword\RecoverPasswordFilter' );
	}
}
