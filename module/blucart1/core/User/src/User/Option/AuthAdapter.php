<?php

namespace User\Option;

use User\Api\UserApi;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class AuthAdapter implements AdapterInterface {
	const NOT_FOUND_MESSAGE = "Invalid Email or Password";
	protected $_email;
	protected $_password;
	protected $_isFront = true;
	protected $user;
	
	/**
	 * Sets email and password for authentication
	 *
	 * @return void
	 */
	public function __construct() {
	}
	
	/**
	 * Performs an authentication attempt
	 *
	 * @return \Zend\Authentication\Result
	 * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If
	 *         authentication cannot be performed
	 */
	public function authenticate() {
		try {
			
			$this->user = UserApi::authenticate ( $this->getEmail (), $this->getPassword (), $this->getIsFront () );
			if (! $this->user) {
				return $this->result ( Result::FAILURE_CREDENTIAL_INVALID, self::NOT_FOUND_MESSAGE );
			}
		} catch ( \Exception $e ) {
			
			return $this->result ( Result::FAILURE_CREDENTIAL_INVALID, self::NOT_FOUND_MESSAGE );
		}
		return $this->result ( Result::SUCCESS );
	}
	private function result($code, $messages = array()) {
		if (! is_array ( $messages )) {
			$messages = array (
					$messages 
			);
		}
		
		return new Result ( $code, $this->user, $messages );
	}
	/**
	 *
	 * @return string $_email
	 */
	public function getEmail() {
		return $this->_email;
	}
	
	/**
	 *
	 * @return string $_password
	 */
	public function getPassword() {
		return $this->_password;
	}
	
	/**
	 *
	 * @return string $_isFront
	 */
	public function getIsFront() {
		return $this->_isFront;
	}
	
	/**
	 *
	 * @return string $user
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 *
	 * @param field_type $_email        	
	 * @param
	 *        	class_name
	 */
	public function setEmail($_email) {
		$this->_email = $_email;
		return $this;
	}
	
	/**
	 *
	 * @param field_type $_password        	
	 * @param
	 *        	class_name
	 */
	public function setPassword($_password) {
		$this->_password = $_password;
		return $this;
	}
	
	/**
	 *
	 * @param boolean $_isFront        	
	 * @param
	 *        	class_name
	 */
	public function setIsFront($_isFront) {
		$this->_isFront = $_isFront;
		return $this;
	}
	
	/**
	 *
	 * @param field_type $user        	
	 * @param
	 *        	class_name
	 */
	public function setUser($user) {
		$this->user = $user;
		return $this;
	}
}