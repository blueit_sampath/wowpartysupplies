<?php

namespace User\Api;

use Zend\Authentication\AuthenticationService;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Common\Api\Api;

class UserApi extends Api {

    protected static $_entity = '\User\Entity\User';
    protected static $_entityForgotPassword = '\User\Entity\UserForgotPassword';
    protected static $_entityAddress = '\User\Entity\UserAddress';

    public static function getEncryptedPassword($password) {
        return md5($password);
    }

    public static function getUsers() {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('user.id', 'userId');
        $item = $queryBuilder->addFrom(static::$_entity, 'user');
        return $queryBuilder->executeQuery();
    }

    public static function getUserById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getUserByEmail($email) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
                    'email' => $email
        ));
    }

    public static function getAddressById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_entityAddress, $id);
    }

    public static function getAddressByUserId($userId) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entityAddress)->findBy(array(
                    'user' => $userId
        ));
    }

    public static function removeAddressById($id) {
        $em = static::getEntityManager();
        $result = static::getAddressById($id);
        if ($result) {
            $em->remove($result);
            $em->flush();
        }
        return true;
    }

    public static function getDefaultAddressByUserId($userId) {
        $em = static::getEntityManager();
        $result = $em->getRepository(static::$_entityAddress)->findOneBy(array(
            'user' => $userId,
            'isDefault' => 1
                ), array('id' => 'asc'));
        if ($result) {
            return $result;
        }
        $result = $em->getRepository(static::$_entityAddress)->findOneBy(array(
            'user' => $userId
                ), array('id' => 'asc'));
        return $result;
    }

    public static function updateDefaultAddressById($addressId, $userId) {
        $em = static::getEntityManager();
        $dql = 'update ' . static::$_entityAddress . ' u SET u.isDefault = 0 WHERE u.user = ' . $userId . ' ';
        $em->createQuery($dql)->getResult();

        $result = static::getAddressById($addressId);
        if ($result) {
            $result->isDefault = 1;
            $em->persist($result);
            $em->flush();
        }

        return $result;
    }

    public static function getUserForgotPasswordByEmail($email) {
        $em = static::getEntityManager();
        $user = static::getUserByEmail($email);
        if ($user) {
            return $em->getRepository(static::$_entityForgotPassword)->findOneBy(array(
                        'user' => $user->id
            ));
        }
        return false;
    }

    public static function getUserForgotPasswordBySecret($secret) {
        $em = static::getEntityManager();

        return $em->getRepository(static::$_entityForgotPassword)->findOneBy(array(
                    'secret' => $secret
        ));
    }

    public static function getUserForgotPasswordById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_entityForgotPassword, $id);
    }

    public static function removeUserForgotPasswordBySecret($secret) {
        $em = static::getEntityManager();

        $entity = static::getUserForgotPasswordBySecret($secret);
        if ($entity) {
            $em->remove($entity);
            $em->flush();
            return true;
        }
        return false;
    }

    public static function authenticate($email, $password, $isFront = true) {
        $password = md5($password);

        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('user.id', 'userId');
        $item = $queryBuilder->addFrom(static::$_entity, 'user');
        $joinItem = new QueryJoinItem('user.userRole', 'userRole');
        $item->addJoin($joinItem);

        $queryBuilder->addWhere('user.email', 'userEmail');
        $queryBuilder->setParameter('userEmail', $email);

        $queryBuilder->addWhere('user.password', 'userPassword');
        $queryBuilder->setParameter('userPassword', $password);

        $queryBuilder->addWhere('userRole.accessAdmin', 'userRoleAccessAdmin');
        $queryBuilder->setParameter('userRoleAccessAdmin', !$isFront);

        $queryBuilder->addWhere('user.status', 'userStatus');
        $queryBuilder->setParameter('userStatus', 1);

        $results = $queryBuilder->executeQuery();

        if (count($results)) {
            $result = $results[0];
            return $result['userId'];
        }
        return false;
    }

    public static function isAdminUser($userId = null) {
        if(!$userId){
        $user = static::getLoggedInUser();
        } else {
            $user = static::getUserById($userId);
        }
        if (!$user) {
            return false;
        }
        if ($user->userRole && $user->userRole->accessAdmin) {
            return $user;
        }

        return false;
    }

    public static function getLoggedInUser() {
        $auth = new AuthenticationService();

        if ($auth->hasIdentity()) {
            $id = $auth->getIdentity();
            return static::getUserById($id);
        }
        return false;
    }

    public static function setLoggedInUser($id) {
        $auth = new AuthenticationService();
        $auth->getStorage()->write($id);
        return $id;
    }

    public static function saveAddress($address = array(), $userId = null) {
        $em = static::getEntityManager();
        $entity = null;
        if (isset($address['id']) && $address['id']) {
            $entity = static::getAddressById($address['id']);
            unset($address['id']);
        }
        if (!$entity) {
            $entity = new static::$_entityAddress();
        }
        foreach ($address as $key => $value) {
            $entity->$key = $value;
        }
        $user = UserApi::getUserById($userId);
        if ($user) {
            $entity->user = $user;
            if (!static::getDefaultAddressByUserId($user->id)) {
                $params['isDefault'] = 1;
            }
        }

        $em->persist($entity);
        $em->flush();
        if (isset($params['isDefault']) && $params['isDefault']) {
            $entity = static::updateDefaultAddressById($entity->id, $userId);
        }

        return $entity;
    }

}
