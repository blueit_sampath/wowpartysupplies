<?php

namespace User\Api;

use Common\Api\Api;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;

class UserReportApi extends Api {
	protected static $_entity = '\User\Entity\User';
	public static function getAllUsersCount($status, $startDate, $endDate) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'count(user.id)', 'countUserId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'user.status', 'userStatus' );
			$queryBuilder->addParameter ( 'userStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'user' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		// $item = $queryBuilder->addWhere ( 'user.id', 'userId' );
		// $queryBuilder->addParameter ( 'userId', $userId );
		return $queryBuilder->executeSingleScalarQuery ( 'count' );
	}
	public static function getAllUserCountByDay($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(user.createdDate, '%D %M,%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(user.id)', 'countUserId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'user.status', 'userStatus' );
			$queryBuilder->addParameter ( 'userStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'user' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getAllUserCountByWeek($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(user.createdDate, '%v,%x')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(user.id)', 'countUserId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'user.status', 'userStatus' );
			$queryBuilder->addParameter ( 'userStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'user' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getAllUserCountByMonth($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(user.createdDate, '%M,%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(user.id)', 'countUserId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'user.status', 'userStatus' );
			$queryBuilder->addParameter ( 'userStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'user' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getAllUserCountByYear($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(user.createdDate, '%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(user.id)', 'countUserId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'user.status', 'userStatus' );
			$queryBuilder->addParameter ( 'userStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'user' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	public static function getRecentUsers($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'user.id', 'userId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'user.status', 'userStatus' );
			$queryBuilder->addParameter ( 'userStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'user' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$endDate = date('Y-m-d',strtotime('+1 day',strtotime($endDate)));
			$queryBuilder->addWhere ( 'user.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addOrder ( 'user.id', 'userId', 'desc' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
}
