<?php
namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User
{

    /**
     *
     * @var integer @ORM\Column(name="id", type="integer", nullable=false)
     *      @ORM\Id
     *      @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var string @ORM\Column(name="first_name", type="string", length=255,
     *      nullable=true)
     */
    private $firstName;

    /**
     *
     * @var string @ORM\Column(name="last_name", type="string", length=255,
     *      nullable=true)
     */
    private $lastName;

    /**
     *
     * @var string @ORM\Column(name="email", type="string", length=255,
     *      nullable=true)
     */
    private $email;

    /**
     *
     * @var string @ORM\Column(name="password", type="string", length=255,
     *      nullable=true)
     */
    private $password;

    /**
     *
     * @var string @ORM\Column(name="contact_number", type="string", length=255,
     *      nullable=true)
     */
    private $contactNumber;

    /**
     *
     * @var boolean @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     *
     * @var \File @ORM\ManyToOne(targetEntity="\File\Entity\File")
     *      @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="avatar_id", referencedColumnName="id",nullable=true,onDelete="SET NULL")
     *      })
     */
    private $avatar;

    /**
     *
     * @var \UserRole @ORM\ManyToOne(targetEntity="\UserRole\Entity\UserRole")
     *      @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="user_role_id", referencedColumnName="id",nullable=true,onDelete="SET NULL")
     *      })
     */
    private $userRole;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="create")
     */
    private $createdDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="update")
     */
    private $updatedDate;

    public function __get($property)
    {
        if (\method_exists($this, "get" .\ucfirst($property))) {
            $method_name = "get" .\ucfirst($property);
            return $this->$method_name();
        } else {
            
            if (is_object($this->$property)) {
                // return $this->$property->id;
            }
            return $this->$property;
        }
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property            
     * @param mixed $value            
     *
     */
    public function __set($property, $value)
    {
        if (\method_exists($this, "set" . \ucfirst($property))) {
            $method_name = "set" . \ucfirst($property);
            
            $this->$method_name($value);
        } else
            $this->$property = $value;
    }

    public function __isset($name)
    {
        return isset($this->$name);
    }
}
