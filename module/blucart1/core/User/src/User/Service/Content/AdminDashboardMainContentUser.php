<?php 
namespace User\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardMainContentUser extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-main-content-user';
	protected $_contentName = 'adminDashboardMainContentUser';
	public function getEventName() {
		return 'adminDashboardMainContent';
	}
	public function getPriority() {
		return 900;
	}
	
}

