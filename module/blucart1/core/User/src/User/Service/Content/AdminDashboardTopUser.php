<?php 
namespace User\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardTopUser extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-top-user';
	protected $_contentName = 'adminDashboardTopUser';
	public function getEventName() {
		return 'adminDashboardTopStastics';
	}
	public function getPriority() {
		return 980;
	}
	
}

