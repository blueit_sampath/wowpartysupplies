<?php 
namespace User\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class UserAddress extends AbstractContentEvent {

	protected $_contentTemplate = 'common/user-address';
	protected $_contentName = 'userAddress';
	public function getEventName() {
		return 'userDashboard';
	}
	public function getPriority() {
		return 1000;
	}
	
}

