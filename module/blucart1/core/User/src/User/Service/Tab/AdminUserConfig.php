<?php 
namespace User\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminUserConfig extends AbstractTabEvent {
	public function getEventName() {
		return 'adminUserConfig';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-user-config-general');
		$tabContainer->add ( 'admin-user-config-general', 'General', $u,1000 );
		
		$u = $url ( 'admin-user-config-registration');
		$tabContainer->add ( 'admin-user-config-registration', 'Registration', $u,990 );
		
		$u = $url ( 'admin-user-config-password-recovery');
		$tabContainer->add ( 'admin-user-config-password-recovery', 'Password Recovery', $u,980 );
		
		$u = $url ( 'admin-user-config-admin-notification');
		$tabContainer->add ( 'admin-user-config-admin-notification', 'Admin Notification', $u,970 );
		
		return $this;
	}
}

