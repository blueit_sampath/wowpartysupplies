<?php

namespace User\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminUser extends AbstractTabEvent {
	public function getEventName() {
		return 'adminUserAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$userId = Functions::fromRoute ( 'userId' );
		if (! $userId) {
			return;
		}
		$u = $url ( 'admin-user-add', array (
				'userId' => $userId 
		) );
		$tabContainer->add ( 'admin-user-add', 'General Information', $u, 1000 );
		
		$u = $url ( 'admin-user-address', array (
				'userId' => $userId
		) );
		$tabContainer->add ( 'admin-user-address', 'Addresses', $u, 900 );
		
		return $this;
	}
}

