<?php 
namespace User\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminChangePassword extends AbstractTabEvent {
	public function getEventName() {
		return 'adminUserAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$userId = Functions::fromRoute('userId');
		if(!$userId){
			return;
		}
		$u = $url ( 'admin-user-changepassword', array(
				'userId' => $userId
				));
		$tabContainer->add ( 'admin-user-changepassword', 'Change Password', $u,900 );
		
		return $this;
	}
}

