<?php
namespace User\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use Core\Functions;

class AdminUserAddress extends AbstractMainBlucartGridEvent
{

    protected $_columnKeys = array(
        'id',
        'firstName',
        'lastName',
        'email',
        'addressLine1',
        'addressLine2',
        'city',
        'state',
        'country',
        'contactNumber',
        'isDefault'
    );
    protected $_entity = '\User\Entity\UserAddress';
    protected $_entityName = 'userAddress';

    public function getEventName()
    {
        return 'adminUserAddress';
    }

    public function preSchema($e)
    {
        parent::preSchema($e);
        
        $userId = Functions::fromRoute ( 'userId', 0 );
        
        $array = array ();
        
        $grid = $this->getGrid ();
        $grid->setAdditionalParameters ( array (
        		'userId' => $userId
        ) );
        
        $array = array();
        
        $grid = $this->getGrid();
        $columns = $grid->getColumns();
        
        $columnItem = new ColumnItem();
        $columnItem->setField('id');
        $columnItem->setTitle('ID');
        $columnItem->setType('number');
        $columnItem->setEditable(false);
        $columnItem->setWeight(1000);
        $columnItem->setIsPrimary(true);
        $columns->addColumn('userAddress.id', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('firstName');
        $columnItem->setTitle('First Name');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(990);
        $columns->addColumn('userAddress.firstName', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('lastName');
        $columnItem->setTitle('Last Name');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('userAddress.lastName', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('email');
        $columnItem->setTitle('Email');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(970);
        $columns->addColumn('userAddress.email', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('addressLine1');
        $columnItem->setTitle('Address Line1');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columns->addColumn('userAddress.addressLine1', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('addressLine2');
        $columnItem->setTitle('Address Line2');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(950);
        $columns->addColumn('userAddress.addressLine2', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('city');
        $columnItem->setTitle('City');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(940);
        $columns->addColumn('userAddress.city', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('state');
        $columnItem->setTitle('State');
        $columnItem->setType('string');
        $columnItem->setEditable(false);
        $columnItem->setWeight(930);
        $columns->addColumn('userAddress.state', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('country');
        $columnItem->setTitle('Country');
        $columnItem->setType('string');
        $columnItem->setEditable(false);
        $columnItem->setWeight(920);
        $columns->addColumn('userAddress.country', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('contactNumber');
        $columnItem->setTitle('Contact Number');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(910);
        $columns->addColumn('userAddress.contactNumber', $columnItem);
        
        $columnItem = new ColumnItem();
        $columnItem->setField('isDefault');
        $columnItem->setTitle('Is Default');
        $columnItem->setType('boolean');
        $columnItem->setEditable(true);
        $columnItem->setWeight(910);
        $columns->addColumn('userAddress.isDefault', $columnItem);
        
        $this->formToolbar();
        
        return $columns;
    }

    public function formToolbar()
    {
        $grid = $this->getGrid();
        
        $toolbar = $grid->getToolbar();
        if (! $toolbar) {
            $toolbar = new Toolbar();
            $grid->setToolbar($toolbar);
        }
        
        $toolBarItem = new ToolbarItem();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(1000);
        $toolbar->addToolbar('save', $toolBarItem);
        
        $toolBarItem = new ToolbarItem();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('cancel', $toolBarItem);
        
        $toolBarItem = new ToolbarItem();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }
    
    public function preRead($e) {
    	parent::preRead ( $e );
    	$userId = Functions::fromQuery ( 'userId', 0 );
    	$grid = $this->getGrid ();
    	$queryBuilder = $this->getGrid ()->getQueryBuilder ();
    	$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
    	
    	$item = $queryBuilder->addWhere ( 'userAddress.user', 'userId' );
    	$queryBuilder->addParameter ( 'userId', $userId );
    
    	return true;
    }
}
