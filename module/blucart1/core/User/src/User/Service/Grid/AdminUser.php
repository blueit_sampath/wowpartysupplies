<?php

namespace User\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;

class AdminUser extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'firstName',
			'lastName',
			'email',
			'contactNumber',
			'status' 
	);
	protected $_entity = '\User\Entity\User';
	protected $_entityName = 'user';
	

	public function getEventName() {
		return 'adminUser';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'USER ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'user.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'firstName' );
		$columnItem->setTitle ( 'First Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'user.firstName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'lastName' );
		$columnItem->setTitle ( 'Last Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'user.lastName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'email' );
		$columnItem->setTitle ( 'Email' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'user.email', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'contactNumber' );
		$columnItem->setTitle ( 'Contact Number' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'user.contactNumber', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'userRoleTitle' );
		$columnItem->setTitle ( 'Role' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 950 );
		$columns->addColumn ( 'userRole.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 940 );
		$columns->addColumn ( 'user.status', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'createdDate' );
		$columnItem->setTitle ( 'Registered Date' );
		$columnItem->setType ( 'date' );
		$columnItem->setEditable ( false );
		$columnItem->setTemplate('#= kendo.toString(createdDate,"g") #');
		$columnItem->setWeight ( 930 );
		// $columns->addColumn ( 'DATE(user.createdDate)', $columnItem );
		$columns->addColumn ( 'user.createdDate', $columnItem );
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$fromItem = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem ( 'user.userRole', 'userRole' );
		$fromItem->addJoin ( $joinItem );
		$queryBuilder->addGroup('user.id','userId');
		return true;
	}
}
