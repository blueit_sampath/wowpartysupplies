<?php 
namespace User\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminUser extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminUser';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-user-add');
		$item = $linkContainer->add ( 'admin-user-add', 'Add Customer', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

