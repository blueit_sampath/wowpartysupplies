<?php

namespace User\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminUserView extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminUserView';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$userId = Functions::fromRoute ( 'userId' );
		$u = $url ( 'admin-user-add', array (
				'userId' => $userId 
		) );
		$item = $linkContainer->add ( 'admin-user-add', 'Edit User', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		$item->setImageClass('splashy-contact_blue_edit');
		$u = $url ( 'admin-user-changepassword', array (
				'userId' => $userId
		) );
		$item = $linkContainer->add ( 'admin-user-changepassword', 'Change Password', $u, 1000 );
		$item->setImageClass('splashy-lock_large_locked');
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

