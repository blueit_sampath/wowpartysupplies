<?php 
namespace User\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminUserAddress extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminUserAddress';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$userId = Functions::fromRoute('userId');
		$u = $url ( 'admin-user-address-add', array(
			'userId' => $userId
		));
		$item = $linkContainer->add ( 'admin-user-address-add', 'Add Address', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

