<?php 
namespace User\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardSimpleUserStat extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-simple-user-stat';

	public function getBlockName() {
		return 'adminDashboardSimpleUserStat';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'adminDashboardSimpleUserStat';
	}
	
}

