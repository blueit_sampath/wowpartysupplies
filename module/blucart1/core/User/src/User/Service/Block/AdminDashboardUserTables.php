<?php 
namespace User\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardUserTables extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-user-tables';

	public function getBlockName() {
		return 'AdminDashboardUserTables';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardUserTables';
	}
	
}

