<?php 
namespace User\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardUserGraph extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-user-graph';

	public function getBlockName() {
		return 'AdminDashboardUserGraph';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardUserGraph';
	}
	
}

