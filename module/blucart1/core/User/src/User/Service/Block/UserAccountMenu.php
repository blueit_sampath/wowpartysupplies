<?php 
namespace User\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class UserAccountMenu extends AbstractBlockEvent {

	protected $_blockTemplate = 'user-account-menu';

	public function getBlockName() {
		return 'userAccountMenu';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'User Menu';
	}
	
	
	
}

