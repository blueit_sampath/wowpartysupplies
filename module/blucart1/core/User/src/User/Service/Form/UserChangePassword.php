<?php

namespace User\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;

class UserChangePassword extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'firstName',
			'lastName',
			'contactNumber' 
	);
	protected $_entity = '\User\Entity\User';
	protected $_entityName = 'user';
	public function getFormName() {
		return 'userChangePassword';
	}
	public function getPriority() {
		return 1000;
	}
	public function getId() {
		return UserApi::getLoggedInUser ()->id;
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
	}
	public function parseSaveParams($params) {
		$params ['id'] = $this->getId ();
		return $params;
	}
	public function parseSaveEntity($params, $entity) {
		if (isset ( $params ['password'] ) && $params ['password'] !== "") {
			$entity->password = UserApi::getEncryptedPassword ( $params ['password'] );
		}
	}
	public function afterSaveEntity($entity) {
	}
}
