<?php

namespace User\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminUserConfigAdminNotification extends AdminConfig {
	protected $_columnKeys = array (
			'USER_NOTIFICATION_FROM_EMAIL',
			'USER_NOTIFICATION_FROM_NAME',
			'USER_ADMIN_NOTIFICATION_NOTIFY',
			'USER_ADMIN_NOTIFICATION_SUBJECT',
			'USER_ADMIN_NOTIFICATION_BODYHTML',
			'USER_ADMIN_NOTIFICATION_BODYPLAIN' 
	);
	public function getFormName() {
		return 'adminUserConfigAdminNotification';
	}
	public function getPriority() {
		return 1000;
	}
}
