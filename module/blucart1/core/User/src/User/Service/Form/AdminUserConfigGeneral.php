<?php

namespace User\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminUserConfigGeneral extends AdminConfig {
	protected $_columnKeys = array (
			'USER_FROM_EMAIL',
			'USER_FROM_NAME',
			'USER_TC_TEXT',
			
	);
	public function getFormName() {
		return 'adminUserConfigGeneral';
	}
	public function getPriority() {
		return 1000;
	}
}
