<?php
namespace User\Service\Form;

use Common\Form\Option\AbstractFrontMainFormEvent;
use UserRole\Api\UserRoleApi;
use User\Api\UserApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class UserRegister extends AbstractFrontMainFormEvent
{

    protected $_columnKeys = array(
        'id',
        'firstName',
        'lastName',
        'email',
        'contactNumber',
        'status'
    )
    ;

    protected $_entity = '\User\Entity\User';

    protected $_entityName = 'user';

    public function getFormName()
    {
        return 'userRegister';
    }

    public function getPriority()
    {
        return 1000;
    }

    public function parseSaveEntity($params, $entity)
    {
        if (isset($params['password']) && $params['password'] !== "") {
            $entity->password = UserApi::getEncryptedPassword($params['password']);
        }
        
        if (isset($params['userRole'])) {
            $entity->userRole = UserRoleApi::getRoleById($params['userRole']);
        } else {
            $userRoles = UserRoleApi::getRolesAtRegistration();
            if ($userRoles) {
                $entity->userRole = array_shift($userRoles);
            }
        }
        $entity->status = 1;
    }

    public function afterSaveEntity($entity)
    {
        UserApi::setLoggedInUser($entity->id);
    }
}
