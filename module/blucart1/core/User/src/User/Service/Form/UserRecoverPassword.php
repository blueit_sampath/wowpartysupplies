<?php 
namespace User\Service\Form;

use User\Api\UserApi;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class UserRecoverPassword extends AbstractMainFormEvent {
	protected $_columnKeys = array (
				);
	protected $_entity = '';
	protected $_entityName = '';
	public function getFormName() {
		return 'userRecoverPassword';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
	
		$secret = Functions::fromRoute('secret','');
		$entity = UserApi::getUserForgotPasswordBySecret ( $secret );
		if ( $entity) {
			$user = $entity->user;
			$user->password = UserApi::getEncryptedPassword($params['password']);
			$em->persist ( $user );
			$em->flush ();
			$em->remove($entity);
			$em->flush();
		}
		$resultContainer = $this->getFormResultContainer ();
		$resultContainer->add ( $this->_entityName, $entity );
		return true;
	}
	
}
