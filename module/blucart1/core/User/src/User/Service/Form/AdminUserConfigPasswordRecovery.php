<?php

namespace User\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminUserConfigPasswordRecovery extends AdminConfig {
	protected $_columnKeys = array (
			'USER_PASSWORD_RECOVERY_SUBJECT',
			'USER_PASSWORD_RECOVERY_BODYHTML',
			'USER_PASSWORD_RECOVERY_BODYPLAIN' 
	);
	public function getFormName() {
		return 'adminUserConfigPasswordRecovery';
	}
	public function getPriority() {
		return 1000;
	}
}
