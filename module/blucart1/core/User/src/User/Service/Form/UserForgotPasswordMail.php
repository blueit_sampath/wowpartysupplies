<?php

namespace User\Service\Form;

use Common\Option\Token\TokenContainer;
use Config\Api\ConfigApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class UserForgotPasswordMail extends AbstractMainFormEvent {
	public function getFormName() {
		return 'userForgotPassword';
	}
	public function getPriority() {
		return null;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		
		$resultContainer = $this->getFormResultContainer ();
		$userForgotPassword = $resultContainer->get ( 'userForgotPassword' )->getValue ();
		
		if ($userForgotPassword) {
			
			$this->sendEmail ( $userForgotPassword );
		}
		return true;
	}
	
	/**
	 *
	 * @return \Common\Option\Token\TokenContainer
	 */
	public function sendEmail($userForgotPassword) {
		$subject = ConfigApi::getConfigByKey ( 'USER_PASSWORD_RECOVERY_SUBJECT' );
		$bodyHtml = ConfigApi::getConfigByKey ( 'USER_PASSWORD_RECOVERY_BODYHTML' );
		$bodyPlain = ConfigApi::getConfigByKey ( 'USER_PASSWORD_RECOVERY_BODYPLAIN' );
		
		$fromName = ConfigApi::getConfigByKey ( 'USER_FROM_NAME', '' );
		$fromEmail = ConfigApi::getConfigByKey ( 'USER_FROM_EMAIL' );
		
		if (! $fromEmail) {
			return;
		}
		
		if ($subject && ($bodyHtml || $bodyPlain)) {
			
			$tokenContainer = $this->getTokenContainer ();
			$tokenContainer->add ( 'userForgotPassword', $userForgotPassword );
			$tokenContainer->add ( 'user', $userForgotPassword->user );
			$tokenContainer->addParam ( 'userForgotPasswordId', $userForgotPassword->id );
			$tokenContainer->setSubject ( $subject );
			$tokenContainer->setBodyHtml ( $bodyHtml );
			$tokenContainer->setBodyText ( $bodyPlain );
			$tokenContainer->setFromEmail ( $fromEmail );
			$tokenContainer->setFromName ( $fromName );
			
			$tokenContainer->setToEmail ( $userForgotPassword->user->email );
			$tokenContainer->setToName ( $userForgotPassword->user->firstName . ' ' . $userForgotPassword->user->lastName );
			
			$tokenContainer->prepare ( 'userForgotPasswordMail' );
			
			$tokenContainer->sendMail ();
		}
		return $tokenContainer;
	}
	
	/**
	 *
	 * @return TokenContainer
	 */
	public function getTokenContainer() {
		return $this->getServiceLocator ()->get ( 'TokenContainer' );
	}
}
