<?php

namespace User\Service\Form;

use File\Api\FileApi;
use Common\Form\Option\AbstractMainFormEvent;
use UserRole\Api\UserRoleApi;
use User\Api\UserApi;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractFormEvent;

class AdminUser extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'firstName',
			'lastName',
			'email',
			'contactNumber',
			'status'
			
	);
	protected $_entity = '\User\Entity\User';
	protected $_entityName = 'user';
	public function getFormName() {
		return 'adminUserAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function postInitEvent() {
		$form = $this->getForm ();
		if (Functions::fromRoute ( 'userId' )) {
			$form->remove ( 'email' );
			$form->remove ( 'password' );
			$form->remove ( 'confirmPassword' );
			
			$inputFilter = $form->getInputFilter ();
			$inputFilter->remove ( 'email' );
			$inputFilter->remove ( 'password' );
			$inputFilter->remove ( 'confirmPassword' );
		}
	}
	public function parseSaveEntity($params, $entity) {
		if (isset ( $params ['password'] ) && $params ['password'] !== "") {
			$entity->password = UserApi::getEncryptedPassword ( $params ['password'] );
		}
		
		if (isset ( $params ['userRole'] )) {
			$entity->userRole = UserRoleApi::getRoleById ( $params ['userRole'] );
		}
		
		FileApi::deleteFiles ( Functions::fromPost ( 'avatar_delete', '' ) );
		
		$image = $params ['avatar'];
		
		if ($image) {
			$array = explode ( ',', $image );
			foreach ( $array as $file ) {
				$imageEntity = FileApi::createOrUpdateFile ( $file, $params ['firstName'] . ' ' . $params ['lastName'], $params ['firstName'] . ' ' . $params ['lastName'] );
				$entity->avatar = $imageEntity;
			}
		}
		
		
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		if ($entity->avatar) {
			$form->get ( 'avatar' )->setValue ( $entity->avatar->path );
		}
		if ($entity->userRole) {
			$form->get ( 'userRole' )->setValue ( $entity->userRole->id );
		}
		else{
			$form->get ( 'userRole' )->setValue ( '' );
		}
		
		
		
	}
}
