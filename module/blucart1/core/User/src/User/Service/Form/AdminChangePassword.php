<?php 
namespace User\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;

use User\Api\UserApi;

use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractFormEvent;

class AdminChangePassword extends AbstractMainFormEvent {
	protected $_columnKeys = array ('id');
	protected $_entity = '\User\Entity\User';
	protected $_entityName = 'user';
	public function getFormName() {
		return 'adminChangePasswordAdd';
	}
	public function getPriority() {
		return 1000;
	}
	
	
	public function parseSaveEntity($params, $entity){
		$entity->password = UserApi::getEncryptedPassword( $params ['password'] );
	}
	
}
