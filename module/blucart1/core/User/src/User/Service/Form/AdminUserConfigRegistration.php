<?php

namespace User\Service\Form;

use Config\Service\Form\AdminConfig;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminUserConfigRegistration extends AdminConfig {
	protected $_columnKeys = array (
			'USER_REGISTRATION_SUBJECT',
			'USER_REGISTRATION_BODYHTML',
			'USER_REGISTRATION_BODYPLAIN' 
	);
	public function getFormName() {
		return 'adminUserConfigRegistration';
	}
	public function getPriority() {
		return 1000;
	}
}
