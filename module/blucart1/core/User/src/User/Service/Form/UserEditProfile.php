<?php

namespace User\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use UserRole\Api\UserRoleApi;
use User\Api\UserApi;

class UserEditProfile extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'firstName',
			'lastName',
			'contactNumber',
	 
	);
	protected $_entity = '\User\Entity\User';
	protected $_entityName = 'user';
	public function getFormName() {
		return 'userEditProfile';
	}
	public function getPriority() {
		return 1000;
	}
	public function getId() {
		return UserApi::getLoggedInUser ()->id;
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		$roles = UserRoleApi::getRolesAtRegistration ();
		if ((count ( $roles ) > 1) && $entity && $entity->userRole) {
			$form->get ( 'userRole' )->setValue ( $entity->userRole->id );
		}
	}
	public function parseSaveParams($params) {
		$params['id']  = $this->getId();
		return $params;
	}
	public function parseSaveEntity($params, $entity) {
		if (isset ( $params ['userRole'] )) {
			$entity->userRole = UserRoleApi::getRoleById ( $params ['userRole'] );
		} 
	}
	public function afterSaveEntity($entity){
		
	}
}
