<?php

namespace User\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;

class UserAddress extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'firstName',
			'lastName',
			'email',
			'addressLine1',
			'addressLine2',
			'city',
			'state',
			'country',
			'postcode',
			'contactNumber' 
	);
	protected $_entity = '\User\Entity\UserAddress';
	protected $_entityName = 'userAddress';
	public function getFormName() {
		return 'userAddressAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		$user = UserApi::getLoggedInUser ();
		
		if ($user) {
			$entity->user = $user;
		}
		return $entity;
	}
	public function afterSaveEntity($entity) {
		$form = $this->getForm ();
		
		$user = UserApi::getLoggedInUser ();
		if ($form->get ( 'isDefault' )->getValue () || !(UserApi::getDefaultAddressByUserId ( $user->id ))) {
			
			if ($user) {
				UserApi::updateDefaultAddressById ( $entity->id, $user->id );
			}
		}
		return parent::afterSaveEntity($entity);
	}
}
