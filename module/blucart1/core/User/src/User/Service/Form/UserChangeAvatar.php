<?php

namespace User\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;
use User\Api\UserApi;
use File\Api\FileApi;

class UserChangeAvatar extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id' 
	)
	;
	protected $_entity = '\User\Entity\User';
	protected $_entityName = 'user';
	public function getFormName() {
		return 'userChangeAvatar';
	}
	public function getPriority() {
		return 1000;
	}
	public function getId() {
		return UserApi::getLoggedInUser ()->id;
	}
	
	public function parseSaveParams($params) {
		$params ['id'] = $this->getId ();
		return $params;
	}
	public function parseSaveEntity($params, $entity) {
		
		FileApi::deleteFiles ( Functions::fromPost ( 'avatar_delete', '' ) );
		
		$image = $params ['avatar'];
		
		if ($image) {
			$array = explode ( ',', $image );
			foreach ( $array as $file ) {
				$imageEntity = FileApi::createOrUpdateFile ( $file, $entity->firstName. ' ' . $entity->lastName,$entity->firstName. ' ' . $entity->lastName);
				$entity->avatar = $imageEntity;
			}
		}
		
	}
	public function afterSaveEntity($entity) {
	}
	
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		if ($entity->avatar) {
			$form->get ( 'avatar' )->setValue ( $entity->avatar->path );
		}
	}
}
