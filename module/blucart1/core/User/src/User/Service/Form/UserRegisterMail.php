<?php

namespace User\Service\Form;

use Config\Api\ConfigApi;
use Common\Option\Token\TokenItem;
use Common\Option\Token\TokenContainer;
use Common\Form\Option\AbstractFrontMainFormEvent;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class UserRegisterMail extends AbstractFrontMainFormEvent {
	public function getFormName() {
		return 'userRegister';
	}
	public function getPriority() {
		return null;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		
		$resultContainer = $this->getFormResultContainer ();
		$user = $resultContainer->get ( 'user' )->getValue ();
		
		if ($user) {
			
			$this->sendEmail ( $user );
		}
		return true;
	}
	
	/**
	 *
	 * @return \Common\Option\Token\TokenContainer
	 */
	public function sendEmail($user) {
		$subject = ConfigApi::getConfigByKey ( 'USER_REGISTRATION_SUBJECT' );
		$bodyHtml = ConfigApi::getConfigByKey ( 'USER_REGISTRATION_BODYHTML' );
		$bodyPlain = ConfigApi::getConfigByKey ( 'USER_REGISTRATION_BODYPLAIN' );
		
		$fromName = ConfigApi::getConfigByKey ( 'USER_FROM_NAME', '' );
		$fromEmail = ConfigApi::getConfigByKey ( 'USER_FROM_EMAIL' );
		
		if (! $fromEmail) {
			return;
		}
		
		if ($subject && ($bodyHtml || $bodyPlain)) {
			
			$tokenContainer = $this->getTokenContainer ();
			$tokenContainer->add ( 'user', $user );
			$tokenContainer->addParam ( 'userId', $user->id );
			$tokenContainer->setSubject ( $subject );
			$tokenContainer->setBodyHtml ( $bodyHtml );
			$tokenContainer->setBodyText ( $bodyPlain );
			$tokenContainer->setFromEmail ( $fromEmail );
			$tokenContainer->setFromName ( $fromName );
			
			$tokenContainer->setToEmail ( $user->email );
			$tokenContainer->setToName ( $user->firstName . ' ' . $user->lastName );
			
			$tokenContainer->prepare ( 'userRegisterMail' );
			
			$tokenContainer->sendMail ();
		}
		
		$subject = ConfigApi::getConfigByKey ( 'USER_ADMIN_NOTIFICATION_SUBJECT' );
		$bodyHtml = ConfigApi::getConfigByKey ( 'USER_ADMIN_NOTIFICATION_BODYHTML' );
		$bodyPlain = ConfigApi::getConfigByKey ( 'USER_ADMIN_NOTIFICATION_BODYPLAIN' );
		$notify = ConfigApi::getConfigByKey ( 'USER_ADMIN_NOTIFICATION_NOTIFY' );
		$toName = ConfigApi::getConfigByKey ( 'USER_NOTIFICATION_FROM_NAME' );
		$toEmail = ConfigApi::getConfigByKey ( 'USER_NOTIFICATION_FROM_EMAIL' );
		
		if ($subject && ($bodyHtml || $bodyPlain) && $toEmail && $notify) {
			
			$tokenContainer = $this->getTokenContainer ();
			$tokenContainer->add ( 'user', $user );
			$tokenContainer->addParam ( 'userId', $user->id );
			$tokenContainer->setSubject ( $subject );
			$tokenContainer->setBodyHtml ( $bodyHtml );
			$tokenContainer->setBodyText ( $bodyPlain );
			$tokenContainer->setFromEmail ( $fromEmail );
			$tokenContainer->setFromName ( $fromName );
			
			$tokenContainer->setToEmail ( $toEmail );
			$tokenContainer->setToName ( $toName  );
			
			$tokenContainer->prepare ( 'userRegisterMail' );
			$tokenContainer->sendMail ();
		}
		
		
		return $tokenContainer;
	}
	
	/**
	 *
	 * @return TokenContainer
	 */
	public function getTokenContainer() {
		return $this->getServiceLocator ()->get ( 'TokenContainer' );
	}
}
