<?php

namespace User\Service\Form;

use User\Api\UserApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class UserForgotPassword extends AbstractMainFormEvent {
	protected $_entity = '\User\Entity\UserForgotPassword';
	protected $_entityName = 'userForgotPassword';
	public function getFormName() {
		return 'userForgotPassword';
	}
	public function getPriority() {
		return 1000;
	}
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		$email = $params ['email'];
		
		$entity = UserApi::getUserForgotPasswordByEmail ( $email );
		
		if (! $entity) {
			$entity = new $this->_entity ();
			$entity->secret = md5 ( $email . time () );
			$entity->user = UserApi::getUserByEmail ( $email );
			$em->persist ( $entity );
			$em->flush ();
		}
		
		$resultContainer = $this->getFormResultContainer ();
		$resultContainer->add ( $this->_entityName, $entity );
		return true;
	}
}
