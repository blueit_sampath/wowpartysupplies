<?php 
namespace User\Service\Navigation;

use User\Api\UserApi;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminUserNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'customerMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'User',
							'route' => 'admin-user',
							'id' => 'admin-user',
							'iconClass' => 'glyphicon-user' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'customerMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Setting',
							'route' => 'admin-user-config-general',
							'id' => 'userSettingMain',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'userSettingMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'General',
							'route' => 'admin-user-config-general',
							'id' => 'admin-user-config-general',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'userSettingMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Registration',
							'route' => 'admin-user-config-registration',
							'id' => 'admin-user-config-registration',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox'
					)
			) );
		}
		$page = $navigation->findOneBy ( 'id', 'userSettingMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Password Recovery',
							'route' => 'admin-user-config-password-recovery',
							'id' => 'admin-user-config-password-recovery',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'userSettingMain' );
		if($page){
			$page->addPages ( array (
					array (
							'label' => 'Admin Notification',
							'route' => 'admin-user-config-admin-notification',
							'id' => 'admin-user-config-admin-notification',
							'iconClass' => 'glyphicon-wrench',
							'aClass' => 'zoombox'
					)
			) );
		}
	}
	
	public function breadcrumb() {
		parent::breadcrumb ();
		$routeName = Functions::getMatchedRouteName ();
	
		if ($routeName === 'admin-user-view') {
			$user = UserApi::getUserById ( Functions::fromRoute ( 'userId' ) );
			$navigation = $this->getNavigation ();
			$page = $navigation->findOneBy ( 'id', 'admin-user' );
			$page->addPages ( array (
					array (
							'label' => $user->firstName.' '.$user->lastName,
							'route' => 'admin-user-view',
							'params' => array (
									'userId' => $user->id
							),
							'id' => 'admin-user-view'
					)
	
			) );
		}
	}
}

