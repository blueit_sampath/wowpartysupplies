<?php

namespace User\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;
use User\Api\UserApi;

class UserAccountNavigation extends NavigationEvent {

    public function account() {
        $navigation = $this->getNavigation();
        $user = UserApi::getLoggedInUser();
        if (!$user) {
            $navigation->addPages(array(
                array(
                    'label' => 'Login',
                    'route' => 'user-login',
                    'id' => 'user-login',
                    'iconClass' => ''
                )
            ));

            $navigation->addPages(array(
                array(
                    'label' => 'Register',
                    'route' => 'user-register',
                    'id' => 'user-register'
                )
            ));
        } else {
            $navigation->addPages(array(
                array(
                    'label' => 'Dashboard',
                    'route' => 'account',
                    'id' => 'account',
                    'order' => -1000
                )
            ));

            $navigation->addPages(array(
                array(
                    'label' => 'Edit Profile',
                    'route' => 'account-editprofile',
                    'id' => 'account-editprofile',
                    'order' => 500,
                    'aClass' => 'zoombox w800 h400'
                )
            ));
            $navigation->addPages(array(
                array(
                    'label' => 'Change Avatar',
                    'route' => 'account-changeavatar',
                    'id' => 'account-changeavatar',
                    'order' => 600,
                    'aClass' => 'zoombox w800 h350'
                )
            ));

            $navigation->addPages(array(
                array(
                    'label' => 'Addresses',
                    'route' => 'account-address',
                    'id' => 'account-address',
                    'order' => 600,
                )
            ));

            $navigation->addPages(array(
                array(
                    'label' => 'Change Password',
                    'route' => 'account-changepassword',
                    'id' => 'account-changepassword',
                    'order' => 700,
                    'aClass' => 'zoombox w800 h300'
                )
            ));
            $navigation->addPages(array(
                array(
                    'label' => 'Logout',
                    'route' => 'user-logout',
                    'id' => 'user-logout',
                    'order' => 1000
                )
            ));
        }
    }

    public function breadcrumb() {
        parent::breadcrumb();

        $navigation = $this->getNavigation();
        $user = UserApi::getLoggedInUser();
        if (!$user) {
            $page = array(
                'label' => 'Login',
                'route' => 'user-login',
                'id' => 'user-login',
            );
            $this->addDefaultMenu($page);

            $page = array(
                'label' => 'Register',
                'route' => 'user-register',
                'id' => 'user-register'
            );
            $this->addDefaultMenu($page);

            $page = array(
                'label' => 'Forgot Password',
                'route' => 'user-forgotpassword',
                'id' => 'user-forgotpassword'
            );
            $this->addDefaultMenu($page);

            $page2 = $navigation->findOneBy('id', 'user-forgotpassword');
            $page2->addPage(
                    array(
                        'label' => 'Recover Password',
                        'route' => 'user-recoverpassword',
                        'id' => 'user-recoverpassword',
                        'uri' => $this->getUrl('user-recoverpassword')
            ));
        } else {
            $page = array(
                'label' => 'Dashboard',
                'route' => 'account',
                'id' => 'account',
                'order' => -1000
            );

            $this->addDefaultMenu($page);
            $page2 = $navigation->findOneBy('id', 'account');
            $page2->addPages(array(
                array(
                    'label' => 'Addresses',
                    'route' => 'account-address',
                    'id' => 'account-address',
                    'uri' => $this->getUrl('account-address'),
                    'order' => -900
                )
            ));
        }
    }

}
