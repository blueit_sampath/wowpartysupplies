<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace User\Controller;

use Common\MVC\Controller\AbstractFrontController;
use Zend\Mvc\Controller\AbstractController;
use Core\Functions;
use User\Option\AuthAdapter;
use Common\MVC\Controller\AbstractAdminController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;

class UserController extends AbstractFrontController {
	public function indexAction() {
	}
	public function editprofileAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserEditProfile\EditProfileFactory' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Saved Successfully' );
				return $this->redirect ()->toRoute ( 'account-editprofile' );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	public function changepasswordAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserChangePassword\ChangePasswordFactory' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Saved Successfully' );
				return $this->redirect ()->toRoute ( 'account-changepassword' );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	public function changeavatarAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserChangeAvatar\ChangeAvatarFactory' );
		
		if ($this->getRequest ()->isPost ()) {
			$data = array_merge ( $this->getRequest ()->getPost ()->toArray (), $this->getRequest ()->getFiles ()->toArray () );
			
			if (! empty ( $data ['avatar'] )) {
				$config = $this->getServiceLocator ()->get ( 'Config' );
				$path = $config ['file_options'] ['upload_dir'];
				if ($name = $this->uploadFile ( $data ['avatar'], $path, false, 'jpg,jpeg,png,gif' )) {
					$data ['avatar'] ['name'] = $name;
					$form->setData($data);
				} else {
					Functions::addErrorMessage ( 'Error in uploading file' );
					$viewModel = new ViewModel ( array (
							'form' => $form 
					) );
					return $viewModel;
				}
			}
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Saved Successfully' );
				return $this->redirect ()->toRoute ( 'account-changeavatar' );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	public function uploadFile($data, $path, $overrite = false, $validateExtensions = '') {
		$path_parts = pathinfo ( $data ['name'] );
		$name = Functions::slugify ( $path_parts ['filename'] );
		$ext = $path_parts ['extension'];
		
		if ($validateExtensions) {
			$exts = explode ( ',', $validateExtensions );
			if (! in_array ( $ext, $exts )) {
				Functions::addErrorMessage ( 'Invalid File Extension' );
				return false;
			}
		}
		$i = 0;
		while ( file_exists ( $path . '/' . $name . '_' . $i . '.' . $ext ) ) {
			$i ++;
		}
		$filename = $name . '_' . $i . '.' . $ext;
		
		move_uploaded_file ( $data ['tmp_name'], $path . '/' . $filename );
		
		$wideImage = new \WideImage ();
		$wideImage->load ( $path . '/' . $filename )->resize ( 100, 100 )->saveToFile ( $path . '/thumbnail/' . $filename );
		return $filename;
	}
}


