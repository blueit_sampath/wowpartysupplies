<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace User\Controller;

use Common\MVC\Controller\AbstractFrontController;
use Zend\Mvc\Controller\AbstractController;
use Core\Functions;
use User\Option\AuthAdapter;
use Common\MVC\Controller\AbstractAdminController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;

class UserForgotPasswordController extends AbstractFrontController {
	
	public function addSuccessMessage() {
		Functions::addSuccessMessage ( 'A mail with further instructions is sent to your Email Id' );
	}
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'User\Form\UserForgotPassword\ForgotPasswordFactory' );
	}
	public function afterSave() {
		$form = $this->getFormFactory ();
	}
	public function getSaveRedirector() {
		return $this->redirect ()->toRoute ( 'user-forgotpassword' );
	}
	public function forgotpasswordAction() {
		$form = $this->getFormFactory ();
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$this->afterSave ();
				$this->addSuccessMessage ();
				return $this->getSaveRedirector ();
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
        
        public function forgotpasswordModalAction() {
		$form = $this->getFormFactory ();
		
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	public function recoverpasswordAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserRecoverPassword\RecoverPasswordFactory' );
	
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Changed Password Successfully' );
				return $this->redirect ()->toRoute ( 'user-recoverpassword' );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form
		) );
	
		return $viewModel;
	}
}


