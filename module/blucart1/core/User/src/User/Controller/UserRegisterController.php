<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Common\MVC\Controller\AbstractFrontController;
use Zend\Mvc\Controller\AbstractController;
use Core\Functions;
use User\Option\AuthAdapter;
use Common\MVC\Controller\AbstractAdminController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;

class UserRegisterController extends AbstractFrontController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('User\Form\UserRegister\RegisterFactory');
    }

    public function afterSave() {
        $form = $this->getFormFactory();
    }

    public function getSaveRedirector() {
        return $this->redirect()->toRoute('home');
    }

    public function addSuccessMessage() {
        Functions::addSuccessMessage('Registered Successfully');
    }

    public function registerAction() {
        $form = $this->getFormFactory();

        if ($this->getRequest()->isPost()) {
            $form->setData($this->params()->fromPost());
            if ($form->isValid()) {
                $form->save();
                $this->afterSave();
                $this->addSuccessMessage();
                if ($redirectUrl = $form->get('redirectUrl')->getValue()) {
                    return $this->redirect()->toUrl(rawurldecode($redirectUrl));
                }
                return $this->getSaveRedirector();
            } else {
                $this->notValid();
            }
        } else {
            $form->getRecord();
        }
        $viewModel = new ViewModel(array(
            'form' => $form
                ));

        return $viewModel;
    }

    public function registerModalAction() {
        $form = $this->getFormFactory();
        $viewModel = new ViewModel(array(
            'form' => $form
                ));
        return $viewModel;
    }

}
