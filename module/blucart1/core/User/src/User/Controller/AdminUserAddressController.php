<?php
namespace User\Controller;

use Common\MVC\Controller\AbstractAdminController;
use Core\Functions;

class AdminUserAddressController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('User\Form\AdminUserAddress\UserAddressFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        $userId = Functions::fromRoute('userId');
        return $this->redirect()->toRoute('admin-user-address-add', array(
            'userId' => $userId,
            'userAddressId' => $form->get('id')
                ->getValue()
        ));
    }
}