<?php

namespace User\Controller;

use Core\Functions;

use Zend\View\Model\ViewModel;

use Common\MVC\Controller\AbstractAdminController;

class AdminUserController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUser\UserFormFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-user-add', array (
				'userId' => $form->get ( 'id' )->getValue () 
		) );
	}
	public function changepasswordAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\AdminChangePassword\ChangePasswordFactory' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$this->addSuccessMessage();
				return $this->redirect ()->toRoute ( 'admin-user-changepassword', array (
						'userId' => $form->get ( 'id' )->getValue () 
				) );
			}
		} else {
			$form->getRecord ();
		}
		return array (
				'form' => $form 
		);
	}
	
	public function viewAction(){
		
		$viewModel = new ViewModel();
		$request = Functions::getServiceLocator ()->get ( 'Request' );
		if ($request->isXmlHttpRequest ()) {
			$viewModel->setTerminal(true);
		}
		return $viewModel;
	}
}