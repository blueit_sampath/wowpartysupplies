<?php

namespace User\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminUserConfigRegistrationController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigRegistration\UserConfigRegistrationFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-user-config-registration' );
	}
}