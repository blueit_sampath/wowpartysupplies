<?php

namespace User\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminUserConfigPasswordRecoveryController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-user-config-password-recovery' );
	}
}