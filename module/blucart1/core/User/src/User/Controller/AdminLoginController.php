<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace User\Controller;

use Zend\Mvc\Controller\AbstractController;

use Core\Functions;

use User\Option\AuthAdapter;
use Common\MVC\Controller\AbstractAdminController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;

class AdminLoginController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminLogin\LoginFactory' );
	}
	public function afterSave() {
		$form = $this->getFormFactory ();
	}
	public function loginAction() {
		$form = $this->getFormFactory ();
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				
				$authAdapter = $this->getAuthAdapter ();
				$authAdapter->setIsFront ( 0 );
				$authAdapter->setEmail ( $form->get ( 'email' )->getValue () );
				$authAdapter->setPassword ( $form->get ( 'password' )->getValue () );
				
				$auth = new AuthenticationService ();
				$result = $auth->authenticate ( $authAdapter );
				if ($result->isValid ()) {
					
					Functions::addSuccessMessage ( 'You are now logged in.' );
					return $this->redirect ()->toRoute ( 'admin' );
				} else {
					
					Functions::addErrorMessage( 'Invalid Email or Password' );
				}
			} else {
				Functions::addErrorMessage( 'Invalid Email or Password' );
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	public function logoutAction() {
		$auth = new AuthenticationService ();
		$auth->clearIdentity ();
		$this->flashMessenger ()->setNamespace ( 'success' )->addMessage ( 'You are now logged out.' );
		return $this->redirect ()->toRoute ( 'admin-login' );
	}
	/**
	 *
	 * @return AuthAdapter
	 */
	public function getAuthAdapter() {
		$sm = $this->getServiceLocator ();
		return $sm->get ( 'User\Option\AuthAdapter' );
	}
}


