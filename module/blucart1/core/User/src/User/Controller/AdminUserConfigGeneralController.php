<?php

namespace User\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminUserConfigGeneralController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'User\Form\AdminUserConfigGeneral\UserConfigGeneralFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-user-config-general' );
	}
}