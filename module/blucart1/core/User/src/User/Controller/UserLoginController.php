<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Common\MVC\Controller\AbstractFrontController;
use Zend\Mvc\Controller\AbstractController;
use Core\Functions;
use User\Option\AuthAdapter;
use Common\MVC\Controller\AbstractAdminController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;

class UserLoginController extends AbstractFrontController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('User\Form\UserLogin\LoginFactory');
    }

    public function afterSave() {
        $form = $this->getFormFactory();
    }

    public function loginAction() {
        $form = $this->getFormFactory();
        if ($this->getRequest()->isPost()) {
            $form->setData($this->params()->fromPost());
            if ($form->isValid()) {
                $form->save();
                $authAdapter = $this->getAuthAdapter();
                $authAdapter->setIsFront(1);
                $authAdapter->setEmail($form->get('email')->getValue());
                $authAdapter->setPassword($form->get('password')->getValue());

                $auth = new AuthenticationService ();
                $result = $auth->authenticate($authAdapter);
                if ($result->isValid()) {
                    Functions::addSuccessMessage('You are now logged in.');
                    $this->getEventManager()->trigger('user-login', $this);
                    if ($redirectUrl = $form->get('redirectUrl')->getValue()) {
                        return $this->redirect()->toUrl(rawurldecode($redirectUrl));
                    }
                    return $this->redirect()->toRoute('home');
                } else {

                    Functions::addErrorMessage('Invalid Email or Password');
                }
            } else {
                Functions::addErrorMessage('Invalid Email or Password');
            }
        } else {
            $form->getRecord();
            $form->get('redirectUrl')->setValue(Functions::fromQuery('redirectUrl'));
        }
        $viewModel = new ViewModel(array(
            'form' => $form
        ));

        return $viewModel;
    }

    public function loginModalAction() {
        $form = $this->getFormFactory();
        $viewModel = new ViewModel(array(
            'form' => $form
        ));

        return $viewModel;
    }

    public function logoutAction() {
        $auth = new AuthenticationService ();
        $auth->clearIdentity();
        $this->flashMessenger()->setNamespace('success')->addMessage('You are now logged out.');
        $this->getEventManager()->trigger('user-logout', $this);
        return $this->redirect()->toRoute('home');
    }

    /**
     *
     * @return AuthAdapter
     */
    public function getAuthAdapter() {
        $sm = $this->getServiceLocator();
        return $sm->get('User\Option\AuthAdapter');
    }

}
