<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/DIYUser for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace User\Controller;

use Common\MVC\Controller\AbstractFrontController;
use Zend\Mvc\Controller\AbstractController;
use Core\Functions;
use User\Option\AuthAdapter;
use Common\MVC\Controller\AbstractAdminController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use User\Api\UserApi;

class UserController extends AbstractFrontController {
	public function indexAction() {
	}
	public function editprofileAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserEditProfile\EditProfileFactory' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Saved Successfully' );
				return $this->redirect ()->toRoute ( 'account-editprofile' );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	public function changepasswordAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserChangePassword\ChangePasswordFactory' );
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Saved Successfully' );
				return $this->redirect ()->toRoute ( 'account-changepassword' );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	
	public function changeavatarAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserChangeAvatar\ChangeAvatarFactory' );
	
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Saved Successfully' );
				return $this->redirect ()->toRoute ( 'account-changeavatar' );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form
		) );
	
		return $viewModel;
	}
	
	public function addressAction() {
		return array();
	}
	
	public function editaddressAction() {
		$form = $this->getServiceLocator ()->get ( 'User\Form\UserAddress\UserAddressFactory' );
	
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				Functions::addSuccessMessage ( 'Saved Successfully' );
				return $this->redirect ()->toRoute ( 'account-address-edit', array(
					'userAddressId' => $form->get('id')->getValue()
						
				) );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form
		) );
	
		return $viewModel;
	}
	
	public function removeAddressAction() {
		$userAddressId = Functions::fromRoute('userAddressId');
		UserApi::removeAddressById($userAddressId);
		return $this->redirect()->toRoute('account-address');
	}
	
}


