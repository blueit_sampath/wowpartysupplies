<?php
		
		return array(
    'router' => array('routes' => array(
            'account' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'index'
                        )
                    )
                ),
            'account-editprofile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/editprofile[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'editprofile',
                        'ajax' => 'true'
                        )
                    )
                ),
            'account-changepassword' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/changepassword[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'changepassword',
                        'ajax' => 'true'
                        )
                    )
                ),
            'account-changeavatar' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/changeavatar[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'changeavatar',
                        'ajax' => 'true'
                        )
                    )
                ),
            'account-address' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/address',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'address'
                        )
                    )
                ),
            'account-address-edit' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/address/edit[/:userAddressId][/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'editaddress',
                        'ajax' => 'true'
                        ),
                    'constraints' => array('userAddressId' => '[a-zA-Z0-9]+')
                    )
                ),
            'account-address-remove' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account/address/remove[/:userAddressId]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'removeaddress'
                        ),
                    'constraints' => array('userAddressId' => '[a-zA-Z0-9]+')
                    )
                ),
            'user-register' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/register',
                    'defaults' => array(
                        'controller' => 'User\Controller\UserRegister',
                        'action' => 'register'
                        )
                    )
                ),
            'user-register-modal' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/register/modal',
                    'defaults' => array(
                        'controller' => 'User\Controller\UserRegister',
                        'action' => 'register-modal'
                        )
                    )
                ),
            'user-forgotpassword' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/forgotpassword',
                    'defaults' => array(
                        'controller' => 'User\Controller\UserForgotPassword',
                        'action' => 'forgotpassword'
                        )
                    )
                ),
            'user-forgotpassword-modal' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/forgotpassword/modal',
                    'defaults' => array(
                        'controller' => 'User\Controller\UserForgotPassword',
                        'action' => 'forgotpassword-modal'
                        )
                    )
                ),
            'user-recoverpassword' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/recoverpassword[/:secret]',
                    'defaults' => array(
                        'controller' => 'User\Controller\UserForgotPassword',
                        'action' => 'recoverpassword'
                        ),
                    'constraints' => array('secret' => '[a-zA-Z0-9]+')
                    )
                ),
            'user-login' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/login',
                    'defaults' => array(
                        'controller' => 'User\Controller\UserLogin',
                        'action' => 'login'
                        )
                    )
                ),
            'user-login-modal' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/login/modal',
                    'defaults' => array(
                        'controller' => 'User\Controller\UserLogin',
                        'action' => 'loginModal'
                        )
                    )
                ),
            'user-logout' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user/logout',
                    'defaults' => array(
                        'controller' => 'User\Controller\UserLogin',
                        'action' => 'logout'
                        )
                    )
                ),
            'admin-user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUser',
                        'action' => 'index'
                        )
                    )
                ),
            'admin-login' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/login',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminLogin',
                        'action' => 'login'
                        )
                    )
                ),
            'admin-logout' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/logout',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminLogin',
                        'action' => 'logout'
                        )
                    )
                ),
            'admin-user-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/view[ajax/:ajax][/:userId]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUser',
                        'action' => 'view',
                        'ajax' => 'false'
                        ),
                    'constraints' => array(
                        'userId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-user-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/add[ajax/:ajax][/:userId]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUser',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'userId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-user-changepassword' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/changepassword[ajax/:ajax][/:userId]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUser',
                        'action' => 'changepassword',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'userId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-user-config-general' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/config/general[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUserConfigGeneral',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array('ajax' => 'true|false')
                    )
                ),
            'admin-user-config-registration' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/config/registration[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUserConfigRegistration',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array('ajax' => 'true|false')
                    )
                ),
            'admin-user-config-admin-notification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/config/admin/notification[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUserConfigAdminNotification',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array('ajax' => 'true|false')
                    )
                ),
            'admin-user-config-password-recovery' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/config/password/recovery[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUserConfigPasswordRecovery',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array('ajax' => 'true|false')
                    )
                ),
            'admin-user-address' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/address/:userId[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUserAddress',
                        'action' => 'index',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'userId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                ),
            'admin-user-address-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/user/address/add[/ajax/:ajax][/:userId][/:userAddressId]',
                    'defaults' => array(
                        'controller' => 'User\Controller\AdminUserAddress',
                        'action' => 'add',
                        'ajax' => 'true'
                        ),
                    'constraints' => array(
                        'userId' => '[0-9]+',
                        'userAddressId' => '[0-9]+',
                        'ajax' => 'true|false'
                        )
                    )
                )
            )),
    'events' => array(
        'User\Service\Grid\AdminUser' => 'User\Service\Grid\AdminUser',
        'User\Service\Link\AdminUser' => 'User\Service\Link\AdminUser',
        'User\Service\Form\AdminUser' => 'User\Service\Form\AdminUser',
        'User\Service\Form\AdminChangePassword' => 'User\Service\Form\AdminChangePassword',
        'User\Service\Tab\AdminChangePassword' => 'User\Service\Tab\AdminChangePassword',
        'User\Service\Tab\AdminUser' => 'User\Service\Tab\AdminUser',
        'User\Service\Content\AdminDashboardTopUser' => 'User\Service\Content\AdminDashboardTopUser',
        'User\Service\Content\AdminDashboardMainContentUser' => 'User\Service\Content\AdminDashboardMainContentUser',
        'User\Service\Link\AdminUserView' => 'User\Service\Link\AdminUserView',
        'User\Service\Navigation\AdminUserNavigation' => 'User\Service\Navigation\AdminUserNavigation',
        'User\Service\Form\AdminUserConfigGeneral' => 'User\Service\Form\AdminUserConfigGeneral',
        'User\Service\Form\AdminUserConfigRegistration' => 'User\Service\Form\AdminUserConfigRegistration',
        'User\Service\Form\AdminUserConfigAdminNotification' => 'User\Service\Form\AdminUserConfigAdminNotification',
        'User\Service\Form\AdminUserConfigPasswordRecovery' => 'User\Service\Form\AdminUserConfigPasswordRecovery',
        'User\Service\Tab\AdminUserConfig' => 'User\Service\Tab\AdminUserConfig',
        'User\Service\Form\UserRegister' => 'User\Service\Form\UserRegister',
        'User\Service\Form\UserRegisterMail' => 'User\Service\Form\UserRegisterMail',
        'User\Service\Form\UserForgotPassword' => 'User\Service\Form\UserForgotPassword',
        'User\Service\Form\UserForgotPasswordMail' => 'User\Service\Form\UserForgotPasswordMail',
        'User\Service\Form\UserRecoverPassword' => 'User\Service\Form\UserRecoverPassword',
        'User\Service\Form\UserEditProfile' => 'User\Service\Form\UserEditProfile',
        'User\Service\Form\UserChangePassword' => 'User\Service\Form\UserChangePassword',
        'User\Service\Form\UserChangeAvatar' => 'User\Service\Form\UserChangeAvatar',
        'User\Service\Block\UserAccountMenu' => 'User\Service\Block\UserAccountMenu',
        'User\Service\Navigation\UserAccountNavigation' => 'User\Service\Navigation\UserAccountNavigation',
        'User\Service\Form\UserAddress' => 'User\Service\Form\UserAddress',
        'User\Service\Content\UserAddress' => 'User\Service\Content\UserAddress',
        'User\Service\Grid\AdminUserAddress' => 'User\Service\Grid\AdminUserAddress',
        'User\Service\Link\AdminUserAddress' => 'User\Service\Link\AdminUserAddress',
        'User\Service\Form\AdminUserAddress' => 'User\Service\Form\AdminUserAddress',
        'User\Service\Block\AdminDashboardSimpleUserStat' => 'User\Service\Block\AdminDashboardSimpleUserStat',
        'User\Service\Block\AdminDashboardUserGraph' => 'User\Service\Block\AdminDashboardUserGraph',
        'User\Service\Block\AdminDashboardUserTables' => 'User\Service\Block\AdminDashboardUserTables'
        ),
    'service_manager' => array(
        'invokables' => array(
            'User\Service\Grid\AdminUser' => 'User\Service\Grid\AdminUser',
            'User\Service\Link\AdminUser' => 'User\Service\Link\AdminUser',
            'User\Service\Form\AdminUser' => 'User\Service\Form\AdminUser',
            'User\Form\AdminUser\UserForm' => 'User\Form\AdminUser\UserForm',
            'User\Form\AdminUser\UserFormFilter' => 'User\Form\AdminUser\UserFormFilter',
            'User\Form\AdminChangePassword\ChangePasswordForm' => 'User\Form\AdminChangePassword\ChangePasswordForm',
            'User\Form\AdminChangePassword\ChangePasswordFilter' => 'User\Form\AdminChangePassword\ChangePasswordFilter',
            'User\Service\Form\AdminChangePassword' => 'User\Service\Form\AdminChangePassword',
            'User\Service\Tab\AdminChangePassword' => 'User\Service\Tab\AdminChangePassword',
            'User\Service\Tab\AdminUser' => 'User\Service\Tab\AdminUser',
            'User\Form\AdminUser\UserSearchForm' => 'User\Form\AdminUser\UserSearchForm',
            'User\Service\Content\AdminDashboardTopUser' => 'User\Service\Content\AdminDashboardTopUser',
            'User\Service\Content\AdminDashboardMainContentUser' => 'User\Service\Content\AdminDashboardMainContentUser',
            'User\Service\Link\AdminUserView' => 'User\Service\Link\AdminUserView',
            'User\Service\Navigation\AdminUserNavigation' => 'User\Service\Navigation\AdminUserNavigation',
            'User\Form\AdminLogin\LoginForm' => 'User\Form\AdminLogin\LoginForm',
            'User\Form\AdminLogin\LoginFilter' => 'User\Form\AdminLogin\LoginFilter',
            'User\Option\AuthAdapter' => 'User\Option\AuthAdapter',
            'User\Form\AdminUserConfigGeneral\UserConfigGeneralForm' => 'User\Form\AdminUserConfigGeneral\UserConfigGeneralForm',
            'User\Form\AdminUserConfigGeneral\UserConfigGeneralFilter' => 'User\Form\AdminUserConfigGeneral\UserConfigGeneralFilter',
            'User\Service\Form\AdminUserConfigGeneral' => 'User\Service\Form\AdminUserConfigGeneral',
            'User\Form\AdminUserConfigRegistration\UserConfigRegistrationForm' => 'User\Form\AdminUserConfigRegistration\UserConfigRegistrationForm',
            'User\Form\AdminUserConfigRegistration\UserConfigRegistrationFilter' => 'User\Form\AdminUserConfigRegistration\UserConfigRegistrationFilter',
            'User\Service\Form\AdminUserConfigRegistration' => 'User\Service\Form\AdminUserConfigRegistration',
            'User\Form\AdminUserConfigAdminNotification\UserConfigAdminNotificationForm' => 'User\Form\AdminUserConfigAdminNotification\UserConfigAdminNotificationForm',
            'User\Form\AdminUserConfigAdminNotification\UserConfigAdminNotificationFilter' => 'User\Form\AdminUserConfigAdminNotification\UserConfigAdminNotificationFilter',
            'User\Service\Form\AdminUserConfigAdminNotification' => 'User\Service\Form\AdminUserConfigAdminNotification',
            'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryForm' => 'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryForm',
            'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryFilter' => 'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryFilter',
            'User\Service\Form\AdminUserConfigPasswordRecovery' => 'User\Service\Form\AdminUserConfigPasswordRecovery',
            'User\Service\Tab\AdminUserConfig' => 'User\Service\Tab\AdminUserConfig',
            'User\Form\UserRegister\RegisterForm' => 'User\Form\UserRegister\RegisterForm',
            'User\Form\UserRegister\RegisterFilter' => 'User\Form\UserRegister\RegisterFilter',
            'User\Service\Form\UserRegister' => 'User\Service\Form\UserRegister',
            'User\Service\Form\UserRegisterMail' => 'User\Service\Form\UserRegisterMail',
            'User\Form\UserLogin\LoginForm' => 'User\Form\UserLogin\LoginForm',
            'User\Form\UserLogin\LoginFilter' => 'User\Form\UserLogin\LoginFilter',
            'User\Form\UserForgotPassword\ForgotPasswordForm' => 'User\Form\UserForgotPassword\ForgotPasswordForm',
            'User\Form\UserForgotPassword\ForgotPasswordFilter' => 'User\Form\UserForgotPassword\ForgotPasswordFilter',
            'User\Service\Form\UserForgotPassword' => 'User\Service\Form\UserForgotPassword',
            'User\Service\Form\UserForgotPasswordMail' => 'User\Service\Form\UserForgotPasswordMail',
            'User\Form\UserRecoverPassword\RecoverPasswordForm' => 'User\Form\UserRecoverPassword\RecoverPasswordForm',
            'User\Form\UserRecoverPassword\RecoverPasswordFilter' => 'User\Form\UserRecoverPassword\RecoverPasswordFilter',
            'User\Service\Form\UserRecoverPassword' => 'User\Service\Form\UserRecoverPassword',
            'User\Form\UserEditProfile\EditProfileForm' => 'User\Form\UserEditProfile\EditProfileForm',
            'User\Form\UserEditProfile\EditProfileFilter' => 'User\Form\UserEditProfile\EditProfileFilter',
            'User\Service\Form\UserEditProfile' => 'User\Service\Form\UserEditProfile',
            'User\Form\UserChangePassword\ChangePasswordForm' => 'User\Form\UserChangePassword\ChangePasswordForm',
            'User\Form\UserChangePassword\ChangePasswordFilter' => 'User\Form\UserChangePassword\ChangePasswordFilter',
            'User\Service\Form\UserChangePassword' => 'User\Service\Form\UserChangePassword',
            'User\Form\UserChangeAvatar\ChangeAvatarForm' => 'User\Form\UserChangeAvatar\ChangeAvatarForm',
            'User\Form\UserChangeAvatar\ChangeAvatarFilter' => 'User\Form\UserChangeAvatar\ChangeAvatarFilter',
            'User\Service\Form\UserChangeAvatar' => 'User\Service\Form\UserChangeAvatar',
            'User\Service\Block\UserAccountMenu' => 'User\Service\Block\UserAccountMenu',
            'User\Service\Navigation\UserAccountNavigation' => 'User\Service\Navigation\UserAccountNavigation',
            'User\Form\UserAddress\UserAddressForm' => 'User\Form\UserAddress\UserAddressForm',
            'User\Form\UserAddress\UserAddressFilter' => 'User\Form\UserAddress\UserAddressFilter',
            'User\Service\Form\UserAddress' => 'User\Service\Form\UserAddress',
            'User\Service\Content\UserAddress' => 'User\Service\Content\UserAddress',
            'User\Service\Grid\AdminUserAddress' => 'User\Service\Grid\AdminUserAddress',
            'User\Service\Link\AdminUserAddress' => 'User\Service\Link\AdminUserAddress',
            'User\Form\AdminUserAddress\UserAddressForm' => 'User\Form\AdminUserAddress\UserAddressForm',
            'User\Form\AdminUserAddress\UserAddressFilter' => 'User\Form\AdminUserAddress\UserAddressFilter',
            'User\Service\Form\AdminUserAddress' => 'User\Service\Form\AdminUserAddress',
            'User\Service\Block\AdminDashboardSimpleUserStat' => 'User\Service\Block\AdminDashboardSimpleUserStat',
            'User\Service\Block\AdminDashboardUserGraph' => 'User\Service\Block\AdminDashboardUserGraph',
            'User\Service\Block\AdminDashboardUserTables' => 'User\Service\Block\AdminDashboardUserTables'
            ),
        'factories' => array(
            'User\Form\AdminUser\UserFormFactory' => 'User\Form\AdminUser\UserFormFactory',
            'User\Form\AdminChangePassword\ChangePasswordFactory' => 'User\Form\AdminChangePassword\ChangePasswordFactory',
            'User\Form\AdminLogin\LoginFactory' => 'User\Form\AdminLogin\LoginFactory',
            'User\Form\AdminUserConfigGeneral\UserConfigGeneralFactory' => 'User\Form\AdminUserConfigGeneral\UserConfigGeneralFactory',
            'User\Form\AdminUserConfigRegistration\UserConfigRegistrationFactory' => 'User\Form\AdminUserConfigRegistration\UserConfigRegistrationFactory',
            'User\Form\AdminUserConfigAdminNotification\UserConfigAdminNotificationFactory' => 'User\Form\AdminUserConfigAdminNotification\UserConfigAdminNotificationFactory',
            'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryFactory' => 'User\Form\AdminUserConfigPasswordRecovery\UserConfigPasswordRecoveryFactory',
            'User\Form\UserRegister\RegisterFactory' => 'User\Form\UserRegister\RegisterFactory',
            'User\Form\UserLogin\LoginFactory' => 'User\Form\UserLogin\LoginFactory',
            'User\Form\UserForgotPassword\ForgotPasswordFactory' => 'User\Form\UserForgotPassword\ForgotPasswordFactory',
            'User\Form\UserRecoverPassword\RecoverPasswordFactory' => 'User\Form\UserRecoverPassword\RecoverPasswordFactory',
            'User\Form\UserEditProfile\EditProfileFactory' => 'User\Form\UserEditProfile\EditProfileFactory',
            'User\Form\UserChangePassword\ChangePasswordFactory' => 'User\Form\UserChangePassword\ChangePasswordFactory',
            'User\Form\UserChangeAvatar\ChangeAvatarFactory' => 'User\Form\UserChangeAvatar\ChangeAvatarFactory',
            'User\Form\UserAddress\UserAddressFactory' => 'User\Form\UserAddress\UserAddressFactory',
            'User\Form\AdminUserAddress\UserAddressFactory' => 'User\Form\AdminUserAddress\UserAddressFactory'
            )
        ),
    'controllers' => array('invokables' => array(
            'User\Controller\User' => 'User\Controller\UserController',
            'User\Controller\AdminUser' => 'User\Controller\AdminUserController',
            'User\Controller\AdminLogin' => 'User\Controller\AdminLoginController',
            'User\Controller\AdminUserConfigGeneral' => 'User\Controller\AdminUserConfigGeneralController',
            'User\Controller\AdminUserConfigRegistration' => 'User\Controller\AdminUserConfigRegistrationController',
            'User\Controller\AdminUserConfigAdminNotification' => 'User\Controller\AdminUserConfigAdminNotificationController',
            'User\Controller\AdminUserConfigPasswordRecovery' => 'User\Controller\AdminUserConfigPasswordRecoveryController',
            'User\Controller\UserLogin' => 'User\Controller\UserLoginController',
            'User\Controller\UserForgotPassword' => 'User\Controller\UserForgotPasswordController',
            'User\Controller\UserRegister' => 'User\Controller\UserRegisterController',
            'User\Controller\AdminUserAddress' => 'User\Controller\AdminUserAddressController'
            )),
    'view_manager' => array('template_path_stack' => array('User' => __DIR__.'/../view')),
    'doctrine' => array('driver' => array(
            'User_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__.'/../src/User/Entity')
                ),
            'orm_default' => array('drivers' => array('User\Entity' => 'User_driver'))
            )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('User' => __DIR__.'/../public')))
    );
