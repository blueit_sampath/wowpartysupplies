<?php
return array(
    'router' => array('routes' => array(
            'product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product[/:productId]',
                    'defaults' => array(
                        'controller' => 'Product\Controller\Index',
                        'action' => 'index'
                    ),
                    'constraints' => array('productId' => '[0-9]+')
                )
            ),
            'product-quickview' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product/quickview[/:productId]',
                    'defaults' => array(
                        'controller' => 'Product\Controller\Index',
                        'action' => 'quickview'
                    ),
                    'constraints' => array('productId' => '[0-9]+')
                )
            ),
            'admin-product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product',
                    'defaults' => array(
                        'controller' => 'Product\Controller\AdminProduct',
                        'action' => 'index'
                    )
                )
            ),
            'admin-product-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/view[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Product\Controller\AdminProduct',
                        'action' => 'view',
                        'ajax' => 'false'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/add[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Product\Controller\AdminProduct',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-image' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/image/:productId[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Product\Controller\AdminProductImage',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-image-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/image/add[/ajax/:ajax][/:productId][/:productImageId]',
                    'defaults' => array(
                        'controller' => 'Product\Controller\AdminProductImage',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'productImageId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-product-image-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/product/image/sort[/ajax/:ajax][/:productId]',
                    'defaults' => array(
                        'controller' => 'Product\Controller\AdminProductImage',
                        'action' => 'sort',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'productId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            )
        )),
    'events' => array(
        'Product\Service\Grid\AdminProduct' => 'Product\Service\Grid\AdminProduct',
        'Product\Service\Form\AdminProduct' => 'Product\Service\Form\AdminProduct',
        'Product\Service\Link\AdminProduct' => 'Product\Service\Link\AdminProduct',
        'Product\Service\Tab\AdminProduct' => 'Product\Service\Tab\AdminProduct',
        'Product\Service\Grid\AdminProductImage' => 'Product\Service\Grid\AdminProductImage',
        'Product\Service\Tab\AdminProductImage' => 'Product\Service\Tab\AdminProductImage',
        'Product\Service\Form\AdminProductImage' => 'Product\Service\Form\AdminProductImage',
        'Product\Service\Link\AdminProductImage' => 'Product\Service\Link\AdminProductImage',
        'Product\Service\NestedSorting\AdminProductImage' => 'Product\Service\NestedSorting\AdminProductImage',
        'Product\Service\Content\AdminDashboardTopProduct' => 'Product\Service\Content\AdminDashboardTopProduct',
        'Product\Service\Content\AdminDashboardMainContentProduct' => 'Product\Service\Content\AdminDashboardMainContentProduct',
        'Product\Service\Link\AdminProductView' => 'Product\Service\Link\AdminProductView',
        'Product\Service\Navigation\AdminProductNavigation' => 'Product\Service\Navigation\AdminProductNavigation',
        'Product\Service\Block\NewProducts' => 'Product\Service\Block\NewProducts',
        'Product\Service\Block\AdminDashboardSimpleProductStat' => 'Product\Service\Block\AdminDashboardSimpleProductStat',
        'Product\Service\Block\AdminDashboardProductTopGraph' => 'Product\Service\Block\AdminDashboardProductTopGraph',
        'Product\Service\Block\AdminDashboardProductTables' => 'Product\Service\Block\AdminDashboardProductTables',
        'Product\Service\Navigation\ProductNavigation' => 'Product\Service\Navigation\ProductNavigation'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Product\Service\Grid\AdminProduct' => 'Product\Service\Grid\AdminProduct',
            'Product\Form\AdminProduct\ProductForm' => 'Product\Form\AdminProduct\ProductForm',
            'Product\Form\AdminProduct\ProductFilter' => 'Product\Form\AdminProduct\ProductFilter',
            'Product\Service\Form\AdminProduct' => 'Product\Service\Form\AdminProduct',
            'Product\Service\Link\AdminProduct' => 'Product\Service\Link\AdminProduct',
            'Product\Service\Tab\AdminProduct' => 'Product\Service\Tab\AdminProduct',
            'Product\Service\Grid\AdminProductImage' => 'Product\Service\Grid\AdminProductImage',
            'Product\Service\Tab\AdminProductImage' => 'Product\Service\Tab\AdminProductImage',
            'Product\Form\AdminProductImage\ProductImageForm' => 'Product\Form\AdminProductImage\ProductImageForm',
            'Product\Form\AdminProductImage\ProductImageFilter' => 'Product\Form\AdminProductImage\ProductImageFilter',
            'Product\Service\Form\AdminProductImage' => 'Product\Service\Form\AdminProductImage',
            'Product\Service\Link\AdminProductImage' => 'Product\Service\Link\AdminProductImage',
            'Product\Service\NestedSorting\AdminProductImage' => 'Product\Service\NestedSorting\AdminProductImage',
            'Product\Form\AdminProduct\ProductSearchForm' => 'Product\Form\AdminProduct\ProductSearchForm',
            'Product\Service\Content\AdminDashboardTopProduct' => 'Product\Service\Content\AdminDashboardTopProduct',
            'Product\Service\Content\AdminDashboardMainContentProduct' => 'Product\Service\Content\AdminDashboardMainContentProduct',
            'Product\Service\Link\AdminProductView' => 'Product\Service\Link\AdminProductView',
            'Product\Service\Navigation\AdminProductNavigation' => 'Product\Service\Navigation\AdminProductNavigation',
            'Product\Service\Block\NewProducts' => 'Product\Service\Block\NewProducts',
            'Product\Service\Block\AdminDashboardSimpleProductStat' => 'Product\Service\Block\AdminDashboardSimpleProductStat',
            'Product\Service\Block\AdminDashboardProductTopGraph' => 'Product\Service\Block\AdminDashboardProductTopGraph',
            'Product\Service\Block\AdminDashboardProductTables' => 'Product\Service\Block\AdminDashboardProductTables',
            'Product\Service\Navigation\ProductNavigation' => 'Product\Service\Navigation\ProductNavigation'
        ),
        'factories' => array(
            'Product\Form\AdminProduct\ProductFormFactory' => 'Product\Form\AdminProduct\ProductFormFactory',
            'Product\Form\AdminProductImage\ProductImageFormFactory' => 'Product\Form\AdminProductImage\ProductImageFormFactory'
        )
    ),
    'controllers' => array('invokables' => array(
            'Product\Controller\AdminProduct' => 'Product\Controller\AdminProductController',
            'Product\Controller\AdminProductImage' => 'Product\Controller\AdminProductImageController',
            'Product\Controller\Index' => 'Product\Controller\IndexController'
        )),
    'view_manager' => array('template_path_stack' => array('Product' => __DIR__ . '/../view')),
    'doctrine' => array('driver' => array(
            'Product_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Product/Entity')
            ),
            'orm_default' => array('drivers' => array('Product\Entity' => 'Product_driver'))
        )),
    'view_helpers' => array(
        'invokables' => array(
            'sellPrice' => 'Product\View\Helper\SellPrice',
            'listPrice' => 'Product\View\Helper\ListPrice',
        )
    ),
    'asset_manager' => array('resolver_configs' => array('paths' => array('Product' => __DIR__ . '/../public')))
);
