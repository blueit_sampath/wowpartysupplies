<?php

namespace Product\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ProductImage
 *
 * @ORM\Table(name="product_image")
 * @ORM\Entity
 */
class ProductImage {

    /**
     *
     * @var integer @ORM\Column(name="id", type="integer", nullable=false)
     *      @ORM\Id
     *      @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var integer @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     *
     * @var boolean @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="create")
     */
    private $createdDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="update")
     */
    private $updatedDate;

    /**
     *
     * @var \Product\Entity\Product @ORM\ManyToOne(targetEntity="Product")
     *      @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="product_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     *      })
     */
    private $product;

    /**
     *
     * @var \File\Entity\File @ORM\ManyToOne(targetEntity="\File\Entity\File")
     *      @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="file_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     *      })
     */
    private $file;

    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property        	
     * @return mixed
     *
     */
    public function __get($property) {
        if (\method_exists($this, "get" . \ucfirst($property))) {
            $method_name = "get" . \ucfirst($property);
            return $this->$method_name();
        } else {

            if (is_object($this->$property)) {
                // return $this->$property->id;
            }
            return $this->$property;
        }
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property        	
     * @param mixed $value        	
     *
     */
    public function __set($property, $value) {
        if (\method_exists($this, "set" . \ucfirst($property))) {
            $method_name = "set" . \ucfirst($property);

            $this->$method_name($value);
        } else
            $this->$property = $value;
    }

    public function __isset($name) {
        if (isset($this->$name)) {
            return true;
        }
        return false;
    }

}
