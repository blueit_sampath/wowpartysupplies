<?php
namespace Product\Form\AdminProductImage;

use Common\Form\Option\AbstractFormFactory;

class ProductImageFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Product\Form\AdminProductImage\ProductImageForm' );
		$form->setName ( 'adminProductImageAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Product\Form\AdminProductImage\ProductImageFilter' );
	}
}
