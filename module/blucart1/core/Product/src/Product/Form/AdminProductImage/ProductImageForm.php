<?php
namespace Product\Form\AdminProductImage;

use Core\Functions;
use Zend\Form\Element\Select;
use Category\Api\CategoryApi;
use Zend\Form\Fieldset;
use Common\Form\Form;
use Zend\Captcha;
use Zend\Form\Element;

class ProductImageForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'product',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		$this->add ( array (
				'name' => 'title',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Title' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'alt',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Alt Text' 
				) 
		), array (
				'priority' => 900 
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Priority' 
				) 
		), array (
				'priority' => 800 
		) );
		
		$this->add ( array (
				'name' => 'image',
				'type' => 'File\Form\Element\FileUpload' 
		)
		, array (
				'priority' => 700 
		) );
		
		$this->getStatus ( 600 );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}