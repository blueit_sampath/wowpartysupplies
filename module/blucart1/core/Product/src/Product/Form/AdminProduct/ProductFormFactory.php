<?php
namespace Product\Form\AdminProduct;

use Common\Form\Option\AbstractFormFactory;

class ProductFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Product\Form\AdminProduct\ProductForm' );
		$form->setName ( 'adminProductAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Product\Form\AdminProduct\ProductFilter' );
	}
}
