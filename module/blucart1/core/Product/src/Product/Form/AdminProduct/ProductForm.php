<?php

namespace Product\Form\AdminProduct;

use Config\Api\ConfigApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Category\Api\CategoryApi;
use Zend\Form\Fieldset;
use Common\Form\Form;
use Zend\Captcha;
use Zend\Form\Element;

class ProductForm extends Form {

    public function init() {
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));


        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Title',
            ),
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'sku',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'SKU',
            )
                ), array(
            'priority' => 990
        ));

        $this->add(array(
            'name' => 'shortDescription',
            'attributes' => array(
                'type' => 'textarea',
                'rows' => 5
            ),
            'options' => array(
                'label' => 'Short Description',
            )
                ), array(
            'priority' => 970
        ));


        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
                'class' => 'wysiwg_full',
                'rows' => 5
            ),
            'options' => array(
                'label' => 'Description',
            )
                ), array(
            'priority' => 960
        ));

        $this->add(array(
            'name' => 'listPrice',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'List Price',
                'add-on-prepend' => ConfigApi::getCurrencySymbol(),
            )
                ), array(
            'priority' => 950
        ));

        $this->add(array(
            'name' => 'costPrice',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Cost Price',
                'add-on-prepend' => ConfigApi::getCurrencySymbol(),
            )
                ), array(
            'priority' => 940
        ));

        $this->add(array(
            'name' => 'sellPrice',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Sell Price',
                'add-on-prepend' => ConfigApi::getCurrencySymbol(),
            )
                ), array(
            'priority' => 930
        ));




        $this->add(array(
            'name' => 'weight',
            'attributes' => array(
                'type' => 'text',
            ),
            'options' => array(
                'label' => 'Priority',
            )
                ), array(
            'priority' => 920
        ));

        $this->add(array(
            'name' => 'searchKeywords',
            'attributes' => array(
                'type' => 'textarea',
                'rows' => 5
            ),
            'options' => array(
                'label' => 'Search Keywords',
            )
                ), array(
            'priority' => 910
        ));

        $this->getStatus(910);

        $this->getSubmitButton();
    }

}
