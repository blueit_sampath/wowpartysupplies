<?php

namespace Product\Form\AdminProduct;

use Common\Form\Form;
use Zend\Form\Element\Select;

class ProductSearchForm extends Form {

    public function __construct($name = "adminProductSearch") {
        parent::__construct($name);
        $this->initPreEvent();
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'class' => 'op-contains',
                'placeholder' => 'Product Title'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'sku',
            'attributes' => array(
                'type' => 'text',
                'class' => 'op-contains',
                'placeholder' => 'SKU'
            )
                ), array(
            'priority' => 990
        ));

        $select = new Select('status');
        $select->setValueOptions(array(
            '' => 'Please select status',
            'true' => 'Enabled',
            'false' => 'Disabled'
        ));
        $select->setAttribute('placeholder', 'Status');
        $this->add($select, array(
            'priority' => 980
        ));


        $this->add(array(
            'name' => 'btnsubmit',
            'type' => 'button',
            'attributes' => array('type' => 'submit'),
            'options' => array('label' => 'Search')
                ), array(
            'priority' => - 1000
        ));
        $this->initPostEvent();
    }

}
