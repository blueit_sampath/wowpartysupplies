<?php

namespace Product\Form\AdminProduct;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 1,
                        'max' => 255
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'sku',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => array(
                            $this,
                            'checkSKU'
                        ),
                        'messages' => array(
                            \Zend\Validator\Callback::INVALID_VALUE => "Product SKU already exists",
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'shortDescription',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'description',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'sellPrice',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));

        $this->add(array(
            'name' => 'costprice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));

        $this->add(array(
            'name' => 'listPrice',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Float'
                )
            )
        ));

        $this->add(array(
            'name' => 'weight',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));

        $this->add(array(
            'name' => 'status',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'Int'
                )
            )
        ));
    }

    public function checkSKU($value, $data = array()) {
        $entity = \Product\Api\ProductApi::getProductBySku($value);
        if (!$entity) {
            return true;
        }
        if ($entity->id == $data['id']) {
            return true;
        }
        return false;
    }

}
