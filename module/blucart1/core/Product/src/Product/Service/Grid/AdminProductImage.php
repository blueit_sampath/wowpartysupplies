<?php

namespace Product\Service\Grid;

use File\Api\FileApi;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use Core\Functions;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;

class AdminProductImage extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'weight',
			'status' 
	);
	protected $_entity = '\Product\Entity\ProductImage';
	protected $_entityName = 'productImage';
	public function getEventName() {
		return 'adminProductImage';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$productId = Functions::fromRoute ( 'productId', 0 );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$grid->setAdditionalParameters ( array (
				'productId' => $productId 
		) );
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'Id' );
		$columnItem->setType ( 'number' );
		$columnItem->setIsPrimary ( true );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'productImage.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'productId' );
		$columnItem->setTitle ( 'Product Id' );
		$columnItem->setType ( 'number' );
		$columnItem->setHidden ( true );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( - 1000 );
		$columns->addColumn ( 'product.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'file.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'alt' );
		$columnItem->setTitle ( 'Alt' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'file.alt', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'filePath' );
		$columnItem->setTitle ( 'Image' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'file.path', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Priority' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'productImage.weight', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 950 );
		$columns->addColumn ( 'productImage.status', $columnItem );
		
		$this->formToolbar ( $grid );
		return $columns;
	}
	public function formToolbar($grid) {
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 600 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$productId = Functions::fromQuery ( 'productId', 0 );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$item = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem ( 'productImage.file', 'file' );
		$item->addJoin ( $joinItem );
		$joinItem = new QueryJoinItem ( 'productImage.product', 'product' );
		$item->addJoin ( $joinItem );
		$item = $queryBuilder->addWhere ( 'product.id', 'productId' );
		$queryBuilder->addParameter ( 'productId', $productId );
		
		return true;
	}
	
	public function beforeUpdate($params, $entity) {
		$entity->file = FileApi::createOrUpdateFile ( $params ['filePath'], $params ['title'], $params ['alt'] );
	}
	public function afterDestroy($params, $entity) {
		$em = $this->getEntityManager ();
		if ($entity) {
			if ($entity->file && $entity->file->path) {
				FileApi::deleteFile ( $entity->file->path );
			}
			if ($entity->file) {
				$em->remove ( $entity->file );
			}
		}
	}
}
