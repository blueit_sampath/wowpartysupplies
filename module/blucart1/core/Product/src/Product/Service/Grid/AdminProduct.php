<?php

namespace Product\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use Core\Functions;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;

class AdminProduct extends AbstractMainBlucartGridEvent {

    protected $_columnKeys = array(
        'id',
        'title',
        'sku',
        'listPrice',
        'costPrice',
        'sellPrice',
        'weight',
        'searchKeywords',
        'status'
    );
    protected $_entity = '\Product\Entity\Product';
    protected $_entityName = 'product';
    protected $_detailTemplateName = 'adminProductImage';
    protected $_detailTemplate = 'common/admin-product-image';

    public function getEventName() {
        return 'adminProduct';
    }

    public function preSchema($e) {
        parent::preSchema($e);

        $array = array();

        $grid = $this->getGrid();
        $columns = $grid->getColumns();

        $columnItem = new ColumnItem ();
        $columnItem->setField('id');
        $columnItem->setTitle('Id');
        $columnItem->setIsPrimary(true);
        $columnItem->setType('number');
        $columnItem->setEditable(false);
        $columnItem->setWeight(1000);
        $columns->addColumn('product.id', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('title');
        $columnItem->setTitle('Title');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(990);
        $columns->addColumn('product.title', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('sku');
        $columnItem->setTitle('SKU');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('product.sku', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('listPrice');
        $columnItem->setTitle('List Price');
        $columnItem->setType('number');
        $columnItem->setFormat('{0:c}');
        $columnItem->setEditable(true);
        $columnItem->setWeight(970);
        $columns->addColumn('product.listPrice', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('costPrice');
        $columnItem->setTitle('Cost Price');
        $columnItem->setFormat('{0:c}');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columns->addColumn('product.costPrice', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('sellPrice');
        $columnItem->setTitle('Sell Price');
        $columnItem->setFormat('{0:c}');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(950);
        $columns->addColumn('product.sellPrice', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('weight');
        $columnItem->setTitle('Priority');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(940);
        $columns->addColumn('product.weight', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('searchKeywords');
        $columnItem->setTitle('Keywords');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(930);
        $columnItem->setHidden(true);
        $columns->addColumn('product.searchKeywords', $columnItem);


        $columnItem = new ColumnItem ();
        $columnItem->setField('status');
        $columnItem->setTitle('Status');
        $columnItem->setType('boolean');
        $columnItem->setEditable(true);
        $columnItem->setWeight(920);
        $columns->addColumn('product.status', $columnItem);

        $this->formToolbar();

        return $columns;
    }

    public function formToolbar() {
        $grid = $this->getGrid();

        $toolbar = $grid->getToolbar();
        if (!$toolbar) {
            $toolbar = new Toolbar ();
            $grid->setToolbar($toolbar);
        }

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(1000);
        $toolbar->addToolbar('save', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('cancel', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }

    public function preRead($e) {
        parent::preRead($e);
        $grid = $this->getGrid();
        $queryBuilder = $this->getGrid()->getQueryBuilder();
        $queryBuilder->addFrom($this->_entity, $this->_entityName);
        $queryBuilder->addGroup('product.id', 'productId');
        return true;
    }

}
