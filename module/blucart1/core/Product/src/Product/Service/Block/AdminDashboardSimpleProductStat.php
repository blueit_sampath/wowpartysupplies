<?php 
namespace Product\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardSimpleProductStat extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-simple-product-stat';

	public function getBlockName() {
		return 'adminDashboardSimpleProductStat';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardSimpleProductStat';
	}
	
}

