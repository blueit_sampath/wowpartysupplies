<?php 
namespace Product\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardProductTables extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-product-tables';

	public function getBlockName() {
		return 'AdminDashboardProductTables';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardProductTables';
	}
	
}

