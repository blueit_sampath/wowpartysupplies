<?php 
namespace Product\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardProductTopGraph extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-product-top-graph';

	public function getBlockName() {
		return 'AdminDashboardProductTopGraph';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardProductTopGraph';
	}
	
}

