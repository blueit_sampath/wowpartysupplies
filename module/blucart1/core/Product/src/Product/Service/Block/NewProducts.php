<?php 
namespace Product\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class NewProducts extends AbstractBlockEvent {

	protected $_blockTemplate = 'products-new';

	public function getBlockName() {
		return 'newProducts';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'New Products';
	}
	
}

