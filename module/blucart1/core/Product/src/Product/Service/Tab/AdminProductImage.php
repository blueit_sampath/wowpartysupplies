<?php 
namespace Product\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminProductImage extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$productId = Functions::fromRoute ( 'productId', 0 );
		if(!$productId){
			return;
		}
		$u = $url ( 'admin-product-image', array (
				'productId' => $productId
		) );
	
		$tabContainer->add ( 'admin-product-image', 'Images', $u,900 );
		
		return $this;
	}
}

