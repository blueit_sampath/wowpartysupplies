<?php 
namespace Product\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminProduct extends AbstractTabEvent {
	public function getEventName() {
		return 'adminProductAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$productId = Functions::fromRoute('productId',null);
		if($productId){
		$u = $url ( 'admin-product-add', array(
				'productId' => $productId
				));
		$tabContainer->add ( 'admin-product-add', 'General Information', $u,1000 );
		}
		
		return $this;
	}
}

