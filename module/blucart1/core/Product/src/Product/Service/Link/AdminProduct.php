<?php 
namespace Product\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProduct extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminProduct';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-product-add');
		$item = $linkContainer->add ( 'admin-product-add', 'Add Product', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

