<?php

namespace Product\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProductView extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminProductView';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$productId = Functions::fromRoute ( 'productId', 0 );
		$u = $url ( 'admin-product-add', array (
				'productId' => $productId 
		) );
		$item = $linkContainer->add ( 'admin-product-add', 'Edit Product', $u, 1000 );
		$item->setLinkClass ( 'zoombox external_link' );
		$item->setImageClass ( 'splashy-pencil' );
		
		return $this;
	}
}

