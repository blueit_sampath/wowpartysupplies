<?php

namespace Product\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminProductImage extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminProductImage';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$productId = Functions::fromRoute ( 'productId', 0 );
		$u = $url ( 'admin-product-image-add', array (
				'productId' => $productId 
		) );
		$item = $linkContainer->add ( 'admin-product-image-add', 'Add Image', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		
		
		$u = $url ( 'admin-product-image-sort', array (
				'productId' => $productId 
		) );
		$item = $linkContainer->add ( 'admin-product-image-sort', 'Arrange Image', $u, 900 );
		$item->setImageClass ( 'splashy-slider_no_pointy_thing' );
		$item->setLinkClass ( 'zoombox ' );
		
		return $this;
	}
}

