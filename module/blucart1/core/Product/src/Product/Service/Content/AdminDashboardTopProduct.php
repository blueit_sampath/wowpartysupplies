<?php 
namespace Product\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardTopProduct extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-top-product';
	protected $_contentName = 'adminDashboardTopProduct';
	public function getEventName() {
		return 'adminDashboardTopStastics';
	}
	public function getPriority() {
		return 970;
	}
	
}

