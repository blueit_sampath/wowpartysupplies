<?php 
namespace Product\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminProductViewImage extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-product-view-image';
	protected $_contentName = 'adminProductViewImage';
	public function getEventName() {
		return 'adminProductView';
	}
	public function getPriority() {
		return 1000;
	}
	
}

