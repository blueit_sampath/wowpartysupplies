<?php 
namespace Product\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardMainContentProduct extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-main-content-product';
	protected $_contentName = 'adminDashboardMainContentProduct';
	public function getEventName() {
		return 'adminDashboardMainContent';
	}
	public function getPriority() {
		return 800;
	}
	
}

