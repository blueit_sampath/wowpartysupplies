<?php 
namespace Product\Service\NestedSorting;

use Core\Functions;
use Product\Api\ProductApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminProductImage extends AbstractNestedSortingEvent {
	protected $_entityName = '\Product\Entity\ProductImage';
	protected $_column = 'path';
	public function getEventName() {
		return 'adminProductImageSort';
	}
	public function getPriority() {
		return 1000;
	}
	

	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		$productId = Functions::fromRoute ( 'productId', 0 );
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (
				'product' => $productId
		), array (
				'weight' => 'desc'
		) );
		if ($results) {
			$column = $this->_column;
			foreach ( $results as $result ) {
				if ($result->file) {
					$array [$result->id] = array (
							'name' => $result->file->$column
					);
				}
			}
		}
		return $array;
	}
}
