<?php 
namespace Product\Service\Navigation;

use Product\Api\ProductApi;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;



class AdminProductNavigation extends AdminNavigationEvent {
	
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'productMain' );
		$page->addPages ( array (
				array (
						'label' => 'Products',
						'route' => 'admin-product',
						'id' => 'admin-product',
						'iconClass' => 'glyphicon glyphicon-th-list' 
				) 
		) );
	}
	
	public function breadcrumb() {
		parent::breadcrumb ();
		$routeName = Functions::getMatchedRouteName ();
	
		if ($routeName === 'admin-product-view') {
			$product = ProductApi::getProductById ( Functions::fromRoute ( 'productId' ) );
			$navigation = $this->getNavigation ();
			$page = $navigation->findOneBy ( 'id', 'admin-product' );
			$page->addPages ( array (
					array (
							'label' => $product->title,
							'route' => 'admin-product-view',
							'params' => array (
									'productId' => $product->id
							),
							'id' => 'admin-product-view'
					)
	
			) );
		}
	}
}

