<?php

namespace Product\Service\Navigation;

use Product\Api\ProductApi;
use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class ProductNavigation extends NavigationEvent {

    protected $_navigation = null;

    public function breadcrumb() {

        $routeName = Functions::getMatchedRouteName();
        if ($routeName != 'product') {
            return false;
        }
        $productId = Functions::fromRoute('productId', 0);
        $product = ProductApi::getProductById($productId);
        if (!$product) {
            return false;
        }
        $page = array(
            'label' => $product->title,
            'route' => 'product',
            'params' => array(
                'productId' => $product->id
            ),
            'id' => 'productId_' . $product->id
        );
        $this->addDefaultMenu($page);
    }

}
