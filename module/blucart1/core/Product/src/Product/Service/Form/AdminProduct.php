<?php

namespace Product\Service\Form;

use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminProduct extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id',
        'title',
        'sku',
        'listPrice' => array('type' => 'float'),
        'costPrice' => array('type' => 'float'),
        'sellPrice' => array('type' => 'float'),
        'weight',
        'shortDescription',
        'description',
        'status',
        'searchKeywords'
    );
    protected $_entity = '\Product\Entity\Product';
    protected $_entityName = 'product';

    public function getFormName() {
        return 'adminProductAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function getId() {
        return Functions::fromRoute('productId');
    }

    public function parseSaveEntity($params, $entity) {
        if ($entity->weight === "") {
            $entity->weight = null;
        }
        return $entity;
    }

    public function afterSaveEntity($entity) {
        $form = $this->getForm();
        $resultContainer = $this->getFormResultContainer();
        parent::afterSaveEntity($entity);
        $resultContainer->add('product', $entity);
    }

}
