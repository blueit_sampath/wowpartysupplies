<?php

namespace Product\Service\Form;

use File\Api\FileApi;
use Product\Api\ProductApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminProductImage extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight',
			'status' 
	);
	protected $_entity = '\Product\Entity\ProductImage';
	protected $_entityName = 'product';
	public function getFormName() {
		return 'adminProductImageAdd';
	}
	public function getPriority() {
		return 1000;
	}
	public function parseSaveEntity($params, $entity) {
		FileApi::deleteFiles ( Functions::fromPost ( 'image_delete', '' ) );
	
		
		$image = $params ['image'];
		if ($image) {
			$array = explode ( ',', $image );
			foreach ( $array as $file ) {
				$imageEntity = FileApi::createOrUpdateFile ( $file, $params ['title'], $params ['alt'] );
			}
		}
		$entity->file = $imageEntity;
		if ($params ['product']) {
			$entity->product = ProductApi::getProductById ( $params ['product'] );
		}
	}
	
	public function afterSaveEntity($entity) {
		$form = $this->getForm ();
		$resultContainer = $this->getFormResultContainer ();
		parent::afterSaveEntity ( $entity );
		$resultContainer->add ( 'productImage', $entity );
		$form->get ( 'product' )->setValue ( $entity->product->id );
		$form->get ( 'id' )->setValue ( $entity->id );
	}
	public function getId() {
		return Functions::fromRoute ( 'productImageId' );
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$form->get ( 'product' )->setValue ( Functions::fromRoute ( 'productId', 0 ) );
		
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		parent::afterGetRecord ( $entity );
		$form->get ( 'title' )->setValue ( $entity->file->title );
		$form->get ( 'alt' )->setValue ( $entity->file->alt );
		$form->get ( 'image' )->setValue ( $entity->file->path );
	}
}
