<?php

namespace Product\Controller;

use Zend\View\Model\ViewModel;

use Core\Functions;

use Common\MVC\Controller\AbstractAdminController;

class AdminProductController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Product\Form\AdminProduct\ProductFormFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-add', array (
				'productId' => $form->get ( 'id' )->getValue () 
		) );
	}
	
}