<?php

namespace Product\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminProductImageController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Product\Form\AdminProductImage\ProductImageFormFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-product-image-add', array (
				'productImageId' => $form->get ( 'id' )->getValue (),
				'productId' => $form->get ( 'product' )->getValue () 
		) );
	}
}