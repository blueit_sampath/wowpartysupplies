<?php
namespace Product\View\Helper;
use Zend\View\Helper\AbstractHelper;

class SellPrice extends AbstractHelper {

    public function __invoke($productId, $isTeaser = true, $onlyValue = false) {

        $product = \Product\Api\ProductApi::getProductById($productId);
        if ($product) {
            if ($onlyValue) {
                return $product->sellPrice;
            }

            return $this->render($product, $isTeaser);
        }
        return false;
    }

    protected function render($product, $isTeaser) {
        $classes = array();
        $classes[] = 'sellPrice';
        if($isTeaser){
            $classes[] = 'teaser';
        }
        else {
              $classes[] = 'full-view';
        }
        $view = $this->getView();
        return '<span id="sellPrice-' . $product->id . '" class="'.  implode(' ', $classes).'">' . $view->currencyFormat($product->sellPrice) . '</span>';
    }

}
