<?php

namespace Product\View\Helper;

use Zend\View\Helper\AbstractHelper;

class ListPrice extends AbstractHelper {

    public function __invoke($productId, $isTeaser = true, $onlyValue = false) {

        $product = \Product\Api\ProductApi::getProductById($productId);

        if ($product) {
            if ($product->listPrice && $product->listPrice > $product->sellPrice) {
                if ($onlyValue) {
                    return $product->listPrice;
                }

                return $this->render($product, $isTeaser);
            } 
        }
        return false;
    }

    protected function render($product, $isTeaser) {
        $classes = array();
        $classes[] = 'listPrice';
        if ($isTeaser) {
            $classes[] = 'teaser';
        } else {
            $classes[] = 'full-view';
        }
        $view = $this->getView();
        return '<span id="listPrice-' . $product->id . '" class="' . implode(' ', $classes) . '">' . $view->currencyFormat($product->listPrice) . '</span>';
    }

}
