<?php

namespace Product\Api;

use Common\Api\Api;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;

class ProductReportApi extends Api {
	protected static $_entity = '\Product\Entity\Product';
	protected static $_imageEntity = '\Product\Entity\ProductImage';
	public static function getAllProductsCount( $status, $startDate, $endDate) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'count(product.id)', 'countProductId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'product.status', 'productStatus' );
			$queryBuilder->addParameter ( 'productStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'product' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		// $item = $queryBuilder->addWhere ( 'product.id', 'productId' );
		// $queryBuilder->addParameter ( 'productId', $productId );
		return $queryBuilder->executeSingleScalarQuery ( 'count' );
	}
	
	public static function getAllProductCountByDay($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
	
		$queryBuilder->addColumn ( "DateFormat(product.createdDate, '%D %M,%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(product.id)', 'countProductId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'product.status', 'productStatus' );
			$queryBuilder->addParameter ( 'productStatus', $status );
		}
	
		$item = $queryBuilder->addFrom ( static::$_entity, 'product' );
	
		if ($startDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
	
		if ($endDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
	
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	
	public static function getAllProductCountByWeek($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
	
		$queryBuilder->addColumn ( "DateFormat(product.createdDate, '%v,%x')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(product.id)', 'countProductId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'product.status', 'productStatus' );
			$queryBuilder->addParameter ( 'productStatus', $status );
		}
	
		$item = $queryBuilder->addFrom ( static::$_entity, 'product' );
	
		if ($startDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
	
		if ($endDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
	
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	
	public static function getAllProductCountByMonth($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(product.createdDate, '%M,%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(product.id)', 'countProductId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'product.status', 'productStatus' );
			$queryBuilder->addParameter ( 'productStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'product' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	
	public static function getAllProductCountByYear($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
	
		$queryBuilder->addColumn ( "DateFormat(product.createdDate, '%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(product.id)', 'countProductId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'product.status', 'productStatus' );
			$queryBuilder->addParameter ( 'productStatus', $status );
		}
	
		$item = $queryBuilder->addFrom ( static::$_entity, 'product' );
	
		if ($startDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
	
		if ($endDate) {
			$queryBuilder->addWhere ( 'product.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
	
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
}
