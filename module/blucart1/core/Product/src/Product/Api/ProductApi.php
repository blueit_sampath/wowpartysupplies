<?php

namespace Product\Api;

use Common\Api\Api;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;

class ProductApi extends Api {

    protected static $_entity = '\Product\Entity\Product';
    protected static $_imageEntity = '\Product\Entity\ProductImage';

    public static function getProductById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getProductBySku($sku) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
                    'sku' => $sku
                        ), array(
                    'weight' => 'desc'
        ));
    }

    public static function getAllProducts($status = true) {
        $em = Functions::getEntityManager();
        $array = array();
        $orderArray = array();
        if ($status !== null) {
            $array['status'] = $status;
        }
        $orderArray['weight'] = 'desc';
       
        return $em->getRepository(static::$_entity)->findBy($array,$orderArray);
    }

    public static function getProducts($page = 1, $perPage = PHP_INT_MAX, $orderBy = array('product.weight' => array(
            'name' => 'productWeight',
            'order' => 'desc'
        ),
        'product.id' => array(
            'name' => 'productId',
            'order' => 'desc'
        )
    )) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('product.id', 'productId');
        $item = $queryBuilder->addFrom(static::$_entity, 'product');

        $whereItem = $queryBuilder->addWhere('product.status', 'productStatus');
        $queryBuilder->addParameter('productStatus', 1);

        if ($orderBy) {
            foreach ($orderBy as $key => $value) {
                if (isset($value['order'])) {
                    $queryBuilder->addOrder($key, $value['name'], $value['order']);
                }
            }
        }
        $queryBuilder->addGroup('product.id', 'productId');

        $results = $queryBuilder->executeQuery(($page - 1) * $perPage, $perPage);

        return $results;
    }

    public static function getProductImageByProductId($productId, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('productImage.id', 'productImageId');

        if ($status !== null) {
            $queryBuilder->addWhere('productImage.status', 'productImageStatus');
            $queryBuilder->addParameter('productImageStatus', $status);
        }
        $queryBuilder->addOrder('productImage.weight', 'productImageWeight', 'desc');

        $item = $queryBuilder->addFrom(static::$_imageEntity, 'productImage');
        $joinItem = new QueryJoinItem('productImage.file', 'file');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('productImage.product', 'product');
        $item->addJoin($joinItem);
        $item = $queryBuilder->addWhere('product.id', 'productId');
        $queryBuilder->addParameter('productId', $productId);
        return $queryBuilder->executeQuery();
    }

    public static function getProductImageById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_imageEntity, $id);
    }

    public static function getTopProductImageByProductId($productId, $status = true) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_imageEntity)->findOneBy(array(
                    'status' => $status,
                    'product' => $productId
                        ), array(
                    'weight' => 'desc'
        ));
    }

    public static function getBottomProductImageByProductId($productId, $status = true) {
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_imageEntity)->findOneBy(array(
                    'status' => $status,
                    'product' => $productId
                        ), array(
                    'weight' => 'asc',
                    'id' => 'desc'
        ));
    }

}
