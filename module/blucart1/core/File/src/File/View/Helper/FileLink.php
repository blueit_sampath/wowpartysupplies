<?php

namespace File\View\Helper;

use Core\Functions;
use Zend\Form\View\Helper\AbstractHelper;

class FileLink extends AbstractHelper {
	public function __invoke($fileName = '') {
		$sm = Functions::getServiceLocator ();
		$config = $sm->get ( 'Config' );
		$fileName = $config ['fileupload'] ['upload_url'] . $fileName;
		
		return $this->getView ()->basePath ( $fileName );
	}
}
