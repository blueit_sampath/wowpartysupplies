<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/FileUpload for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace File\Controller;

use Zend\View\Model\JsonModel;
use File\Service\CustomUploadHandler;
use Zend\Mvc\Controller\AbstractActionController;

class FileUploadController extends AbstractActionController {
	public function indexAction() {
		$config = $this->getServiceLocator ()->get ( 'Config' );
		$options = $config ['file_options'];
		
		new CustomUploadHandler ( $options );
		$res = $this->getResponse ()->setContent ( '' );
		return $res;
	}
}
