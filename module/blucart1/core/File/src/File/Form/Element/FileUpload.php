<?php

namespace File\Form\Element;

use Zend\Form\Element\Hidden;

class FileUpload extends Hidden {
	protected $_acceptFileTypes = '/(\.|\/)(gif|jpe?g|png|pdf|doc|txt|docx|xls|xlsx|ppt|pptx)$/i';
	protected $_maxFileSize = "20000000";
	protected $_maxWidth = "1400";
	protected $_maxHeight = "900";
	protected $_buttonText = "Add Files";
	protected $_fileTypes = '/^image\/(gif|jpeg|png)$/';
	protected $_maxNumberOfFiles = 1;
	public function setValue($value) {
		if (is_array ( $value )) {
			return parent::setValue ( $value );
		}
		return parent::setValue ( explode ( ',', $value ) );
	}
	
	/**
	 * Prepare the form element (mostly used for rendering purposes)
	 *
	 * @param Form $form        	
	 * @return mixed
	 */
	public function prepareElement(Form $form) {
		// Ensure the form is using correct enctype
		$form->setAttribute ( 'enctype', 'multipart/form-data' );
	}
	/**
	 *
	 * @return the $_acceptFileTypes
	 */
	public function getAcceptFileTypes() {
		return $this->_acceptFileTypes;
	}
	
	/**
	 *
	 * @param string $_acceptFileTypes        	
	 */
	public function setAcceptFileTypes($_acceptFileTypes) {
		$this->_acceptFileTypes = $_acceptFileTypes;
	}
	
	/**
	 *
	 * @return the $_maxFileSize
	 */
	public function getMaxFileSize() {
		return $this->_maxFileSize;
	}
	
	/**
	 *
	 * @param string $_maxFileSize        	
	 */
	public function setMaxFileSize($_maxFileSize) {
		$this->_maxFileSize = $_maxFileSize;
	}
	
	/**
	 *
	 * @return the $_maxWidth
	 */
	public function getMaxWidth() {
		return $this->_maxWidth;
	}
	
	/**
	 *
	 * @param string $_maxWidth        	
	 */
	public function setMaxWidth($_maxWidth) {
		$this->_maxWidth = $_maxWidth;
	}
	
	/**
	 *
	 * @return the $_maxHeight
	 */
	public function getMaxHeight() {
		return $this->_maxHeight;
	}
	
	/**
	 *
	 * @param string $_maxHeight        	
	 */
	public function setMaxHeight($_maxHeight) {
		$this->_maxHeight = $_maxHeight;
	}
	
	/**
	 *
	 * @return the $_buttonText
	 */
	public function getButtonText() {
		return $this->_buttonText;
	}
	
	/**
	 *
	 * @param string $_buttonText        	
	 */
	public function setButtonText($_buttonText) {
		$this->_buttonText = $_buttonText;
	}
	
	/**
	 *
	 * @return the $_fileTypes
	 */
	public function getFileTypes() {
		return $this->_fileTypes;
	}
	
	/**
	 *
	 * @param string $_fileTypes        	
	 */
	public function setFileTypes($_fileTypes) {
		$this->_fileTypes = $_fileTypes;
	}
	/**
	 *
	 * @return the $_maxNumberOfFiles
	 */
	public function getMaxNumberOfFiles() {
		return $this->_maxNumberOfFiles;
	}
	
	/**
	 *
	 * @param number $_maxNumberOfFiles        	
	 */
	public function setMaxNumberOfFiles($_maxNumberOfFiles) {
		$this->_maxNumberOfFiles = $_maxNumberOfFiles;
	}
}

