<?php

namespace File\Form\View\Helper;

use Zend\Form\View\Helper\FormHidden;

use DluTwBootstrap\Form\View\Helper\FormHiddenTwb;
use Zend\Form\View\Helper\FormHidden as ZFFormHidden;
use File\Form\Element\FileUpload;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;

class FormFileUpload extends ZFFormHidden {
	
	// protected $_url;
	// protected $_basePath;
	public function render(ElementInterface $element) {
		if ($element instanceof FileUpload) {
			$name = $element->getName ();
			if ($name === null || $name === '') {
				throw new Exception\DomainException ( sprintf ( '%s requires that the element has an assigned name; none discovered', __METHOD__ ) );
			}
			
			$attributes = $element->getAttributes ();
			$attributes ['name'] = $name;
			$attributes ['type'] = $this->getType ( $element );
			
			$attributes ['value'] = $element->getValue () ? implode ( ',', $element->getValue () ) : '';
			$view = $this->getView ();
			$string = $view->render ( 'common/file-element', array (
					'element' => $element,
					'attributeString' => $this->createAttributesString ( $attributes ),
					'closeBracketString' => $this->getInlineClosingBracket (),
					'url' => $this->getUrl () 
			) );
			return $string;
			
			// return sprintf ( '<input %s%s', $this->createAttributesString (
			// $attributes ), $this->getInlineClosingBracket () );
		} else {
			return parent::render ( $element );
		}
	}
	public function setUrl() {
	}
	public function getUrl() {
		return $this->getView ()->url ( 'fileupload' );
	}
}