<?php
namespace File\Form\View\Helper;


use File\Form\Element;
use Zend\Form\View\Helper\FormElement as BaseFormElement;
use Zend\Form\ElementInterface;

class FormElement extends \TwbBundle\Form\View\Helper\TwbBundleFormElement
{
    public function render(ElementInterface $element)
    {
        $renderer = $this->getView();
       
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }

        if ($element instanceof Element\FileUpload) {
            $helper = $renderer->plugin('form_file_upload');
            return $helper($element);
        }

        
        
        return parent::render($element);
    }
}

