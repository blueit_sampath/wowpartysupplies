<?php

namespace File\Api;

use Core\Functions;

class FileApi {

    protected static $_entity = '\File\Entity\File';

    public static function getFileById($id) {
        $em = Functions::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getFileByName($name) {
        $name = trim($name);
        $em = Functions::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
                    'path' => $name
        ));
    }

    public static function getUploadUrl() {
        $config = Functions::getServiceLocator()->get('Config');
        return $config ['file_options'] ['upload_url'];
    }

    public static function getUploadDir() {
        $config = Functions::getServiceLocator()->get('Config');
        return $config ['file_options'] ['upload_dir'];
    }

    public static function getUploadThumbnailConfiguration() {
        $config = Functions::getServiceLocator()->get('Config');
        return $config ['file_options'] ['image_versions'];
    }

    public static function deleteFile($file) {
        $em = Functions::getEntityManager();
        $dql = " delete from " . static::$_entity . " u where u.path ='" . $file . "'";
        $em->createQuery($dql)->execute();

        $file = static::getUploadDir() . '/' . $file;

        if (file_exists($file)) {
            unlink($file);
        }
        return true;
    }

    public static function deleteFiles($fileSepratedWithComma) {
        if (!is_array($fileSepratedWithComma)) {
            $array = explode(',', $fileSepratedWithComma);
        } else {
            $array = $fileSepratedWithComma;
        }

        foreach ($array as $file) {
            if ($file) {
                static::deleteFile($file);
            }
        }

        return true;
    }

    public static function createOrUpdateFile($file, $title = '', $alt = '') {
        $em = Functions::getEntityManager();
        $entity = new static::$_entity ();
        if (!($entity = static::getFileByName($file))) {
            $entity = new static::$_entity ();
        }

        $entity->path = $file;
        $entity->title = $title;
        $entity->alt = $alt;
        $em->persist($entity);
        $em->flush();
        return $entity;
    }

    public static function formImage($image, $width, $height, $attributes = array(), $linkOnly = false, $color = null) {

        try {
            $wideImage = new \WideImage ();
            $basePath = Functions::getServiceLocator()->get('viewhelpermanager')->get('basePath');

            $folderName = $width . 'X' . $height;

            $uploadDir = static::getUploadDir();
            $uploadUrl = static::getUploadUrl();

            $baseDir = $uploadDir;
            $dir = $uploadDir . '/' . $folderName;
            if (!is_dir($dir)) {
                if (!file_exists($dir)) {
                    mkdir($dir);
                }
            }
            $uploadDir = $dir;
            $uploadUrl = $uploadUrl . '/' . $folderName;
            //return "<img src='" . $image . "' " . $string . " >";exit;

            if (!file_exists($baseDir . '/' . $image)) {
                return false;
            }

            if ((!file_exists($uploadDir . '/' . $image)) || (filemtime($uploadDir . '/' . $image) < filemtime($baseDir . '/' . $image))) {


                $wideImage->load($baseDir . '/' . $image)->resize($width, $height)->saveToFile($uploadDir . '/' . $image);

                if ($color) {
                    $wideImage->load($uploadDir . '/' . $image)->resizeCanvas($width, $height, 'center', 'center', hexdec($color))->saveToFile($uploadDir . '/' . $image);
                }
            }
            $string = '';
            foreach ($attributes as $key => $attribute) {
                $string .= ' ' . $key . " = '" . $attribute . "' ";
            }

            if ($linkOnly) {
                return static::removeDoubleSlashes($basePath($uploadUrl . '/' . $image));
            }

            return "<img src='" . static::removeDoubleSlashes($basePath($uploadUrl . '/' . $image)) . "' " . $string . " >";
        } catch (\Exception $e) {
            
        }

        return '';
    }

    public static function getImageUrl($string, $force_canonical = false) {
        $serverUrl = Functions::getServiceLocator()->get('viewhelpermanager')->get('serverUrl');
        $basePath = Functions::getServiceLocator()->get('viewhelpermanager')->get('basePath');
        $uploadUrl = static::getUploadUrl();
        if ($force_canonical) {
            return $serverUrl(static::removeDoubleSlashes($basePath($uploadUrl . '/' . $string)));
        }
        return static::removeDoubleSlashes($basePath($uploadUrl . '/' . $string));
    }

    public static function removeDoubleSlashes($string) {

        return str_replace('//', '/', $string);
    }

    public static function saveImageOnDisk($imagePath, $imageName = null, $title = '', $alt = '') {
        $wideImage = new \WideImage ();
        $uploadDir = static::getUploadDir();
        if (!$imageName) {
            $imageName = time() . '_' . uniqid();
        }
        $imageName = static::formUserFriendlyImageName($imageName);
        $temp1 = 0;
        do {

            $fileName = $imageName;
            if ($temp1 != 0) {
                $fileName .= '-' . $temp1;
            }
            $fileName .= '.png';
            $temp1 ++;
        } while (is_file($uploadDir . '/' . $fileName));


        $img = $uploadDir . '/' . $fileName;


        $wideImage->load($imagePath)->saveToFile($img);

        $thumbnailArray = static::getUploadThumbnailConfiguration();

        foreach ($thumbnailArray as $key => $value) {
            $path = $uploadDir . '/' . $key . '/';
            $wideImage->load($imagePath)->resize($value['max_width'], $value['max_height'])->saveToFile($path . $fileName);
        }
        $file = static::createOrUpdateFile($fileName, $title, $alt);
        return $file;
    }

    public static function formUserFriendlyImageName($string) {
        return strtolower(trim(preg_replace('~[^0-9a-z/]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }

}
