<?php

namespace File\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminFile extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'title',
			'alt',
			'path' 
	);
	protected $_entity = 'File\Entity\File';
	protected $_entityName = 'file';
	public function getEventName() {
		return 'adminFile';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'file.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'file.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'alt' );
		$columnItem->setTitle ( 'Alt' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'file.alt', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'path' );
		$columnItem->setTitle ( 'Path' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'file.path', $columnItem );
		
		$this->formToolbar ( $grid );
		return $columns;
	}
	public function formToolbar($grid) {
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'create' );
		$toolBarItem->setText ( 'Add New File' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'create', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 600 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
}
