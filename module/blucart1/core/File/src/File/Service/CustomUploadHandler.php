<?php

namespace File\Service;

class CustomUploadHandler extends BaseUploadHandler {
	public function __construct($options = null, $initialize = true) {
		return parent::__construct ( $options, $initialize );
	}
	
	/*
	 * modifying the get
	 */
	protected function get_file_objects($iteration_method = 'get_file_object') {
		$images = explode ( ',', $_REQUEST ['images'] );
		
		$upload_dir = $this->get_upload_path ();
		
		$array = array ();
		foreach ( $images as $image ) {
			if (! $this->is_valid_file_object ( $image )) {
				continue;
			}
			$array [] = $image;
		}
		
		if (! is_dir ( $upload_dir )) {
			return array ();
		}
		
		return array_values ( array_filter ( array_map ( array (
				$this,
				$iteration_method 
		), $array ) ) );
	}
}
