<?php

namespace File;

class Module {

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
       
        $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
            $this,
            'attachAssets'
        ));
        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();
        $viewHelperPluginManager = $serviceManager->get('view_helper_manager');
        $viewHelperPluginManager->setInvokableClass('formelement', 'File\Form\View\Helper\FormElement');
        $viewHelperPluginManager->setInvokableClass('formFileUpload', 'File\Form\View\Helper\FormFileUpload');

        require_once __DIR__ . '/src/File/lib/WideImage.php';
    }

    public function attachAssets($e) {
        $controller = $e->getTarget();

        if (!($controller instanceof \Zend\Mvc\Controller\AbstractActionController)) {
            return;
        }

        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();
        $viewHelperPluginManager = $serviceManager->get('view_helper_manager');


        $config = $serviceManager->get('config');
        $headScript = $viewHelperPluginManager->get('headScript');
        $headLink = $viewHelperPluginManager->get('headLink');
        $basePath = $viewHelperPluginManager->get('basePath');
        if ($config ['file_options'] ['include_css']) {
            $headLink()->appendStylesheet($basePath('/assets/file/css/fileupload.css'));
        }
        if ($config ['file_options'] ['include_js']) {
            $headScript()->appendFile($basePath('/assets/file/js/fileupload.js'));
            $headScript()->appendFile($basePath('/assets/file/js/cors/jquery.xdr-transport.js'), 'text/javascript', array(
                'conditional' => 'gte IE 8'
            ));
        }
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

}
