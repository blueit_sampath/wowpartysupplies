<?php
namespace File;

return array (
		'router' => array (
				'routes' => array (
						'admin-file' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/file',
										'defaults' => array (
												'controller' => 'File\Controller\AdminFile',
												'action' => 'index' 
										) 
								) 
						),
						'fileupload' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/fileupload',
										
										'defaults' => array (
												'controller' => __NAMESPACE__ . '\Controller\FileUpload',
												'action' => 'index' 
										) 
								) 
						) 
				) 
		),
		
		
		'file_options' => array (
				'script_url' => '/fileupload',
				'upload_dir' => __DIR__ . '/../public/files/',
				'upload_url' => '/files/',
				'user_dirs' => false,
				'mkdir_mode' => 0755,
				'param_name' => 'files',
				'delete_type' => 'DELETE',
				'access_control_allow_origin' => '*',
				'access_control_allow_credentials' => false,
				'access_control_allow_methods' => array (
						'OPTIONS',
						'HEAD',
						'GET',
						'POST',
						'PUT',
						'PATCH',
						'DELETE' 
				),
				'access_control_allow_headers' => array (
						'Content-Type',
						'Content-Range',
						'Content-Disposition' 
				),
				'download_via_php' => false,
				'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
				'accept_file_types' => '/.+$/i',
				'max_file_size' => null,
				'min_file_size' => 1,
				'max_number_of_files' => null,
				'max_width' => null,
				'max_height' => null,
				'min_width' => 1,
				'min_height' => 1,
				'discard_aborted_uploads' => true,
				'orient_image' => false,
				'image_versions' => array (
						'thumbnail' => array (
								'max_width' => 80,
								'max_height' => 80 
						) 
				),
				'include_js' => true,
				'include_css' => true 
		),
		
		'events' => array (
				'File\Service\Grid\AdminFile' => 'File\Service\Grid\AdminFile' 
		),
		'service_manager' => array (
				'invokables' => array (
						'File\Service\Grid\AdminFile' => 'File\Service\Grid\AdminFile' 
				),
				'factories' => array () 
		),
		'controllers' => array (
				'invokables' => array (
						'File\Controller\AdminFile' => 'File\Controller\AdminFileController' ,
						'File\Controller\FileUpload' => 'File\Controller\FileUploadController'
				) 
		),
		'view_manager' => array (
				'template_path_stack' => array (
						__NAMESPACE__ => __DIR__ . '/../view' 
				) 
		),
		'doctrine' => array (
				'driver' => array (
						__NAMESPACE__ . '_driver' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity' 
								) 
						),
						'orm_default' => array (
								'drivers' => array (
										__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver' 
								) 
						) 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'collections' => array (
								'assets/file/js/fileupload.js' => array (
										'/assets/file/js/tmpl.min.js',
										'/assets/file/js/load-image.min.js',
										"/assets/file/js/canvas-to-blob.min.js",
										"/assets/file/js/jquery.blueimp-gallery.min.js",
										"/assets/file/js/jquery.iframe-transport.js",
										"/assets/file/js/jquery.fileupload.js",
										"/assets/file/js/jquery.fileupload-process.js",
										"/assets/file/js/jquery.fileupload-image.js",
										"/assets/file/js/jquery.fileupload-audio.js",
										"/assets/file/js/jquery.fileupload-video.js",
										"/assets/file/js/jquery.fileupload-validate.js",
										"/assets/file/js/jquery.fileupload-ui.js"
										
								),
								'assets/file/css/fileupload.css' => array (
										'/assets/file/css/style.css',
										'/assets/file/css/jquery.fileupload-ui.css',
										//'/assets/file/css/jquery.fileupload-ui-noscript.css' 
								) 
						)
						,
						'paths' => array (
								'File' => __DIR__.'/../public' 
						) 
				) 
		),
		
);




