<?php
namespace MenuManager\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminMenuConfigController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('MenuManager\Form\AdminMenuConfig\MenuConfigFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-menumanager-config');
    }
}