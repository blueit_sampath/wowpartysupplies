<?php

namespace MenuManager\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminMenuManagerOptionController extends AbstractAdminController {

    public function getFormFactory() {
        return $this->getServiceLocator()->get('MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionFactory');
    }

    public function getSaveRedirector() {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-menumanager-option-add', array(
                    'menuId' => $form->get('menu')->getValue(),
                    'menuOptionId' => $form->get('id')->getValue()
                ));
    }

}
