<?php 
namespace MenuManager\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminMenuManagerController extends AbstractAdminController {

	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'MenuManager\Form\AdminMenuManager\MenuManagerFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-menumanager-add', array (
				'menuId' => $form->get ( 'id' )->getValue () 
		) );
	}
}