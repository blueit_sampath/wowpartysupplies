<?php

namespace MenuManager\Form\AdminMenuManager;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class MenuManagerForm extends Form {

    public function init() {
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Title'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Name'
            )
                ), array(
            'priority' => 990
        ));
        
         $this->add(array(
            'name' => 'menuClass',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Menu Class'
            )
                ), array(
            'priority' => 980
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isCollapasable',
            'options' => array(
                'label' => 'Is Collapasable',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 970
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isVerticalMenu',
            'options' => array(
                'label' => 'Is Vertical Menu',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 970
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isBreadcrumb',
            'options' => array(
                'label' => 'Use for breadcrumb',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 960
        ));
        
         
         $this->add(array(
            'name' => 'weight',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Priority'
            )
                ), array(
            'priority' => 950
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
