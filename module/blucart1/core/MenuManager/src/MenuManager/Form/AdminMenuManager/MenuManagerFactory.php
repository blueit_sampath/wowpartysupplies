<?php
namespace MenuManager\Form\AdminMenuManager;

use Common\Form\Option\AbstractFormFactory;

class MenuManagerFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'MenuManager\Form\AdminMenuManager\MenuManagerForm' );
		$form->setName ( 'adminMenuManagerAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'MenuManager\Form\AdminMenuManager\MenuManagerFilter' );
	}
}
