<?php

namespace MenuManager\Form\AdminMenuConfig;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class MenuConfigForm extends Form {

    public function init() {
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));



        $this->add(array(
            'name' => 'BREADCRUMB_HOME_LABEL',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Breadcrumb Home Label'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'BREADCRUMB_ENABLE_HOME',
            'options' => array(
                'label' => 'Display home in breadcrumb',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 990
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'BREADCRUMB_LAST_LINK',
            'options' => array(
                'label' => 'Enable Last Link',
                'use_hidden_element' => true
            )
                ), array(
            'priority' => 990
        ));

        $this->add(array(
            'name' => 'BREADCRUMB_MIN_DEPTH',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Breadcrumb minimum depth'
            )
                ), array(
            'priority' => 1000
        ));
        $this->add(array(
            'name' => 'BREADCRUMB_MAX_DEPTH',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Breadcrumb max depth'
            )
                ), array(
            'priority' => 1000
        ));

       

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
