<?php
namespace MenuManager\Form\AdminMenuConfig;
use Zend\InputFilter\InputFilter;


class MenuConfigFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {

        $this->add(array(
            'name' => 'BREADCRUMB_HOME_LABEL',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));
        
         $this->add(array(
            'name' => 'BREADCRUMB_MIN_DEPTH',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            ),
             'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));
         
         $this->add(array(
            'name' => 'BREADCRUMB_MAX_DEPTH',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            ),
             'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));
         
        $this->add(array(
            'name' => 'BREADCRUMB_ENABLE_HOME',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));
        
        $this->add(array(
            'name' => 'BREADCRUMB_LAST_LINK',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));
        
       
    }

}
