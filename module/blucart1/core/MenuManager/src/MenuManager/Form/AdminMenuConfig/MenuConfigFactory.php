<?php
namespace MenuManager\Form\AdminMenuConfig;

use Common\Form\Option\AbstractFormFactory;

class MenuConfigFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'MenuManager\Form\AdminMenuConfig\MenuConfigForm' );
		$form->setName ( 'adminMenuConfig' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'MenuManager\Form\AdminMenuConfig\MenuConfigFilter' );
	}
}
