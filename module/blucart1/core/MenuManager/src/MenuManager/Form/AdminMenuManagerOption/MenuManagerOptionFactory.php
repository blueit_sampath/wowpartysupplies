<?php
namespace MenuManager\Form\AdminMenuManagerOption;

use Common\Form\Option\AbstractFormFactory;

class MenuManagerOptionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionForm' );
		$form->setName ( 'adminMenuManagerOptionAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionFilter' );
	}
}
