<?php

namespace MenuManager\Form\AdminMenuManagerOption;

use Common\Form\Form;

class MenuManagerOptionForm extends Form {

    public function init() {
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'name' => 'menu',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Name'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Title'
            )
                ), array(
            'priority' => 990
        ));

      

        $this->add(array(
            'name' => 'uri',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'URI'
            )
                ), array(
            'priority' => 960
        ));

        $this->add(array(
            'name' => 'linkTarget',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Link Target'
            )
                ), array(
            'priority' => 950
        ));


        $select = new \Zend\Form\Element\Select('parent');
        $select->setLabel('Parent Menu Option');
        $select->setValueOptions($this->getMenuOptions());
        $this->add($select, array(
            'priority' => 940
        ));

        $this->add(array(
            'name' => 'iconClass',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Icon Class'
            )
                ), array(
            'priority' => 930
        ));

        $this->add(array(
            'name' => 'linkClass',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Link Class'
            )
                ), array(
            'priority' => 920
        ));

        $this->add(array(
            'name' => 'weight',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Priority'
            )
                ), array(
            'priority' => 910
        ));
        $this->getStatus(900);

        $select = new \Zend\Form\Element\Select('displayTo');
        $select->setLabel('Display To');
        $select->setValueOptions(array(
            \MenuManager\Api\MenuManagerApi::BOTH_USER => 'All',
            \MenuManager\Api\MenuManagerApi::GUEST_USER => 'Only Guest',
            \MenuManager\Api\MenuManagerApi::LOGGED_IN_USER => 'Only Logged In User',
        ));
        $this->add($select, array(
            'priority' => 890
        ));

        $this->add(array(
            'name' => 'btnsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

    public function getMenuOptions() {
        $array = array(
            '' => ''
        );
        $menuId = \Core\Functions::fromRoute('menuId');
        $menuOptionId = \Core\Functions::fromRoute('menuOptionId');
        $results = \MenuManager\Api\MenuManagerApi::getChildrenMenuOptions($menuId, null, null);

        $result = $this->formOptions($results, $menuId, $menuOptionId);
        return $array + $result;
    }

    public function formOptions($array, $menuId, $id, $level = 0) {
        $result = array();

        foreach ($array as $res) {
            $menuOptionId = $res ['menuOptionId'];

            if ($id == $menuOptionId) {
                continue;
            }
            $menuOption = \MenuManager\Api\MenuManagerApi::getMenuOptionById($menuOptionId);
            $space = '';

            $name = '';
            $parent = $menuOption;
            while ($parent->parent) {
                $name = $parent->parent->name . ' > ' . $name;
                $parent = $parent->parent;
            }

            if (!$name) {
                $result [$menuOption->id] = $menuOption->name;
            } else {
                $result [$menuOption->id] = $name . $menuOption->name;
            }

            $children = \MenuManager\Api\MenuManagerApi::getChildrenMenuOptions($menuId, $menuOption->id, null);
            if (count($children)) {
                $result = $result + $this->formOptions($children, $menuId, $id, ($level + 1));
            }
        }

        return $result;
    }

}
