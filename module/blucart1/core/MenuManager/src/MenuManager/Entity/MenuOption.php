<?php

namespace MenuManager\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * MenuOption
 *
 * @ORM\Table(name="menu_option")
 * @ORM\Entity
 */
class MenuOption {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255, nullable=true)
     */
    private $uri;

    /**
     * @var string
     *
     * @ORM\Column(name="link_class", type="string", length=255, nullable=true)
     */
    private $linkClass;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_class", type="string", length=255, nullable=true)
     */
    private $iconClass;

    /**
     * @var string
     *
     * @ORM\Column(name="weight", type="integer",  nullable=true)
     */
    private $weight;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="display_to", type="integer",  nullable=true)
     */
    private $displayTo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="link_target", type="string", length=255, nullable=true)
     */
    private $linkTarget;

    /**
     * @var boolen
     *
     * @ORM\Column(name="status", type="boolean",  nullable=true)
     */
    private $status;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="create")
     */
    private $createdDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="update")
     */
    private $updatedDate;

    /**
     * @var \Menu
     *
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $menu;

    /**
     * @var \MenuOption
     *
     * @ORM\ManyToOne(targetEntity="MenuOption")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
     * })
     */
    private $parent;

    public function __get($property) {
        if (\method_exists($this, "get" . \ucfirst($property))) {
            $method_name = "get" . \ucfirst($property);
            return $this->$method_name();
        } else {

            if (is_object($this->$property)) {
                // return $this->$property->id;
            }
            return $this->$property;
        }
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     *
     */
    public function __set($property, $value) {
        if (\method_exists($this, "set" . \ucfirst($property))) {
            $method_name = "set" . \ucfirst($property);

            $this->$method_name($value);
        } else
            $this->$property = $value;
    }

}
