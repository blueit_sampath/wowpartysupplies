<?php

namespace MenuManager\Api;

class MenuManagerApi extends \Common\Api\Api {

    protected static $_entity = '\MenuManager\Entity\Menu';
    protected static $_entityOption = '\MenuManager\Entity\MenuOption';

    const GUEST_USER = 2;
    const LOGGED_IN_USER = 1;
    const BOTH_USER = 0;

    public static function getMenuById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getMenuOptionById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_entityOption, $id);
    }

    
    public static function getMenuOptionByName($name) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entityOption)->findOneBy(array('name' => $name));
    }

    public static function getAllMenus() {
        $em = static::getEntityManager();
        try{
        return $em->getRepository(static::$_entity)->findBy(array(), array('weight' => 'desc'));
        }
        catch(\Doctrine\DBAL\DBALException $e){
            return array();
        }
        return false;
    }

    public static function getMenuByName($name) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array('name' => $name));
    }

    public static function getBreadcrumbMenus() {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entity)->findBy(array('isBreadcrumb' => 1), array('weight' => 'desc'));
    }

    public static function getChildrenMenuOptions($menuId, $menuOptionId, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('menuOption.id', 'menuOptionId');
        $queryBuilder->addFrom(static::$_entityOption, 'menuOption');

        if ($status !== null) {
            $queryBuilder->addWhere('menuOption.status', 'menuOptionStatus');
            $queryBuilder->addParameter('menuOptionStatus', $status);
        }

        if ($menuId) {
            $queryBuilder->addWhere('menuOption.menu', 'menuOptionMenu');
            $queryBuilder->addParameter('menuOptionMenu', $menuId);
        }
        if ($menuOptionId === null) {
            $queryBuilder->addWhere('menuOption.parent', 'menuOptionParent', null);
        } else {
            $queryBuilder->addWhere('menuOption.parent', 'menuOptionParent');
            $queryBuilder->addParameter('menuOptionParent', $menuOptionId);
        }

        $queryBuilder->addOrder('menuOption.weight', 'menuOptionWeight', 'desc');

        return $queryBuilder->executeQuery();
    }

    public static function convertMenuToNavigationArray($menuId) {
        $array = static::getChildrenMenuOptions($menuId, null, null);
        return static::makeArray($array, $menuId);
    }

    public static function makeArray($array, $menuId) {
        $result = array();
        foreach ($array as $categoryResults) {
            $catId = $categoryResults ['menuOptionId'];
            $menuOption = static::getMenuOptionById($catId);
            if (!$menuOption->status) {
                continue;
            }

            if ($menuOption->displayTo) {
               
                $user = \User\Api\UserApi::getLoggedInUser();
                if($menuOption->displayTo == static::GUEST_USER && $user){
                    continue;
                }
                if($menuOption->displayTo == static::LOGGED_IN_USER && !$user){
                    continue;
                }
            }

            $res = array();

            $res['id'] = $menuOption->id;
            if ($menuOption->title) {
                $res['label'] = $menuOption->title;
            }


            if ($menuOption->iconClass) {
                $res['iconClass'] = $menuOption->iconClass;
            }

            if ($menuOption->linkClass) {
                $res['linkClass'] = $menuOption->linkClass;
            }

            if ($menuOption->linkTarget) {
                $res['target'] = $menuOption->linkTarget;
            }
             $router = \Core\Functions::getServiceLocator()->get('Router');
            $basePathPlugin = \Core\Functions::getViewHelperManager()->get('basepath');
             $res['uri'] = '#';
            
            if ($menuOption->uri) {
                $res['uri'] = $menuOption->uri;
                $request = new \Zend\Http\Request();
                $request->setUri($basePathPlugin($menuOption->uri));
                $matchedRoute = $router->match($request);
                if(null !== $matchedRoute){
                   $res['route'] = $matchedRoute->getMatchedRouteName();
                   $res['params'] = $matchedRoute->getParams();
                }
            }
           
            $children = static::getChildrenMenuOptions($menuId, $menuOption->id, null);
            if (count($children)) {
                $res['pages'] = static::makeArray($children, $menuId);
            }
            $result [] = $res;
        }
        
        return $result;
    }

}
