<?php

namespace MenuManager\Service\NestedSorting;

use Core\Functions;
use MenuManager\Api\MenuManagerApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminMenuManagerOptionSort extends AbstractNestedSortingEvent {

    protected $_entityName = '\MenuManager\Entity\MenuOption';

    public function getEventName() {
        return 'adminMenuManagerOptionSort';
    }

    public function getPriority() {
        return 1000;
    }

    public function getSortedArray() {
       $menuId = Functions::fromRoute('menuId',0);
        $array = MenuManagerApi::getChildrenMenuOptions($menuId,null, null);
        return $this->makeArray($array,$menuId);
    }

    public function makeArray($array,$menuId) {
        $result = array();
        foreach ($array as $categoryResults) {
            $catId = $categoryResults ['menuOptionId'];
            $menuOption = MenuManagerApi::getMenuOptionById($catId);
            $result [$menuOption->id] = array(
                'name' => $menuOption->title
            );

            $children = MenuManagerApi::getChildrenMenuOptions($menuId,$menuOption->id, null);
            if (count($children)) {

                $result [$menuOption->id] ['children'] = $this->makeArray($children,$menuId);
            }
        }
        return $result;
    }

    public function saveList($e) {
        $lists = Functions::getServiceLocator()->get('Request')->getPost('list');
        $weight = count($lists) * 10;
        $em = $this->getEntityManager();
        foreach ($lists as $key => $c) {

            $model = $em->find($this->_entityName, $key);

            $model->weight = $weight;
            if ($c) {
                $model->parent = $em->find($this->_entityName, $c);
            } else {
                $model->parent = null;
            }
            $em->persist($model);
            $em->flush();

            $weight = $weight - 10;
        }

        return $this;
    }

}
