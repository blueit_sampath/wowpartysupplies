<?php 
namespace MenuManager\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminMenuManagerAddTab extends AbstractTabEvent {
	public function getEventName() {
		return 'adminMenuManagerAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		$menuId = Functions::fromRoute ( 'menuId', null );
                if($menuId){
		$u = $url ( 'admin-menumanager-add',array('menuId' => $menuId));
		$tabContainer->add ( 'admin-menumanager-add', 'General', $u,1000 );
                }
		
                
                
		$menuId = Functions::fromRoute ( 'menuId', null );
		if ($menuId) {
			$u = $url ( 'admin-menumanager-option', array (
					'menuId' => $menuId 
			) );
			$tabContainer->add ( 'admin-menumanager-option', 'Menu Option', $u, null );
		}
		
		
		return $this;
	}
}

