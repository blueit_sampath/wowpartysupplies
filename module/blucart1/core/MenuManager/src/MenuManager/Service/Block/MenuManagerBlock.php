<?php

namespace MenuManager\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;
use Zend\EventManager\EventManagerInterface;

class MenuManagerBlock extends AbstractBlockEvent {

    protected $_blockTemplate = 'menu-block';

    public function getBlockName() {
        return 'menuBlock';
    }

    public function getTitle() {
        return 'Menu Block';
    }

    public function getPriority() {
        return 1000;
    }

    public function attach(EventManagerInterface $events) {
        $priority = $this->getPriority();
        $sharedEventManager = $events->getSharedManager();
        $this->_listeners [] = $sharedEventManager->attach('*', 'block-render-name', array(
            $this,
            'preRenderName'
                ), $priority);
        $array = array();
        $results = \MenuManager\Api\MenuManagerApi::getAllMenus();
        foreach ($results as $menu) {


            $array [] = $menu->name . '_' . $this->getBlockName() . '-block-render';
        }

        $this->_listeners [] = $sharedEventManager->attach('*', $array, array(
            $this,
            'preRender'
                ), $priority);

        return $this;
    }

    public function renderName($e) {
        $blockContainer = $this->getBlockContainer();
        $results = \MenuManager\Api\MenuManagerApi::getAllMenus();
        foreach ($results as $menu) {
            $blockContainer->add($menu->name . '_' . $this->getBlockName(), $menu->title);
        }
        return true;
    }

    public function render($e) {
        $eventName = $e->getName();


        $blockName = preg_match("/(.*?)_" . $this->getBlockName() . "-block-render$/", $eventName, $match) ? $match [1] : "";

        if (!$blockName) {
            return;
        }

        $entity = \MenuManager\Api\MenuManagerApi::getMenuByName($blockName);

        if (!$entity) {
            return;
        }
        $factory = new \MenuManager\Navigation\Service\MenuManagerNavigationFactory();
        $factory->setEventName($entity->name);
        $navigation = $factory->createService($this->getServiceLocator());
        if ($this->_blockTemplate) {
            $blockContainer = $this->getBlockContainer();

            $renderer = Functions::getServiceLocator()->get('viewrenderer');
            $item = $blockContainer->add($entity->name . '_' . $this->getBlockName(), $entity->title);
            $renderer->render($this->getBlockTemplate(), array(
                'item' => $item,
                'blockContainer' => $blockContainer,
                'entity' => $entity,
                'navigation' => $navigation
            ));
        }
    }

}
