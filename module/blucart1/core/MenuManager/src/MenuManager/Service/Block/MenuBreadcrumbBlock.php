<?php

namespace MenuManager\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;
use Zend\EventManager\EventManagerInterface;

class MenuBreadcrumbBlock extends AbstractBlockEvent {

    protected $_blockTemplate = 'menu-breadcrumb-block';

    public function getBlockName() {
        return 'menuBreadcrumbBlock';
    }

    public function getTitle() {
        return 'Menu Breadcrumb Block';
    }

    public function getPriority() {
        return 1000;
    }

    

}
