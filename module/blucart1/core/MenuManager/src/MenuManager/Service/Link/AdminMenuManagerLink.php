<?php 
namespace MenuManager\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminMenuManagerLink extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminMenuManager';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-menumanager-add');
		$item = $linkContainer->add ( 'admin-menumanager-add', 'Add Menu', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

