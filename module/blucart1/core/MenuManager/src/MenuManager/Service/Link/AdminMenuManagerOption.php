<?php

namespace MenuManager\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminMenuManagerOption extends AbstractLinkEvent {

    public function getEventName() {
        return 'adminMenuManagerOption';
    }

    public function link() {
        $linkContainer = $this->getLinkContainer();
        $url = Functions::getUrlPlugin();

        $menuId = Functions::fromRoute('menuId', null);
        if ($menuId) {
            $u = $url('admin-menumanager-option-add', array('menuId' => $menuId));
            $item = $linkContainer->add('admin-menumanager-option-add', 'Add Menu Option', $u, null);
            $item->setLinkClass('zoombox ');

            $u = $url('admin-menumanager-option-sort', array('menuId' => $menuId));
            $item = $linkContainer->add('admin-menumanager-option-sort', 'Arrange Menu Option', $u, null);
            $item->setLinkClass('zoombox ');
        }

        return $this;
    }

}
