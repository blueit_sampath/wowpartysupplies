<?php

namespace MenuManager\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminMenuManager extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id', 'title', 'name', 'menuClass',
        'isCollapasable',
        'isBreadcrumb',
        'weight', 'isVerticalMenu');
    protected $_entity = '\MenuManager\Entity\Menu';
    protected $_entityName = 'menu';

    public function getFormName() {
        return 'adminMenuManagerAdd';
    }

    public function getPriority() {
        return 1000;
    }

    public function afterGetRecord($entity) {
        parent::afterGetRecord($entity);
        $form = $this->getForm();

        $form->get('name')->setAttribute('readonly', true);
    }

}
