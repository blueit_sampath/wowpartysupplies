<?php

namespace MenuManager\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminMenuManagerOption extends AbstractMainFormEvent {

    protected $_columnKeys = array(
        'id', 'title', 'name'=>'name', 'uri',  'status',  'weight', 'linkClass', 'iconClass', 'linkTarget', 'displayTo'
    );
    protected $_entity = '\MenuManager\Entity\MenuOption';
    protected $_entityName = 'menuOption';

    public function getFormName() {
        return 'adminMenuManagerOptionAdd';
    }

    public function getPriority() {
        return;
    }

    public function parseSaveEntity($params, $entity) {
        parent::parseSaveEntity($params, $entity);
        if (isset($params ['parent'])) {
            if ($params ['parent']) {
                $entity->parent = \MenuManager\Api\MenuManagerApi::getMenuOptionById($params ['parent']);
            } else {
                $entity->parent = null;
            }
        }

        if ($params ['menu']) {
            $entity->menu = \MenuManager\Api\MenuManagerApi::getMenuById($params ['menu']);
        }
        return $entity;
    }

    public function beforeGetRecord() {
        $form = $this->getForm();
        $form->get('menu')->setValue(Functions::fromRoute('menuId', 0));
        
    }

    public function afterGetRecord($entity) {
        parent::afterGetRecord($entity);
        $form = $this->getForm();
        if ($entity->parent) {
            $form->get('parent')->setValue($entity->parent->id);
        }
        $form->get('name')->setAttribute('readonly', true);
    }

}
