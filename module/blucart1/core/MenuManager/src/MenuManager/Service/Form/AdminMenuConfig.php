<?php

namespace MenuManager\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminMenuConfig extends \Config\Service\Form\AdminConfig {

    protected $_columnKeys = array(
        'BREADCRUMB_ENABLE_HOME',
        'BREADCRUMB_HOME_LABEL',
        'BREADCRUMB_LAST_LINK',
        'BREADCRUMB_MIN_DEPTH',
        'BREADCRUMB_MAX_DEPTH'
    );

    public function getFormName() {
        return 'adminMenuConfig';
    }

    public function getPriority() {
        return 1000;
    }

}
