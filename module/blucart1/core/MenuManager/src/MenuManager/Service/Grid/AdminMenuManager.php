<?php

namespace MenuManager\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminMenuManager extends AbstractMainBlucartGridEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'title',
        'menuClass',
        'isCollapasable',
        'isBreadcrumb',
        'weight'
    );
    protected $_entity = '\MenuManager\Entity\Menu';
    protected $_entityName = 'menu';

    public function getEventName() {
        return 'adminMenuManager';
    }

    public function preSchema($e) {
        parent::preSchema($e);

        $array = array();

        $grid = $this->getGrid();
        $columns = $grid->getColumns();


        $columnItem = new ColumnItem ();
        $columnItem->setField('id');
        $columnItem->setTitle('ID');
        $columnItem->setType('number');
        $columnItem->setEditable(false);
        $columnItem->setWeight(1000);
        $columnItem->setIsPrimary(true);
        $columns->addColumn('menu.id', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('name');
        $columnItem->setTitle('Name');
        $columnItem->setType('string');
        $columnItem->setEditable(false);
        $columnItem->setWeight(990);
        $columns->addColumn('menu.name', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('title');
        $columnItem->setTitle('Title');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('menu.title', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('menuClass');
        $columnItem->setTitle('Menu Class');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(970);
        $columns->addColumn('menu.menuClass', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('isCollapasable');
        $columnItem->setTitle('Is Collapasable');
        $columnItem->setType('boolean');
        $columnItem->setEditable(true);
        $columnItem->setWeight(960);
        $columns->addColumn('menu.isCollapasable', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('isBreadcrumb');
        $columnItem->setTitle('Is Breadcrumb');
        $columnItem->setType('boolean');
        $columnItem->setEditable(true);
        $columnItem->setWeight(950);
        $columns->addColumn('menu.isBreadcrumb', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('weight');
        $columnItem->setTitle('Weight');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(940);
        $columns->addColumn('menu.weight', $columnItem);




        $this->formToolbar();

        return $columns;
    }

    public function formToolbar() {
        $grid = $this->getGrid();


        $toolbar = $grid->getToolbar();
        if (!$toolbar) {
            $toolbar = new Toolbar ();
            $grid->setToolbar($toolbar);
        }

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(1000);
        $toolbar->addToolbar('save', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('cancel', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }

}
