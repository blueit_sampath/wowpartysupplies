<?php

namespace MenuManager\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminMenuManagerOption extends AbstractMainBlucartGridEvent {

    protected $_columnKeys = array(
        'id',
        'name',
        'title',
        'weight',
        
        'uri',
        'linkClass',
        'iconClass',
        'linkTarget',
        'status',
        'displayTo'
    );
    protected $_entity = '\MenuManager\Entity\MenuOption';
    protected $_entityName = 'menuOption';

    public function getEventName() {
        return 'adminMenuManagerOption';
    }

    public function preSchema($e) {
        parent::preSchema($e);

        $array = array();

        $grid = $this->getGrid();
        $columns = $grid->getColumns();
        $menuId = \Core\Functions::fromRoute('menuId', 0);

        $grid->setAdditionalParameters(array(
            'menuId' => $menuId
        ));

        $columns = $grid->getColumns();

        $columnItem = new ColumnItem ();
        $columnItem->setField('id');
        $columnItem->setTitle('ID');
        $columnItem->setType('number');
        $columnItem->setIsPrimary(true);
        $columnItem->setEditable(false);
        $columnItem->setWeight(1000);
        $columns->addColumn('menuOption.id', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('name');
        $columnItem->setTitle('Name');
        $columnItem->setType('string');
        $columnItem->setEditable(false);
        $columnItem->setWeight(990);
        $columns->addColumn('menuOption.name', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('title');
        $columnItem->setTitle('Title');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(980);
        $columns->addColumn('menuOption.title', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('parentName');
        $columnItem->setTitle('Parent Menu');
        $columnItem->setType('string');
        $columnItem->setEditable(false);
        $columnItem->setWeight(960);
        $columns->addColumn('menuOptionParent.name', $columnItem);

      

        $columnItem = new ColumnItem ();
        $columnItem->setField('uri');
        $columnItem->setTitle('URI');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(950);
        $columns->addColumn('menuOption.uri', $columnItem);


        $columnItem = new ColumnItem ();
        $columnItem->setField('linkClass');
        $columnItem->setTitle('Link Class');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(940);
        $columns->addColumn('menuOption.linkClass', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('iconClass');
        $columnItem->setTitle('Icon Class');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(930);
        $columns->addColumn('menuOption.iconClass', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('linkTarget');
        $columnItem->setTitle('Link Target');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(930);
        $columns->addColumn('menuOption.linkTarget', $columnItem);

        $columnItem = new ColumnItem ();
        $columnItem->setField('weight');
        $columnItem->setTitle('Priority');
        $columnItem->setType('number');
        $columnItem->setEditable(true);
        $columnItem->setWeight(920);
        $columns->addColumn('menuOption.weight', $columnItem);



        $columnItem = new ColumnItem ();
        $columnItem->setField('displayTo');
        $columnItem->setTitle('Display To');
        $columnItem->setType('string');
        $columnItem->setEditable(true);
        $columnItem->setWeight(910);
        $columnItem->setValues(array(
            array(
                'value' => \MenuManager\Api\MenuManagerApi::BOTH_USER,
                'text' => 'All',
            ),
            array(
                'value' => \MenuManager\Api\MenuManagerApi::GUEST_USER,
                'text' => 'Guest',
            ),
            array(
                'value' => \MenuManager\Api\MenuManagerApi::LOGGED_IN_USER,
                'text' => 'Logged In',
            ),
        ));
        $columns->addColumn('menuOption.displayTo', $columnItem);
        
        $columnItem = new ColumnItem ();
        $columnItem->setField('status');
        $columnItem->setTitle('Status');
        $columnItem->setType('boolean');
        $columnItem->setEditable(true);
        $columnItem->setWeight(900);
        $columns->addColumn('menuOption.status', $columnItem);

        $this->formToolbar();

        return $columns;
    }

    public function preRead($e) {
        parent::preRead($e);
        $menuId = \Core\Functions::fromQuery('menuId', 0);
        $grid = $this->getGrid();
        $queryBuilder = $this->getGrid()->getQueryBuilder();
        $fromItem = $queryBuilder->addFrom($this->_entity, $this->_entityName);
        $joinItem = new \QueryBuilder\Option\QueryFrom\QueryJoinItem('menuOption.parent', 'menuOptionParent');
        $fromItem->addJoin($joinItem);

        $joinItem = new \QueryBuilder\Option\QueryFrom\QueryJoinItem($this->_entityName . '.menu', 'menu');
        $fromItem->addJoin($joinItem);

        $item = $queryBuilder->addWhere('menu.id', 'menuId');
        $queryBuilder->addParameter('menuId', $menuId);
        return true;
    }

    public function formToolbar() {
        $grid = $this->getGrid();


        $toolbar = $grid->getToolbar();
        if (!$toolbar) {
            $toolbar = new Toolbar ();
            $grid->setToolbar($toolbar);
        }

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('save');
        $toolBarItem->setWeight(1000);
        $toolbar->addToolbar('save', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('cancel');
        $toolBarItem->setWeight(900);
        $toolbar->addToolbar('cancel', $toolBarItem);

        $toolBarItem = new ToolbarItem ();
        $toolBarItem->setName('destroy');
        $toolBarItem->setWeight(800);
        $toolbar->addToolbar('destroy', $toolBarItem);
    }

}
