<?php

namespace MenuManager\Service\Navigation;

use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class AdminMenuManagerMenu extends \Admin\Navigation\Event\AdminNavigationEvent {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'websiteMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Menu Manager',
                    'uri' => '#',
                    'id' => 'menuManagerMain',
                    'iconClass' => 'glyphicon-wrench'
                )
            ));
        }

        $page = $navigation->findOneBy('id', 'menuManagerMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Menu',
                    'route' => 'admin-menumanager',
                    'id' => 'admin-menumanager',
                    'iconClass' => 'glyphicon-font'
                )
            ));
        }

        $page = $navigation->findOneBy('id', 'menuManagerMain');
        if ($page) {
            $page->addPages(array(
                array(
                    'label' => 'Menu Settings',
                    'route' => 'admin-menumanager-config',
                    'id' => 'admin-menumanager-config',
                    'iconClass' => 'glyphicon-font',
                    'aClass' => 'zoombox'
                )
            ));
        }
    }

}
