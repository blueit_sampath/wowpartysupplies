<?php

namespace MenuManager\Navigation\Service;

use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\Navigation\Exception;
use Zend\Navigation\Navigation;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class MenuManagerBreadcrumbFactory extends DefaultNavigationFactory implements EventManagerAwareInterface {

    protected $_events;
    protected $_eventName = 'menu_breadcrumb';
    protected $_defaultPages = array();

    protected function getName() {
        return 'menu_breadcrumb';
    }

    public function setEventName($eventName) {
        $this->_eventName = $eventName;
    }

    public function getEventName() {
        return $this->_eventName;
    }

    public function setDefaultPages($defaultPages) {
        $this->_defaultPages = $pages;
    }

    /**
     *
     * @param ServiceLocatorInterface $serviceLocator        	
     * @return array
     * @throws \Zend\Navigation\Exception\InvalidArgumentException
     */
    protected function getPages(ServiceLocatorInterface $serviceLocator) {
        if (null === $this->pages) {
            $configuration = $serviceLocator->get('Config');

            if (!isset($configuration ['navigation'])) {
                throw new Exception\InvalidArgumentException('Could not find navigation configuration key');
            }
            if (!isset($configuration ['navigation'] [$this->getName()])) {
                $menus = \MenuManager\Api\MenuManagerApi::getBreadcrumbMenus();
                $array = array(
                );
                foreach ($menus as $menu) {
                    $temp = \MenuManager\Api\MenuManagerApi::convertMenuToNavigationArray($menu->id);

                    $array = array_merge($array, $temp);
                }

                $configuration ['navigation'] [$this->getName()] = $array;
            }
            $navigation = new Navigation($configuration ['navigation'] [$this->getName()]);
            $params ['navigation'] = $navigation;

            $this->getEventManager()->trigger($this->getEventName() . 'Navigation', $this, $params);

            $array = $this->removeEmpty($navigation->toArray());

          
            foreach ($array as $key => $subarray) {
                $url = \Core\Functions::getCurrentUrl(true);
                if (!$this->recursiveArraySearch($url, $subarray)) {
                    unset($array[$key]);
                }
            }
            
            $breadCrumb = array();
            if (count($array)) {
                $breadCrumb = array(array_shift($array));
            }
            $result = \Config\Api\ConfigApi::getConfigByKey('BREADCRUMB_ENABLE_HOME', true);
            if ($result) {
                $homeLabel = \Config\Api\ConfigApi::getConfigByKey('BREADCRUMB_HOME_LABEL');
                $res = array();
                $res['id'] = 'home';
                $res['label'] = $homeLabel ? $homeLabel : 'Home';
                $res['route'] = 'home';
                $res['uri'] = '/';
                $res['pages'] = $breadCrumb;
                $breadCrumb = array($res);
            }

           
            $pages = $this->getPagesFromConfig($breadCrumb);
            $this->pages = $this->preparePages($serviceLocator, $pages);
        }
        return $this->pages;
    }

    protected function recursiveArraySearch($needle, $haystack) {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $this->addBasePath($value) OR (is_array($value) && $this->recursiveArraySearch($needle, $value) !== false)) {
              
                return $current_key;
            }
        }
        return false;
    }

    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    public function getEventManager() {
        if (!$this->_events) {
            $this->setEventManager(new EventManager(array(
                __CLASS__,
                get_called_class()
            )));
        }
        return $this->_events;
    }

    public function removeEmpty($haystack) {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack [$key] = $this->removeEmpty($haystack [$key]);
            }

            if (empty($haystack [$key])) {
                unset($haystack [$key]);
            }
        }

        return $haystack;
    }

    protected function addBasePath($url) {
        if(!is_string($url)){
            return $url;
        }
        $basePathPlugin = \Core\Functions::getViewHelperManager()->get('basePath');
        
        if (strpos($url, $basePathPlugin('/')) === 0) {
            return $url;
        }

        return $basePathPlugin($url);
    }

}
