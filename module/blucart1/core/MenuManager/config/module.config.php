<?php

return array(
    'router' => array('routes' => array(
            'admin-menumanager' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/menumanager',
                    'defaults' => array(
                        'controller' => 'MenuManager\Controller\AdminMenuManager',
                        'action' => 'index'
                    )
                )
            ),
            'admin-menumanager-config' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/menumanager/config[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'MenuManager\Controller\AdminMenuConfig',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-menumanager-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/menumanager/add[/ajax/:ajax][/:menuId]',
                    'defaults' => array(
                        'controller' => 'MenuManager\Controller\AdminMenuManager',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'menuId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-menumanager-option' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/menumanager/option[/ajax/:ajax][/:menuId]',
                    'defaults' => array(
                        'controller' => 'MenuManager\Controller\AdminMenuManagerOption',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'menuId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-menumanager-option-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/menumanager/option/add[/ajax/:ajax][/:menuId][/:menuOptionId]',
                    'defaults' => array(
                        'controller' => 'MenuManager\Controller\AdminMenuManagerOption',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'menuId' => '[0-9]+',
                        'menuOptionId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-menumanager-option-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/menumanager/option/sort[/ajax/:ajax][/:menuId]',
                    'defaults' => array(
                        'controller' => 'MenuManager\Controller\AdminMenuManagerOption',
                        'action' => 'sort',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'menuId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            )
        )),
    'events' => array(
        'MenuManager\Service\Grid\AdminMenuManager' => 'MenuManager\Service\Grid\AdminMenuManager',
        'MenuManager\Service\Form\AdminMenuManager' => 'MenuManager\Service\Form\AdminMenuManager',
        'MenuManager\Service\Link\AdminMenuManagerLink' => 'MenuManager\Service\Link\AdminMenuManagerLink',
        'MenuManager\Service\Tab\AdminMenuManagerAddTab' => 'MenuManager\Service\Tab\AdminMenuManagerAddTab',
        'MenuManager\Service\Navigation\AdminMenuManagerMenu' => 'MenuManager\Service\Navigation\AdminMenuManagerMenu',
        'MenuManager\Service\Form\AdminMenuManagerOption' => 'MenuManager\Service\Form\AdminMenuManagerOption',
        'MenuManager\Service\Grid\AdminMenuManagerOption' => 'MenuManager\Service\Grid\AdminMenuManagerOption',
        'MenuManager\Service\Link\AdminMenuManagerOption' => 'MenuManager\Service\Link\AdminMenuManagerOption',
        'MenuManager\Service\NestedSorting\AdminMenuManagerOptionSort' => 'MenuManager\Service\NestedSorting\AdminMenuManagerOptionSort',
        'MenuManager\Service\Block\MenuManagerBlock' => 'MenuManager\Service\Block\MenuManagerBlock',
        'MenuManager\Service\Block\MenuBreadcrumbBlock' => 'MenuManager\Service\Block\MenuBreadcrumbBlock',
        'MenuManager\Service\Form\AdminMenuConfig' => 'MenuManager\Service\Form\AdminMenuConfig'
    ),
    'service_manager' => array(
        'invokables' => array(
            'MenuManager\Service\Grid\AdminMenuManager' => 'MenuManager\Service\Grid\AdminMenuManager',
            'MenuManager\Form\AdminMenuManager\MenuManagerForm' => 'MenuManager\Form\AdminMenuManager\MenuManagerForm',
            'MenuManager\Form\AdminMenuManager\MenuManagerFilter' => 'MenuManager\Form\AdminMenuManager\MenuManagerFilter',
            'MenuManager\Service\Form\AdminMenuManager' => 'MenuManager\Service\Form\AdminMenuManager',
            'MenuManager\Service\Link\AdminMenuManagerLink' => 'MenuManager\Service\Link\AdminMenuManagerLink',
            'MenuManager\Service\Tab\AdminMenuManagerAddTab' => 'MenuManager\Service\Tab\AdminMenuManagerAddTab',
            'MenuManager\Service\Navigation\AdminMenuManagerMenu' => 'MenuManager\Service\Navigation\AdminMenuManagerMenu',
            'MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionForm' => 'MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionForm',
            'MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionFilter' => 'MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionFilter',
            'MenuManager\Service\Form\AdminMenuManagerOption' => 'MenuManager\Service\Form\AdminMenuManagerOption',
            'MenuManager\Service\Grid\AdminMenuManagerOption' => 'MenuManager\Service\Grid\AdminMenuManagerOption',
            'MenuManager\Service\Link\AdminMenuManagerOption' => 'MenuManager\Service\Link\AdminMenuManagerOption',
            'MenuManager\Service\NestedSorting\AdminMenuManagerOptionSort' => 'MenuManager\Service\NestedSorting\AdminMenuManagerOptionSort',
            'MenuManager\Service\Block\MenuManagerBlock' => 'MenuManager\Service\Block\MenuManagerBlock',
            'MenuManager\Service\Block\MenuBreadcrumbBlock' => 'MenuManager\Service\Block\MenuBreadcrumbBlock',
            'MenuManager\Form\AdminMenuConfig\MenuConfigForm' => 'MenuManager\Form\AdminMenuConfig\MenuConfigForm',
            'MenuManager\Form\AdminMenuConfig\MenuConfigFilter' => 'MenuManager\Form\AdminMenuConfig\MenuConfigFilter',
            'MenuManager\Service\Form\AdminMenuConfig' => 'MenuManager\Service\Form\AdminMenuConfig'
        ),
        'factories' => array(
            'MenuManager\Form\AdminMenuManager\MenuManagerFactory' => 'MenuManager\Form\AdminMenuManager\MenuManagerFactory',
            'MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionFactory' => 'MenuManager\Form\AdminMenuManagerOption\MenuManagerOptionFactory',
            'menu_manager' => 'MenuManager\Navigation\Service\MenuManagerNavigationFactory',
            'menu_breadcrumb' => 'MenuManager\Navigation\Service\MenuManagerBreadcrumbFactory',
            'MenuManager\Form\AdminMenuConfig\MenuConfigFactory' => 'MenuManager\Form\AdminMenuConfig\MenuConfigFactory'
        )
    ),
    'controllers' => array('invokables' => array(
            'MenuManager\Controller\AdminMenuManager' => 'MenuManager\Controller\AdminMenuManagerController',
            'MenuManager\Controller\AdminMenuManagerOption' => 'MenuManager\Controller\AdminMenuManagerOptionController',
            'MenuManager\Controller\AdminMenuConfig' => 'MenuManager\Controller\AdminMenuConfigController'
        )),
    'view_manager' => array('template_path_stack' => array('MenuManager' => __DIR__ . '/../view')),
    'doctrine' => array('driver' => array(
            'MenuManager_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/MenuManager/Entity')
            ),
            'orm_default' => array('drivers' => array('MenuManager\Entity' => 'MenuManager_driver'))
        )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('MenuManager' => __DIR__ . '/../public')))
);
