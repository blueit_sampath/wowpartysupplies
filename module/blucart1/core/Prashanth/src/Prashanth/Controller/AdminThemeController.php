<?php

namespace Prashanth\Controller;

use Common\MVC\Controller\AbstractAdminController;
use Admin\Api\AdminApi;

class AdminThemeController extends AbstractAdminController {

    
    public function indexAction() {

        $directories = \Config\Api\ConfigApi::getConfigByKey('THEME_DIR', array());

        $theme = $this->params()->fromRoute('theme');
        if ($theme) {
            // \Config\Api\ConfigApi::createOrUpdateKey('BLUCART_CURRENT_THEME', $theme);
            $this->writeFile($theme);
            return $this->redirect()->toRoute('admin-theme');
        }

        $dir = array();

        foreach ($directories as $path) {

            foreach (new \DirectoryIterator($path) as $file) {

                if ($file->isDot())
                    continue;
                if ($file->isDir()) {
                    $temp = array();
                    $temp['name'] = $file->getFilename();
                    if (file_exists($path . '/' . $file->getFilename() . '/preview/screenshot.png')) {
                        $temp['image'] = $path . '/' . $file->getFilename() . '/preview//screenshot.png';
                    }
                    if (file_exists($path . '/' . $file->getFilename() . '/preview/screenshot_large.png')) {
                        $temp['largeImage'] = $path . '/' . $file->getFilename() . '/preview/screenshot_large.png';
                    }
                    $dir[] = $temp;
                }
            }
        }

        return array('dir' => $dir);
    }

    public function previewAction() {
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', "image/png");

        $image = $this->params()->fromQuery('image', false);

        if ($image) {



            if (file_exists($image) !== false) {
                $imagegetcontent = @file_get_contents($image);

                $response->setStatusCode(200);
                $response->setContent($imagegetcontent);
            }
        }
        return $response;
    }

    public function writeFile($theme) {
        $array = array();
        if (file_exists('./config/autoload/theme.global.php')) {
            $array = include './config/autoload/theme.global.php';
        }
        $array['THEME_MODULE'] = $theme;
        $array['BLUCART_CURRENT_THEME'] = $theme;
        $config = new \Zend\Config\Config($array);
        $writer = new \Zend\Config\Writer\PhpArray();
        $writer->toFile('./config/autoload/theme.global.php', $config);
    }

}
