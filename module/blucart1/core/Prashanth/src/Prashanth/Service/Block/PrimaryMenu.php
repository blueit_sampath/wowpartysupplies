<?php 
namespace Prashanth\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class PrimaryMenu extends AbstractBlockEvent {

	protected $_blockTemplate = 'primary-menu';

	public function getBlockName() {
		return 'PrimaryMenu';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'Primary Menu';
	}
	
}

