<?php 
namespace Prashanth\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class PageTitle extends AbstractBlockEvent {

	protected $_blockTemplate = 'page-title';

	public function getBlockName() {
		return 'pageTitle';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'Page Title';
	}
	
}

