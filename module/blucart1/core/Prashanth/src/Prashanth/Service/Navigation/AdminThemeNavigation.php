<?php

namespace Prashanth\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;

class AdminThemeNavigation extends AdminNavigationEvent {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'websiteMain');
        $page->addPages(array(
            array(
                'label' => 'Themes/Designs/Templates',
                'route' => 'admin-theme',
                'id' => 'admin-theme',
                'iconClass' => 'glyphicon-font'
            )
        ));
    }

}
