<?php

namespace Prashanth\Navigation\Service;

use Common\Navigation\Service\MenuNavigationFactory;

class FrontLinkNavigationFactory extends MenuNavigationFactory {
	protected function getName() {
		return 'menu_link';
	}
	public function getEventName() {
		return 'menu-link';
	}
}
