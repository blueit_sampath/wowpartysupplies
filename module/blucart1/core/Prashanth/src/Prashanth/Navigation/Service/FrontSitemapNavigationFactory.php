<?php
namespace Prashanth\Navigation\Service;
use Common\Navigation\Service\MenuNavigationFactory;

class FrontSitemapNavigationFactory extends MenuNavigationFactory {
	protected function getName() {
		return 'front_sitemap';
	}
	protected function getEventName() {
		return 'front-sitemap';
	}
	
}
