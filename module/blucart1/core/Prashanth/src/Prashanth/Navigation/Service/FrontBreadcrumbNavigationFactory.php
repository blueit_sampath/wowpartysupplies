<?php

namespace Prashanth\Navigation\Service;


use Common\Navigation\Service\MenuNavigationFactory;

class FrontBreadcrumbNavigationFactory extends MenuNavigationFactory {
	protected function getName() {
		return 'front_breadcrumb';
	}
	
	
	public function getEventName(){
		return 'front-breadcrumb';
	}
	
	
}
