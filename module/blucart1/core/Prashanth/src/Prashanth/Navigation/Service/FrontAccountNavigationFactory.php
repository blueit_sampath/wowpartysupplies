<?php

namespace Prashanth\Navigation\Service;


use Common\Navigation\Service\MenuNavigationFactory;

class FrontAccountNavigationFactory extends MenuNavigationFactory {
	protected function getName() {
		return 'front_account';
	}
	
	
	public function getEventName(){
		return 'front-account';
	}
	
	
}
