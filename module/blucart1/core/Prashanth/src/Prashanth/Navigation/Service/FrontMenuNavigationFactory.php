<?php

namespace Prashanth\Navigation\Service;

use Common\Navigation\Service\MenuNavigationFactory;


class FrontMenuNavigationFactory extends MenuNavigationFactory {
	
	protected function getName() {
		return 'front_menu';
	}
	
	public function getEventName(){
		return 'front-menu';
	}
	
	
}
