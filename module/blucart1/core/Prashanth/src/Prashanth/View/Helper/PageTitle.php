<?php

namespace Prashanth\View\Helper;

use Core\Functions;
use Zend\Form\View\Helper\AbstractHelper;

class PageTitle extends AbstractHelper {
	protected $_title = '';
	public function __invoke($title = '') {
		if ($title) {
			$this->_title = $title;
		}
		return $this->_title;
	}
}
