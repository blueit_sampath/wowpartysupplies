<?php

return array(
    'controllers' => array('invokables' => array('Prashanth\Controller\AdminTheme' => 'Prashanth\Controller\AdminThemeController')),
    'router' => array('routes' => array(
            'admin-theme' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/theme[/:theme]',
                    'defaults' => array(
                        'controller' => 'Prashanth\Controller\AdminTheme',
                        'action' => 'index'
                    )
                )
            ),
            'admin-theme-preview' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/theme/preview',
                    'defaults' => array(
                        'controller' => 'Prashanth\Controller\AdminTheme',
                        'action' => 'preview'
                    )
                )
            )
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
            'front_menu' => 'Prashanth\Navigation\Service\FrontMenuNavigationFactory',
            'front_breadcrumb' => 'Prashanth\Navigation\Service\FrontBreadcrumbNavigationFactory',
            'front_sitemap' => 'Prashanth\Navigation\Service\FrontSitemapNavigationFactory',
            'front_link' => 'Prashanth\Navigation\Service\FrontLinkNavigationFactory',
            'front_account' => 'Prashanth\Navigation\Service\FrontAccountNavigationFactory'
        ),
        'invokables' => array(
            'Prashanth\Service\Block\PrimaryMenu' => 'Prashanth\Service\Block\PrimaryMenu',
            'Prashanth\Service\Block\Breadcrumb' => 'Prashanth\Service\Block\Breadcrumb',
            'Prashanth\Service\Block\PageTitle' => 'Prashanth\Service\Block\PageTitle',
            'Prashanth\Service\Navigation\AdminThemeNavigation' => 'Prashanth\Service\Navigation\AdminThemeNavigation'
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_path_stack' => array(__DIR__ . '/../view'),
        'strategies' => array('ViewJsonStrategy')
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'collections' => array(
                'assets/blucart/js/blucart_main.js' => array(
                    '/assets/blucart/js/jquery.js',
                    '/assets/blucart/js/jquery-migrate-1.2.1.min.js',
                    '/assets/blucart/bootstrap/js/bootstrap.min.js',
                    '/assets/blucart/lib/jquery-ui/jquery-ui-1.10.0.custom.min.js',
                    '/assets/blucart/lib/ajaxify/jquery.bindLast.js',
                    '/assets/blucart/lib/ajaxify/jquery-scrollto.js',
                    '/assets/blucart/lib/ajaxify/jquery.history.js',
                    '/assets/blucart/lib/ajaxify/ajaxify-html5.js',
                    '/assets/blucart/lib/nprogress/nprogress.js',
                    '/assets/blucart/lib/Zoombox/zoombox.js',
                    '/assets/blucart/lib/datepicker/bootstrap-datepicker.js',
                    '/assets/blucart/lib/loadmask/jquery.loadmask.min.js',
                    '/assets/blucart/lib/jquery-ui/jquery-ui-1.10.0.custom.min.js',
                    '/assets/blucart/lib/bootstrap-modal/js/bootstrap-modalmanager.js',
                    '/assets/blucart/lib/bootstrap-modal/js/bootstrap-modal.js',
                    '/assets/blucart/lib/elevatezoom/jquery.elevatezoom.js',
                    '/assets/blucart/lib/typeahead/hogan.js',
                    '/assets/blucart/lib/typeahead/typeahead.js',
                    '/assets/blucart/lib/classy/classy.js',
                    '/assets/blucart/lib/fancybox/jquery.fancybox.js',
                    '/assets/blucart/lib/fancybox/jquery.fancybox.pack.js',
                    '/assets/blucart/lib/slider/js/bootstrap-slider.js',
                    '/assets/blucart/lib/metisMenu/jquery.metisMenu.js',
                    '/assets/blucart/lib/eldarion/js/eldarion-ajax.min.js',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.min.js',
                ),
                'assets/blucart/js/blucart_iframe_main.js' => array(
                    '/assets/blucart/js/jquery.js',
                    '/assets/blucart/js/jquery-migrate-1.2.1.min.js',
                    '/assets/blucart/bootstrap/js/bootstrap.min.js',
                    '/assets/blucart/lib/jquery-ui/jquery-ui-1.10.0.custom.min.js',
                    '/assets/blucart/js/blucart_iframe.js',
                    '/assets/blucart/lib/nprogress/nprogress.js',
                    '/assets/blucart/lib/Zoombox/zoombox.js',
                    '/assets/blucart/lib/datepicker/bootstrap-datepicker.js',
                    '/assets/blucart/lib/loadmask/jquery.loadmask.min.js',
                    '/assets/blucart/lib/chosen/chosen.jquery.js',
                    '/assets/blucart/lib/classy/classy.js',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.min.js',
                ),
                'assets/prashanth/js/prashanth_main.js' => array(
                    '/assets/prashanth/js/jquery.carouFredSel-6.2.1-packed.js',
                    '/assets/prashanth/js/jquery.countdown.min.js',
                    '/assets/prashanth/js/jquery.navgoco.min.js',
                    '/assets/prashanth/js/filter.js'
                )
            ),
            'paths' => array('Prashanth' => __DIR__ . '/../public')
        ),
    ),
    'navigation' => array(
        'default' => array(),
        'front_menu' => array(),
        'front_breadcrumb' => array('home' => array(
                'label' => 'Home',
                'route' => 'home',
                'id' => 'home'
            )),
        'front_account' => array()
    ),
    'theme' => array(
        'Blucart' => array(
            'js' => array(
                'ajaxify' => array(),
                'main' => array(
                    'assets/blucart/js/blucart_main.js',
                    'assets/blucart/js/blucart.js'
                ),
                'iframe' => array('assets/blucart/js/blucart_iframe_main.js')
            ),
            'css' => array(
                'ajaxify' => array(),
                'main' => array(
                    '/assets/blucart/bootstrap/css/bootstrap.min.css',
                    '/assets/blucart/lib/datepicker/datepicker.css',
                    '/assets/blucart/lib/Zoombox/zoombox.css',
                    '/assets/blucart/lib/loadmask/jquery.loadmask.css',
                    '/assets/blucart/lib/nprogress/nprogress.css',
                    '/assets/blucart/lib/jquery-ui/css/Aristo/Aristo.css',
                    '/assets/blucart/lib/bootstrap-modal/css/bootstrap-modal-bs3patch.css',
                    '/assets/blucart/lib/bootstrap-modal/css/bootstrap-modal.css',
                    '/assets/blucart/lib/typeahead/typeahead.css',
                    '/assets/blucart/lib/fancybox/jquery.fancybox.css',
                    '/assets/blucart/lib/slider/css/slider.css',
                    '/assets/blucart/lib/font-awesome/css/font-awesome.min.css',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.css',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.icons.css',
                    '/assets/blucart/css/blucart.css'
                ),
                'iframe' => array(
                    '/assets/blucart/bootstrap/css/bootstrap.min.css',
                    '/assets/blucart/lib/datepicker/datepicker.css',
                    '/assets/blucart/lib/nprogress/nprogress.css',
                    '/assets/blucart/lib/jquery-ui/css/Aristo/Aristo.css',
                    '/assets/blucart/lib/typeahead/typeahead.css',
                    '/assets/blucart/lib/font-awesome/css/font-awesome.min.css',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.css',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.icons.css',
                    '/assets/blucart/css/blucart.css'
                )
            )
        ),
        'Prashanth' => array(
            'js' => array(
                'ajaxify' => array(),
                'main' => array(
                    'assets/prashanth/js/prashanth_main.js',
                    'assets/prashanth/js/custom.js'
                ),
                'iframe' => array()
            ),
            'css' => array(
                'ajaxify' => array(),
                'main' => array(
                    '//fonts.googleapis.com/css?family=Open+Sans+Condensed:300',
                    '//fonts.googleapis.com/css?family=Open+Sans:400italic,400,600italic,600',
                    '/assets/prashanth/css/animate.min.css',
                    '/assets/prashanth/css/ddlevelsmenu-base.css',
                    '/assets/prashanth/css/ddlevelsmenu-topbar.css',
                    '/assets/prashanth/css/jquery.countdown.css',
                    '/assets/prashanth/css/style.css',
                    '/assets/prashanth/css/prashanth.css'
                ),
                'iframe' => array(
                    '//fonts.googleapis.com/css?family=Open+Sans+Condensed:300',
                    '//fonts.googleapis.com/css?family=Open+Sans:400italic,400,600italic,600',
                    '/assets/prashanth/css/style.css',
                    '/assets/prashanth/css/prashanth_iframe.css'
                )
            )
        )
    ),
    'BLUCART_CURRENT_THEME' => 'Prashanth',
    'events' => array(
        'Prashanth\Service\Block\PrimaryMenu' => 'Prashanth\Service\Block\PrimaryMenu',
        'Prashanth\Service\Block\Breadcrumb' => 'Prashanth\Service\Block\Breadcrumb',
        'Prashanth\Service\Block\PageTitle' => 'Prashanth\Service\Block\PageTitle',
        'Prashanth\Service\Navigation\AdminThemeNavigation' => 'Prashanth\Service\Navigation\AdminThemeNavigation'
    ),
    'view_helpers' => array('invokables' => array('pageTitle' => 'Prashanth\View\Helper\PageTitle'))
);
