$(function() {
    /* Bootstrap Carousel */
    onloadPrashanthTheme();
    $(document).on('statechangecomplete', function(e) {
        onloadPrashanthTheme();
    });


});



function onloadPrashanthTheme() {
    $('.carousel').carousel({
        interval: 8000,
        pause: "hover"
    });

    /* Ecommerce single item carousel */

    $('.ecarousel').carousel({
        interval: 8000,
        pause: "hover"
    });


    /* Recent post carousel (CarouFredSel) */

    /* Carousel */


    $('#carousel_container').carouFredSel({
        responsive: true,
        width: '100%',
        direction: 'right',
        scroll: {
            items: 4,
            delay: 2000,
            duration: 500,
            pauseOnHover: "true"
        },
        prev: {
            button: "#car_prev",
            key: "left"
        },
        next: {
            button: "#car_next",
            key: "right"
        },
        items: {
            visible: {
                min: 1,
                max: 4
            }
        }
    });

    /* Scroll to Top */


    $(".totop").hide();

    $("#slist a").click(function(e) {
        e.preventDefault();
        $(this).next('p').toggle(200);
    });

    /* Careers */

    $('#myTab a').click(function(e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $(window).scroll(function() {
        if ($(this).scrollTop() > 300)
        {
            $('.totop').slideDown();
        }
        else
        {
            $('.totop').slideUp();
        }
    });

    $('.totop a').click(function(e) {
        e.preventDefault();
        $('body,html').animate({scrollTop: 0}, 500);
    });
   // $('.sidey .nav').navgoco();
}