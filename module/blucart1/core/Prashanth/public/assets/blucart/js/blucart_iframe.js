jQuery(function() {

	$('a.zoombox').zoombox();

	$('form.format_form :input').each(
			function() {

				if ($(this).hasClass('input-medium')) {

					$(this).closest('.control-group').css('width', '50%');
					$(this).closest('.control-group').css('float', 'left');

					$(this).css('width', '90%');
				} else {

					$(this).closest('.control-group').css('width', '100%');

					if ($(this).attr('type') == 'checkbox'
							|| $(this).attr('type') == 'radio'
							|| $(this).attr('type') == 'submit') {
						return;
					}

					$(this).css('width', '95%');
				}
			});

});