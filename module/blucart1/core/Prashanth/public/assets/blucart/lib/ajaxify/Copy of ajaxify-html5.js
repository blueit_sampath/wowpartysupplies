// Ajaxify
// v1.0.1 - 30 September, 2012
// https://github.com/browserstate/ajaxify
(function(window, undefined) {

	// Prepare our Variables
	var History = window.History, $ = window.jQuery, document = window.document;
	return false;
	// Check to see if History.js is enabled for our Browser
	if (!History.enabled) {
		return false;
	}

	// Wait for Document
	$(function() {
		lastElement = null;
		// Prepare Variables
		var completedEventName = 'statechangecomplete',
		/* Application Generic Variables */
		$window = $(window), $body = $(document.body), rootUrl = History
				.getRootUrl(), scrollOptions = {
			duration : 800,
			easing : 'swing'
		};

		// Ensure Content

		$content = $body;

		// Internal Helper
		$.expr[':'].internal = function(obj, index, meta, stack) {
			// Prepare
			var $this = $(obj), url = $this.attr('href') || '', isInternalLink;

			// Check link
			isInternalLink = url.substring(0, rootUrl.length) === rootUrl
					|| url.indexOf(':') === -1;

			// Ignore or Keep
			return isInternalLink;
		};

		// HTML Helper
		var documentHtml = function(html) {
			// Prepare
			var result = String(html).replace(/<\!DOCTYPE[^>]*>/i, '').replace(
					/<(html|head|body|title)([\s\>])/gi,
					'<div class="document-$1"$2').replace(
					/<\/(html|head|body|title)\>/gi, '</div>');

			// Return
			return $.trim(result);
		};

		$.fn.serializeObject = function() {
			var o = {};
			var a = this.serializeArray();
			$.each(a, function() {
				if (o[this.name] !== undefined) {
					if (!o[this.name].push) {
						o[this.name] = [ o[this.name] ];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});
			return o;
		};
		// Ajaxify Helper
		$.fn.ajaxify = function() {
			// Prepare
			var $this = $(this);

			// Ajaxify
			$this
					.find(
							'a[target!=_blank]:internal:not(.no-ajaxy,.zoombox,[href^="#"])')
					.click(
							function(event) {
								// Prepare
								var $this = $(this), url = $this.attr('href'), title = $this
										.attr('title')
										|| null;

								// Continue as normal for cmd clicks etc
								if (event.which == 2 || event.metaKey) {
									return true;
								}

								lastElement = {
									element : this,
									type : 'get',
									data : null
								};
								// Ajaxify this link
								History.pushState(null, title, url);
								event.preventDefault();
								return false;
							});

			$this.find('form[target!=_blank]:not(.no-ajaxy)').submit(
							function(event) {
								// Prepare

								var $this = $(this), url = $this.attr('action');

								if (!$(this).valid()) {
									return false;
								}
								$(this).off('submit');

								var type = $this.attr('method') || 'get';
								type = type.toLowerCase();
								var data = null;
								if (type == 'get') {
									data = $(this).serializeObject();
									url = url + '?' + $(this).serialize();
								}

								lastElement = {
									element : this,
									type : type,
									data : $(this).serialize()
								};

								if (!$body.hasClass('loading')) {
									History.pushState(data, null, url);
									remoteCall(url);
								}

								return false;
							});

			// Chain
			return $this;
		};

		function remoteCall(url) {

			// Set Loading
			$body.addClass('loading');
			// Start Fade Out
			// Animating to opacity to 0 still keeps the
			// element's height intact
			// Which prevents that annoying pop bang issue when
			// loading in new content
			$content.animate({
				opacity : 0.5
			}, 800);

			if (!lastElement) {
				lastElement = {
					type : 'get',
					data : null,
					element : null
				};
			}
			// Ajax Request the Traditional Page
			$
					.ajax({
						url : url,
						type : lastElement.type,
						data : lastElement.data,
						beforeSend : function(xhr) {
							xhr.setRequestHeader('X-AJAXIFY', 'TRUE');

						},
						success : function(data, textStatus, jqXHR) {

							// Prepare
							var $data = $(documentHtml(data)),

							
							// Fetch the content
							contentHtml = $data.html();
							if (!contentHtml) {
								document.location.href = url;
								return false;
							}

							// Update the content
							$content.stop(true, true);
							$content.html(contentHtml).ajaxify().css('opacity',
									100).show();
							document.title = $data
									.find('.document-title:first').text();
							try {
								document.getElementsByTagName('title')[0].innerHTML = document.title
										.replace('<', '&lt;').replace('>',
												'&gt;').replace(' & ',
												' &amp; ');
								$('.document-title').remove();

							} catch (Exception) {
							}

							if ($body.ScrollTo || false) {
								$body.ScrollTo(scrollOptions);
							} /* http://balupton.com/projects/jquery-scrollto */
							var location =  $('meta[name=location]').attr("content");
							$('meta[name=location]').remove();
							History.pushState(location, '', location);
							$body.removeClass('loading');
							$window.trigger(completedEventName);

							// Inform Google Analytics of the
							// change
							if (typeof window._gaq !== 'undefined') {
								window._gaq.push([ '_trackPageview',
										relativeUrl ]);
							}

							// Inform ReInvigorate of a state
							// change
							if (typeof window.reinvigorate !== 'undefined'
									&& typeof window.reinvigorate.ajax_track !== 'undefined') {
								reinvigorate.ajax_track(url);
								// ^ we use the full url here as
								// that is what reinvigorate
								// supports
							}
							lastElement = null;

						},
						error : function(jqXHR, textStatus, errorThrown) {
							document.location.href = url;
							return false;
						}
					});
		}
		;

		// Ajaxify our Internal Links
		$body.ajaxify();

		// Hook into State Changes
		$window.bind('statechange', function() {

			var State = History.getState(), url = State.url;

			if (lastElement) {
				ele = lastElement.element;

				if (ele && ($(ele).prop('tagName') == 'form')) {
					return;
				}
			}
			
			remoteCall(url);

		}); // end onStateChange

	}); // end onDomLoad

})(window); // end closure
