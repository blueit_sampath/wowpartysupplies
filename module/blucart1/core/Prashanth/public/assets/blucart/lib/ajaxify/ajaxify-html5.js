(function(window, undefined) {
	var History = window.History, $ = window.jQuery, document = window.document;
	var lastUrl = '';
	if (!History.enabled) {
		return false;
	}

	$(function() {

		var completedEventName = 'statechangecomplete', $window = $(window), $body = $(document.body), rootUrl = History
				.getRootUrl(), scrollOptions = {
			duration : 800,
			easing : 'swing'
		};
		var aSelector = 'body:not(.iframe) a[target!=_blank][href]:internal:not(.no-ajaxy,.zoombox,[href^="#"],[data-toggle="modal"])';
		var formSelector = 'body:not(.iframe) form[target!=_blank]:not(.no-ajaxy)';

		$content = $body;

		$.expr[':'].internal = function(obj, index, meta, stack) {
			var $this = $(obj), url = $this.attr('href') || '', isInternalLink;
			isInternalLink = url.substring(0, rootUrl.length) === rootUrl
					|| url.indexOf(':') === -1;
			return isInternalLink;
		};
		var documentHtml = function(html) {
			var result = String(html).replace(/<\!DOCTYPE[^>]*>/i, '').replace(
					/<(html|head|body|title)([\s\>])/gi,
					'<div class="document-$1"$2').replace(
					/<\/(html|head|body|title)\>/gi, '</div>');
			return $.trim(result);
		};

		$.fn.serializeObject = function() {
			var o = {};
			var a = this.serializeArray();
			$.each(a, function() {
				if (o[this.name] !== undefined) {
					if (!o[this.name].push) {
						o[this.name] = [ o[this.name] ];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});
			return o;
		};

		$.fn.ajaxify = function() {

			
			var $this = $(this);
			

			$(aSelector).live('click', function(event) {
				$('[data-dismiss]').trigger('click');//closes the modals
				if (event.result == false) {
					return false;
				}
				var $this = $(this), url = $this.attr('href');
				if (event.which == 2 || event.metaKey) {
					return true;
				}
				$this.find(aSelector).die('click');
				$.event.trigger({
					type : 'ajaxpageload',
					url : url
				});
				event.preventDefault();
				return false;
			});

			$(formSelector).live('submit', function(event) {
				$('[data-dismiss]').trigger('click');//closes the modals
				if (event.result == false) {
					return false;
				}

				var $this = $(this), url = $this.attr('action');
				if (!$(this).valid()) {
					return false;
				}
				var type = $this.attr('method') || 'post';
				type = type.toLowerCase();
				var data = $(this).serialize();

				if (!$body.hasClass('loading')) {
					$.event.trigger({
						type : 'ajaxpageload',
						url : url,
						method : type,
						fromData : data
					});
				}
				return false;
			});

		};

		$(document)
				.on(
						"ajaxpageload",
						function(e) {
							var url = e.url || '';
							var formData = e.fromData || null;
							var method = e.method || 'get';
							$body.addClass('loading');
							$content.animate({
								opacity : 0.25
							}, 800);
							$.ajax({
										url : url,
										type : method,
										data : formData,
										beforeSend : function(xhr) {
											xhr.setRequestHeader('X-AJAXIFY',
													'TRUE');
										},
										success : function(data, textStatus,
												jqXHR) {

											var $data = $(documentHtml(data)), contentHtml = $data
													.html();

											if (!contentHtml) {
												document.location.href = url;
												return false;
											}
											$content.stop(true, true);
											$content.html(contentHtml);
											$content.animate({
												opacity : 1.0
											}, 800);
											document.title = $data.find(
													'.document-title:first')
													.text();

											try {
												title = document.title.replace(
														'<', '&lt;').replace(
														'>', '&gt;').replace(
														' & ', ' &amp; ');
												document
														.getElementsByTagName('title')[0].innerHTML = title;

											} catch (Exception) {
											}

											if ($body.ScrollTo || false) {
												$body.ScrollTo(scrollOptions);
											}
											var location = $(
													'meta[name=location]')
													.attr("content");
											$('meta[name=location]').remove();
											lastUrl = location;
											History.pushState(formData, title,
													location);
											// $body.ajaxify();
											$body.removeClass('loading');
											$window.trigger(completedEventName);
                                                                                       
                                                                                        $body.trigger('resize');
 $.event.trigger(completedEventName);
											if (typeof window._gaq !== 'undefined') {
												window._gaq.push([
														'_trackPageview',
														location ]);
											}

											if (typeof window.reinvigorate !== 'undefined'
													&& typeof window.reinvigorate.ajax_track !== 'undefined') {
												reinvigorate
														.ajax_track(location);
											}

										},
										error : function(jqXHR, textStatus,
												errorThrown) {
											document.location.href = url;
											return false;
										}
									});

						});

		$window.bind('statechange', function() {
			var State = History.getState();
			var url = $(location).attr('pathname');

			if (url != lastUrl) {
				$.event.trigger({
					type : 'ajaxpageload',
					url : url
				});
			}
		}); // end onStateChange

	}); // end onDomLoad

})(window); // end closure
$(function() {
	$('body').ajaxify();
});
