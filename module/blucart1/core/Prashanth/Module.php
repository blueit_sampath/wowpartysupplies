<?php

namespace Prashanth;

use Zend\Loader\ModuleAutoloader;
use Zend\ModuleManager\Listener;
use Zend\ModuleManager\ModuleManager;

class Module {

    protected $_messages = array();

    public function init(\Zend\ModuleManager\ModuleManager $moduleManager) {

        $this->loadThemeModule($moduleManager);
    }

    public function loadThemeModule($moduleManager) {


        if (file_exists('./config/autoload/theme.global.php')) {
            $array = include './config/autoload/theme.global.php';
            if (isset($array['THEME_MODULE']) && $array['THEME_MODULE']) {
                try {
                    $moduleManager->loadModule($array['THEME_MODULE']);
                } catch (\Zend\ModuleManager\Exception\RuntimeException $e) {
                    $this->_messages[] = $e->getMessage();
                }
            }
        }
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
        $sm = $e->getApplication()->getServiceManager();

        if (count($this->_messages)) {
            foreach ($this->_messages as $message) {
                \Core\Functions::addErrorMessage($message);
            }
            $this->_messages = array();
        }
        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
                    $this,
                    'attachBlucartAssets'
                        ), 1000);


        $eventManager->attach('dispatch.error', array(
            $this,
            'attachBlucartAssets'
        ));

        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
                    $this,
                    'attachThemeAssets'
                        ), -1000);


        $eventManager->attach('dispatch.error', array(
            $this,
            'attachThemeAssets'
                ), -1000);

        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('*', 'renderer', array(
                    $this,
                    'attachMetaTag'
                        ), 1000);
    }

    public function attachMetaTag($e) {

        $sm = \Core\Functions::getServiceLocator();
        $request = $sm->get('Request');

        $requestHeaders = $request->getHeaders();

        $headScript = $sm->get('viewhelpermanager')->get('headScript');
        $basePath = $sm->get('viewhelpermanager')->get('basePath');
        if ($requestHeaders->has('X-AJAXIFY')) {
            $headMeta = $sm->get('viewhelpermanager')->get('headMeta');
            $url = \Core\Functions::getCurrentUrl();
            $headMeta()->setName('location', $url);
            return;
        }
    }

    public function attachBlucartAssets($e) {

        $controller = $e->getTarget();

        if ($controller instanceof \Common\MVC\Controller\AbstractAdminController) {
            return;
        }

        $app = $e->getApplication();
        $sm = $app->getServiceManager();
        $request = $sm->get('Request');

        $requestHeaders = $request->getHeaders();

        $headScript = $sm->get('viewhelpermanager')->get('headScript');
        $basePath = $sm->get('viewhelpermanager')->get('basePath');
        $headScript->appendScript(' var baseUrl = "' . $basePath('/') . '";');
        if ($requestHeaders->has('X-AJAXIFY')) {

            $this->attachAssetsForTheme($sm, 'Blucart', 'ajaxify');
            return;
        }

        if ($requestHeaders->has('X-BLUCART-MODAL')) {

            return;
        }

        if ($request->isXmlHttpRequest()) {
            $view_model = $e->getViewModel();
            $view_model->setTerminal(true);
            return;
        }

        $matches = $e->getRouteMatch();

        if ($matches) {
            $value = \Core\Functions::fromRoute('ajax');
            if ($value && ($value === 'true')) {
                $this->attachAssetsForTheme($sm, 'Blucart', 'iframe');
                $controller->layout('layout/iframe-layout');
                return;
            }
        }
        $this->attachAssetsForTheme($sm, 'Blucart', 'main');
    }

    public function attachThemeAssets($e) {

        $controller = $e->getTarget();

        if ($controller instanceof \Common\MVC\Controller\AbstractAdminController) {
            return;
        }


        $app = $e->getApplication();
        $sm = $app->getServiceManager();
        $request = $sm->get('Request');

        $requestHeaders = $request->getHeaders();
        $config = $sm->get('config');
        $currentTheme = $config['BLUCART_CURRENT_THEME'] ? $config['BLUCART_CURRENT_THEME'] : 'Prashanth';

        if (!\Core\Functions::hasModule($currentTheme)) {
            $currentTheme = 'Prashanth';
        }

        if ($requestHeaders->has('X-AJAXIFY')) {
            $this->attachAssetsForTheme($sm, $currentTheme, 'ajaxify');
            return;
        }
        if ($requestHeaders->has('X-BLUCART-MODAL')) {
            if ($controller instanceof \Zend\Mvc\Controller\AbstractActionController) {
                $controller->layout('layout/layout-modal');
            }
            return;
        }

        if ($request->isXmlHttpRequest()) {
            $view_model = $e->getViewModel();
            $view_model->setTerminal(true);
            return;
        }

        $matches = $e->getRouteMatch();

        if ($matches) {
            $value = \Core\Functions::fromRoute('ajax');
            if ($value && ($value === 'true')) {
                $this->attachAssetsForTheme($sm, $currentTheme, 'iframe');
                return;
            }
        }
        $this->attachAssetsForTheme($sm, $currentTheme, 'main');
    }

    public function attachAssetsForTheme($sm, $theme, $type = 'main') {

        $config = $sm->get('Config');

        if (isset($config['theme'][$theme])) {
            $headScript = $sm->get('viewhelpermanager')->get('headScript');
            $headLink = $sm->get('viewhelpermanager')->get('headLink');

            $basePath = $sm->get('viewhelpermanager')->get('basePath');
            $themeConfig = $config['theme'][$theme];

            if (isset($themeConfig['js']) && isset($themeConfig['js'][$type])) {
                $array = $themeConfig['js'][$type];
                foreach ($array as $value) {
                    $value = trim($value);

                    if ($value) {
                        if ((strpos($value, '//') === 0) || (strpos($value, 'http://') === 0) || (strpos($value, 'https://') === 0)) {
                            $headScript->appendFile($value);
                        } else {
                            $headScript->appendFile($basePath($value));
                        }
                    }
                }
            }

            if (isset($themeConfig['css']) && isset($themeConfig['css'][$type])) {
                $array = $themeConfig['css'][$type];
                foreach ($array as $value) {
                    $value = trim($value);
                    if ($value) {
                        if ((strpos($value, '//') === 0) || (strpos($value, 'http://') === 0) || (strpos($value, 'https://') === 0)) {
                            $headLink->appendStylesheet($value);
                        } else {
                            $headLink->appendStylesheet($basePath($value));
                        }
                    }
                }
            }
        }
    }

}
