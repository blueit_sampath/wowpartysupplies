<?php
return array(
    'router' => array(
        'routes' => array(
            'admin-config' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/config',
                    'defaults' => array(
                        'controller' => 'Config\Controller\AdminConfig',
                        'action' => 'index'
                    )
                )
            ),
            'admin-config-setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/config/setting',
                    'defaults' => array(
                        'controller' => 'Config\Controller\AdminConfig',
                        'action' => 'add'
                    )
                )
            ),
            'admin-config-currency' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/config/currency',
                    'defaults' => array(
                        'controller' => 'Config\Controller\AdminCurrency',
                        'action' => 'index'
                    )
                )
            ),
            'admin-config-currency-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/config/currency/add[/ajax/:ajax][/:currencyId]',
                    'defaults' => array(
                        'controller' => 'Config\Controller\AdminCurrency',
                        'action' => 'add',
                        'ajax' => 'true'
                    )
                )
            ),
            'admin-config-weight' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/config/weight',
                    'defaults' => array(
                        'controller' => 'Config\Controller\AdminWeight',
                        'action' => 'index'
                    )
                )
            ),
            'admin-config-length' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/config/length',
                    'defaults' => array(
                        'controller' => 'Config\Controller\AdminLength',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    'events' => array(
        'Config\Service\Grid\AdminConfig' => 'Config\Service\Grid\AdminConfig',
        'Config\Service\Form\AdminConfig' => 'Config\Service\Form\AdminConfig',
        'Config\Service\Tab\AdminConfig' => 'Config\Service\Tab\AdminConfig',
        'Config\Service\Grid\AdminCurrency' => 'Config\Service\Grid\AdminCurrency',
        'Config\Service\Link\AdminCurrency' => 'Config\Service\Link\AdminCurrency',
        'Config\Service\Form\AdminCurrency' => 'Config\Service\Form\AdminCurrency',
        'Config\Service\Grid\AdminWeight' => 'Config\Service\Grid\AdminWeight',
        'Config\Service\Grid\AdminLength' => 'Config\Service\Grid\AdminLength',
        'Config\Service\Navigation\AdminConfigNavigation' => 'Config\Service\Navigation\AdminConfigNavigation',
        'Config\Service\Block\LogoBlock' => 'Config\Service\Block\LogoBlock'
    ),
    'service_manager' => array(
        'invokables' => array(
            'Config\Service\Grid\AdminConfig' => 'Config\Service\Grid\AdminConfig',
            'Config\Form\AdminConfig\ConfigForm' => 'Config\Form\AdminConfig\ConfigForm',
            'Config\Form\AdminConfig\ConfigFilter' => 'Config\Form\AdminConfig\ConfigFilter',
            'Config\Service\Form\AdminConfig' => 'Config\Service\Form\AdminConfig',
            'Config\Service\Tab\AdminConfig' => 'Config\Service\Tab\AdminConfig',
            'Config\Service\Grid\AdminCurrency' => 'Config\Service\Grid\AdminCurrency',
            'Config\Service\Link\AdminCurrency' => 'Config\Service\Link\AdminCurrency',
            'Config\Form\AdminCurrency\CurrencyForm' => 'Config\Form\AdminCurrency\CurrencyForm',
            'Config\Form\AdminCurrency\CurrencyFilter' => 'Config\Form\AdminCurrency\CurrencyFilter',
            'Config\Service\Form\AdminCurrency' => 'Config\Service\Form\AdminCurrency',
            'Config\Service\Grid\AdminWeight' => 'Config\Service\Grid\AdminWeight',
            'Config\Service\Grid\AdminLength' => 'Config\Service\Grid\AdminLength',
            'Config\Service\Navigation\AdminConfigNavigation' => 'Config\Service\Navigation\AdminConfigNavigation',
            'Config\Service\Block\LogoBlock' => 'Config\Service\Block\LogoBlock'
        ),
        'factories' => array(
            'Config\Form\AdminConfig\ConfigFactory' => 'Config\Form\AdminConfig\ConfigFactory',
            'Config\Form\AdminCurrency\CurrencyFactory' => 'Config\Form\AdminCurrency\CurrencyFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Config\Controller\AdminConfig' => 'Config\Controller\AdminConfigController',
            'Config\Controller\AdminCurrency' => 'Config\Controller\AdminCurrencyController',
            'Config\Controller\AdminWeight' => 'Config\Controller\AdminWeightController',
            'Config\Controller\AdminLength' => 'Config\Controller\AdminLengthController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Config' => __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'Config_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Config/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Config\Entity' => 'Config_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Config' => __DIR__ . '/../public'
            )
        )
    )
);
