<?php

namespace Config;

use Config\Api\ConfigApi;

class Module {

    public function getConfig() {
        // $e->getApplication()->getServiceManager->get('ModuleManager')->loadModule('Chat');
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
        $app = $e->getApplication();
        $sm = $app->getServiceManager();
        $config = ConfigApi::getConfigArray();

        $viewHelperPluginManager = $sm->get('view_helper_manager');
        $locale = ConfigApi::getLocale();
        if ($locale) {
            $locale = str_replace('-', '_', $locale);
            \Locale::setDefault($locale);
        }

        $timeZone = ConfigApi::getConfigByKey('DEFAULT_TIMEZONE');
        if ($timeZone) {
            date_default_timezone_set($timeZone);
        }

        $currency = ConfigApi::getCurrencyCode();
        if ($currency) {

            $viewHelperPluginManager->get('currencyFormat')->setCurrencyCode($currency);
        }
    }

}
