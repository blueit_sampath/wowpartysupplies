<?php

namespace Config\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminConfig extends AbstractTabEvent {
	public function getEventName() {
		return 'adminConfigAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-config-setting' );
		$tabContainer->add ( 'admin-config-setting', 'General', $u, 1000 );
		$u = $url ( 'admin-config-currency' );
		$tabContainer->add ( 'admin-config-currency', 'Currency', $u, 900 );
		$u = $url ( 'admin-config-weight' );
		$tabContainer->add ( 'admin-config-weight', 'Weight', $u, 800 );
		$u = $url ( 'admin-config-length' );
		$tabContainer->add ( 'admin-config-length', 'Length', $u, 800 );
		
		return $this;
	}
}

