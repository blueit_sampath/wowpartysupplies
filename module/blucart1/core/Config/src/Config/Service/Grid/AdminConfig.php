<?php

namespace Config\Service\Grid;

use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;

class AdminConfig extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'configKey',
			'configValue' 
	);
	protected $_entity = '\Config\Entity\Config';
	protected $_entityName = 'config';
	public function getEventName() {
		return 'adminConfig';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'Id' );
		$columnItem->setType ( 'number' );
		$columnItem->setIsPrimary ( true );
		$columnItem->setEditable ( false );
		
		$columnItem->setWeight ( 1000 );
		$columns->addColumn ( 'config.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'configKey' );
		$columnItem->setTitle ( 'Key' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'config.configKey', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'configValue' );
		$columnItem->setTitle ( 'Value' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'config.configValue', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'create' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'create', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 700 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
	
}
