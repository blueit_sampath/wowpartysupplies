<?php

namespace Config\Service\Grid;

use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use BlucartGrid\Option\ColumnItem;
use BlucartGrid\Event\AbstractMainBlucartGridEvent;

class AdminLength extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			
			'name',
			
			'shortName',
			
			'rate' 
	)
	;
	protected $_entity = '\Config\Entity\LengthType';
	protected $_entityName = 'lengthType';
	public function getEventName() {
		return 'adminConfigLength';
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		
		$array = array ();
		
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ();
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'ID' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'lengthType.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 990 );
		$columnItem->setValidation(array('required'=>'true'));
		$columns->addColumn ( 'lengthType.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'shortName' );
		$columnItem->setTitle ( 'Short Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columnItem->setValidation(array('required'=>'true'));
		$columns->addColumn ( 'lengthType.shortName', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'rate' );
		$columnItem->setTitle ( 'Rate' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 970 );
		$columnItem->setValidation(array('required'=>'true', 'min'=> 0));
		$columns->addColumn ( 'lengthType.rate', $columnItem );
		
		$this->formToolbar ();
		
		return $columns;
	}
	public function formToolbar() {
		$grid = $this->getGrid ();
		
		$toolbar = $grid->getToolbar ();
		if (! $toolbar) {
			$toolbar = new Toolbar ();
			$grid->setToolbar ( $toolbar );
		}
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'create' );
		$toolBarItem->setWeight ( 1000 );
		$toolbar->addToolbar ( 'create', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'save' );
		$toolBarItem->setWeight ( 900 );
		$toolbar->addToolbar ( 'save', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'cancel' );
		$toolBarItem->setWeight ( 800 );
		$toolbar->addToolbar ( 'cancel', $toolBarItem );
		
		$toolBarItem = new ToolbarItem ();
		$toolBarItem->setName ( 'destroy' );
		$toolBarItem->setWeight ( 700 );
		$toolbar->addToolbar ( 'destroy', $toolBarItem );
	}
}
