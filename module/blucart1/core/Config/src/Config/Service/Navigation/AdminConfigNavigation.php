<?php

namespace Config\Service\Navigation;

use Core\Functions;
use Admin\Navigation\Event\AdminNavigationEvent;

class AdminConfigNavigation extends AdminNavigationEvent {
	public function common() {
		$navigation = $this->getNavigation ();
		$page = $navigation->findOneBy ( 'id', 'websiteMain' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Setting',
							'uri' => '#',
							'id' => 'admin-config',
							'iconClass' => 'glyphicon-wrench' 
					) 
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'admin-config' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Website',
							'route' => 'admin-config-setting',
							'id' => 'admin-config-setting',
							'iconClass' => 'glyphicon-wrench'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'admin-config' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Currency',
							'route' => 'admin-config-currency',
							'id' => 'admin-config-setting',
							'iconClass' => 'glyphicon-wrench'
					)
			) );
			
		}
		
		$page = $navigation->findOneBy ( 'id', 'admin-config' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Weight',
							'route' => 'admin-config-weight',
							'id' => 'admin-config-weight',
							'iconClass' => 'glyphicon-wrench'
					)
			) );
		}
		
		$page = $navigation->findOneBy ( 'id', 'admin-config' );
		if ($page) {
			$page->addPages ( array (
					array (
							'label' => 'Length',
							'route' => 'admin-config-length',
							'id' => 'admin-config-length',
							'iconClass' => 'glyphicon-wrench'
					)
			) );
		}
		
	}
}

