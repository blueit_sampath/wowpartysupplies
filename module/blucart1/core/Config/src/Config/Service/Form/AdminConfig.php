<?php
namespace Config\Service\Form;

use Common\Form\Option\AbstractMainFormEvent;
use Config\Api\ConfigApi;
use Core\Functions;
use File\Api\FileApi;

class AdminConfig extends AbstractMainFormEvent
{

    protected $_columnKeys = array(
        'SITE_NAME',
        'SITE_EMAIL',
        'DEFAULT_COUNTRY',
        'DEFAULT_TIMEZONE',
        'SITE_LOGO',
        'storeAddress' => array(
            'STORE_NAME',
            'STORE_ADDRESS_LINE_1',
            'STORE_ADDRESS_LINE_2',
            'STORE_CITY',
            'STORE_COUNTRY',
            'STORE_STATE',
            'STORE_POSTCODE',
            'STORE_PHONE',
            'STORE_FAX',
            
        ),
        'storeFormat' => array(
            'STORE_WEIGHT',
            'STORE_LENGTH'
        )
    );

    public function getFormName()
    {
        return 'adminConfigAdd';
    }

    public function getPriority()
    {
        return 1000;
    }

    public function save()
    {
        $resultContainer = $this->getFormResultContainer();
        $form = $this->getForm();
        $params = $form->getData();
        
        if (isset($params['SITE_LOGO'])) {
            FileApi::deleteFiles(Functions::fromPost('SITE_LOGO_delete', ''));
            
            $image = $params['SITE_LOGO'];
            
            if ($image) {
                $array = explode(',', $image);
                foreach ($array as $file) {
                    $imageEntity = FileApi::createOrUpdateFile($file, $params['SITE_NAME'], $params['SITE_NAME']);
                }
                
                $params['SITE_LOGO'] = $imageEntity->path;
            }
        }
        
        $this->saveRecursively($this->_columnKeys, $params);
        return true;
    }

    public function saveRecursively($columnKeys, $params)
    {
        foreach ($columnKeys as $key => $columnKey) {
            
            if (is_array($columnKey)) {
                
                $this->saveRecursively($columnKey, $params[$key]);
            } else {
                if (isset($params[$columnKey])) {
                    ConfigApi::createOrUpdateKey($columnKey, $params[$columnKey]);
                }
            }
        }
    }

    public function getRecord()
    {
        $form = $this->getForm();
        $this->populateRecursively($this->_columnKeys, $form);
       
        return true;
    }
    
 
    public function populateRecursively($columnKeys, $form)
    {
        foreach ($columnKeys as $key => $columnKey) {
            
            if (is_array($columnKey)) {
                $this->populateRecursively($columnKey, $form->get($key));
            } else 
                if ($element = $form->get($columnKey)) {
                    $value = ConfigApi::getConfigByKey($columnKey, null);
                    if ($value !== null) {
                        $element->setValue($value);
                    }
                }
        }
    }
}
