<?php

namespace Config\Service\Form;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminCurrency extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'code',
			'rate',
			'status',
			'weight' 
	);
	protected $_entity = '\Config\Entity\Currency';
	protected $_entityName = 'currency';
	public function getFormName() {
		return 'adminCurrencyAdd';
	}
	public function getPriority() {
		return 1000;
	}
}
