<?php 
namespace Config\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCurrency extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCurrency';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-config-currency-add');
		$item = $linkContainer->add ( 'admin-config-currency-add', 'Add Currency', $u,1000 );
				$item->setLinkClass('zoombox ');
				
		return $this;
	}
}

