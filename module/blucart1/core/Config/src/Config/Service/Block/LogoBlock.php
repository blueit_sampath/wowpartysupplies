<?php 
namespace Config\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class LogoBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'logo-block';

	public function getBlockName() {
		return 'logoBlock';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'Logo';
	}
	
}

