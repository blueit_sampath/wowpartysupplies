<?php
namespace Config\Form\AdminCurrency;

use Common\Form\Option\AbstractFormFactory;

class CurrencyFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Config\Form\AdminCurrency\CurrencyForm' );
		$form->setName ( 'adminCurrencyAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Config\Form\AdminCurrency\CurrencyFilter' );
	}
}
