<?php

namespace Config\Form\AdminCurrency;

use Config\Api\CurrencyApi;
use Zend\Validator\Callback;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CurrencyFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'name',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'code',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StringTrim' 
						) 
				)
				,
				'validators' => array (
						array (
								'name' => 'Callback',
								'options' => array (
										'callback' => array (
												$this,
												'checkCurrency' 
										),
										'messages' => array (
												Callback::INVALID_VALUE => "Currency already exists" 
										) 
								) 
						)
						 
				) 
		) );
		$this->add ( array (
				'name' => 'status',
				'required' => true 
		) );
		
		$this->add ( array (
				'name' => 'rate',
				'required' => true,
				'validators' => array (
						
						array (
								'name' => 'Float' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'required' => false,
				'validators' => array (
		
						array (
								'name' => 'Int'
						)
				)
		) );
	}
	public function checkCurrency($value, $context) {
		if ($entity = CurrencyApi::getCurrencyByCode ( $value )) {
			if ($entity->id == $context ['id']) {
				return true;
			}
			return false;
		}
		return true;
	}
} 