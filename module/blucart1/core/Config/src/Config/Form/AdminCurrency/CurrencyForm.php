<?php

namespace Config\Form\AdminCurrency;

use Config\Api\ConfigApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class CurrencyForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$select = new Select ( 'code' );
		$select->setLabel ( 'CurrencyCode' );
		$select->setValueOptions ( ConfigApi::getAllCurrenciesArray () );
		$this->add ( $select, array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'rate',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Conversion Rate' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'attributes' => array (
						'type' => 'text'
				),
				'options' => array (
						'label' => 'Priority'
				)
		), array (
				'priority' => 980
		) );
		
		$this->getStatus ( 970 );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}