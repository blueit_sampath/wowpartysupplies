<?php

namespace Config\Form\AdminConfig;

use Config\Api\LengthApi;

use Config\Api\WeightApi;

use Zend\Form\Element\Select;

use Config\Api\ConfigApi;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class StoreFormatFieldset extends Fieldset implements InputFilterProviderInterface {
	public function __construct($name = 'storeFormat') {
		parent::__construct ( $name );
		
		
		$select = new Select ( 'STORE_WEIGHT' );
		$select->setLabel ( 'Default Weight' );
		$select->setValueOptions ( $this->getWeights() );
		$this->add ( $select, array (
				'priority' => 1000
		) );
		$select = new Select ( 'STORE_LENGTH' );
		$select->setLabel ( 'Default LENGTH' );
		$select->setValueOptions ( $this->getLengths() );
		$this->add ( $select, array (
				'priority' => 990
		) );
	}
	
	/**
	 * Should return an array specification compatible with
	 * {@link Zend\InputFilter\Factory::createInputFilter()}.
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		return array (
				'STORE_WEIGHT' => array (
						'required' => true,
						'validators' => array (
								array (
										'name' => 'StringLength',
										'options' => array (
												'encoding' => 'UTF-8',
												'max' => 255 
										) 
								) 
						) 
				),
				'STORE_LENGTH' => array (
						'required' => true,
						'validators' => array (
								array (
										'name' => 'StringLength',
										'options' => array (
												'encoding' => 'UTF-8',
												'max' => 255 
										) 
								) 
						) 
				),
				
				 
		);
	}
	public function getWeights() {
		$array = array ();
		$weights = WeightApi::getWeights();
		foreach ( $weights as $weightId ) {
			$weight = WeightApi::getWeightById($weightId['weightTypeId']);
			$array [$weight->shortName] = $weight->name;
		}
		return $array;
	}
	
	public function getLengths() {
		$array = array ();
		$results = LengthApi::getlengths();
		foreach ( $results as $result ) {
			$length = LengthApi::getLengthById($result['lengthTypeId']);
			$array [$length->shortName] = $length->name;
		}
		return $array;
	}
}
