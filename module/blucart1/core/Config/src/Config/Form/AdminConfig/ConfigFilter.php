<?php

namespace Config\Form\AdminConfig;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ConfigFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		$this->add ( array (
				'name' => 'SITE_NAME',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 3,
										'max' => 45 
								) 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'SITE_LOGO',
				'required' => false,
				
		) );
		
		$this->add ( array (
				'name' => 'SITE_EMAIL',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				'validators' => array (
						array (
								'name' => 'StringLength',
								'options' => array (
										'encoding' => 'UTF-8',
										'min' => 1,
										'max' => 255 
								) 
						),
						array (
								'name' => 'EmailAddress' 
						) 
				) 
		) );
		$this->add ( array (
				'name' => 'DEFAULT_COUNTRY',
				'required' => true 
		)
		 );
		
		$this->add ( array (
				'name' => 'DEFAULT_TIMEZONE',
				'required' => true 
		)
		 );
	}
} 