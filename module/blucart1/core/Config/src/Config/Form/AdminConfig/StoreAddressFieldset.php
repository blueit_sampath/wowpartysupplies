<?php

namespace Config\Form\AdminConfig;

use Zend\Form\Element\Select;

use Config\Api\ConfigApi;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class StoreAddressFieldset extends Fieldset implements InputFilterProviderInterface {
	public function __construct($name = 'storeAddress') {
		parent::__construct ( $name );
		
		$this->add ( array (
				'name' => 'STORE_NAME',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Store Name' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'STORE_ADDRESS_LINE_1',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Address Line 1' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'STORE_ADDRESS_LINE_2',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Address Line 2' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'STORE_CITY',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'City' 
				) 
		), array (
				'priority' => 970 
		) );
		
		$select = new Select ( 'STORE_COUNTRY' );
		$select->setLabel ( 'Country' );
		$select->setValueOptions ( $this->getCountries () );
		$this->add ( $select, array (
				'priority' => 950
		) );
		$select->setValue('United Kingdom');
		$this->add ( array (
				'name' => 'STORE_STATE',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'State' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$this->add ( array (
				'name' => 'STORE_POSTCODE',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Postcode' 
				) 
		), array (
				'priority' => 950 
		) );
		
		$this->add ( array (
				'name' => 'STORE_PHONE',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Phone' 
				) 
		), array (
				'priority' => 940 
		) );
		$this->add ( array (
				'name' => 'STORE_FAX',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Fax' 
				) 
		), array (
				'priority' => 930 
		) );
	}
	
	/**
	 * Should return an array specification compatible with
	 * {@link Zend\InputFilter\Factory::createInputFilter()}.
	 *
	 * @return array \
	 */
	public function getInputFilterSpecification() {
		return array (
				'STORE_NAME' => array (
						'required' => true,
						'validators' => array (
								array (
										'name' => 'StringLength',
										'options' => array (
												'encoding' => 'UTF-8',
												'max' => 255 
										) 
								) 
						) 
				),
				'STORE_ADDRESS_LINE_1' => array (
						'required' => true,
						'validators' => array (
								array (
										'name' => 'StringLength',
										'options' => array (
												'encoding' => 'UTF-8',
												'max' => 255 
										) 
								) 
						) 
				),
				'STORE_ADDRESS_LINE_2' => array (
						'required' => false 
				)
				,
				'STORE_CITY' => array (
						'required' => false,
						'validators' => array (
								array (
										'name' => 'StringLength',
										'options' => array (
												'encoding' => 'UTF-8',
												'max' => 255 
										) 
								) 
						) 
				),
				'STORE_COUNTRY' => array (
						'required' => TRUE 
				)
				,
				'STORE_STATE' => array (
						'required' => FALSE 
				)
				,
				'STORE_POSTCODE' => array (
						'required' => FALSE 
				)
				,
				'STORE_PHONE' => array (
						'required' => FALSE 
				)
				,
				'STORE_FAX' => array (
						'required' => FALSE 
				)
				 
		);
	}
	public function getCountries() {
		$array = array ();
		$countries = ConfigApi::getCountries ();
		foreach ( $countries as $country ) {
			$array [$country->name] = $country->name;
		}
		return $array;
	}
}
