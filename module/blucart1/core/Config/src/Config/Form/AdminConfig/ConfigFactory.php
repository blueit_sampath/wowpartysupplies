<?php
namespace Config\Form\AdminConfig;

use Common\Form\Option\AbstractFormFactory;

class ConfigFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Config\Form\AdminConfig\ConfigForm' );
		$form->setName ( 'adminConfigAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Config\Form\AdminConfig\ConfigFilter' );
	}
}
