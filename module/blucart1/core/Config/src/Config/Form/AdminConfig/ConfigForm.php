<?php
namespace Config\Form\AdminConfig;

use Config\Api\ConfigApi;
use Core\Functions;
use Zend\Form\Element\Select;
use Category\Api\CategoryApi;
use Zend\Form\Fieldset;
use Common\Form\Form;
use Zend\Captcha;
use Zend\Form\Element;
use File\Form\Element\FileUpload;

class ConfigForm extends Form

{

    public function init()
    {
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'SITE_NAME',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Site Name'
            )
        ), array(
            'priority' => 960
        ));
        $this->add(array(
            'name' => 'SITE_EMAIL',
            'attributes' => array(
                'type' => 'text'
            ),
            'options' => array(
                'label' => 'Site Email'
            )
        ), array(
            'priority' => 960
        ));
        
        $select = new Select('DEFAULT_COUNTRY');
        $select->setLabel('Default Country');
        $select->setValueOptions($this->getCountries());
        $this->add($select, array(
            'priority' => 950
        ));
        
        $select = new Select('DEFAULT_TIMEZONE');
        $select->setLabel('Default Timezone');
        $select->setValueOptions($this->getTimeZones());
        $this->add($select, array(
            'priority' => 950
        ));
        
        $logo = new FileUpload('SITE_LOGO');
        $logo->setButtonText('Add/Edit Logo');
        $this->add($logo, array(
            'priority' => 940
        ));
        
        $this->add(array(
            'name' => 'storeAddress',
            'type' => 'Config\Form\AdminConfig\StoreAddressFieldset',
            'options' => array(
                'legend' => 'Store Address'
            )
        ), array(
            'priority' => 940
        ));
        
        $this->add(array(
            'name' => 'storeFormat',
            'type' => 'Config\Form\AdminConfig\StoreFormatFieldset',
            'options' => array(
                'legend' => 'Store Format'
            )
        ), 

        array(
            'priority' => 930
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
        ), array(
            'priority' => - 100
        ));
    }

    public function getCountries()
    {
        $array = array();
        $countries = ConfigApi::getCountries();
        foreach ($countries as $country) {
            $array[$country->name] = $country->name;
        }
        return $array;
    }

    public function getTimeZones()
    {
        $array = array();
        $tzlist = \DateTimeZone::listIdentifiers(\DateTimeZone::ALL);
        foreach ($tzlist as $value) {
            $array[$value] = $value;
        }
        return $array;
    }
}