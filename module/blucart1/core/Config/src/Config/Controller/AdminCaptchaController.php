<?php

namespace Config\Controller;

use Common\MVC\Controller\AbstractAdminController;


class AdminConfigController extends AbstractAdminController {
	
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Config\Form\AdminConfig\ConfigFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-config-setting' );
	}
	
	
}