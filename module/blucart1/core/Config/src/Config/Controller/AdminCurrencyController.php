<?php

namespace Config\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminCurrencyController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Config\Form\AdminCurrency\CurrencyFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-config-currency-add', array (
				'currencyId' => $form->get ( 'id' )->getValue () 
		) );
	}
}