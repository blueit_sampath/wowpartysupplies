<?php

namespace Config\Api;

use Core\Functions;
use Common\Api\Api;

class WeightApi extends Api {
	protected static $_entity = '\Config\Entity\WeightType';
	public static function getWeightById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getWeightByShortName($shortName) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findOneBy ( array (
				'shortName' => $shortName 
		) );
	}
	public static function getWeights() {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'weightType.id', 'weightTypeId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'weightType' );
		return $queryBuilder->executeQuery ();
	}
}
