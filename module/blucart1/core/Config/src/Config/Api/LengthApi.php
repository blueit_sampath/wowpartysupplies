<?php

namespace Config\Api;

use Core\Functions;
use Common\Api\Api;

class LengthApi extends Api {
	protected static $_entity = '\Config\Entity\LengthType';
	public static function getLengthById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getLengthByShortName($shortName) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findOneBy ( array (
				'shortName' => $shortName 
		) );
	}
	public static function getLengths() {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'lengthType.id', 'lengthTypeId' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'lengthType' );
		return $queryBuilder->executeQuery ();
	}
}
