<?php

namespace Config\Api;

use Core\Functions;
use Common\Api\Api;

class CurrencyApi extends Api {
	protected static $_entity = '\Config\Entity\Currency';
	public static function getCurrencyById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getCurrencyByCode($code) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_entity )->findOneBy ( array (
				'code' => $code 
		) );
	}
	public static function getCurrencies($status = true) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'currency.id', 'currencyId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'currency.status', 'currencyStatus' );
			$queryBuilder->addParameter ( 'currencyStatus', $status );
		}
		$queryBuilder->addOrder ( 'currency.weight', 'currencyWeight', 'desc' );
		$item = $queryBuilder->addFrom ( static::$_entity, 'currency' );
		return $queryBuilder->executeQuery ();
	}
}
