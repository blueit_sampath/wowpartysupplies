<?php

namespace Config\Api;

use Core\Functions;

class ConfigApi {
	protected static $_entity = '\Config\Entity\Config';
	protected static $_countryEntity = '\Config\Entity\Country';
	protected static $_stateEntity = '\Config\Entity\CountryState';
	public static function getConfigById($id) {
		$em = Functions::getEntityManager ();
		return $em->find ( static::$_entity, $id );
	}
	public static function getConfigArray() {
		$em = Functions::getEntityManager ();
		$results = $em->getRepository ( static::$_entity )->findAll ();
		$array = array ();
		if ($results) {
			foreach ( $results as $result ) {
				$array [$result->configKey] = $result->configValue;
			}
		}
		return $array;
	}
	public static function getConfigByKey($configKey, $defaultValue = null) {
		// echo $configKey;
		// return $configKey;
		// exit;
		$configKey = trim ( $configKey );
		$em = Functions::getEntityManager ();
		$result = $em->getRepository ( static::$_entity )->findOneBy ( array (
				'configKey' => $configKey 
		) );
		if ($result) {
			return $result->configValue;
		} else {
			$config = Functions::getServiceLocator ()->get ( 'Config' );
			if (isset ( $config [$configKey] )) {
				return $config [$configKey];
			}
		}
		return $defaultValue;
	}
	public static function createOrUpdateKey($configKey, $configValue) {
		$em = Functions::getEntityManager ();
		$entity = $em->getRepository ( static::$_entity )->findOneBy ( array (
				'configKey' => $configKey 
		) );
		if (! $entity) {
			$entity = new static::$_entity ();
		}
		$entity->configKey = $configKey;
		$entity->configValue = $configValue;
		$em->persist ( $entity );
		$em->flush ();
		return $entity;
	}
	public static function getCountryByName($name) {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_countryEntity )->findOneBy ( array (
				'name' => $name 
		) );
	}
	public static function getCountries() {
		$em = Functions::getEntityManager ();
		return $em->getRepository ( static::$_countryEntity )->findAll ();
	}
	public static function getStatesByCountry($name) {
		$results = static::getCountryByCountry ( $name );
		if ($results) {
			return $results->states;
		}
		return false;
	}
	public static function getStateByCountryAndState($country, $state) {
		$em = Functions::getEntityManager ();
		$dql = ' select u from ' . static::$_stateEntity . ' u where u.name= :state and u.country= :country ';
		$query = $em->createQuery ( $dql );
		$query->setParameter ( 'state', $state );
		$query->setParameter ( 'country', $country );
		
		$results = $query->getResult ();
		if ($results) {
			return array_pop ( $results );
		}
		return false;
	}
	public static function getCountryNameByCountryCode($countryCode = '') {
		$em = Functions::getEntityManager ();
                if(!$countryCode){
                    return static::getConfigByKey ( 'DEFAULT_COUNTRY' );
                }
		$result = $em->getRepository ( static::$_countryEntity )->findOneBy ( array (
				'iso' => $countryCode 
		) );
		if ($result) {
			return $result->name;
		}
		return null;
	}
	public static function getCountryCodeByCountryName($country = null, $returnEntity = false) {
		if (! $country) {
			$country = static::getConfigByKey ( 'DEFAULT_COUNTRY' );
		}
		$countryEntity = static::getCountryByName ( $country );
		
		if ($countryEntity) {
			if (! $returnEntity) {
				return $countryEntity->iso;
			}
			return $countryEntity;
		}
		return null;
	}
	public static function getStateNameByStateCode($iso) {
		$em = Functions::getEntityManager ();
		$result = $em->getRepository ( static::$_stateEntity )->findOneBy ( array (
				'iso' => $iso 
		) );
		return $result->name;
	}
	public static function getStateCodeByStateName($name) {
		$em = Functions::getEntityManager ();
		$result = $em->getRepository ( static::$_stateEntity )->findOneBy ( array (
				'name' => $name 
		) );
		return $result->iso;
	}
	public static function getCurrencyCode($country = null) {
		if (! $country) {
			$country = static::getConfigByKey ( 'DEFAULT_COUNTRY' );
		}
		$countryEntity = static::getCountryByName ( $country );
		if ($countryEntity) {
			return $countryEntity->currencyCode;
		}
		return null;
	}
	public static function getCurrencySymbol($locale = null) {
		if (! $locale) {
			$locale = static::getLocale ();
			$locale = str_replace ( '-', '_', $locale );
		}
		
		$fomatter = new \NumberFormatter ( $locale, \NumberFormatter::CURRENCY );
		return $fomatter->getSymbol ( \NumberFormatter::CURRENCY_SYMBOL );
	}
	public static function getLocale($country = null) {
		if (! $country) {
			$country = static::getConfigByKey ( 'DEFAULT_COUNTRY' );
		}
		$countryEntity = static::getCountryByName ( $country );
		if ($countryEntity) {
			if ($countryEntity->languages) {
				$langs = explode ( ',', $countryEntity->languages );
				foreach ( $langs as $lang ) {
					if (strpos ( $lang, '-' )) {
						return $lang;
					}
				}
			}
		}
		return null;
	}
	public static function getAllCurrenciesArray() {
		$em = Functions::getEntityManager ();
		$dql = ' select Distinct u.currencyCode as currencyCode from ' . static::$_countryEntity . ' u order by u.currencyCode asc ';
		$query = $em->createQuery ( $dql );
		$results = $query->getResult ();
		$array = array ();
		if ($results) {
			foreach ( $results as $result ) {
				$array [$result ['currencyCode']] = $result ['currencyCode'];
			}
		}
		return $array;
	}
	public static function getWeightSymbol() {
		return static::getConfigByKey ( 'STORE_WEIGHT' );
	}
	public static function getLengthSymbol() {
		return static::getConfigByKey ( 'STORE_LENGTH' );
	}
}
