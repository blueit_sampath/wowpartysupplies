<?php
namespace Config\Entity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="iso", type="string", length=45, nullable=true)
     */
    private $iso;

    /**
     * @var string
     *
     * @ORM\Column(name="iso3", type="string", length=45, nullable=true)
     */
    private $iso3;

    /**
     * @var string
     *
     * @ORM\Column(name="fips", type="string", length=45, nullable=true)
     */
    private $fips;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="continent", type="string", length=45, nullable=true)
     */
    private $continent;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_code", type="string", length=45, nullable=true)
     */
    private $currencyCode;

    /**
     * @var string
     *
     * @ORM\Column(name="currency_name", type="string", length=45, nullable=true)
     */
    private $currencyName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_prefix", type="string", length=45, nullable=true)
     */
    private $phonePrefix;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=45, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="languages", type="string", length=45, nullable=true)
     */
    private $languages;

    /**
     * @var string
     *
     * @ORM\Column(name="geonameid", type="string", length=45, nullable=true)
     */
    private $geonameid;

    /**
     *
     * @param \Doctrine\Common\Collections\Collection $property
     *        	@ORM\OneToMany(targetEntity="CountryState",mappedBy="country")
     */
    private $states;

    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     *
     */
    public function __get($property) {
    	if (\method_exists ( $this, "get" . \ucfirst ( $property ) )) {
    		$method_name = "get" . \ucfirst ( $property );
    		return $this->$method_name ();
    	} else {
    		 
    		if (is_object ( $this->$property )) {
    			// return $this->$property->id;
    		}
    		return $this->$property;
    	}
    }
    
    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     *
     */
    public function __set($property, $value) {
    	if (\method_exists ( $this, "set" .\ucfirst ( $property ) )) {
    		$method_name = "set" .\ucfirst ( $property );
    		 
    		$this->$method_name ( $value );
    	} else
    		$this->$property = $value;
    }
}
