<?php

namespace Config\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CountryState
 *
 * @ORM\Table(name="country_state")
 * @ORM\Entity
 */
class CountryState {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="name", type="string", length=45,
	 *      nullable=true)
	 */
	private $name;
	
	/**
	 *
	 * @var string @ORM\Column(name="timezone", type="string", length=45,
	 *      nullable=true)
	 */
	private $timezone;
	
	/**
	 *
	 * @var \Country @ORM\ManyToOne(targetEntity="Country", inversedBy="states")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="country_id", referencedColumnName="id",nullable=true,onDelete="CASCADE")
	 *      })
	 */
	private $country;
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $property        	
	 * @return mixed
	 *
	 */
	public function __get($property) {
		if (\method_exists ( $this, "get" .\ucfirst ( $property ) )) {
			$method_name = "get" .\ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" . \ucfirst ( $property ) )) {
			$method_name = "set" . \ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
}
