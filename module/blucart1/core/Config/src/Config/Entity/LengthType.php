<?php
namespace Config\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * LengthType
 *
 * @ORM\Table(name="length_type")
 * @ORM\Entity
 */
class LengthType {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="name", type="string", length=45,
	 *      nullable=true)
	 */
	private $name;
	
	/**
	 *
	 * @var string @ORM\Column(name="short_name", type="string", length=45,
	 *      nullable=true)
	 */
	private $shortName;
	
	/**
	 *
	 * @var float @ORM\Column(name="rate", type="float", nullable=true)
	 */
	private $rate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	public function __get($property) {
		if (\method_exists ( $this, "get" . \ucfirst ( $property ) )) {
			$method_name = "get" . \ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" .\ucfirst ( $property ) )) {
			$method_name = "set" .\ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
}
