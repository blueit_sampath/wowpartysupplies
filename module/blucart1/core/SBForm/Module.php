<?php

namespace SBForm;

class Module {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
       
         $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
            $this,
            'attachAssets'
        ));
       
    }

    public function attachAssets($e) {
        $controller = $e->getTarget();

        if (!($controller instanceof \Zend\Mvc\Controller\AbstractActionController)) {
            return;
        }

        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();

        $viewHelperPluginManager = $serviceManager->get('view_helper_manager');

        $headScript = $viewHelperPluginManager->get('headScript');
        $basePath = $viewHelperPluginManager->get('basePath');

        $config = $serviceManager->get('Config');
        if (isset($config ['validation_options'])) {

            if (isset($config ['validation_options'] ['include_js']) && $config ['validation_options'] ['include_js']) {
                $headScript()->appendFile($basePath('/assets/jquery_validate/js/jquery.validate.js'));
                $headScript()->appendFile($basePath('/assets/jquery_validate/js/additional-methods.js'));
                $headScript()->appendFile($basePath('/assets/jquery_validate/js/sb-form.js'));
//                 $view->headScript ()->appendFile ( $view->basePath (
//                 'jquery_validate/js/languages/jquery.validationEngine-en.js'
//                 ) );
//                 $view->headScript ()->appendFile ( $view->basePath (
//                 'jquery_validate/js/jquery.validationEngine.js' ) );
            }

            if (isset($config ['validation_options'] ['include_twitter_bootstrap']) && $config ['validation_options'] ['include_twitter_bootstrap']) {
                $headScript()->appendFile($basePath('/assets/jquery_validate/js/jquery.validate.bootstrap.js'));
            }
        }
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}
