<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Form\Form;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;
use SBForm\Service\Renderer\Interfaces\AttachElementInterface;

abstract class AbstractElement implements AttachElementInterface {
	protected $_element;
	protected $_validator;
	protected $_form;
	
	/**
	 * @return Form
	 */
	public function getForm(){
		return $this->_form;
	}
	
	public function setForm($form){
		$this->_form = $form;
		return $this;
	}
	/**
	 * (non-PHPdoc)
	 *
	 * @see \SBForm\Service\Renderer\Interfaces\AttachElementInterface::setValidator()
	 */
	public function setValidator(AbstractValidator $validator) {
		$this->_validator = $validator;
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \SBForm\Service\Renderer\Interfaces\AttachElementInterface::getValidator()
	 * @return AbstractValidator
	 */
	public function getValidator() {
		return $this->_validator;
	}
	/**
	 * (non-PHPdoc)
	 *
	 * @see \SBForm\Service\Renderer\Interfaces\AttachElementInterface::setElement()
	 *
	 */
	public function setElement(Element $element) {
		$this->_element = $element;
		return $this;
	}
	
	/**
	 *
	 * @return Element
	 */
	public function getElement() {
		return $this->_element;
	}
	public function setServiceLocator($sm) {
		$this->_sm = $sm;
		return $this;
	}
	public function getServiceLocator() {
		return $this->_sm;
	}
	public function getGivenMessages() {
		$validator = $this->getValidator ();
		$element = $this->getElement ();
		$className = get_class ( $validator );
		$options = $validator->getOptions();
		
	
		
		$class = new $className ();
		
		$array1 = $class->getMessageTemplates ();
		$array2 = $this->getMessageTemplates ();
		
		$array = array ();
		foreach ( $array2 as $key => $value ) {
			if (isset ( $array1 [$key] ) && $value != $array1 [$key]) {
				$array [$key] = $value;
			}
		}
		return $array;
	}
	public function getMessageTemplates() {
		$validator = $this->getValidator ();
		return $validator->getMessageTemplates ();
	}
	public function attachAttributes() {
		$messages = $this->getMessageTemplates ();
		foreach ( $messages as $key => $message ) {
			$this->addValidationType ( $key );
		}
		$messages = $this->getGivenMessages ();
		foreach ( $messages as $key => $message ) {
			$this->addMessage ( $key, $message );
		}
		
		return true;
	}
	public function addCustomValidationType($errorType, $value) {
		$element = $this->getElement ();
		$errorTypes = $element->getAttribute ( 'customErrorType' );
		if (! $errorTypes) {
			$errorTypes = array ();
		}
		$errorTypes [$errorType] = $value;
		$element->setAttribute ( 'customErrorType', $errorTypes );
	}
	public function addCustomValidationMessage($errorType, $message) {
		$element = $this->getElement ();
		$errorTypes = $element->getAttribute ( 'customErrorMessage' );
		if (! $errorTypes) {
			$errorTypes = array ();
		}
		$errorTypes [$errorType] = $message;
		$element->setAttribute ( 'customErrorMessage', $errorTypes );
	}
	abstract protected function addMessage($key, $message);
	abstract protected function addValidationType($key);
}

