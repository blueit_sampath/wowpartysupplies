<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class Digits extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		
		switch ($key) {
			case 'notDigits' :
			case 'digitsStringEmpty' :
			case 'digitsInvalid' :
				$this->addCustomValidationMessage ( 'numbersonly', $message );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		
		switch ($key) {
			case 'notDigits' :
			case 'digitsStringEmpty' :
			case 'digitsInvalid' :
				$this->addCustomValidationType ( 'numbersonly', true );
		}
		return true;
	}
}
