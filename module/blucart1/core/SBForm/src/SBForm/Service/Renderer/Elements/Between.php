<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class Between extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		
		switch ($key) {
			
			case 'notBetween' :
			case 'notBetweenStrict' :
				$message = str_replace ( '%min%', '{0}', $message );
				$message = str_replace ( '%max%', '{1}', $message );
				$this->addCustomValidationMessage ( 'range', $message );
				
				break;
			
			// $this->addValidationTypeToElement ( 'custom[email]' );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			case 'notBetween' :
			case 'notBetweenStrict' :
				
				$min = $validator->getMin ();
				$max = $validator->getMax ();
				if ($min) {
					if(is_numeric($min)){
						$min = (int)$min;
					}
					if(is_numeric($max)){
						$max = (int)$max;
					}
					$this->addCustomValidationType ( 'range', array (
							$min,
							$max 
					) );
				}
				break;
				
				return true;
		}
	}
}
