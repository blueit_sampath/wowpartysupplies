<?php

namespace SBForm\Service\Renderer\Interfaces;

use Zend\Form\Element;

use Zend\Validator\AbstractValidator;

interface AttachElementInterface {
	public function setValidator(AbstractValidator $validator);
	public function getValidator();
	
	public function setElement(Element $element);
	public function getElement();
	public function getGivenMessages();
	public function attachAttributes();
	

}
