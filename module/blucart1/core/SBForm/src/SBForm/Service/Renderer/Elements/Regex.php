<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class Regex extends AbstractElement {
	public function getGivenMessages() {
		$validator = $this->getValidator ();
		$element = $this->getElement ();
		$className = get_class ( $validator );
		$options = $validator->getOptions ();
		
		$class = new $className ( array (
				'pattern' => $validator->getPattern () 
		) );
		
		$array1 = $class->getMessageTemplates ();
		$array2 = $this->getMessageTemplates ();
		
		$array = array ();
		foreach ( $array2 as $key => $value ) {
			if (isset ( $array1 [$key] ) && $value != $array1 [$key]) {
				$array [$key] = $value;
			}
		}
		return $array;
	}
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		
		switch ($key) {
			case 'regexInvalid' :
			case 'regexNotMatch' :
			case 'regexErrorous' :
				$this->addCustomValidationMessage ( 'regex', $message );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator();
		switch ($key) {
			case 'regexInvalid' :
			case 'regexNotMatch' :
			case 'regexErrorous' :
				
				$this->addCustomValidationType ( 'regex', $validator->getPattern() );
		}
		return true;
	}
}
