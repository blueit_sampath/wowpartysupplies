<?php
namespace SBForm\Service\Renderer\Interfaces;
interface AttachFormInterface {
	public function setForm($form);
	public function getForm();
	public function attachAttributesToElements();
}

