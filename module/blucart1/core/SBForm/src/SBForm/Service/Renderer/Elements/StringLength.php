<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class StringLength extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
	
		switch ($key) {
			
			case 'stringLengthInvalid' :
			
				break;
			
			case 'stringLengthTooShort' :
				$message = str_replace ( '%min%' , '{0}' , $message );
				$this->addCustomValidationMessage('minlength', $message);
				break;
			
			case 'stringLengthTooLong' :
				$message = str_replace ( '%max%' , '{0}' , $message );
				$this->addCustomValidationMessage('maxlength', $message);
				break;
			
			// $this->addValidationTypeToElement ( 'custom[email]' );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			case 'stringLengthInvalid' :
				
				break;
			
			case 'stringLengthTooShort' :
				
				$min = $validator->getMin ();
				
				if ($min) {
					$this->addCustomValidationType ( 'minlength',  $min  );
				}
				break;
			
			case 'stringLengthTooLong' :
				$max = $validator->getMax ();
				if ($max) {
						$this->addCustomValidationType ( 'maxlength',  $max  );
				}
				break;
		}
		return true;
	}
}
