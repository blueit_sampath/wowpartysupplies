<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class GreaterThan extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			
			case 'notGreaterThan' :
			case 'notGreaterThanInclusive' :
				$message = str_replace ( '%min%' , '{0}' , $message );
				$inclusive = $validator->getInclusive();
				if ($inclusive) {
						$this->addCustomValidationMessage('min', $message);
				}
				else{
					$this->addCustomValidationMessage ( 'greaterthan',  $message  );
				}
				
				
				break;
			
			
			// $this->addValidationTypeToElement ( 'custom[email]' );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			
			case 'notGreaterThan' :
			case 'notGreaterThanInclusive' :
				$min = $validator->getMin ();
				if(is_numeric($min)){
					$min = (int)$min;
				}
				$inclusive = $validator->getInclusive();
				if ($inclusive) {
					$this->addCustomValidationType ( 'min',  $min  );
				}
				else{
					$this->addCustomValidationType ( 'greaterthan',  $min  );
				}
				
				break;
		}
		return true;
	}
}
