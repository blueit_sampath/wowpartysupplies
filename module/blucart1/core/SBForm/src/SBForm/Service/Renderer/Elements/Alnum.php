<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class Alnum extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			case 'alnumInvalid' :
			case 'notAlnum' :
			case 'alnumStringEmpty' :
				if (! $validator->getOption ( 'allowWhiteSpace' )) {
					$this->addCustomValidationMessage ( 'alphanumericWithoutSpace', $message );
				} else {
					$this->addCustomValidationMessage ( 'alphanumericWithSpace', $message );
				}
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		
		switch ($key) {
			case 'alnumInvalid' :
			case 'notAlnum' :
			case 'alnumStringEmpty' :
				if (! $validator->getOption ( 'allowWhiteSpace' )) {
					$this->addCustomValidationType ( 'alphanumericWithoutSpace', true );
				} else {
					$this->addCustomValidationType ( 'alphanumericWithSpace', true );
				}
		}
		return true;
	}
}
