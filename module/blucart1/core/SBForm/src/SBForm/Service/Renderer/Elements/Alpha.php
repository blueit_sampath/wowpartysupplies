<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class Alpha extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			case 'alphaInvalid' :
			case 'notAlpha' :
			case 'alphaStringEmpty' :
				if (! $validator->getOption ( 'allowWhiteSpace' )) {
					$this->addCustomValidationMessage ( 'lettersonly', $message );
				} else {
					$this->addCustomValidationMessage ( 'lettersWithSpace', $message );
				}
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		
		switch ($key) {
			case 'alphaInvalid' :
			case 'notAlpha' :
			case 'alphaStringEmpty' :
				if (! $validator->getOption ( 'allowWhiteSpace' )) {
					$this->addCustomValidationType ( 'lettersonly', true );
				} else {
					$this->addCustomValidationType( 'lettersWithSpace', true );
				}
		}
		return true;
	}
}
