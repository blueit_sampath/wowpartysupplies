<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class Identical extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			
			case 'notSame' :
			case 'missingToken' :
					$this->addCustomValidationMessage ( 'equalTo',  $message  );
				
				
				break;
			
			
			// $this->addValidationTypeToElement ( 'custom[email]' );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			
		case 'notSame' :
			case 'missingToken' :
				$token = $validator->getToken();
					$this->addCustomValidationType ( 'equalTo', 'form[name="'.$this->getForm()->getName().'"] input[name="'.$token.'"]'  );
				
				break;
		}
		return true;
	}
}
