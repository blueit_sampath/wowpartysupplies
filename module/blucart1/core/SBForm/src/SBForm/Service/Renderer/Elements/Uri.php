<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class Uri extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		
		switch ($key) {
			case 'uriInvalid' :
			case 'notUri' :
				
				$this->addCustomValidationMessage ( 'url', $message );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		
		switch ($key) {
			case 'uriInvalid' :
			case 'notUri' :
				
				$this->addCustomValidationType ( 'url', true );
		}
		return true;
	}
}
