<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;

use Zend\Form\Element;



class Int extends AbstractElement{
	
	
	protected function addMessage($key, $message){
		$element = $this->getElement();
		
		switch ($key){
			case 'intInvalid':
			case 'notInt':
				$this->addCustomValidationMessage('integer', $message);
				
		}
		return true;
	}
	
	protected function addValidationType($key){
		$element = $this->getElement();
		
		switch ($key){
			case 'intInvalid':
			case 'notInt':
				$this->addCustomValidationType ( 'integer',  true  );
	
		}
		return true;
	}

}
