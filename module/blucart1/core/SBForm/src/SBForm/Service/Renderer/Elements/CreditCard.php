<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class CreditCard extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		
		switch ($key) {
			case 'creditcardChecksum' :
			case 'creditcardContent' :
			case 'creditcardLength' :
			case 'creditcardInvalid' :
			case 'creditcardPrefix' :
			case 'creditcardService' :
			case 'creditcardServiceFailure' :
				
				$this->addCustomValidationMessage ( 'creditcard', $message );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		
		switch ($key) {
			case 'creditcardChecksum' :
			case 'creditcardContent' :
			case 'creditcardLength' :
			case 'creditcardInvalid' :
			case 'creditcardPrefix' :
			case 'creditcardService' :
			case 'creditcardServiceFailure' :
				
				$this->addCustomValidationType ( 'creditcard', true );
		}
		return true;
	}
}
