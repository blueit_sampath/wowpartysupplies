<?php

namespace SBForm\Service\Renderer;

use SBForm\Service\Renderer\Elements\AbstractElement;
use Zend\Form\Fieldset;
use Zend\Di\ServiceLocator;
use Zend\Form\Form;
use SBForm\Service\Renderer\Interfaces\AttachFormInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PrepareForm implements AttachFormInterface, ServiceLocatorAwareInterface {

    /**
     *
     * @var ServiceLocator
     */
    protected $services;

    /**
     *
     * @var Form
     */
    protected $_form;

    /**
     * (non-PHPdoc)
     *
     * @see \SBForm\Service\Renderer\Interfaces\AttachFormInterface::setForm()
     */
    public function setForm($form) {
        $this->_form = $form;
        return $this;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \SBForm\Service\Renderer\Interfaces\AttachFormInterface::getForm()
     * @return Form
     */
    public function getForm() {
        return $this->_form;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->services;
    }

    public function getConfig() {
        $config = $this->getServiceLocator()->get('config');
        if (isset($config ['validation_mappers'])) {
            return $config ['validation_mappers'];
        }
        return false;
    }

    public function attachAttributesToElements($form = null, $inputFilter = null, $displayOptions = array()) {
        $options = array();
        $displayColon = false;
        $displayAstrik = true;


        if (!$form) {
            $form = $this->getForm();
            if (! $form->getAttribute('action')) {
                $sm = $this->getServiceLocator();
                $request = $sm->get ( 'Request' );
                $uri = $request->getUri ();
                $path = $uri->getPath ();
                $form->setAttribute('action', $path);
            }

            if (isset($displayOptions ['elements'])) {
                $options = $displayOptions ['elements'];
            }
        } else {

            if (isset($displayOptions [$form->getName()])) {
                $options = $displayOptions [$form->getName()];
            }
        }
        if ($options && count($options)) {
            if (isset($options ['attributes'])) {
                $attributes = $form->getAttributes();
                $form->setAttributes(($attributes + $options['attributes']));
            }
        }

        if (!$inputFilter) {
            $inputFilter = $form->getInputFilter();
        }

        $config = $this->getConfig();

        foreach ($form->getElements() as $name => $element) {
            $optionE = $element->getOptions();
            $attributes = $element->getAttributes();
            if (isset($options [$name])) {

                if (isset($options [$name] ['attributes'])) {
                    $attributes = $attributes + $options [$name] ['attributes'];
                }

                if (isset($options [$name] ['options'])) {
                    $optionE = $optionE + $options [$name] ['options'];
                }
            }
            if ($displayColon) {
                $l = $element->getLabel() ? $element->getLabel() : (isset($optionE['label']) ? $optionE['label'] : '');
                if ($l) {
                    $l = $l . ': ';
                }
                $optionE['label'] = $l;
            }


            if ($validatorItem = $inputFilter->get($name)) {

                if ($validatorItem->isRequired()) {
                    $errorTypes = $element->getAttribute('customErrorType');
                    if (!$errorTypes) {
                        $errorTypes = array();
                    }
                    $errorTypes ['required'] = true;
                    $element->setAttribute('customErrorType', $errorTypes);
                    if ($displayAstrik) {
                        if (isset($optionE['label'])) {
                            $l = $optionE['label'];
                        } else {
                            $l = $element->getLabel();
                        }
                        if ($l) {
                            $l = $l . '*';
                        }
                        //$element->setLabel($l);
                        $optionE['label'] = $l;
                    }
                }

                $validators = $validatorItem->getValidatorChain()->getValidators();

                foreach ($validators as $key => $validator) {

                    $instance = $validator ['instance'];

                    $className = get_class($instance);
                    foreach ($config as $validationmapper => $value) {

                        if ($instance instanceof $validationmapper) {

                            $parseValidatorObject = $this->getJValidator($value);
                            $parseValidatorObject->setForm($this->getForm());
                            $parseValidatorObject->setElement($element);
                            $parseValidatorObject->setValidator($instance);
                            $parseValidatorObject->setServiceLocator($this->getServiceLocator());
                            $parseValidatorObject->attachAttributes();
                        }
                    }
                }
            }
            $element->setAttributes($attributes);
            $element->setOptions($optionE);
        }

        foreach ($form->getFieldsets() as $fieldset) {
            $name = $fieldset->getName();
            $attributes = $fieldset->getAttributes();
            if (isset($options [$name] ['attributes'])) {
                $fieldset->setAttributes(($attributes + $options [$name] ['attributes']));
            }
            $this->attachAttributesToElements($fieldset, $inputFilter->get($name), $displayOptions);
        }
        return $this;
    }

    /**
     *
     * @param string $value        	
     * @return AbstractElement
     */
    public function getJValidator($value) {
        return $this->getServiceLocator()->get($value);
    }

}
