<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;

use Zend\Form\Element;



class Float extends AbstractElement{
	
	
	protected function addMessage($key, $message){
		$element = $this->getElement();
		
		switch ($key){
			case 'floatInvalid':
			case 'notFloat':
				$this->addCustomValidationMessage('number', $message);
				
		}
		return true;
	}
	
	protected function addValidationType($key){
		$element = $this->getElement();
		
		switch ($key){
			case 'floatInvalid':
			case 'notFloat':
				$this->addCustomValidationType ( 'number',  true  );
	
		}
		return true;
	}

}
