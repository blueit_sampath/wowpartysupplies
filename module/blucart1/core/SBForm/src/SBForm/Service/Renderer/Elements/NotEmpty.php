<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;

use Zend\Form\Element;



class NotEmpty extends AbstractElement{
	
	
	protected function addMessage($key, $message){
		$element = $this->getElement();
		
		switch ($key){
			case 'isEmpty':
			case 'notEmptyInvalid':
				$this->addCustomValidationMessage('required', $message);
				
		}
		return true;
	}
	
	protected function addValidationType($key){
		$element = $this->getElement();
		
		switch ($key){
			case 'isEmpty':
			case 'notEmptyInvalid':
				
				$this->addCustomValidationType ( 'required', true );
	
		}
		return true;
	}
	
	
}

