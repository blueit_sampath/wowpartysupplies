<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class EmailAddress extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
	
		switch ($key) {
			case 'emailAddressInvalid' :
			case 'emailAddressInvalidFormat' :
			case 'emailAddressInvalidHostname' :
			case 'emailAddressInvalidMxRecord' :
			case 'emailAddressInvalidSegment' :
			case 'emailAddressDotAtom' :
			case 'emailAddressQuotedString' :
			case 'emailAddressInvalidLocalPart' :
			case 'emailAddressLengthExceeded' :
				$this->addCustomValidationMessage('email', $message);
				
			
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		
		switch ($key) {
			case 'emailAddressInvalid' :
			case 'emailAddressInvalidFormat' :
			case 'emailAddressInvalidHostname' :
			case 'emailAddressInvalidMxRecord' :
			case 'emailAddressInvalidSegment' :
			case 'emailAddressDotAtom' :
			case 'emailAddressQuotedString' :
			case 'emailAddressInvalidLocalPart' :
			case 'emailAddressLengthExceeded' :
				$this->addCustomValidationType ( 'email', true );
		}
		return true;
	}
}
