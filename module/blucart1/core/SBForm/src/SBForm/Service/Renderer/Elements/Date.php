<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class Date extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			case 'dateInvalid' :
			case 'dateInvalidDate' :
			case 'dateFalseFormat' :
					$this->addCustomValidationMessage ( 'dateISO', $message );
				
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		
		switch ($key) {
			case 'dateInvalid' :
			case 'dateInvalidDate' :
			case 'dateFalseFormat' :
					$this->addCustomValidationType( 'dateISO', true );
				
		}
		return true;
	}
}
