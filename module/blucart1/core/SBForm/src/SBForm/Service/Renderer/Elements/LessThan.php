<?php

namespace SBForm\Service\Renderer\Elements;

use Zend\Validator\AbstractValidator;
use Zend\Form\Element;

class LessThan extends AbstractElement {
	protected function addMessage($key, $message) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			
			case 'notLessThan' :
			case 'notLessThanInclusive' :
				$message = str_replace ( '%max%' , '{0}' , $message );
				$inclusive = $validator->getInclusive();
				if ($inclusive) {
						$this->addCustomValidationMessage('max', $message);
				}
				else{
					$this->addCustomValidationMessage ( 'lessthan',  $message  );
				}
				
				
				break;
			
			
			// $this->addValidationTypeToElement ( 'custom[email]' );
		}
		return true;
	}
	protected function addValidationType($key) {
		$element = $this->getElement ();
		$validator = $this->getValidator ();
		switch ($key) {
			
			case 'notLessThan' :
			case 'notLessThanInclusive' :
				$max = $validator->getMax ();
				if(is_numeric($max)){
					$max = (int)$max;
				}
				$inclusive = $validator->getInclusive();
				if ($inclusive) {
					$this->addCustomValidationType ( 'max',  $max  );
				}
				else{
					$this->addCustomValidationType ( 'lessthan',  $max  );
				}
				
				break;
		}
		return true;
	}
}
