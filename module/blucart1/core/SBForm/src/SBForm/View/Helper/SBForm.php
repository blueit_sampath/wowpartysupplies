<?php

namespace SBForm\View\Helper;

use SBForm\Service\Renderer\PrepareForm;
use Zend\Debug\Debug;
use Zend\Form\Form;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

class SBForm extends AbstractHelper implements ServiceLocatorAwareInterface {
	protected $services;
	protected $messages = array ();
	protected $errors = array ();
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->services->getServiceLocator ();
	}
	public function __invoke(Form $form, $displayOptions = array()) {
		$form->prepare ();
		$prepareFormObject = $this->prepareForm ();
		$prepareFormObject->setForm ( $form );
		$prepareFormObject->attachAttributesToElements (null, null,$displayOptions);
		$this->attachJavascript ( $form );
		return $form;
	}
	/**
	 *
	 * @return PrepareForm
	 */
	public function prepareForm() {
		return $this->getServiceLocator ()->get ( 'SBForm\Service\Renderer\PrepareForm' );
	}
	public function attachJavascript($form) {
		$view = $this->getView ();
		$config = $this->getConfig();
		$string = '';
		if (isset ( $config ['validation_options'] ['validateOptions'] ) && $config ['validation_options'] ['validateOptions']) {
			$string = implode ( ', ', $config ['validation_options'] ['validateOptions'] );
		}
		
		$onload = $view->render ( 'common/sb-form.phtml', array (
				'form' => $form,
				'validationTypes' => $this->formValidationTypes ( $form ),
				'validationMessages' => $this->formValidationMessages ( $form ),
				'config' => $this->getServiceLocator ()->get ( 'Config' ) 
		) );
		$view->inlineScript ()->appendScript ( $onload );
		$this->clearNewAddedAttributes ( $form );
	}
	public function formValidationTypes($form) {
		foreach ( $form->getElements () as $element ) {
			$errors = $element->getAttribute ( 'customErrorType' );
			if ($errors) {
				$this->errors [$element->getName ()] = $errors;
			}
		}
		foreach ( $form->getFieldsets () as $fieldset ) {
			$this->formValidationTypes ( $fieldset );
		}
		return $this->errors;
	}
	public function formValidationMessages($form) {
		foreach ( $form->getElements () as $element ) {
			$errors = $element->getAttribute ( 'customErrorMessage' );
			if ($errors) {
				$this->messages [$element->getName ()] = $errors;
			}
		}
		foreach ( $form->getFieldsets () as $fieldset ) {
			$this->formValidationMessages ( $fieldset );
		}
		return $this->messages;
	}
	public function clearNewAddedAttributes($form) {
		foreach ( $form->getElements () as $element ) {
			$element->removeAttribute ( 'customErrorType' );
			$element->removeAttribute ( 'customErrorMessage' );
		}
		foreach ( $form->getFieldsets () as $fieldset ) {
			$this->clearNewAddedAttributes ( $fieldset );
		}
		return;
	}
	public function getConfig() {
		return $this->getServiceLocator ()->get ( 'Config' );
	}
}
