<?php
namespace SBForm;
return array (
		'validation_mappers' => array (
				'Zend\Validator\NotEmpty' => 'SBForm\Service\Renderer\Elements\NotEmpty',
				'Zend\Validator\EmailAddress' => 'SBForm\Service\Renderer\Elements\EmailAddress',
				'Zend\Validator\StringLength' => 'SBForm\Service\Renderer\Elements\StringLength',
				'Zend\I18n\Validator\Int' => 'SBForm\Service\Renderer\Elements\Int',
				'Zend\I18n\Validator\Float' => 'SBForm\Service\Renderer\Elements\Float',
				'Zend\I18n\Validator\Alpha' => 'SBForm\Service\Renderer\Elements\Alpha',
				'Zend\I18n\Validator\Alnum' => 'SBForm\Service\Renderer\Elements\Alnum',
				'Zend\Validator\Digits' => 'SBForm\Service\Renderer\Elements\Digits',
				'Zend\Validator\Between' => 'SBForm\Service\Renderer\Elements\Between',
				'Zend\Validator\Date' => 'SBForm\Service\Renderer\Elements\Date',
				'Zend\Validator\GreaterThan' => 'SBForm\Service\Renderer\Elements\GreaterThan',
				'Zend\Validator\LessThan' => 'SBForm\Service\Renderer\Elements\LessThan',
				'Zend\Validator\CreditCard' => 'SBForm\Service\Renderer\Elements\CreditCard',
				'Zend\Validator\Uri' => 'SBForm\Service\Renderer\Elements\Uri',
				'Zend\Validator\Regex' => 'SBForm\Service\Renderer\Elements\Regex',
				'Zend\Validator\Identical' => 'SBForm\Service\Renderer\Elements\Identical'
		),
		'service_manager' => array (
				'invokables' => array (
						'SBForm\Service\Renderer\PrepareForm' => 'SBForm\Service\Renderer\PrepareForm',
						/* validators */
						'SBForm\Service\Renderer\Elements\EmailAddress' => 'SBForm\Service\Renderer\Elements\EmailAddress',
						'SBForm\Service\Renderer\Elements\NotEmpty' => 'SBForm\Service\Renderer\Elements\NotEmpty',
						'SBForm\Service\Renderer\Elements\StringLength' => 'SBForm\Service\Renderer\Elements\StringLength',
						'SBForm\Service\Renderer\Elements\Int' => 'SBForm\Service\Renderer\Elements\Int',
						'SBForm\Service\Renderer\Elements\Float' => 'SBForm\Service\Renderer\Elements\Float',
						'SBForm\Service\Renderer\Elements\Alpha' => 'SBForm\Service\Renderer\Elements\Alpha',
						'SBForm\Service\Renderer\Elements\Alnum' => 'SBForm\Service\Renderer\Elements\Alnum',
						'SBForm\Service\Renderer\Elements\Digits' => 'SBForm\Service\Renderer\Elements\Digits',
						'SBForm\Service\Renderer\Elements\Between' => 'SBForm\Service\Renderer\Elements\Between',
						'SBForm\Service\Renderer\Elements\Date' => 'SBForm\Service\Renderer\Elements\Date',
						'SBForm\Service\Renderer\Elements\GreaterThan' => 'SBForm\Service\Renderer\Elements\GreaterThan',
						'SBForm\Service\Renderer\Elements\LessThan' => 'SBForm\Service\Renderer\Elements\LessThan',
						'SBForm\Service\Renderer\Elements\CreditCard' => 'SBForm\Service\Renderer\Elements\CreditCard',
						'SBForm\Service\Renderer\Elements\Uri' => 'SBForm\Service\Renderer\Elements\Uri',
						'SBForm\Service\Renderer\Elements\Regex' => 'SBForm\Service\Renderer\Elements\Regex',
						'SBForm\Service\Renderer\Elements\Identical' => 'SBForm\Service\Renderer\Elements\Identical'
				) 
		),
		'view_helpers' => array (
				'invokables' => array (
						'sbForm' => 'SBForm\View\Helper\SBForm' 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'SBForm' => __DIR__ . '/../public' 
						) 
				) 
		),
		'validation_options' => array (
				'include_js' => true,
				'include_css' => true,
				'include_twitter_bootstrap' => true,
				'validateOptions' => array (
						'onsubmit' => true,
				       
						
				) 
		),
		'view_manager' => array (
				
				'template_path_stack' => array (
						'SBForm' => __DIR__ . '/../view' 
				),
				'template_map' => array (
						'common/sb-form' => __DIR__ . '/../view/common/sb-form.phtml' 
				) 
		) 
);
