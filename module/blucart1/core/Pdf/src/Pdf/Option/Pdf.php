<?php
namespace Pdf\Option;

use Doctrine\ORM\Query\AST\ParenthesisExpression;
use Core\Functions;

class Pdf extends \mPDF {

    protected $url = '';

    public function WriteHTML($html, $sub = 0, $init = true, $close = true) {
        $html = $this->substituteUrls($html);
        return parent::WriteHTML($html, $sub, $init, $close);
       
    }

    public function substituteUrls($html) {
        $DOM = new \DOMDocument();
        libxml_use_internal_errors(true);
        $DOM->loadHTML($html);

        $imgs = $DOM->getElementsByTagName('img');
        foreach ($imgs as $img) {
            $src = $img->getAttribute('src');
            $src = trim($src);
            if (!((strpos($src, 'http://') === 0) || (strpos($src, 'https://') === 0) || (strpos($src, 'data:') === 0))) {
                $img->setAttribute('src', $this->getServerUrl($src));
            }
        }

        $as = $DOM->getElementsByTagName('a');
        foreach ($as as $a) {
            $src = $a->getAttribute('href');
            if ($src) {
                $src = trim($src);
                if (!((strpos($src, 'http://') === 0) || (strpos($src, 'https://') === 0) || (strpos($src, 'data:') === 0))) {
                    $a->setAttribute('href', $this->getServerUrl($src));
                }
            }
        }

        $html = $DOM->saveHTML();
        return $html;
    }

    public function getServerUrl($src) {

        $sm = Functions::getServiceLocator();
        $serverUrl = $sm->get('viewHelperManager')->get('serverUrl');
        return rtrim($serverUrl($src), " \t\n\r\0\x0B/");
    }
    
    
    public function getContent(){
        return $this->Output('', 'S');
    }
    
    public function sendToBrowserAsInline(){
        return $this->Output();
       
    }
    public function sendToBrowserAsDownload($fileName = ''){
        return $this->Output($fileName,'D');
       
    }
    
    public function saveAsFile($filePath = ''){
        return $this->Output($filePath, 'F');
    }
    
    
}
