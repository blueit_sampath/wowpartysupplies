<?php 
namespace Pdf;

return array (
		'router' => array (
				'routes' => array (
				
// 						'automate' => array (
// 								'type' => 'segment',
// 								'options' => array (
// 										'route' => '/automate[/:action][/:limit]',
// 										'constraints' => array (
// 												'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
// 										)
// 										,
// 										'defaults' => array (
// 												'controller' => __NAMESPACE__ . '\Controller\Index',
// 												'action' => 'index' 
// 										) 
// 								) 
//						) 
				
				)

				 
		),
		'events' => array (
			
		),
		'service_manager' => array (
				'invokables' => array (
		
				),
				'factories' => array (
	
				) 
		),
		
		'controllers' => array (
				'invokables' => array (
					 
				) 
		),
		'view_manager' => array (
				
				'template_path_stack' => array (
						__NAMESPACE__ => __DIR__ . '/../view'
				),
		),
		
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								__NAMESPACE__ => __DIR__ . '/../public'
						)
				)
		),
);
