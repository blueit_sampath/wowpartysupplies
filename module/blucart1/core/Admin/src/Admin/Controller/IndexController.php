<?php

namespace Admin\Controller;

use Common\MVC\Controller\AbstractAdminController;
use Admin\Api\AdminApi;
use Zend\Session\Container;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractAdminController {

    public function indexAction() {
      
        $session = AdminApi::getDashboardSession();
        if ($startDate = $this->params()->fromPost('startDate')) {
            $session->startDate = \DateTime::createFromFormat('Y-m-d', $startDate);
        }
        if ($endDate = $this->params()->fromPost('endDate')) {
            $session->endDate = \DateTime::createFromFormat('Y-m-d', $endDate);
        }
        return array();
    }

}
