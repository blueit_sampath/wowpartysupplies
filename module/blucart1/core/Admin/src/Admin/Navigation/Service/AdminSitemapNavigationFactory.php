<?php
namespace Admin\Navigation\Service;


class AdminSitemapNavigationFactory extends AdminMenuNavigationFactory {
	protected function getName() {
		return 'admin_sitemap';
	}
	protected function getEventName() {
		return 'admin-sitemap';
	}
	
}
