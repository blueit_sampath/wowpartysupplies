<?php

namespace Admin\Navigation\Service;



class AdminLinkNavigationFactory extends AdminMenuNavigationFactory {
	protected function getName() {
		return 'admin_link';
	}
	public function getEventName() {
		return 'admin-link';
	}
}
