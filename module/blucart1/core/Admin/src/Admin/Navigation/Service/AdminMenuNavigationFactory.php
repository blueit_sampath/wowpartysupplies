<?php

namespace Admin\Navigation\Service;

use Common\Navigation\Service\MenuNavigationFactory;

use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\Config;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\RouteStackInterface as Router;
use Zend\Navigation\Exception;
use Zend\Navigation\Navigation;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class AdminMenuNavigationFactory extends MenuNavigationFactory {
	protected $_events;
	protected function getName() {
		return 'admin_menu';
	}
	
	public function getEventName(){
		return 'admin-menu';
	}
	
	
}
