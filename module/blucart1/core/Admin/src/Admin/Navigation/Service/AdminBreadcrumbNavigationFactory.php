<?php

namespace Admin\Navigation\Service;


class AdminBreadcrumbNavigationFactory extends AdminMenuNavigationFactory {
	protected function getName() {
		return 'admin_breadcrumb';
	}
	
	
	public function getEventName(){
		return 'admin-breadcrumb';
	}
	
	
}
