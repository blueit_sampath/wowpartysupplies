<?php

namespace Admin\Navigation\Event;

use Zend\Navigation\Navigation;
use Core\Functions;
use Doctrine\ORM\EntityManager;
use Core\Form\Form;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

class AdminNavigationEvent extends AbstractEvent {
	protected $_navigation = null;
	
	public function getPriority(){
		return null;
	}
	
	public function attach(EventManagerInterface $events) {
		
		$priority = $this->getPriority ();
		
		$sharedEventManager = $events->getSharedManager ();
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'admin-menu', array (
				$this,
				'preAdminMenu' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'admin-breadcrumb', array (
				$this,
				'preAdminBreadcrumb' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'admin-link', array (
				$this,
				'preAdminLink' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'admin-sitemap', array (
				$this,
				'preAdminSitemap' 
		), $priority );
		
		return $this;
	}
	public function preAdminMenu($e) {
		$navigation = $e->getParam ( 'navigation' );
		$this->setNavigation ( $navigation );
		return $this->menu ();
	}
	
	public function preAdminBreadcrumb($e) {
		$navigation = $e->getParam ( 'navigation' );
		$this->setNavigation ( $navigation );
		return $this->breadcrumb ();
	}
	
	
	public function menu() {
		$this->common ();
	}
	public function breadcrumb() {
		$this->common ();
	}
	
	public function common(){
		
	}
	
	public function preAdminSitemap($e) {
		$navigation = $e->getParam ( 'navigation' );
		$this->setNavigation ( $navigation );
		return $this->sitemap ();
	}
	public function sitemap() {
	}
	public function preAdminLink($e) {
		$navigation = $e->getParam ( 'navigation' );
		$this->setNavigation ( $navigation );
		return $this->link ();
	}
	public function link() {
		
	}
	
	/**
	 *
	 * @return Navigation $_navigation
	 */
	public function getNavigation() {
		if(!$this->_navigation){
			return new Navigation();
		}
		return $this->_navigation;
	}
	
	/**
	 *
	 * @param Navigation $_navigation        	
	 * @param
	 *        	class_name
	 */
	public function setNavigation($_navigation) {
		$this->_navigation = $_navigation;
		return $this;
	}
}
