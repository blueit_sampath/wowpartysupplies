<?php 
namespace Admin\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardDateRangeForm extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-date-range-form';

	public function getBlockName() {
		return 'AdminDashboardDateRangeForm';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardDateRangeForm';
	}
	
}

