<?php

namespace Admin\Api;

use Common\Api\Api;
use Zend\Session\Container;

class AdminApi extends Api {

    /**
     *
     * @return \Zend\Session\Container
     */
    public static function getDashboardSession() {
        $session = new Container('adminDashboard');
        if (!$session->startDate) {
            $startDate = new \DateTime ();
            $startDate->setTimestamp(strtotime('-1 week'));
            $session->startDate = $startDate;

            $session->endDate = new \DateTime ();
        }
        return $session;
    }

    public static function getChartType() {
        $session = static::getDashboardSession();
        $startDate = $session->startDate;
        $endDate = $session->endDate;
        $diff = $endDate->diff($startDate)->format("%a");
        $diff = abs($diff);
        if ($diff >= 0 && $diff <= 10) {
            return 'day';
        }

        if ($diff >= 11 && $diff <= 60) {
            return 'week';
        }

        if ($diff >= 61 && $diff <= 365) {
            return 'month';
        }

        if ($diff >= 365) {
            return 'year';
        }
    }

   

}
