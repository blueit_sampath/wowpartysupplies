<?php

namespace Admin;

use Zend\ModuleManager\ModuleManager;
use Common\MVC\Controller\AbstractAdminController;
use Core\Functions;
use User\Api\UserApi;
use Zend\Mvc\ModuleRouteListener;

class Module {

    public function init(ModuleManager $moduleManager) {
        // $sharedEvents =
        // $moduleManager->getEventManager()->getSharedManager();
        // $sharedEvents->attach('*', 'route', function($e) {
        // $controller = $e->getTarget();
        // if ($controller instanceof AbstractAdminController) {
        // $controller->layout('layout/front');
        // }
        // }, 100);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener ();
        $moduleRouteListener->attach($eventManager);

        $eventManager = $e->getApplication()->getEventManager();

        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
                    $this,
                    'checkAdminAccess'
        ));

        $e->getApplication()->getEventManager()->getSharedManager()
                ->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
                    $this,
                    'changeLayout'
                        ), -100);
    }

    public function checkAdminAccess($e) {
        $matches = $e->getRouteMatch();
        if ($matches) {

            $controller = $e->getTarget();
            $action = $matches->getParam('action');
            $routeName = $matches->getMatchedRouteName();
            if ($routeName == 'admin-login') {
                return;
            }
            $app = $e->getApplication();
            $sm = $app->getServiceManager();
            if ($controller instanceof AbstractAdminController) {
                $urlPlugin = Functions::getUrlPlugin();
                $user = UserApi::getLoggedInUser();
                if (!$user) {
                    $response = $sm->get('Response');
                    $response->getHeaders()->addHeaderLine('Location', $urlPlugin('admin-login'));
                    $response->setStatusCode(302);
                    return $response;
                }
                if (!($user->userRole && $user->userRole->accessAdmin)) {
                    Functions::addErrorMessage('Access Denied');
                    $response = $sm->get('Response');
                    $response->getHeaders()->addHeaderLine('Location', $urlPlugin('home'));
                    $response->setStatusCode(302);
                    return $response;
                }
            }
        }
    }

    public function changeLayout($e) {
        $matches = $e->getRouteMatch();

        if ($matches) {
            $routeName = $matches->getMatchedRouteName();
            $controller = $e->getTarget();
            $app = $e->getApplication();
            $sm = $app->getServiceManager();
            $request = $sm->get('Request');


            $routeName = $matches->getMatchedRouteName();
            if ($controller instanceof AbstractAdminController) {
                if ($routeName == 'admin-login') {
                    $this->attachAssetsForTheme($sm, 'admin', 'login');
                    $controller->layout('layout/admin-login');
                    return;
                }

                if ($request->isXmlHttpRequest()) {

                    $controller->layout('layout/admin-ajax-layout');
                    return;
                }

                $value = Functions::fromRoute('ajax');
                if ($value && ($value === 'true')) {
                    $this->attachAssetsForTheme($sm, 'admin', 'iframe');
                    $controller->layout('layout/admin-iframe-layout');
                } else {
                    $this->attachAssetsForTheme($sm, 'admin', 'main');
                    $controller->layout('layout/admin-layout');
                }
            }
        }
    }

    public function attachAssetsForTheme($sm, $theme, $type = 'main') {

        $config = $sm->get('Config');


        if (isset($config['theme'][$theme])) {
            $headScript = $sm->get('viewhelpermanager')->get('headScript');
            $headLink = $sm->get('viewhelpermanager')->get('headLink');

            $basePath = $sm->get('viewhelpermanager')->get('basePath');
            $themeConfig = $config['theme'][$theme];


            if (isset($themeConfig['js']) && isset($themeConfig['js'][$type])) {

                $array = array_reverse($themeConfig['js'][$type], true);
                foreach ($array as $value) {
                    $value = trim($value);

                    if ($value) {
                        if ((strpos($value, '//') === 0) || (strpos($value, 'http://') === 0) || (strpos($value, 'https://') === 0)) {
                            $headScript->prependFile($value);
                        } else {
                            $headScript->prependFile($basePath($value));
                        }
                    }
                }
            }

            if (isset($themeConfig['css']) && isset($themeConfig['css'][$type])) {
                $array = array_reverse($themeConfig['css'][$type], true);
                foreach ($array as $value) {
                    $value = trim($value);
                    if ($value) {
                        if ((strpos($value, '//') === 0) || (strpos($value, 'http://') === 0) || (strpos($value, 'https://') === 0)) {
                            $headLink->prependStylesheet($value);
                        } else {
                            $headLink->prependStylesheet($basePath($value));
                        }
                    }
                }
            }
        }
    }

}
