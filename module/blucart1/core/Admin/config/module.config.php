<?php

return array(
    'controllers' => array('invokables' => array('Admin\Controller\Index' => 'Admin\Controller\IndexController')),
    'router' => array('routes' => array('admin' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Index',
                        'action' => 'index'
                    )
                )
            ))),
    'service_manager' => array(
        'factories' => array(
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
            'admin_menu' => 'Admin\Navigation\Service\AdminMenuNavigationFactory',
            'admin_breadcrumb' => 'Admin\Navigation\Service\AdminBreadcrumbNavigationFactory',
            'admin_sitemap' => 'Admin\Navigation\Service\AdminSitemapNavigationFactory',
            'admin_link' => 'Admin\Navigation\Service\AdminLinkNavigationFactory'
        ),
        'invokables' => array('Admin\Service\Block\AdminDashboardDateRangeForm' => 'Admin\Service\Block\AdminDashboardDateRangeForm')
    ),
    'view_manager' => array(
        'template_path_stack' => array('Admin' => __DIR__.'/../view'),
        'template_map' => array()
    ),
    
    'asset_manager' => array(
        'resolver_configs' => array(
            'collections' => array(
                'assets/admin/js/base-all.js' => array(
                    '/assets/admin/js/jquery.min.js',
                    '/assets/admin/js/jquery-migrate.min.js',
                    '/assets/admin/lib/jquery-ui/jquery-ui-1.10.0.custom.min.js',
                    '/assets/admin/lib/kendo/js/kendo.all.min.js',
                    '/assets/admin/js/jquery.debouncedresize.min.js',
                    '/assets/admin/js/jquery.actual.min.js',
                    '/assets/admin/js/jquery_cookie.min.js',
                    '/assets/admin/bootstrap/js/bootstrap.min.js',
                    '/assets/admin/js/bootstrap.plugins.min.js',
                    '/assets/admin/lib/qtip2/jquery.qtip.min.js',
                    '/assets/admin/lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js',
                    
                    '/assets/admin/js/ios-orientationchange-fix.js',
                    '/assets/admin/lib/antiscroll/antiscroll.js',
                    '/assets/admin/lib/antiscroll/jquery-mousewheel.js',
                    '/assets/admin/lib/colorbox/jquery.colorbox.min.js',
                    '/assets/admin/js/selectNav.js',
                    '/assets/admin/js/common.js',
                    '/assets/admin/lib/jquery-ui/external/globalize.js',
                    '/assets/admin/js/forms/jquery.ui.touch-punch.min.js',
                    '/assets/admin/js/forms/jquery.inputmask.min.js',
                    '/assets/admin/js/forms/jquery.autosize.min.js',
                    '/assets/admin/js/forms/jquery.counter.min.js',
                    '/assets/admin/lib/datepicker/bootstrap-datepicker.min.js',
                    '/assets/admin/lib/datepicker/bootstrap-timepicker.min.js',
                    '/assets/admin/lib/tag_handler/jquery.taghandler.min.js',
                    '/assets/admin/lib/uniform/jquery.uniform.min.js',
                    '/assets/admin/js/forms/jquery.progressbar.anim.js',
                    '/assets/admin/lib/multi-select/js/jquery.multi-select.js',
                    '/assets/admin/lib/multi-select/js/jquery.quicksearch.js',
                    '/assets/admin/lib/chosen/chosen.jquery.min.js',
                    '/assets/admin/lib/tiny_mce/jquery.tinymce.js',
                    '/assets/admin/lib/plupload/js/plupload.full.js',
                    '/assets/admin/lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.full.js',
                    '/assets/admin/lib/colorpicker/bootstrap-colorpicker.js',
                    '/assets/admin/lib/toggle_buttons/jquery.toggle.buttons.js',
                    '/assets/admin/js/forms.js',
                    '/assets/admin/lib/Zoombox/zoombox.js',
                    '/assets/admin/js/jquery.textarea-expander.js',
                    '/assets/admin/lib/UItoTop/jquery.ui.totop.min.js',
                    '/assets/admin/lib/loadmask/jquery.loadmask.min.js',
                    '/assets/admin/js/jquery.peity.min.js',
                    '/assets/admin/js/jquery.mediaTable.min.js',
                    '/assets/admin/js/jquery.imagesloaded.min.js',
                    '/assets/admin/js/jquery.wookmark.js',
                    '/assets/admin/lib/nprogress/nprogress.js',
                    '/assets/admin/lib/bootstrap-daterangepicker/moment.js',
                    '/assets/admin/lib/bootstrap-daterangepicker/daterangepicker.js',
                    '/assets/admin/js/jquery-ui-i18n.js',
                    '/assets/admin/js/dashboard.js',
                    '/assets/admin/lib/classy/classy.js',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.min.js',
                ),
                'assets/admin/js/admin-login.js' => array(
                    '/assets/admin/js/jquery.min.js',
                    '/assets/admin/js/jquery-migrate.min.js',
                    '/assets/admin/js/jquery.actual.min.js',
                    '/assets/admin/bootstrap/js/bootstrap.min.js',
                    '/assets/admin/js/bootstrap.plugins.min.js',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.min.js',
                )
            ),
            'paths' => array('Admin' => __DIR__.'/../public')
        ),
        'caching' => array(
            'assets/admin/js/base-all.js' => array(
                'cache' => 'FilePath',
                'options' => array('dir' => 'public')
            ),
            'assets/admin/js/admin-login.js' => array(
                'cache' => 'FilePath',
                'options' => array('dir' => 'public')
            )
        )
    ),
    'view_helpers' => array('invokables' => array('newmenu' => 'Admin\View\Helper\Menu')),
    'navigation' => array(
        'admin_menu' => array(
            array(
                'label' => 'Products',
                'uri' => '#',
                'id' => 'productMain',
                'iconClass' => 'glyphicon glyphicon-list-alt'
            ),
            array(
                'label' => 'Orders',
                'uri' => '#',
                'id' => 'orderMain',
                'iconClass' => 'glyphicon glyphicon-gift'
            ),
            array(
                'label' => 'Customers',
                'uri' => '#',
                'id' => 'customerMain',
                'iconClass' => 'glyphicon glyphicon-user'
            ),
            array(
                'label' => 'Website',
                'uri' => '#',
                'id' => 'websiteMain',
                'iconClass' => 'glyphicon glyphicon-globe '
            ),
            array(
                'label' => 'Marketing',
                'uri' => '#',
                'id' => 'marketingMain',
                'iconClass' => 'glyphicon glyphicon-film'
            ),
            array(
                'label' => 'Report',
                'uri' => '#',
                'id' => 'reportMain',
                'iconClass' => 'glyphicon glyphicon-signal '
            )
        ),
        'admin_breadcrumb' => array('home' => array(
                'label' => '<i class="glyphicon glyphicon-home"></i>',
                'route' => 'admin',
                'pages' => array(
                    array(
                        'label' => 'Products',
                        'uri' => '#',
                        'id' => 'productMain',
                        'iconClass' => 'glyphicon glyphicon-th-large icon-white'
                    ),
                    array(
                        'label' => 'Orders',
                        'uri' => '#',
                        'id' => 'orderMain',
                        'iconClass' => 'glyphicon glyphicon-briefcase'
                    ),
                    array(
                        'label' => 'Customers',
                        'uri' => '#',
                        'id' => 'customerMain',
                        'iconClass' => 'glyphicon glyphicon-user'
                    ),
                    array(
                        'label' => 'Website',
                        'uri' => '#',
                        'id' => 'websiteMain',
                        'iconClass' => 'glyphicon glyphicon-globe '
                    ),
                    array(
                        'label' => 'Marketing',
                        'uri' => '#',
                        'id' => 'marketingMain',
                        'iconClass' => 'glyphicon glyphicon-film '
                    ),
                    array(
                        'label' => 'Report',
                        'uri' => '#',
                        'id' => 'reportMain',
                        'iconClass' => 'glyphicon glyphicon-signal'
                    )
                )
            ))
    ),
    'events' => array(
        'Admin\Service\Block\AdminDashboardDateRangeForm' => 'Admin\Service\Block\AdminDashboardDateRangeForm'
    ),
    'theme' => array(
        'admin' => array(
            'js' => array(
                'iframe' => array(
                    '/assets/admin/js/base-all.js'
                ),
                'main' => array(
                    '/assets/admin/js/base-all.js'),
                'login' => array(
                    '/assets/admin/js/admin-login.js'
                ),
            ),
            'css' => array(
                'iframe' => array(
                    '/assets/admin/bootstrap/css/bootstrap.min.css',
                    '/assets/admin/lib/jquery-ui/css/Aristo/Aristo.css',
                    '/assets/admin/lib/jBreadcrumbs/css/BreadCrumb.css',
                    '/assets/admin/lib/qtip2/jquery.qtip.min.css',
                    '/assets/admin/lib/google-code-prettify/prettify.css',
                    "/assets/admin/img/splashy/splashy.css",
                    "/assets/admin/lib/datepicker/datepicker.css",
                    "/assets/admin/lib/tag_handler/css/jquery.taghandler.css",
                    "/assets/admin/lib/uniform/Aristo/uniform.aristo.css",
                    "/assets/admin/lib/multi-select/css/multi-select.css",
                    "/assets/admin/lib/chosen/chosen.css",
                    "/assets/admin/lib/plupload/js/jquery.plupload.queue/css/plupload-gebo.css",
                    "/assets/admin/lib/colorbox/colorbox.css",
                    "/assets/admin/lib/colorpicker/css/colorpicker.css",
                    "/assets/admin/lib/toggle_buttons/bootstrap-toggle-buttons.css",
                    "/assets/admin/lib/kendo/css/kendo.common-bootstrap.min.css",
                    "/assets/admin/lib/kendo/css/kendo.bootstrap.min.css",
                    "/assets/admin/lib/kendo/css/kendo.dataviz.min.css",
                    "/assets/admin/lib/kendo/css/kendo.dataviz.bootstrap.min.css",
                    "/assets/admin/lib/Zoombox/zoombox.css",
                    "/assets/admin/lib/loadmask/jquery.loadmask.css",
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.css',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.icons.css',
                    '/assets/admin/css/style.css',
                    '/assets/admin/css/blue.css',
                    '/assets/admin/css/blucart.css',
                    "/assets/admin/lib/nprogress/nprogress.css",
                    '/assets/admin/lib/bootstrap-daterangepicker/daterangepicker-bs3.css',
                    'http://fonts.googleapis.com/css?family=PT+Sans'
                ),
                'main' => array(
                    '/assets/admin/bootstrap/css/bootstrap.min.css',
                    '/assets/admin/lib/jquery-ui/css/Aristo/Aristo.css',
                    '/assets/admin/lib/jBreadcrumbs/css/BreadCrumb.css',
                    '/assets/admin/lib/qtip2/jquery.qtip.min.css',
                    
                    '/assets/admin/lib/google-code-prettify/prettify.css',
                    "/assets/admin/img/splashy/splashy.css",
                    "/assets/admin/lib/datepicker/datepicker.css",
                    "/assets/admin/lib/tag_handler/css/jquery.taghandler.css",
                    "/assets/admin/lib/uniform/Aristo/uniform.aristo.css",
                    "/assets/admin/lib/multi-select/css/multi-select.css",
                    "/assets/admin/lib/chosen/chosen.css",
                    "/assets/admin/lib/plupload/js/jquery.plupload.queue/css/plupload-gebo.css",
                    "/assets/admin/lib/colorbox/colorbox.css",
                    "/assets/admin/lib/colorpicker/css/colorpicker.css",
                    "/assets/admin/lib/toggle_buttons/bootstrap-toggle-buttons.css",
                    "/assets/admin/lib/kendo/css/kendo.common-bootstrap.min.css",
                    "/assets/admin/lib/kendo/css/kendo.bootstrap.min.css",
                    "/assets/admin/lib/kendo/css/kendo.dataviz.min.css",
                    "/assets/admin/lib/kendo/css/kendo.dataviz.bootstrap.min.css",
                    "/assets/admin/lib/Zoombox/zoombox.css",
                    "/assets/admin/lib/loadmask/jquery.loadmask.css",
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.css',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.icons.css',
                    '/assets/admin/css/style.css',
                    '/assets/admin/css/blue.css',
                    '/assets/admin/css/blucart.css',
                    "/assets/admin/lib/nprogress/nprogress.css",
                    '/assets/admin/lib/bootstrap-daterangepicker/daterangepicker-bs3.css',
                    'http://fonts.googleapis.com/css?family=PT+Sans'
                ),
                'login' => array(
                    '/assets/admin/bootstrap/css/bootstrap.min.css',
                    '/assets/admin/css/style.css',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.css',
                    '/assets/blucart/lib/pnotify/jquery.pnotify.default.icons.css',
                    '/assets/admin/css/blue.css',
                    '/assets/admin/css/blucart.css',
                    'http://fonts.googleapis.com/css?family=PT+Sans'
                )
            )
        ),
    )
);
