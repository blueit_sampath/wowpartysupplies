<?php

namespace Mail\Form\AdminMail;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class MailForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'subject',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Subject' 
				) 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'htmlMessage',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'HTML Message' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'plainMessage',
				'attributes' => array (
						'type' => 'textarea',
						'rows' => '5',
						'columns' => '100' 
				),
				'options' => array (
						'label' => 'Plain Message' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'fromName',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'fromEmail',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'toEmails',
				'attributes' => array (
						'type' => 'hidden'
				)
		) );
		
		$this->add ( array (
				'name' => 'toNames',
				'attributes' => array (
						'type' => 'hidden'
				)
		) );
		
		$this->add ( array (
				'name' => 'redirectUrl',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Send' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
}