<?php
namespace Mail\Form\AdminMail;

use Common\Form\Option\AbstractFormFactory;

class MailFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Mail\Form\AdminMail\MailForm' );
		$form->setName ( 'adminMailAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Mail\Form\AdminMail\MailFilter' );
	}
}
