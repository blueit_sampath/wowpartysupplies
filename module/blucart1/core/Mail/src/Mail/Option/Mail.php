<?php
namespace Mail\Option;

use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mime\Part as MimePart;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mime\Mime;

class Mail implements ServiceLocatorAwareInterface
{

    protected $_message;

    protected $_transport;

    protected $_attachments = array();

    protected $_subject = '';

    protected $_textMessage = '';

    protected $_htmlMessage = '';

    protected $_from = array();

    protected $_reply = array();

    protected $_to = array();

    protected $_bcc = array();

    protected $_cc = array();

    protected $_encoding = 'UTF-8';

    protected $_sm;

 
    public function sendMail($queue = false, $sendAtOnce = false)
    {
        if ($sendAtOnce) {
            $this->sendBulk($queue);
        } else {
            $this->sendOneByOne($queue);
        }
    }

    public function sendBulk($queue)
    {
        $message = $this->getMessage();
        $transport = $this->getTransport();
        $message->setTo(array());
        $emails = array_unique($this->getFroms());
        $message->getHeaders()->removeHeader('to');
        
        foreach ($emails as $email => $nameOrEmail) {
            if (is_int($email)) {
                $message->addTo($nameOrEmail);
            } else {
                $message->addTo($email, $nameOrEmail);
            }
        }
        $transport->send($message);
    }

    public function sendOneByOne($queue)
    {
        $message = $this->getMessage();
        $transport = $this->getTransport();
        $message->setTo(array());
        $emails = array_unique($this->getTos());
        foreach ($emails as $email => $nameOrEmail) {
            $message->getHeaders()->removeHeader('to');
            if (is_int($email)) {
                $message->addTo($nameOrEmail);
            } else {
                $message->addTo($email, $nameOrEmail);
            }
           
            $transport->send($message);
          
          
            
        }
       
    }

    public function formMessage()
    {
        $message = $this->getMessage();
        $encoding = $this->getEncoding();
        $message->setEncoding($encoding);
        
        $content = new MimeMessage();
        $htmlPart = null;
        if ($html = $this->getHtmlMessage()) {
            $html = $this->substituteUrls($html);
            $htmlPart = new MimePart($html);
            $htmlPart->type = 'text/html';
        }
      
        $textPart = null;
        if ($text = $this->getTextMessage()) {
            $textPart = new MimePart($text);
            $textPart->type = 'text/plain';
        }
        $content->setParts(array(
            $textPart,
            $htmlPart
        ));
        
        
        
        $contentPart = new MimePart($content->generateMessage());
        $contentPart->type = 'multipart/alternative;' . PHP_EOL . ' boundary="' . $content->getMime()->boundary() . '"';
        
        $array = array(
            $contentPart
        );
        $attachments = $this->getAttachments();
        
        foreach ($attachments as $key => $attachment) {
            $array[] = $attachment;
        }
       
        
        $body = new MimeMessage();
        $body->setParts($array);
        $message->setBody($body);
        
        $message->setSubject($this->getSubject());
        $message->getHeaders()->removeHeader('from');
        
        $emails = $this->getFroms();
        foreach ($emails as $email => $nameOrEmail) {
            if (is_int($email)) {
                $message->addFrom($nameOrEmail);
            } else {
                $message->addFrom($email, $nameOrEmail);
            }
        }
        
        $emails = $this->getReplys();
        $message->getHeaders()->removeHeader('reply-to');
        
        foreach ($emails as $email => $nameOrEmail) {
            if (is_int($email)) {
                $message->addReplyTo($nameOrEmail);
            } else {
                $message->addReplyTo($email, $nameOrEmail);
            }
        }
        
        $emails = $this->getBccs();
        $message->getHeaders()->removeHeader('bcc');
        
        foreach ($emails as $email => $nameOrEmail) {
            if (is_int($email)) {
                $message->addBcc($nameOrEmail);
            } else {
                $message->addBcc($email, $nameOrEmail);
            }
        }
        $emails = $this->getCcs();
        $message->getHeaders()->removeHeader('cc');
        foreach ($emails as $email => $nameOrEmail) {
            if (is_int($email)) {
                $message->addCc($nameOrEmail);
            } else {
                $message->addCc($email, $nameOrEmail);
            }
        }
       
        return $this;
    }

    public function substituteUrls($html)
    {
        $DOM = new \DOMDocument();
         libxml_use_internal_errors(true);
        $DOM->loadHTML($html);
        
        $imgs = $DOM->getElementsByTagName('img');
        foreach ($imgs as $img) {
            $src = $img->getAttribute('src');
            $src = trim($src);
            if (! ((strpos($src, 'http://') === 0) || (strpos($src, 'https://') === 0) || (strpos($src, 'data:') === 0))) {
                $img->setAttribute('src', $this->getServerUrl($src));
            }
        }
        
        $as = $DOM->getElementsByTagName('a');
        foreach ($as as $a) {
            $src = $a->getAttribute('href');
            if ($src) {
                $src = trim($src);
                if (! ((strpos($src, 'http://') === 0) || (strpos($src, 'https://') === 0) || (strpos($src, 'data:') === 0))) {
                    $a->setAttribute('href', $this->getServerUrl($src));
                }
            }
        }
        
        $html = $DOM->saveHTML();
        return $html;
    }

    public function getServerUrl($src)
    {
        $serverUrl = $this->getServiceLocator()
            ->get('viewHelperManager')
            ->get('serverUrl');
        return rtrim($serverUrl($src), " \t\n\r\0\x0B/");
    }

    /**
     *
     * @param Message $_message            
     * @return \Mail\Option\Mail
     */
    public function setMessage(Message $_message)
    {
       
        $this->_message = $_message;
        return $this;
    }

    /**
     *
     * @return Message
     */
    public function getMessage()
    {
        if (! $this->_message) {
            $this->_message = new Message();
        }
        
        return $this->_message;
    }

    /**
     *
     * @param SendmailTransport $_transport            
     * @return \Mail\Option\Mail
     */
    public function setTransport(SendmailTransport $_transport)
    {
        $this->_transport = $_transport;
        return $this;
    }

    /**
     *
     * @return SendmailTransport
     */
    public function getTransport()
    {
        if (! $this->_transport) {
            $this->_transport = new SendmailTransport();
        }
        return $this->_transport;
    }

    /**
     *
     * @return MimeAttachment[] $_attachments
     */
    public function getAttachments()
    {
        return $this->_attachments;
    }

    /**
     *
     * @param MimeAttachment[] $_attachments            
     * @return \Mail\Option\Mail
     */
    public function setAttachments($_attachments)
    {
        $this->_attachments = $_attachments;
        return $this;
    }

    /**
     *
     * @param string $key            
     * @param string $message            
     * @param string $type            
     * @return \Zend\Mime\Part
     */
    public function addAttachment($key, $message, $type = 'application/pdf', $fileName = '', $encoding = Mime::ENCODING_BASE64, $disposition = Mime::DISPOSITION_ATTACHMENT)
    {
   
        $attachment = new MimePart($message);
        $attachment->type = $type;
         $attachment->encoding = $encoding;
        $attachment->disposition = $disposition;
        if ($fileName) {
            $attachment->filename = $fileName;
        }
        $attachment->id = $key;
      
        $this->_attachments[$key] = $attachment;
        return $attachment;
    }

    /**
     *
     * @param string $key            
     * @return \Zend\Mime\Attachment
     */
    public function getAttachment($key)
    {
        if (isset($this->_attachments[$key])) {
            return $this->_attachments[$key];
        }
        return false;
    }

    /**
     *
     * @param string $key            
     * @return \Mail\Option\Mail
     */
    public function removeAttachment($key)
    {
        if ($this->getAttachment($key)) {
            unset($this->_attachments[$key]);
        }
        return $this;
    }

    /**
     *
     * @return \Mail\Option\Mail
     */
    public function clearAttachments()
    {
        $this->_attachments = array();
        return $this;
    }

    /**
     *
     * @return string $_subject
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    /**
     *
     * @param string $_subject            
     * @return \Mail\Option\Mail
     */
    public function setSubject($_subject)
    {
        $this->_subject = $_subject;
        return $this;
    }

    /**
     *
     * @return string $_textMessage
     */
    public function getTextMessage()
    {
        return $this->_textMessage;
    }

    /**
     *
     * @param string $_textMessage            
     * @return \Mail\Option\Mail
     */
    public function setTextMessage($_textMessage)
    {
        $this->_textMessage = $_textMessage;
        return $this;
    }

    /**
     *
     * @return string $_htmlMessage
     */
    public function getHtmlMessage()
    {
        return $this->_htmlMessage;
    }

    /**
     *
     * @param string $_htmlMessage            
     * @return \Mail\Option\Mail
     */
    public function setHtmlMessage($_htmlMessage)
    {
     
        $this->_htmlMessage = $_htmlMessage;
        return $this;
    }

    /**
     *
     * @return string $_defaultFromEmail
     */
    public function getDefaultFromEmail()
    {
        return $this->_defaultFromEmail;
    }

    /**
     *
     * @param string $_defaultFromEmail            
     * @return \Mail\Option\Mail
     */
    public function setDefaultFromEmail($_defaultFromEmail)
    {
        $this->_defaultFromEmail = $_defaultFromEmail;
        return $this;
    }

    /**
     *
     * @return string $_defaultFromName
     */
    public function getDefaultFromName()
    {
        return $this->_defaultFromName;
    }

    /**
     *
     * @param string $_defaultFromName            
     * @return \Mail\Option\Mail
     */
    public function setDefaultFromName($_defaultFromName)
    {
        $this->_defaultFromName = $_defaultFromName;
        return $this;
    }

    /**
     *
     * @return string $_defaultReplyEmail
     */
    public function getDefaultReplyEmail()
    {
        return $this->_defaultReplyEmail;
    }

    /**
     *
     * @param string $_defaultReplyEmail            
     * @return \Mail\Option\Mail
     */
    public function setDefaultReplyEmail($_defaultReplyEmail)
    {
        $this->_defaultReplyEmail = $_defaultReplyEmail;
        return $this;
    }

    /**
     *
     * @return string $_defaultReplyName
     */
    public function getDefaultReplyName()
    {
        return $this->_defaultReplyName;
    }

    /**
     *
     * @param string $_defaultReplyName            
     * @return \Mail\Option\Mail
     */
    public function setDefaultReplyName($_defaultReplyName)
    {
        $this->_defaultReplyName = $_defaultReplyName;
        return $this;
    }

    /**
     *
     * @return array $_to
     */
    public function getTos()
    {
        return $this->_to;
    }

    /**
     *
     * @param array $_to            
     * @return \Mail\Option\Mail
     */
    public function setTos($_to)
    {
        $this->_to = $_to;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @param string $name            
     * @return \Mail\Option\Mail
     */
    public function addTo($email, $name = null)
    {
        $this->_to[$email] = $name;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @return string boolean
     */
    public function getTo($email)
    {
        if (isset($this->_to[$email])) {
            return $this->_to[$email];
        }
        $key = array_search($email, $this->_to);
        if ($key) {
            $email;
        }
        return false;
    }

    /**
     *
     * @param string $email            
     * @return \Mail\Option\Mail
     */
    public function removeTo($email)
    {
        if (isset($this->_to[$email])) {
            unset($this->_to[$email]);
        }
        $key = array_search($email, $this->_to);
        if ($key) {
            unset($this->_to[$key]);
        }
        return $this;
    }

    /**
     *
     * @return \Mail\Option\Mail
     */
    public function clearTos()
    {
        $this->_to = array();
        return $this;
    }

    /**
     *
     * @return array $_bcc
     */
    public function getBccs()
    {
        return $this->_bcc;
    }

    /**
     *
     * @param array $_bcc            
     * @return \Mail\Option\Mail
     */
    public function setBccs($_bcc)
    {
        $this->_bcc = $_bcc;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @param string $name            
     * @return \Mail\Option\Mail
     */
    public function addBcc($email, $name = null)
    {
        $this->_bcc[$email] = $name;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @return string boolean
     */
    public function getBcc($email)
    {
        if (isset($this->_bcc[$email])) {
            return $this->_bcc[$email];
        }
        $key = array_search($email, $this->_bcc);
        if ($key) {
            $email;
        }
        return false;
    }

    /**
     *
     * @param string $email            
     * @return \Mail\Option\Mail
     */
    public function removeBcc($email)
    {
        if (isset($this->_bcc[$email])) {
            unset($this->_bcc[$email]);
        }
        $key = array_search($email, $this->_bcc);
        if ($key) {
            unset($this->_bcc[$key]);
        }
        return $this;
    }

    /**
     *
     * @return \Mail\Option\Mail
     */
    public function clearBccs()
    {
        $this->_bcc = array();
        return $this;
    }

    /**
     *
     * @return array $_cc
     */
    public function getCcs()
    {
        return $this->_cc;
    }

    /**
     *
     * @param array $_cc            
     * @return \Mail\Option\Mail
     */
    public function setCcs($_cc)
    {
        $this->_cc = $_cc;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @param string $name            
     * @return \Mail\Option\Mail
     */
    public function addCc($email, $name = null)
    {
        $this->_cc[$email] = $name;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @return string boolean
     */
    public function getCc($email)
    {
        if (isset($this->_cc[$email])) {
            return $this->_cc[$email];
        }
        $key = array_search($email, $this->_cc);
        if ($key) {
            $email;
        }
        return false;
    }

    /**
     *
     * @param string $email            
     * @return \Mail\Option\Mail
     */
    public function removeCc($email)
    {
        if (isset($this->_cc[$email])) {
            unset($this->_cc[$email]);
        }
        $key = array_search($email, $this->_cc);
        if ($key) {
            unset($this->_cc[$key]);
        }
        return $this;
    }

    /**
     *
     * @return \Mail\Option\Mail
     */
    public function clearCcs()
    {
        $this->_cc = array();
        return $this;
    }

    /**
     *
     * @return array $_from
     */
    public function getFroms()
    {
        return $this->_from;
    }

    /**
     *
     * @param array $_from            
     * @return \Mail\Option\Mail
     */
    public function setFroms($_from)
    {
        $this->_from = $_from;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @param string $name            
     * @return \Mail\Option\Mail
     */
    public function addFrom($email, $name = null)
    {
        $this->_from[$email] = $name;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @return string boolean
     */
    public function getFrom($email)
    {
        if (isset($this->_from[$email])) {
            return $this->_from[$email];
        }
        $key = array_search($email, $this->_from);
        if ($key) {
            $email;
        }
        return false;
    }

    /**
     *
     * @param string $email            
     * @return \Mail\Option\Mail
     */
    public function removeFrom($email)
    {
        if (isset($this->_from[$email])) {
            unset($this->_from[$email]);
        }
        $key = array_search($email, $this->_from);
        if ($key) {
            unset($this->_from[$key]);
        }
        return $this;
    }

    /**
     *
     * @return \Mail\Option\Mail
     */
    public function clearFroms()
    {
        $this->_from = array();
        return $this;
    }

    /**
     *
     * @return array $_reply
     */
    public function getReplys()
    {
        return $this->_reply;
    }

    /**
     *
     * @param array $_reply            
     * @return \Mail\Option\Mail
     */
    public function setReplys($_reply)
    {
        $this->_reply = $_reply;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @param string $name            
     * @return \Mail\Option\Mail
     */
    public function addReply($email, $name = null)
    {
        $this->_reply[$email] = $name;
        return $this;
    }

    /**
     *
     * @param string $email            
     * @return string boolean
     */
    public function getReply($email)
    {
        if (isset($this->_reply[$email])) {
            return $this->_reply[$email];
        }
        $key = array_search($email, $this->_reply);
        if ($key) {
            $email;
        }
        return false;
    }

    /**
     *
     * @param string $email            
     * @return \Mail\Option\Mail
     */
    public function removeReply($email)
    {
        if (isset($this->_reply[$email])) {
            unset($this->_reply[$email]);
        }
        $key = array_search($email, $this->_reply);
        if ($key) {
            unset($this->_reply[$key]);
        }
        return $this;
    }

    /**
     *
     * @return \Mail\Option\Mail
     */
    public function clearReplys()
    {
        $this->_reply = array();
        return $this;
    }

    /**
     *
     * @return string $_encoding
     */
    public function getEncoding()
    {
        return $this->_encoding;
    }

    /**
     *
     * @param string $_encoding            
     * @return \Mail\Option\Mail
     */
    public function setEncoding($_encoding)
    {
        $this->_encoding = $_encoding;
        return $this;
    }

    /**
     *
     * @param ServiceLocatorInterface $serviceLocator            
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->_sm = $serviceLocator;
    }

    /**
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->_sm;
    }
}
