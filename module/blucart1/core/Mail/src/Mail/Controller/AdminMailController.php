<?php

namespace Mail\Controller;

use Mail\Option\Mail;
use Core\Functions;
use Common\MVC\Controller\AbstractAdminController;

class AdminMailController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Mail\Form\AdminMail\MailFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-mail-add', array (
				'action' => 'add' 
		) );
	}
	public function addAction() {
		$form = $this->getFormFactory ();
		$form->setAttribute ( 'action', $this->url ()->fromRoute ( 'admin-mail-add', array (
				'action' => 'add' 
		) ) );
		
		if (Functions::fromQuery ( 'subject' ) || Functions::fromQuery ( 'htmlMessage' ) || Functions::fromQuery ( 'plainMessage' )) {
			
			$form->get ( 'subject' )->setValue ( Functions::fromQuery ( 'subject' ) );
			$form->get ( 'htmlMessage' )->setValue ( Functions::fromQuery ( 'htmlMessage' ) );
			$form->get ( 'plainMessage' )->setValue ( Functions::fromQuery ( 'plainMessage' ) );
			$form->get ( 'fromName' )->setValue ( Functions::fromQuery ( 'fromName' ) );
			$form->get ( 'fromEmail' )->setValue ( Functions::fromQuery ( 'fromEmail' ) );
			$form->get ( 'toEmails' )->setValue ( Functions::fromQuery ( 'toEmails' ) );
			$form->get ( 'toNames' )->setValue ( Functions::fromQuery ( 'toNames' ) );
			$form->get ( 'redirectUrl' )->setValue ( Functions::fromQuery ( 'redirectUrl', '' ) );
		} elseif ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$mail = Functions::getMail ();
				$mail->addFrom ( $form->get ( 'fromEmail' )->getValue (), $form->get ( 'fromName' )->getValue () );
				$mail->setSubject ( $form->get ( 'subject' )->getValue () );
				$mail->setHtmlMessage ( $form->get ( 'htmlMessage' )->getValue () );
				$mail->setTextMessage ( $form->get ( 'plainMessage' )->getValue () );
				
				$array1 = explode ( ',', $form->get ( 'toEmails' )->getValue () );
				$array2 = explode ( ',', $form->get ( 'toNames' )->getValue () );
				
				foreach ( $array1 as $key => $value ) {
					$mail->addTo ( $value, $array2 [$key] );
				}
				
				$mail->formMessage ();
				$mail->sendMail ( true );
				
				Functions::addSuccessMessage ( 'Mail Sent Successfully' );
				return $this->redirect ()->toUrl ( $form->get ( 'redirectUrl' )->getValue () );
			} else {
				$this->notValid ();
			}
		} else {
		}
		return array (
				'form' => $form 
		);
	}
}