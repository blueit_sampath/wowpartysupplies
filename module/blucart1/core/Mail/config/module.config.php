<?php
namespace Mail;
return array (
		'router' => array (
				'routes' => array (
						'admin-mail-add' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin/mail/add[/ajax/:ajax]',
										'defaults' => array (
												'controller' => 'Mail\Controller\AdminMail',
												'action' => 'add',
												'ajax' => 'true'
										),
										'constraints' => array (
												'ajax' => 'true|false'
										)
								)
						),
						
				)
		),
		'controllers' => array (
				'invokables' => array (
						'Mail\Controller\AdminMail' => 'Mail\Controller\AdminMailController',
						
				)
		),
		'service_manager' => array (
				'invokables' => array (
						'Mail\Option\Mail' => 'Mail\Option\Mail',
						'Mail\Form\AdminMail\MailForm' => 'Mail\Form\AdminMail\MailForm',
						'Mail\Form\AdminMail\MailFilter' => 'Mail\Form\AdminMail\MailFilter' 
				),
				'shared' => array (
						'Mail\Option\Mail' => false 
				),
				'factories' => array (
						'Mail\Form\AdminMail\MailFactory' => 'Mail\Form\AdminMail\MailFactory' 
				) 
		),
		
		'view_manager' => array (
				'template_path_stack' => array (
						__NAMESPACE__ => __DIR__ . '/../view'
				)
		),
		
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								__NAMESPACE__ => __DIR__ . '/../public'
						)
				)
		)
);
