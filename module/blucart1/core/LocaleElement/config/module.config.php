<?php

namespace LocaleElement;

return array (
		
		'view_manager' => array (
				'template_path_stack' => array (
						'LocaleElement' => __DIR__ . '/../view' 
				),
				
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'LocaleElement' => __DIR__ . '/../public' 
						) 
				) 
		),
		
		
)
;
