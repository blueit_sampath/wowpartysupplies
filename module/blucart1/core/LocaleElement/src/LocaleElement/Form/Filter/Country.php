<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace LocaleElement\Form\Filter;

use Config\Api\ConfigApi;
use Zend\Filter\AbstractFilter;
class Country extends AbstractFilter
{
    /**
     * Defined by Zend\Filter\FilterInterface
     *
     * Returns (int) $value
     *
     * If the value provided is non-scalar, the value will remain unfiltered
     * and an E_USER_WARNING will be raised indicating it's unfilterable.
     *
     * @param  string $value
     * @return int|mixed
     */
    public function filter($value)
    {
    	 if (null === $value) {
            return null;
        }

        $v = ConfigApi::getCountryNameByCountryCode ( $value );
        if ($v) {
        	$value = $v;
        } 
        return $value;
    }
}
