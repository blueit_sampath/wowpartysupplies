<?php
namespace LocaleElement\Form\Element;

use Zend\Form\Element\Select;

use Zend\Form\Form;
use Zend\Form\Element\Hidden;

class StatePicker extends Select {
	
	/**
	 * @var bool
	 */
	protected $disableInArrayValidator = true;
	
	
	
	
	
	protected $country = null;
	
	

	protected $emptyOption = 'Please Select';
	
	/**
	 * Set the element value
	 *
	 * @param  mixed $value
	 * @return Element
	 */
	public function setValue($value)
	{
		$this->value = $value;
		$this->setAttribute('data-state', $value);
		return $this;
	}
	
	/**
	 * Retrieve the element value
	 *
	 * @return mixed
	 */
	public function getValue()
	{
		if(!$this->value){
			return $this->getAttribute('data-state');
		}
		return $this->value;
	}
	/**
	 * @return string $country
	 */
	public function getCountry() {
		return $this->country;
	}

	

	/**
	 * @param boolean $country
	 * @param class_name
	 */
	public function setCountry($country) {
		$this->country = $country;
		return $this;
	}

	
}