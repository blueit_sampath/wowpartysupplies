<?php
namespace LocaleElement\Form\Element;

use Zend\Form\Element\Checkbox;


use Zend\Form\Form;
use Zend\Form\Element\Hidden;

class CheckboxHtmlText extends Checkbox {
	
	protected $htmlText = '';
	/**
	 * @return string $htmlText
	 */
	public function getHtmlText() {
		return $this->htmlText;
	}

	/**
	 * @param string $htmlText
	 * @param class_name
	 */
	public function setHtmlText($htmlText) {
		$this->htmlText = $htmlText;
		return $this;
	}


	
}