<?php

namespace LocaleElement\Form\Element;

use Zend\Form\Element\Select;
use Zend\Form\Form;
use Zend\Form\Element\Hidden;
use Config\Api\ConfigApi;

class CountryPicker extends Select {
	
	/**
	 *
	 * @var bool
	 */
	protected $disableInArrayValidator = true;
	
	/**
	 *
	 * @var bool
	 */
	protected $countryFlags = true;
	
	/**
	 * Create an empty option (option with label but no value).
	 * If set to null, no option is created
	 *
	 * @var bool
	 */
	protected $emptyOption = 'Please Select';
	
	/**
	 * Set the element value
	 *
	 * @param mixed $value        	
	 * @return Element
	 */
	public function setValue($value) {
		$v = ConfigApi::getCountryNameByCountryCode ( $value );
		if ($v) {
			$this->value = $v;
		} else {
			$this->value = $value;
		}
		return $this;
	}
	
	/**
	 * Retrieve the element value
	 *
	 * @return mixed
	 */
	public function getValue() {
		return $this->value;
	}
	/**
	 *
	 * @return string $countryFlags
	 */
	public function getCountryFlags() {
		return $this->countryFlags;
	}
	
	/**
	 *
	 * @param boolean $countryFlags        	
	 * @param
	 *        	class_name
	 */
	public function setCountryFlags($countryFlags) {
		$this->countryFlags = $countryFlags;
		return $this;
	}
	
	
}