<?php

namespace LocaleElement\Form\View\Helper;

use Zend\Form\View\Helper\FormText;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;

class AddressPicker extends \TwbBundle\Form\View\Helper\TwbBundleFormElement {

    public function render(ElementInterface $element) {
        if ($element instanceof \LocaleElement\Form\Element\AddressPicker) {
            $name = $element->getName();
            if ($name === null || $name === '') {
                throw new Exception\DomainException(sprintf('%s requires that the element has an assigned name; none discovered', __METHOD__));
            }

            $attributes = $element->getAttributes();
            $view = $this->getView();
            $string = $view->render('common/address-picker-element', array(
                'element' => $element
                    ));
            return parent::render($element);
        } else {
            return parent::render($element);
        }
    }

}
