<?php

namespace LocaleElement\Form\View\Helper;

use Zend\Form\View\Helper\FormSelect;
use DluTwBootstrap\Form\View\Helper\FormSelectTwb;
use LocaleElement\Form\Element\StatePicker as sp;
use DluTwBootstrap\Form\View\Helper\FormHiddenTwb;
use FileUpload\Form\View\Helper\FormFile;
use TagElement\Form\Element\StatePicker as Tagele;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;

class StatePicker extends FormSelect {
	
	// protected $_url;
	// protected $_basePath;
	public function render(ElementInterface $element) {
		if ($element instanceof sp) {
			$name = $element->getName ();
			if ($name === null || $name === '') {
				throw new Exception\DomainException ( sprintf ( '%s requires that the element has an assigned name; none discovered', __METHOD__ ) );
			}
			
			$attributes = $element->getAttributes ();
			$attributes ['name'] = $name;
			
			$attributes ['data-state'] = $element->getValue ();
			$attributes ['id'] = $element->getName ();
			$country = $element->getCountry ();
			if ($country) {
				$attributes ['data-country'] = $country;
			}
			
			$class = "";
			if (isset ( $attributes ['class'] )) {
				$class .= $attributes ['class'];
			}
			$attributes ['class'] = $class . ' form-control';
			$view = $this->getView ();
			$string = $view->render ( 'common/state-picker-element', array (
					'element' => $element,
					'attributeString' => $this->createAttributesString ( $attributes ) 
			) );
			return $string;
		} else {
			return parent::render ( $element );
		}
	}
}