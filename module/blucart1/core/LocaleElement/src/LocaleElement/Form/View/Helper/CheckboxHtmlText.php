<?php

namespace LocaleElement\Form\View\Helper;

use Zend\Form\View\Helper\FormCheckbox;

use DluTwBootstrap\Form\View\Helper\FormCheckboxTwb;

use LocaleElement\Form\Element\StatePicker as sp;
use DluTwBootstrap\Form\View\Helper\FormHiddenTwb;
use FileUpload\Form\View\Helper\FormFile;
use TagElement\Form\Element\StatePicker as Tagele;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;

class CheckboxHtmlText extends FormCheckbox {
	
/**
	 * Render a form <input> checkbox element from the provided $element,
	 * 
	 * @param ElementInterface $element        	
	 * @param null|string $formType        	
	 * @param array $displayOptions        	
	 * @return string
	 */
	public function render(ElementInterface $element) {
		$html = parent::render ( $element );
		// Wrap the simple checkbox into label for proper rendering
		$html = '<div class="col-xs-12"><label class="checkbox">'.$html. '<span class="htmltext">'.$element->getHtmlText(). '</span></label></div>';
		return $html;
	}
}