<?php

namespace LocaleElement\Form\View\Helper;

use Zend\Form\View\Helper\FormSelect;



use LocaleElement\Form\Element\CountryPicker as cp;

use DluTwBootstrap\Form\View\Helper\FormHiddenTwb;

use FileUpload\Form\View\Helper\FormFile;

use TagElement\Form\Element\CountryPicker as Tagele;


use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Config\Api\ConfigApi;

class CountryPicker extends FormSelect {
	
	public function render(ElementInterface $element) {
		
		if ($element instanceof cp) {
			$name = $element->getName ();
			if ($name === null || $name === '') {
				throw new Exception\DomainException ( sprintf ( '%s requires that the element has an assigned name; none discovered', __METHOD__ ) );
			}
			
			$attributes = $element->getAttributes ();
			$attributes ['name'] = $name;
			$attributes['id'] = $element->getName();
			$value = $element->getValue();
			$v = ConfigApi::getCountryCodeByCountryName($value);
			if($v){
				$value = $v;
			}
			$attributes ['data-country'] =  $value;
			
			$flags = $element->getCountryFlags();
			if($flags){
				$attributes ['data-flags'] = 'true';
			}
			
			$class = "";
			if(isset($attributes['class'])){
				$class .= $attributes['class'];
			}
			$attributes['class'] = $class. ' form-control';
			$view = $this->getView ();
			$string = $view->render ( 'common/country-picker-element', array (
					'element' => $element,
					'attributeString' => $this->createAttributesString ( $attributes )
					
			) );
			return $string;
		} else {
			return parent::render ( $element );
		}
	}
	
}