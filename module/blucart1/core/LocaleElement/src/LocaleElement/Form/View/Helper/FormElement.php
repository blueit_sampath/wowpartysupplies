<?php

namespace LocaleElement\Form\View\Helper;

use LocaleElement\Form\Element\CheckboxHtmlText;

use LocaleElement\Form\Element\StatePicker;

use TagElement\Form\Element\Tag;

use LocaleElement\Form\Element\CountryPicker;
use Core\Functions;
use File\Form\Element;

use Zend\Form\ElementInterface;

class FormElement extends \TwbBundle\Form\View\Helper\TwbBundleFormElement {
	public function render(ElementInterface $element) {
		$renderer = $this->getView ();
		
		if (! method_exists ( $renderer, 'plugin' )) {
			// Bail early if renderer is not pluggable
			return '';
		}
		
		if (Functions::hasModule ( 'File' )) {
			if ($element instanceof Element\FileUpload) {
				$helper = $renderer->plugin ( 'form_file_upload' );
				return $helper ( $element );
			}
		}
		
		if (Functions::hasModule ( 'TagElement' )) {
			if ($element instanceof Tag) {
				$helper = $renderer->plugin ( 'form_tag' );
				return $helper ( $element );
			}
		}
		
		if ($element instanceof CountryPicker) {
			$helper = $renderer->plugin ( 'form_country_picker' );
			return $helper ( $element );
		}
		
		if ($element instanceof StatePicker) {
			$helper = $renderer->plugin ( 'form_state_picker' );
			return $helper ( $element );
		}
		
		if ($element instanceof CheckboxHtmlText) {
			$helper = $renderer->plugin ( 'form_checkbox_html_text' );
			return $helper ( $element );
		}
                
                if ($element instanceof \LocaleElement\Form\Element\AddressPicker) {
			$helper = $renderer->plugin ( 'form_address_picker' );
			return $helper ( $element );
		}
		
		return parent::render ( $element );
	}
}

