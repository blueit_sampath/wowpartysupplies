<?php

namespace LocaleElement;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

class Module implements AutoloaderProviderInterface {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
       
       $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
            $this,
            'attachAssets'
        ));

        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();
        $viewHelperPluginManager = $serviceManager->get('view_helper_manager');
        $viewHelperPluginManager->setInvokableClass('formelement', 'LocaleElement\Form\View\Helper\FormElement');
        $viewHelperPluginManager->setInvokableClass('formCountryPicker', 'LocaleElement\Form\View\Helper\CountryPicker');
        $viewHelperPluginManager->setInvokableClass('formStatePicker', 'LocaleElement\Form\View\Helper\StatePicker');
        $viewHelperPluginManager->setInvokableClass('formCheckboxHtmlText', 'LocaleElement\Form\View\Helper\CheckboxHtmlText');
        $viewHelperPluginManager->setInvokableClass('formAddressPicker', 'LocaleElement\Form\View\Helper\AddressPicker');
    }

    public function attachAssets($e) {
        $controller = $e->getTarget();

        if (!($controller instanceof \Zend\Mvc\Controller\AbstractActionController)) {
            return;
        }
        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();
        $viewHelperPluginManager = $serviceManager->get('view_helper_manager');
        $headScript = $viewHelperPluginManager->get('headScript');
        $headLink = $viewHelperPluginManager->get('headLink');
        $basePath = $viewHelperPluginManager->get('basePath');

        $headScript()->appendFile('//maps.googleapis.com/maps/api/js?sensor=false&libraries=places');
        $headScript()->appendFile($basePath('assets/locale_element/geocomplete/jquery.geocomplete.min.js'));
        $headLink()->appendStylesheet($basePath('assets/locale_element/css/bootstrap-formhelpers.css'));
        $headScript()->appendFile($basePath('assets/locale_element/js/bootstrap-formhelpers.js'));
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}
