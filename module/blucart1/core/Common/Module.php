<?php

namespace Common;

use Core\Functions;

class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
        $app = $e->getApplication();
        $sm = $app->getServiceManager();
        $configuration = $sm->get('Doctrine\ORM\EntityManager')->getConfiguration();
        $configuration->addCustomStringFunction('GroupConcat', 'Common\Doctrine\Functions\GroupConcat');
        $configuration->addCustomDatetimeFunction('Date', 'Common\Doctrine\Functions\Date');
        $configuration->addCustomDatetimeFunction('DateFormat', 'Common\Doctrine\Functions\DateFormat');
        $configuration->addCustomDatetimeFunction('Month', 'Common\Doctrine\Functions\Month');
        $configuration->addCustomDatetimeFunction('Week', 'Common\Doctrine\Functions\Week');
        $configuration->addCustomDatetimeFunction('Year', 'Common\Doctrine\Functions\Year');
    }

}
