<?php

namespace Common\Navigation\Event;

use Zend\Navigation\Navigation;
use Core\Functions;
use Doctrine\ORM\EntityManager;
use Core\Form\Form;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

class NavigationEvent extends AbstractEvent {

    protected $_navigation = null;

    public function getPriority() {
        return null;
    }

    public function getMenuName(){
        return '';
    }
    public function attach(EventManagerInterface $events) {

        $priority = $this->getPriority();

        $sharedEventManager = $events->getSharedManager();
        $menuName = $this->getMenuName();
        $this->_listeners [] = $sharedEventManager->attach('*', $menuName.'Navigation', array(
            $this,
            'preFrontMenu'
                ), $priority);

        $this->_listeners [] = $sharedEventManager->attach('*', 'menu_breadcrumbNavigation', array(
            $this,
            'preFrontBreadcrumb'
                ), $priority);

        $this->_listeners [] = $sharedEventManager->attach('*', 'front-link', array(
            $this,
            'preFrontLink'
                ), $priority);

        $this->_listeners [] = $sharedEventManager->attach('*', 'front-sitemap', array(
            $this,
            'preFrontSitemap'
                ), $priority);

        $this->_listeners [] = $sharedEventManager->attach('*', 'front-account', array(
            $this,
            'preFrontAccount'
                ), $priority);

        return $this;
    }

    public function preFrontMenu($e) {
        $navigation = $e->getParam('navigation');
        $this->setNavigation($navigation);
        return $this->menu();
    }

    public function preFrontBreadcrumb($e) {
        $navigation = $e->getParam('navigation');
        $this->setNavigation($navigation);
        return $this->breadcrumb();
    }

    public function menu() {
        $this->common();
    }

    public function breadcrumb() {
        $this->common();
    }

    public function common() {
        
    }

    public function preFrontSitemap($e) {
        $navigation = $e->getParam('navigation');
        $this->setNavigation($navigation);
        return $this->sitemap();
    }

    public function sitemap() {
        
    }

    public function preFrontLink($e) {
        $navigation = $e->getParam('navigation');
        $this->setNavigation($navigation);
        return $this->link();
    }

    public function link() {
        
    }

    public function preFrontAccount($e) {
        $navigation = $e->getParam('navigation');
        $this->setNavigation($navigation);
        return $this->account();
    }

    public function account() {
        
    }

    /**
     *
     * @return Navigation $_navigation
     */
    public function getNavigation() {
        if (!$this->_navigation) {
            return new Navigation();
        }
        return $this->_navigation;
    }

    /**
     *
     * @param Navigation $_navigation        	
     * @param
     *        	class_name
     */
    public function setNavigation($_navigation) {
        $this->_navigation = $_navigation;
        return $this;
    }

    public function findPagesAndAddMenuByUrl($url, $page = array()) {
        $navigation = $this->getNavigation();
        $routeName = '';
        $routeParams = array();
        if (isset($page['params']) && $page['params']) {
            $routeParams = $page['params'];
        }
        if (isset($page['route']) && $page['route']) {
            $routeName = $page['route'];
            $page['uri'] = $this->getUrl($routeName, $routeParams);
        }
        $pages = $navigation->findBy('uri', $url, true);
        foreach ($pages as $pageresult) {
            $pageresult->addPage($page);
        }
        true;
    }

    public function addDefaultMenu($page = array()) {
        $navigation = $this->getNavigation();
        $routeName = '';
        $routeParams = array();
        if (isset($page['params']) && $page['params']) {
            $routeParams = $page['params'];
        }
        if (isset($page['route']) && $page['route']) {
            $routeName = $page['route'];
            $page['uri'] = $this->getUrl($routeName, $routeParams);
        }
        $page = $navigation->addPage($page);
        return $page;
    }

    public function getUrl($routeName, $routeParams = array(), $options = array()) {
        $urlPlugin = Functions::getUrlPlugin();
        return $urlPlugin($routeName, $routeParams, $options);
    }

}
