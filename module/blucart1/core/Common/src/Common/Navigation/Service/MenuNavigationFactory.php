<?php

namespace Common\Navigation\Service;

use Zend\Navigation\Service\DefaultNavigationFactory;
use Zend\Config;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\RouteStackInterface as Router;
use Zend\Navigation\Exception;
use Zend\Navigation\Navigation;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class MenuNavigationFactory extends DefaultNavigationFactory implements EventManagerAwareInterface {
	protected $_events;
	protected function getName() {
		return 'admin_menu';
	}
	
	public function getEventName(){
		return 'admin-menu';
	}
	
	/**
	 *
	 * @param ServiceLocatorInterface $serviceLocator        	
	 * @return array
	 * @throws \Zend\Navigation\Exception\InvalidArgumentException
	 */
	protected function getPages(ServiceLocatorInterface $serviceLocator) {
		if (null === $this->pages) {
			$configuration = $serviceLocator->get ( 'Config' );
			
			if (! isset ( $configuration ['navigation'] )) {
				throw new Exception\InvalidArgumentException ( 'Could not find navigation configuration key' );
			}
			if (! isset ( $configuration ['navigation'] [$this->getName ()] )) {
				$configuration ['navigation'] [$this->getName ()] = array ();
			}
			
			$navigation = new Navigation ( $configuration ['navigation'] [$this->getName ()] );
			$params ['navigation'] = $navigation;
			
			$this->getEventManager ()->trigger ( $this->getEventName(), $this, $params );
			
			$array = $this->removeEmpty ( $navigation->toArray () );
			
			$pages = $this->getPagesFromConfig ( $array );
			$this->pages = $this->preparePages ( $serviceLocator, $pages );
		}
		return $this->pages;
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (! $this->_events) {
			$this->setEventManager ( new EventManager ( array (
					__CLASS__,
					get_called_class () 
			) ) );
		}
		return $this->_events;
	}
	public function removeEmpty($haystack) {
		foreach ( $haystack as $key => $value ) {
			if (is_array ( $value )) {
				$haystack [$key] = $this->removeEmpty ( $haystack [$key] );
			}
			
			if (empty ( $haystack [$key] )) {
				unset ( $haystack [$key] );
			}
		}
		
		return $haystack;
	}
}
