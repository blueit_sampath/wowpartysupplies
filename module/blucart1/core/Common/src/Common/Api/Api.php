<?php

namespace Common\Api;

use Doctrine\ORM\EntityManager;
use QueryBuilder\Option\QueryBuilder;
use Core\Functions;

class Api {
	
	/**
	 *
	 * @return EntityManager
	 */
	public static function getEntityManager() {
		return Functions::getEntityManager ();
	}
	/**
	 *
	 * @return QueryBuilder
	 */
	public static function getQueryBuilder() {
		$trace = debug_backtrace ();
		$caller1 = array_shift ( $trace );
		$caller2 = array_shift ( $trace );
		$queryBuilder = Functions::getServiceLocator ()->get ( 'QueryBuilder' );
		if (isset ( $caller2 ['class'] )) {
			$queryBuilder->setEventName ( $caller2 ['class'] . '.' . $caller2 ['function'] );
		}
		return $queryBuilder;
	}
}
