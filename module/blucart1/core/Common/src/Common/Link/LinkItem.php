<?php

namespace Common\Link;

use Core\Item\Item;

class LinkItem extends Item {
	protected $_url;
	protected $_title;
	protected $_imageClass;
	protected $_linkClass;
	/**
	 *
	 * @return string $_url
	 */
	public function getUrl() {
		return $this->_url;
	}
	
	/**
	 *
	 * @return string $_title
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 *
	 * @param field_type $_url        	
	 * @param
	 *        	class_name
	 */
	public function setUrl($_url) {
		$this->_url = $_url;
		return $this;
	}
	
	/**
	 *
	 * @param field_type $_title        	
	 * @param
	 *        	class_name
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
		return $this;
	}
	/**
	 * @return string $_imageClass
	 */
	public function getImageClass() {
		return $this->_imageClass;
	}

	/**
	 * @param field_type $_imageClass
	 * @param class_name
	 */
	public function setImageClass($_imageClass) {
		$this->_imageClass = $_imageClass;
		return $this;
	}
	/**
	 * @return string $_linkClass
	 */
	public function getLinkClass() {
		return $this->_linkClass;
	}

	/**
	 * @param field_type $_linkClass
	 * @param class_name
	 */
	public function setLinkClass($_linkClass) {
		$this->_linkClass = $_linkClass;
		return $this;
	}


}
