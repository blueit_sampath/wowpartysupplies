<?php

namespace Common\Link;

use Core\Item\Container\ItemContainer;

class LinkContainer extends ItemContainer {
	
	/**
	 *
	 * @param string $uniqueKey        	
	 * @param string $title        	
	 * @param string $url        	
	 * @param int $weight        	
	 * @return LinkItem
	 */
	public function add($uniqueKey, $title, $url = '', $weight = null,$imageClass='splashy-add',$linkClass = '') {
		$item = new LinkItem ( $uniqueKey );
		$item->setTitle ( $title );
		$item->setUrl ( $url );
		$item->setWeight ( $weight );
		$item->setImageClass($imageClass);
		$item->setLinkClass($linkClass);
		$this->addItem ( $item );
		
		return $item;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return LinkItem
	 */
	public function get($id) {
		return $this->getItem ( $id );
	}
}
