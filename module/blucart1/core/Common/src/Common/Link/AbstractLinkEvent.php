<?php

namespace Common\Link;

use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractLinkEvent extends AbstractEvent {
	protected $_linkContainer = null;
	protected $_event = null;
	abstract public function getEventName();
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$eventName = $this->getEventName ();
		$priority = $this->getPriority ();
		$sharedEventManager = $events->getSharedManager ();
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-link', array (
				$this,
				'formLink' 
		), $priority );
		return $this;
	}
	public function formLink($e) {
		$this->setEvent ( $e );
		$linkContainer = $e->getParam ( 'linkContainer' );
		$this->setLinkContainer ( $linkContainer );
		$this->link ();
	}
	public function link() {
	}
	/**
	 *
	 * @return LinkContainer
	 */
	public function getLinkContainer() {
		return $this->_linkContainer;
	}
	
	/**
	 *
	 * @param LinkContainer $_linkContainer        	
	 *
	 */
	public function setLinkContainer($_linkContainer) {
		$this->_linkContainer = $_linkContainer;
		return $this;
	}
	/**
	 *
	 * @return string $_event
	 */
	public function getEvent() {
		return $this->_event;
	}
	
	/**
	 *
	 * @param NULL $_event        	
	 * 
	 *        
	 */
	public function setEvent($_event) {
		$this->_event = $_event;
		return $this;
	}
}
