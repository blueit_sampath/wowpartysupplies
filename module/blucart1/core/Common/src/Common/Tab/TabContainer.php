<?php

namespace Common\Tab;

use Core\Item\Container\ItemContainer;

class TabContainer extends ItemContainer {
	
	/**
	 *
	 * @param string $uniqueKey        	
	 * @param string $title        	
	 * @param string $url        	
	 * @param int $weight        	
	 * @return TabItem
	 */
	public function add($uniqueKey, $title, $url = '', $weight = null) {
		$item = new TabItem ( $uniqueKey );
		$item->setTitle ( $title );
		$item->setUrl ( $url );
		$item->setWeight ( $weight );
		$this->addItem ( $item );
		
		return $item;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return TabItem
	 */
	public function get($id) {
		return $this->getItem ( $id );
	}
}
