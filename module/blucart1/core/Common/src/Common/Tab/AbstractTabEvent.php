<?php

namespace Common\Tab;

use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractTabEvent extends AbstractEvent {
	protected $_tabContainer = null;
	protected $_event = null;
	abstract public function getEventName();
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$eventName = $this->getEventName ();
		$priority = $this->getPriority ();
		$sharedEventManager = $events->getSharedManager ();
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-tab', array (
				$this,
				'formTab' 
		), $priority );
		return $this;
	}
	public function formTab($e) {
		$this->setEvent ( $e );
		$tabContainer = $e->getParam ( 'tabContainer' );
		$this->setTabContainer ( $tabContainer );
		$this->tab ();
	}
	public function tab() {
	}
	/**
	 *
	 * @return TabContainer
	 */
	public function getTabContainer() {
		return $this->_tabContainer;
	}
	
	/**
	 *
	 * @param TabContainer $_tabContainer        	
	 *
	 */
	public function setTabContainer($_tabContainer) {
		$this->_tabContainer = $_tabContainer;
		return $this;
	}
	/**
	 *
	 * @return string $_event
	 */
	public function getEvent() {
		return $this->_event;
	}
	
	/**
	 *
	 * @param NULL $_event        	
	 * 
	 *        
	 */
	public function setEvent($_event) {
		$this->_event = $_event;
		return $this;
	}
}
