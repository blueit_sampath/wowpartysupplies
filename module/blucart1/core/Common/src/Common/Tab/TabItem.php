<?php

namespace Common\Tab;

use Core\Item\Item;

class TabItem extends Item {
	protected $_url;
	protected $_title;
	/**
	 *
	 * @return string $_url
	 */
	public function getUrl() {
		return $this->_url;
	}
	
	/**
	 *
	 * @return string $_title
	 */
	public function getTitle() {
		return $this->_title;
	}
	
	/**
	 *
	 * @param field_type $_url        	
	 * @param
	 *        	class_name
	 */
	public function setUrl($_url) {
		$this->_url = $_url;
		return $this;
	}
	
	/**
	 *
	 * @param field_type $_title        	
	 * @param
	 *        	class_name
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
		return $this;
	}
}
