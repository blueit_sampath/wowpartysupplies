<?php

namespace Common\Form;

use Config\Api\ConfigApi;
use Zend\Form\Element\Radio;
use Zend\Form\Element\Select;
use Core\Functions;
use Common\Form\Option\FormValidationContainer;
use Common\Form\Option\FormResultContainer;
use Zend\Captcha\Factory;
use Zend\Form\Element\Captcha;
use Zend\Form\Form as ZFForm;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Form\FormInterface;
use Zend\Captcha\Image as CaptchaImage;

class Form extends ZFForm implements EventManagerAwareInterface {

    protected $_events;
    protected $_formData;

    public function setRawFormData($data) {
        $this->_formData = $data;
        return $this;
    }

    public function getRawFormData() {
        return $this->_formData;
    }

    public function initPreEvent() {
        return $this->getEventManager()->trigger($this->getName() . '-init-form.pre', $this);
    }

    public function initPostEvent() {
        return $this->getEventManager()->trigger($this->getName() . '-init-form.post', $this);
    }

    public function triggerAttachFormElements() {
        return $this->getEventManager()->trigger($this->getName() . '-trigger-attach-form-elements', $this);
    }

    public function getData($flag = FormInterface::VALUES_NORMALIZED) {
        $this->getEventManager()->trigger($this->getName() . '-getData-form.pre', $this);
        $result = parent::getData($flag);
        $this->getEventManager()->trigger($this->getName() . '-getData-form.post', $this);
        return $result;
    }

    public function setData($data) {
        $this->setRawFormData($data);
        $this->getEventManager()->trigger($this->getName() . '-setData-form.pre', $this);
        $result = parent::setData($data);
        $this->getEventManager()->trigger($this->getName() . '-setData-form.post', $this);
        return $result;
    }

    public function isValid() {
        $formValidationContainer = new FormValidationContainer();
        $params = array(
            'formValidationContainer' => $formValidationContainer
        );
        $this->getEventManager()->trigger($this->getName() . '-isValid-form.pre', $this, $params);
        $result = parent::isValid();
        $formValidationContainer->setFormValid($result);
        $this->getEventManager()->trigger($this->getName() . '-isValid-form.post', $this, $params);

        $this->injectMessages($formValidationContainer);

        return ($formValidationContainer->isValid());
    }

    protected function injectMessages(FormValidationContainer $formValidationContainer) {
        $items = $formValidationContainer->getItems();
        foreach ($items as $item) {
            if ($item->getMessage()) {
                Functions::addMessage($item->getMessage(), $item->getMessageType());
            }
        }
        return $this;
    }

    protected function getCaptchaOptions() {
        $config = $this->getServiceLocator()->get('Config');
        if (isset($config['captcha'])) {
            return Factory::factory($config['captcha']);
        }
        return false;
    }

    public function save() {
        $formResultContainer = new FormResultContainer();
        $params = array(
            'formResultContainer' => $formResultContainer
        );
        return $this->getEventManager()->trigger($this->getName() . '-save', $this, $params);
    }

    public function getRecord($params = array()) {
        return $this->getEventManager()->trigger($this->getName() . '-getRecord', $this, $params);
    }

    public function getStatus($priority, $name = 'status', $label = 'Status') {
        $radio = $this->add(array(
            'name' => $name,
            'type' => 'radio',
            'options' => array(
                'value_options' =>
                array(
                    array('label' => 'Enable', 'value' => 1, 'options' => array('label_attributes' => array('class' => 'col-sm-4'))),
                    array('label' => 'Disable', 'value' => 0, 'options' => array('label_attributes' => array('class' => 'col-sm-4')))
                ),
                'label' => $label,
            )), array(
            'priority' => $priority
        ));
        return $radio;
    }

    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    public function getEventManager() {
        if (!$this->_events) {
            $this->setEventManager(new EventManager(array(
                __CLASS__,
                get_called_class()
            )));
        }
        return $this->_events;
    }

    public function addCaptcha($priority = 100, $label = "Please verify you are human") {
        $dirdata = './data';
        $captchaImage = new CaptchaImage(array(
            'font' => $dirdata . '/fonts/arial.ttf',
            'width' => 250,
            'height' => 100,
            'dotNoiseLevel' => 70,
            'lineNoiseLevel' => 10
        ));
        $url = Functions::getUrlPlugin();
        $captchaImage->setImgDir($dirdata . '/captcha');
        $captchaImage->setImgUrl($url('captcha') . '/');

// add captcha element...
        $this->add(array(
            'type' => 'Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => array(
                'label' => $label,
                'captcha' => $captchaImage
            )
                ), array(
            'priority' => $priority
        ));
    }

    public function addCsrf($priority = 50) {
        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
        ));
    }

    public function hasElements($name) {
        if ($this->has($name) && count($this->get($name)->getElements())) {
            return true;
        }
        return false;
    }

    public function prepare() {
        $this->getEventManager()->trigger($this->getName() . '-prepare-form.pre', $this);
        $result = parent::prepare();
//$this->attachAttributesToElements($this);

        $this->getEventManager()->trigger($this->getName() . '-prepare-form.post', $this);
        return $result;
    }

    public function attachAttributesToElements($form = null) {
        foreach ($form->getElements() as $name => $element) {
            $optionE = $element->getOptions();
            $attributes = $element->getAttributes();
            $optionE['column-size'] = 'sm-5';
            $optionE['label_attributes'] = array('class' => 'col-sm-1');
            $element->setAttributes($attributes);
            $element->setOptions($optionE);
        }

        foreach ($form->getFieldsets() as $fieldset) {
            $this->attachAttributesToElements($fieldset);
        }
        return $this;
    }

    public function getSubmitButton($priority = "-1000", $label = "Submit") {
        return $this->add(array(
                    'name' => 'btnsubmit',
                    'attributes' => array(
                        'type' => 'submit',
                        'value' => 'Submit',
                    ),
                        ), array(
                    'priority' => $priority
        ));
    }

}
