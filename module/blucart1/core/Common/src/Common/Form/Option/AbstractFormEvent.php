<?php

namespace Common\Form\Option;

use Core\Functions;

use Doctrine\ORM\EntityManager;
use Core\Form\Form;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractFormEvent extends AbstractEvent {
	protected $_form = null;
	protected $_formValidateContainer = null;
	protected $_formResultContainer = null;
	protected $_event = null;
	abstract public function getFormName();
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$formName = $this->getFormName ();
		$priority = $this->getPriority ();
		
		$sharedEventManager = $events->getSharedManager ();
		$this->_listeners [] = $sharedEventManager->attach ( '*', $formName . '-init-form.pre', array (
				$this,
				'formPreInitEvent' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $formName . '-init-form.post', array (
				$this,
				'formPostInitEvent' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $formName . '-setData-form.pre', array (
				$this,
				'formPreSetDataEvent' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $formName . '-setData-form.post', array (
				$this,
				'formPostSetDataEvent' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $formName . '-isValid-form.pre', array (
				$this,
				'formPreIsValidEvent' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $formName . '-isValid-form.post', array (
				$this,
				'formPostIsValidEvent' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $formName . '-save', array (
				$this,
				'formSave' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $formName . '-getRecord', array (
				$this,
				'formGetRecord'
		), $priority );
		
		return $this;
	}
	public function formPreInitEvent($e) {
		$form = $e->getTarget ();
		$this->setForm ( $form );
		$this->setEvent ( $e );
		$this->preInitEvent ();
	}
	public function preInitEvent() {
	}
	public function formPostInitEvent($e) {
		$form = $e->getTarget ();
		$this->setForm ( $form );
		$this->setEvent ( $e );
		$this->postInitEvent ();
	}
	public function postInitEvent() {
	}
	public function formPreSetDataEvent($e) {
		$form = $e->getTarget ();
		$this->setForm ( $form );
		$this->setEvent ( $e );
		$this->preSetDataEvent ();
	}
	public function preSetDataEvent() {
	}
	public function formPostSetDataEvent($e) {
		$form = $e->getTarget ();
		$this->setForm ( $form );
		$this->setEvent ( $e );
		$this->postSetDataEvent ();
	}
	public function postSetDataEvent() {
	}
	public function formPreIsValidEvent($e) {
		$form = $e->getTarget ();
		$this->setForm ( $form );
		$this->setEvent ( $e );
		$formValidateContainer = $e->getParam ( 'formValidateContainer' );
		$this->setFormValidateContainer ( $formValidateContainer );
		$this->preIsValidEvent ();
	}
	public function preIsValidEvent() {
	}
	public function formPostIsValidEvent($e) {
		$form = $e->getTarget ();
		$this->setForm ( $form );
		$this->setEvent ( $e );
		$formValidateContainer = $e->getParam ( 'formValidateContainer' );
		$this->setFormValidateContainer ( $formValidateContainer );
		$this->postIsValidEvent ();
	}
	public function postIsValidEvent() {
	}
	public function formSave($e) {
		$form = $e->getTarget ();
		$this->setForm ( $form );
		$this->setEvent ( $e );
		$formResultContainer = $e->getParam ( 'formResultContainer' );
		$this->setFormResultContainer($formResultContainer);
		$this->save ();
	}
	public function save() {
	}
	
	
	public function formGetRecord($e) {
		$form = $e->getTarget ();
		$this->setForm ( $form );
		$this->setEvent ( $e );
		
		$this->getRecord ();
	}
	public function getRecord() {
	}
	
	/**
	 *
	 * @return the $_event
	 */
	public function getEvent() {
		return $this->_event;
	}
	
	/**
	 *
	 * @param field_type $_event        	
	 */
	public function setEvent($_event) {
		$this->_event = $_event;
	}
	/**
	 *
	 * @param NULL $_form        	
	 */
	public function setForm($_form) {
		$this->_form = $_form;
	}
	
	/**
	 *
	 * @return Form $_form
	 */
	public function getForm() {
		return $this->_form;
	}
	
	/**
	 *
	 * @return FormValidateContainer $_formValidateContainer
	 */
	public function getFormValidateContainer() {
		return $this->_formValidateContainer;
	}
	
	/**
	 *
	 * @param NULL $_formValidateContainer        	
	 */
	public function setFormValidateContainer($_formValidateContainer) {
		$this->_formValidateContainer = $_formValidateContainer;
		return $this;
	}
	
	/**
	 *
	 * @return FormResultContainer $_formResultContainer
	 */
	public function getFormResultContainer() {
		return $this->_formResultContainer;
	}
	
	/**
	 *
	 * @param NULL $_formValidateContainer
	 */
	public function setFormResultContainer($_formResultContainer) {
		$this->_formResultContainer = $_formResultContainer;
		return $this;
	}
	
	public function getEntityManager(){
		return Functions::getEntityManager();
	}
	
}
