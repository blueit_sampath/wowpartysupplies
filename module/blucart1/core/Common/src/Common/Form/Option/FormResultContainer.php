<?php

namespace Common\Form\Option;

use Core\Item\Container\ItemContainer;


class FormResultContainer extends ItemContainer {
	

	/**
	 * @param string $uniqueKey
	 * @param string $message
	 * @return \Common\Form\Option\FormResultItem
	 */
	public function add($uniqueKey, $value) {
		$item = new FormResultItem($uniqueKey);
		$item->setValue($value);
		$this->addItem($item);
		return $item;
	}
	
	
	/**
	 * @param string $id
	 * @return \Common\Form\Option\FormResultItem
	 */
	public function get($id) {
		return $this->getItem($id);
	}
	
	
}
