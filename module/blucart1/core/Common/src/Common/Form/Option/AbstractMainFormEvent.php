<?php

namespace Common\Form\Option;

use Core\Functions;
use Doctrine\ORM\EntityManager;
use Common\Form\Form;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractMainFormEvent extends AbstractEvent {

    protected $_form = null;
    protected $_formValidateContainer = null;
    protected $_formResultContainer = null;
    protected $_event = null;
    protected $_entityName = '';

    abstract public function getFormName();

    public function getPriority() {
        return null;
    }

    public function attach(EventManagerInterface $events) {
        $formName = $this->getFormName();
        $priority = $this->getPriority();

        $sharedEventManager = $events->getSharedManager();
        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-init-form.pre', array(
            $this,
            'formPreInitEvent'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-init-form.post', array(
            $this,
            'formPostInitEvent'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-setData-form.pre', array(
            $this,
            'formPreSetDataEvent'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-setData-form.post', array(
            $this,
            'formPostSetDataEvent'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-isValid-form.pre', array(
            $this,
            'formPreIsValidEvent'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-isValid-form.post', array(
            $this,
            'formPostIsValidEvent'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-save', array(
            $this,
            'formSave'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-getRecord', array(
            $this,
            'formGetRecord'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-prepare-form.pre', array(
            $this,
            'formPrePrepare'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-prepare-form.post', array(
            $this,
            'formPostPrepare'
                ), $priority);

        $this->_listeners[] = $sharedEventManager->attach('*', $formName . '-trigger-attach-form-elements', array(
            $this,
            'formAttachFormElements'
                ), $priority);
        return $this;
    }

    public function formPreInitEvent($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $this->preInitEvent();
    }

    public function preInitEvent() {
        
    }

    public function formAttachFormElements($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $this->attachFormElements();
    }

    public function attachFormElements() {
        
    }

    public function formPostInitEvent($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $this->postInitEvent();
    }

    public function postInitEvent() {
        
    }

    public function formPreSetDataEvent($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $this->preSetDataEvent();
    }

    public function preSetDataEvent() {
        
    }

    public function formPostSetDataEvent($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $this->postSetDataEvent();
    }

    public function postSetDataEvent() {
        
    }

    public function formPreIsValidEvent($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $formValidateContainer = $e->getParam('formValidateContainer');
        $this->setFormValidateContainer($formValidateContainer);
        $this->preIsValidEvent();
    }

    public function preIsValidEvent() {
        
    }

    public function formPostIsValidEvent($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $formValidateContainer = $e->getParam('formValidateContainer');
        $this->setFormValidateContainer($formValidateContainer);
        $this->postIsValidEvent();
    }

    public function postIsValidEvent() {
        
    }

    public function formSave($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $formResultContainer = $e->getParam('formResultContainer');
        $this->setFormResultContainer($formResultContainer);
        $this->save();
    }

    public function formGetRecord($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);

        $this->getRecord();
    }

    public function formPrePrepare($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);
        $this->prePrepareEvent();
    }

    public function prePrepareEvent() {
        
    }

    public function formPostPrepare($e) {
        $form = $e->getTarget();
        $this->setForm($form);
        $this->setEvent($e);

        $this->postPrepareEvent();
    }

    public function postPrepareEvent() {
        
    }

    /**
     *
     * @return the $_event
     */
    public function getEvent() {
        return $this->_event;
    }

    /**
     *
     * @param field_type $_event            
     */
    public function setEvent($_event) {
        $this->_event = $_event;
    }

    /**
     *
     * @param NULL $_form            
     */
    public function setForm($_form) {
        $this->_form = $_form;
    }

    /**
     *
     * @return Form $_form
     */
    public function getForm() {
        return $this->_form;
    }

    /**
     *
     * @return FormValidateContainer $_formValidateContainer
     */
    public function getFormValidateContainer() {
        return $this->_formValidateContainer;
    }

    /**
     *
     * @param NULL $_formValidateContainer            
     */
    public function setFormValidateContainer($_formValidateContainer) {
        $this->_formValidateContainer = $_formValidateContainer;
        return $this;
    }

    /**
     *
     * @return FormResultContainer $_formResultContainer
     */
    public function getFormResultContainer() {
        return $this->_formResultContainer;
    }

    /**
     *
     * @param NULL $_formValidateContainer            
     */
    public function setFormResultContainer($_formResultContainer) {
        $this->_formResultContainer = $_formResultContainer;
        return $this;
    }

    public function getEntityManager() {
        return Functions::getEntityManager();
    }

    /*
     * Save Begin
     */

    public function parseSaveParams($params) {
        return $params;
    }

    public function parseSaveEntity($params, $entity) {
        return $entity;
    }

    public function afterSaveEntity($entity) {
        $form = $this->getForm();
        if ($form->has('id')) {
            $form->get('id')->setValue($entity->id);
        }
    }

    public function save() {
        $form = $this->getForm();
        $params = $form->getData();
        $params = $this->parseSaveParams($params);
        $em = $this->getEntityManager();
        if ($this->_entityName) {
            if (isset($params['id']) && $params['id']) {
                $entity = $em->find($this->_entity, $params['id']);
            } else {
                $entity = new $this->_entity();
            }
            foreach ($this->_columnKeys as $key => $columnKey) {
                if (is_array($columnKey)) {
                    if (isset($params[$key])) {
                        $value = $params[$key];
//                        if (isset($columnKey['type'])) {
//                            $value = $this->formatValue($value, $columnKey['type']);
//                        }

                        $entity->$key = $value;
                    }
                } else {
                    if (isset($params[$columnKey])) {

                        $entity->$columnKey = $params[$columnKey];
                    }
                }
            }
            $this->parseSaveEntity($params, $entity);
            $em->persist($entity);
            $em->flush();
            $this->afterSaveEntity($entity);
            $resultContainer = $this->getFormResultContainer();
            $resultContainer->add($this->_entityName, $entity);
        }
        return true;
    }

    public function formatValue($value, $type) {
        if ($type == 'float') {
            return number_format($value, 2, '.', '');
        }
        return $value;
    }

    /*
     * Save End
     */
    /*
     * getRecord Begin
     */

    public function beforeGetRecord() {
        
    }

    public function getId() {
        return Functions::fromRoute($this->_entityName . 'Id', 0);
    }

    public function getRecord() {
        $this->beforeGetRecord();
        $id = $this->getId();
        if (!$id) {
            return;
        }
        $form = $this->getForm();
        $em = $this->getEntityManager();

        $entity = $em->find($this->_entity, $id);
        if (!$entity) {
            return;
        }

        foreach ($this->_columnKeys as $key => $columnKey) {

            if (is_array($columnKey)) {
                if ($form->has($key)) {
                    $element = $form->get($key);
                    $value = $entity->$key;
                    $value = $this->formatValue($value, $columnKey['type']);
                    $element->setValue($value);
                }
            } else {
                if ($form->has($columnKey)) {
                    $element = $form->get($columnKey);
                    $element->setValue($entity->$columnKey);
                }
            }
        }
        $this->afterGetRecord($entity);
        return true;
    }

    public function afterGetRecord($entity) {
        $form = $this->getForm();
        try {
            
        } catch (\Exception $e) {
            
        }
    }

    /*
     * getRecordEnd
     */
}
