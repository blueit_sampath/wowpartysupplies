<?php

namespace Common\Form\Option;

use Core\Item\Item;

class FormResultItem extends Item {
	protected $_value;
	/**
	 *
	 * @return string $_value
	 */
	public function getValue() {
		return $this->_value;
	}
	
	/**
	 *
	 * @param field_type $_value
	 * @return FormResultItem   	
	 */
	public function setValue($_value) {
		$this->_value = $_value;
		return $this;
	}
}
