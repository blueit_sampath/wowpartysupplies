<?php

namespace Common\Form\Option;

use Core\Functions;
use Doctrine\ORM\EntityManager;
use Common\Form\Form;
use Zend\EventManager\SharedEventManager;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractFrontMainFormEvent extends AbstractMainFormEvent {
	
	/*
	 * getRecordEnd
	 */
	
	public function afterSaveEntity($entity) {
		
	}
}
