<?php

namespace Common\Form\Option;

use Core\Item\Item;

class FormValidationItem extends Item {
	
	
	protected $_message = '';
	
	protected $_messageType = 'error';
	
	protected $_validState = false;
	
	
	/**
	 * @return the $_messageType
	 */
	public function getMessageType() {
		return $this->_messageType;
	}

	/**
	 * @param string $_messageType
	 */
	public function setMessageType($_messageType) {
		$this->_messageType = $_messageType;
	}

	/**
	 * @param boolean $validState
	 * @return \Common\Form\Option\FormValidationItem
	 */
	public function setValidState($validState){
		$this->_validState = $validState;
		return $this;
	}
	
	/**
	 * @return boolean
	 */
	public function getValidState(){
		return $this->_validState;
		
	}
	/**
	 * @param string $message
	 * @param string $messageType
	 * @return \Common\Form\Option\FormValidationItem
	 */
	public function setMessage($message) {
		
		$this->_message = $message;
		return $this;
	}
	/**
	 * @return multitype:string 
	 */
	public function getMessage() {
	return $this->_message;
	}
}
