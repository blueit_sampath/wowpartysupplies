<?php

namespace Common\Form\Option;

use Common\Form\Form;
use Traversable;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;

abstract class AbstractFormFactory implements FactoryInterface {
	protected $_serviceLocator;
	
	/**
	 *
	 * @return Form
	 */
	abstract public function getForm();
	abstract public function getFormFilter();
	public function createService(ServiceLocatorInterface $services) {
		$this->setServiceLocator ( $services );
		$filter = $this->getFormFilter ();
		$form = $this->getForm ();
		$form->initPreEvent ();
		$form->init ();
		if ($filter) {
			$form->setInputFilter ( $filter );
		}
		
		$form->initPostEvent ();
		return $form;
	}
	
	/**
	 *
	 * @return the $_serviceLocator
	 */
	public function getServiceLocator() {
		return $this->_serviceLocator;
	}
	
	/**
	 *
	 * @param field_type $_serviceLocator        	
	 */
	public function setServiceLocator($_serviceLocator) {
		$this->_serviceLocator = $_serviceLocator;
	}
}
