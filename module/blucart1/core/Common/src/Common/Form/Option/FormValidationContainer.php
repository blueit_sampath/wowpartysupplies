<?php

namespace Common\Form\Option;

use Core\Item\Container\ItemContainer;


class FormValidationContainer extends ItemContainer {
	
	const ERROR = 'error';
	const SUCCESS = 'success';
	const WARNING = 'warning';
	const INFO = 'info';
	
	protected $_formValid =  false;
	
	/**
	 * @return formValid $_formValid
	 */
	public function getFormValid() {
		return $this->_formValid;
	}

	/**
	 * @param boolean $_formValid
	 */
	public function setFormValid($_formValid) {
		$this->_formValid = $_formValid;
	}

	/**
	 * @param string $uniqueKey
	 * @param string $message
	 * @param string $errorType
	 * @param integer $weight
	 * @return \Common\Form\Option\FormValidationItem
	 */
	public function add($uniqueKey, $message, $messageType = self::ERROR, $weight = null, $validState = false) {
		$item = new FormValidationItem($uniqueKey);
		
		$item->setMessageType($messageType);
		$item->setMessage($message);
		$item->setWeight($weight);
		$item->setValidState($validState);
		$this->addItem($item);
		return $item;
	}
	
	
	/**
	 * @param string $id
	 * @return \Common\Form\Option\FormValidationItem
	 */
	public function get($id) {
		return $this->getItem($id);
	}
	
	
	/**
	 * @return boolean
	 */
	public function isValid(){
		$items = $this->getItems();
		$result = true;
		foreach($items as $item){
			$result = ($result && $item->getValidState());
		}
		return ($this->getFormValid() && $result);
	}
	
}
