<?php

namespace Common\Form;

use Config\Api\ConfigApi;
use Zend\Form\Element\Radio;
use Zend\Form\Element\Select;
use Core\Functions;
use Common\Form\Option\FormValidationContainer;
use Common\Form\Option\FormResultContainer;
use Zend\Captcha\Factory;
use Zend\Form\Element\Captcha;
use Zend\Form\Form as ZFForm;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\Form\FormInterface;
use Zend\Captcha\Image as CaptchaImage;

class Form extends ZFForm implements EventManagerAwareInterface {
	protected $_events;
	protected $_formData;
	public function setRawFormData($data) {
		$this->_formData = $data;
		return $this;
	}
	public function getRawFormData() {
		return $this->_formData;
	}
	public function initPreEvent() {
		return $this->getEventManager ()->trigger ( $this->getName () . '-init-form.pre', $this );
	}
	public function initPostEvent() {
		return $this->getEventManager ()->trigger ( $this->getName () . '-init-form.post', $this );
	}
	public function getData($flag = FormInterface::VALUES_AS_ARRAY) {
		$this->getEventManager ()->trigger ( $this->getName () . '-getData-form.pre', $this );
		$result = parent::getData ( $flag );
		$this->getEventManager ()->trigger ( $this->getName () . '-getData-form.post', $this );
		return $result;
	}
	public function setData($data) {
		$this->setRawFormData ( $data );
		$this->getEventManager ()->trigger ( $this->getName () . '-setData-form.pre', $this );
		$result = parent::setData ( $data );
		$this->getEventManager ()->trigger ( $this->getName () . '-setData-form.post', $this );
		return $result;
	}
	public function isValid() {
		$formValidationContainer = new FormValidationContainer ();
		$params = array (
				'formValidationContainer' => $formValidationContainer 
		);
		$this->getEventManager ()->trigger ( $this->getName () . '-isValid-form.pre', $this, $params );
		$result = parent::isValid ();
		$formValidationContainer->setFormValid ( $result );
		$this->getEventManager ()->trigger ( $this->getName () . '-isValid-form.post', $this, $params );
		
		$this->injectMessages ( $formValidationContainer );
		
		return ($formValidationContainer->isValid ());
	}
	protected function injectMessages(FormValidationContainer $formValidationContainer) {
		$items = $formValidationContainer->getItems ();
		foreach ( $items as $item ) {
			if ($item->getMessage ()) {
				Functions::addMessage ( $item->getMessage (), $item->getMessageType () );
			}
		}
		return $this;
	}
	protected function getCaptcha($name = 'captcha') {
		$captchaOptions = $this->getCaptchaOptions ();
		if ($captchaOptions) {
			$captcha = new Captcha ( $name );
			$captcha->setCaptcha ( $captchaOptions );
			$captcha->setOptions ( array (
					'label' => 'Please verify you are human.' 
			) );
		}
		
		$dirdata = './data';
		
		//pass captcha image options
		$captchaImage = new CaptchaImage(  array(
				'font' => $dirdata . '/fonts/arial.ttf',
				'width' => 250,
				'height' => 100,
				'dotNoiseLevel' => 40,
				'lineNoiseLevel' => 3)
		);
		$captchaImage->setImgDir($dirdata.'/captcha');
		$captchaImage->setImgUrl($urlcaptcha);
		
		
		//add captcha element...
		$this->add(array(
				'type' => 'Zend\Form\Element\Captcha',
				'name' => 'captcha',
				'options' => array(
						'label' => 'Please verify you are human',
						'captcha' => $captchaImage,
				),
		));
		
		
		return $captchaImage;
	}
	protected function getCaptchaOptions() {
		$config = $this->getServiceLocator ()->get ( 'Config' );
		if (isset ( $config ['captcha'] )) {
			return Factory::factory ( $config ['captcha'] );
		}
		return false;
	}
	public function save() {
		$formResultContainer = new FormResultContainer ();
		$params = array (
				'formResultContainer' => $formResultContainer 
		);
		return $this->getEventManager ()->trigger ( $this->getName () . '-save', $this, $params );
	}
	public function getRecord($params = array()) {
		return $this->getEventManager ()->trigger ( $this->getName () . '-getRecord', $this, $params );
	}
	public function getStatus($priority, $name = 'status', $label = 'Status') {
		$radio = new Radio ( $name );
		$radio->setLabel ( $label );
		$radio->setValueOptions ( array (
				'1' => 'Enable',
				'0' => 'Disable' 
		) );
		
		$this->add ( $radio, array (
				'priority' => $priority 
		) );
		return $radio;
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (! $this->_events) {
			$this->setEventManager ( new EventManager ( array (
					__CLASS__,
					get_called_class () 
			) ) );
		}
		return $this->_events;
	}
	public function addCaptcha($priority = 100, $label = "Please verify you are human") {
// 		$publicKey = ConfigApi::getConfigByKey ( 'CAPTCHA_PUBLIC_KEY' );
// 		$privateKey = ConfigApi::getConfigByKey ( 'CAPTCHA_PRIVATE_KEY' );
// 		$enable = ConfigApi::getConfigByKey ( 'CAPTCHA_ENABLE' );
// 		if ($enable && $publicKey && $privateKey) {
// 			$captcha = new Captcha ( $this->getName().'captcha' );
// 			$captcha->setCaptcha ( Factory::factory ( array (
// 					'class' => 'recaptcha',
// 					'options' => array (
// 							'pubkey' => $publicKey,
// 							'privkey' => $privateKey,
// 							'theme' => 'clean'
// 					) 
// 			) ) );
// 			$captcha->setOptions ( array (
// 					'label' => $label 
// 			) );
// 			$this->add ( $captcha, array (
// 					'priority' => $priority 
// 			) );
// 			return $captcha;
// 		}

		
		
		$dirdata = './data';
		
		//pass captcha image options
		$captchaImage = new CaptchaImage(  array(
				'font' => $dirdata . '/fonts/arial.ttf',
				'width' => 250,
				'height' => 100,
				'dotNoiseLevel' => 40,
				'lineNoiseLevel' => 3)
		);
		$url = Functions::getUrlPlugin();
		$captchaImage->setImgDir($dirdata.'/captcha');
		$captchaImage->setImgUrl($url('captcha').'/');
		
		
		//add captcha element...
		$this->add(array(
				'type' => 'Zend\Form\Element\Captcha',
				'name' => 'captcha',
				'options' => array(
						'label' => $label,
						'captcha' => $captchaImage,
				),
		), array('priotity' => $priority));
		
	}
	
	public function addCsrf($priority = 50){
		$this->add ( array (
				'type' => 'Zend\Form\Element\Csrf',
				'name' => 'csrf'
		) );
	}
}
