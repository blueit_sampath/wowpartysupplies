<?php

namespace Common\Form\View\Helper;

class TwbBundleFormMultiCheckbox extends \Zend\Form\View\Helper\FormMultiCheckbox {

    /**
     * @see \Zend\Form\View\Helper\FormMultiCheckbox::render()
     * @param \Zend\Form\ElementInterface $oElement
     * @return string
     */
    public function render(\Zend\Form\ElementInterface $oElement) {

        $aElementOptions = $oElement->getOptions();
        $sCheckboxClass = isset($aElementOptions['inline']) && $aElementOptions['inline'] == true ? 'checkbox-inline' : 'checkbox';

        //$aLabelAttributes = $oElement->getLabelAttributes();
        $aLabelAttributes = array();
        if (empty($aLabelAttributes['class']))
            $aLabelAttributes['class'] = $sCheckboxClass;
        elseif (!preg_match('/(\s|^)' . $sCheckboxClass . '(\s|$)/', $aLabelAttributes['class']))
            $aLabelAttributes['class'] .= ' ' . $sCheckboxClass;
        $oElement->setLabelAttributes($aLabelAttributes);

        return parent::render($oElement);
    }

}
