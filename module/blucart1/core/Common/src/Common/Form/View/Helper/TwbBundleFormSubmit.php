<?php
namespace Common\Form\View\Helper;
class TwbBundleFormSubmit extends \TwbBundle\Form\View\Helper\TwbBundleFormButton
{
    

    /**
     * @see \Zend\Form\View\Helper\FormButton::render()
     * @param \Zend\Form\ElementInterface $oElement
     * @param string $sButtonContent
     * @throws \LogicException
     * @throws \Exception
     * @return string
     */
    public function render(\Zend\Form\ElementInterface $oElement, $sButtonContent = null)
    {
        $oElement->setLabel($oElement->getValue());
        return parent::render($oElement, $sButtonContent);
    }
}