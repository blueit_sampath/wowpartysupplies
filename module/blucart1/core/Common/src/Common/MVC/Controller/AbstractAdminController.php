<?php

namespace Common\MVC\Controller;

use Zend\View\Model\ViewModel;
use Core\Functions;
use Zend\Mvc\Controller\AbstractActionController;

class AbstractAdminController extends AbstractActionController {
	public function getFormFactory() {
	}
	public function addSuccessMessage() {
		Functions::addSuccessMessage ( 'Saved Successfully' );
	}
	public function getSaveRedirector() {
	}
	public function afterSave() {
	}
	public function notValid() {
	}
	public function indexAction() {
	}
	public function addAction() {
		$form = $this->getFormFactory ();
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$this->afterSave ();
				$this->addSuccessMessage ();
				return $this->getSaveRedirector ();
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel( array (
				'form' => $form 
		));
		
		return $viewModel;
	}
	public function sortAction() {
	}
	public function viewAction() {
		return array();
	}
}
