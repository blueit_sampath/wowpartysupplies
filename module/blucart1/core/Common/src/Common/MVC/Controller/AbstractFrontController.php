<?php

namespace Common\MVC\Controller;

use Zend\View\Model\ViewModel;
use Core\Functions;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Response;
use User\Controller\UserLoginController;
use User\Api\UserApi;
use Zend\Form\Fieldset;

class AbstractFrontController extends AbstractActionController {
	public function getFormFactory() {
	}
	public function addSuccessMessage() {
		Functions::addSuccessMessage ( 'Saved Successfully' );
	}
	public function getSaveRedirector() {
	}
	public function afterSave() {
	}
	public function notValid() {
	}
	public function indexAction() {
	}
	public function addAction() {
		$form = $this->getFormFactory ();
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				$this->afterSave ();
				$this->addSuccessMessage ();
				return $this->getSaveRedirector ();
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
	public function accessDenied() {
		$response = $this->getResponse ();
		$urlPlugin = Functions::getUrlPlugin ();
		
		$string = rawurlencode ( $this->getRequest ()->getUri () );
		if (! UserApi::getLoggedInUser ()) {
			Functions::addErrorMessage('Access Denied');
			header ( 'HTTP/1.1 401 Unauthorized' );
			header ( 'Location:' . $urlPlugin ( 'user-login' ) . '?redirectUrl=' . $string );
			exit ();
		}
		
		header ( 'HTTP/1.1 401 Unauthorized' );
		header ( 'Location:' . $urlPlugin ( 'access-denied' ) );
		exit ();
	}
	public function addFormErrorMessages($form) {
		$message = $form->getMessages ();
	
		$topMessage = array ();
		foreach ( $message as $key => $m ) {
			$element = $form->get ( $key );
			if ($element instanceof Fieldset) {
				return $this->addFormErrorMessages ( $element );
			}
	
			$label = $element->getLabel ();
	
			if(!$label){
				$label = $key;
			}
			foreach ( $m as $t ) {
				$topMessage [] = $label . ': ' . $t;
			}
		}
		return Functions::addErrorMessage( implode ( '<br>', $topMessage ) );
	}
	
}
