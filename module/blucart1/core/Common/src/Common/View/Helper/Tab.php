<?php

namespace Common\View\Helper;

use Common\Tab\TabContainer;
use Zend\View\Helper\AbstractHelper;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class Tab extends AbstractHelper implements EventManagerAwareInterface {
	protected $events;
	protected $tabContainer = null;
	
	public function __invoke($eventName, $params = array()) {
		$view = $this->getView ();
		$tabContainer = $this->getTabContainer ();
		$params ['tabContainer'] = $tabContainer;
		$this->getEventManager ()->trigger ( $eventName . '-tab', $this, $params );
		return $this;
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->events = $events;
		return $this;
	}
	public function getEventManager() {
		if (! $this->events) {
			$this->setEventManager ( new EventManager ( array (
					__CLASS__,
					get_called_class () 
			) ) );
		}
		return $this->events;
	}
	
	public function getTabContainer() {
		if (! $this->tabContainer) {
			$this->tabContainer = new TabContainer ();
		}
		return $this->tabContainer;
	}
	public function render($template = 'common/admin-tabs') {
		return $this->getView ()->render ( $template, array (
				'tabContainer' => $this->getTabContainer () 
		) );
	}
}
