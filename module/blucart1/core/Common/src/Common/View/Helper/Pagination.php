<?php

namespace Common\View\Helper;

use Core\Functions;
use Zend\View\Helper\AbstractHelper;

class Pagination extends AbstractHelper {
	public function __invoke($total, $routeName = null, $params = null, $pageRouteName = 'page', $perPageRouteName = 'perPage') {
		$sm = Functions::getServiceLocator ();
		
		if (! $params) {
			$params = Functions::getRouteParams ();
		}
		if (! $routeName) {
			$routeName = Functions::getMatchedRouteName ();
		}
		
		$page = $params [$pageRouteName] = Functions::fromRoute ( $pageRouteName );
		$perPage = $params [$perPageRouteName] = Functions::fromRoute ( $perPageRouteName );
		
		$query = $sm->get ( 'Request' )->getQuery ()->getArrayCopy ();
		$post = $sm->get ( 'Request' )->getPost ()->getArrayCopy ();
		$urlOptions = array (
				'query' => $query
		);
		$qP = array_merge ( $query, $post );
		
		$params = array_merge ( $qP, $params );
		
		$page = $params [$pageRouteName];
		$perPage = $params [$perPageRouteName];
		
		$adjacents = "2";
		$page = ($page == 0 ? 1 : $page);
		$start = ($page - 1) * $perPage;
		
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil ( $total / $perPage );
		
		$lpm1 = $lastpage - 1;
		
		$pagination = "";
		if ($lastpage > 1) {
			
			$pagination .= "<ul class='pagination'>";
			// $pagination .= "<li class='details'>Page $page of
			// $lastpage</li>";
			
			if ($page != 1) {
				$params [$pageRouteName] = 1;
				$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
				$pagination .= "<li><a href='{$url}'>First</a></li>";
				$params [$pageRouteName] = $prev;
				$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
				$pagination .= "<li><a href='{$url}'>Previous</a></li>";
			} else {
				$pagination .= "<li><a class='current'>First</a></li>";
				$pagination .= "<li><a class='current'>Previous</a></li>";
			}
			if ($lastpage < 7 + ($adjacents * 2)) {
				for($counter = 1; $counter <= $lastpage; $counter ++) {
					if ($counter == $page)
						$pagination .= "<li><a class='current'>$counter</a></li>";
					else {
						$params [$pageRouteName] = $counter;
						$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
						$pagination .= "<li><a href='{$url}'>$counter</a></li>";
					}
				}
			} elseif ($lastpage > 5 + ($adjacents * 2)) {
				if ($page < 1 + ($adjacents * 2)) {
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter ++) {
						if ($counter == $page)
							$pagination .= "<li><a class='current'>$counter</a></li>";
						else {
							$params [$pageRouteName] = $counter;
							$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
							$pagination .= "<li><a href='{$url}'>$counter</a></li>";
						}
					}
					$pagination .= "<li class='dot'><a href='javascript:void(0);'>...</a></li>";
					$params [$pageRouteName] = $lpm1;
					$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
					$pagination .= "<li><a href='{$url}'>$lpm1</a></li>";
					$params [$pageRouteName] = $lastpage;
					$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
					$pagination .= "<li><a href='{$url}'>$lastpage</a></li>";
				} elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
					
					$params [$pageRouteName] = 1;
					$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
					$pagination .= "<li><a href='{$url}'>1</a></li>";
					
					$params [$pageRouteName] = 2;
					$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
					$pagination .= "<li><a href='{$url}'>2</a></li>";
					$pagination .= "<li class='dot'><a href='javascript:void(0);'>...</a></li>";
					
					for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter ++) {
						if ($counter == $page)
							$pagination .= "<li><a class='current'>$counter</a></li>";
						else {
							$params [$pageRouteName] = $counter;
							$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
							$pagination .= "<li><a href='{$url}'>$counter</a></li>";
						}
					}
					$pagination .= "<li class='dot'><a href='javascript:void(0);'>...</a></li>";
					
					$params [$pageRouteName] = $lpm1;
					$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
					$pagination .= "<li><a href='{$url}'>$lpm1</a></li>";
					
					$params [$pageRouteName] = $lastpage;
					$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
					$pagination .= "<li><a href='{$url}'>$lastpage</a></li>";
				} else {
					
					$params [$pageRouteName] = 1;
					$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
					
					$pagination .= "<li><a href='{$url}'>1</a></li>";
					
					$params [$pageRouteName] = 2;
					$url = $this->getView ()->url ( $routeName, $params,$urlOptions );
					$pagination .= "<li><a href='{$url}'>2</a></li>";
					
					$pagination .= "<li class='dot'><a href='javascript:void(0);'>...</a></li>";
					for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter ++) {
						if ($counter == $page)
							$pagination .= "<li><a class='current'>$counter</a></li>";
						else {
							$params [$pageRouteName] = $counter;
							$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
							$pagination .= "<li><a href='{$url}'>$counter</a></li>";
						}
					}
				}
			}
			
			if ($page < $counter - 1) {
				$params [$pageRouteName] = $next;
				$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
				$pagination .= "<li><a href='{$url}'>Next</a></li>";
				$params [$pageRouteName] = $lastpage;
				$url = $this->getView ()->url ( $routeName, $params, $urlOptions );
				$pagination .= "<li><a href='{$url}'>Last</a></li>";
			} else {
				$pagination .= "<li><a class='current'>Next</a></li>";
				$pagination .= "<li><a class='current'>Last</a></li>";
			}
			$pagination .= "</ul>\n";
			// $pagination .= "<div class='details'>Page $page of
			// $lastpage</div>";
		}
		
		return $pagination;
	}
}