<?php

namespace Common\View\Helper;

use Core\Functions;

use Zend\View\Helper\AbstractHelper;

class StickyMessage extends AbstractHelper {
	
	
	public function __invoke() {
		$namespaces = array (
				'error',
				'success',
				'info',
				'warning' 
		);
		
		// messages as string
		$flashMessenger = Functions::getFlashMessenger();
		$view = $this->getView ();
		
		$view->inlineScript ()->captureStart ();
		echo 'jQuery(function(){' ;
		echo '$.pnotify.defaults.styling = "bootstrap3";' . PHP_EOL;
                echo '$.pnotify.defaults.history = false;' . PHP_EOL;
                
		foreach ( $namespaces as $ns ) {
			
			$flashMessenger->setNamespace ( $ns );
			
			$messages = array_merge ( $flashMessenger->getMessages (), $flashMessenger->getCurrentMessages () );
			
                        if (! $messages)
				continue;
			
                        $messages = array_unique($messages);
			foreach ( $messages as $message ) {
				echo '$.pnotify({text : "'.$this->getView()->escapeHtml($message).'", type: "'.$ns.'"});' . PHP_EOL;
			}
			
			$flashMessenger->clearCurrentMessages ();
			$flashMessenger->clearMessages ();
		}
		echo '});';
		
		$view->inlineScript ()->captureEnd ();
	}
}