<?php

namespace Common\View\Helper;

use Core\Functions;
use Zend\View\Helper\AbstractHelper;
use Config\Api\ConfigApi;

class Twig extends AbstractHelper {
	protected $_twig;
	public function __invoke($string, $params = array()) {
		$params ['view'] = $this->getView ();
		
		$array = ConfigApi::getConfigArray ();
		
		//$params = $array + $params;
		$twig = $this->getTwig ();
		ob_start ();
		echo $twig->render ( $string, $params );
		$out1 = ob_get_contents ();
		ob_end_clean ();
		return $out1;
	}
	public function getTwig() {
		if (! $this->_twig) {
			$loader = new \Twig_Loader_String ();
			$twig = new \Twig_Environment ( $loader );
			$this->_twig = $twig;
		}
		return $this->_twig;
	}
}