<?php

namespace Common\View\Helper;

use Core\Functions;
use Zend\View\Helper\AbstractHelper;

class FlashMessenger extends AbstractHelper {

    public function __invoke() {
        $namespaces = array(
            'error',
            'info',
            'warning',
            'success',
        );
        $flashMessenger = Functions::getFlashMessenger();
        // messages as string
        $messageString = '';

        foreach ($namespaces as $ns) {

            $flashMessenger->setNamespace($ns);

            $messages = array_merge($flashMessenger->getMessages(), $flashMessenger->getCurrentMessages());

            if (!$messages)
                continue;
            $messages = array_unique($messages);
            if ($ns == 'error') {
                $ns = 'danger';
            }
            $messageString .= "<div class='alert alert-$ns'> " . '<a class="close" data-dismiss="alert">×</a>' . implode('<br />', $messages) . '</div>';
            $flashMessenger->clearCurrentMessages();
            $flashMessenger->clearMessages();
        }

        return $messageString;
    }

}
