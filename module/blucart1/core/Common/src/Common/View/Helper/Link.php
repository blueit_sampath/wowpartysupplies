<?php

namespace Common\View\Helper;

use Common\Link\LinkContainer;
use Zend\View\Helper\AbstractHelper;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class Link extends AbstractHelper implements EventManagerAwareInterface {
	protected $events;
	protected $linkContainer = null;
	
	public function __invoke($eventName, $params = array()) {
		$view = $this->getView ();
		$linkContainer = $this->getLinkContainer ();
		$params ['linkContainer'] = $linkContainer;
		$this->getEventManager ()->trigger ( $eventName . '-link', $this, $params );
		return $this;
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->events = $events;
		return $this;
	}
	public function getEventManager() {
		if (! $this->events) {
			$this->setEventManager ( new EventManager ( array (
					__CLASS__,
					get_called_class () 
			) ) );
		}
		return $this->events;
	}
	
	public function getLinkContainer() {
		if (! $this->linkContainer) {
			$this->linkContainer = new LinkContainer ();
		}
		return $this->linkContainer;
	}
	public function render($template = 'common/admin-links') {
		return $this->getView ()->render ( $template, array (
				'linkContainer' => $this->getLinkContainer () 
		) );
	}
}
