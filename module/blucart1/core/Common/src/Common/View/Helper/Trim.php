<?php
namespace Common\View\Helper;
use Zend\View\Helper\AbstractHelper;

class Trim extends AbstractHelper {

    function __invoke($str, $limit = 100,$appendString = '...', $strip = false) {
        $str = ($strip == true) ? strip_tags($str) : $str;
        if (strlen($str) > $limit) {
            $str = substr($str, 0, $limit - 3);
            return (substr($str, 0, strrpos($str, ' ')) . $appendString);
        }
        return trim($str);
    }

}
