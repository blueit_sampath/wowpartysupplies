<?php

namespace Common\View\Helper;

use Common\Option\Content\ContentContainer;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\View\Helper\AbstractHelper;

class Content extends AbstractHelper implements ServiceLocatorAwareInterface, EventManagerAwareInterface {
	protected $_services;
	protected $_contentContainer;
	protected $_events;
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services->getServiceLocator ();
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (null === $this->_events) {
			$this->setEventManager ( new EventManager () );
		}
		return $this->_events;
	}
	public function __invoke($name, $params = array()) {
		$this->_contentContainer = null;
		$contentContainer = $this->getContentContainer ();
		$contentContainer->setParams ( $params );
		$params['contentContainer'] = $contentContainer;
		$this->getEventManager ()->trigger ( $name . '-content-render', $this, $params );
		return $this;
	}
	public function render($template = 'common/content') {
		$view = $this->getView ();
		
		return $view->render ( $template, array (
				'contentContainer' => $this->getContentContainer () 
		) );
	}
	/**
	 *
	 * @return \Common\Option\Content\ContentContainer
	 */
	public function getContentContainer() {
		if (! $this->_contentContainer) {
			$this->_contentContainer = new ContentContainer ();
		}
		return $this->_contentContainer;
	}
	/**
	 *
	 * @param unknown_type $contentContainer        	
	 * @return \Common\View\Helper\Content
	 */
	public function setContentContainer($contentContainer) {
		$this->_contentContainer = $contentContainer;
		return $this;
	}
}
