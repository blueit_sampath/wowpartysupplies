<?php

namespace Common\Option\Token;

use Core\Item\Item;



class TokenItem extends Item {
	protected $_entity = '';
	protected $_helpText = '';
	/**
	 * @return string $_entity
	 */
	public function getEntity() {
		return $this->_entity;
	}

	/**
	 * @param string $_entity
	 * @param class_name
	 */
	public function setEntity($_entity) {
		$this->_entity = $_entity;
		return $this;
	}

	/**
	 * @return string $_helpText
	 */
	public function getHelpText() {
		return $this->_helpText;
	}

	/**
	 * @param string $_helpText
	 * @param class_name
	 */
	public function setHelpText($_helpText) {
		$this->_helpText = $_helpText;
		return $this;
	}

	
	
	
}
