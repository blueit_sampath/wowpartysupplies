<?php

namespace Common\Option\Token;

use Mail\Option\Mail;
use Core\Functions;
use Zend\Validator\IsInstanceOf;
use Config\Api\ConfigApi;
use Core\Item\Container\ItemContainer;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class TokenContainer extends ItemContainer implements ServiceLocatorAwareInterface, EventManagerAwareInterface {
	protected $_params = array ();
	protected $_subject = '';
	protected $_bodyHtml = '';
	protected $_bodyText = '';
	protected $_fromName = '';
	protected $_fromEmail = '';
	protected $_toName = '';
	protected $_toEmail = '';
	protected $_services;
	protected $_events;
	protected $_attachments = array();
    protected $_mail;
	/**
	 *
	 * @param string $uniqueKey        	
	 * @param string $title        	
	 * @param string $content        	
	 * @param integer $weight        	
	 * @return TokenItem
	 */
	public function add($uniqueKey, $entity = '', $helpText = '', $weight = null) {
		$item = new TokenItem ( $uniqueKey );
		$item->setEntity ( $entity );
		$item->setHelpText ( $helpText );
		$item->setWeight ( $weight );
		$this->addItem ( $item );
		return $item;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return TokenItem
	 */
	public function get($id) {
		return $this->getItem ( $id );
	}
	
	/**
	 *
	 * @return string $_params
	 */
	public function getParams() {
		return $this->_params;
	}
	
	/**
	 *
	 * @param multitype: $_params        	
	 * @param
	 *        	class_name
	 */
	public function setParams($_params) {
		$this->_params = $_params;
		return $this;
	}
	public function getParam($key) {
		if (isset ( $this->_params [$key] )) {
			return $this->_params [$key];
		}
		return false;
	}
	public function addParam($key, $value) {
		$this->_params [$key] = $value;
		return false;
	}
	public function removeParam($key) {
		if (isset ( $this->_params [$key] )) {
			unset ( $this->_params [$key] );
		}
		return true;
	}
	public function clearParams() {
		$this->_params = array ();
		return true;
	}
	/**
	 *
	 * @return string $_subject
	 */
	public function getSubject() {
		return $this->_subject;
	}
	
	/**
	 *
	 * @return string $_bodyHtml
	 */
	public function getBodyHtml() {
		return $this->_bodyHtml;
	}
	
	/**
	 *
	 * @return string $_bodyText
	 */
	public function getBodyText() {
		return $this->_bodyText;
	}
	
	/**
	 *
	 * @param string $_subject        	
	 * @param
	 *        	class_name
	 */
	public function setSubject($_subject) {
		$this->_subject = $_subject;
		return $this;
	}
	
	/**
	 *
	 * @param string $_bodyHtml        	
	 * @param
	 *        	class_name
	 */
	public function setBodyHtml($_bodyHtml) {
		$this->_bodyHtml = $_bodyHtml;
		return $this;
	}
	
	/**
	 *
	 * @param string $_bodyText        	
	 * @param
	 *        	class_name
	 */
	public function setBodyText($_bodyText) {
		$this->_bodyText = $_bodyText;
		return $this;
	}
	public function prepare($eventName = '') {
		if ($eventName) {
			$this->getEventManager ()->trigger ( $eventName . '-token', $this );
		}
		
		$bodyText = $this->getBodyText ();
		if ($bodyText) {
			$bodyText = $this->parse ( $bodyText );
			
			$this->setBodyText ( $bodyText );
		}
		$bodyHtml = $this->getBodyHtml ();
		if ($bodyHtml) {
			$bodyHtml = $this->parse ( $bodyHtml );
			$this->setBodyHtml ( $bodyHtml );
		}
		
		$subject = $this->getSubject ();
		if ($subject) {
			$subject = $this->parse ( $subject );
			$this->setSubject ( $subject );
		}
		$mail = $this->getMail ();
		$mail->setSubject ( $this->getSubject () );
		$mail->setHtmlMessage ( $this->getBodyHtml () );
		$mail->setTextMessage ( $this->getBodyText () );
		return $mail;
	}
	public function parse($string) {
		$sm = Functions::getServiceLocator();
		$tokenItems = $this->getItems ();
		$array = array ();
		foreach ( $tokenItems as $token ) {
			$array [$token->getId ()] = $token->getEntity ();
		}
		$twig = $sm->get ( 'view_helper_manager' )->get ( 'twig' );
		$string =  $twig ( $string, $array );
		return $string;
	}
	public function sendMail() {
	    $mail = $this->getMail ();
		$mail->addTo ( $this->getToEmail (), $this->getToName () );
		$mail->addFrom ( $this->getFromEmail (), $this->getFromName () );
		$mail->formMessage();
		$mail->sendMail ();
		return $mail;
	}
	/**
	 *
	 * @return Mail
	 */
	public function getMail() {
		$sm = Functions::getServiceLocator();
		if(!$this->_mail){
		$this->_mail = $sm->get ( 'Mail\Option\Mail' );
		}
		return $this->_mail;
	}
	/*
	 * public function parse($string) { $matches = $this->findMatching ( $string
	 * ); if (count ( $matches ) && count ( $matches [0] ) && count ( $matches
	 * [1] )) { $tokenItems = $this->getItems (); foreach ( $tokenItems as
	 * $token ) { ${$token->getId ()} = $token->getEntity (); } foreach (
	 * $matches [1] as $key => $value ) { $tempObjectString = $value; if (strpos
	 * ( $value, '.' ) !== false) { try { $temp = explode ( '.', $value );
	 * $tempObjectString = $value; $level = 0; foreach ( $temp as $value1 ) { if
	 * ($level == 0) { $item = $this->get ( $value1 ); if (!$item) { break; }
	 * $tempObjectString = $item->getEntity (); } else { // 								if
	 * (!property_exists ( $tempObjectString, $value1 )) { // 									$x = new
	 * \ReflectionClass($tempObjectString); // 									$r =
	 * $x->hasProperty($value); // 									if(!$r){ // 										break; //
	 * 									} // 								} $tempObjectString = $tempObjectString->$value1; }
	 * $level ++; } } catch ( \Exception $e ) { } } else { ; $t1 =
	 * ConfigApi::getConfigByKey ( $value ); if ($t1 !== null) {
	 * $tempObjectString = $t1; } } $matches [1] [$key] = $tempObjectString; }
	 * $string = $this->replaceMatching ( $matches, $string ); } return $string;
	 * } public function findMatching($string) { preg_match_all (
	 * '/\$\{(\S*)\}\$/', $string, $matches ); return $matches; } public
	 * function replaceMatching($array, $string) { foreach ( $array [0] as $key
	 * => $value ) { $sub= $array [1] [$key]; if(is_object($sub)){ if($sub
	 * instanceof \DateTime){ $dateFormat =
	 * Functions::getViewHelperManager()->get('dateFormat'); $sub =
	 * $dateFormat($sub, \IntlDateFormatter::MEDIUM, \IntlDateFormatter::SHORT);
	 * } else{ continue; } } $string = str_replace ( $value, $sub, $string ); }
	 * return $string; }
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services;
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (null === $this->_events) {
			$this->setEventManager ( new EventManager () );
		}
		return $this->_events;
	}
	/**
	 *
	 * @return string $_fromName
	 */
	public function getFromName() {
		return $this->_fromName;
	}
	
	/**
	 *
	 * @return string $_fromEmail
	 */
	public function getFromEmail() {
		return $this->_fromEmail;
	}
	
	/**
	 *
	 * @param string $_fromName        	
	 * @param
	 *        	class_name
	 */
	public function setFromName($_fromName) {
		$this->_fromName = $_fromName;
		return $this;
	}
	
	/**
	 *
	 * @param string $_fromEmail        	
	 * @param
	 *        	class_name
	 */
	public function setFromEmail($_fromEmail) {
		$this->_fromEmail = $_fromEmail;
		return $this;
	}
	/**
	 *
	 * @return string $_toName
	 */
	public function getToName() {
		return $this->_toName;
	}
	
	/**
	 *
	 * @return string $_toEmail
	 */
	public function getToEmail() {
		return $this->_toEmail;
	}
	
	/**
	 *
	 * @param string $_toName        	
	 * @param
	 *        	class_name
	 */
	public function setToName($_toName) {
		$this->_toName = $_toName;
		return $this;
	}
	
	/**
	 *
	 * @param string $_toEmail        	
	 * @param
	 *        	class_name
	 */
	public function setToEmail($_toEmail) {
		$this->_toEmail = $_toEmail;
		return $this;
	}
}
