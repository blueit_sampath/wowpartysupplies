<?php
namespace Common\Option\Token;

use Core\Functions;
use Core\Event\Core\AbstractEvent;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventCollection;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventInterface;
use Config\Api\ConfigApi;

abstract class AbstractTokenEvent extends AbstractEvent
{

    /**
     *
     * @var TokenContainer
     */
    protected $_tokenContainer;

    abstract public function getEventName();

    public function getPriority()
    {
        return null;
    }

    public function attach(EventManagerInterface $events)
    {
        $priority = $this->getPriority();
        $eventName = $this->getEventName();
        
        $sharedEventManager = $events->getSharedManager();
        
        $this->_listeners[] = $sharedEventManager->attach('*', $eventName . '-token', array(
            $this,
            'preToken'
        ), $priority);
        
        return $this;
    }

    public function preToken($e)
    {
        $tokenContainer = $e->getTarget();
        if (! $tokenContainer->get('config')) {
            $array = ConfigApi::getConfigArray();
            $item = $tokenContainer->add('config', $array, '', 1000);
        }
        $this->setTokenContainer($tokenContainer);
        return $this->token();
    }

    abstract public function token();

    /**
     *
     * @return TokenContainer $_tokenContainer
     */
    public function getTokenContainer()
    {
        return $this->_tokenContainer;
    }

    /**
     *
     * @param TokenContanier $_tokenContainer            
     * @return \Common\Option\Token\AbstractTokenEvent
     */
    public function setTokenContainer($_tokenContainer)
    {
        $this->_tokenContainer = $_tokenContainer;
        return $this;
    }
}
