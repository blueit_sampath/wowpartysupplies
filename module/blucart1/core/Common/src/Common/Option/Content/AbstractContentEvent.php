<?php

namespace Common\Option\Content;

use Core\Functions;
use Core\Event\Core\AbstractEvent;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventCollection;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventInterface;

abstract class AbstractContentEvent extends AbstractEvent {
	protected $_contentContainer;
	protected $_contentTemplate;
	protected $_contentName;
	abstract public function getEventName();
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$priority = $this->getPriority ();
		$eventName = $this->getEventName ();
		
		$sharedEventManager = $events->getSharedManager ();
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-content-render', array (
				$this,
				'preRender' 
		), $priority );
		
		return $this;
	}
	public function preRender($e) {
		$contentContainer = $e->getParam ( 'contentContainer' );
		
		$this->setContentContainer ( $contentContainer );
		return $this->render ();
	}
	public function render() {
		if ($this->_contentTemplate) {
			$contentContainer = $this->getContentContainer ();
			
			$renderer = Functions::getServiceLocator ()->get ( 'viewrenderer' );
			$item = $contentContainer->add ( $this->_contentName );
			$renderer->render ( $this->_contentTemplate, array (
					'item' => $item,
					'contentContainer' => $contentContainer 
			) );
		}
	}
	/**
	 *
	 * @return ContentContainer $_contentContainer
	 */
	public function getContentContainer() {
		return $this->_contentContainer;
	}
	
	/**
	 *
	 * @param ContentContainer $_contentContainer        	
	 *
	 */
	public function setContentContainer($_contentContainer) {
		$this->_contentContainer = $_contentContainer;
		return $this;
	}
}
