<?php

namespace Common\Option\Content;

use Core\Item\Container\ItemContainer;

class ContentContainer extends ItemContainer {
	protected $_params = array ();
	
	/**
	 *
	 * @param string $uniqueKey        	
	 * @param string $title        	
	 * @param string $content        	
	 * @param integer $weight        	
	 * @return ContentItem
	 */
	public function add($uniqueKey, $title = '', $content = '', $weight = null) {
		$item = new ContentItem ( $uniqueKey );
		$item->setTitle ( $title );
		$item->setContent ( $content );
		$item->setWeight ( $weight );
		$this->addItem ( $item );
		return $item;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return DetailItem
	 */
	public function get($id) {
		return $this->getItem ( $id );
	}
	
	/**
	 *
	 * @return string $_params
	 */
	public function getParams() {
		return $this->_params;
	}
	
	/**
	 *
	 * @param multitype: $_params        	
	 * @param
	 *        	class_name
	 */
	public function setParams($_params) {
		$this->_params = $_params;
		return $this;
	}
	public function getParam($key) {
		if (isset ( $this->_params [$key] )) {
			return $this->_params [$key];
		}
		return false;
	}
	public function addParam($key, $value) {
		$this->_params [$key] = $value;
		return false;
	}
	public function removeParam($key) {
		if (isset ( $this->_params [$key] )) {
			unset ( $this->_params [$key] );
		}
		return true;
	}
	public function clearParams() {
		$this->_params = array ();
		return true;
	}
}
