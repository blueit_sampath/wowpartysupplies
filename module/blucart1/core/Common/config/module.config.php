<?php

namespace Common;

return array(
    'router' => array(
        'routes' => array(
            'captcha' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/captcha[/:id]',
                    'constraints' => array()
                    ,
                    'defaults' => array(
                        'controller' => 'Common\Controller\Captcha',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    'events' => array(),
    'service_manager' => array(
        'invokables' => array(
            'TokenContainer' => 'Common\Option\Token\TokenContainer'
        ),
        'shared' => array(
            'TokenContainer' => false
        )
    ),
    'controllers' => array(
        'invokables' => array()
    )
    ,
    'view_manager' => array(
        'template_path_stack' => array(
            __NAMESPACE__ => __DIR__ . '/../view'
        )
    ),
    
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                __NAMESPACE__ => __DIR__ . '/../public'
            )
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'tab' => 'Common\View\Helper\Tab',
            'link' => 'Common\View\Helper\Link',
            'flashMessenger' => 'Common\View\Helper\FlashMessenger',
            'stickyMessage' => 'Common\View\Helper\StickyMessage',
            'content' => 'Common\View\Helper\Content',
            'pagination' => 'Common\View\Helper\Pagination',
            'twig' => 'Common\View\Helper\Twig',
            'formRadio' => 'Common\Form\View\Helper\TwbBundleFormRadio',
            'formRow' => 'Common\Form\View\Helper\TwbBundleFormRow',
            'formSubmit' => 'Common\Form\View\Helper\TwbBundleFormSubmit',
            'formMultiCheckbox' => 'Common\Form\View\Helper\TwbBundleFormMultiCheckbox',
            'trim' => 'Common\View\Helper\Trim',
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Common\Controller\Captcha' => 'Common\Controller\CaptchaController'
        )
    ),
    'form_options' => array(
        'element_size' => 'col-sm-4',
        'label_size' => 'col-sm-2',
        'skip_label_size' => 'col-sm-offset-2',
    )
);
