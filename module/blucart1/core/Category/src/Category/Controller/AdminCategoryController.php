<?php

namespace Category\Controller;

use Zend\View\Model\ViewModel;

use Common\MVC\Controller\AbstractAdminController;
use Core\Functions;

class AdminCategoryController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Category\Form\AdminCategory\CategoryFormFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-category-add', array (
				'categoryId' => $form->get ( 'id' )->getValue () 
		) );
	}
	
}