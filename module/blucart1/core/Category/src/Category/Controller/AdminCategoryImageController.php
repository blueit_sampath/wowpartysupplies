<?php

namespace Category\Controller;
use Common\MVC\Controller\AbstractAdminController;

class AdminCategoryImageController extends AbstractAdminController {
	public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'Category\Form\AdminCategoryImage\CategoryImageFormFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		return $this->redirect ()->toRoute ( 'admin-category-image-add', array (
						'categoryImageId' => $form->get ( 'id' )->getValue (),
						'categoryId' => $form->get ( 'category' )->getValue () 
				) );
	}
	
	
}