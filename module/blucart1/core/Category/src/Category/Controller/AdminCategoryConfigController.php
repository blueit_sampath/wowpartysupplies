<?php
namespace Category\Controller;

use Common\MVC\Controller\AbstractAdminController;

class AdminCategoryConfigController extends AbstractAdminController
{

    public function getFormFactory()
    {
        return $this->getServiceLocator()->get('Category\Form\AdminCategoryConfig\CategoryConfigFormFactory');
    }

    public function getSaveRedirector()
    {
        $form = $this->getFormFactory();
        return $this->redirect()->toRoute('admin-category-config');
    }
}