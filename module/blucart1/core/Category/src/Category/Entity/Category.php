<?php

namespace Category\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 */
class Category {
	/**
	 *
	 * @var integer @ORM\Column(name="id", type="integer", nullable=false)
	 *      @ORM\Id
	 *      @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;
	
	/**
	 *
	 * @var string @ORM\Column(name="name", type="string", length=45,
	 *      nullable=false)
	 */
	private $name;
	
	/**
	 *
	 * @var string @ORM\Column(name="title", type="string", length=255,
	 *      nullable=true)
	 */
	private $title;
	
	/**
	 *
	 * @var string @ORM\Column(name="short_description", type="string",
	 *      length=255, nullable=true)
	 */
	private $shortDescription;
	
		
	
	/**
	 *
	 * @var integer @ORM\Column(name="weight", type="integer", nullable=true)
	 */
	private $weight;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="created_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="create")
	 */
	private $createdDate;
	
	/**
	 *
	 * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
	 *      nullable=true)
	 *      @Gedmo\Timestampable(on="update")
	 */
	private $updatedDate;
	
	/**
	 *
	 * @var string @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;
	
	/**
	 *
	 * @var boolean @ORM\Column(name="status", type="boolean", nullable=true)
	 */
	private $status;
	
	/**
	 *
	 * @var \Category @ORM\ManyToOne(targetEntity="Category")
	 *      @ORM\JoinColumns({
	 *      @ORM\JoinColumn(name="parent_id", referencedColumnName="id",nullable=true,onDelete="SET NULL")
	 *      })
	 */
	private $parent;
	/**
	 * Magic getter to expose protected properties.
	 *
	 * @param string $property        	
	 * @return mixed
	 *
	 */
	public function __get($property) {
		if (\method_exists ( $this, "get" . \ucfirst ( $property ) )) {
			$method_name = "get" . \ucfirst ( $property );
			return $this->$method_name ();
		} else {
			
			if (is_object ( $this->$property )) {
				// return $this->$property->id;
			}
			return $this->$property;
		}
	}
	
	/**
	 * Magic setter to save protected properties.
	 *
	 * @param string $property        	
	 * @param mixed $value        	
	 *
	 */
	public function __set($property, $value) {
		if (\method_exists ( $this, "set" .\ucfirst ( $property ) )) {
			$method_name = "set" .\ucfirst ( $property );
			
			$this->$method_name ( $value );
		} else
			$this->$property = $value;
	}
        public function __isset($name){
            if(isset($this->$name)){
                return true;
            }
            return false;
	}
}
