<?php

namespace Category\Api;

use Common\Api\Api;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Core\Functions;

class CategoryReportApi extends Api {
	protected static $_entity = '\Category\Entity\Category';
	public static function getAllCategorysCount( $status, $startDate, $endDate) {
		$queryBuilder = static::getQueryBuilder ();
		$queryBuilder->addColumn ( 'count(category.id)', 'countCategoryId' );
		
		if ($status !== null) {
			$queryBuilder->addWhere ( 'category.status', 'categoryStatus' );
			$queryBuilder->addParameter ( 'categoryStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'category' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		// $item = $queryBuilder->addWhere ( 'category.id', 'categoryId' );
		// $queryBuilder->addParameter ( 'categoryId', $categoryId );
		return $queryBuilder->executeSingleScalarQuery ( 'count' );
	}
	
	public static function getAllCategoryCountByDay($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
	
		$queryBuilder->addColumn ( "DateFormat(category.createdDate, '%D %M,%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(category.id)', 'countCategoryId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'category.status', 'categoryStatus' );
			$queryBuilder->addParameter ( 'categoryStatus', $status );
		}
	
		$item = $queryBuilder->addFrom ( static::$_entity, 'category' );
	
		if ($startDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
	
		if ($endDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
	
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	
	public static function getAllCategoryCountByWeek($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
	
		$queryBuilder->addColumn ( "DateFormat(category.createdDate, '%v,%x')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(category.id)', 'countCategoryId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'category.status', 'categoryStatus' );
			$queryBuilder->addParameter ( 'categoryStatus', $status );
		}
	
		$item = $queryBuilder->addFrom ( static::$_entity, 'category' );
	
		if ($startDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
	
		if ($endDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
	
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	
	public static function getAllCategoryCountByMonth($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
		
		$queryBuilder->addColumn ( "DateFormat(category.createdDate, '%M,%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(category.id)', 'countCategoryId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'category.status', 'categoryStatus' );
			$queryBuilder->addParameter ( 'categoryStatus', $status );
		}
		
		$item = $queryBuilder->addFrom ( static::$_entity, 'category' );
		
		if ($startDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
		
		if ($endDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
		
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
	
	public static function getAllCategoryCountByYear($status, $startDate, $endDate, $skip = 0, $limit = PHP_INT_MAX) {
		$queryBuilder = static::getQueryBuilder ();
	
		$queryBuilder->addColumn ( "DateFormat(category.createdDate, '%Y')", 'createdDate' );
		$queryBuilder->addColumn ( 'count(category.id)', 'countCategoryId' );
		if ($status !== null) {
			$queryBuilder->addWhere ( 'category.status', 'categoryStatus' );
			$queryBuilder->addParameter ( 'categoryStatus', $status );
		}
	
		$item = $queryBuilder->addFrom ( static::$_entity, 'category' );
	
		if ($startDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_start', ' >= ' );
			$queryBuilder->setParameter ( 'createdDate_start', $startDate );
		}
	
		if ($endDate) {
			$queryBuilder->addWhere ( 'category.createdDate', 'createdDate_end', ' <= ' );
			$queryBuilder->setParameter ( 'createdDate_end', $endDate );
		}
		$queryBuilder->addGroup ( 'createdDate', 'createdDate' );
	
		return $queryBuilder->executeQuery ( $skip, $limit );
	}
}
