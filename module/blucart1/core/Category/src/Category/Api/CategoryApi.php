<?php

namespace Category\Api;

use QueryBuilder\Option\QueryFrom\QueryJoinItem;
use Common\Api\Api;

class CategoryApi extends Api {

    protected static $_entity = '\Category\Entity\Category';
    protected static $_imageEntity = '\Category\Entity\CategoryImage';

    public static function getAllCategories($status = null) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('category.id', 'categoryId');
        $queryBuilder->addFrom(static::$_entity, 'category');
        if ($status !== null) {
            $queryBuilder->addWhere('category.status', 'categoryStatus');
            $queryBuilder->addParameter('categoryStatus', $status);
        }
        $queryBuilder->addOrder('category.weight', 'categoryWeight', 'desc');
        return $queryBuilder->executeQuery();
    }

    public static function getCategoryById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_entity, $id);
    }

    public static function getCategoryByName($name) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array('name' => $name));
    }

    public static function getChildrenCategories($categoryId, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('category.id', 'categoryId');
        $queryBuilder->addFrom(static::$_entity, 'category');

        if ($status !== null) {
            $queryBuilder->addWhere('category.status', 'categoryStatus');
            $queryBuilder->addParameter('categoryStatus', $status);
        }

        if ($categoryId === null) {
            $queryBuilder->addWhere('category.parent', 'categoryParent', null);
        } else {
            $queryBuilder->addWhere('category.parent', 'categoryParent');
            $queryBuilder->addParameter('categoryParent', $categoryId);
        }

        $queryBuilder->addOrder('category.weight', 'categoryWeight', 'desc');

        return $queryBuilder->executeQuery();
    }

    public static function getSortedArray() {
        $array = static::getChildrenCategories(null, null);
        return static::makeArray($array);
    }

    public static function makeArray($array) {
        $result = array();
        foreach ($array as $categoryResults) {
            $catId = $categoryResults ['categoryId'];
            $category = static::getCategoryById($catId);
            $result [$category->id] = array(
                'name' => $category->name
            );

            $children = static::getChildrenCategories($category->id, null);
            if (count($children)) {

                $result [$category->id] ['children'] = static::makeArray($children);
            }
        }
        return $result;
    }

    public static function getCategoryImageByCategoryId($categoryId, $status = true) {
        $queryBuilder = static::getQueryBuilder();
        $queryBuilder->addColumn('categoryImage.id', 'categoryImageId');

        if ($status !== null) {
            $queryBuilder->addWhere('categoryImage.status', 'categoryImageStatus');
            $queryBuilder->addParameter('categoryImageStatus', $status);
        }
        $queryBuilder->addOrder('categoryImage.weight', 'categoryImageWeight', 'desc');

        $item = $queryBuilder->addFrom(static::$_imageEntity, 'categoryImage');
        $joinItem = new QueryJoinItem('categoryImage.file', 'file');
        $item->addJoin($joinItem);
        $joinItem = new QueryJoinItem('categoryImage.category', 'category');
        $item->addJoin($joinItem);
        $item = $queryBuilder->addWhere('category.id', 'categoryId');
        $queryBuilder->addParameter('categoryId', $categoryId);
        return $queryBuilder->executeQuery();
    }

    public static function getCategoryImageById($id) {
        $em = static::getEntityManager();
        return $em->find(static::$_imageEntity, $id);
    }

    public static function getSortedImageArray($categoryId) {
        $array = static::getCategoryImageByCategoryId($categoryId);
        return static::makeImageArray($array);
    }

    public static function makeImageArray($array) {
        $result = array();
        foreach ($array as $categoryResults) {
            $catImageId = $categoryResults ['categoryImageId'];
            $categoryImage = static::getCategoryImageById($catImageId);
            $result [$categoryImage->id] = array(
                'name' => $categoryImage->file->path
            );
        }
        return $result;
    }

    public static function getTopCategoryImageByCategoryId($categoryId, $status = true) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_imageEntity)->findOneBy(array(
                    'status' => $status,
                    'category' => $categoryId
                        ), array(
                    'weight' => 'desc'
        ));
    }

    public static function getBottomProductImageByProductId($categoryId, $status = true) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_imageEntity)->findOneBy(array(
                    'status' => $status,
                    'category' => $categoryId
                        ), array(
                    'weight' => 'asc',
                    'id' => 'desc'
        ));
    }

}
