<?php 
namespace Category\Service\Content;

use Core\Functions;
use Common\Option\Content\AbstractContentEvent;

class AdminDashboardTopCategory extends AbstractContentEvent {

	protected $_contentTemplate = 'common/admin-dashboard-top-categories';
	protected $_contentName = 'adminDashboardTopCategories';
	public function getEventName() {
		return 'adminDashboardTopStastics';
	}
	public function getPriority() {
		return 950;
	}
	
}

