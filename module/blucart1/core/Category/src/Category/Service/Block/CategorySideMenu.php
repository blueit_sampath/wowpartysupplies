<?php 
namespace Category\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class CategorySideMenu extends AbstractBlockEvent {

	protected $_blockTemplate = 'category-side-menu';

	public function getBlockName() {
		return 'categorySideMenu';
	}
	public function getPriority() {
		return 1000;
	}
	public function getTitle() {
		return 'Category Side Menu';
	}
	
}

