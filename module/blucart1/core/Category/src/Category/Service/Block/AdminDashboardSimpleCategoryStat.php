<?php 
namespace Category\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class AdminDashboardSimpleCategoryStat extends AbstractBlockEvent {

	protected $_blockTemplate = 'admin-dashboard-simple-category-stat';

	public function getBlockName() {
		return 'adminDashboardSimpleCategoryStat';
	}
	public function getPriority() {
		return ;
	}
	public function getTitle() {
		return 'AdminDashboardSimpleCategoryStat';
	}
	
}

