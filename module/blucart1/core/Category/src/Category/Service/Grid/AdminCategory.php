<?php

namespace Category\Service\Grid;

use BlucartGrid\Event\AbstractMainBlucartGridEvent;
use BlucartGrid\Option\ColumnItem;
use Core\Functions;
use QueryBuilder\Option\QueryFrom\QueryJoinItem;

class AdminCategory extends AbstractMainBlucartGridEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'title',
			'weight',
			'shortDescription',
			'description',
			'status' 
	);
	protected $_entity = '\Category\Entity\Category';
	protected $_entityName = 'category';
	protected $_detailTemplateName = 'adminCategoryImage';
	protected $_detailTemplate = 'common/admin-category-image';
	public function getEventName() {
		return 'adminCategory';
	}
	public function getPriority() {
		return 1000;
	}
	public function preSchema($e) {
		parent::preSchema ( $e );
		$array = array ();
		$grid = $this->getGrid ();
		
		$columns = $grid->getColumns ();
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'id' );
		$columnItem->setTitle ( 'Id' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 1000 );
		$columnItem->setIsPrimary ( true );
		$columns->addColumn ( 'category.id', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'name' );
		$columnItem->setTitle ( 'Name' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 990 );
		$columns->addColumn ( 'category.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'title' );
		$columnItem->setTitle ( 'Title' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 980 );
		$columns->addColumn ( 'category.title', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'categoryParentName' );
		$columnItem->setTitle ( 'Parent Category' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 970 );
		$columns->addColumn ( 'categoryParent.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'parentName' );
		$columnItem->setTitle ( 'Parent Category' );
		$columnItem->setType ( 'string' );
		$columnItem->setEditable ( false );
		$columnItem->setWeight ( 960 );
		$columns->addColumn ( 'categoryParent.name', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'weight' );
		$columnItem->setTitle ( 'Priority' );
		$columnItem->setType ( 'number' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 950 );
		$columns->addColumn ( 'category.weight', $columnItem );
		
		$columnItem = new ColumnItem ();
		$columnItem->setField ( 'status' );
		$columnItem->setTitle ( 'Status' );
		$columnItem->setType ( 'boolean' );
		$columnItem->setEditable ( true );
		$columnItem->setWeight ( 940 );
		$columns->addColumn ( 'category.status', $columnItem );
		
		return $columns;
	}
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$fromItem = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		$joinItem = new QueryJoinItem ( 'category.parent', 'categoryParent' );
		$fromItem->addJoin ( $joinItem );
		return true;
	}
	public function afterInsert($params, $entity) {
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		// $params['id'] = $entity->id;
		$resultObject->setResults ( $params );
		$resultContainer->add ( $this->_entityName, $entity );
	}
	
}
