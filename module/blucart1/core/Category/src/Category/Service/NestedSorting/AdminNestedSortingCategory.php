<?php

namespace Category\Service\NestedSorting;

use Core\Functions;
use Category\Api\CategoryApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminNestedSortingCategory extends AbstractNestedSortingEvent {
	protected $_entityName = '\Category\Entity\Category';

	public function getEventName() {
		return 'adminCategorySort';
	}
	public function getPriority() {
		return 1000;
	}
	
	public function getSortedArray() {
		$array = CategoryApi::getChildrenCategories ( null, null );
		return $this->makeArray ( $array );
	}
	public function makeArray($array) {
		$result = array ();
		foreach ( $array as $categoryResults ) {
			$catId = $categoryResults ['categoryId'];
			$category = CategoryApi::getCategoryById ( $catId );
			$result [$category->id] = array (
					'name' => $category->name 
			);
			
			$children = CategoryApi::getChildrenCategories ( $category->id, null );
			if (count ( $children )) {
				
				$result [$category->id] ['children'] = CategoryApi::makeArray ( $children );
			}
		}
		return $result;
	}
	public function saveList($e) {
		$lists = Functions::getServiceLocator ()->get ( 'Request' )->getPost ( 'list' );
		$weight = count ( $lists ) *10;
		$em = $this->getEntityManager ();
		foreach ( $lists as $key => $c ) {
			
			$model = $em->find ( $this->_entityName, $key );
			
			$model->weight = $weight;
			if ($c) {
				$model->parent = $em->find ( $this->_entityName, $c );
			} else {
				$model->parent = null;
			}
			$em->persist ( $model );
			$em->flush ();
			
			$weight = $weight-10;
		}
		
		return $this;
	}
}
