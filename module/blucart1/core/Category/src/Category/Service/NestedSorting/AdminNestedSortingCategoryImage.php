<?php

namespace Category\Service\NestedSorting;

use Core\Functions;
use Category\Api\CategoryApi;
use NestedSorting\Event\AbstractNestedSortingEvent;

class AdminNestedSortingCategoryImage extends AbstractNestedSortingEvent {
	protected $_entityName = '\Category\Entity\CategoryImage';
	protected $_column = 'path';
	public function getEventName() {
		return 'adminCategoryImageSort';
	}
	public function getPriority() {
		return 1000;
	}
	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (
				'category' => $categoryId 
		), array (
				'weight' => 'desc' 
		) );
		if ($results) {
			$column = $this->_column;
			foreach ( $results as $result ) {
				if ($result->file) {
					$array [$result->id] = array (
							'name' => $result->file->$column 
					);
				}
			}
		}
		return $array;
	}
}
