<?php

namespace Category\Service\Navigation;

use Category\Api\CategoryApi;
use Core\Functions;
use Common\Navigation\Event\NavigationEvent;

class CategoryMenu extends NavigationEvent {

    protected $_navigation = null;

    public function breadcrumb() {
        $navigation = $this->getNavigation();
        $categoryId = Functions::fromRoute('categoryId', 0);
        if ($categoryId) {
            $this->formOptions(CategoryApi::getChildrenCategories(null, true), $navigation);
        }
    }

    public function formOptions($array, $navigation, $level = 0) {
        $result = array();

        foreach ($array as $res) {
            $categoryId = $res ['categoryId'];
            $category = CategoryApi::getCategoryById($categoryId);
            $temp = array(
                'label' => $category->title,
                'route' => 'category',
                'params' => array(
                    'categoryId' => $category->id
                ),
                'uri' => $this->getUrl('category', array('categoryId' => $category->id)),
                'id' => 'categoryId' . $category->id
            );
            $navigation->addPage($temp);
            $page = $navigation->findOneBy('id', 'categoryId' . $category->id);

            $children = CategoryApi::getChildrenCategories($category->id, null);
            if (count($children)) {

                $this->formOptions($children, $page, ($level + 1));
            }
        }

        return $result;
    }

}
