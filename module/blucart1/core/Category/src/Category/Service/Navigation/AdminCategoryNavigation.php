<?php

namespace Category\Service\Navigation;

use Category\Api\CategoryApi;
use Admin\Navigation\Event\AdminNavigationEvent;
use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategoryNavigation extends AdminNavigationEvent {

    public function common() {
        $navigation = $this->getNavigation();
        $page = $navigation->findOneBy('id', 'productMain');

        $page->addPages(array(
            array(
                'label' => 'Category',
                'uri' => '#',
                'id' => 'categoryMain',
                'iconClass' => 'glyphicon glyphicon-briefcase'
            )
        ));
        $page = $navigation->findOneBy('id', 'categoryMain');
        $page->addPages(array(
            array(
                'label' => 'Category',
                'route' => 'admin-category',
                'id' => 'admin-category',
                'iconClass' => 'glyphicon glyphicon-briefcase'
            )
        ));
        $page = $navigation->findOneBy('id', 'categoryMain');
        $page->addPages(array(
            array(
                'label' => 'Settings',
                'route' => 'admin-category-config',
                'id' => 'admin-category-config',
                'iconClass' => 'glyphicon glyphicon-wrench',
                'aClass' => 'zoombox'
            )
        ));
    }

    public function breadcrumb() {
        parent::breadcrumb();
        $routeName = Functions::getMatchedRouteName();

        if ($routeName === 'admin-category-view') {
            $category = CategoryApi::getCategoryById(Functions::fromRoute('categoryId'));
            $navigation = $this->getNavigation();
            $page = $navigation->findOneBy('id', 'admin-category');
            $page->addPages(array(
                array(
                    'label' => $category->name,
                    'route' => 'admin-category-view',
                    'params' => array(
                        'categoryId' => $category->id
                    ),
                    'id' => 'admin-category-view'
                )
            ));
        }
    }

}
