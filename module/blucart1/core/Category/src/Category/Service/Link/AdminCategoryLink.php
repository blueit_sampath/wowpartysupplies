<?php

namespace Category\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategoryLink extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategory';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$u = $url ( 'admin-category-add' );
		$item = $linkContainer->add ( 'admin-category-add', 'Add New Category', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		$u = $url ( 'admin-category-sort' );
		$item = $linkContainer->add ( 'admin-category-sort', 'Arrange Categories', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		$item->setImageClass ( 'splashy-slider_no_pointy_thing' );
		return $this;
	}
}
