<?php

namespace Category\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategoryView extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategoryView';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		
		$categoryId = Functions::fromRoute ( 'categoryId' );
		$u = $url ( 'admin-category-add', array (
				'categoryId' => $categoryId 
		) );
		$item = $linkContainer->add ( 'admin-category-add', 'Edit Category', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		$item->setImageClass ( 'splashy-pencil' );
		return $this;
	}
}

