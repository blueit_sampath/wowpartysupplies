<?php

namespace Category\Service\Link;

use Core\Functions;
use Common\Link\AbstractLinkEvent;

class AdminCategoryImageLink extends AbstractLinkEvent {
	public function getEventName() {
		return 'adminCategoryImage';
	}
	public function link() {
		$linkContainer = $this->getLinkContainer ();
		$url = Functions::getUrlPlugin ();
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		$u = $url ( 'admin-category-image-add', array (
				'categoryId' => $categoryId 
		) );
		$item = $linkContainer->add ( 'adminCategoryImageAdd', 'Add Image', $u, 1000 );
		$item->setLinkClass ( 'zoombox ' );
		
		$u = $url ( 'admin-category-image-sort', array (
				'categoryId' => $categoryId 
		) );
		$item = $linkContainer->add ( 'admin-category-image-sort', 'Arrange Images', $u, 1000 );
		$item->setLinkClass ( 'zoombox' );
		$item->setImageClass ( 'splashy-slider_no_pointy_thing' );
		
		return $this;
	}
}

