<?php

namespace Category\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategoryAddTab extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$u = $url ( 'admin-category-add', array (
				'categoryId' => Functions::fromRoute ( 'categoryId' )
		) );
		$tabContainer->add ( 'admin-category-add', 'General Information', $u,1000 );
		
		return $this;
	}
}
