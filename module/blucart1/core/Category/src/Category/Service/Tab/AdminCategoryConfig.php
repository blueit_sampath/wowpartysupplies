<?php

namespace Category\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategoryConfig extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryConfigAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		
		$u = $url ( 'admin-category-config' );
		$tabContainer->add ( 'admin-category-config', 'General/Default Settings', $u, 900 );
		
		return $this;
	}
}

