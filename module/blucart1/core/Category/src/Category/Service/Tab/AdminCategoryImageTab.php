<?php

namespace Category\Service\Tab;

use Core\Functions;
use Common\Tab\AbstractTabEvent;

class AdminCategoryImageTab extends AbstractTabEvent {
	public function getEventName() {
		return 'adminCategoryAdd';
	}
	public function tab() {
		$tabContainer = $this->getTabContainer ();
		$url = Functions::getUrlPlugin ();
		
		$categoryId = Functions::fromRoute ( 'categoryId', 0 );
		
		if(!$categoryId){
			return $this;
		}
		$u = $url ( 'admin-category-image', array (
				'categoryId' => $categoryId 
		) );
		$tabContainer->add ( 'admin-category-image', 'Images', $u, 900 );
		
		return $this;
	}
}

