<?php

namespace Category\Service\Form;

use File\Api\FileApi;
use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminCategoryImage extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'weight',
			'status' 
	);
	protected $_entity = '\Category\Entity\CategoryImage';
	protected $_entityName = 'categoryImage';
	public function getFormName() {
		return 'adminCategoryImageAdd';
	}
	public function getPriority() {
		return 1000;
	}
	
	public function parseSaveEntity($params, $entity) {
		FileApi::deleteFiles ( Functions::fromPost ( 'image_delete', '' ) );
		
		$image = $params ['image'];
		if ($image) {
			$array = explode ( ',', $image );
			foreach ( $array as $file ) {
				$imageEntity = FileApi::createOrUpdateFile ( $file, $params ['title'], $params ['alt'] );
			}
		}
		$entity->file = $imageEntity;
		if ($params ['category']) {
			$entity->category = CategoryApi::getCategoryById ( $params ['category'] );
		}
	}
	
	public function afterSaveEntity($entity) {
		$form = $this->getForm ();
		$resultContainer = $this->getFormResultContainer ();
		parent::afterSaveEntity ( $entity );
		$resultContainer->add ( 'categoryImage', $entity );
		$form->get ( 'category' )->setValue ( $entity->category->id );
	}
	public function getId() {
		return Functions::fromRoute ( 'categoryImageId' );
	}
	public function beforeGetRecord() {
		$form = $this->getForm ();
		$form->get ( 'category' )->setValue ( Functions::fromRoute ( 'categoryId', 0 ) );
	}
	public function afterGetRecord($entity) {
		$form = $this->getForm ();
		parent::afterGetRecord ( $entity );
		$form->get ( 'title' )->setValue ( $entity->file->title );
		$form->get ( 'alt' )->setValue ( $entity->file->alt );
		$form->get ( 'image' )->setValue ( $entity->file->path );
	}
}
