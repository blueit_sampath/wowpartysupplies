<?php
namespace Category\Service\Form;


class AdminCategoryConfig extends \Config\Service\Form\AdminConfig {

    protected $_columnKeys = array(
        'CATEGORY_DEFAULT_TITLE',
        'CATEGORY_DEFAULT_DESCRIPTION',
        'CATEGORY_DEFAULT_SHORT_DESCRIPTION',
       
    );

    public function getFormName() {
        return 'adminCategoryConfig';
    }

    public function getPriority() {
        return 1000;
    }

}
