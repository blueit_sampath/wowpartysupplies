<?php

namespace Category\Service\Form;

use Category\Api\CategoryApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class AdminCategory extends AbstractMainFormEvent {
	protected $_columnKeys = array (
			'id',
			'name',
			'title',
			'weight',
			'shortDescription',
			'description',
			'status' 
	);
	protected $_entity = '\Category\Entity\Category';
	protected $_entityName = 'category';
	public function getFormName() {
		return 'adminCategory';
	}
	public function getPriority() {
		return 1000;
	}
	
	public function getId() {
		return Functions::fromRoute ( 'categoryId' );
	}
	
	public function afterGetRecord($entity) {
		parent::afterGetRecord ($entity);
		$form = $this->getForm ();
		if ($entity->parent) {
			$form->get ( 'parent' )->setValue ( $entity->parent->id );
		}
                $form->get ( 'name' )->setAttribute ( 'readonly',true );
	}
	
	public function parseSaveEntity($params, $entity) {
		parent::parseSaveEntity ( $params, $entity );
		if (isset ( $params ['parent'] )) {
			if ($params ['parent']) {
				$entity->parent = CategoryApi::getCategoryById ( $params ['parent'] );
			} else {
				$entity->parent = null;
			}
		}
		return $entity;
	}
	public function afterSaveEntity($entity) {
		$resultContainer = $this->getFormResultContainer ();
		parent::afterSaveEntity ( $entity );
		$resultContainer->add ( 'category', $entity );
	}
	
	
}
