<?php

namespace Category\Form\AdminCategory;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CategoryFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'regex',
                    'options' => array(
                        'pattern' => "/^[a-zA-Z0-9_]+$/",
                        'messages' => array(
                            \Zend\Validator\Regex::NOT_MATCH => 'Use only letters, numbers or _'
                        )
                    )
                ),
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => array(
                            $this,
                            'checkCategory'
                        ),
                        'messages' => array(
                            \Zend\Validator\Callback::INVALID_VALUE => "Category Name already exists",
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter title',
                        ),
                    ),
                )
            )
        ));

        $this->add(array(
            'name' => 'shortDescription',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'description',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));
        $this->add(array(
            'name' => 'weight',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));

        $this->add(array(
            'name' => 'parent',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Int'
                )
            )
        ));

        $this->add(array(
            'name' => 'status',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty'
                ),
                array(
                    'name' => 'Int'
                )
            )
        ));
    }

    public function checkCategory($value, $data = array()) {
        $entity = \Category\Api\CategoryApi::getCategoryByName($value);
        if (!$entity) {
            return true;
        }
        if ($entity->id == $data['id']) {
            return true;
        }
        return false;
    }

}
