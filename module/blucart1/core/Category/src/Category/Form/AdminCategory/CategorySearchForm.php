<?php

namespace Category\Form\AdminCategory;

use Common\Form\Form;
use Zend\Form\Element\Select;

class CategorySearchForm extends Form {
	public function __construct($name = 'adminCategorySearch') {
		parent::__construct ( $name );
		$this->initPreEvent();
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Category Name' 
				) 
		), array (
				'priority' => 100 
		) );
		
		$this->add ( array (
				'name' => 'parentName',
				'attributes' => array (
						'type' => 'text',
						'class' => 'op-contains',
						'placeholder' => 'Parent Category' 
				) 
		), array (
				'priority' => 100 
		) );
		
		$select = new Select ( 'status' );
		$select->setValueOptions ( array (
				'' => 'Please select status',
				'true' => 'Enabled',
				'false' => 'Disabled' 
		) );
		$select->setAttribute ( 'placeholder', 'Status' );
		$this->add ( $select, array (
				'priority' => 80 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Search' 
				) 
		), array (
				'priority' => - 100 
		) );
		$this->initPostEvent();
	}
}