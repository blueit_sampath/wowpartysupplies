<?php

namespace Category\Form\AdminCategory;

use Common\Form\Option\AbstractFormFactory;

class CategoryFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Category\Form\AdminCategory\CategoryForm' );
		$form->setName ( 'adminCategory' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Category\Form\AdminCategory\CategoryFilter' );
	}
}
