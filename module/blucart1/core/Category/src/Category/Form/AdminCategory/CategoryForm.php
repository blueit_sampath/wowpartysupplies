<?php

namespace Category\Form\AdminCategory;

use Core\Functions;
use Zend\Form\Element\Select;
use Category\Api\CategoryApi;
use Zend\Form\Fieldset;
use Common\Form\Form;
use Zend\Captcha;
use Zend\Form\Element;

class CategoryForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'id',
				'attributes' => array (
						'type' => 'hidden' 
				) 
		) );
		$this->add ( array (
				'name' => 'name',
				'attributes' => array (
						'type' => 'text'
                                               
				),
				'options' => array (
						'label' => 'Name',
						'description' => 'Please enter the description' 
				)
				 
		), array (
				'priority' => 1000 
		) );
		
		$this->add ( array (
				'name' => 'title',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Title' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'shortDescription',
				'attributes' => array (
						'type' => 'textarea' 
				),
				'options' => array (
						'label' => 'Short Description' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'description',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'Description' 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'attributes' => array (
						'type' => 'text',
						'title' => 'Please enter priority' 
				),
				'options' => array (
						'label' => 'Priority' 
				) 
		), array (
				'priority' => 960 
		) );
		
		$select = new Select ( 'parent' );
		$select->setLabel ( 'Parent Category' );
		$select->setValueOptions ( $this->getCategories () );
		$this->add ( $select, array (
				'priority' => 950 
		) );
		
		$this->getStatus ( 940 );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getCategories() {
		$array = array (
				'' => '' 
		);
		$result = $this->formOptions ( CategoryApi::getChildrenCategories ( null, null ), Functions::fromRoute ( 'categoryId' ) );
		return $array + $result;
	}
	
	public function formOptions($array, $id, $level = 0) {
		$result = array ();
		
		foreach ( $array as $res ) {
			$categoryId = $res ['categoryId'];
			
			if ($id == $categoryId) {
				continue;
			}
			$category = CategoryApi::getCategoryById ( $categoryId );
			$space = '';
			
			$name = '';
			$parent = $category;
			while ( $parent->parent ) {
				$name = $parent->parent->name . ' > ' . $name;
				$parent = $parent->parent;
			}
			
			if (! $name) {
				$result [$category->id] = $category->name;
			} else {
				$result [$category->id] = $name . $category->name;
			}
			
			$children = CategoryApi::getChildrenCategories ( $category->id, null );
			if (count ( $children )) {
				
				$result = $result + $this->formOptions ( $children, $id, ($level + 1) );
			}
		}
		
		return $result;
	}
}