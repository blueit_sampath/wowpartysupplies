<?php

namespace Category\Form\AdminCategoryConfig;

use Common\Form\Option\AbstractFormFactory;

class CategoryConfigFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Category\Form\AdminCategoryConfig\CategoryConfigForm' );
		$form->setName ( 'adminCategoryConfig' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Category\Form\AdminCategoryConfig\CategoryConfigFilter' );
	}
}
