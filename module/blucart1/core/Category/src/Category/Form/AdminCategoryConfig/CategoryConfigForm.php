<?php

namespace Category\Form\AdminCategoryConfig;

use Core\Functions;
use Zend\Form\Element\Select;
use Category\Api\CategoryApi;
use Zend\Form\Fieldset;
use Common\Form\Form;
use Zend\Captcha;
use Zend\Form\Element;

class CategoryConfigForm extends Form 

{
	public function init() {
		$this->setAttribute ( 'method', 'post' );
		
		$this->add ( array (
				'name' => 'CATEGORY_DEFAULT_TITLE',
				'attributes' => array (
						'type' => 'text' 
				),
				'options' => array (
						'label' => 'Title' 
				) 
		), array (
				'priority' => 990 
		) );
		
		$this->add ( array (
				'name' => 'CATEGORY_DEFAULT_SHORT_DESCRIPTION',
				'attributes' => array (
						'type' => 'textarea' 
				),
				'options' => array (
						'label' => 'Short Description' 
				) 
		), array (
				'priority' => 980 
		) );
		
		$this->add ( array (
				'name' => 'CATEGORY_DEFAULT_DESCRIPTION',
				'attributes' => array (
						'type' => 'textarea',
						'class' => 'wysiwg_full' 
				),
				'options' => array (
						'label' => 'Description' 
				) 
		), array (
				'priority' => 970 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	
}