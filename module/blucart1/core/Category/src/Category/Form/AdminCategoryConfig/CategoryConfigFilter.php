<?php

namespace Category\Form\AdminCategoryConfig;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CategoryConfigFilter extends InputFilter {

    protected $inputFilter;

    public function __construct() {


        $this->add(array(
            'name' => 'CATEGORY_DEFAULT_TITLE',
            'required' => FALSE,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter title',
                        ),
                    ),
                )
            )
        ));

        $this->add(array(
            'name' => 'CATEGORY_DEFAULT_SHORT_DESCRIPTION',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            )
        ));

        $this->add(array(
            'name' => 'CATEGORY_DEFAULT_DESCRIPTION',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                )
            )
        ));
    }

}
