<?php

namespace Category\Form\AdminCategoryImage;

use Common\Form\Option\AbstractFormFactory;

class CategoryImageFormFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'Category\Form\AdminCategoryImage\CategoryImageForm' );
		$form->setName ( 'adminCategoryImageAdd' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'Category\Form\AdminCategoryImage\CategoryImageFilter' );
	}
}
