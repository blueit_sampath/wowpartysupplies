<?php

namespace Category\Form\AdminCategoryImage;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CategoryImageFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		
		$this->add ( array (
				'name' => 'title',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StripTags' 
						),
						array (
								'name' => 'StringTrim' 
						) 
				),
				
		) );
		$this->add ( array (
				'name' => 'image',
				'required' => true,
				'filters' => array (
						array (
								'name' => 'StripTags'
						),
						array (
								'name' => 'StringTrim'
						)
				),
				
		) );
		
		$this->add ( array (
				'name' => 'alt',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StripTags'
						),
						array (
								'name' => 'StringTrim'
						)
				),
				
		) );
		
		$this->add ( array (
				'name' => 'weight',
				'required' => false,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'category',
				'required' => true,
				'validators' => array (
						array (
								'name' => 'Int' 
						) 
				) 
		) );
		
		$this->add ( array (
				'name' => 'status',
				'required' => true,
				'validators' => array (
						
						array (
								'name' => 'Int' 
						) 
				) 
		) );
	}
} 