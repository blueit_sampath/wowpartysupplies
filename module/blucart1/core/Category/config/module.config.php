<?php

return array(
    'controllers' => array('invokables' => array(
            'Category\Controller\AdminCategory' => 'Category\Controller\AdminCategoryController',
            'Category\Controller\AdminCategoryImage' => 'Category\Controller\AdminCategoryImageController',
            'Category\Controller\Index' => 'Category\Controller\IndexController',
            'Category\Controller\AdminCategoryConfig' => 'Category\Controller\AdminCategoryConfigController'
        )),
    'router' => array('routes' => array(
            'category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/category[/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\Index',
                        'action' => 'index'
                    ),
                    'constraints' => array('categoryId' => '[0-9]+')
                )
            ),
            'admin-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category',
                    'defaults' => array(
                        'controller' => 'Category\Controller\AdminCategory',
                        'action' => 'index'
                    )
                )
            ),
            'admin-category-config' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/config[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\AdminCategoryConfig',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-category-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/view[/ajax/:ajax][/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\AdminCategory',
                        'action' => 'view',
                        'ajax' => 'false'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-category-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/add[/ajax/:ajax][/:categoryId]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\AdminCategory',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-category-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/sort[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\AdminCategory',
                        'action' => 'sort',
                        'ajax' => 'true'
                    ),
                    'constraints' => array('ajax' => 'true|false')
                )
            ),
            'admin-category-image' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/image/:categoryId[/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\AdminCategoryImage',
                        'action' => 'index',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-category-image-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/image/add[/ajax/:ajax][/:categoryId][/:categoryImageId]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\AdminCategoryImage',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'categoryImageId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-category-image-sort' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/category/image/sort[/:categoryId][/ajax/:ajax]',
                    'defaults' => array(
                        'controller' => 'Category\Controller\AdminCategoryImage',
                        'action' => 'sort',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'categoryId' => '[0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            )
        )),
    'events' => array(
        'Category\Service\Grid\AdminCategory' => 'Category\Service\Grid\AdminCategory',
        'Category\Service\Tab\AdminCategoryAddTab' => 'Category\Service\Tab\AdminCategoryAddTab',
        'Category\Service\Link\AdminCategoryLink' => 'Category\Service\Link\AdminCategoryLink',
        'Category\Service\Form\AdminCategory' => 'Category\Service\Form\AdminCategory',
        'Category\Service\NestedSorting\AdminNestedSortingCategory' => 'Category\Service\NestedSorting\AdminNestedSortingCategory',
        'Category\Service\Grid\AdminCategoryImage' => 'Category\Service\Grid\AdminCategoryImage',
        'Category\Service\Form\AdminCategoryImage' => 'Category\Service\Form\AdminCategoryImage',
        'Category\Service\Tab\AdminCategoryImageTab' => 'Category\Service\Tab\AdminCategoryImageTab',
        'Category\Service\Link\AdminCategoryImageLink' => 'Category\Service\Link\AdminCategoryImageLink',
        'Category\Service\NestedSorting\AdminNestedSortingCategoryImage' => 'Category\Service\NestedSorting\AdminNestedSortingCategoryImage',
        'Category\Service\Content\AdminDashboardTopCategory' => 'Category\Service\Content\AdminDashboardTopCategory',
        'Category\Service\Link\AdminCategoryView' => 'Category\Service\Link\AdminCategoryView',
        'Category\Service\Navigation\AdminCategoryNavigation' => 'Category\Service\Navigation\AdminCategoryNavigation',
        'Category\Service\Navigation\CategoryMenu' => 'Category\Service\Navigation\CategoryMenu',
        'Category\Service\Block\CategorySideMenu' => 'Category\Service\Block\CategorySideMenu',
        'Category\Service\Block\AdminDashboardSimpleCategoryStat' => 'Category\Service\Block\AdminDashboardSimpleCategoryStat',
        'Category\Service\Form\AdminCategoryConfig' => 'Category\Service\Form\AdminCategoryConfig',
        'Category\Service\Tab\AdminCategoryConfig' =>  'Category\Service\Tab\AdminCategoryConfig',
    ),
    'service_manager' => array(
        'invokables' => array(
            'Category\Service\Grid\AdminCategory' => 'Category\Service\Grid\AdminCategory',
            'Category\Form\AdminCategory\CategoryForm' => 'Category\Form\AdminCategory\CategoryForm',
            'Category\Form\AdminCategory\CategoryFilter' => 'Category\Form\AdminCategory\CategoryFilter',
            'Category\Form\AdminCategory\CategorySearchForm' => 'Category\Form\AdminCategory\CategorySearchForm',
            'Category\Service\Form\AdminCategory' => 'Category\Service\Form\AdminCategory',
            'Category\Service\Tab\AdminCategoryAddTab' => 'Category\Service\Tab\AdminCategoryAddTab',
            'Category\Service\Link\AdminCategoryLink' => 'Category\Service\Link\AdminCategoryLink',
            'Category\Service\NestedSorting\AdminNestedSortingCategory' => 'Category\Service\NestedSorting\AdminNestedSortingCategory',
            'Category\Service\Grid\AdminCategoryImage' => 'Category\Service\Grid\AdminCategoryImage',
            'Category\Service\Form\AdminCategoryImage' => 'Category\Service\Form\AdminCategoryImage',
            'Category\Form\AdminCategoryImage\CategoryImageForm' => 'Category\Form\AdminCategoryImage\CategoryImageForm',
            'Category\Form\AdminCategoryImage\CategoryImageFilter' => 'Category\Form\AdminCategoryImage\CategoryImageFilter',
            'Category\Service\Tab\AdminCategoryImageTab' => 'Category\Service\Tab\AdminCategoryImageTab',
            'Category\Service\Link\AdminCategoryImageLink' => 'Category\Service\Link\AdminCategoryImageLink',
            'Category\Service\NestedSorting\AdminNestedSortingCategoryImage' => 'Category\Service\NestedSorting\AdminNestedSortingCategoryImage',
            'Category\Service\Content\AdminDashboardTopCategory' => 'Category\Service\Content\AdminDashboardTopCategory',
            'Category\Service\Link\AdminCategoryView' => 'Category\Service\Link\AdminCategoryView',
            'Category\Service\Navigation\AdminCategoryNavigation' => 'Category\Service\Navigation\AdminCategoryNavigation',
            'Category\Service\Navigation\CategoryMenu' => 'Category\Service\Navigation\CategoryMenu',
            'Category\Service\Block\CategorySideMenu' => 'Category\Service\Block\CategorySideMenu',
            'Category\Service\Block\AdminDashboardSimpleCategoryStat' => 'Category\Service\Block\AdminDashboardSimpleCategoryStat',
            'Category\Form\AdminCategoryConfig\CategoryConfigForm' => 'Category\Form\AdminCategoryConfig\CategoryConfigForm',
            'Category\Form\AdminCategoryConfig\CategoryConfigFilter' => 'Category\Form\AdminCategoryConfig\CategoryConfigFilter',
            'Category\Service\Form\AdminCategoryConfig' => 'Category\Service\Form\AdminCategoryConfig',
             'Category\Service\Tab\AdminCategoryConfig' =>  'Category\Service\Tab\AdminCategoryConfig'
        ),
        'factories' => array(
            'Category\Form\AdminCategory\CategoryFormFactory' => 'Category\Form\AdminCategory\CategoryFormFactory',
            'Category\Form\AdminCategoryImage\CategoryImageFormFactory' => 'Category\Form\AdminCategoryImage\CategoryImageFormFactory',
            'Category\Form\AdminCategoryConfig\CategoryConfigFormFactory' => 'Category\Form\AdminCategoryConfig\CategoryConfigFormFactory'
        )
    ),
    'view_manager' => array('template_path_stack' => array('Category' => __DIR__ . '/../view')),
    'doctrine' => array('driver' => array(
            'Category_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Category/Entity')
            ),
            'orm_default' => array('drivers' => array('Category\Entity' => 'Category_driver'))
        )),
    'asset_manager' => array('resolver_configs' => array('paths' => array('Category' => __DIR__ . '/../public')))
);
