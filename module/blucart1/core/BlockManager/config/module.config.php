<?php

return array(
    'router' => array(
        'routes' => array(
            'block' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/block',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Index',
                        'action' => 'index'
                    )
                )
            ),
            'block-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/block/add[/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Index',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'block-load' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/block/load[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Index',
                        'action' => 'load',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'block-get' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/block/get[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Index',
                        'action' => 'get',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'block-update' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/block/update[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Index',
                        'action' => 'update',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'save-block-region' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save/block/region[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Index',
                        'action' => 'save-block-region',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'block-remove' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/block/remove[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Index',
                        'action' => 'remove',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            //-- admin block
            'admin-block' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/block',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Admin',
                        'action' => 'index'
                    )
                )
            ),
            'admin-block-add' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/block/add[/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Admin',
                        'action' => 'add',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-block-load' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/block/load[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Admin',
                        'action' => 'load',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-block-get' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/block/get[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Admin',
                        'action' => 'get',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-block-update' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/block/update[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Admin',
                        'action' => 'update',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-save-block-region' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/save/block/region[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Admin',
                        'action' => 'save-block-region',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            ),
            'admin-block-remove' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/admin/block/remove[/block/:block][/ajax/:ajax][/region/:region]',
                    'defaults' => array(
                        'controller' => 'BlockManager\Controller\Admin',
                        'action' => 'remove',
                        'ajax' => 'true'
                    ),
                    'constraints' => array(
                        'block' => '[a-zA-Z0-9_]+',
                        'region' => '[a-zA-Z0-9]+',
                        'ajax' => 'true|false'
                    )
                )
            )
        )
    ),
    'events' => array(
        'BlockManager\Service\Block\TestBlock' => 'BlockManager\Service\Block\TestBlock',
        'BlockManager\Service\Form\BlockManagerRegion' => 'BlockManager\Service\Form\BlockManagerRegion',
        'BlockManager\Service\Form\BlockManagerBlockRegion' => 'BlockManager\Service\Form\BlockManagerBlockRegion'
    ),
    'service_manager' => array(
        'invokables' => array(
            'BlockManager\Form\BlockManagerRegion\BlockManagerRegionForm' => 'BlockManager\Form\BlockManagerRegion\BlockManagerRegionForm',
            'BlockManager\Form\BlockManagerRegion\BlockManagerRegionFilter' => 'BlockManager\Form\BlockManagerRegion\BlockManagerRegionFilter',
            'BlockManager\Service\Block\TestBlock' => 'BlockManager\Service\Block\TestBlock',
            'BlockManager\Service\Form\BlockManagerRegion' => 'BlockManager\Service\Form\BlockManagerRegion',
            'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionForm' => 'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionForm',
            'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionFilter' => 'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionFilter',
            'BlockManager\Service\Form\BlockManagerBlockRegion' => 'BlockManager\Service\Form\BlockManagerBlockRegion'
        ),
        'factories' => array(
            'BlockManager\Form\BlockManagerRegion\BlockManagerRegionFactory' => 'BlockManager\Form\BlockManagerRegion\BlockManagerRegionFactory',
            'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionFactory' => 'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionFactory'
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'BlockManager\Controller\Index' => 'BlockManager\Controller\IndexController',
            'BlockManager\Controller\Admin' => 'BlockManager\Controller\AdminController'
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'BlockManager' => __DIR__ . '/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'BlockManager_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/BlockManager/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'BlockManager\Entity' => 'BlockManager_driver'
                )
            )
        )
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'BlockManager' => __DIR__ . '/../public'
            )
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'block' => 'BlockManager\View\Helper\Block'
        )
    )
);
