<?php

namespace BlockManager\View\Helper;

use BlockManager\Api\BlockApi;
use BlockManager\Option\Block\BlockContainer;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\View\Helper\AbstractHelper;

class Block extends AbstractHelper implements ServiceLocatorAwareInterface, EventManagerAwareInterface {

    protected $_services;
    protected $_blockContainer;
    protected $_events;
    protected $_blockBasePath = 'block';

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->_services = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->_services->getServiceLocator();
    }

    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    public function getEventManager() {
        if (null === $this->_events) {
            $this->setEventManager(new EventManager());
        }
        return $this->_events;
    }

    public function __invoke($name, $params = array()) {
        $this->_blockContainer = null;
        $this->_blockContainer = BlockApi::getBlocks($name, $params);

        return $this;
    }

    public function render($template = 'block/block') {
        $view = $this->getView();



        return $view->render($this->getBlockTemplate($template), array(
                    'blockContainer' => $this->getBlockContainer()
        ));
    }

    /**
     *
     * @return \BlockManager\Option\Block\BlockContainer
     */
    public function getBlockContainer() {
        if (!$this->_blockContainer) {
            $this->_blockContainer = new BlockContainer ();
        }
        return $this->_blockContainer;
    }

    /**
     *
     * @param unknown_type $blockContainer        	
     * @return \Common\View\Helper\Block
     */
    public function setBlockContainer($blockContainer) {
        $this->_blockContainer = $blockContainer;
        return $this;
    }

    public function getArrayTemplates() {
        return array(
            $this->_blockBasePath . '/blockRegion-{region}',
           
        );
    }

    public function getBlockTemplate($template) {
        $region = $this->getBlockContainer()->getRegionName();
        $templates = $this->getArrayTemplates();

        $routeName = \Core\Functions::getMatchedRouteName();
        if($routeName === 'block' || $routeName === 'admin-block'){
            return $template;
        }
        $sm = \Core\Functions::getServiceLocator();
        $resolver = $sm->get('Zend\View\Resolver\TemplatePathStack');

        foreach ($templates as $tmpl) {
         
            $tmpl = str_replace('{region}', $region, $tmpl);
            if ($resolver->resolve($tmpl)) {
                return $tmpl;
            }
        }


        return $template;
    }

}
