<?php

namespace BlockManager\Controller;

use BlockManager\Api\BlockApi;
use Core\Functions;
use Zend\View\Model\JsonModel;
use Common\MVC\Controller\AbstractFrontController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Common\MVC\Controller\AbstractAdminController;

class IndexController extends AbstractFrontController {

    public function indexAction() {

        if (!\User\Api\UserApi::isAdminUser()) {
            return $this->accessDenied();
        }
    }

    public function adminAction() {
        
    }

    public function getFormFactory() {
        return $this->getServiceLocator()->get('BlockManager\Form\BlockManagerRegion\BlockManagerRegionFactory');
    }

    public function getSaveRedirector() {
         if (!\User\Api\UserApi::isAdminUser()) {
            return $this->accessDenied();
        }
        $form = $this->getFormFactory();
        $regionName = Functions::fromRoute('region');
        return $this->redirect()->toRoute('block-add', array(
                    'region' => $regionName
                ));
    }

    public function getAction() {
         if (!\User\Api\UserApi::isAdminUser()) {
            return $this->accessDenied();
        }
        
    }

    public function removeAction() {
         if (!\User\Api\UserApi::isAdminUser()) {
            return $this->accessDenied();
        }
        $regionName = Functions::fromRoute('region');
        $block = Functions::fromRoute('block');
        BlockApi::deleteByBlockNameAndRegion($block, $regionName);
        return new JsonModel(array());
    }

    public function updateAction() {
         if (!\User\Api\UserApi::isAdminUser()) {
            return $this->accessDenied();
        }
        $regionName = Functions::fromRoute('region');
        $blocks = Functions::fromPost('blocks');

        $count = count($blocks);
        foreach ($blocks as $value) {
            BlockApi::updateByBlockNameAndRegion($value, $regionName, $count);
            $count --;
        }
        return new JsonModel(array());
    }

    public function loadAction() {
         if (!\User\Api\UserApi::isAdminUser()) {
            return $this->accessDenied();
        }
        $regionName = Functions::fromRoute('region');
        $uri = Functions::fromPost('uri');
        $blockContainer = BlockApi::getBlocks($regionName);
        $blockContainer->setUri($uri);
        $array = array();
        $items = $blockContainer->getItems();
        foreach ($items as $item) {
            $temp = array();
            $temp ['name'] = $item->getId();

            $renderer = Functions::getServiceLocator()->get('viewrenderer');
            $block = $renderer->render('block/block-item', array(
                'item' => $item,
                'blockContainer' => $blockContainer
                    ));
            $temp ['block'] = $block;
            $array [$item->getId()] = $temp;
        }
        return new JsonModel($array);
    }

    public function getBlockRegionForm() {
        
        return $this->getServiceLocator()->get('BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionFactory');
    }

    public function saveBlockRegionAction() {
         if (!\User\Api\UserApi::isAdminUser()) {
            return $this->accessDenied();
        }
        $form = $this->getBlockRegionForm();

        if ($this->getRequest()->isPost()) {
            $form->setData($this->params()->fromPost());
            if ($form->isValid()) {
                $form->save();

                $this->addSuccessMessage();
                $form = $this->getFormFactory();
                $blockName = Functions::fromRoute('block');
                $regionName = Functions::fromRoute('region');
                return $this->redirect()->toRoute('save-block-region', array(
                            'region' => $regionName,
                            'block' => $blockName
                        ));
            } else {
                $this->notValid();
            }
        } else {
            $form->getRecord();
        }
        $viewModel = new ViewModel(array(
            'form' => $form
                ));

        return $viewModel;
    }

}
