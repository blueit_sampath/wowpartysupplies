<?php

namespace BlockManager\Controller;

use BlockManager\Api\BlockApi;
use Core\Functions;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Common\MVC\Controller\AbstractAdminController;

class AdminController extends AbstractAdminController {
	
    public function indexAction(){
        
    }
   
    public function getFormFactory() {
		return $this->getServiceLocator ()->get ( 'BlockManager\Form\BlockManagerRegion\BlockManagerRegionFactory' );
	}
	public function getSaveRedirector() {
		$form = $this->getFormFactory ();
		$regionName = Functions::fromRoute ( 'region' );
		return $this->redirect ()->toRoute ( 'admin-block-add', array (
				'region' => $regionName 
		) );
	}
	public function getAction() {
	}
	public function removeAction() {
		$regionName = Functions::fromRoute ( 'region' );
		$block = Functions::fromRoute ( 'block' );
		BlockApi::deleteByBlockNameAndRegion ( $block, $regionName );
		return new JsonModel ( array () );
	}
	public function updateAction() {
		$regionName = Functions::fromRoute ( 'region' );
		$blocks = Functions::fromPost ( 'blocks' );
		
		$count = count ( $blocks );
		foreach ( $blocks as $value ) {
			BlockApi::updateByBlockNameAndRegion ( $value , $regionName, $count );
			$count --;
		}
		return new JsonModel ( array () );
	}
	public function loadAction() {
		$regionName = Functions::fromRoute ( 'region' );
		$uri = Functions::fromPost ( 'uri' );
		$blockContainer = BlockApi::getBlocks ( $regionName );
		$blockContainer->setUri ( $uri );
		$array = array ();
		$items = $blockContainer->getItems ();
		foreach ( $items as $item ) {
			$temp = array ();
			$temp ['name'] = $item->getId ();
			
			$renderer = Functions::getServiceLocator ()->get ( 'viewrenderer' );
			$block = $renderer->render ( 'block/admin-block-item', array (
					'item' => $item,
					'blockContainer' => $blockContainer 
			) );
			$temp ['block'] = $block;
			$array [$item->getId ()] = $temp;
		}
		return new JsonModel ( $array );
	}
	public function getBlockRegionForm() {
		return $this->getServiceLocator ()->get ( 'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionFactory' );
	}
	public function saveBlockRegionAction() {
		$form = $this->getBlockRegionForm ();
		
		if ($this->getRequest ()->isPost ()) {
			$form->setData ( $this->params ()->fromPost () );
			if ($form->isValid ()) {
				$form->save ();
				
				$this->addSuccessMessage ();
				$form = $this->getFormFactory ();
				$blockName = Functions::fromRoute ( 'block' );
				$regionName = Functions::fromRoute ( 'region' );
				return $this->redirect ()->toRoute ( 'admin-save-block-region', array (
						'region' => $regionName,
						'block' => $blockName 
				) );
			} else {
				$this->notValid ();
			}
		} else {
			$form->getRecord ();
		}
		$viewModel = new ViewModel ( array (
				'form' => $form 
		) );
		
		return $viewModel;
	}
}