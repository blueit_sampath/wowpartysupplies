<?php

namespace BlockManager\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BlockManager
 *
 * @ORM\Table(name="block")
 * @ORM\Entity
 */
class Block {

    /**
     *
     * @var integer @ORM\Column(name="id", type="integer", nullable=false)
     *      @ORM\Id
     *      @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var string @ORM\Column(name="name", type="string",
     *      length=255, nullable=true)
     */
    private $name;

    /**
     *
     * @var string @ORM\Column(name="title", type="string",
     *      length=255, nullable=true)
     */
    private $title;

    /**
     *
     * @var string @ORM\Column(name="region", type="string",
     *      length=255, nullable=true)
     */
    private $region;

    /**
     *
     * @var integer @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     *
     * @var string @ORM\Column(name="include_url", type="text",
     *      nullable=true)
     */
    private $includeUrl;

    /**
     *
     * @var string @ORM\Column(name="exclude_url", type="text",
     *      nullable=true)
     */
    private $excludeUrl;

    /**
     *
     * @var string @ORM\Column(name="classes", type="string",length=255, nullable=true)
     */
    private $classes;

    /**
     *
     * @var boolean @ORM\Column(name="display_title", type="boolean", nullable=true)
     */
    private $displayTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="display_to", type="integer",  nullable=true)
     */
    private $displayTo;

    /**
     *
     * @var \DateTime @ORM\Column(name="created_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="create")
     */
    private $createdDate;

    /**
     *
     * @var \DateTime @ORM\Column(name="updated_date", type="datetime",
     *      nullable=true)
     *      @Gedmo\Timestampable(on="update")
     */
    private $updatedDate;

    public function __get($property) {
        if (\method_exists($this, "get" . \ucfirst($property))) {
            $method_name = "get" . \ucfirst($property);
            return $this->$method_name();
        } else {

            if (is_object($this->$property)) {
                // return $this->$property->id;
            }
            return $this->$property;
        }
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property            
     * @param mixed $value            
     *
     */
    public function __set($property, $value) {
        if (\method_exists($this, "set" . \ucfirst($property))) {
            $method_name = "set" . \ucfirst($property);

            $this->$method_name($value);
        } else
            $this->$property = $value;
    }

}
