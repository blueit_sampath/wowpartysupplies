<?php
namespace BlockManager\Form\BlockManagerRegion;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  BlockManagerRegionFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'blocks',
				'required' => false,
				'filters' => array (
						array (
								'name' => 'StringTrim'
						)
				)
		) );
		
	}
} 