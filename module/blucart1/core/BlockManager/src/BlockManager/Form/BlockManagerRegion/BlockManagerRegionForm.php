<?php

namespace BlockManager\Form\BlockManagerRegion;

use BlockManager\Api\BlockApi;
use Zend\Form\Element\MultiCheckbox;
use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;

class BlockManagerRegionForm extends Form 

{
	public function init() {
		$this->add ( array (
				'type' => 'Zend\Form\Element\MultiCheckbox',
				'name' => 'blocks',
				'options' => array (
						'label' => 'Blocks',
						'attributes' => array (
								'inline' => true 
						),
						'description' => 'Description',
						'value_options' => $this->getBlockNames () 
				)
				 
		) );
		
		$this->add ( array (
				'name' => 'submit',
				'attributes' => array (
						'type' => 'submit',
						'value' => 'Submit' 
				) 
		), array (
				'priority' => - 100 
		) );
	}
	public function getBlockNames() {
		$name = Functions::fromRoute ( 'regionName' );
		$array = BlockApi::getBlockNames ( $name );
                asort($array);
		return $array;
	}
}