<?php
namespace BlockManager\Form\BlockManagerRegion;

use Common\Form\Option\AbstractFormFactory;

class BlockManagerRegionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'BlockManager\Form\BlockManagerRegion\BlockManagerRegionForm' );
		$form->setName ( 'blockManagerRegion' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'BlockManager\Form\BlockManagerRegion\BlockManagerRegionFilter' );
	}
}
