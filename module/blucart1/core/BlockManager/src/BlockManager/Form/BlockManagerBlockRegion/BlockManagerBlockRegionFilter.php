<?php
namespace BlockManager\Form\BlockManagerBlockRegion;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class  BlockManagerBlockRegionFilter extends InputFilter 

{
	protected $inputFilter;
	public function __construct() {
		
		$this->add ( array (
				'name' => 'includeUrl',
				'required' => false,
				
		) );
                $this->add ( array (
				'name' => 'title',
				'required' => false,
				
		) );
		$this->add ( array (
				'name' => 'excludeUrl',
				'required' => false,
		
		) );
		$this->add ( array (
				'name' => 'classes',
				'required' => false,
		
		) );
		$this->add ( array (
				'name' => 'displayTitle',
				'required' => true,
		
		) );
                $this->add ( array (
				'name' => 'displayTo',
				'required' => false,
		
		) );
		
	}
} 