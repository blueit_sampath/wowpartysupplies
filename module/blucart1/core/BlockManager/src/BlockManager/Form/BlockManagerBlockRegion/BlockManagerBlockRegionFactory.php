<?php
namespace BlockManager\Form\BlockManagerBlockRegion;

use Common\Form\Option\AbstractFormFactory;

class BlockManagerBlockRegionFactory extends AbstractFormFactory {
	public function getForm() {
		$form = $this->getServiceLocator ()->get ( 'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionForm' );
		$form->setName ( 'blockManagerBlockRegion' );
		return $form;
	}
	public function getFormFilter() {
		return $this->getServiceLocator ()->get ( 'BlockManager\Form\BlockManagerBlockRegion\BlockManagerBlockRegionFilter' );
	}
}
