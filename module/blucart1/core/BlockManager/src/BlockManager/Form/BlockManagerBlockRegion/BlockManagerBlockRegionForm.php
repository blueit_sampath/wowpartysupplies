<?php

namespace BlockManager\Form\BlockManagerBlockRegion;

use Core\Functions;
use Zend\Form\Element\Select;
use Common\Form\Form;
use Zend\Form\Element\Radio;

class BlockManagerBlockRegionForm extends Form {

    public function init() {

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text'
            )
            ,
            'options' => array(
                'label' => 'Title'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'excludeUrl',
            'attributes' => array(
                'type' => 'textarea'
            )
            ,
            'options' => array(
                'label' => 'Exclude'
            )
                ), array(
            'priority' => 1000
        ));

        $this->add(array(
            'name' => 'includeUrl',
            'attributes' => array(
                'type' => 'textarea'
            )
            ,
            'options' => array(
                'label' => 'Include Urls'
            )
                ), array(
            'priority' => 990
        ));



        $this->add(array(
            'name' => 'classes',
            'attributes' => array(
                'type' => 'text'
            )
            ,
            'options' => array(
                'label' => 'Class Attributes'
            )
                ), array(
            'priority' => 990
        ));

        $radio = new Radio('displayTitle');
        $radio->setLabel('Display Title');
        $radio->setValueOptions(array(
            '1' => 'Enable',
            '0' => 'Disable'
        ));
        $radio->setValue(0);
        $this->add($radio, array(
            'priority' => 980
        ));


        $select = new \Zend\Form\Element\Select('displayTo');
        $select->setLabel('Display To');
        $select->setValueOptions(array(
            \BlockManager\Api\BlockApi::BOTH_USER => 'Both',
            \BlockManager\Api\BlockApi::GUEST_USER => 'Only Guest',
            \BlockManager\Api\BlockApi::LOGGED_IN_USER => 'Only Logged In User',
        ));
        $this->add($select, array(
            'priority' => 970
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit'
            )
                ), array(
            'priority' => - 100
        ));
    }

}
