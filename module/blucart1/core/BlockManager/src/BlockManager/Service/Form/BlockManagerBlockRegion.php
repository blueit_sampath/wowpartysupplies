<?php 
namespace BlockManager\Service\Form;

use BlockManager\Api\BlockApi;

use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class BlockManagerBlockRegion extends AbstractMainFormEvent {
	protected $_columnKeys = array (
				);
	protected $_entity = '\BlockManager\Entity\Block';
	protected $_entityName = 'block';
	public function getFormName() {
		return 'blockManagerBlockRegion';
	}
	public function getPriority() {
		return 1000;
	}
	
	public function getRecord() {
		$form = $this->getForm ();
		$regionName = Functions::fromRoute ( 'region' );
		$blockName = Functions::fromRoute ( 'block' );
	
		$array = array ();
		$result = BlockApi::getBlockByNameAndRegion($blockName, $regionName);
		
		if($result){
			$form->get ( 'includeUrl' )->setValue ( $result->includeUrl );
			$form->get ( 'excludeUrl' )->setValue ( $result->excludeUrl );
			$form->get ( 'displayTitle' )->setValue ( $result->displayTitle );
			$form->get ( 'classes' )->setValue ( $result->classes );
                        $form->get ( 'title' )->setValue ( $result->title );
                         $form->get ( 'displayTo' )->setValue ( $result->displayTo );
		}
	
		
	}
	
	public function save() {
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		
		$resultContainer = $this->getFormResultContainer ();
		$regionName = Functions::fromRoute ( 'region' );
		$blockName = Functions::fromRoute ( 'block' );
		$params = $form->getData ();
		
		$result = BlockApi::getBlockByNameAndRegion($blockName, $regionName);
		
		
		if($result){
			$result->includeUrl = $params['includeUrl'];
			$result->excludeUrl = $params['excludeUrl'];
			$result->classes = $params['classes'];
			$result->displayTitle = $params['displayTitle'];
			$result->title = $params['title'];
                        $result->displayTo = $params['displayTo'];
			$em->persist($result);
			$em->flush();
			
		}
		return true;
	}
	
}
