<?php

namespace BlockManager\Service\Form;

use BlockManager\Api\BlockApi;
use Core\Functions;
use Common\Form\Option\AbstractMainFormEvent;

class BlockManagerRegion extends AbstractMainFormEvent {
	protected $_entity = 'BlockManager\Entity\Block';
	protected $_entityName = 'block';
	public function getFormName() {
		return 'blockManagerRegion';
	}
	public function getPriority() {
		return 1000;
	}
	public function getRecord() {
		$form = $this->getForm ();
		$regionName = Functions::fromRoute ( 'region' );
		
		$array = array ();
		$results = BlockApi::getBlockNamesByRegion ( $regionName );
		foreach ( $results as $result ) {
			$array [] = $result->name;
		}
		
		$form->get ( 'blocks' )->setValue ( $array );
	}
	
	public function save() {
		$resultContainer = $this->getFormResultContainer ();
		$regionName = Functions::fromRoute ( 'region' );
		$form = $this->getForm ();
		$params = $form->getData ();
		$em = $this->getEntityManager ();
		if (! ($params ['blocks']) || ! count ( $params ['blocks'] )) {
			$dql = ' delete from ' . $this->_entity . ' u where u.region = \'' . $regionName . "'";
			$em->createQuery ( $dql )->execute ();
			$resultContainer->add ( 'blocks', null );
		} else {
			$results = BlockApi::getBlockNamesByRegion ( $regionName );
			$blockIds = array ();
			if ($results) {
				foreach ( $results as $result ) {
					$blockIds [] = $result->name;
				}
			}
			$temp = array_diff ( $params ['blocks'], $blockIds );
			if ($temp && count ( 'temp' )) {
				$temp1 = array ();
				foreach ( $temp as $value ) {
					$entity = new $this->_entity ();
					$entity->name = $value;
					$entity->region = $regionName;
					$em->persist ( $entity );
				}
			}
			$em->flush ();
			$temp = array_diff ( $blockIds, $params ['blocks'] );
			if ($temp && count ( 'temp' )) {
				$dql = ' delete from ' . $this->_entity . ' u where u.name in (\'' . implode ( '\',\'', $temp ) . '\')';
				$em->createQuery ( $dql )->execute ();
			}
		}
		return true;
	}
}
