<?php 
namespace BlockManager\Service\Block;

use Core\Functions;
use BlockManager\Option\Block\AbstractBlockEvent;

class TestBlock extends AbstractBlockEvent {

	protected $_blockTemplate = 'test';

	public function getBlockName() {
		return 'test';
	}
	public function getPriority() {
		return 1000;
	}
	
}

