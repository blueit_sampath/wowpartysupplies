<?php

namespace BlockManager\Api;

use BlockManager\Option\Block\BlockContainer;
use Core\Functions;
use Common\Api\Api;

class BlockApi extends Api {

    protected static $_entity = 'BlockManager\Entity\Block';

    const GUEST_USER = 2;
    const LOGGED_IN_USER = 1;
    const BOTH_USER = 0;

    public static function getBlockNamesByRegion($regionName) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entity)->findBy(array(
                    'region' => $regionName
                        ), array(
                    'weight' => 'desc'
                ));
    }

    public static function getBlockByNameAndRegion($blockName, $regionName) {
        $em = static::getEntityManager();
        return $em->getRepository(static::$_entity)->findOneBy(array(
                    'region' => $regionName,
                    'name' => $blockName
                ));
    }

    public static function updateByBlockNameAndRegion($blockName, $regionName, $weight) {
        $em = static::getEntityManager();

        $result = static::getBlockByNameAndRegion($blockName, $regionName);
        if ($result) {
            $result->weight = $weight;
            $em->persist($result);
            $em->flush();
        }
        return $result;
    }

    public static function deleteByBlockNameAndRegion($blockName, $regionName) {
        $em = static::getEntityManager();
        $result = static::getBlockByNameAndRegion($blockName, $regionName);
        if ($result) {
            $em->remove($result);
            $em->flush();
        }
        return true;
    }

    public static function getBlockNames($name = '') {
        $params = array();
        $blockContainer = new BlockContainer ();

        $params ['blockContainer'] = $blockContainer;
        $blockContainer->setRegionName($name);
        $sm = Functions::getServiceLocator();
        $sm->get('Application')->getEventManager()->trigger('block-render-name', '*', $params);
        $array = array();
        $items = $blockContainer->getItems();

        foreach ($items as $item) {
            $array [$item->getId()] = $item->getTitle();
        }
        return $array;
    }

    public static function getBlocks($regionName = '', $params = array()) {
        $blockContainer = new BlockContainer ();
        $blockContainer->setParams($params);
        $params ['blockContainer'] = $blockContainer;
        $blockContainer->setRegionName($regionName);
        $results = static::getBlockNamesByRegion($regionName);

        $sm = Functions::getServiceLocator();

        foreach ($results as $result) {
            $sm->get('Application')->getEventManager()->trigger($result->name . '-block-render', '*', $params);
            if ($item = $blockContainer->get($result->name)) {
                $item->setWeight($result->weight);
            }
        }

        return $blockContainer;
    }

}
