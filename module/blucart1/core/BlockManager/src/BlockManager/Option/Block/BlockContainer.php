<?php

namespace BlockManager\Option\Block;

use BlockManager\Api\BlockApi;
use Core\Functions;
use Core\Item\Container\ItemContainer;

class BlockContainer extends ItemContainer {

    protected $_params = array();
    protected $_regionName = "";
    protected $_uri = "";

    /**
     *
     * @param string $uniqueKey        	
     * @param string $description        	
     * @param string $block        	
     * @param integer $weight        	
     * @return BlockItem
     */
    public function add($uniqueKey, $title = '', $block = '', $weight = null, $description = '') {
        $item = new BlockItem($uniqueKey);
        $item->setTitle($title);
        $item->setDescription($description);
        $item->setBlock($block);
        $item->setWeight($weight);
        $this->addItem($item);
        return $item;
    }

    /**
     *
     * @param string $id        	
     * @return DetailItem
     */
    public function get($id) {
        return $this->getItem($id);
    }

    /**
     *
     * @return string $_params
     */
    public function getParams() {
        return $this->_params;
    }

    /**
     *
     * @param multitype: $_params        	
     * @param
     *        	class_name
     */
    public function setParams($_params) {
        $this->_params = $_params;
        return $this;
    }

    public function getParam($key) {
        if (isset($this->_params [$key])) {
            return $this->_params [$key];
        }
        return false;
    }

    public function addParam($key, $value) {
        $this->_params [$key] = $value;
        return false;
    }

    public function removeParam($key) {
        if (isset($this->_params [$key])) {
            unset($this->_params [$key]);
        }
        return true;
    }

    public function clearParams() {
        $this->_params = array();
        return true;
    }

    /**
     *
     * @return string $_regionName
     */
    public function getRegionName() {
        return $this->_regionName;
    }

    /**
     *
     * @param string $_regionName        	
     * @param
     *        	class_name
     */
    public function setRegionName($_regionName) {
        $this->_regionName = $_regionName;
        return $this;
    }

    public function getItems($raw = false) {
        $items = parent::getItems();
        if ($raw) {
            return $items;
        }
        $temp = $items;
        foreach ($items as $key => $item) {

            $result = BlockApi::getBlockByNameAndRegion($item->getId(), $this->getRegionName());
            if (!$result) {
                continue;
            }

            if (!$this->checkUrl($item->getId(), $result->includeUrl, $result->excludeUrl, $result)) {
                unset($temp [$key]);
            }

            if (!$this->checkUser($result)) {
                unset($temp [$key]);
            }
        }

        return $temp;
    }

    protected function checkUser($result) {
        $routeName = Functions::getMatchedRouteName();
        if ($routeName == 'block' || $routeName == 'admin-block') {
            return true;
        }
        if ($result->displayTo) {

            $user = \User\Api\UserApi::getLoggedInUser();
            if ($result->displayTo == BlockApi::GUEST_USER && $user) {
                return false;
            }
            if ($result->displayTo == BlockApi::LOGGED_IN_USER && !$user) {
                return false;
            }
        }
        return true;
    }

    protected function checkUrl($blockName, $includeUrls, $excludeUrls) {
        $routeName = Functions::getMatchedRouteName();
        if ($routeName == 'block' || $routeName == 'admin-block') {
            return true;
        }

        $uri = $this->getUri();
        $basePath = Functions::getViewHelperManager()->get('basePath');
        $display = true;


        if ($excludeUrls) {
            $array = explode("\r\n", $excludeUrls);
            foreach ($array as $value) {
                $value = $basePath($value);
                if (fnmatch($value, $uri)) {
                    $display = false;
                }
            }
        }
        if ($includeUrls) {
            $display = false;
            $array = explode("\r\n", $includeUrls);
            foreach ($array as $value) {
                $value = $basePath($value);
                if (fnmatch($value, $uri)) {
                    $display = true;
                }
            }
        }
        return $display;
    }

    /**
     * @return string $_uri
     */
    public function getUri() {
        if (!$this->_uri) {
            $sm = Functions::getServiceLocator();
            $request = $sm->get('Request');
            return $request->getUri()->getPath();
        }
        return $this->_uri;
    }

    /**
     * @param string $_uri
     * @param class_name
     */
    public function setUri($_uri) {
        $this->_uri = $_uri;
        return $this;
    }

}
