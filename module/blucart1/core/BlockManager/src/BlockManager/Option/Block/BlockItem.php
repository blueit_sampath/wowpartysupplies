<?php

namespace BlockManager\Option\Block;

use Core\Item\Item;

class BlockItem extends Item {
	protected $_description = '';
	protected $_title = '';
	protected $_block = '';
	
	/**
	 *
	 * @return the $_description
	 */
	public function getDescription() {
		return $this->_description;
	}
	
	/**
	 *
	 * @return the $_block
	 */
	public function getBlock() {
		return $this->_block;
	}
	
	/**
	 *
	 * @param string $_description        	
	 */
	public function setDescription($_description) {
		$this->_description = $_description;
	}
	
	/**
	 *
	 * @param string $_block        	
	 */
	public function setBlock($_block) {
		$this->_block = $_block;
	}
	
	/**
	 * @return string $_title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * @param string $_title
	 * @param class_name
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
		return $this;
	}

}
