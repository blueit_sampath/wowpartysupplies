<?php

namespace BlockManager\Option\Block;

use Core\Functions;
use Core\Event\Core\AbstractEvent;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventCollection;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventInterface;

abstract class AbstractBlockEvent extends AbstractEvent {
	protected $_blockContainer;
	protected $_blockTemplate;
	protected $_blockBasePath = 'block';
	abstract public function getBlockName();
	public function getTitle() {
		return $this->getBlockName ();
	}
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$priority = $this->getPriority ();
		$sharedEventManager = $events->getSharedManager ();
		$this->_listeners [] = $sharedEventManager->attach ( '*', 'block-render-name', array (
				$this,
				'preRenderName' 
		), $priority );
		$this->_listeners [] = $sharedEventManager->attach ( '*', $this->getBlockName () . '-block-render', array (
				$this,
				'preRender' 
		), $priority );
		
		return $this;
	}
	public function preRenderName($e) {
		$blockContainer = $e->getParam ( 'blockContainer' );
		
		$this->setBlockContainer ( $blockContainer );
		return $this->renderName ( $e );
	}
	public function renderName($e) {
		$blockContainer = $this->getBlockContainer ();
		$item = $blockContainer->add ( $this->getBlockName (), $this->getTitle () );
		return $item;
	}
	public function preRender($e) {
		$blockContainer = $e->getParam ( 'blockContainer' );
		
		$this->setBlockContainer ( $blockContainer );
		return $this->render ( $e );
	}
	
	public function getArrayTemplates(){
		return array(
				$this->_blockBasePath .'/{template}-{region}-{id}',
				$this->_blockBasePath .'/{template}-{id}',
				$this->_blockBasePath .'/{template}-{region}',
				$this->_blockBasePath .'/{template}',
		);
	}
	public function getBlockTemplate() {
		$region = $this->getBlockContainer()->getRegionName();
		$templates = $this->getArrayTemplates();
		
		$sm = Functions::getServiceLocator();
		$resolver = $sm->get ( 'Zend\View\Resolver\TemplatePathStack' );
		
		foreach($templates as $tmpl){
			$tmpl = str_replace('{template}', $this->_blockTemplate, $tmpl);
			$tmpl = str_replace('{region}', $region, $tmpl);
			$tmpl = str_replace('{id}', $this->getBlockName(), $tmpl);
			if ($resolver->resolve ( $tmpl )) {
				return $tmpl;
			}
			
			
		}
		
		
		return $this->_blockBasePath . '/' . $this->_blockTemplate;
	}
	public function render($e) {
		if ($this->_blockTemplate) {
			$blockContainer = $this->getBlockContainer ();
			
			$renderer = Functions::getServiceLocator ()->get ( 'viewrenderer' );
			$item = $blockContainer->add ( $this->getBlockName (), $this->getTitle () );
			$renderer->render ( $this->getBlockTemplate (), array (
					'item' => $item,
					'blockContainer' => $blockContainer 
			) );
		}
	}
	/**
	 *
	 * @return BlockContainer $_blockContainer
	 */
	public function getBlockContainer() {
		return $this->_blockContainer;
	}
	
	/**
	 *
	 * @param BlockContainer $_blockContainer        	
	 *
	 */
	public function setBlockContainer($_blockContainer) {
		$this->_blockContainer = $_blockContainer;
		return $this;
	}
}
