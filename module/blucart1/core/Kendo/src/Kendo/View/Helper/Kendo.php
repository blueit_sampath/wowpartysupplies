<?php

namespace Kendo\View\Helper;

use Kendo\Lib\Core\Serializable;
use Kendo\Option\Grid\JavascriptFunction;
use Kendo\Option\Grid\Grid;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

class Kendo extends AbstractHelper implements ServiceLocatorAwareInterface {
	protected $services;
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->services->getServiceLocator ();
	}
	public function __invoke(Serializable $object = null) {
		if(!$object){
			return $this;
		}
		$view = $this->getView ();
		$this->attachScripts ();
		$array = $object->serialize ();
		
		
		
		$string = json_encode ( $array );
		while ( $parsed = $this->getStringBetween ( $string, "<kendo_script_value>", "<\/kendo_script_value>" ) ) {
			$replaceString = trim ( str_replace ( array (
					'\\t',
					'\\n',
					'\\r',
					'\\0',
					'\\x0B' 
			), "", $parsed ) );
			$replaceString = stripslashes ( trim ( preg_replace ( '/\s\s+/', ' ', $replaceString ) ) );
			$string = str_replace ( '"<kendo_script_value>' . $parsed . '<\/kendo_script_value>"', $replaceString, $string );
		}
		
		return $string;
	}
	
	public function attachScripts() {
		$view = $this->getView ();
		$config = $this->getConfig ();
		$request  = $this->getServiceLocator()->get('Request');
		
		if (!$request->isXmlHttpRequest() && isset ( $config ['kendo_options'] )) {
			
			if (isset ( $config ['kendo_options'] ['include_js'] ) && $config ['kendo_options'] ['include_js']) {
				$view->headScript ()->appendFile ( $view->basePath ( 'assets/kendo/js/kendo.all.min.js' ) );
			}
			
			if (isset ( $config ['kendo_options'] ['locale'] ) && $config ['kendo_options'] ['locale']) {
				$view->headScript ()->appendFile ( $view->basePath ( 'assets/kendo/js/cultures/kendo.culture.'.$config ['kendo_options'] ['locale'].'.min.js' ) );
				$view->headScript ()->appendScript ('kendo.culture("'.$config ['kendo_options'] ['locale'].'");' );
			}
			
			$string = '';
			if (isset ( $config ['kendo_options'] ['include_css'] ) && $config ['kendo_options'] ['include_css']) {
				
				$view->headLink ()->appendStylesheet ( $view->basePath ( 'assets/kendo/css/kendo.common.min.css' ) );
				$view->headLink ()->appendStylesheet ( $view->basePath ( 'assets/kendo/css/kendo.rtl.min.css' ) );
				$view->headLink ()->appendStylesheet ( $view->basePath ( 'assets/kendo/css/kendo.default.min.css' ) );
				$view->headLink ()->appendStylesheet ( $view->basePath ( 'assets/kendo/css/kendo.dataviz.min.css' ) );
				$view->headLink ()->appendStylesheet ( $view->basePath ( 'assets/kendo/css/kendo.dataviz.default.min.css' ) );
				$view->headLink ()->appendStylesheet ( $view->basePath ( 'assets/kendo/css/kendo.dataviz.default.min.css' ) );
			}
		}
	}
	public function getConfig() {
		return $this->getServiceLocator ()->get ( 'Config' );
	}
	public function getStringBetween($string, $start, $end) {
		$string = " " . $string;
		$ini = strpos ( $string, $start );
		if ($ini == 0)
			return "";
		$ini += strlen ( $start );
		
		$len = strpos ( $string, $end, $ini ) - $ini;
		if ($len < 0) {
			return false;
		}
		return substr ( $string, $ini, $len );
	}
}
