<?php

namespace Kendo\View\Helper;

use Kendo\Option\Grid\JavascriptFunction;
use Kendo\Option\Grid\Grid;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Helper\AbstractHelper;

class KendoGrid extends Kendo {
	public function __invoke($grid) {
		$view = $this->getView ();
		$gridString = parent::__invoke ($grid);
		$string = $view->render ( 'common/kendo-grid.phtml', array (
				'grid' => $grid,
				'string' => $gridString, 
				'config' => $this->getServiceLocator()->get('Config')  
		) );
		
		return $string;
	}
}
