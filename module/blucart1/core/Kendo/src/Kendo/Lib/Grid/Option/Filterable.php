<?php

namespace Kendo\Lib\Grid\Option;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Filterable extends SerializableContainer {
	protected $_extra;
	protected $_messages;
	protected $_operators;
	public function setMessages($_messages) {
		$this->_messages = $_messages;
	}
	public function addMessage($key, $value) {
		$this->_messages [$key] = $value;
		return $this;
	}
	public function getMessage($key) {
		if (isset ( $this->_messages [$key] )) {
			return $this->_messages [$key];
		}
		return false;
	}
	public function getMessages() {
		return $this->_messages;
	}
	public function removeMessage($key) {
		if (isset ( $this->_messages [$key] )) {
			unset ( $this->_messages [$key] );
			return true;
		}
		return false;
	}
	public function clearMessages() {
		$this->_messages = null;
		return true;
	}
	public function setOperators($_operators) {
		$this->_operators = $_operators;
	}
	public function addOperator($key, $value) {
		$this->_operators [$key] = $value;
		return $this;
	}
	public function getOperator($key) {
		if (isset ( $this->_operators [$key] )) {
			return $this->_operators [$key];
		}
		return false;
	}
	public function getOperators() {
		return $this->_operators;
	}
	public function removeOperator($key) {
		if (isset ( $this->_operators [$key] )) {
			unset ( $this->_operators [$key] );
			return true;
		}
		return false;
	}
	public function clearOperators() {
		$this->_operators = null;
		return true;
	}
	
	
	public function addStringOperator($key, $value) {
		if (isset ( $this->_operators ['string'] )) {
			$this->_operators ['string'] = array();
		}
		$this->_operators ['string'] [$key] = $value;
		return $this;
	}
	public function getStringOperator($key) {
		if (isset ( $this->_operators ['string'] [$key] )) {
			return $this->_operators ['string'] [$key];
		}
		return false;
	}
	public function getStringOperators($key) {
		if (! isset ( $this->_operators ['string'] )) {
			return;
		}
		return $this->_operators ['string'];
	}
	public function removeStringOperator($key) {
		if (isset ( $this->_operators ['string'] [$key] )) {
			unset ( $this->_operators ['string'] [$key] );
			return true;
		}
		return false;
	}
	public function clearStringOperators() {
		if (isset ( $this->_operators ['string'] )) {
			unset ( $this->_operators ['string'] );
		}
		return true;
	}
	
	public function addNumberOperator($key, $value) {
		if (isset ( $this->_operators ['number'] )) {
			$this->_operators ['number'] = array();
		}
		$this->_operators ['number'] [$key] = $value;
		return $this;
	}
	public function getNumberOperator($key) {
		if (isset ( $this->_operators ['number'] [$key] )) {
			return $this->_operators ['number'] [$key];
		}
		return false;
	}
	public function getNumberOperators($key) {
		if (! isset ( $this->_operators ['number'] )) {
			return;
		}
		return $this->_operators ['number'];
	}
	public function removeNumberOperator($key) {
		if (isset ( $this->_operators ['number'] [$key] )) {
			unset ( $this->_operators ['number'] [$key] );
			return true;
		}
		return false;
	}
	public function clearNumberOperators() {
		if (isset ( $this->_operators ['number'] )) {
			unset ( $this->_operators ['number'] );
		}
		return true;
	}
	
	
	public function addDateOperator($key, $value) {
		if (isset ( $this->_operators ['date'] )) {
			$this->_operators ['date'] = array();
		}
		$this->_operators ['date'] [$key] = $value;
		return $this;
	}
	public function getDateOperator($key) {
		if (isset ( $this->_operators ['date'] [$key] )) {
			return $this->_operators ['date'] [$key];
		}
		return false;
	}
	public function getDateOperators($key) {
		if (! isset ( $this->_operators ['date'] )) {
			return;
		}
		return $this->_operators ['date'];
	}
	public function removeDateOperator($key) {
		if (isset ( $this->_operators ['date'] [$key] )) {
			unset ( $this->_operators ['date'] [$key] );
			return true;
		}
		return false;
	}
	public function clearDateOperators() {
		if (isset ( $this->_operators ['date'] )) {
			unset ( $this->_operators ['date'] );
		}
		return true;
	}
	
	
	public function addEnumsOperator($key, $value) {
		if (isset ( $this->_operators ['enums'] )) {
			$this->_operators ['enums'] = array();
		}
		$this->_operators ['enums'] [$key] = $value;
		return $this;
	}
	public function getEnumsOperator($key) {
		if (isset ( $this->_operators ['enums'] [$key] )) {
			return $this->_operators ['enums'] [$key];
		}
		return false;
	}
	public function getEnumsOperators($key) {
		if (! isset ( $this->_operators ['enums'] )) {
			return;
		}
		return $this->_operators ['enums'];
	}
	public function removeEnumsOperator($key) {
		if (isset ( $this->_operators ['enums'] [$key] )) {
			unset ( $this->_operators ['enums'] [$key] );
			return true;
		}
		return false;
	}
	public function clearEnumsOperators() {
		if (isset ( $this->_operators ['enums'] )) {
			unset ( $this->_operators ['enums'] );
		}
		return true;
	}
	/**
	 * @return the $_extra
	 */
	public function getExtra() {
		return $this->_extra;
	}

	/**
	 * @param field_type $_extra
	 */
	public function setExtra($_extra) {
		$this->_extra = $_extra;
	}

}
