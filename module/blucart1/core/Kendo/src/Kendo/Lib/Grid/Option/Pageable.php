<?php

namespace Kendo\Lib\Grid\Option;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Pageable extends SerializableContainer {
	protected $_pageSize;
	protected $_previousNext;
	protected $_numeric;
	protected $_buttonCount;
	protected $_input;
	protected $_pageSizes;
	protected $_refresh;
	protected $_info;
	protected $_messages;
	
	public function setMessages($_messages) {
		$this->_messages = $_messages;
	}
	public function addMessage($key, $value) {
		$this->_messages [$key] = $value;
		return $this;
	}
	public function getMessage($key) {
		if (isset ( $this->_messages [$key] )) {
			return $this->_messages [$key];
		}
		return false;
	}
	public function getMessages() {
		return $this->_messages;
	}
	public function removeMessage($key) {
		if (isset ( $this->_messages [$key] )) {
			unset ( $this->_messages [$key] );
			return true;
		}
		return false;
	}
	public function clearMessages() {
		$this->_messages = null;
		return true;
	}
	/**
	 * @return the $_pageSize
	 */
	public function getPageSize() {
		return $this->_pageSize;
	}

	/**
	 * @param field_type $_pageSize
	 */
	public function setPageSize($_pageSize) {
		$this->_pageSize = $_pageSize;
	}

	/**
	 * @return the $_previousNext
	 */
	public function getPreviousNext() {
		return $this->_previousNext;
	}

	/**
	 * @param field_type $_previousNext
	 */
	public function setPreviousNext($_previousNext) {
		$this->_previousNext = $_previousNext;
	}

	/**
	 * @return the $_numeric
	 */
	public function getNumeric() {
		return $this->_numeric;
	}

	/**
	 * @param field_type $_numeric
	 */
	public function setNumeric($_numeric) {
		$this->_numeric = $_numeric;
	}

	/**
	 * @return the $_buttonCount
	 */
	public function getButtonCount() {
		return $this->_buttonCount;
	}

	/**
	 * @param field_type $_buttonCount
	 */
	public function setButtonCount($_buttonCount) {
		$this->_buttonCount = $_buttonCount;
	}

	/**
	 * @return the $_input
	 */
	public function getInput() {
		return $this->_input;
	}

	/**
	 * @param field_type $_input
	 */
	public function setInput($_input) {
		$this->_input = $_input;
	}

	/**
	 * @return the $_pageSizes
	 */
	public function getPageSizes() {
		return $this->_pageSizes;
	}

	/**
	 * @param field_type $_pageSizes
	 */
	public function setPageSizes($_pageSizes) {
		$this->_pageSizes = $_pageSizes;
	}

	/**
	 * @return the $_refresh
	 */
	public function getRefresh() {
		return $this->_refresh;
	}

	/**
	 * @param field_type $_refresh
	 */
	public function setRefresh($_refresh) {
		$this->_refresh = $_refresh;
	}

	/**
	 * @return the $_info
	 */
	public function getInfo() {
		return $this->_info;
	}

	/**
	 * @param field_type $_info
	 */
	public function setInfo($_info) {
		$this->_info = $_info;
	}

}
