<?php

namespace Kendo\Lib\Grid\Option\Option;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Groupable extends SerializableContainer {
	protected $_messages;
	public function setMessages($_messages) {
		$this->_messages = $_messages;
	}
	public function addMessage($key, $value) {
		$this->_messages [$key] = $value;
		return $this;
	}
	public function getMessage($key) {
		if (isset ( $this->_messages [$key] )) {
			return $this->_messages [$key];
		}
		return false;
	}
	public function getMessages() {
		return $this->_messages;
	}
	public function removeMessage($key) {
		if (isset ( $this->_messages [$key] )) {
			unset ( $this->_messages [$key] );
			return true;
		}
		return false;
	}
	public function clearMessages() {
		$this->_messages = null;
		return true;
	}
	
}
