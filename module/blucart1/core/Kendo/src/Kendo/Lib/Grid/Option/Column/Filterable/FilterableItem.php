<?php

namespace Kendo\Lib\Grid\Option\Column\Filterable;

use Kendo\Lib\Core\SerializableItem;



class FilterableItem extends SerializableItem {
	
	protected $_ui;
	/**
	 * @return string $_ui
	 */
	public function getUi() {
		return $this->_ui;
	}

	/**
	 * @param field_type $_ui
	 */
	public function setUi($_ui) {
		$this->_ui = $_ui;
		return $this;
	}

	
	

		
}
