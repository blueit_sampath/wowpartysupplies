<?php

namespace Kendo\Lib\Grid\Option\Column;

use Kendo\Lib\Grid\Option\Column\Command\CommandItem;

use Kendo\Lib\Core\SerializableContainer;


/**
 * @author Prashanth Pratapagiri(apple.php@gmail.com) - 26 Jul 2013 13:08:12
 *
 */
class Columns extends SerializableContainer {
	protected $_columns;
	
	protected $_commands;
	
	/* (non-PHPdoc)
	 * @see \Kendo\Lib\Core\Serializable::toArray()
	 */
	public function toArray() {
		$array = array ();
		if ( $this->getColumns () !== null ) {
			if(is_array($this->getColumns ())){
			$array = array_values( $this->getColumns ());
			}
			else{
				$array = $this->getColumns ();
			}
		}
		
		if ( $this->getCommands () !== null ) {
			if(is_array($this->getCommands ())){
				$array[]['command'] = array_values( $this->getCommands ());
			}
			else{
				$array[]['command'] = $this->getCommands ();
			}
		}
		

		return $array;
	}
	
	
	
	
	/**
	 * @param array $_columns
	 * @return \Kendo\Lib\Grid\Option\Column\Columns
	 */
	public function setColumns($_columns) {
		$this->_columns = $_columns;
		return $this;
	}
	/**
	 * @param string $key
	 * @param ColumnItem $item
	 * @return \Kendo\Lib\Grid\Option\Column\Columns
	 */
	public function addColumn($key, ColumnItem $item) {
		$this->_columns [$key] = $item;
		return $this;
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function getColumn($key) {
		if (isset ( $this->_columns [$key] )) {
			return $this->_columns [$key];
		}
		return false;
	}
	/**
	 * @array return
	 */
	public function getColumns() {
		return $this->sortArray ( $this->_columns );
	}
	public function removeColumn($key) {
		if (isset ( $this->_columns [$key] )) {
			unset ( $this->_columns [$key] );
			return true;
		}
		return false;
	}
	/**
	 * @return boolean
	 */
	public function clearColumns() {
		$this->_columns = array ();
		return true;
	}
	
	
	/**
	 * @param array $_commands
	 * @return \Kendo\Lib\Grid\Option\Column\Columns
	 */
	public function setCommands($_commands) {
		$this->_commands = $_commands;
		return $this;
	}
	/**
	 * @param string $key
	 * @param CommandItem $item
	 * @return \Kendo\Lib\Grid\Option\Column\Columns
	 */
	public function addCommand($key, CommandItem $item) {
		$this->_commands [$key] = $item;
		return $this;
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function getCommand($key) {
		if (isset ( $this->_commands [$key] )) {
			return $this->_commands [$key];
		}
		return false;
	}
	/**
	 * @return array
	 */
	public function getCommands() {
		return $this->sortArray ( $this->_commands );
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function removeCommands($key) {
		if (isset ( $this->_commands [$key] )) {
			unset ( $this->_commands [$key] );
			return true;
		}
		return false;
	}
	/**
	 * @return boolean
	 */
	public function clearCommands() {
		$this->_commands = array ();
		
		return true;
	}
}
