<?php

namespace Kendo\Lib\Grid\Option\Column\Command;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Commands extends SerializableContainer {
	
	protected $_commands;
	
	public function toArray() {
		$array = array ();
		if ( $this->getCommands () !== null) {
			if(is_array($this->getCommands ())){
				$array = array_values( $this->getCommands ());
			}
			else{
				$array = $this->getCommands();
			}
		}
	
		return $array;
	}
	
	/**
	 * @param array $_commands
	 * @return \Kendo\Lib\Grid\Option\Column\Command\Commands
	 */
	public function setCommands($_commands) {
		$this->_commands = $_commands;
		return $this;
	}
	/**
	 * @param string $key
	 * @param CommandItem $item
	 * @return \Kendo\Lib\Grid\Option\Column\Command\Commands
	 */
	public function addCommand($key, CommandItem $item) {
		$this->_commands [$key] = $item;
		return $this;
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function getCommand($key) {
		if (isset ( $this->_commands [$key] )) {
			return $this->_commands [$key];
		}
		return false;
	}
	/**
	 * @return array
	 */
	public function getCommands() {
		return $this->sortArray ( $this->_commands );
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function removeCommands($key) {
		if (isset ( $this->_commands [$key] )) {
			unset ( $this->_commands [$key] );
			return true;
		}
		return false;
	}
	/**
	 * @return boolean
	 */
	public function clearCommands() {
		$this->_commands = array ();
		return true;
	}
}
