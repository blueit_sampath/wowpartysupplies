<?php

namespace Kendo\Lib\Grid\Option;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class ColumnMenu extends SerializableContainer {
	protected $_columns;
	protected $_filterable;
	protected $_sortable;
	protected $_messages;
	
	
	public function setMessages($_messages) {
		$this->_messages = $_messages;
	}
	public function addMessage($key, $value) {
		$this->_messages [$key] = $value;
		return $this;
	}
	public function getMessage($key) {
		if (isset ( $this->_messages [$key] )) {
			return $this->_messages [$key];
		}
		return false;
	}
	public function getMessages() {
		return  $this->_messages;
	}
	public function removeMessage($key) {
		if (isset ( $this->_messages [$key] )) {
			unset ( $this->_messages [$key] );
			return true;
		}
		return false;
	}
	public function clearMessages() {
		$this->_messages = null;
		return true;
	}
	/**
	 * @return the $_columns
	 */
	public function getColumns() {
		return $this->_columns;
	}

	/**
	 * @param field_type $_columns
	 */
	public function setColumns($_columns) {
		$this->_columns = $_columns;
	}

	/**
	 * @return the $_filterable
	 */
	public function getFilterable() {
		return $this->_filterable;
	}

	/**
	 * @param field_type $_filterable
	 */
	public function setFilterable($_filterable) {
		$this->_filterable = $_filterable;
	}

	/**
	 * @return the $_sortable
	 */
	public function getSortable() {
		return $this->_sortable;
	}

	/**
	 * @param field_type $_sortable
	 */
	public function setSortable($_sortable) {
		$this->_sortable = $_sortable;
	}

}
