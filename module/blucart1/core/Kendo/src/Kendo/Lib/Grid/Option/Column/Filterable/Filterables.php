<?php

namespace Kendo\Lib\Grid\Option\Column\Filterable;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Filterables extends SerializableContainer {
	
	protected $_filterables;
	
	public function toArray() {
		$array = array ();
		if ( $this->getFilterables () !== null) {
			if(is_array($this->getFilterables ())){
				$array = array_values( $this->getFilterables ());
			}
			else{
				$array = $this->getFilterables();
			}
		}
	
		return $array;
	}
	
	/**
	 * @param string $key
	 * @param string $_ui
	 * @return \Kendo\Lib\Grid\Option\Column\Filterable\FilterableItem
	 */
	public function add($key, $_ui){
		$item = new FilterableItem();
		$item->setUi($_ui);
		$this->addFilterable($key, $item);
		return $item;
	}
	/**
	 *
	 * @param field_type $_filterables        	
	 */
	public function setFilterables($_filterables) {
		$this->_filterables = $_filterables;
	}
	public function addFilterable($key, FilterableItem $item) {
		$this->_filterables [$key] = $item;
		return $this;
	}
	public function getFilterable($key) {
		if (isset ( $this->_filterables [$key] )) {
			return $this->_filterables [$key];
		}
		return false;
	}
	public function getFilterables() {
		return $this->sortArray ( $this->_filterables );
	}
	public function removeFilterables($key) {
		if (isset ( $this->_filterables [$key] )) {
			unset ( $this->_filterables [$key] );
			return true;
		}
		return false;
	}
	public function clearFilterables() {
		$this->_filterables = array ();
		return true;
	}
}
