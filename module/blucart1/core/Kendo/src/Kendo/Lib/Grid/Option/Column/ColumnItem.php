<?php

namespace Kendo\Lib\Grid\Option\Column;

use Kendo\Lib\Option\Aggregate\Aggregates;

use Kendo\Lib\Core\SerializableItem;

class ColumnItem extends SerializableItem {
	
	protected $_field;
	protected $_aggregates = array(); 
	protected $_attributes;
	protected $_title;
	protected $_editor;
	protected $_command;
	protected $_encoded;
	protected $_filterable;
	protected $_footerTemplate;
	protected $_format;
	protected $_groupHeaderTemplate;
	protected $_groupFooterTemplate;
	protected $_headerTemplate;
	protected $_hidden;
	protected $_sortable;
	protected $_template;
	protected $_width;
	protected $_values;
	protected $_menu;
	public function __construct(){
		//$this->_aggregates = new Aggregates();
	}
	/**
	 * @return string $_field
	 */
	public function getField() {
		return $this->_field;
	}

	/**
	 * @param string $_field
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setField($_field) {
		$this->_field = $_field;
		return $this;
	}

	/**
	 * @return string $_aggregates
	 */
	public function getAggregates() {
		return $this->_aggregates;
	}

	/**
	 * @param string $_aggregates
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setAggregates($_aggregates) {
		$this->_aggregates = $_aggregates;
		return $this;
	}

	/**
	 * @return string $_attributes
	 */
	public function getAttributes() {
		return $this->_attributes;
	}

	/**
	 * @param string $_attributes
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setAttributes($_attributes) {
		$this->_attributes = $_attributes;
		return $this;
	}

	/**
	 * @return string $_title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * @param string $_title
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
		return $this;
	}

	/**
	 * @return string $_editor
	 */
	public function getEditor() {
		return $this->_editor;
	}

	/**
	 * @param string $_editor
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setEditor($_editor) {
		$this->_editor = $_editor;
		return $this;
	}

	/**
	 * @return string $_command
	 */
	public function getCommand() {
		return $this->_command;
	}

	/**
	 * @param string $_command
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setCommand($_command) {
		$this->_command = $_command;
		return $this;
	}

	/**
	 * @return string $_encoded
	 */
	public function getEncoded() {
		return $this->_encoded;
	}

	/**
	 * @param string $_encoded
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setEncoded($_encoded) {
		$this->_encoded = $_encoded;
		return $this;
	}

	/**
	 * @return string $_filterable
	 */
	public function getFilterable() {
		return $this->_filterable;
	}

	/**
	 * @param string $_filterable
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setFilterable($_filterable) {
		$this->_filterable = $_filterable;
		return $this;
	}

	/**
	 * @return string $_footerTemplate
	 */
	public function getFooterTemplate() {
		return $this->_footerTemplate;
	}

	/**
	 * @param string $_footerTemplate
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setFooterTemplate($_footerTemplate) {
		$this->_footerTemplate = $_footerTemplate;
		return $this;
	}

	/**
	 * @return string $_format
	 */
	public function getFormat() {
		return $this->_format;
	}

	/**
	 * @param string $_format
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setFormat($_format) {
		$this->_format = $_format;
		return $this;
	}

	/**
	 * @return string $_groupHeaderTemplate
	 */
	public function getGroupHeaderTemplate() {
		return $this->_groupHeaderTemplate;
	}

	/**
	 * @param string $_groupHeaderTemplate
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setGroupHeaderTemplate($_groupHeaderTemplate) {
		$this->_groupHeaderTemplate = $_groupHeaderTemplate;
		return $this;
	}

	/**
	 * @return string $_groupFooterTemplate
	 */
	public function getGroupFooterTemplate() {
		return $this->_groupFooterTemplate;
	}

	/**
	 * @param string $_groupFooterTemplate
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setGroupFooterTemplate($_groupFooterTemplate) {
		$this->_groupFooterTemplate = $_groupFooterTemplate;
		return $this;
	}

	/**
	 * @return string $_headerTemplate
	 */
	public function getHeaderTemplate() {
		return $this->_headerTemplate;
	}

	/**
	 * @param string $_headerTemplate
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setHeaderTemplate($_headerTemplate) {
		$this->_headerTemplate = $_headerTemplate;
		return $this;
	}

	/**
	 * @return string $_hidden
	 */
	public function getHidden() {
		return $this->_hidden;
	}

	/**
	 * @param string $_hidden
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setHidden($_hidden) {
		$this->_hidden = $_hidden;
		return $this;
	}

	/**
	 * @return string $_sortable
	 */
	public function getSortable() {
		return $this->_sortable;
	}

	/**
	 * @param string $_sortable
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setSortable($_sortable) {
		$this->_sortable = $_sortable;
		return $this;
	}

	/**
	 * @return string $_template
	 */
	public function getTemplate() {
		return $this->_template;
	}

	/**
	 * @param string $_template
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setTemplate($_template) {
		$this->_template = $_template;
		return $this;
	}

	/**
	 * @return string $_width
	 */
	public function getWidth() {
		return $this->_width;
	}

	/**
	 * @param string $_width
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setWidth($_width) {
		$this->_width = $_width;
		return $this;
	}

	/**
	 * @return string $_values
	 */
	public function getValues() {
		return $this->_values;
	}

	/**
	 * @param string $_values
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setValues($_values) {
		$this->_values = $_values;
		return $this;
	}

	/**
	 * @return string $_menu
	 */
	public function getMenu() {
		return $this->_menu;
	}

	/**
	 * @param string $_menu
	 * @param Kendo\Lib\Grid\Option\Column\ColumnItem
	 */
	public function setMenu($_menu) {
		$this->_menu = $_menu;
		return $this;
	}

	
	

}
