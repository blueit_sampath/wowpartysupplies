<?php

namespace Kendo\Lib\Grid\Option;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Editable extends SerializableContainer {
	
	
	protected $_confirmation;
	protected $_createAt;
	protected $_destroy;
	protected $_mode;
	protected $_template;
	protected $_update;
	/**
	 * @return the $_confirmation
	 */
	public function getConfirmation() {
		return $this->_confirmation;
	}

	/**
	 * @param field_type $_confirmation
	 */
	public function setConfirmation($_confirmation) {
		$this->_confirmation = $_confirmation;
	}

	/**
	 * @return the $_createAt
	 */
	public function getCreateAt() {
		return $this->_createAt;
	}

	/**
	 * @param field_type $_createAt
	 */
	public function setCreateAt($_createAt) {
		$this->_createAt = $_createAt;
	}

	/**
	 * @return the $_destroy
	 */
	public function getDestroy() {
		return $this->_destroy;
	}

	/**
	 * @param field_type $_destroy
	 */
	public function setDestroy($_destroy) {
		$this->_destroy = $_destroy;
	}

	/**
	 * @return the $_mode
	 */
	public function getMode() {
		return $this->_mode;
	}

	/**
	 * @param field_type $_mode
	 */
	public function setMode($_mode) {
		$this->_mode = $_mode;
	}

	/**
	 * @return the $_template
	 */
	public function getTemplate() {
		return $this->_template;
	}

	/**
	 * @param field_type $_template
	 */
	public function setTemplate($_template) {
		$this->_template = $_template;
	}

	/**
	 * @return the $_update
	 */
	public function getUpdate() {
		return $this->_update;
	}

	/**
	 * @param field_type $_update
	 */
	public function setUpdate($_update) {
		$this->_update = $_update;
	}

	
	
	

	
}
