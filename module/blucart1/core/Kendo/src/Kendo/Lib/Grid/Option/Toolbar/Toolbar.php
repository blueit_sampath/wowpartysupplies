<?php

namespace Kendo\Lib\Grid\Option\Toolbar;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Toolbar extends SerializableContainer {
	
	protected $_toolbar;
	
	public function toArray() {
		$array = array ();
		if ( $this->getToolbars () !== null) {
			if(is_array($this->getToolbars ())){
				$array = array_values( $this->getToolbars ());
			}
			else{
				$array = $this->getToolbars();
			}
		}
	
		return $array;
	}
	
	/**
	 * @param array $_toolbar
	 * @return \Kendo\Lib\Grid\Option\Column\Toolbar\Toolbar
	 */
	public function setToolbars($_toolbar) {
		$this->_toolbar = $_toolbar;
		return $this;
	}
	/**
	 * @param string $key
	 * @param ToolbarItem $item
	 * @return \Kendo\Lib\Grid\Option\Column\Toolbar\Toolbar
	 */
	public function addToolbar($key, ToolbarItem $item) {
		$this->_toolbar [$key] = $item;
		return $this;
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function getToolbar($key) {
		if (isset ( $this->_toolbar [$key] )) {
			return $this->_toolbar [$key];
		}
		return false;
	}
	/**
	 * @return array
	 */
	public function getToolbars() {
		return $this->sortArray ( $this->_toolbar );
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function removeToolbars($key) {
		if (isset ( $this->_toolbar [$key] )) {
			unset ( $this->_toolbar [$key] );
			return true;
		}
		return false;
	}
	/**
	 * @return boolean
	 */
	public function clearToolbars() {
		$this->_toolbar = array ();
		return true;
	}
}
