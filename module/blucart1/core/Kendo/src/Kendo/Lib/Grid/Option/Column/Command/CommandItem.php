<?php

namespace Kendo\Lib\Grid\Option\Column\Command;

use Kendo\Lib\Core\SerializableItem;



class CommandItem extends SerializableItem {
	
	protected $_name;
	protected $_text;
	protected $_className;
	protected $_click;
	/**
	 * @return string $_name
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * @return string $_text
	 */
	public function getText() {
		return $this->_text;
	}

	/**
	 * @return string $_className
	 */
	public function getClassName() {
		return $this->_className;
	}

	/**
	 * @return string $_click
	 */
	public function getClick() {
		return $this->_click;
	}

	/**
	 * @param string $_name
	 * @param Kendo\Lib\Grid\Option\Column\Command\CommandItem
	 */
	public function setName($_name) {
		$this->_name = $_name;
		return $this;
	}

	/**
	 * @param string $_text
	 * @param Kendo\Lib\Grid\Option\Column\Command\CommandItem
	 */
	public function setText($_text) {
		$this->_text = $_text;
		return $this;
	}

	/**
	 * @param string $_className
	 * @param Kendo\Lib\Grid\Option\Column\Command\CommandItem
	 */
	public function setClassName($_className) {
		$this->_className = $_className;
		return $this;
	}

	/**
	 * @param string $_click
	 * @param Kendo\Lib\Grid\Option\Column\Command\CommandItem
	 */
	public function setClick($_click) {
		$this->_click = $_click;
		return $this;
	}
		
}
