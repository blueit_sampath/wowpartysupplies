<?php

namespace Kendo\Lib\Grid\Option\Toolbar;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class ToolbarItem extends SerializableItem {

    protected $_name;
    protected $_template;
    protected $_text;
    protected $_imageClass;

    public function getImageClass() {
        return $this->_imageClass;
    }

    public function setImageClass($_imageClass) {
        $this->_imageClass = $_imageClass;
        return $this;
    }

    /**
     * @return the $_name
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * @param field_type $_name
     */
    public function setName($_name) {
        $this->_name = $_name;
    }

    /**
     * @return the $_template
     */
    public function getTemplate() {
        return $this->_template;
    }

    /**
     * @param field_type $_template
     */
    public function setTemplate($_template) {
        $this->_template = $_template;
    }

    /**
     * @return the $_text
     */
    public function getText() {
        return $this->_text;
    }

    /**
     * @param field_type $_text
     */
    public function setText($_text) {
        $this->_text = $_text;
    }

}
