<?php

namespace Kendo\Lib\Grid;


use Kendo\Lib\Grid\Option\Column\Columns;

use Kendo\Lib\Datasource\Datasource;

use Kendo\Lib\Core\Serializable;

class Grid extends Serializable{
	protected $name;
	protected $_altRowTemplate;
	protected $_autoBind;
	protected $_columns;
	protected $_columnMenu;
	protected $_dataSource;
	protected $_detailTemplate;
	protected $_editable;
	protected $_filterable;
	protected $_groupable;
	protected $_height;
	protected $_navigatable;
	protected $_pageable;
	protected $_reorderable;
	protected $_resizable;
	protected $_rowTemplate;
	protected $_scrollable;
	protected $_sortable;
	protected $_selectable;
	protected $_toolbar;
	protected $_detailInit;
	

	
	public function __construct($name){
		$this->setName($name);
		$this->_columns = new Columns();
		$this->_dataSource = new Datasource();
		
	}
	/**
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string $_altRowTemplate
	 */
	public function getAltRowTemplate() {
		return $this->_altRowTemplate;
	}

	/**
	 * @param string $_altRowTemplate
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setAltRowTemplate($_altRowTemplate) {
		$this->_altRowTemplate = $_altRowTemplate;
		return $this;
	}

	/**
	 * @return string $_autoBind
	 */
	public function getAutoBind() {
		return $this->_autoBind;
	}

	/**
	 * @param string $_autoBind
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setAutoBind($_autoBind) {
		$this->_autoBind = $_autoBind;
		return $this;
	}

	/**
	 * @return \Kendo\Lib\Grid\Option\Column\Columns $_columns
	 */
	public function getColumns() {
		return $this->_columns;
	}

	/**
	 * @param \Kendo\Lib\Grid\Option\Column\Columns $_columns
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setColumns($_columns) {
		$this->_columns = $_columns;
		return $this;
	}

	/**
	 * @return string $_columnMenu
	 */
	public function getColumnMenu() {
		return $this->_columnMenu;
	}

	/**
	 * @param string $_columnMenu
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setColumnMenu($_columnMenu) {
		$this->_columnMenu = $_columnMenu;
		return $this;
	}

	/**
	 * @return \Kendo\Lib\Datasource\Datasource $_dataSource
	 */
	public function getDataSource() {
		return $this->_dataSource;
	}

	/**
	 * @param \Kendo\Lib\Datasource\Datasource $_dataSource
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setDataSource($_dataSource) {
		$this->_dataSource = $_dataSource;
		return $this;
	}

	/**
	 * @return string $_detailTemplate
	 */
	public function getDetailTemplate() {
		return $this->_detailTemplate;
	}

	/**
	 * @param string $_detailTemplate
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setDetailTemplate($_detailTemplate) {
		$this->_detailTemplate = $_detailTemplate;
		return $this;
	}

	/**
	 * @return string $_editable
	 */
	public function getEditable() {
		return $this->_editable;
	}

	/**
	 * @param string $_editable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setEditable($_editable) {
		$this->_editable = $_editable;
		return $this;
	}

	/**
	 * @return string $_filterable
	 */
	public function getFilterable() {
		return $this->_filterable;
	}

	/**
	 * @param string $_filterable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setFilterable($_filterable) {
		$this->_filterable = $_filterable;
		return $this;
	}

	/**
	 * @return string $_groupable
	 */
	public function getGroupable() {
		return $this->_groupable;
	}

	/**
	 * @param string $_groupable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setGroupable($_groupable) {
		$this->_groupable = $_groupable;
		return $this;
	}

	/**
	 * @return string $_height
	 */
	public function getHeight() {
		return $this->_height;
	}

	/**
	 * @param string $_height
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setHeight($_height) {
		$this->_height = $_height;
		return $this;
	}

	/**
	 * @return string $_navigatable
	 */
	public function getNavigatable() {
		return $this->_navigatable;
	}

	/**
	 * @param string $_navigatable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setNavigatable($_navigatable) {
		$this->_navigatable = $_navigatable;
		return $this;
	}

	/**
	 * @return string $_pageable
	 */
	public function getPageable() {
		return $this->_pageable;
	}

	/**
	 * @param string $_pageable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setPageable($_pageable) {
		$this->_pageable = $_pageable;
		return $this;
	}

	/**
	 * @return string $_reorderable
	 */
	public function getReorderable() {
		return $this->_reorderable;
	}

	/**
	 * @param string $_reorderable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setReorderable($_reorderable) {
		$this->_reorderable = $_reorderable;
		return $this;
	}

	/**
	 * @return string $_resizable
	 */
	public function getResizable() {
		return $this->_resizable;
	}

	/**
	 * @param string $_resizable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setResizable($_resizable) {
		$this->_resizable = $_resizable;
		return $this;
	}

	/**
	 * @return string $_rowTemplate
	 */
	public function getRowTemplate() {
		return $this->_rowTemplate;
	}

	/**
	 * @param string $_rowTemplate
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setRowTemplate($_rowTemplate) {
		$this->_rowTemplate = $_rowTemplate;
		return $this;
	}

	/**
	 * @return string $_scrollable
	 */
	public function getScrollable() {
		return $this->_scrollable;
	}

	/**
	 * @param string $_scrollable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setScrollable($_scrollable) {
		$this->_scrollable = $_scrollable;
		return $this;
	}

	/**
	 * @return string $_sortable
	 */
	public function getSortable() {
		return $this->_sortable;
	}

	/**
	 * @param string $_sortable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setSortable($_sortable) {
		$this->_sortable = $_sortable;
		return $this;
	}

	/**
	 * @return string $_selectable
	 */
	public function getSelectable() {
		return $this->_selectable;
	}

	/**
	 * @param string $_selectable
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setSelectable($_selectable) {
		$this->_selectable = $_selectable;
		return $this;
	}

	/**
	 * @return string $_toolbar
	 */
	public function getToolbar() {
		return $this->_toolbar;
	}

	/**
	 * @param string $_toolbar
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setToolbar($_toolbar) {
		$this->_toolbar = $_toolbar;
		return $this;
	}

	/**
	 * @return string $_detailInit
	 */
	public function getDetailInit() {
		return $this->_detailInit;
	}

	/**
	 * @param string $_detailInit
	 * @param Kendo\Lib\Grid\Grid
	 */
	public function setDetailInit($_detailInit) {
		$this->_detailInit = $_detailInit;
		return $this;
	}
	

	
}
