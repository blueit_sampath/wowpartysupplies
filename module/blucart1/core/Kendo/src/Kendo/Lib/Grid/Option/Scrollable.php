<?php

namespace Kendo\Lib\Grid\Option;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Scrollable extends SerializableContainer {
	protected $_virtual;
	protected $_selectable;
	/**
	 * @return the $_virtual
	 */
	public function getVirtual() {
		return $this->_virtual;
	}

	/**
	 * @return the $_selectable
	 */
	public function getSelectable() {
		return $this->_selectable;
	}

	/**
	 * @param field_type $_virtual
	 */
	public function setVirtual($_virtual) {
		$this->_virtual = $_virtual;
	}

	/**
	 * @param field_type $_selectable
	 */
	public function setSelectable($_selectable) {
		$this->_selectable = $_selectable;
	}

	
	
	

}
