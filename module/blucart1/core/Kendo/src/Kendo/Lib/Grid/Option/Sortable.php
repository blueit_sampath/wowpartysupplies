<?php

namespace Kendo\Lib\Grid\Option;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Sortable extends SerializableContainer {
	protected $_allowUnsort;
	protected $_mode;
	/**
	 * @return the $_allowUnsort
	 */
	public function getAllowUnsort() {
		return $this->_allowUnsort;
	}

	/**
	 * @param field_type $_allowUnsort
	 */
	public function setAllowUnsort($_allowUnsort) {
		$this->_allowUnsort = $_allowUnsort;
	}

	/**
	 * @return the $_mode
	 */
	public function getMode() {
		return $this->_mode;
	}

	/**
	 * @param field_type $_mode
	 */
	public function setMode($_mode) {
		$this->_mode = $_mode;
	}

}
