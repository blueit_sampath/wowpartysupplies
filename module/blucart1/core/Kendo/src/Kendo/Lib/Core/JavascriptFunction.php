<?php

namespace Kendo\Lib\Core;

class JavascriptFunction{
	
	protected $_value; 
	public function __construct($value){
		$this->_value = $value;
		return $this;
	}
	
	public function getValue(){
		
		return '<kendo_script_value>'.$this->_value.'</kendo_script_value>';
	}
	
}
