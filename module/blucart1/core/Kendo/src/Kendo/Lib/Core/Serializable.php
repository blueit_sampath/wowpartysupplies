<?php

namespace Kendo\Lib\Core;

class Serializable {
	public function toArray() {
		$array = array ();
		$reflect = new \ReflectionClass ( $this );
		$props = $reflect->getProperties ( \ReflectionProperty::IS_PROTECTED );
		
		foreach ( $props as $prop ) {
			$name = $prop->getName ();
			
			if (strpos ( $name, '_' ) !== 0) {
				continue;
			}
			$key = str_replace ( '_', '', $name );
			$function = 'get' . ucfirst ( $key );
			$value = $this->$function();
			
			if ($value !== null ) {
				$array [$key] = $value;
			}
			
		}
		return $array;
	}
	public function serialize() {
		$array = $this->toArray ();
		
		array_walk_recursive ( $array, array (
				$this,
				'walk' 
		) );
		//$this->array_remove_empty($array);
		return $array;
	}
	public function walk(&$item, $key) {
		if ($item instanceof Serializable) {
			$array = $item->serialize ();
			$item = $array;
		}
		if ($item instanceof JavascriptFunction) {
			$value = $item->getValue ();
			$item = $value;
		}
	}
	
	public function array_remove_empty($haystack)
	{
		foreach ($haystack as $key => $value) {
			if (is_array($value)) {
				$haystack[$key] = $this->array_remove_empty($haystack[$key]);
			}
	
			if (empty($haystack[$key])) {
				unset($haystack[$key]);
			}
		}
	
		return $haystack;
	}
}
