<?php

namespace Kendo\Lib\Core;

class SerializableItem extends Serializable {
	protected $weight;
	
	/**
	 * @return the $weight
	 */
	public function getWeight() {
		return $this->weight;
	}

	/**
	 * @param field_type $weight
	 */
	public function setWeight($weight) {
		$this->weight = $weight;
	}

	
	
}
