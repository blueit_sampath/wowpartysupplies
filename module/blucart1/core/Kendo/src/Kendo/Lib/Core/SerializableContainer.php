<?php

namespace Kendo\Lib\Core;

class SerializableContainer extends Serializable {
	protected function sortArray($array) {
		if(!is_array($array)){
			return $array;
		}
		uasort ( $array, array (
				$this,
				'cmp' 
		) );
		return $array;
	}
	protected function cmp($a, $b) {
		return (( int ) $a->getWeight () <= ( int ) $b->getWeight ()) ? 1 : - 1;
	}
}
