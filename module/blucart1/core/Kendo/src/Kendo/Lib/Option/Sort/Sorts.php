<?php

namespace Kendo\Lib\Option\Sort;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Sorts extends SerializableContainer {
	protected $_sorts;
	
	/*
	 * (non-PHPdoc) @see \Kendo\Lib\Core\Serializable::toArray() @return array
	 */
	public function toArray() {
		$array = array ();
		if ($this->getSorts () !== null) {
			if (is_array ( $this->getSorts () )) {
				$array = array_values ( $this->getSorts () );
			} else {
				$array = $this->getSorts ();
			}
		}
		
		return $array;
	}
	
	/**
	 * @param string $key
	 * @param string $_field
	 * @param string $_dir
	 * @return \Kendo\Lib\Option\Sort\SortItem
	 */
	public function add($key, $_field, $_dir = 'asc'){
		$sortItem = new SortItem();
		$sortItem->setField($_field);
		$sortItem->setDir($_dir);
		$this->addSort($key, $sortItem);
		return $sortItem;
	}
	
	/**
	 *
	 * @param array $_sorts        	
	 * @return \Kendo\Lib\Option\Sort\Sorts
	 */
	public function setSorts($_sorts) {
		$this->_sorts = $_sorts;
		return $this;
	}
	/**
	 *
	 * @param string $key        	
	 * @param SortItem $item        	
	 * @return \Kendo\Lib\Option\Sort\Sorts
	 */
	public function addSort($key, SortItem $item) {
		$this->_sorts [$key] = $item;
		return $this;
	}
	/**
	 * @param string $key
	 * @return SortItem
	 */
	public function getSort($key) {
		if (isset ( $this->_sorts [$key] )) {
			return $this->_sorts [$key];
		}
		return false;
	}
	/**
	 * @return array
	 */
	public function getSorts() {
		return $this->sortArray ( $this->_sorts );
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function removeSorts($key) {
		if (isset ( $this->_sorts [$key] )) {
			unset ( $this->_sorts [$key] );
			return true;
		}
		return false;
	}
	/**
	 * @return boolean
	 */
	public function clearSorts() {
		$this->_sorts = array ();
		return true;
	}
}
