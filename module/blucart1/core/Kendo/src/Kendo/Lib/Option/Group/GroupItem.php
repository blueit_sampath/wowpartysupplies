<?php

namespace Kendo\Lib\Option\Group;

use Kendo\Lib\Option\Aggregate;

use Kendo\Lib\Core\SerializableItem;

class GroupItem extends SerializableItem {
	
	protected $_field;
	protected $_aggregates;
	protected $_dir = 'asc';
	
	/**
	 * @return string $_field
	 */
	public function getField() {
		return $this->_field;
	}

	/**
	 * @return Aggregate\Aggregates $_aggregates
	 */
	public function getAggregates() {
		return $this->_aggregates;
	}

	/**
	 * @return string $_dir
	 */
	public function getDir() {
		return $this->_dir;
	}

	
	/**
	 * @param string $_field
	 * @return \Kendo\Lib\Option\Group\GroupItem
	 */
	public function setField($_field) {
		$this->_field = $_field;
		return $this;
	}

	/**
	 * @param Aggregate\Aggregates $_aggregates
	 * @return \Kendo\Lib\Option\Group\GroupItem
	 */
	public function setAggregates($_aggregates) {
		$this->_aggregates = $_aggregates;
		return $this;
	}

	/**
	 * @param string $_dir
	 * @return \Kendo\Lib\Option\Group\GroupItem
	 */
	public function setDir($_dir) {
		$this->_dir = $_dir;
		return $this;
	}

	
	
}
