<?php

namespace Kendo\Lib\Option\Aggregate;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Aggregates extends SerializableContainer {
	protected $_aggregates;
	public function toArray() {
		$array = array ();
		if ($this->getAggregates () !== null) {
			if (is_array ( $this->getAggregates () )) {
				$array = array_values ( $this->getAggregates () );
			} else {
				$array = $this->getAggregates ();
			}
		}
		
		return $array;
	}
	
	/**
	 *
	 * @param string $key        	
	 * @param string $_field        	
	 * @param string $_aggregate        	
	 * @return AggregateItem
	 */
	public function add($key, $_field, $_aggregate) {
		$item = new AggregateItem ();
		$item->setField ( $_field );
		$item->setAggregate ( $_aggregate );
		$this->addAggregate ( $key, $item );
		return $item;
	}
	
	/**
	 *
	 * @param array $_aggregates        	
	 */
	public function setAggregates($_aggregates) {
		$this->_aggregates = $_aggregates;
		return $this;
	}
	
	/**
	 *
	 * @param string $key        	
	 * @param AggregateItem $item        	
	 * @return \Kendo\Lib\Option\Aggregate\Aggregates
	 */
	public function addAggregate($key, AggregateItem $item) {
		$this->_aggregates [$key] = $item;
		return $this;
	}
	/**
	 *
	 * @param string $key        	
	 * @return AggregateItem
	 */
	public function getAggregate($key) {
		if (isset ( $this->_aggregates [$key] )) {
			return $this->_aggregates [$key];
		}
		return false;
	}
	/**
	 *
	 * @return array
	 */
	public function getAggregates() {
		return $this->sortArray ( $this->_aggregates );
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function removeAggregates($key) {
		if (isset ( $this->_aggregates [$key] )) {
			unset ( $this->_aggregates [$key] );
			return true;
		}
		return false;
	}
	/**
	 * @return boolean
	 */
	public function clearAggregates() {
		$this->_aggregates = array ();
		return true;
	}
}
