<?php

namespace Kendo\Lib\Option\Filter;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;


/**
 * @author Prashanth Pratapagiri(apple.php@gmail.com) - 26 Jul 2013 12:32:29
 *
 */
class Filters extends SerializableContainer {
	protected $_logic = 'and';
	protected $_filters;
	public function toArray() {
		if (! empty ( $this->_filters )) {
			return parent::toArray ();
		}
		return array ();
	}
	
	
	/**
	 * @param string $key
	 * @param string $_field
	 * @param string $_operator
	 * @param string $_value
	 * @return \Kendo\Lib\Option\Filter\FilterItem
	 */
	public function add($key, $_field, $_operator, $_value){
		$item = new FilterItem();
		$item->setField($_field);
		$item->setOperator($_operator);
		$item->setValue($_value);
		$this->addFilter($key, $item);
		return $item;
	}
	/**
	 *
	 * @return string $_logic
	 */
	public function getLogic() {
		return $this->_logic;
	}
	
	/**
	 *
	 * @param string $_logic        	
	 * @return \Kendo\Lib\Option\Filter\Filters
	 */
	public function setLogic($_logic) {
		$this->_logic = $_logic;
		return $this;
	}
	
	/**
	 *
	 * @param array $_filters        	
	 * @return \Kendo\Lib\Option\Filter\Filters
	 */
	public function setFilters($_filters) {
		$this->_filters = $_filters;
		return $this;
	}
	/**
	 *
	 * @param string $key        	
	 * @param FilterItem $item        	
	 * @return \Kendo\Lib\Option\Filter\Filters
	 */
	public function addFilter($key, FilterItem $item) {
		$this->_filters [$key] = $item;
		return $this;
	}
	/**
	 *
	 * @param string $key        	
	 * @return FilterItem
	 */
	public function getFilter($key) {
		if (isset ( $this->_filters [$key] )) {
			return $this->_filters [$key];
		}
		return false;
	}
	/**
	 * @return array
	 */
	public function getFilters() {
		return $this->sortArray ( $this->_filters );
	}
	/**
	 * @param string $key
	 * @return boolean
	 */
	public function removeFilters($key) {
		if (isset ( $this->_filters [$key] )) {
			unset ( $this->_filters [$key] );
			return true;
		}
		return false;
	}
	
	/**
	 * @return boolean
	 */
	public function clearFilters() {
		$this->_filters = array ();
		return true;
	}
}
