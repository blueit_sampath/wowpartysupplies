<?php

namespace Kendo\Lib\Option\Aggregate;

use Kendo\Lib\Core\SerializableItem;

class AggregateItem extends SerializableItem{

	
	
	/**
	 *
	 * @var String
	 */
	protected $_field;
	/**
	 *
	 * @var String
	 */
	protected $_aggregate;
	
	
	/**
	 * @return the $_field
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 * @param string $_field
	 */
	public function setField($_field) {
		$this->_field = $_field;
		return $this;
	}
	
	/**
	 * @return the $_aggregate
	 */
	public function getAggregate() {
		return $this->_aggregate;
	}
	
	/**
	 * @param string $_aggregate
	 */
	public function setAggregate($_aggregate) {
		$this->_aggregate = $_aggregate;
		return $this;
	}
	
	
	

}
