<?php

namespace Kendo\Lib\Option\Group;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

/**
 *
 * @author Prashanth Pratapagiri(apple.php@gmail.com) - 26 Jul 2013 12:36:20
 *        
 */
class Groups extends SerializableContainer {
	protected $_groups;
	public function toArray() {
		$array = array ();
		if ($this->getGroups () !== null) {
			if (is_array ( $this->getGroups () )) {
				$array = array_values ( $this->getGroups () );
			} else {
				$array = $this->getGroups ();
			}
		}
		
		return $array;
	}
	
	/**
	 *
	 * @param string $key        	
	 * @param string $_field        	
	 * @param string $_dir        	
	 * @param Kendo\Lib\Option\Aggregate\Aggregates $_aggregates        	
	 * @return \Kendo\Lib\Option\Group\GroupItem
	 */
	public function add($key, $_field, $_dir = 'asc', $_aggregates = null) {
		$item = new GroupItem ();
		$item->setField ( $_field );
		$item->setDir ( $_dir );
		$item->setAggregates ( $_aggregates );
		$this->addGroup ( $key, $item );
		return $item;
	}
	
	/**
	 *
	 * @param string $_groups        	
	 * @return \Kendo\Lib\Option\Group\Groups
	 */
	public function setGroups($_groups) {
		$this->_groups = $_groups;
		return $this;
	}
	/**
	 *
	 * @param string $key        	
	 * @param GroupItem $item        	
	 * @return \Kendo\Lib\Option\Group\Groups
	 */
	public function addGroup($key, GroupItem $item) {
		$this->_groups [$key] = $item;
		return $this;
	}
	/**
	 *
	 * @param string $key        	
	 * @return GroupItem
	 */
	public function getGroup($key) {
		if (isset ( $this->_groups [$key] )) {
			return $this->_groups [$key];
		}
		return false;
	}
	/**
	 *
	 * @return array
	 */
	public function getGroups() {
		return $this->sortArray ( $this->_groups );
	}
	/**
	 *
	 * @param string $key        	
	 * @return boolean
	 */
	public function removeGroups($key) {
		if (isset ( $this->_groups [$key] )) {
			unset ( $this->_groups [$key] );
			return true;
		}
		return false;
	}
	/**
	 *
	 * @return boolean
	 */
	public function clearGroups() {
		$this->_groups = array ();
		return true;
	}
}
