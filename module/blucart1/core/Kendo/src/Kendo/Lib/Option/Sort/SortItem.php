<?php

namespace Kendo\Lib\Option\Sort;

use Kendo\Lib\Core\SerializableItem;

/**
 * @author Prashanth Pratapagiri(apple.php@gmail.com) - 26 Jul 2013 12:37:44
 *
 */
class SortItem extends SerializableItem {
	protected $_field;
	protected $_dir;
	
	/**
	 *
	 * @return string $_field
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 *
	 * @param string $_field        	
	 * @return \Kendo\Lib\Option\Sort\SortItem
	 */
	public function setField($_field) {
		$this->_field = $_field;
		return $this;
	}
	/**
	 *
	 * @return string $_dir
	 */
	public function getDir() {
		return $this->_dir;
	}
	
	/**
	 *
	 * @param string $_dir        	
	 * @return \Kendo\Lib\Option\Sort\SortItem
	 */
	public function setDir($_dir) {
		$this->_dir = $_dir;
		return $this;
	}
}
