<?php

namespace Kendo\Lib\Option\Filter;

use Kendo\Lib\Core\SerializableItem;

class FilterItem extends SerializableItem {
	protected $_field;
	protected $_operator;
	protected $_value;
	
	/**
	 *
	 * @return string $_field
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 *
	 * @return string $_operator
	 */
	public function getOperator() {
		return $this->_operator;
	}
	
	/**
	 *
	 * @return string $_value
	 */
	public function getValue() {
		return $this->_value;
	}
	
	/**
	 *
	 * @param string $_field        	
	 * @return \Kendo\Lib\Option\Filter\FilterItem
	 */
	public function setField($_field) {
		$this->_field = $_field;
		return $this;
	}
	
	/**
	 *
	 * @param string $_operator        	
	 * @return \Kendo\Lib\Option\Filter\FilterItem
	 */
	public function setOperator($_operator) {
		$this->_operator = $_operator;
		return $this;
	}
	
	/**
	 *
	 * @param string $_value   	
	 * @return \Kendo\Lib\Option\Filter\FilterItem
	 */
	public function setValue($_value) {
		$this->_value = $_value;
		return $this;
	}
}
