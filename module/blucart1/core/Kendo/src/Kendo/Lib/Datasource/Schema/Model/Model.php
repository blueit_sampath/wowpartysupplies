<?php

namespace Kendo\Lib\Datasource\Schema\Model;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

/**
 *
 * @author prashanth
 *        
 */
class Model extends SerializableContainer {
	protected $_id;
	protected $_fields;
	
	/**
	 *
	 * @param string $key        	
	 * @param string $_type        	
	 * @param boolean $_editable        	
	 * @param boolean $_nullable        	
	 * @param string $_validation        	
	 * @param string $_defaultValue        	
	 * @return FieldItem
	 */
	public function add($key, $_type = 'string', $_editable = true, $_nullable = true, $_validation = null, $_defaultValue = null) {
		$fieldItem = new FieldItem ();
		$fieldItem->setType ( $_type );
		$fieldItem->setEditable ( $_editable );
		$fieldItem->setNullable ( $_nullable );
		$fieldItem->setValidation ( $_validation );
		$fieldItem->setDefaultValue ( $_defaultValue );
		$this->addField ( $key, $fieldItem );
		return $fieldItem;
	}
	
	/**
	 *
	 * @param array $_fields        	
	 */
	public function setFields($_fields) {
		$this->_fields = $_fields;
		return $this;
	}
	/**
	 *
	 * @param string $key        	
	 * @param FieldItem $item        	
	 * @return \Kendo\Lib\Datasource\Schema\Model\Model
	 */
	public function addField($key, FieldItem $item) {
		$this->_fields [$key] = $item;
		return $this;
	}
	
	/**
	 *
	 * @param string $key        	
	 * @return FieldItem
	 */
	public function getField($key) {
		if (isset ( $this->_fields [$key] )) {
			return $this->_fields [$key];
		}
		return false;
	}
	
	/**
	 *
	 * @return array
	 */
	public function getFields() {
		return $this->sortArray ( $this->_fields );
	}
	
	/**
	 *
	 * @param string $key        	
	 * @return boolean
	 */
	public function removeFields($key) {
		if (isset ( $this->_fields [$key] )) {
			unset ( $this->_fields [$key] );
			return true;
		}
		return false;
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function clearFields() {
		$this->_fields = array ();
		return true;
	}
	/**
	 *
	 * @return string $_id
	 */
	public function getId() {
		return $this->_id;
	}
	
	/**
	 *
	 * @param string $_id        	
	 */
	public function setId($_id) {
		$this->_id = $_id;
		return $this;
	}
	
	/**
	 *
	 * @return FieldItem
	 */
	public function get($key) {
		return $this->getField ( $key );
	}
}
