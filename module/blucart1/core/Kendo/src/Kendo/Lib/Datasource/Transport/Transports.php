<?php

namespace Kendo\Lib\Datasource\Transport;

use Kendo\Lib\Core\SerializableContainer;
use Kendo\Lib\Core\SerializableItem;

class Transports extends SerializableContainer {
	protected $_parameterMap;
	protected $_transports;
	
	public function toArray() {
		$array = array ();
		if ($this->getTransports () !== null) {
			$array = $array + $this->getTransports ();
		}
		if ($this->getParameterMap () !== null) {
			$array = $array + array (
					'parameterMap' => $this->getParameterMap ()
			);
		}
		return $array;
	}
	
	/**
	 * @param string $key
	 * @param string $_url
	 * @param string $_type
	 * @param string $_contentType
	 * @param string $_dataType
	 * @param string $_data
	 * @param string $_cache
	 * @return TransportItem
	 */
	public function add($key, $_url, $_type = 'GET', $_contentType = 'application/json', $_dataType = null, $_data = null, $_cache = null) {
		$transportItem = new TransportItem ();
		$transportItem->setUrl ( $_url );
		$transportItem->setType ( $_type );
		$transportItem->setContentType ( $_contentType );
		$transportItem->setDataType ( $_dataType );
		$transportItem->setCache ( $_cache );
		$transportItem->setData ( $_data );
		$this->addTransport ( $key, $transportItem );
		return $transportItem;
	}
	
	
	/**
	 *
	 * @param field_type $_transports        	
	 */
	public function setTransports($_transports) {
		$this->_transports = $_transports;
	}
	public function addTransport($key, TransportItem $item) {
		$this->_transports [$key] = $item;
		return $this;
	}
	public function getTransport($key) {
		if (isset ( $this->_transports [$key] )) {
			return $this->_transports [$key];
		}
		return false;
	}
	public function getTransports() {
		return $this->sortArray ( $this->_transports );
	}
	public function removeTransports($key) {
		if (isset ( $this->_transports [$key] )) {
			unset ( $this->_transports [$key] );
			return true;
		}
		return false;
	}
	public function clearTransports() {
		$this->_transports = array ();
		return true;
	}
	/**
	 *
	 * @return the $_parameterMap
	 */
	public function getParameterMap() {
		return $this->_parameterMap;
	}
	
	/**
	 *
	 * @param field_type $_parameterMap        	
	 */
	public function setParameterMap($_parameterMap) {
		$this->_parameterMap = $_parameterMap;
	}
}
