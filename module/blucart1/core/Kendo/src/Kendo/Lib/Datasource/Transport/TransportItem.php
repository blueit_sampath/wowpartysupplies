<?php

namespace Kendo\Lib\Datasource\Transport;

use Kendo\Lib\Core\SerializableItem;



class TransportItem extends SerializableItem {
	
	protected $_url;
	protected $_dataType;
	protected $_cache;
	protected $_contentType;
	protected $_data;
	protected $_type;
	/**
	 * @return the $_url
	 */
	public function getUrl() {
		return $this->_url;
	}

	/**
	 * @param field_type $_url
	 */
	public function setUrl($_url) {
		$this->_url = $_url;
	}

	/**
	 * @return the $_dataType
	 */
	public function getDataType() {
		return $this->_dataType;
	}

	/**
	 * @param field_type $_dataType
	 */
	public function setDataType($_dataType) {
		$this->_dataType = $_dataType;
	}

	/**
	 * @return the $_cache
	 */
	public function getCache() {
		return $this->_cache;
	}

	/**
	 * @param field_type $_cache
	 */
	public function setCache($_cache) {
		$this->_cache = $_cache;
	}

	/**
	 * @return the $_contentType
	 */
	public function getContentType() {
		return $this->_contentType;
	}

	/**
	 * @param field_type $_contentType
	 */
	public function setContentType($_contentType) {
		$this->_contentType = $_contentType;
	}

	/**
	 * @return the $_data
	 */
	public function getData() {
		return $this->_data;
	}

	/**
	 * @param field_type $_data
	 */
	public function setData($_data) {
		$this->_data = $_data;
	}

	/**
	 * @return the $_type
	 */
	public function getType() {
		return $this->_type;
	}

	/**
	 * @param field_type $_type
	 */
	public function setType($_type) {
		$this->_type = $_type;
	}

	
	


	

	
	
}
