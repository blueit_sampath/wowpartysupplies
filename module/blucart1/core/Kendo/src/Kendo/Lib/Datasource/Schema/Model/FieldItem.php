<?php

namespace Kendo\Lib\Datasource\Schema\Model;

use Kendo\Lib\Core\SerializableItem;

class FieldItem extends SerializableItem {
	
	
	protected $_editable;
	protected $_nullable;
	protected $_validation;
	protected $_type;
	protected $_defaultValue;
	

	/**
	 * @return the $_editable
	 */
	public function getEditable() {
		return $this->_editable;
	}

	
	/**
	 * @param string $_editable
	 * @return \Kendo\Lib\Datasource\Schema\Model\FieldItem
	 */
	public function setEditable($_editable) {
		$this->_editable = $_editable;
		return $this;
	}

	/**
	 * @return string $_nullable
	 */
	public function getNullable() {
		return $this->_nullable;
	}

	/**
	 * @param string $_nullable
	 * @return \Kendo\Lib\Datasource\Schema\Model\FieldItem
	 */
	public function setNullable($_nullable) {
		$this->_nullable = $_nullable;
		return $this;
	}

	/**
	 * @return string $_validation
	 */
	public function getValidation() {
		return $this->_validation;
		
	}

	/**
	 * @param string $_validation
	 * @return \Kendo\Lib\Datasource\Schema\Model\FieldItem
	 */
	public function setValidation($_validation) {
		$this->_validation = $_validation;
		return $this;
	}

	/**
	 * @return string $_type
	 */
	public function getType() {
		return $this->_type;
	}

	/**
	 * @param string $_type
	 */
	public function setType($_type) {
		$this->_type = $_type;
		return $this;
	}

	

	/**
	 * @return the $_defaultValue
	 */
	public function getDefaultValue() {
		return $this->_defaultValue;
	}

	/**
	 * @param field_type $_defaultValue
	 * @return \Kendo\Lib\Datasource\Schema\Model\FieldItem
	 */
	public function setDefaultValue($_defaultValue) {
		$this->_defaultValue = $_defaultValue;
		return $this;
	}
}
