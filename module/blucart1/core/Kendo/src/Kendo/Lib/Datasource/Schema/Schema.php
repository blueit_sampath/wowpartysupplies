<?php

namespace Kendo\Lib\Datasource\Schema;

use Kendo\Lib\Datasource\Schema\Model\Model;
use Kendo\Lib\Core\SerializableContainer;

class Schema extends SerializableContainer {
	protected $_data = 'data';
	protected $_aggregates = 'aggregates';
	protected $_errors = 'errors';
	protected $_groups = 'groups';
	protected $_model;
	protected $_parse;
	protected $_total = 'total';
	protected $_type;
	public function __construct() {
		$this->_model = new Model ();
	}
	
	/**
	 * @return string $_data
	 */
	public function getData() {
		return $this->_data;
	}

	
	/**
	 * @param string $_data
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function setData($_data) {
		$this->_data = $_data;
		return $this;
	}

	/**
	 * @return string $_aggregates
	 */
	public function getAggregates() {
		return $this->_aggregates;
	}

	
	/**
	 * @param string $_aggregates
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function setAggregates($_aggregates) {
		$this->_aggregates = $_aggregates;
		return $this;
	}

	/**
	 * @return string $_errors
	 */
	public function getErrors() {
		return $this->_errors;
	}

	/**
	 * @param string $_errors
	 */
	public function setErrors($_errors) {
		$this->_errors = $_errors;
		return $this;
	}

	/**
	 * @return string $_groups
	 */
	public function getGroups() {
		return $this->_groups;
	}

	
	/**
	 * @param string $_groups
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function setGroups($_groups) {
		$this->_groups = $_groups;
		return $this;
	}

	/**
	 * @return the $_model
	 */
	public function getModel() {
		return $this->_model;
	}

	
	/**
	 * @param Model $_model
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function setModel($_model) {
		$this->_model = $_model;
		return $this;
	}

	/**
	 * @return string $_parse
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function getParse() {
		return $this->_parse;
	}

	/**
	 * @param string $_parse
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function setParse($_parse) {
		$this->_parse = $_parse;
		return $this;
	}

	/**
	 * @return string $_total
	 */
	public function getTotal() {
		return $this->_total;
	}

	/**
	 * @param string $_total
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function setTotal($_total) {
		$this->_total = $_total;
		return $this;
	}

	/**
	 * @return string $_type
	 * 
	 */
	public function getType() {
		return $this->_type;
	}

	/**
	 * @param string $_type
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function setType($_type) {
		$this->_type = $_type;
		return $this;
	}

	
}
