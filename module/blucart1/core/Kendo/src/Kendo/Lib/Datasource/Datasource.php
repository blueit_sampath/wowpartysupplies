<?php

namespace Kendo\Lib\Datasource;

use Kendo\Lib\Option\Filter\Filters;
use Kendo\Lib\Option\Sort\Sorts;
use Kendo\Lib\Option\Group\Groups;
use Kendo\Lib\Option\Aggregate\Aggregates;
use Kendo\Lib\Option\Aggregate\AggregateItem;
use Kendo\Lib\Datasource\Schema\Schema;
use Kendo\Lib\Datasource\Transport\Transports;
use Kendo\Lib\Core\Serializable;

class Datasource extends Serializable {
	protected $_aggregate; // string or Object Option\Aggregate
	protected $_autoSync;
	protected $_batch;
	protected $_data; // array or string
	protected $_filter;
	protected $_group;
	protected $_page;
	protected $_pageSize;
	protected $_schema;
	protected $_serverAggregates;
	protected $_serverFiltering;
	protected $_serverGrouping;
	protected $_serverPaging;
	protected $_serverSorting;
	protected $_sort;
	protected $_transport;
	protected $_type;
	public function __construct() {
		$this->_transport = new Transports ();
		$this->_schema = new Schema ();
		//$this->_aggregate = new Aggregates ();
		//$this->_group = new Groups ();
		$this->_sort = new Sorts ();
		$this->_filter = new Filters ();
	}
	
	/**
	 *
	 * @return \Kendo\Lib\Option\Aggregate\Aggregates
	 */
	public function getAggregate() {
		return $this->_aggregate;
	}
	
	/**
	 *
	 * @param \Kendo\Lib\Option\Aggregate\Aggregates $_aggregate        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setAggregate($_aggregate) {
		$this->_aggregate = $_aggregate;
		return $this;
	}
	
	/**
	 *
	 * @return boolean $_autoSync
	 */
	public function getAutoSync() {
		return $this->_autoSync;
	}
	
	/**
	 *
	 * @param boolean $_autoSync        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setAutoSync($_autoSync) {
		$this->_autoSync = $_autoSync;
		return $this;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getBatch() {
		return $this->_batch;
	}
	
	/**
	 *
	 * @param string $_batch        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setBatch($_batch) {
		$this->_batch = $_batch;
		return $this;
	}
	
	/**
	 *
	 * @return string $_data
	 */
	public function getData() {
		return $this->_data;
	}
	
	/**
	 *
	 * @param string $_data        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setData($_data) {
		$this->_data = $_data;
		return $this;
	}
	
	/**
	 *
	 * @return \Kendo\Lib\Option\Filter\Filters
	 */
	public function getFilter() {
		return $this->_filter;
	}
	
	/**
	 *
	 * @param \Kendo\Lib\Option\Filter\Filters $_filter        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setFilter($_filter) {
		$this->_filter = $_filter;
		return $this;
	}
	
	/**
	 *
	 * @return \Kendo\Lib\Option\Group\Groups
	 */
	public function getGroup() {
		return $this->_group;
	}
	
	/**
	 *
	 * @param \Kendo\Lib\Option\Group\Groups $_group        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setGroup($_group) {
		$this->_group = $_group;
		return $this;
	}
	
	/**
	 *
	 * @return string $_page
	 *        
	 */
	public function getPage() {
		return $this->_page;
	}
	
	/**
	 *
	 * @param string $_page        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setPage($_page) {
		$this->_page = $_page;
		return $this;
	}
	
	/**
	 *
	 * @return string $_pageSize
	 */
	public function getPageSize() {
		return $this->_pageSize;
	}
	
	/**
	 *
	 * @param string $_pageSize        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setPageSize($_pageSize) {
		$this->_pageSize = $_pageSize;
		return $this;
	}
	
	/**
	 *
	 * @return \Kendo\Lib\Datasource\Schema\Schema
	 */
	public function getSchema() {
		return $this->_schema;
	}
	
	/**
	 *
	 * @param \Kendo\Lib\Datasource\Schema\Schema $_schema        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setSchema($_schema) {
		$this->_schema = $_schema;
		return $this;
	}
	
	/**
	 *
	 * @return boolean $_serverAggregates
	 */
	public function getServerAggregates() {
		return $this->_serverAggregates;
	}
	
	/**
	 *
	 * @param boolean $_serverAggregates        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setServerAggregates($_serverAggregates) {
		$this->_serverAggregates = $_serverAggregates;
		return $this;
	}
	
	/**
	 *
	 * @return boolean $_serverFiltering
	 */
	public function getServerFiltering() {
		return $this->_serverFiltering;
	}
	
	/**
	 *
	 * @param boolean $_serverFiltering        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setServerFiltering($_serverFiltering) {
		$this->_serverFiltering = $_serverFiltering;
		return $this;
	}
	
	/**
	 *
	 * @return boolean $_serverGrouping
	 */
	public function getServerGrouping() {
		return $this->_serverGrouping;
	}
	
	/**
	 *
	 * @param boolean $_serverGrouping        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setServerGrouping($_serverGrouping) {
		$this->_serverGrouping = $_serverGrouping;
		return $this;
	}
	
	/**
	 *
	 * @return boolean $_serverPaging
	 *        
	 */
	public function getServerPaging() {
		return $this->_serverPaging;
	}
	
	/**
	 *
	 * @param boolean $_serverPaging        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setServerPaging($_serverPaging) {
		$this->_serverPaging = $_serverPaging;
		return $this;
	}
	
	/**
	 *
	 * @return boolean $_serverSorting
	 */
	public function getServerSorting() {
		return $this->_serverSorting;
	}
	
	/**
	 *
	 * @param boolean $_serverSorting        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setServerSorting($_serverSorting) {
		$this->_serverSorting = $_serverSorting;
		return $this;
	}
	
	/**
	 *
	 * @return \Kendo\Lib\Option\Sort\Sorts $_sort
	 */
	public function getSort() {
		return $this->_sort;
	}
	
	/**
	 *
	 * @param \Kendo\Lib\Option\Sort\Sorts $_sort        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setSort($_sort) {
		$this->_sort = $_sort;
		return $this;
	}
	
	/**
	 *
	 * @return \Kendo\Lib\Datasource\Transport\Transports $_transport
	 */
	public function getTransport() {
		return $this->_transport;
	}
	
	/**
	 *
	 * @param \Kendo\Lib\Datasource\Transport\Transports $_transport        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setTransport($_transport) {
		$this->_transport = $_transport;
		return $this;
	}
	
	/**
	 *
	 * @return string $_type
	 */
	public function getType() {
		return $this->_type;
	}
	
	/**
	 *
	 * @param string $_type        	
	 * @return \Kendo\Lib\Datasource\Datasource
	 */
	public function setType($_type) {
		$this->_type = $_type;
		return $this;
	}
}
