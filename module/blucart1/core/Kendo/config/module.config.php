<?php
return array (
		
		'kendo_options' => array (
				'include_js' => true,
				'include_css' => true,
				'locale' => 'en-GB' 
		)
		,
		'view_helpers' => array (
				'invokables' => array (
						'kendoGrid' => 'Kendo\View\Helper\KendoGrid',
						'kendo' => 'Kendo\View\Helper\Kendo' 
				) 
		),
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								'Kendo' => __DIR__ . '/../public' 
						) 
				) 
		),
		'view_manager' => array (
				
				'template_path_stack' => array (
						'Kendo' => __DIR__ . '/../view' 
				),
				'template_map' => array (
						'common/kendo-grid' => __DIR__ . '/../view/common/kendo-grid.phtml' 
				) 
		) 
);