<?php

namespace NestedSorting;

return array (
		
		'controllers' => array (
				'invokables' => array (
						'NestedSorting\Controller\Index' => 'NestedSorting\Controller\IndexController' 
				) 
		),
		'router' => array (
				'routes' => array (
						'nested-sorting' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/nested-sorting[/:action][/:eventname]',
										'constraints' => array (
												'eventname' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
										),
										'defaults' => array (
												'controller' => __NAMESPACE__ . '\Controller\Index',
												'action' => 'index' 
										) 
								) 
						) 
				) 
		),
		
		'view_helpers' => array (
				'invokables' => array (
						'nestedSortingList' => 'NestedSorting\View\Helper\NestedSorting' 
				) 
		),
		'nested_sorting_options' => array (
				'include_js' => true,
				'include_css' => true 
		)
		,
		'view_manager' => array (	
				'template_path_stack' => array (
						__NAMESPACE__ => __DIR__ . '/../view' 
				),
				'strategies' => array (
						'ViewJsonStrategy' 
				) 
		)
		,
		'asset_manager' => array (
				'resolver_configs' => array (
						'paths' => array (
								__NAMESPACE__ => __DIR__ . '/../public' 
						) 
				) 
		) 
);

