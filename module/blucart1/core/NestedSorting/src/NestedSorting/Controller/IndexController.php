<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace NestedSorting\Controller;


use Core\Functions;

use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {
	
	/**
	 *
	 * @return \Zend\View\Model\ViewModel
	 */
	public function saveAction() {
		$eventName = $this->params ()->fromRoute ( 'eventname' );
		$this->getEventManager ()->trigger ( $eventName . '-saveList', $this );
		Functions::addSuccessMessage('Saved Successfully');
		$viewModel = new ViewModel();
		$viewModel->setTerminal(true);
		return $viewModel;
	}
	
}