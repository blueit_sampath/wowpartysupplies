<?php

namespace NestedSorting\Event;

use Core\Functions;
use NestedSorting\Option\NestedSortingList;
use Doctrine\ORM\EntityManager;
use QueryBuilder\Option\QueryHaving\QueryHavingItem;
use QueryBuilder\Option\QueryHaving\QueryHaving;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use Zend\Http\Request;
use QueryBuilder\Option\QueryBuilder;
use BlucartGrid\Option\Grid;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Kendo\Lib\Grid\Option\Column\Columns;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use BlucartGrid\Option\BlucartGridColumnItem;

abstract class AbstractNestedSortingEvent extends AbstractEvent {
	protected $_entityName = ''; // '\Category\Entity\Category';
	protected $_column = 'name'; // 'name';
	protected $_nestedSortingList;
	abstract public function getEventName();
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$priority = $this->getPriority ();
		$eventName = $this->getEventName ();
		
		$sharedEventManager = $events->getSharedManager ();
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-formList', array (
				$this,
				'formList' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-saveList', array (
				$this,
				'saveList' 
		), $priority );
		
		return $this;
	}
	public function formList($e) {
		$nestedSortingList = $e->getParam ( 'nestedSortingList' );
		$this->setNestedSortingList ( $nestedSortingList );
		$nestedSortingList = $this->getNestedSortingList ();
		$nestedSortingList->setResult ( $this->getSortedArray () );
		return $this;
	}
	public function getSortedArray() {
		$em = Functions::getEntityManager ();
		$array = array ();
		$results = $em->getRepository ( $this->_entityName )->findBy ( array (), array (
				'weight' => 'desc' 
		) );
		if ($results) {
			$column = $this->_column;
			foreach ( $results as $result ) {
				
				$array [$result->id] = array (
						'name' => $result->$column 
				);
			}
		}
		return $array;
	}
	public function saveList($e) {
		$lists = Functions::getServiceLocator ()->get ( 'Request' )->getPost ( 'list' );
		$weight = count ( $lists ) * 10;
		$em = $this->getEntityManager ();
		foreach ( $lists as $key => $c ) {
			$model = $em->find ( $this->_entityName, $key );
			$model->weight = $weight;
			$em->persist ( $model );
			$em->flush ();
			
			$weight = $weight - 10;
		}
		
		return $this;
	}
	
	/**
	 *
	 * @return EntityManager
	 */
	public function getEntityManager() {
		$sm = $this->getServiceLocator ();
		return $sm->get ( 'Doctrine\ORM\EntityManager' );
	}
	/**
	 *
	 * @return NestedSortingList $_nestedSortingList
	 */
	public function getNestedSortingList() {
		return $this->_nestedSortingList;
	}
	
	/**
	 *
	 * @param field_type $_nestedSortingList        	
	 * @param
	 *        	class_name
	 */
	public function setNestedSortingList($_nestedSortingList) {
		$this->_nestedSortingList = $_nestedSortingList;
		return $this;
	}
}
