<?php

namespace NestedSorting\View\Helper;

use NestedSorting\Option\NestedSortingList;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\View\Helper\AbstractHelper;

class NestedSorting extends AbstractHelper implements ServiceLocatorAwareInterface, EventManagerAwareInterface {
	protected $_services;
	protected $_eventName;
	protected $_events;
	protected $_maxLevels = 1;
	protected $nestedSortingList;
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services->getServiceLocator ();
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (null === $this->_events) {
			$this->setEventManager ( new EventManager () );
		}
		return $this->_events;
	}
	public function __invoke($eventName, $maxLevels = 1) {
		$this->_maxLevels = $maxLevels;
		$this->_eventName = $eventName;
		$params = array ();
		$this->attachJavascript ();
		$params ['nestedSortingList'] = $this->getNestedSortingList ();
		$this->getEventManager ()->trigger ( $eventName . '-formList', $this, $params );
	
		return $this;
	}
	
	/**
	 *
	 * @return \NestedSorting\Option\NestedSortingList
	 */
	public function getNestedSortingList() {
		if (! $this->nestedSortingList) {
			$this->nestedSortingList = new NestedSortingList ();
			$this->nestedSortingList->setMaxLevels ( $this->_maxLevels );
		}
		return $this->nestedSortingList;
	}
	public function render() {
		
		$view = $this->getView ();
		
		$string = $view->render ( 'common/nested-sorting', array (
				'listArray' => $this->getNestedSortingList ()->getResult (),
				'maxLevels' => $this->_maxLevels,
				'eventName' => $this->_eventName 
		) );
		return $string;
	}
	public function attachJavascript() {
		$view = $this->getView ();
		$config = $this->getConfig();
		if (isset ( $config ['nested_sorting_options'] )) {
			
			if (isset ( $config ['nested_sorting_options'] ['include_js'] ) && $config ['nested_sorting_options'] ['include_js']) {
				$view->headScript ()->appendFile ( $view->basePath ( 'assets/nested_sorting/jquery.mjs.nestedSortable.js' ) );
			}
			
			if (isset ( $config ['validation_options'] ['include_css'] ) && $config ['validation_options'] ['include_css']) {
				$view->headLink ()->appendStylesheet ( $view->basePath ( 'assets/nested_sorting/nestedsorting.css' ) );
			}
		}
	}
	
	public function getConfig(){
		return $this->getServiceLocator()->get('Config');
	}
}
