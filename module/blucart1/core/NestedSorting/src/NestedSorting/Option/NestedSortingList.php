<?php

namespace NestedSorting\Option;



class NestedSortingList {

	protected $maxLevels;
	protected $result;
	
	/**
	 * @return string $maxLevels
	 */
	public function getMaxLevels() {
		return $this->maxLevels;
	}

	/**
	 * @param field_type $maxLevels
	 * @param class_name
	 */
	public function setMaxLevels($maxLevels) {
		$this->maxLevels = $maxLevels;
		return $this;
	}

	/**
	 * @return string $result
	 */
	public function getResult() {
		return $this->result;
	}

	/**
	 * @param field_type $result
	 * @param class_name
	 */
	public function setResult($result) {
		$this->result = $result;
		return $this;
	}

	
}
