<?php
// Generated by ZF2's ./bin/classmap_generator.php
return array(
  'QueryBuilder\Option\Core\SerializableContainer'      => __DIR__ . '/src/QueryBuilder/Option/Core/SerializableContainer.php',
  'QueryBuilder\Option\Core\SerializableItem'           => __DIR__ . '/src/QueryBuilder/Option/Core/SerializableItem.php',
  'QueryBuilder\Option\QueryOrder\QueryOrder'           => __DIR__ . '/src/QueryBuilder/Option/QueryOrder/QueryOrder.php',
  'QueryBuilder\Option\QueryOrder\QueryOrderItem'       => __DIR__ . '/src/QueryBuilder/Option/QueryOrder/QueryOrderItem.php',
  'QueryBuilder\Option\Event\AbstractQueryBuilderEvent' => __DIR__ . '/src/QueryBuilder/Option/Event/AbstractQueryBuilderEvent.php',
  'QueryBuilder\Option\QueryHaving\QueryHavingItem'     => __DIR__ . '/src/QueryBuilder/Option/QueryHaving/QueryHavingItem.php',
  'QueryBuilder\Option\QueryHaving\QueryHaving'         => __DIR__ . '/src/QueryBuilder/Option/QueryHaving/QueryHaving.php',
  'QueryBuilder\Option\ResultArrayObject'               => __DIR__ . '/src/QueryBuilder/Option/ResultArrayObject.php',
  'QueryBuilder\Option\QueryGroup\QueryGroupItem'       => __DIR__ . '/src/QueryBuilder/Option/QueryGroup/QueryGroupItem.php',
  'QueryBuilder\Option\QueryGroup\QueryGroup'           => __DIR__ . '/src/QueryBuilder/Option/QueryGroup/QueryGroup.php',
  'QueryBuilder\Option\QueryColumn\Column'              => __DIR__ . '/src/QueryBuilder/Option/QueryColumn/Column.php',
  'QueryBuilder\Option\QueryColumn\ColumnItem'          => __DIR__ . '/src/QueryBuilder/Option/QueryColumn/ColumnItem.php',
  'QueryBuilder\Option\QueryWhere\QueryWhere'           => __DIR__ . '/src/QueryBuilder/Option/QueryWhere/QueryWhere.php',
  'QueryBuilder\Option\QueryWhere\QueryWhereItem'       => __DIR__ . '/src/QueryBuilder/Option/QueryWhere/QueryWhereItem.php',
  'QueryBuilder\Option\QueryBuilder'                    => __DIR__ . '/src/QueryBuilder/Option/QueryBuilder.php',
  'QueryBuilder\Option\QueryFrom\QueryJoinItem'         => __DIR__ . '/src/QueryBuilder/Option/QueryFrom/QueryJoinItem.php',
  'QueryBuilder\Option\QueryFrom\QueryFromItem'         => __DIR__ . '/src/QueryBuilder/Option/QueryFrom/QueryFromItem.php',
  'QueryBuilder\Option\QueryFrom\QueryFrom'             => __DIR__ . '/src/QueryBuilder/Option/QueryFrom/QueryFrom.php',
  'QueryBuilder\Module'                                 => __DIR__ . '/Module.php',
);
