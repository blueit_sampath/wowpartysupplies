<?php

namespace QueryBuilder\Option\QueryGroup;
use QueryBuilder\Option\Core\SerializableItem;

class QueryGroupItem extends SerializableItem {
	protected $_field;
	protected $_alias;
	
	public function __construct($_field, $_alias){
		$this->setField($_field);
		$this->setAlias($_alias);
	}
	
	public function serialize(){
		return $this->getField();
	}
	
	/**
	 *
	 * @return the $_field
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 *
	 * @param field_type $_field        	
	 */
	public function setField($_field) {
		$this->_field = $_field;
	}
	
	
	
	/**
	 * @return the $_alias
	 */
	public function getAlias() {
		return $this->_alias;
	}

	/**
	 * @param field_type $_alias
	 */
	public function setAlias($_alias) {
		$this->_alias = $_alias;
	}
	

}
