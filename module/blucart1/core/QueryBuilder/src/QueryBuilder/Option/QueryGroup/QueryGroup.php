<?php

namespace QueryBuilder\Option\QueryGroup;

use QueryBuilder\Option\Core\SerializableContainer;

class QueryGroup extends SerializableContainer {
	protected $_groups;
	public function serialize() {
		$groups = $this->getGroups ();
		$array = array ();
		if ($groups !== null) {
			foreach ( $groups as $group ) {
				$array [] = $group->serialize ();
			}
		}
		return implode ( ' ', $array );
	}
	
	/**
	 *
	 * @param field_type $_groups        	
	 */
	public function setGroups($_groups) {
		$this->_groups = $_groups;
	}
	public function addGroup(QueryGroupItem $item) {
		$this->_groups [$item->getAlias ()] = $item;
		return $this;
	}
	/**
	 *
	 * @param string $alias        	
	 * @return GroupItem
	 */
	public function getGroup($alias) {
		if (isset ( $this->_groups [$alias] )) {
			return $this->_groups [$alias];
		}
		return false;
	}
	/**
	 *
	 * @return array[GroupItem]
	 */
	public function getGroups() {
		return $this->sortArray ( $this->_groups );
	}
	public function removeGroup($alias) {
		if (isset ( $this->_groups [$alias] )) {
			unset ( $this->_groups [$alias] );
			return true;
		}
		return false;
	}
	public function clearGroups() {
		$this->_groups = array ();
		return true;
	}
}
