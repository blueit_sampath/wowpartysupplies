<?php

namespace QueryBuilder\Option\QueryHaving;

use QueryBuilder\Option\Core\SerializableContainer;

class QueryHaving extends SerializableContainer {
	protected $_havings;
	protected $_logic = 'and';
	protected $_havinggroups;
	public function serialize() {
		$havings = $this->getHavings ();
		$array = array ();
		if ($havings !== null) {
			foreach ( $havings as $having ) {
				$array [] = $having->serialize ();
			}
		}
		$havinggroups = $this->getHavingGroups ();
		if ($havinggroups !== null) {
			foreach ( $havinggroups as $havinggroup ) {
				
				if ($code = trim ( $havinggroup->serialize () )) {
					$array [] = '(' . $code . ')';
				}
			}
		}
		return implode ( ' ' . $this->getLogic () . ' ', $array );
	}
	/**
	 *
	 * @param field_type $_havings        	
	 */
	public function setHavings($_havings) {
		$this->_havings = $_havings;
	}
	public function addHaving(QueryHavingItem $item, $key = '') {
		if ($item->getAlias ()) {
			$this->_havings [$item->getAlias ()] = $item;
		} else {
			$this->_havings [$key] = $item;
		}
		return $this;
	}
	public function getHaving($key) {
		if (isset ( $this->_havings [$key] )) {
			return $this->_havings [$key];
		}
		return false;
	}
	public function getHavings() {
		return $this->sortArray ( $this->_havings );
	}
	public function removeHaving($alias) {
		if (isset ( $this->_havings [$alias] )) {
			unset ( $this->_havings [$alias] );
			return true;
		}
		return false;
	}
	public function clearHavings() {
		$this->_havings = array ();
		return true;
	}
	/**
	 *
	 * @return the $_logic
	 */
	public function getLogic() {
		return $this->_logic;
	}
	
	/**
	 *
	 * @param string $_logic        	
	 */
	public function setLogic($_logic) {
		$this->_logic = $_logic;
	}
	public function setHavingGroups($_havinggroup) {
		$this->_havinggroups = $_havinggroup;
	}
	public function addHavingGroup($key, QueryHaving $_havinggroup) {
		$this->_havinggroups [$key] = $_havinggroup;
		return $this;
	}
	public function getHavingGroup($key) {
		if (isset ( $this->_havinggroups [$key] )) {
			return $this->_havinggroups [$key];
		}
		return false;
	}
	public function getHavingGroups() {
		return $this->_havinggroups;
	}
	public function removeHavingGroup($key) {
		if (isset ( $this->_havinggroups [$key] )) {
			unset ( $this->_havinggroups [$key] );
			return true;
		}
		return false;
	}
	public function clearHavingGroup() {
		$this->_havinggroups = array ();
		return true;
	}
}
