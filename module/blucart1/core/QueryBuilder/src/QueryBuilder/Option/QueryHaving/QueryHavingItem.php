<?php

namespace QueryBuilder\Option\QueryHaving;

use QueryBuilder\Option\Core\SerializableItem;

class QueryHavingItem extends SerializableItem {
	protected $_field;
	protected $_operator;
	protected $_alias;
	protected $_havings;
	protected $_logic = 'or';
	protected $_noValue = false;
	public function __construct($_field, $_alias, $_operator, $_noValue = false) {
		$this->setField ( $_field );
		$this->setAlias ( $_alias );
		$this->setOperator ( $_operator );
		$this->setNoValue ( $_noValue );
	}
	public function serialize() {
		$havings = $this->getHavings ();
		$array = array ();
		if ($havings !== null) {
			foreach ( $havings as $having ) {
				$str = trim ( $having->serialize () );
				if ($str) {
					$array [] = $str;
				}
			}
		}
		$string = implode ( ' ' . $this->getLogic () . ' ', $array );
	
		if ($string) {
			if ($code = $this->formLine ()) {
				$code = $code . ' ' . $this->getLogic () . ' ';
			}
			return ' ( ' . $code . $string . ' )';
		}
	
		return $this->formLine ();
	}
	public function formLine() {
		if ($this->getOperator () === null) {
			if (! $this->getField ()) {
				return '';
			}
			return ' ' . $this->getField () . ' is null ';
		}
		if ($this->getNoValue ()) {
			if (! ($this->getField () && $this->getAlias ())) {
				return '';
			}
			return ' ' . $this->getField () . ' ' . $this->getOperator () . ' ' . $this->getAlias ();
		}
	
		if (! ($this->getField () && $this->getAlias ())) {
			return '';
		}
		return ' ' . $this->getField () . ' ' . $this->getOperator () . ' :' . $this->getAlias ();
	}
	
	/**
	 *
	 * @return the $_field
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 *
	 * @param field_type $_field        	
	 */
	public function setField($_field) {
		$this->_field = $_field;
	}
	
	/**
	 *
	 * @return the $_alias
	 */
	public function getAlias() {
		return $this->_alias;
	}
	
	/**
	 *
	 * @param field_type $_alias        	
	 */
	public function setAlias($_alias) {
		$this->_alias = $_alias;
	}
	
	/**
	 *
	 * @param field_type $_havings        	
	 */
	public function setHavings($_havings) {
		$this->_havings = $_havings;
	}
	/* note this function is different from whereitem */
	public function addHaving($key, HavingItem $item) {
		$this->_havings [$key] = $item;
		return $this;
	}
	public function getHaving($key) {
		if (isset ( $this->_havings [$key] )) {
			return $this->_havings [$key];
		}
		return false;
	}
	public function getHavings() {
		return $this->sortArray ( $this->_havings );
	}
	public function removeHavings($key) {
		if (isset ( $this->_havings [$key] )) {
			unset ( $this->_havings [$key] );
			return true;
		}
		return false;
	}
	public function clearHavings() {
		$this->_havings = array ();
		return true;
	}
	/**
	 *
	 * @return the $_operator
	 */
	public function getOperator() {
		return $this->_operator;
	}
	
	/**
	 *
	 * @param field_type $_operator        	
	 */
	public function setOperator($_operator) {
		$this->_operator = $_operator;
	}
	/**
	 *
	 * @return the $_logic
	 */
	public function getLogic() {
		return $this->_logic;
	}
	
	/**
	 *
	 * @param string $_logic        	
	 */
	public function setLogic($_logic) {
		$this->_logic = $_logic;
	}
	/**
	 * @return string $_noValue
	 */
	public function getNoValue() {
		return $this->_noValue;
	}

	/**
	 * @param boolean $_noValue
	 * @param class_name
	 */
	public function setNoValue($_noValue) {
		$this->_noValue = $_noValue;
		return $this;
	}

}
