<?php

namespace QueryBuilder\Option\Core;



abstract  class SerializableItem {
	protected $weight;
	
	/**
	 * @return the $weight
	 */
	public function getWeight() {
		return $this->weight;
	}

	/**
	 * @param field_type $weight
	 */
	public function setWeight($weight) {
		$this->weight = $weight;
	}
	public abstract  function serialize();
	
	protected function sortArray($array) {
		if(!is_array($array)){
			return $array;
		}
		uasort ( $array, array (
		$this,
		'cmp'
				) );
				return $array;
	}
	protected function cmp($a, $b) {
		return (( int ) $a->getWeight () <= ( int ) $b->getWeight ()) ? 1 : - 1;
	}
	
}
