<?php

namespace QueryBuilder\Option\QueryColumn;

use QueryBuilder\Option\Core\SerializableContainer;

class Column extends SerializableContainer {
	protected $_columns;
	public function serialize() {
		$string = '';
		if ($this->getColumns () !== null) {
			$columns = $this->getColumns ();
			$array = array ();
			foreach ( $columns as $column ) {
				$array [] = $column->serialize ();
			}
		}
		
		return  implode ( ', ', $array );
	}
	
	/**
	 *
	 * @param field_type $_columns        	
	 */
	public function setColumns($_columns) {
		$this->_columns = $_columns;
	}
	public function addColumn($item) {
		$this->_columns [$item->getField ()] = $item;
		return $this;
	}
	/**
	 *
	 * @param string $alias        	
	 * @return ColumnItem
	 */
	public function getColumn($alias) {
		if (isset ( $this->_columns [$alias] )) {
			return $this->_columns [$alias];
		}
		return false;
	}
	/**
	 *
	 * @return array[ColumnItem]
	 */
	public function getColumns() {
		return $this->sortArray ( $this->_columns );
	}
	public function removeColumn($alias) {
		if (isset ( $this->_columns [$alias] )) {
			unset ( $this->_columns [$alias] );
			return true;
		}
		return false;
	}
	public function clearColumns() {
		$this->_columns = array ();
		return true;
	}
}
