<?php

namespace QueryBuilder\Option\Event;

use QueryBuilder\Option\QueryBuilder;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;

abstract class AbstractQueryBuilderEvent extends AbstractEvent {
	protected $_queryBuilder;
	abstract public function getEventName();
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$priority = $this->getPriority ();
		$eventName = $this->getEventName ();
		
		$sharedEventManager = $events->getSharedManager ();
		
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-queryBuilder.pre', array (
				$this,
				'preQueryBuilder' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-queryBuilder.post', array (
				$this,
				'postQueryBuilder' 
		), $priority );
		
		return $this;
	}
	public function preQueryBuilder($e) {
	
		$queryBuilder = $e->getTarget ();
		$this->setQueryBuilder ( $queryBuilder );
		$this->preQuery ();
	}
	public function preQuery() {
	}
	public function postQueryBuilder($e) {
		$queryBuilder = $e->getTarget ();
		$this->setQueryBuilder ( $queryBuilder );
		//$results = $queryBuilder->getParam ( 'results' );
		$this->postQuery ();
	}
	public function postQuery() {
	}
	
	/**
	 *
	 * @return QueryBuilder $_queryBuilder
	 */
	public function getQueryBuilder() {
		return $this->_queryBuilder;
	}
	
	/**
	 *
	 * @param field_type $_queryBuilder        	
	 * @param
	 *        	class_name
	 */
	public function setQueryBuilder($_queryBuilder) {
		$this->_queryBuilder = $_queryBuilder;
		return $this;
	}
}
