<?php

namespace QueryBuilder\Option;

use DoctrineExtensions\Paginate\Paginate;

class ResultArrayObject extends \ArrayIterator {

    protected $query;

    public function __construct($value, $query) {
        parent::__construct($value);
        $this->query = $query;
    }

    public function count() {
        try {
            return Paginate::count($this->query);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return 0;
        }
    }

}
