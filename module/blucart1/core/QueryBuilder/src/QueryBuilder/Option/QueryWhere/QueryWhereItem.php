<?php

namespace QueryBuilder\Option\QueryWhere;

use QueryBuilder\Option\Core\SerializableItem;

class QueryWhereItem extends SerializableItem {
	protected $_field;
	protected $_operator;
	protected $_alias;
	protected $_wheres;
	protected $_noValue = false;
	protected $_logic = 'or';
	public function __construct($_field = '', $_alias = '', $_operator = '=', $_noValue = false) {
		$this->setField ( $_field );
		$this->setAlias ( $_alias );
		$this->setOperator ( $_operator );
		$this->setNoValue ( $_noValue );
	}
	public function serialize() {
		$wheres = $this->getWheres ();
		$array = array ();
		if ($wheres !== null) {
			foreach ( $wheres as $where ) {
				$str = trim ( $where->serialize () );
				if ($str) {
					$array [] = $str;
				}
			}
		}
		$string = implode ( ' ' . $this->getLogic () . ' ', $array );
		
		if ($string) {
			if ($code = $this->formLine ()) {
				$code = $code . ' ' . $this->getLogic () . ' ';
			}
			return ' ( ' . $code . $string . ' )';
		}
		
		return $this->formLine ();
	}
	public function formLine() {
		if ($this->getOperator () === null) {
			if (! $this->getField ()) {
				return '';
			}
			return ' ' . $this->getField () . ' is null ';
		}
		if ($this->getNoValue ()) {
			if (! ($this->getField () && $this->getAlias ())) {
				return '';
			}
			return ' ' . $this->getField () . ' ' . $this->getOperator () . ' ' . $this->getAlias ();
		}
		
		if (! ($this->getField () && $this->getAlias ())) {
			return '';
		}
		return ' ' . $this->getField () . ' ' . $this->getOperator () . ' :' . $this->getAlias ();
	}
	
	/**
	 *
	 * @return the $_field
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 *
	 * @param field_type $_field        	
	 */
	public function setField($_field) {
		$this->_field = $_field;
	}
	
	/**
	 *
	 * @return the $_alias
	 */
	public function getAlias() {
		return $this->_alias;
	}
	
	/**
	 *
	 * @param field_type $_alias        	
	 */
	public function setAlias($_alias) {
		$this->_alias = $_alias;
	}
	
	/**
	 *
	 * @param field_type $_wheres        	
	 */
	/**
	 *
	 * @param field_type $_wheres        	
	 */
	public function setWheres($_wheres) {
		$this->_wheres = $_wheres;
	}
	public function addWhere(QueryWhereItem $item, $key = '') {
		if ($item->getAlias ()) {
			$this->_wheres [$item->getAlias ()] = $item;
		} else {
			$this->_wheres [$key] = $item;
		}
		return $this;
	}
	public function getWhere($key) {
		if (isset ( $this->_wheres [$key] )) {
			return $this->_wheres [$key];
		}
		return false;
	}
	public function getWheres() {
		return $this->sortArray ( $this->_wheres );
	}
	public function removeWhere($alias) {
		if (isset ( $this->_wheres [$alias] )) {
			unset ( $this->_wheres [$alias] );
			return true;
		}
		return false;
	}
	public function clearWheres() {
		$this->_wheres = array ();
		return true;
	}
	/**
	 *
	 * @return the $_operator
	 */
	public function getOperator() {
		return $this->_operator;
	}
	
	/**
	 *
	 * @param field_type $_operator        	
	 */
	public function setOperator($_operator) {
		$this->_operator = $_operator;
	}
	
	/**
	 *
	 * @return the $_logic
	 */
	public function getLogic() {
		return $this->_logic;
	}
	
	/**
	 *
	 * @param string $_logic        	
	 */
	public function setLogic($_logic) {
		$this->_logic = $_logic;
	}
	/**
	 *
	 * @return string $_noValue
	 */
	public function getNoValue() {
		return $this->_noValue;
	}
	
	/**
	 *
	 * @param boolean $_noValue        	
	 * @param
	 *        	class_name
	 */
	public function setNoValue($_noValue) {
		$this->_noValue = $_noValue;
		return $this;
	}
}
