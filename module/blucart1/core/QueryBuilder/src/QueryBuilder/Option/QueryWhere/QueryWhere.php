<?php

namespace QueryBuilder\Option\QueryWhere;

use QueryBuilder\Option\Core\SerializableContainer;

class QueryWhere extends SerializableContainer {
	protected $_wheres;
	protected $_wheregroups;
	protected $_logic = 'and';
	public function serialize() {
		$wheres = $this->getWheres ();
		$array = array ();
		if ($wheres !== null) {
			foreach ( $wheres as $where ) {
				$array [] = $where->serialize ();
			}
		}
		$wheregroups = $this->getWhereGroups ();
		if ($wheregroups !== null) {
			foreach ( $wheregroups as $wheregroup ) {
				if ($code = trim ( $wheregroup->serialize () )) {
					$array [] = '(' . $code . ')';
				}
			}
		}
		return implode ( ' ' . $this->getLogic () . ' ', $array );
	}
	
	/**
	 *
	 * @param field_type $_wheres        	
	 */
	public function setWheres($_wheres) {
		$this->_wheres = $_wheres;
	}
	public function addWhere(QueryWhereItem $item, $key = '') {
		if ($item->getAlias ()) {
			$this->_wheres [$item->getAlias ()] = $item;
		} else {
			$this->_wheres [$key] = $item;
		}
		return $this;
	}
	public function getWhere($key) {
		if (isset ( $this->_wheres [$key] )) {
			return $this->_wheres [$key];
		}
		return false;
	}
	public function getWheres() {
		return $this->sortArray ( $this->_wheres );
	}
	public function removeWhere($alias) {
		if (isset ( $this->_wheres [$alias] )) {
			unset ( $this->_wheres [$alias] );
			return true;
		}
		return false;
	}
	public function clearWheres() {
		$this->_wheres = array ();
		return true;
	}
	/**
	 *
	 * @return the $_logic
	 */
	public function getLogic() {
		return $this->_logic;
	}
	
	/**
	 *
	 * @param string $_logic        	
	 */
	public function setLogic($_logic) {
		$this->_logic = $_logic;
	}
	
	/**
	 *
	 * @param field_type $_wheres        	
	 */
	public function setWhereGroups($_wheregroup) {
		$this->_wheregroups = $_wheregroup;
	}
	public function addWhereGroup($key, QueryWhere $_wheregroup) {
		$this->_wheregroups [$key] = $_wheregroup;
		return $this;
	}
	public function getWhereGroup($key) {
		if (isset ( $this->_wheregroups [$key] )) {
			return $this->_wheregroups [$key];
		}
		return false;
	}
	public function getWhereGroups() {
		return $this->_wheregroups;
	}
	public function removeWhereGroup($key) {
		if (isset ( $this->_wheregroups [$key] )) {
			unset ( $this->_wheregroups [$key] );
			return true;
		}
		return false;
	}
	public function clearWhereGroup() {
		$this->_wheregroups = array ();
		return true;
	}
}
