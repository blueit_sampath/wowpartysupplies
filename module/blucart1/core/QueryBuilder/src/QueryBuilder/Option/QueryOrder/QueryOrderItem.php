<?php

namespace QueryBuilder\Option\QueryOrder;
use QueryBuilder\Option\Core\SerializableItem;

class QueryOrderItem extends SerializableItem {
	protected $_field;
	protected $_dir;
	protected $_alias;
	
	
	public function serialize(){
		
		return $this->getField() .'  '. $this->getDir();
	}
	
	public function __construct($_field, $_alias, $dir = 'asc'){
		$this->setField($_field);
		$this->setAlias($_alias);
		$this->setDir($dir);
	
	}
	/**
	 *
	 * @return the $_field
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 *
	 * @param field_type $_field        	
	 */
	public function setField($_field) {
		$this->_field = $_field;
	}
	
	
	
	/**
	 * @return the $_alias
	 */
	public function getAlias() {
		return $this->_alias;
	}

	/**
	 * @param field_type $_alias
	 */
	public function setAlias($_alias) {
		$this->_alias = $_alias;
	}
	/**
	 * @return the $_dir
	 */
	public function getDir() {
		return $this->_dir;
	}

	/**
	 * @param field_type $_dir
	 */
	public function setDir($_dir) {
		$this->_dir = $_dir;
	}


}
