<?php

namespace QueryBuilder\Option\QueryOrder;

use QueryBuilder\Option\Core\SerializableContainer;

class QueryOrder extends SerializableContainer {
	protected $_orders;
	
	
	public function serialize() {
		$orders = $this->getOrders ();
		$array = array ();
		if ($orders !== null) {
			foreach ( $orders as $order ) {
				$array [] = $order->serialize ();
			}
		}
		return implode ( ', ', $array );
	}
	
	/**
	 *
	 * @param field_type $_orders        	
	 */
	public function setOrders($_orders) {
		$this->_orders = $_orders;
	}
	public function addOrder(QueryOrderItem $item) {
		$this->_orders [$item->getAlias ()] = $item;
		return $this;
	}
	/**
	 *
	 * @param string $alias        	
	 * @return OrderItem
	 */
	public function getOrder($alias) {
		if (isset ( $this->_orders [$alias] )) {
			return $this->_orders [$alias];
		}
		return false;
	}
	/**
	 *
	 * @return array[OrderItem]
	 */
	public function getOrders() {
		return $this->sortArray ( $this->_orders );
	}
	public function removeOrder($alias) {
		if (isset ( $this->_orders [$alias] )) {
			unset ( $this->_orders [$alias] );
			return true;
		}
		return false;
	}
	public function clearOrders() {
		$this->_orders = array ();
		return true;
	}
}
