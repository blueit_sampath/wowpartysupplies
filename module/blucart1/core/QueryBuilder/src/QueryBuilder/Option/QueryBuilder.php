<?php

namespace QueryBuilder\Option;

use DoctrineExtensions\Paginate\Paginate;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query;
use Doctrine\ORM\EntityManager;
use Core\Event\Core\AbstractEvent;
use QueryBuilder\Option\QueryOrder\QueryOrder;
use QueryBuilder\Option\QueryHaving\QueryHaving;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use QueryBuilder\Option\QueryGroup\QueryGroup;
use QueryBuilder\Option\QueryHaving\QueryHavingItem;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use QueryBuilder\Option\QueryOrder\QueryOrderItem;
use QueryBuilder\Option\QueryGroup\QueryGroupItem;
use QueryBuilder\Option\QueryFrom\QueryFromItem;
use QueryBuilder\Option\QueryFrom\QueryFrom;
use QueryBuilder\Option\QueryColumn\ColumnItem;
use QueryBuilder\Option\QueryColumn\Column;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class QueryBuilder implements ServiceLocatorAwareInterface, EventManagerAwareInterface {

    protected $_services;
    protected $_events;
    protected $_eventName;
    protected $_columns;
    protected $_froms;
    protected $_groups;
    protected $_wheres;
    protected $_havings;
    protected $_orders;
    protected $_parameters = array();
    protected $_results;

    public function __construct() {
        $this->_columns = new Column ();
        $this->_froms = new QueryFrom ();
        $this->_groups = new QueryGroup ();
        $this->_wheres = new QueryWhere ();
        $this->_havings = new QueryHaving ();
        $this->_orders = new QueryOrder ();
    }

    public function setEventName($eventName) {
        $this->_eventName = $eventName;
    }

    public function getEventName() {
        return $this->_eventName;
    }

    /**
     *
     * @param string $_field        	
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryColumn\ColumnItem
     */
    public function addColumn($_field, $_alias) {
        $columns = $this->getColumns();
        $columnItem = new ColumnItem($_field, $_alias);
        $columns->addColumn($columnItem);
        return $columnItem;
    }

    /**
     *
     * @param Column $columns        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function setColumns(Column $columns) {
        $this->_columns = $columns;
        return $this;
    }

    /**
     *
     * @return Column
     */
    public function getColumns() {
        return $this->_columns;
    }

    /**
     *
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryColumn\ColumnItem
     */
    public function getColumn($_alias) {
        return $this->getColumns()->getColumn($_alias);
    }

    /**
     *
     * @param string $_alias        	
     * @return boolean
     */
    public function removeColumn($_alias) {
        return $this->getColumns()->removeColumn($_alias);
    }

    /**
     *
     * @return boolean
     */
    public function clearColumns() {
        return $this->getColumns()->clearColumns();
    }

    /**
     *
     * @param string $_field        	
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryFrom\QueryFromItem
     */
    public function addFrom($_field, $_alias) {
        $froms = $this->getFroms();
        $fromItem = new QueryFromItem($_field, $_alias);
        $froms->addFrom($fromItem);
        return $fromItem;
    }

    /**
     *
     * @param QueryFrom $froms        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function setFroms(QueryFrom $froms) {
        $this->_froms = $froms;
        return $this;
    }

    /**
     *
     * @return QueryFrom
     */
    public function getFroms() {
        return $this->_froms;
    }

    /**
     *
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryFrom\QueryFromItem
     */
    public function getFrom($_alias) {
        return $this->getFroms()->getFrom($_alias);
    }

    /**
     *
     * @param string $_alias        	
     * @return boolean
     */
    public function removeFrom($_alias) {
        return $this->getFroms()->removeFrom($_alias);
    }

    /**
     *
     * @return boolean
     */
    public function clearFroms() {
        return $this->getFroms()->clearFroms();
    }

    /**
     *
     * @param string $_field        	
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryGroup\QueryGroupItem
     */
    public function addGroup($_field, $_alias) {
        $groups = $this->getGroups();
        $groupItem = new QueryGroupItem($_field, $_alias);
        $groups->addGroup($groupItem);
        return $groupItem;
    }

    /**
     *
     * @param QueryGroup $groups        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function setGroups(QueryGroup $groups) {
        $this->_groups = $groups;
        return $this;
    }

    /**
     *
     * @return QueryGroup
     */
    public function getGroups() {
        return $this->_groups;
    }

    /**
     *
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryGroup\QueryGroupItem
     */
    public function getGroup($_alias) {
        return $this->getGroups()->getGroup($_alias);
    }

    /**
     *
     * @param string $_alias        	
     * @return boolean
     */
    public function removeGroup($_alias) {
        return $this->getGroups()->removeGroup($_alias);
    }

    /**
     *
     * @return boolean
     */
    public function clearGroups() {
        return $this->getGroups()->clearGroups();
    }

    /**
     *
     * @param string $_field        	
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryOrder\QueryOrderItem
     */
    public function addOrder($_field, $_alias, $dir = 'asc') {
        $orders = $this->getOrders();
        $orderItem = new QueryOrderItem($_field, $_alias, $dir);
        $orders->addOrder($orderItem);
        return $orderItem;
    }

    /**
     *
     * @param QueryOrder $orders        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function setOrders(QueryOrder $orders) {
        $this->_orders = $orders;
        return $this;
    }

    /**
     *
     * @return QueryOrder
     */
    public function getOrders() {
        return $this->_orders;
    }

    /**
     *
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryOrder\QueryOrderItem
     */
    public function getOrder($_alias) {
        return $this->getOrders()->getOrder($_alias);
    }

    /**
     *
     * @param string $_alias        	
     * @return boolean
     */
    public function removeOrder($_alias) {
        return $this->getOrders()->removeOrder($_alias);
    }

    /**
     *
     * @return boolean
     */
    public function clearOrders() {
        return $this->getOrders()->clearOrders();
    }

    /**
     *
     * @param string $_field        	
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryWhere\QueryWhereItem
     */
    public function addWhere($_field, $_alias, $operator = '=', $noValue = false) {
        $wheres = $this->getWheres();
        $whereItem = new QueryWhereItem($_field, $_alias, $operator, $noValue);
        $wheres->addWhere($whereItem);
        return $whereItem;
    }

    /**
     *
     * @param QueryWhere $wheres        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function setWheres(QueryWhere $wheres) {
        $this->_wheres = $wheres;
        return $this;
    }

    /**
     *
     * @return QueryWhere
     */
    public function getWheres() {
        return $this->_wheres;
    }

    /**
     *
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryWhere\QueryWhereItem
     */
    public function getWhere($_alias) {
        return $this->getWheres()->getWhere($_alias);
    }

    /**
     *
     * @param string $_alias        	
     * @return boolean
     */
    public function removeWhere($_alias) {
        return $this->getWheres()->removeWhere($_alias);
    }

    /**
     *
     * @return boolean
     */
    public function clearWheres() {
        return $this->getWheres()->clearWheres();
    }

    /**
     *
     * @param string $_field        	
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryHaving\QueryHavingItem
     */
    public function addHaving($_field, $_alias, $operator = '=', $noValue = false) {
        $havings = $this->getHavings();
        $havingItem = new QueryHavingItem($_field, $_alias, $operator, $noValue);
        $havings->addHaving($havingItem);
        return $havingItem;
    }

    /**
     *
     * @param QueryHaving $havings        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function setHavings(QueryHaving $havings) {
        $this->_havings = $havings;
        return $this;
    }

    /**
     *
     * @return QueryHaving
     */
    public function getHavings() {
        return $this->_havings;
    }

    /**
     *
     * @param string $_alias        	
     * @return \QueryBuilder\Option\QueryHaving\QueryHavingItem
     */
    public function getHaving($_alias) {
        return $this->getHavings()->getHaving($_alias);
    }

    /**
     *
     * @param string $_alias        	
     * @return boolean
     */
    public function removeHaving($_alias) {
        return $this->getHavings()->removeHaving($_alias);
    }

    /**
     *
     * @return boolean
     */
    public function clearHavings() {
        return $this->getHavings()->clearHavings();
    }

    /**
     *
     * @param string $_key        	
     * @param string $_value        	
     */
    public function addParameter($_key, $_value) {
        $this->_parameters [$_key] = $_value;
        return $this;
    }

    /**
     *
     * @param array $_parameters        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function setParameter($_key, $_value) {
        $this->_parameters [$_key] = $_value;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getParamters() {
        return $this->_parameters;
    }

    /**
     *
     * @param string $_key        	
     * @param string $defaultValue        	
     * @return multitype:array string
     */
    public function getParameter($_key, $defaultValue = '') {
        if (isset($this->_parameters [$_key])) {
            return $this->_parameters [$_key];
        }
        return $defaultValue;
    }

    /**
     *
     * @param string $_key        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function removeParameter($_key) {
        if (isset($this->_parameters [$_key])) {
            unset($this->_parameters [$_key]);
        }
        return $this;
    }

    /**
     *
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function clearParameters() {
        $this->_parameters = array();
        return $this;
    }

    /**
     * ;
     *
     * @return string
     */
    public function getDql() {
        $columns = $this->getColumns()->serialize();
        $froms = $this->getFroms()->serialize();
        $groups = $this->getGroups()->serialize();
        $wheres = $this->getWheres()->serialize();
        $havings = $this->getHavings()->serialize();
        $orders = $this->getOrders()->serialize();
        $dql = array();
        $dql [] = 'Select ' . $columns . ' from ' . $froms;

        if (trim($wheres)) {
            $dql [] = 'where ' . trim($wheres);
        }
        if (trim($groups)) {
            $dql [] = 'group by ' . trim($groups);
        }
        if (trim($havings)) {
            $dql [] = 'having ' . trim($havings);
        }

        if (trim($orders)) {
            $dql [] = 'order by ' . trim($orders);
        }

        return trim(implode(' ', $dql));
    }

    /**
     *
     * @return array
     */
    public function getOldResults() {
        return $this->_results;
    }

    /**
     *
     * @param array $results        	
     * @return QueryBuilder
     */
    public function setOldResults($results = null) {
        $this->_results = $results;
        return $this;
    }

    /**
     *
     * @return \Doctrine\ORM\Query
     */
    public function getQuery() {
        $em = $this->getEntityManager();
        $dql = $this->getDql();
        $query = $em->createQuery($dql);
        foreach ($this->getParamters() as $key => $value) {

            $query->setParameter($key, $value);
        }
        return $query;
    }

    /**
     *
     * @param array $params        	
     * @return multitype:array boolean
     */
    public function executeQuery($skip = 0, $take = PHP_INT_MAX) {
        $eventName = $this->getEventName();

        $params = array();
        $params ['queryBuilder'] = $this;

        if ($eventName) {
            $this->getEventManager()->trigger($eventName . '-queryBuilder.pre', $this, $params);
        }

        $query = $this->getQuery();
        $query->setFirstResult($skip);
        $query->setMaxResults($take);;
        $results = $query->getResult();
        $this->setOldResults($results);
        $params ['results'] = &$results;
        if ($eventName) {
            $this->getEventManager()->trigger($eventName . '-queryBuilder.post', $this, $params);
        }

        
        
        //return $results;
        return new ResultArrayObject($results, $this->getQuery());
    }

    public function executeSingleScalarQuery($type = 'count', $params = array(), $query = null) {
        $eventName = $this->getEventName();
        $params ['queryBuilder'] = $this;
        $this->clearOrders();
        if ($eventName) {
            $this->getEventManager()->trigger($eventName . '-queryBuilder.pre', $this, $params);
        }
        if (!$query) {
            $query = $this->getQuery();
        }


        try {

            $results = $query->getSingleScalarResult();
            $this->setOldResults($results);
            $params ['results'] = &$results;
            if ($eventName) {
                $this->getEventManager()->trigger($eventName . '-queryBuilder.post', $this, $params);
            }
            return $results;
        } catch (\Doctrine\ORM\NoResultException $e) {

            if ($type == 'count') {
//                try {
//                    $results = Paginate::count($query);
//                    $this->setOldResults($results);
//                    $params ['results'] = &$results;
//                    if ($eventName) {
//                        $this->getEventManager()->trigger($eventName . '-queryBuilder.post', $this, $params);
//                    }
//                    return $results;
//                } catch (\Doctrine\ORM\NoResultException $e) {
//                  
//                }
            }

            $results = $query->getResult();
            $array = array();
            foreach ($results as $result) {
                $array [] = array_shift($result);
            }

            if (count($results)) {
                switch ($type) {
                    case 'max' :
                        $results = max($array);
                        break;
                    case 'min' :
                        $results = min($array);
                        break;
                    case 'avg' :
                        if (count($array) > 0) {
                            $results = array_sum($array) / count($array);
                        } else {
                            $results = 0;
                        }
                        break;
                    case 'sum' :
                        $results = array_sum($array);
                        break;
                    default :
                        $results = count($results);
                        break;
                }
            } else {
                $results = 0;
            }
        }


        $this->setOldResults($results);
        $params ['results'] = &$results;
        if ($eventName) {
            $this->getEventManager()->trigger($eventName . '-queryBuilder.post', $this, $params);
        }
        return $results;
    }

    
//    SELECT count(p1.*) 
//FROM Application\Entity\Modules_ProductVersions p1 
//WHERE p1.id IN (
//    SELECT p.id
//    FROM Application\Entity\Modules_ProductVersions p
//    LEFT JOIN p.stocks s
//    GROUP BY p.id
//    HAVING SUM(s.stock) > 0
//);
    /**
     *
     * @param array $params        	
     * @return multitype:array boolean
     */
    /*
      public function executeSingleScalarQuery($type = 'count', $params = array(), $query = null) {
      $eventName = $this->getEventName ();
      $params ['queryBuilder'] = $this;
      $this->clearOrders ();
      if ($eventName) {
      $this->getEventManager ()->trigger ( $eventName . '-queryBuilder.pre', $this, $params );
      }
      if (! $query) {
      $query = $this->getQuery ();
      }

      try {

      $results = $query->getSingleScalarResult ();
      } catch ( \Exception $e ) {

      if ($type == 'count') {
      $results = Paginate::count ( $query );
      } else {
      $results = $query->getResult ();
      $array = array ();
      foreach ( $results as $result ) {
      $array [] = array_shift ( $result );
      }

      if (count ( $results )) {
      switch ($type) {
      case 'max' :
      $results = max ( $array );
      break;
      case 'min' :
      $results = min ( $array );
      break;
      case 'avg' :
      if (count ( $array ) > 0) {
      $results = array_sum ( $array ) / count ( $array );
      } else {
      $results = 0;
      }
      break;
      case 'sum' :
      $results = array_sum ( $array );
      break;
      default :
      $results = 0;
      break;
      }
      } else {
      $results = 0;
      }
      }
      }

      $this->setOldResults ( $results );
      $params ['results'] = &$results;
      if ($eventName) {
      $this->getEventManager ()->trigger ( $eventName . '-queryBuilder.post', $this, $params );
      }
      return $results;
      }

     */

    /**
     *
     * @return EntityManager
     */
    public function getEntityManager() {
        $sm = $this->getServiceLocator();
        return $sm->get('Doctrine\ORM\EntityManager');
    }

    /**
     *
     * @param ServiceLocatorInterface $serviceLocator        	
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->_services = $serviceLocator;
    }

    /**
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator() {
        return $this->_services;
    }

    /**
     *
     * @param EventManagerInterface $events        	
     * @return \QueryBuilder\Option\QueryBuilder
     */
    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    /**
     *
     * @return EventManagerInterface
     */
    public function getEventManager() {
        if (null === $this->_events) {
            $this->setEventManager(new EventManager());
        }
        return $this->_events;
    }

    public function __clone() {
        $this->_columns = clone $this->_columns;
        $this->_froms = clone $this->_froms;
        $this->_groups = clone $this->_groups;
        $this->_wheres = clone $this->_wheres;
        $this->_havings = clone $this->_havings;
        $this->_orders = clone $this->_orders;

        $this->_services = clone $this->_services;
        $this->_events = clone $this->_events;
    }

}
