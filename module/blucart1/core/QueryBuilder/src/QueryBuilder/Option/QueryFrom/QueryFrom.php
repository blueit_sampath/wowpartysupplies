<?php

namespace QueryBuilder\Option\QueryFrom;

use QueryBuilder\Option\Core\SerializableContainer;

class QueryFrom extends SerializableContainer {
	protected $_froms;
	
	public function serialize() {
		$string = '';
		if ($this->getFroms () !== null) {
			$froms = $this->getFroms ();
			$array = array ();
			foreach ( $froms as $from ) {
				$array [] = $from->serialize ();
			}
		}
		
		return implode ( ', ', $array );
	}
	
	/**
	 *
	 * @param field_type $_froms        	
	 */
	public function setFroms($_froms) {
		$this->_froms = $_froms;
	}
	public function addFrom(QueryFromItem $item) {
		$this->_froms [$item->getAlias ()] = $item;
		return $this;
	}
	public function getFrom($key) {
		if (isset ( $this->_froms [$key] )) {
			return $this->_froms [$key];
		}
		return false;
	}
	public function getFroms() {
		return $this->sortArray ( $this->_froms );
	}
	public function removeFrom($alias) {
		if (isset ( $this->_froms [$alias] )) {
			unset ( $this->_froms [$alias] );
			return true;
		}
		return false;
	}
	public function clearFroms() {
		$this->_froms = array ();
		return true;
	}
}
