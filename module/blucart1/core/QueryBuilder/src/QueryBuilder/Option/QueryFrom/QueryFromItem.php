<?php

namespace QueryBuilder\Option\QueryFrom;

use QueryBuilder\Option\Core\SerializableItem;

class QueryFromItem extends SerializableItem {
	protected $_field;
	protected $_alias;
	protected $_joins;
	public function __construct($_field, $_alias) {
		$this->setField ( $_field );
		$this->setAlias ( $_alias );
	}
	
	/**
	 *
	 * @return the $_field
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 *
	 * @param field_type $_field        	
	 */
	public function setField($_field) {
		$this->_field = $_field;
	}
	
	/**
	 *
	 * @return the $_alias
	 */
	public function getAlias() {
		return $this->_alias;
	}
	
	/**
	 *
	 * @param field_type $_alias        	
	 */
	public function setAlias($_alias) {
		$this->_alias = $_alias;
	}
	
	/**
	 *
	 * @param field_type $_joins        	
	 */
	public function setJoins($_joins) {
		$this->_joins = $_joins;
	}
	public function addJoin(QueryJoinItem $item) {
		$this->_joins [$item->getAlias()] = $item;
		return $this;
	}
	public function getJoin($key) {
		if (isset ( $this->_joins [$key] )) {
			return $this->_joins [$key];
		}
		return false;
	}
	public function getJoins() {
		return $this->sortArray ( $this->_joins );
	}
	public function removeJoins($key) {
		if (isset ( $this->_joins [$key] )) {
			unset ( $this->_joins [$key] );
			return true;
		}
		return false;
	}
	public function clearJoins() {
		$this->_joins = array ();
		return true;
	}
	public function serialize() {
		$joins = $this->getJoins ();
		$array = array ();
		if ($joins !== null) {
			foreach ( $joins as $join ) {
				$array [] = $join->serialize ();
			}
		}
		return $this->getField () . ' ' . $this->getAlias () . ' ' . implode ( ' ', $array );
	}
}
