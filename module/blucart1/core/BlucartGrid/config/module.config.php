<?php

namespace BlucartGrid;

return array(
    'controllers' => array(
        'invokables' => array(
            'BlucartGrid\Controller\Index' => 'BlucartGrid\Controller\IndexController'
        )
    ),
    'router' => array(
        'routes' => array(
            'blucart-grid' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/blucart-grid[/:action][/:eventname]',
                    'constraints' => array(
                        'eventname' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => __NAMESPACE__ . '\Controller\Index',
                        'action' => 'index'
                    )
                )
            ),
        )
    ),
    'service_manager' => array(
        'invokables' => array(
            //'BlucartGrid\Option\BlucartGrid' => 'BlucartGrid\Option\BlucartGrid' ,
            'BlucartGrid\Option\Helper\Binder' => 'BlucartGrid\Option\Helper\Binder',
            'BlucartGrid\Option\Helper\FormResult' => 'BlucartGrid\Option\Helper\FormResult'
        )
    ),
    'view_helpers' => array(
        'invokables' => array(
            'blucartGrid' => 'BlucartGrid\View\Helper\BlucartGrid'
        )
    ),
    'blucartgrid_options' => array(
        'datasource' => array(
            'read' => array(
                'route' => array(
                    'blucart-grid',
                    array(
                        'action' => 'read'
                    )
                ),
                'url' => '',
                'type' => 'GET',
                'contentType' => 'application/json'
            ),
            'create' => array(
                'route' => array(
                    'blucart-grid',
                    array(
                        'action' => 'create'
                    )
                ),
                'url' => '',
                'type' => 'GET',
                'contentType' => 'application/json'
            ),
            'update' => array(
                'route' => array(
                    'blucart-grid',
                    array(
                        'action' => 'update'
                    )
                ),
                'url' => '',
                'type' => 'GET',
                'contentType' => 'application/json'
            ),
            'destroy' => array(
                'route' => array(
                    'blucart-grid',
                    array(
                        'action' => 'destroy'
                    )
                ),
                'url' => '',
                'type' => 'GET',
                'contentType' => 'application/json'
            ),
            'serverPaging' => true,
            'serverAggregates' => true,
            'serverFiltering' => true,
            'serverGrouping' => true,
            'serverSorting' => true
        ),
        'grid' => array(
            'pageable' => array(
                'pageSize' => 30,
                'previousNext' => true,
                'numeric' => true,
                'buttonCount' => 10,
                'input' => true,
                'pageSizes' => array(10, 30, 50, 100,500, 1000, 10000),
                'refresh' => true,
                'info' => true
            ),
            'reorderable' => true,
            'resizable' => true,
            'scrollable' => true,
            'filterable' => true,
            'editable' => array(
                'mode' => 'incell'
            ),
            'selectable' => "multiple, row",
            'sortable' => array(
                'mode' => 'multiple',
                'allowUnsort' => true
            ),
            'navigatable' => true,
            'columnMenu' => true,
            'groupable' => true,
            'detailInit' => 'detailInit',
            'detailTemplateId' => 'detailTemplate',
            'detailUrl' => array(
                'route' => array(
                    'blucart-grid',
                    array(
                        'action' => 'detail'
                    )
                ),
                'url' => '',
            ),
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'BlucartGrid' => __DIR__ . '/../view'
        ),
        'template_map' => array(
        //	'common/sb-form' => __DIR__ . '/../view/common/sb-form.phtml'
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'BlucartGrid' => __DIR__ . '/../public'
            )
        )
    ),
    'kendo_options' => array(
        'include_js' => false,
        'include_css' => false,
        'locale' => false
    ),
);
