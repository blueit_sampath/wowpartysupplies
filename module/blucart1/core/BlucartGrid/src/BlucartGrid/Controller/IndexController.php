<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace BlucartGrid\Controller;

use BlucartGrid\Option\Grid;
use BlucartGrid\Option\DetailContainer;
use BlucartGrid\Option\ResultContainer;
use BlucartGrid\Option\BlucartGridParseResult;
use BlucartGrid\Option\BlucartGrid;
use QueryBuilder\Option\QueryBuilder;
use BlucartGrid\Option\BlucartGridResult;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

    public function indexAction() {
        
    }

    public function createAction() {
        $eventName = $this->params()->fromRoute('eventname');
        $grid = $this->getGrid($eventName);
        $grid->prepare();
        $params = array(
            'grid' => $grid
        );
        $queryValues = $this->associateStrictDataValues($grid, $this->params()->fromQuery());
        $grid->getResultObject()->setBaseResults($queryValues);
        $grid->getResultObject()->setResults($queryValues);

        $this->getEventManager()->trigger($eventName . '-create.pre', $this, $params);
        if ($grid->getValidateContainer()->isValid()) {
            $this->getEventManager()->trigger($eventName . '-create.post', $this, $params);
            $jsonModel = new JsonModel(
                    array(
                'data' => $this->removeAssociateStrictDataValues($grid->getResultObject()->getResults())
            ));
        } else {
            $jsonModel = new JsonModel(array(
                'errors' => $grid->getValidateContainer()->getErrorMessages()
            ));
        }

        return $jsonModel;
    }

    public function readAction() {
        $eventName = $this->params()->fromRoute('eventname');
        $grid = $this->getGrid($eventName);
        $grid->prepare();
        $params = array(
            'grid' => $grid
        );
        $this->getEventManager()->trigger($eventName . '-read.pre', $this, $params);
        $grid->getFormResult()->parseResult();
        $this->getEventManager()->trigger($eventName . '-read.post', $this, $params);
        $jsonModel = new JsonModel($grid->getResultObject()->parse());

        return $jsonModel;
    }

    public function updateAction() {
        $eventName = $this->params()->fromRoute('eventname');
        $grid = $this->getGrid($eventName);
        $grid->prepare();
        $params = array(
            'grid' => $grid
        );
        $queryValues = $this->associateStrictDataValues($grid, $this->params()->fromQuery());
        $grid->getResultObject()->setBaseResults($queryValues);
        $grid->getResultObject()->setResults($queryValues);

        $this->getEventManager()->trigger($eventName . '-update.pre', $this, $params);
        if ($grid->getValidateContainer()->isValid()) {
            $this->getEventManager()->trigger($eventName . '-update.post', $this, $params);
            $jsonModel = new JsonModel(array(
                'data' => $this->removeAssociateStrictDataValues($grid->getResultObject()->getResults())
            ));
        } else {
             $jsonModel = new JsonModel(array(
                'errors' => $grid->getValidateContainer()->getErrorMessages()
            ));
        }

        return $jsonModel;
    }

    public function destroyAction() {
        $eventName = $this->params()->fromRoute('eventname');

        $grid = $this->getGrid($eventName);
        $grid->prepare();
        $params = array(
            'grid' => $grid
        );

        $queryValues = $this->associateStrictDataValues($grid, $this->params()->fromQuery());
        $grid->getResultObject()->setBaseResults($queryValues);
        $grid->getResultObject()->setResults($queryValues);

        $this->getEventManager()->trigger($eventName . '-destroy.pre', $this, $params);
        if ($grid->getValidateContainer()->isValid()) {
            $this->getEventManager()->trigger($eventName . '-destroy.post', $this, $params);
            $jsonModel = new JsonModel(
                    array(
                'data' => $this->removeAssociateStrictDataValues($grid->getResultObject()->getResults())
            ));
        } else {
             $jsonModel = new JsonModel(array(
                'errors' => $grid->getValidateContainer()->getErrorMessages()
            ));
        }

        return $jsonModel;
    }

    public function detailAction() {
        $eventName = $this->params()->fromRoute('eventname');

        $grid = $this->getGrid($eventName);
        $grid->prepare();
        $params = array(
            'grid' => $grid
        );
        $queryValues = $this->associateStrictDataValues($grid, $this->params()->fromPost());
        $grid->getResultObject()->setBaseResults($queryValues);
        $grid->getResultObject()->setResults($queryValues);

        $this->getEventManager()->trigger($eventName . '-details', $this, $params);

        $viewModel = new ViewModel(array(
            'grid' => $grid
        ));

        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function exportAction() {
        $data = $this->params()->fromPost('data', array());
        $array = json_decode($data, true);
        $this->arrayToCsvDownload($array, 'export_' . date('d_m_y') . '.csv');
        exit;
    }

    public function arrayToCsvDownload($array, $filename = "export.csv", $delimiter = ",") {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        foreach ($array as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // rewrind the "file" with the csv lines
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachement; filename="' . $filename . '"');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }

    /**
     *
     * @param String $name        	
     * @return \BlucartGrid\Option\Grid
     */
    public function getGrid($name) {
        $grid = new Grid($name);
        $grid->setServiceLocator($this->getServiceLocator());
        return $grid;
    }

    public function associateStrictDataValues(Grid $grid, $queryValues = array()) {
        $columns = $grid->getColumns()->getColumns();

        foreach ($columns as $key => $column) {

            $field = $column->getField();

            $datatype = strtolower($column->getType());
            if (isset($queryValues [$field])) {

                if ($datatype === "boolean" && $queryValues [$field]) {

                    $queryValues [$field] = ($queryValues [$field] === 'true') ? true : false;
                }
                if ($datatype === "number" && ($queryValues [$field] === "")) {
                    $queryValues [$field] == null;
                }
                if ($datatype === "date" && $queryValues [$field]) {

                    $dateTime = new \DateTime ();
                    $dateTime->setTimestamp(strtotime($queryValues [$field]));

                    $queryValues [$field] = $dateTime;
                }
            }
        }

        return $queryValues;
    }

    public function removeAssociateStrictDataValues($queryValues = array()) {
        foreach ($queryValues as $key => $value) {
            if ($value instanceof \DateTime) {
                $queryValues [$key] = $value->format('Y-m-d H:i:s');
            }
        }

        return $queryValues;
    }

}
