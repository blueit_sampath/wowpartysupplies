<?php

namespace BlucartGrid\Event;

use Doctrine\ORM\EntityManager;
use QueryBuilder\Option\QueryHaving\QueryHavingItem;
use QueryBuilder\Option\QueryHaving\QueryHaving;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use Zend\Http\Request;
use QueryBuilder\Option\QueryBuilder;
use BlucartGrid\Option\Grid;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Kendo\Lib\Grid\Option\Column\Columns;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use BlucartGrid\Option\BlucartGridColumnItem;

abstract class AbstractBlucartGridEvent extends AbstractEvent {
	protected $_grid;
	abstract public function getEventName();
	public function getPriority() {
		return null;
	}
	public function attach(EventManagerInterface $events) {
		$priority = $this->getPriority ();
		$eventName = $this->getEventName ();
		
		$sharedEventManager = $events->getSharedManager ();
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-schema.pre', array (
				$this,
				'preSchema' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-schema.post', array (
				$this,
				'postSchema' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-read.pre', array (
				$this,
				'preRead' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-read.post', array (
				$this,
				'postRead' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-create.pre', array (
				$this,
				'preCreate' 
		), $priority );
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-create.post', array (
				$this,
				'postCreate' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-update.pre', array (
				$this,
				'preUpdate' 
		), $priority );
		$this->listeners [] = $sharedEventManager->attach ( '*', $eventName . '-update.post', array (
				$this,
				'postUpdate' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-destroy.pre', array (
				$this,
				'preDestroy' 
		), $priority );
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-destroy.post', array (
				$this,
				'postDestroy' 
		), $priority );
		
		$this->_listeners [] = $sharedEventManager->attach ( '*', $eventName . '-details', array (
				$this,
				'details' 
		), $priority );
		
		return $this;
	}
	public function preCreate($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
		return $this;
	}
	public function postCreate($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
		return $this;
	}

	public function preSchema($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
		
		return $this;
	}
	public function postSchema($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
		return $this;
	}
	public function preRead($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
		
		return $this;
	}
	public function postRead($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
		
		return $this;
	}
	public function preDestroy($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
	}
	public function postDestroy($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
	}
	public function preUpdate($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
	}
	public function postUpdate($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
	}
	public function details($e) {
		$grid = $e->getParam ( 'grid' );
		$this->setGrid ( $grid );
	}
	
	/**
	 *
	 * @return Grid
	 */
	public function getGrid() {
		return $this->_grid;
	}
	/**
	 *
	 * @param Grid $_grid        	
	 */
	public function setGrid($_grid) {
		$this->_grid = $_grid;
		return $this;
	}
	/**
	 *
	 * @return EntityManager
	 */
	public function getEntityManager() {
		$sm = $this->getServiceLocator ();
		return $sm->get ( 'Doctrine\ORM\EntityManager' );
	}
}
