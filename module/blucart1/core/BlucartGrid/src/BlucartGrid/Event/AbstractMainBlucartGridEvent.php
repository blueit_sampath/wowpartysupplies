<?php

namespace BlucartGrid\Event;

use QueryBuilder\Option\QueryFrom\QueryFrom;

use Doctrine\ORM\EntityManager;
use QueryBuilder\Option\QueryHaving\QueryHavingItem;
use QueryBuilder\Option\QueryHaving\QueryHaving;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use Zend\Http\Request;
use QueryBuilder\Option\QueryBuilder;
use BlucartGrid\Option\Grid;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Kendo\Lib\Grid\Option\Column\Columns;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use BlucartGrid\Option\BlucartGridColumnItem;

abstract class AbstractMainBlucartGridEvent extends AbstractBlucartGridEvent {
	protected $_grid;
	protected $_entity = ''; // \Category\Entity\Category
	protected $_entityName = ''; // category
	protected $_detailTemplateName = ''; // adminCategoryImage
	protected $_detailTemplate = ''; // common/admin-category-image
	protected $_columnKeys = array ();
	
	public function getPriority(){
		return 1000;
	}
	
	public function postCreate($e) {
		parent::postCreate ( $e );
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		$params = $resultObject->getResults ();
		$params = $this->parseInsertParams ( $params );
		if ($params) {
			$em = $this->getEntityManager ();
			
			$entity = new $this->_entity ();
			
			foreach ( $this->_columnKeys as $columnKey ) {
				if (isset ( $params [$columnKey] )) {
					
					$entity->$columnKey = $params [$columnKey];
				}
			}
			$params = $this->beforeInsert ( $params, $entity );
			$em->persist ( $entity );
			$em->flush ();
			$this->afterInsert ( $params, $entity );
		}
		return true;
	}
	public function parseInsertParams($params) {
		return $params;
	}
	public function beforeInsert($params, $entity) {
		return $params;
	}
	public function afterInsert($params, $entity) {
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		$params ['id'] = $entity->id;
		$resultObject->setResults ( $params );
		$resultContainer->add ( $this->_entityName, $entity );
	}
	public function postDestroy($e) {
		parent::postDestroy ( $e );
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		$params = $resultObject->getResults ();
		
		if ($params && isset ( $params ['id'] )) {
			$em = $this->getEntityManager ();
			$entity = $em->find ( $this->_entity, $params ['id'] );
			if ($entity) {
				
				$resultContainer->add ( $this->_entityName, $entity );
				$em->remove ( $entity );
				$params = $this->afterDestroy ( $params, $entity );
			}
			$em->flush ();
		}
		return true;
	}
	public function afterDestroy($params, $entity) {
		return $params;
	}
	public function postUpdate($e) {
		parent::postUpdate ( $e );
		$resultObject = $this->getGrid ()->getResultObject ();
		$resultContainer = $this->getGrid ()->getResultContainer ();
		$params = $resultObject->getResults ();
		
		if ($params && isset ( $params ['id'] )) {
			$em = $this->getEntityManager ();
			$entity = $em->find ( $this->_entity, $params ['id'] );
			foreach ( $this->_columnKeys as $columnKey ) {
				if (isset ( $params [$columnKey] )) {
					
					$entity->$columnKey = $params [$columnKey];
				}
			}
			$params = $this->beforeUpdate ( $params, $entity );
			$em->flush ();
			$resultContainer->add ( $this->_entityName, $entity );
			$params = $this->afterUpdate ( $params, $entity );
		}
		return true;
	}
	public function beforeUpdate($params, $entity) {
		return $params;
	}
	public function afterUpdate($params, $entity) {
		return $params;
	}
	public function details($e) {
		parent::details ( $e );
		
		if ($this->_detailTemplate) {
			$detailContainer = $this->getGrid ()->getDetailContainer ();
			$renderer = $this->getServiceLocator ()->get ( 'viewrenderer' );
			$resultObject = $this->getGrid ()->getResultObject ();
			$params = $resultObject->getResults ();
			$item = $detailContainer->add ( $this->_detailTemplateName );
			$renderer->render ( $this->_detailTemplate, array (
					'item' => $item,
					'params' => $params 
			) );
		}
	}
	/**
	 * @return QueryFrom
	 * @see \BlucartGrid\Event\AbstractBlucartGridEvent::preRead()
	 */
	public function preRead($e) {
		parent::preRead ( $e );
		$grid = $this->getGrid ();
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$fromItem = $queryBuilder->addFrom ( $this->_entity, $this->_entityName );
		return $fromItem;
	}
	
	/**
	 *
	 * @return Grid
	 */
	public function getGrid() {
		return $this->_grid;
	}
	/**
	 *
	 * @param Grid $_grid        	
	 */
	public function setGrid($_grid) {
		$this->_grid = $_grid;
		return $this;
	}
	/**
	 *
	 * @return EntityManager
	 */
	public function getEntityManager() {
		$sm = $this->getServiceLocator ();
		return $sm->get ( 'Doctrine\ORM\EntityManager' );
	}
}
