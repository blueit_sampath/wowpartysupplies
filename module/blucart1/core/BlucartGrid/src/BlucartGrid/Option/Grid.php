<?php

namespace BlucartGrid\Option;

use BlucartGrid\Option\Helper\ValidateContainer;
use BlucartGrid\Option\Helper\FormResult;
use BlucartGrid\Option\Helper\Binder;
use BlucartGrid\Option\Helper\ResultContainer;
use BlucartGrid\Option\Helper\DetailContainer;
use QueryBuilder\Option\QueryBuilder;
use BlucartGrid\Option\Helper\Result;
use Kendo\Lib\Grid\Grid as KendoGrid;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Grid extends KendoGrid {
	protected $hasDetail = false;
	protected $eventName;
	protected $resultObject;
	protected $queryBuilder;
	protected $services;
	protected $detailContainer;
	protected $resultContainer;
	protected $formResult;
	protected $validateContainer;
        protected $isExport = true;
	protected $additionalParameters = array ();
	
	
	protected $gridName = '';
	public function prepare() {
		$binder = $this->getBinder ();
		$binder->bind ( $this );
		return $this;
	}
	
	/**
	 *
	 * @return Binder
	 */
	public function getBinder() {
		return $this->getServiceLocator ()->get ( 'BlucartGrid\Option\Helper\Binder' );
	}
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->services = $serviceLocator;
		return $this;
	}
	public function getServiceLocator() {
		return $this->services;
	}
	public function getIsExport() {
            return $this->isExport;
        }

        public function setIsExport($isExport) {
            $this->isExport = $isExport;
            return $this;
        }

        	/**
	 *
	 * @return the $hasDetail
	 */
	public function getHasDetail() {
		return $this->hasDetail;
	}
	
	/**
	 *
	 * @param boolean $hasDetail        	
	 */
	public function setHasDetail($hasDetail) {
		$this->hasDetail = $hasDetail;
		return $this;
	}
	/**
	 *
	 * @return the $eventName
	 */
	public function getEventName() {
		return $this->eventName;
	}
	
	/**
	 *
	 * @param field_type $eventName        	
	 */
	public function setEventName($eventName) {
		$this->eventName = $eventName;
		return $this;
	}
	/**
	 *
	 * @return Result $resultObject
	 */
	public function getResultObject() {
		if (! $this->resultObject) {
			$this->resultObject = new Result ();
		}
		return $this->resultObject;
	}
	
	/**
	 *
	 * @param Result $resultObject        	
	 */
	public function setResultObject($resultObject) {
		$this->resultObject = $resultObject;
		return $this;
	}
	
	/**
	 *
	 * @return QueryBuilder $queryBuilder
	 */
	public function getQueryBuilder() {
		if (! $this->queryBuilder) {
			$this->queryBuilder = $this->getServiceLocator ()->get ( 'QueryBuilder' );
		}
		return $this->queryBuilder;
	}
	
	/**
	 *
	 * @param QueryBuilder $queryBuilder        	
	 */
	public function setQueryBuilder($queryBuilder) {
		$this->queryBuilder = $queryBuilder;
		return $this;
	}
	/**
	 *
	 * @return DetailContainer $detailContainer
	 */
	public function getDetailContainer() {
		if (! $this->detailContainer) {
			$this->detailContainer = new DetailContainer ();
		}
		return $this->detailContainer;
	}
	
	/**
	 *
	 * @param DetailContainer $detailContainer        	
	 */
	public function setDetailContainer($detailContainer) {
		$this->detailContainer = $detailContainer;
		return $this;
	}
	
	/**
	 *
	 * @return ResultContainer $resultContainer
	 */
	public function getResultContainer() {
		if (! $this->resultContainer) {
			$this->resultContainer = new ResultContainer ();
		}
		return $this->resultContainer;
	}
	
	/**
	 *
	 * @param ResultContainer $resultContainer        	
	 */
	public function setResultContainer($resultContainer) {
		$this->resultContainer = $resultContainer;
		return $this;
	}
	/**
	 *
	 * @return FormResult $formResult
	 */
	public function getFormResult() {
		if (! $this->formResult) {
			$this->formResult = $this->getServiceLocator ()->get ( 'BlucartGrid\Option\Helper\FormResult' );
			$this->formResult->setGrid ( $this );
		}
		return $this->formResult;
	}
	
	/**
	 *
	 * @param FormResult $formResult        	
	 */
	public function setFormResult($formResult) {
		$this->formResult = $formResult;
		return $this;
	}
	/**
	 *
	 * @return ValidateContainer $validateContainer
	 */
	public function getValidateContainer() {
		if (! $this->validateContainer) {
			$this->validateContainer = new ValidateContainer ();
		}
		return $this->validateContainer;
	}
	
	/**
	 *
	 * @param ValidateContainer $validateContainer        	
	 */
	public function setValidateContainer($validateContainer) {
		$this->validateContainer = $validateContainer;
		return $this;
	}
	
	/**
	 *
	 * @return string $additionalParameters
	 */
	public function getAdditionalParameters() {
		return $this->additionalParameters;
	}
	
	/**
	 *
	 * @param multitype: $additionalParameters        	
	 * @param
	 *        	class_name
	 */
	public function setAdditionalParameters($additionalParameters) {
		$this->additionalParameters = $additionalParameters;
		return $this;
	}
	public function addAdditionalParameter($key, $value) {
		$this->additionalParameters [$key] = $value;
		return $this;
	}
	public function getAdditionalParameter($key) {
		return $this->additionalParameters [$key];
	}
	public function removeAdditionalParameter($key) {
		unset ( $this->additionalParameters [$key] );
		return $this;
	}
	/**
	 * @return string $gridName
	 */
	public function getGridName() {
		if($this->gridName){
		return $this->gridName;
		}
		return $this->getName();
	}

	/**
	 * @param string $gridName
	 * @param class_name
	 */
	public function setGridName($gridName) {
		$this->gridName = $gridName;
		return $this;
	}
	


	

}
