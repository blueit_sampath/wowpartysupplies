<?php

namespace BlucartGrid\Option\Helper;

use Core\Item\Container\ItemContainer;

class ValidateContainer extends ItemContainer {
	const ERROR = 'error';
	const SUCCESS = 'success';
	const WARNING = 'warning';
	const INFO = 'info';
	
	/**
	 *
	 * @param string $uniqueKey        	
	 * @param string $message        	
	 * @param string $errorType        	
	 * @param integer $weight        	
	 * @return \Common\Form\Option\FormResultItem
	 */
	public function add($uniqueKey, $message, $messageType = self::ERROR, $validState = false,  $weight = null) {
		$item = new ValidateItem ( $uniqueKey );
		$item->setMessageType ( $messageType );
		$item->setMessage ( $message );
		$item->setWeight ( $weight );
		$item->setValidState ( $validState );
	
		$this->addItem ( $item );
		return $item;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return Ambigous <\Core\Item\Item, boolean>
	 */
	public function get($id) {
		return $this->getItem ( $id );
	}
	
	/**
	 *
	 * @return boolean
	 */
	public function isValid() {
		$items = $this->getItems ();
		$result = true;
		foreach ( $items as $item ) {
			$result = ($result && $item->getValidState ());
		}
		return $result;
	}
	
	/**
	 *
	 * @return array
	 */
	public function getErrorMessages() {
		$items = $this->getItems ();
		$array = array ();
		foreach ( $items as $item ) {
			if (!$item->getValidState ()) {
				if($item->getMessage()){
				$array [$item->getId ()] = $item->getMessage ();
				}
			}
		}
		return $array;
	}
}
