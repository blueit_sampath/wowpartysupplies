<?php

namespace BlucartGrid\Option\Helper;

use Core\Item\Container\ItemContainer;

class DetailContainer extends ItemContainer {
	
	
	/**
	 *
	 * @param string $uniqueKey        	
	 * @param string $title        	
	 * @param string $content       	
	 * @param integer $weight        	
	 * @return DetailItem
	 */
	public function add($uniqueKey, $title='', $content = '', $weight = null) {
		$item = new DetailItem ( $uniqueKey );
		$item->setTitle ( $title );
		$item->setContent($content);
		$item->setWeight ( $weight );
		$this->addItem ( $item );
		return $item;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return DetailItem
	 */
	public function get($id) {
		return $this->getItem ( $id );
	}
	
}
