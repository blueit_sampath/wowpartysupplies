<?php

namespace BlucartGrid\Option\Helper;

use BlucartGrid\Option\Grid;
use BlucartGrid\Option\ColumnItem;
use Kendo\Lib\Core\JavascriptFunction;
use Kendo\Lib\Grid\Option\Sortable;
use Kendo\Lib\Grid\Option\Editable;
use Kendo\Lib\Grid\Option\Pageable;
use Kendo\Lib\Datasource\Transport\TransportItem;
use Kendo\Lib\Datasource\Transport\Transports;
use Kendo\Lib\Datasource\Schema\Model\FieldItem;
use Kendo\Lib\Grid\Option\Column\Columns;
use Kendo\Lib\Datasource\Schema\Model\Model;
use Kendo\Lib\Datasource\Schema\Schema;
use Kendo\Lib\Datasource\Datasource;
use Kendo\Lib\Grid\Option\Toolbar\ToolbarItem;
use Kendo\Lib\Grid\Option\Toolbar\Toolbar;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class Binder implements ServiceLocatorAwareInterface, EventManagerAwareInterface {

    protected $_services;
    protected $_events;
    protected $_grid;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->_services = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->_services;
    }

    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class()
        ));
        $this->_events = $events;
        return $this;
    }

    public function getEventManager() {
        if (null === $this->_events) {
            $this->setEventManager(new EventManager());
        }
        return $this->_events;
    }

    public function bind(Grid $grid) {
        $this->setGrid($grid);
        $params = array();
        $params ['grid'] = $grid;
        $name = $grid->getName();
        $this->getEventManager()->trigger($name . '-schema.pre', $this, $params);

        $config = $this->getBlucartGridConfig();
        $dataSource = $this->getDatasource($grid);

        if ($dataSource instanceof Datasource) {

            $schema = $this->getSchema($dataSource);
            if ($schema instanceof Schema) {
                $model = $this->getModel($schema);
                if ($model instanceof Model) {
                    $this->prepareModel($grid, $model);
                }
            }
            $transport = $this->getTransport($dataSource);
            if ($transport instanceof Transports) {
                $this->attachTransports($transport);
            }
            $this->attachAdditionalFromDatasourceConfig($dataSource);
        }

        $this->attachAdditionalFromGridConfig($grid);

        $this->getEventManager()->trigger($name . '-schema.post', $this, $params);
    }

    /**
     *
     * @param Grid $grid        	
     * @return Datasource
     */
    public function getDatasource(Grid $grid) {
        if (!$grid->getDataSource()) {
            $grid->setDataSource(new Datasource());
        }
        return $grid->getDataSource();
    }

    /**
     *
     * @param Datasource $dataSource        	
     * @return Schema
     */
    public function getSchema(Datasource $dataSource) {
        if (!$dataSource->getSchema()) {
            $dataSource->setSchema(new Schema());
        }

        return $dataSource->getSchema();
    }

    /**
     *
     * @param Schema $schema        	
     * @return Model
     */
    public function getModel(Schema $schema) {
        if (!$schema->getModel()) {
            $schema->setModel(new Model());
        }

        return $schema->getModel();
    }

    public function prepareModel(Grid $grid, Model $model) {
        $columns = $grid->getColumns();

        if (!($columns instanceof Columns)) {
            return false;
        }
        $columns = $columns->getColumns();
        $id = $model->getId();

        if (!$id) {

            /* @var $column BlucartGrid\Option\BlucartGridColumnItem */
            foreach ($columns as $column) {

                if ($column->getIsPrimary()) {
                    $model->setId($column->getField());
                }
            }
        }

        if (!$model->getFields()) {
            /* @var $column BlucartGrid\Option\BlucartGridColumnItem */
            foreach ($columns as $column) {
                if ($column instanceof ColumnItem) {
                    if ($column->getField()) {
                        $fieldItem = new FieldItem ();
                        $fieldItem->setType($column->getType());

                        $nullable = $column->getNullable();
                        if ($nullable !== null) {
                            $fieldItem->setNullable($nullable);
                        }

                        $validation = $column->getValidation();
                        if ($validation !== null) {
                            $fieldItem->setValidation($validation);
                        }

                        $defaultValue = $column->getDefaultValue();
                        if ($defaultValue !== null) {
                            $fieldItem->setDefaultValue($defaultValue);
                        }

                        $editable = $column->getEditable();
                        if ($editable !== null) {
                            $fieldItem->setEditable($editable);
                        }
                        $model->addField($column->getField(), $fieldItem);
                    }
                }
            }
        }

        return true;
    }

    /**
     *
     * @param Datasource $dataSource        	
     * @return Transports
     */
    public function getTransport(Datasource $dataSource) {
        if (!$dataSource->getTransport()) {
            $dataSource->setTransport(new Transports());
        }

        return $dataSource->getTransport();
    }

    public function getBlucartGridConfig() {
        $config = $this->getServiceLocator()->get('config');
        return $config ['blucartgrid_options'];
    }

    public function attachTransports(Transports $transport) {
        $config = $this->getBlucartGridConfig();
        $datasourceConfig = $config ['datasource'];
        $urlFunction = $this->getServiceLocator()->get('viewHelperManager')->get('url');

        if (!($transport->getTransport('read'))) {
            $tempConfig = $datasourceConfig ['read'];
            $transportItem = new TransportItem ();
            if ($tempConfig ['route']) {
                $routeName = $tempConfig ['route'] [0];
                $routeOptions = $tempConfig ['route'] [1];
                $routeOptions ['eventname'] = $this->getGrid()->getName();
                $url = $urlFunction($routeName, $routeOptions);
            } else {
                $url = $tempConfig ['url'];
            }

            $grid = $this->getGrid();
            if ($parameters = $grid->getAdditionalParameters()) {
                if (is_array($parameters)) {
                    if (count($parameters)) {
                        $transportItem->setData($parameters);
                    }
                } else {
                    $url .= '?' . $parameters;
                }
            }

            $transportItem->setUrl($url);
            $transportItem->setType($tempConfig ['type']);
            $transportItem->setContentType($tempConfig ['contentType']);
            $transport->addTransport('read', $transportItem);
        }

        if (!($transport->getTransport('create'))) {
            $tempConfig = $datasourceConfig ['create'];
            $transportItem = new TransportItem ();
            if ($tempConfig ['route']) {
                $routeName = $tempConfig ['route'] [0];
                $routeOptions = $tempConfig ['route'] [1];
                $routeOptions ['eventname'] = $this->getGrid()->getName();
                $transportItem->setUrl($urlFunction($routeName, $routeOptions));
            } else {
                $transportItem->setUrl($tempConfig ['url']);
            }
            $transportItem->setType($tempConfig ['type']);
            $transportItem->setContentType($tempConfig ['contentType']);
            $transport->addTransport('create', $transportItem);
        }

        if (!($transport->getTransport('destroy'))) {
            $tempConfig = $datasourceConfig ['destroy'];
            $transportItem = new TransportItem ();
            if ($tempConfig ['route']) {
                $routeName = $tempConfig ['route'] [0];
                $routeOptions = $tempConfig ['route'] [1];
                $routeOptions ['eventname'] = $this->getGrid()->getName();
                $transportItem->setUrl($urlFunction($routeName, $routeOptions));
            } else {
                $transportItem->setUrl($tempConfig ['url']);
            }
            $transportItem->setType($tempConfig ['type']);
            $transportItem->setContentType($tempConfig ['contentType']);
            $transport->addTransport('destroy', $transportItem);
        }

        if (!($transport->getTransport('update'))) {
            $tempConfig = $datasourceConfig ['update'];
            $transportItem = new TransportItem ();
            if ($tempConfig ['route']) {
                $routeName = $tempConfig ['route'] [0];
                $routeOptions = $tempConfig ['route'] [1];
                $routeOptions ['eventname'] = $this->getGrid()->getName();
                $transportItem->setUrl($urlFunction($routeName, $routeOptions));
            } else {
                $transportItem->setUrl($tempConfig ['url']);
            }
            $transportItem->setType($tempConfig ['type']);
            $transportItem->setContentType($tempConfig ['contentType']);
            $transport->addTransport('update', $transportItem);
        }

        $transport->setParameterMap(new JavascriptFunction("function(data) {
		 return eval('('+kendo.stringify(data)+')');
		 }"));
    }

    public function attachAdditionalFromDatasourceConfig(Datasource $dataSource) {
        $config = $this->getBlucartGridConfig();
        $datasourceConfig = $config ['datasource'];

        if ($dataSource->getServerPaging() === null) {
            $dataSource->setServerPaging($datasourceConfig ['serverPaging']);
        }

        if ($dataSource->getServerAggregates() === null) {
            $dataSource->setServerAggregates($datasourceConfig ['serverAggregates']);
        }

        if ($dataSource->getServerFiltering() === null) {
            $dataSource->setServerFiltering($datasourceConfig ['serverFiltering']);
        }

        if ($dataSource->getServerGrouping() === null) {

            $dataSource->setServerGrouping($datasourceConfig ['serverGrouping']);
        }

        if ($dataSource->getServerSorting() === null) {
            $dataSource->setServerSorting($datasourceConfig ['serverSorting']);
        }
    }

    public function attachAdditionalFromGridConfig(Grid $grid) {
        $config = $this->getBlucartGridConfig();
        $gridConfig = $config ['grid'];

        if ($grid->getPageable() === null) {
            $pageable = new Pageable ();
            $pageable->setPageSize($gridConfig ['pageable'] ['pageSize']);
            $pageable->setPageSizes($gridConfig ['pageable'] ['pageSizes']);
            $pageable->setPreviousNext($gridConfig ['pageable'] ['previousNext']);
            $pageable->setNumeric($gridConfig ['pageable'] ['numeric']);
            $pageable->setButtonCount($gridConfig ['pageable'] ['buttonCount']);
            $pageable->setInput($gridConfig ['pageable'] ['input']);
            $pageable->setPageSizes($gridConfig ['pageable'] ['pageSizes']);
            $pageable->setRefresh($gridConfig ['pageable'] ['refresh']);
            $pageable->setInfo($gridConfig ['pageable'] ['info']);


            $grid->setPageable($pageable);
        }

        if ($grid->getReorderable() === null) {
            $grid->setReorderable($gridConfig ['reorderable']);
        }

        if ($grid->getResizable() === null) {
            $grid->setResizable($gridConfig ['resizable']);
        }
        if ($grid->getScrollable() === null) {
            $grid->setScrollable($gridConfig ['scrollable']);
        }

        if ($grid->getFilterable() === null) {
            $grid->setFilterable($gridConfig ['filterable']);
        }

        if ($grid->getEditable() === null) {
            $editable = new Editable ();
            $editable->setMode($gridConfig ['editable'] ['mode']);
            $grid->setEditable($editable);
        }
        if ($grid->getSelectable() === null) {
            $grid->setSelectable($gridConfig ['selectable']);
        }

        if ($grid->getSortable() === null) {
            $sortable = new Sortable ();
            $sortable->setMode($gridConfig ['sortable'] ['mode']);
            $sortable->setAllowUnsort($gridConfig ['sortable'] ['allowUnsort']);
            $grid->setSortable($sortable);
        }
        if ($grid->getGroupable() === null) {
            $grid->setGroupable($gridConfig ['groupable']);
        }
        
         if ($grid->getColumnMenu() === null) {
            $grid->setColumnMenu($gridConfig ['columnMenu']);
        }

        if ($grid->getHasDetail()) {
            if (!($grid->getDetailInit() || $grid->getDetailTemplate())) {
                $grid->setDetailInit(new JavascriptFunction($gridConfig ['detailInit'] . '_' . $grid->getName()));
                $grid->setDetailTemplate(new JavascriptFunction('kendo.template($("#' . $gridConfig ['detailTemplateId'] . '_' . $grid->getName() . '").html())'));
            }
        }

        if ($grid->getIsExport()) {
            $toolbar = $grid->getToolbar();
            if (!$toolbar) {
                $toolbar = new Toolbar ();
                $grid->setToolbar($toolbar);
            }
            
            $toolBarItem = new ToolbarItem ();
            $toolBarItem->setName('export');
            $toolBarItem->setText('Export');
            $toolBarItem->setImageClass('k-icon k-i-seek-s');
            $toolBarItem->setWeight(700);
            $toolbar->addToolbar('export', $toolBarItem);
        }
    }

    /**
     * @return Grid $_grid
     */
    public function getGrid() {
        return $this->_grid;
    }

    /**
     * @param Grid $_grid
     */
    public function setGrid($_grid) {
        $this->_grid = $_grid;
        return $this;
    }

}
