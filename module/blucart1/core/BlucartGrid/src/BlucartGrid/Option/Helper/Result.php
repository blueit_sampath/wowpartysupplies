<?php

namespace BlucartGrid\Option\Helper;

class Result {
	
	protected $_baseResults= array();
	protected $_results = array();
	
	/**
	 * @return the $_baseResults
	 */
	public function getBaseResults() {
		return $this->_baseResults;
	}

	/**
	 * @param field_type $_baseResults
	 */
	public function setBaseResults($_baseResults) {
		$this->_baseResults = $_baseResults;
	}

	/**
	 * @return the $_results
	 */
	public function getResults() {
		
		return $this->_results;
	}

	/**
	 * @param field_type $_results
	 */
	public function setResults($_results) {
		$this->_results = $_results;
	}

	public function parse(){
		$results = $this->getResults();
		array_walk_recursive($results, array($this,'modifyDataValue'));
		return $results;
	}
	
	public function modifyDataValue(&$item, $key){
		if($item instanceof  \DateTime){
			$item = $item->format(\DateTime::ISO8601);
			//$item = "<kendo_script_value>new Date(\"".($item->getTimestamp()*1000 )."\")</kendo_script_value>";
		}
		return true;
	}
	
}
