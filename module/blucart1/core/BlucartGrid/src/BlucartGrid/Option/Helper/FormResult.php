<?php

namespace BlucartGrid\Option\Helper;

use DoctrineExtensions\Paginate\Paginate;

use BlucartGrid\Option\Grid;
use QueryBuilder\Option\QueryHaving\QueryHavingItem;
use QueryBuilder\Option\QueryHaving\QueryHaving;
use QueryBuilder\Option\QueryWhere\QueryWhere;
use QueryBuilder\Option\QueryWhere\QueryWhereItem;
use Zend\Http\Request;
use QueryBuilder\Option\QueryBuilder;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Kendo\Lib\Grid\Option\Column\Columns;
use Core\Event\Core\AbstractEvent;
use Zend\EventManager\AbstractListenerAggregate;
use BlucartGrid\Option\BlucartGridColumnItem;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class FormResult implements ServiceLocatorAwareInterface, EventManagerAwareInterface {
	protected $_grid;
	protected $_resultObject;
	protected $_queryBuilder;
	protected $_eventName;
	protected $_events;
	/**
	 *
	 * @var ServiceLocator
	 */
	protected $_services;
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services;
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (null === $this->_events) {
			$this->setEventManager ( new EventManager () );
		}
		return $this->_events;
	}
	
	/**
	 *
	 * @return QueryBuilder
	 */
	public function getQueryBuilder() {
		if (! $this->_queryBuilder) {
			$this->_queryBuilder = $this->getGrid ()->getQueryBuilder ();
		}
		return $this->_queryBuilder;
	}
	
	/**
	 *
	 * @param QueryBuilder $_queryBuilder        	
	 */
	public function setQueryBuilder($_queryBuilder) {
		$this->_queryBuilder = $_queryBuilder;
		return $this;
	}
	
	/**
	 *
	 * @return Grid
	 */
	public function getGrid() {
		return $this->_grid;
	}
	
	/**
	 *
	 * @param Grid $_grid        	
	 */
	public function setGrid($_grid) {
		$this->_grid = $_grid;
		return $this;
	}
	
	/**
	 *
	 * @return Result $_resultObject
	 */
	public function getResultObject() {
		if (! $this->_resultObject) {
			$this->_resultObject = $this->getGrid ()->getResultObject ();
		}
		return $this->_resultObject;
	}
	
	/**
	 *
	 * @param Result $_resultObject        	
	 */
	public function setResultObject($_resultObject) {
		$this->_resultObject = $_resultObject;
		return $this;
	}
	
	/**
	 *
	 * @return Request
	 */
	public function getRequest() {
		return $this->getServiceLocator ()->get ( 'request' );
	}
	public function parseResult() {
		$queryBuilder = $this->getQueryBuilder ();
		$blucartGridResult = $this->getResultObject ();
		$array = array ();
		$this->attachColumns ();
		$this->attachWheres ();
		$this->attachHavings ();
		$this->attachOrders ();
		
		$queryBuilder->setEventName ( $this->getEventName () );
		$request = $this->getRequest ();
		$skip = $request->getQuery ( 'skip', 0 );
		$take = $request->getQuery ( 'take', PHP_INT_MAX );
		$results = $queryBuilder->executeQuery ( $skip, $take );
		
		$array ['data'] = (array)$results;

              
		$array ['total'] = count($results);
              
		$blucartGridResult->setBaseResults ( $array );
		$blucartGridResult->setResults ( $array );
		$grid = $this->getGrid ();
		$name = $grid->getName ();
		$params = array (
				'grid' => $grid 
		);
		$this->getEventManager ()->trigger ( $name . '-parse.pre', $this, $params );
		$this->parseGroupResults ();
		$this->parseAggregateResults ();
		$this->getEventManager ()->trigger ( $name . '-parse.post', $this, $params );
		return $blucartGridResult;
	}
	protected function parseAggregateResults() {
		$request = $this->getRequest ();
		
		$aggregates = $request->getQuery ( 'aggregate', null );
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ()->getColumns ();
		if (! $aggregates) {
			return false;
		}
		$array = array ();
		foreach ( $aggregates as $aggregate ) {
			$fieldName = $aggregate ['field'];
			$functionName = $aggregate ['aggregate'];
			
			$queryBuilder = $this->getQueryBuilder ();
			$queryBuilderTemp = clone $queryBuilder;
			$queryBuilderTemp->clearColumns ();
			$columnKey = $this->findGridColumnByFieldName ( $fieldName );
			$column = $columns [$columnKey];
			if (! $column) {
				continue;
			}
			$queryBuilderTemp->addColumn ( $functionName . '(' . $columnKey . ')', $fieldName . '_' . $functionName );
			$columns1 = $grid->getColumns ()->getColumns ();
			
			foreach ( $columns1 as $key1 => $column1 ) {
				$queryBuilderTemp->addColumn ( $key1, $column1->getField () );
			}
			
			$queryBuilderTemp->setEventName ( $this->getEventName () . '-group-' . $functionName );
			
			$array [$fieldName] [$functionName] = $queryBuilderTemp->executeSingleScalarQuery ( $functionName );
		}
		$resultObject = $this->getResultObject ();
		$results = $resultObject->getResults ();
		$results ['aggregates'] = $array;
		$resultObject->setResults ( $results );
		$resultObject->setBaseResults ( $results );
	}
	public function attachColumns() {
		$grid = $this->getGrid ();
		$queryBuilder = $this->getQueryBuilder ();
		$columns = $grid->getColumns ()->getColumns ();
		foreach ( $columns as $key => $item ) {
			/* @var BlucartGridColumnItem $item */
			$column = $queryBuilder->addColumn ( $key, $item->getField () );
			$column->setWeight ( $item->getWeight () );
		}
		
		return true;
	}
	protected function attachCountColumns($queryBuilder) {
		$grid = $this->getGrid ();
		
		$columns = $grid->getColumns ()->getColumns ();
		$queryBuilder->addColumn ( 'count(' . $this->getTopMostFrom () . ')', 'total' );
		foreach ( $columns as $key => $item ) {
			
			// if ($item->getIsPrimary () && $item->getIsHaving () ) {
			// $queryBuilder->addColumn ( $key, $item->getField () );
			
			// continue;
			// }
			// /* @var BlucartGridColumnItem $item */
			// if ($item->getIsPrimary ()) {
			// $queryBuilder->addColumn ( "count(" . $key . ")", 'total' );
			// }
			if ($item->getIsHaving ()) {
				$queryBuilder->addColumn ( $key, $item->getField () );
			}
			if ($item->getIsGrouped ()) {
				$queryBuilder->addColumn ( $key, $item->getField () );
			}
		}
		return true;
	}
	public function getTopMostFrom() {
		$queryBuilder = $this->getGrid ()->getQueryBuilder ();
		$frs = $queryBuilder->getFroms ();
		
		$froms = $frs->getFroms ();
		foreach ( $froms as $from ) {
			return $from->getAlias ();
		}
		return false;
		// $fromItem = $queryBuilder->addFrom ( $this->_entity,
	// $this->_entityName );
	}
	protected function attachWheres() {
		$grid = $this->getGrid ();
		$queryBuilder = $this->getQueryBuilder ();
		
		$columns = $grid->getColumns ()->getColumns ();
		
		$request = $this->getRequest ();
		$filters = $request->getQuery ( 'filter', null );
		
		if ($filters) {
			$wheres = $queryBuilder->getWheres ();
			$wheres->setLogic ( $filters ['logic'] );
			$this->attachFilters ( $filters ['filters'], $wheres );
		}
	}
	/**
	 *
	 * @param array $filters        	
	 * @param QueryWhere $wheres        	
	 */
	protected function attachFilters($filters, $wheres) {
		$grid = $this->getGrid ();
		$queryBuilder = $this->getQueryBuilder ();
		
		foreach ( $filters as $key => $filter ) {
			
			if (is_array ( $filter ) && isset ( $filter ['logic'] )) {
				$whereGroup = new QueryWhere ();
				$whereGroup->setLogic ( $filter ['logic'] );
				$key = md5 ( $filter ); // dummy key
				$wheres->addWhereGroup ( $key, $whereGroup );
				$this->attachFilters ( $filter ['filters'], $whereGroup );
			} else {
				$key1 = $key;
				$key = $this->findGridColumnByFieldName ( $filter ['field'] );
				$columns = $grid->getColumns ()->getColumns ();
				$column = $columns [$key];
				
				if (! $column || $column->getIsHaving ()) {
					continue;
				}
				$queryWhereItem = new QueryWhereItem ( $key, $column->getField () . '_' . $key1, $this->getParsedOperator ( $filter ) );
				$queryBuilder->setParameter ( $column->getField () . '_' . $key1, $this->getParsedValue ( $filter ) );
				$wheres->addWhere ( $queryWhereItem );
			}
		}
	}
	/**
	 *
	 * @param string $columnName        	
	 * @return BlucartGridColumnItem boolean
	 */
	protected function findGridColumnByFieldName($columnName) {
		$grid = $this->getGrid ();
		$queryBuilder = $this->getQueryBuilder ();
		$columns = $grid->getColumns ()->getColumns ();
		foreach ( $columns as $key => $item ) {
			/* @var BlucartGridColumnItem $item */
			if ($item->getField () == $columnName) {
				return $key;
			}
		}
		return false;
	}
	protected function attachOrders() {
		$grid = $this->getGrid ();
		$queryBuilder = $this->getQueryBuilder ();
		$request = $this->getRequest ();
		$groups = $request->getQuery ( 'group', null );
		
		if ($groups) {
			foreach ( $groups as $group ) {
				$key = $this->findGridColumnByFieldName ( $group ['field'] );
				$columns = $grid->getColumns ()->getColumns ();
				$column = $columns [$key];
				if (! $column) {
					continue;
				}
				
				$orderItem = $queryBuilder->addOrder ( $group ['field'], $group ['field'], $group ['dir'] );
				$orderItem->setWeight ( 1000 );
			}
		}
		$sorts = $request->getQuery ( 'sort', null );
		
		if ($sorts) {
			foreach ( $sorts as $sort ) {
				$key = $this->findGridColumnByFieldName ( $sort ['field'] );
				$columns = $grid->getColumns ()->getColumns ();
				$column = $columns [$key];
				if (! $column) {
					continue;
				}
				
				$queryBuilder->addOrder ( $sort ['field'], $sort ['field'], $sort ['dir'] );
			}
		}
	}
	protected function getParsedOperator($filter) {
		$field = $filter ['field'];
		$op = $filter ['operator'];
		$value = $filter ['value'];
		$operator = "";
		switch ($op) {
			case "eq" :
			case "==" :
			case "isequalto" :
			case "equals" :
			case "equalto" :
			case "equal" :
				$operator = '=';
				break;
			case "neq" :
			case "!=" :
			case "isnotequalto" :
			case "notequals" :
			case "notequalto" :
			case "ne" :
				$operator = '!=';
				break;
			case "lt" :
			case "<" :
			case "islessthan" :
			case "lessthan" :
			case "less" :
				$operator = '<';
				break;
			
			case "lte" :
			case "<=" :
			case "islessthanorequalto" :
			case "lessthanequal" :
			case "le" :
				$operator = '<=';
				break;
			
			case "gt" :
			case ">" :
			case "isgreaterthan" :
			case "greaterthan" :
			case "greater" :
				$operator = '>';
				break;
			
			case "gte" :
			case ">=" :
			case "isgreaterthanorequalto" :
			case "greaterthanequal" :
			case "ge" :
				$operator = '>=';
				break;
			case "startswith" :
			case "endswith" :
			case "contains" :
			case "substringof" :
				$operator = 'like';
				break;
			
			case "doesnotcontain" :
				
				$operator = 'not like';
				break;
			default :
				$operator = '=';
				break;
		}
		return $operator;
	}
	protected function getParsedValue($filter) {
		$field = $filter ['field'];
		$op = $filter ['operator'];
		$value = $filter ['value'];
		
		$key = $this->findGridColumnByFieldName ( $field );
		$columns = $this->getGrid ()->getColumns ()->getColumns ();
		$column = $columns [$key];
		
		if ($column) {
			if ($column->getType () && (strtolower ( $column->getType () ) === 'date') && $value) {
				$value = date ( 'Y-m-d H:i:s', strtotime ( $value ) );
			}
			if ($column->getType () && (strtolower ( $column->getType () ) === 'boolean') && $value) {
				
				$value = ($value === true or $value === 'true') ? 1 : 0;
			}
		}
		
		switch ($op) {
			
			case "startswith" :
				$value = $value . '%';
				break;
			case "endswith" :
				$value = '%' . $value;
				break;
			case "contains" :
				$value = '%' . $value . '%';
				break;
			case "substringof" :
				$value = '%' . $value . '%';
				break;
			
			case "doesnotcontain" :
				
				$value = '%' . $value . '%';
				break;
			default :
				break;
		}
		return $value;
	}
	protected function attachHavings() {
		$grid = $this->getGrid ();
		$queryBuilder = $this->getQueryBuilder ();
		
		$columns = $grid->getColumns ()->getColumns ();
		
		$request = $this->getRequest ();
		$filters = $request->getQuery ( 'filter', null );
		
		if ($filters) {
			$havings = $queryBuilder->getHavings ();
			$havings->setLogic ( $filters ['logic'] );
			$this->attachHavingFilters ( $filters ['filters'], $havings );
		}
	}
	/**
	 *
	 * @param array $filters        	
	 * @param QueryHaving $havings        	
	 */
	protected function attachHavingFilters($filters, $havings) {
		$grid = $this->getGrid ();
		$queryBuilder = $this->getQueryBuilder ();
		
		foreach ( $filters as $key => $filter ) {
			
			if (is_array ( $filter ) && isset ( $filter ['logic'] )) {
				
				$havingGroup = new QueryHaving ();
				$havingGroup->setLogic ( $filter ['logic'] );
				$key = md5 ( $filter ); // dummy key
				$havings->addHavingGroup ( $key, $havingGroup );
				$this->attachFilters ( $filter ['filters'], $havingGroup );
			} else {
				
				$key1 = $key;
				$key = $this->findGridColumnByFieldName ( $filter ['field'] );
				$columns = $grid->getColumns ()->getColumns ();
				$column = $columns [$key];
				
				if (! $column || ! $column->getIsHaving ()) {
					continue;
				}
				
				$queryHavingItem = new QueryHavingItem ( $key, $column->getField () . '_' . $key1, $this->getParsedOperator ( $filter ) );
				$queryBuilder->setParameter ( $column->getField () . '_' . $key1, $this->getParsedValue ( $filter ) );
				$havings->addHaving ( $queryHavingItem );
			}
		}
	}
	protected function parseGroupResults() {
		$resultObject = $this->getResultObject ();
		$results = $resultObject->getResults ();
		
		$groupResult = array ();
		foreach ( $results ['data'] as $result ) {
			$groupResult = array_merge_recursive ( $groupResult, $this->parseGroupResult ( $result, $groupResult ) );
		}
		$array = $this->formGroupResults ( $groupResult );
		$results ['groups'] = $array;
		$resultObject->setResults ( $results );
		$resultObject->setBaseResults ( $results );
	}
	protected function parseGroupResult($data, $groups) {
		$request = $this->getRequest ();
		$groups = $request->getQuery ( 'group', null );
		$groupArray = array ();
		
		if ($groups) {
			
			$temp = &$groupArray;
			
			foreach ( $groups as $group ) {
				
				$value = $data [$group ['field']];
				if ($value instanceof \DateTime) {
					$value = $value->format ( \DateTime::ISO8601 );
				}
				
				$key = $value . ' '; // cheat to convert string
				unset ( $data [$group ['field']] );
				$temp = &$temp [$key];
			}
			$temp [] = $data;
		}
		return $groupArray;
	}
	protected function formGroupResults($groupResult, $level = 0) {
		$request = $this->getRequest ();
		$groups = $request->getQuery ( 'group', null );
		$array = array ();
		
		if (count ( $groups ) == ($level)) {
			return false;
		}
		if (count ( $groups ) == ($level + 1)) {
			$hasSubgroups = false;
		}
		$group = $groups [$level];
		$hasSubgroups = true;
		if (count ( $groups ) == ($level + 1)) {
			$hasSubgroups = false;
		}
		foreach ( $groupResult as $groupValue => $groupData ) {
			$temp = $this->createGroupResult ( $group, $hasSubgroups, $groupData, $groupValue );
			$temp2 = $this->formGroupResults ( $groupData, ($level + 1) );
			if ($temp2) {
				$temp ['items'] = $temp2;
			} else {
				$temp ['items'] = $groupData;
			}
			$array [] = $temp;
		}
		return $array;
	}
	protected function createGroupResult($group, $hasSubgroups, $data, $value) {
		$array = array ();
		$value = trim ( $value ); // removing the cheat to convert string
		$array ['field'] = $group ['field'];
		$column = $this->findGridColumnByFieldName ( $group ['field'] );
		
		if ($column) {
			
			if (isset ( $group ['aggregates'] )) {
				$array ['aggregates'] = $this->formGroupAggregates ( $group ['aggregates'], $group, $value );
			}
		}
		
		$array ['value'] = $value;
		$array ['hasSubgroups'] = $hasSubgroups;
		$array ['items'] = $data;
		return $array;
	}
	protected function formGroupAggregates($aggregates, $group, $value) {
		$array = array ();
		$queryBuilder = $this->getQueryBuilder ();
		$grid = $this->getGrid ();
		$columns = $grid->getColumns ()->getColumns ();
		
		foreach ( $aggregates as $aggregate ) {
			if (! isset ( $aggregate ['aggregate'] )) {
				continue;
			}
			$functionName = $aggregate ['aggregate'];
			$fieldName = $aggregate ['field'];
			$columnKey = $this->findGridColumnByFieldName ( $group ['field'] );
			
			$column = $columns [$columnKey];
			if (! $column) {
				continue;
			}
			$queryBuilderTemp = null;
			$queryBuilderTemp = clone $queryBuilder;
			$queryBuilderTemp->clearColumns ();
			
			if (! ($column->getIsHaving ())) {
				$wheres = $queryBuilderTemp->getWheres ();
				if ($column->getType () !== "string" && $value === "" && $column->getBelongsToEntity ()) {
					$queryWhereItem = new QueryWhereItem ( $column->getBelongsToEntity (), $group ['field'] . '_' . $functionName, null );
					$wheres->addWhere ( $queryWhereItem );
				} 

				else {
					$queryWhereItem = new QueryWhereItem ( $columnKey, $group ['field'] . '_' . $functionName, '=' );
					$queryBuilderTemp->setParameter ( $group ['field'] . '_' . $functionName, $value );
					$wheres->addWhere ( $queryWhereItem );
				}
			} else {
				
				$havings = $queryBuilderTemp->getHavings ();
				if ($value === '') {
					$wheres = $queryBuilderTemp->getWheres ();
					$queryWhereItem = new QueryWhereItem ( $column->getBelongsToEntity (), $group ['field'] . '_' . $functionName, null );
					$wheres->addWhere ( $queryWhereItem );
				} else {
					$queryHavingItem = new QueryHavingItem ( $columnKey, $group ['field'] . '_' . $functionName, '=' );
					$queryBuilderTemp->setParameter ( $group ['field'] . '_' . $functionName, $value );
					$havings->addHaving ( $queryHavingItem );
				}
			}
			$columnKey = $this->findGridColumnByFieldName ( $fieldName );
			$column = $columns [$columnKey];
			if (! $column) {
				continue;
			}
			$functionName = $this->getAggregateFunctionName ( $functionName );
			
			$column = $queryBuilderTemp->addColumn ( $functionName . '(' . $columnKey . ')', $fieldName . '_' . $functionName );
			$column->setWeight ( 1000 );
			$columns1 = $grid->getColumns ()->getColumns ();
			// foreach ( $columns1 as $key1 => $column1 ) {
			// $queryBuilderTemp->addColumn ( $key1, $column1->getField () );
			// }
			
			$queryBuilderTemp->setEventName ( $this->getEventName () . '-group-' . $functionName );
			$array [$fieldName] [$functionName] = $queryBuilderTemp->executeSingleScalarQuery ( $functionName );
		}
		return $array;
	}
	/**
	 *
	 * @return the $_eventName
	 */
	public function getEventName() {
		if (! $this->_eventName) {
			$this->_eventName = $this->getGrid ()->getName ();
		}
		return $this->_eventName;
	}
	
	/**
	 *
	 * @param field_type $_eventName        	
	 */
	public function setEventName($_eventName) {
		$this->_eventName = $_eventName;
	}
	public function getAggregateFunctionName($functionName) {
		if ($functionName == 'average') {
			$functionName = 'avg';
		}
		return $functionName;
	}
}
