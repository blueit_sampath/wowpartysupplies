<?php

namespace BlucartGrid\Option\Helper;

use Core\Item\Item;

class ResultItem extends Item {
	protected $_value = null;
	
	
	/**
	 * @return the $_value
	 */
	public function getValue() {
		return $this->_value;
	}

	/**
	 * @param NULL $_value
	 */
	public function setValue($_value) {
		$this->_value = $_value;
	}

	
	
}
