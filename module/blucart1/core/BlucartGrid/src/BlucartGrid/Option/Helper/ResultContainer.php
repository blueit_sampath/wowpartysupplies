<?php
namespace BlucartGrid\Option\Helper;
use Core\Item\Container\ItemContainer;

class ResultContainer extends ItemContainer {
	
	/**
	 *
	 * @param string $uniqueKey        	
	 * @param integer $value        	
	 * @param integer $weight        	
	 * @return ResultItem
	 */
	public function add($uniqueKey, $value = null, $weight = null) {
		$item = new ResultItem ( $uniqueKey );
		$item->setValue ( $value );
		$this->addItem ( $item );
		return $item;
	}
	
	/**
	 *
	 * @param string $id        	
	 * @return ResultItem
	 */
	public function get($id) {
		return $this->getItem ( $id );
	}
	
	
}
