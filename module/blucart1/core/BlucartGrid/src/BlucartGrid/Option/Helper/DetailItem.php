<?php

namespace BlucartGrid\Option\Helper;

use Core\Item\Item;

class DetailItem extends Item {
	protected $_title = '';
	protected $_content = '';
	/**
	 * @return the $_title
	 */
	public function getTitle() {
		return $this->_title;
	}

	/**
	 * @return the $_content
	 */
	public function getContent() {
		return $this->_content;
	}

	/**
	 * @param string $_title
	 */
	public function setTitle($_title) {
		$this->_title = $_title;
	}

	/**
	 * @param string $_content
	 */
	public function setContent($_content) {
		$this->_content = $_content;
	}

	
	
}
