<?php

namespace BlucartGrid\Option;

use Kendo\Lib\Grid\Option\Column\ColumnItem as KendoColumnItem;

class ColumnItem extends KendoColumnItem{

	protected $isPrimary = false;
	protected $editable;
	protected $nullable;
	protected $validation;
	protected $_visible = true;
	protected $defaultValue;
	protected $type = 'string';
	protected $isHaving = false;
	protected $isGrouped = false;
	
	protected $belongsToEntity = '';
	
	/**
	 * @return the $isPrimary
	 */
	public function getIsPrimary() {
		return $this->isPrimary;
	}

	/**
	 * @param boolean $isPrimary
	 */
	public function setIsPrimary($isPrimary) {
		$this->isPrimary = $isPrimary;
	}
	/**
	 * @return the $editable
	 */
	public function getEditable() {
		return $this->editable;
	}

	/**
	 * @return the $nullable
	 */
	public function getNullable() {
		return $this->nullable;
	}

	/**
	 * @return the $validation
	 */
	public function getValidation() {
		return $this->validation;
	}

	/**
	 * @return the $visible
	 */
	public function getVisible() {
		return $this->_visible;
	}

	/**
	 * @return the $defaultValue
	 */
	public function getDefaultValue() {
		return $this->defaultValue;
	}

	/**
	 * @return the $type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param field_type $editable
	 */
	public function setEditable($editable) {
		$this->editable = $editable;
	}

	/**
	 * @param field_type $nullable
	 */
	public function setNullable($nullable) {
		$this->nullable = $nullable;
	}

	/**
	 * @param field_type $validation
	 */
	public function setValidation($validation) {
		$this->validation = $validation;
	}

	/**
	 * @param boolean $visible
	 */
	public function setVisible($visible) {
		$this->_visible = $visible;
	}

	/**
	 * @param field_type $defaultValue
	 */
	public function setDefaultValue($defaultValue) {
		$this->defaultValue = $defaultValue;
	}

	/**
	 * @param field_type $type
	 */
	public function setType($type) {
		$this->type = $type;
	}
	/**
	 * @return the $isHaving
	 */
	public function getIsHaving() {
		return $this->isHaving;
	}

	/**
	 * @param boolean $isHaving
	 */
	public function setIsHaving($isHaving) {
		$this->isHaving = $isHaving;
	}
	/**
	 * @return string $belongsToEntity
	 */
	public function getBelongsToEntity() {
		return $this->belongsToEntity;
	}

	/**
	 * @param string $belongsToEntity
	 * @param class_name
	 */
	public function setBelongsToEntity($belongsToEntity) {
		$this->belongsToEntity = $belongsToEntity;
		return $this;
	}

	/**
	 * @return string $isGrouped
	 */
	public function getIsGrouped() {
		return $this->isGrouped;
	}
	
	/**
	 * @param boolean $isGrouped
	 * @param class_name
	 */
	public function setIsGrouped($isGrouped) {
		$this->isGrouped = $isGrouped;
		return $this;
	}

}
