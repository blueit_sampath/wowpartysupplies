<?php

namespace BlucartGrid\View\Helper;

use BlucartGrid\Option\Grid;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\View\Helper\AbstractHelper;

class BlucartGrid extends AbstractHelper implements ServiceLocatorAwareInterface, EventManagerAwareInterface {
	protected $_services;
	protected $_grid;
	protected $_events;
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services->getServiceLocator ();
	}
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (null === $this->_events) {
			$this->setEventManager ( new EventManager () );
		}
		return $this->_events;
	}
	public function __invoke(Grid $grid) {
		$grid->setServiceLocator ( $this->getServiceLocator () );
		$grid->prepare ();
		$this->_grid = $grid;
		return $this;
	}
	public function render() {
		$view = $this->getView ();
		
		$string = $view->kendoGrid ( $this->_grid );
		$request = $this->getServiceLocator ()->get ( 'Request' );
		
		$view->headScript ()->appendFile ( $view->basePath ( 'assets/blucart-grid/js/blucart_grid_wrapper.js' ) );
		
		return $string;
	}
}
