<?php

namespace BlucartGrid;

class Module {

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap($e) {
        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();

        $locale = \Locale::getDefault();
        $locale = str_replace('_', '-', $locale);

        $config = $serviceManager->get('Config');
        $config ['kendo_options'] ['locale'] = $locale;
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

}
