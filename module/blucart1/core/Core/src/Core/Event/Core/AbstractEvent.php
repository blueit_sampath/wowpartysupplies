<?php

namespace Core\Event\Core;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventCollection;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventInterface;

abstract class AbstractEvent implements ServiceLocatorAwareInterface, EventManagerAwareInterface, ListenerAggregateInterface {
	
	/**
	 *
	 * @var ServiceLocator
	 */
	protected $_services;
	protected $_events;
	protected $_event;
	protected $_listeners = array ();
	public function setEventManager(EventManagerInterface $events) {
		$events->setIdentifiers ( array (
				__CLASS__,
				get_called_class () 
		) );
		$this->_events = $events;
		return $this;
	}
	public function getEventManager() {
		if (null === $this->_events) {
			$this->setEventManager ( new EventManager () );
		}
		return $this->_events;
	}
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services;
	}
	
	
	
	public function detach(EventManagerInterface $events) {
		foreach ( $this->_listeners as $index => $listener ) {
			if ($events->detach ( $listener )) {
				unset ( $this->_listeners [$index] );
			}
		}
		return $this;
	}
	
	abstract public function attach(EventManagerInterface $events);
	/**
	 * @return string $_event
	 */
	public function getEvent() {
		return $this->_event;
	}

	/**
	 * @param field_type $_event
	 * @param class_name
	 */
	public function setEvent($_event) {
		$this->_event = $_event;
		return $this;
	}

}
