<?php

namespace Core\Event;
interface LoadEventsInterface {
	public function loadEvents();
}
