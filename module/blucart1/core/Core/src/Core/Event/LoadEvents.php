<?php

namespace Core\Event;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoadEvents implements LoadEventsInterface, ServiceLocatorAwareInterface {
	
	/**
	 *
	 * @var ServiceLocator
	 */
	protected $_services;
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->_services = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->_services;
	}
	public function getEventConfig() {
		$config = $this->getServiceLocator ()->get ( 'config' );
		if (isset ( $config ['events'] )) {
			return $config ['events'];
		}
		return false;
	}
	public function loadEvents() {
		$events = $this->getEventConfig ();
		
		if ($events) {
			foreach ( $events as $event ) {
				$eventObject = $this->getServiceLocator ()->get ( $event );
				$eventObject->getEventManager ()->attachAggregate($eventObject);
			}
		}
		return $this;
	}
}
