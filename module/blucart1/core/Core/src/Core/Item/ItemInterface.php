<?php

namespace Core\Item;

interface ItemInterface {
	public function getId();
	
	public function setWeight($weight=null);
	public function getWeight();
}
