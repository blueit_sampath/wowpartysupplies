<?php

namespace Core\Item\Container;

use Core\Item\Item;

class ItemContainer implements ItemContainerInterface {
	protected $_itemContainer = array ();
	/* (non-PHPdoc)
	 * @see \Core\Item\Container\ItemContainerInterface::addItem()
	 */
	public function addItem($item) {
		$this->_itemContainer [$item->getId ()] = $item;
		return $this;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Core\Item\Container\ItemContainerInterface::getItem()
	 * @return Item
	 */
	public function getItem($id) {
		if (isset ( $this->_itemContainer [$id] )) {
			return $this->_itemContainer [$id];
		}
		return false;
	}
	/* (non-PHPdoc)
	 * @see \Core\Item\Container\ItemContainerInterface::getItems()
	 * @return multitype:\Core\Item\Item
	 */
	public function getItems() {
		return $this->sortArray ( $this->_itemContainer );
	}
	/* (non-PHPdoc)
	 * @see \Core\Item\Container\ItemContainerInterface::removeItem()
	 */
	public function removeItem($id) {
		if (isset ( $this->_itemContainer [$id] )) {
			unset ( $this->_itemContainer [$id] );
			return true;
		}
		return false;
	}
	/* (non-PHPdoc)
	 * @see \Core\Item\Container\ItemContainerInterface::clearItems()
	 */
	public function clearItems() {
		$this->_itemContainer = array ();
		return true;
	}
	/* (non-PHPdoc)
	 * @see \Core\Item\Container\ItemContainerInterface::sortArray()
	 */
	protected function sortArray($itemContainer) {
		if(!is_array($itemContainer)){
		    return $itemContainer;
		}
	    uasort ($itemContainer , array (
				$this,
				'cmp' 
		) );
		return $itemContainer;
	}
	
	/**
	 * @param Item $a
	 * @param Item $b
	 * @return number
	 */
	protected function cmp(Item $a, Item $b) {
		
			return ((int)$a->getWeight () <= (int)$b->getWeight ()) ?  1 : -1;
		
	}
}
