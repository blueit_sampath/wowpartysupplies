<?php

namespace Core\Item\Container;

use Core\Item\ItemInterface;

interface ItemContainerInterface {
	public function addItem($item);
	public function getItem($id);
	public function getItems();
	public function removeItem($id);
	public function clearItems();

}
