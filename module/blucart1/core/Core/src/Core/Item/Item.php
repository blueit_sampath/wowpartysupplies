<?php

namespace Core\Item;

class Item implements ItemInterface {
	protected $_id;
	protected $_weight = null;
		
	
	public function __construct($id){
		$this->_id = $id;
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function setWeight($weight = null) {
		$this->_weight = $weight;
		return $this;
	}
	public function getWeight() {
		return $this->_weight;
	}
	
	protected function sortArray($array) {
		if(!is_array($array)){
			return $array;
		}
		uasort ( $array, array (
		$this,
		'cmp'
				) );
				return $array;
	}
	protected function cmp($a, $b) {
		return (( int ) $a->getWeight () <= ( int ) $b->getWeight ()) ? 1 : - 1;
	}
}
