<?php

namespace Core;

use Zend\ModuleManager\ModuleManager;
use Config\Api\ConfigApi;
use Mail\Option\Mail;
use Doctrine\ORM\EntityManager;

class Functions {

    protected static $_sm;
    protected static $_config;

    public static function setServiceLocator($_sm) {
        static::$_sm = $_sm;
    }

    public static function getServiceLocator() {
        return static::$_sm;
    }

    /**
     *
     * @return ModuleManager
     */
    public static function getModuleManager() {
        return static::getServiceLocator()->get('ModuleManager');
    }

    public static function hasModule($name) {
        $mm = static::getModuleManager();
        return $mm->getModule($name);
    }

    /* flash messages */

    public static function getFlashMessenger() {
        return static::getServiceLocator()->get('ControllerPluginManager')->get('flashmessenger');
    }

    public static function addMessage($message = "", $messageType = 'error') {
        $message = htmlspecialchars($message);
        return static::getFlashMessenger()->setNamespace($messageType)->addMessage($message);
    }

    public static function addErrorMessage($message = "") {
        return static::addMessage($message, 'error');
    }

    public static function addSuccessMessage($message = "") {
        
        return static::addMessage($message, 'success');
    }

    public static function addWarningMessage($message = '') {
        return static::addMessage($message, 'warning');
    }

    public function addInformationMessage($message = "") {
        return static::addMessage($message, 'info');
    }

    /* flash messages */

    /**
     *
     * @return EntityManager
     */
    public static function getEntityManager() {
        $sm = static::getServiceLocator();
        return $sm->get('Doctrine\ORM\EntityManager');
    }

    public static function fromRoute($key, $defaultValue = null) {
        $sm = static::getServiceLocator();
        if ($sm->get('Application')->getMvcEvent()->getRouteMatch()) {
            return $sm->get('Application')->getMvcEvent()->getRouteMatch()->getParam($key, $defaultValue);
        }
        return false;
    }

    public static function getRouteParams() {
        $sm = static::getServiceLocator();
        if ($sm->get('Application')->getMvcEvent()->getRouteMatch()) {
            return $sm->get('Application')->getMvcEvent()->getRouteMatch()->getParams();
        }
        return array();
    }

    public static function getCurrentUrl($rawUrl = false) {

        $sm = static::getServiceLocator();

        $request = $sm->get('Request');
        $uri = $request->getUri();
        $path = '';
        if ($uri && $uri->getPath()) {
            $path = $uri->getPath();
        }
        if (!$rawUrl) {
            $urlPlugin = static::getUrlPlugin();
            $routeName = Functions::getMatchedRouteName();
            if ($routeName) {
                $path = $urlPlugin(Functions::getMatchedRouteName(), Functions::getRouteParams());
            }
        }
        return $path;
    }

    public static function setRouteParam($key, $value) {
        $sm = static::getServiceLocator();
        return $sm->get('Application')->getMvcEvent()->getRouteMatch()->setParam($key, $value);
    }

    public static function fromPost($key, $defaultValue = null) {
        $sm = static::getServiceLocator();
        return $sm->get('Request')->getPost($key, $defaultValue);
    }

    public static function fromQuery($key, $defaultValue = null) {
        $sm = static::getServiceLocator();
        return $sm->get('Request')->getQuery($key, $defaultValue);
    }

    public static function getViewHelperManager() {
        $sm = static::getServiceLocator();
        return $sm->get('view_helper_manager');
    }

    public static function getUrlPlugin() {
        $sm = static::getServiceLocator();
        return $sm->get('viewHelperManager')->get('url');
    }

    /**
     *
     * @return Mail
     */
    public static function getMail() {
        $sm = static::getServiceLocator();
        return $sm->get('Mail\Option\Mail');
    }

    public static function getMatchedRoute() {
        $router = static::getServiceLocator()->get('router');
        $request = static::getServiceLocator()->get('request');
        try {
            $routeMatch = $router->match($request);

            if (!is_null($routeMatch)) {

                return $routeMatch;
            }
        } catch (\Exception $e) {
            
        }
        return false;
    }

    public static function getMatchedRouteName() {
        $routeMatch = static::getMatchedRoute();
        if ($routeMatch) {
            return $routeMatch->getMatchedRouteName();
        }
        return false;
    }

    public static function fromCamelCase($input, $seperator = '-') {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches [0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode($seperator, $ret);
    }

    public static function slugify($text) {
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return '';
        }

        return $text;
    }

}
