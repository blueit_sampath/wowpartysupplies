<?php

namespace Core;

use Zend\EventManager\EventInterface as Event;
use Zend\ModuleManager\ModuleManager;
use Config\Api\ConfigApi;

class Module {

   

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap($e) {
        $eventManager = $e->getApplication()->getEventManager();
        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();
        Functions::setServiceLocator($serviceManager);

        /* @var $loadEvents Core\Event\LoadEvents */
        $loadEvents = $serviceManager->get('Core\Event\LoadEvents');
        $loadEvents->loadEvents();
    }

}
