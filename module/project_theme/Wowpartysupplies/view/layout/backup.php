<?php
$leftRegion = $this->block('contentLeftRegion')->render();
$rightRegion = $this->block('contentRightRegion')->render();

$leftBlockSize = 3;
$rightBlockSize = 3;
$contentBlockSize = 12;

if ($leftRegion) {
    $contentBlockSize = $contentBlockSize - $leftBlockSize;
}
if ($rightRegion) {
    $contentBlockSize = $contentBlockSize - $rightBlockSize;
}
?>
<div class="col-md-12 col-sm-12 col-xs-12">
     <?= $this->block('contentHomeTopRegion')->render(); ?> 
</div>

<div class="col-xs-12">
    <div class="row">
        <?= $this->block('contentWrapperTopRegion')->render(); ?>
        <!-- Sidebar starts-->
    </div>
</div>
<div class="clearfix content-wrapper">
    <?php if ($leftRegion) { ?>
        <div class="col-md-<?php echo $leftBlockSize; ?>  col-sm-4"><div class="row clearfix left-region-wrapper"> <?php echo $leftRegion ?></div></div>
    <?php } ?>

    <div class="col-md-<?php echo $contentBlockSize; ?> col-sm-8 col-xs-12 content">
        <div class="clearfix row content-top-region">
            <?= $this->block('contentTopRegion')->render(); ?>
        </div>
        <div class="content-main-region clearfix">

            <?php echo $this->content; ?>
        </div>
        <div class="clearfix content-bottom-region">
            <?= $this->block('contentBottomRegion')->render(); ?>
        </div>
    </div>

    <?php if ($rightRegion) { ?>
        <div class="hidden-xs col-md-3"><div class="row clearfix right-region-wrapper"> <?php echo $rightRegion ?></div></div>
    <?php } ?>
</div>    

<div class="col-xs-12">
    <div class="row">
        <!--Content Ends-->
        <?= $this->block('contentWrapperBottomRegion')->render(); ?>
    </div>
</div>