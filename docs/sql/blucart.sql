-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 12, 2014 at 03:16 PM
-- Server version: 5.5.35-0ubuntu0.13.10.2
-- PHP Version: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blucart`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon`
--

CREATE TABLE IF NOT EXISTS `addon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_file1` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `addon`
--

INSERT INTO `addon` (`id`, `name`, `title`, `description`, `file_id`, `type`, `created_date`, `updated_date`) VALUES
(1, 'brand', 'Brand', NULL, NULL, 'select', '2014-01-13 18:01:57', '2014-01-13 18:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `addon_category`
--

CREATE TABLE IF NOT EXISTS `addon_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `addon_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_product_attribute1` (`addon_id`),
  KEY `fk_attribute_product_product1` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `addon_category`
--

INSERT INTO `addon_category` (`id`, `title`, `addon_id`, `category_id`, `type`, `weight`, `created_date`, `updated_date`) VALUES
(1, NULL, 1, 19, NULL, NULL, '2014-02-24 17:51:08', '2014-02-24 17:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `addon_option`
--

CREATE TABLE IF NOT EXISTS `addon_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `default_adjustment_sell_price` double DEFAULT NULL,
  `default_adjustment_list_price` double DEFAULT NULL,
  `default_adjustment_cost_price` double DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `addon_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_option_attribute1` (`addon_id`),
  KEY `addon_id` (`addon_id`),
  KEY `file_id` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `addon_option`
--

INSERT INTO `addon_option` (`id`, `name`, `title`, `weight`, `default_adjustment_sell_price`, `default_adjustment_list_price`, `default_adjustment_cost_price`, `file_id`, `addon_id`, `created_date`, `updated_date`) VALUES
(1, 'Adidas', 'Adidas', '', 0, 0, 0, NULL, 1, '2014-01-13 18:06:59', '2014-01-13 18:06:59'),
(2, 'Nike', 'Nike', '', 0, 0, 0, NULL, 1, '2014-01-13 18:07:33', '2014-01-13 18:07:33');

-- --------------------------------------------------------

--
-- Table structure for table `addon_option_product`
--

CREATE TABLE IF NOT EXISTS `addon_option_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `addon_option_id` int(11) DEFAULT NULL,
  `adjustment_sell_price` varchar(255) DEFAULT NULL,
  `adjustment_list_price` varchar(255) DEFAULT NULL,
  `adjustment_cost_price` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `is_percentage` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_option_product_file1` (`file_id`),
  KEY `product_id` (`product_id`),
  KEY `addon_option_id` (`addon_option_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `addon_option_product`
--

INSERT INTO `addon_option_product` (`id`, `name`, `addon_option_id`, `adjustment_sell_price`, `adjustment_list_price`, `adjustment_cost_price`, `file_id`, `product_id`, `is_percentage`, `created_date`, `updated_date`) VALUES
(1, 'Adidas', 1, '5', '10', '5', NULL, 647, 1, '2014-01-13 18:09:25', '2014-02-18 17:54:51'),
(2, NULL, 2, NULL, NULL, NULL, NULL, 647, NULL, '2014-01-13 18:09:25', '2014-01-13 18:09:25'),
(3, NULL, 1, NULL, NULL, NULL, NULL, 646, NULL, '2014-02-24 17:42:21', '2014-02-24 17:42:21'),
(4, NULL, 2, NULL, NULL, NULL, NULL, 646, NULL, '2014-02-24 17:42:21', '2014-02-24 17:42:21');

-- --------------------------------------------------------

--
-- Table structure for table `addon_option_product_order`
--

CREATE TABLE IF NOT EXISTS `addon_option_product_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addon_option_product_id` int(11) DEFAULT NULL,
  `order_product_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_main_id` (`order_product_id`),
  KEY `IDX_80AECE7F2E659EFF` (`addon_option_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `addon_product`
--

CREATE TABLE IF NOT EXISTS `addon_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `addon_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `is_required` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_product_attribute1` (`addon_id`),
  KEY `fk_attribute_product_product1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `addon_product`
--

INSERT INTO `addon_product` (`id`, `title`, `addon_id`, `product_id`, `type`, `weight`, `is_required`, `created_date`, `updated_date`) VALUES
(1, 'Brand', 1, 647, 'select', 0, 0, '2014-01-13 18:09:25', '2014-01-13 19:35:55'),
(2, NULL, 1, 646, 'select', NULL, NULL, '2014-02-24 17:42:21', '2014-02-24 17:42:21');

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE IF NOT EXISTS `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_file1` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`id`, `name`, `title`, `description`, `file_id`, `type`, `created_date`, `updated_date`) VALUES
(1, 'Size', 'Size', NULL, NULL, 'select', '2013-11-30 13:19:39', '2014-01-13 17:29:30'),
(2, 'Colour', 'Colour', NULL, NULL, 'select', '2013-11-30 13:20:31', '2014-01-13 17:29:53');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_category`
--

CREATE TABLE IF NOT EXISTS `attribute_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_product_attribute1` (`attribute_id`),
  KEY `fk_attribute_product_product1` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `attribute_category`
--

INSERT INTO `attribute_category` (`id`, `title`, `attribute_id`, `category_id`, `type`, `weight`, `created_date`, `updated_date`) VALUES
(1, NULL, 1, 19, NULL, NULL, '2014-02-24 17:50:10', '2014-02-24 17:50:10'),
(2, NULL, 2, 19, NULL, NULL, '2014-02-24 17:50:10', '2014-02-24 17:50:10');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_option`
--

CREATE TABLE IF NOT EXISTS `attribute_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `default_adjustment_sell_price` varchar(255) DEFAULT NULL,
  `default_adjustment_list_price` varchar(255) DEFAULT NULL,
  `default_adjustment_cost_price` varchar(255) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_option_attribute1` (`attribute_id`),
  KEY `file_id` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `attribute_option`
--

INSERT INTO `attribute_option` (`id`, `name`, `title`, `weight`, `default_adjustment_sell_price`, `default_adjustment_list_price`, `default_adjustment_cost_price`, `attribute_id`, `file_id`, `created_date`, `updated_date`) VALUES
(4, 'Green', 'Green', '100', '7', '7', '7', 2, NULL, '2013-11-30 13:22:21', '2014-01-13 17:29:11'),
(5, 'Red', 'Red', '1000', '5', '5', '5', 2, NULL, '2013-11-30 13:22:48', '2014-01-11 15:57:59'),
(6, 'small', 's', '', '10', '10', '10', 1, NULL, '2014-01-11 15:54:18', '2014-01-11 15:55:30'),
(7, 'large', 'L', '', '10', '10', '10', 1, NULL, '2014-01-11 15:55:11', '2014-01-11 15:55:11'),
(8, 'medium', 'M', '', '12', '12', '12', 1, NULL, '2014-01-11 15:56:23', '2014-01-11 15:56:23');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_option_product`
--

CREATE TABLE IF NOT EXISTS `attribute_option_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `attribute_options` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `adjustment_sell_price` varchar(255) DEFAULT NULL,
  `adjustment_list_price` varchar(255) DEFAULT NULL,
  `adjustment_cost_price` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `is_percentage` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_option_product_file1` (`file_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `attribute_option_product`
--

INSERT INTO `attribute_option_product` (`id`, `name`, `title`, `attribute_options`, `sku`, `adjustment_sell_price`, `adjustment_list_price`, `adjustment_cost_price`, `file_id`, `product_id`, `is_percentage`, `created_date`, `updated_date`) VALUES
(1, 'small(Size)-Red(Colour)', NULL, '6,5', 'ABCDEF1-6-5', '15', '15', '20', NULL, 646, NULL, '2014-02-15 13:03:05', '2014-02-15 13:03:05'),
(2, 'small(Size)-Green(Colour)', NULL, '6,4', 'ABCDEF1-6-4', '17', '17', '24', NULL, 646, NULL, '2014-02-15 13:03:05', '2014-02-15 13:03:05'),
(3, 'large(Size)-Red(Colour)', NULL, '7,5', 'ABCDEF1-7-5', '15', '15', '20', NULL, 646, NULL, '2014-02-15 13:03:05', '2014-02-15 13:03:05'),
(4, 'large(Size)-Green(Colour)', NULL, '7,4', 'ABCDEF1-7-4', '17', '17', '24', NULL, 646, NULL, '2014-02-15 13:03:05', '2014-02-15 13:03:05'),
(5, 'medium(Size)-Red(Colour)', NULL, '8,5', 'ABCDEF1-8-5', '17', '17', '22', NULL, 646, NULL, '2014-02-15 13:03:05', '2014-02-15 13:03:05'),
(6, 'medium(Size)-Green(Colour)', NULL, '8,4', 'ABCDEF1-8-4', '19', '19', '26', NULL, 646, NULL, '2014-02-15 13:03:05', '2014-02-15 13:03:05');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_option_product_order`
--

CREATE TABLE IF NOT EXISTS `attribute_option_product_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_option_product_id` int(11) DEFAULT NULL,
  `order_product_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_main_id` (`order_product_id`),
  KEY `IDX_770F63CEF576382A` (`attribute_option_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_product`
--

CREATE TABLE IF NOT EXISTS `attribute_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_product_attribute1` (`attribute_id`),
  KEY `fk_attribute_product_product1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `attribute_product`
--

INSERT INTO `attribute_product` (`id`, `title`, `attribute_id`, `product_id`, `type`, `weight`, `created_date`, `updated_date`) VALUES
(5, NULL, 1, 646, 'select', NULL, '2014-02-15 13:03:05', '2014-02-15 13:03:05'),
(6, NULL, 2, 646, 'select', NULL, '2014-02-15 13:03:05', '2014-02-15 13:03:05');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_banner_file1` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `url`, `file_id`, `status`, `weight`, `created_date`, `updated_date`) VALUES
(1, '', 955, 1, '1000', '2014-01-03 17:42:53', '2014-01-03 17:42:53'),
(2, '', 956, 1, '1000', '2014-01-03 17:43:22', '2014-01-03 17:43:22');

-- --------------------------------------------------------

--
-- Table structure for table `banner2`
--

CREATE TABLE IF NOT EXISTS `banner2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_banner_file1` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

CREATE TABLE IF NOT EXISTS `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `include_url` text,
  `exclude_url` text,
  `classes` varchar(255) DEFAULT NULL,
  `display_title` tinyint(1) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `display_to` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `block`
--

INSERT INTO `block` (`id`, `name`, `region`, `weight`, `include_url`, `exclude_url`, `classes`, `display_title`, `title`, `display_to`, `created_date`, `updated_date`) VALUES
(1, 'logoBlock', 'topRegion', 3, '', '', 'col-md-2 col-sm-2', 0, NULL, NULL, '2014-01-03 15:24:08', '2014-01-03 16:39:13'),
(5, 'userAccountMenu', 'topRegion', 1, '', '', 'col-md-4 col-sm-5 kart-links', 0, NULL, NULL, '2014-01-03 16:53:51', '2014-01-03 17:01:27'),
(6, 'bannerBlock', 'headerRegion', NULL, '/', '', '', 0, NULL, NULL, '2014-01-03 17:34:25', '2014-01-03 17:49:55'),
(7, 'productFeatured', 'contentWrapperTopRegion', NULL, '/', '', 'col-xs-12 shop-items', 1, 'Featured Products', NULL, '2014-01-03 17:52:30', '2014-01-03 18:51:09'),
(9, 'newProducts', 'contentWrapperBottomRegion', 2, '/', '', 'col-xs-12 recent-posts shop-items clearfix', 1, '<i class="icon-desktop color"></i> Whats New in Our Company ', NULL, '2014-01-03 20:00:49', '2014-01-04 17:56:41'),
(10, 'footerMenu_staticBlock', 'footerRegion', 2, '', '', 'col-md-4 col-sm-4', 1, '', NULL, '2014-01-03 20:12:20', '2014-01-03 20:13:36'),
(11, 'footerDummyMessage_staticBlock', 'footerRegion', 3, '', '', 'col-md-4 col-sm-4', 0, '', NULL, '2014-01-03 20:12:20', '2014-01-03 20:13:18'),
(12, 'getInTouch_staticBlock', 'footerRegion', 1, '', '', 'col-md-4 col-sm-4', 1, '', NULL, '2014-01-03 20:12:20', '2014-01-03 20:13:49'),
(13, 'newsletterBlock', 'contentWrapperBottomRegion', 1, '', '', 'col-xs-12 cta', 0, '', NULL, '2014-01-03 20:33:25', '2014-01-04 17:56:41'),
(15, 'productFeatured', 'contentLeftRegion', 1, '/category/*\r\n/product/*', '', 'sidebar-items col-xs-12 hidden-xs', 1, 'Featured Products', 0, '2014-01-03 21:46:35', '2014-02-21 16:12:10'),
(18, 'pageTitle', 'contentWrapperTopRegion', NULL, '', '/\r\n/user/login\r\n/user/register\r\n/user/forgotpassword\r\n/user/recoverpassword', 'col-xs-12', 0, '', NULL, '2014-01-03 22:03:07', '2014-01-05 13:59:09'),
(19, 'productRecommended', 'contentWrapperBottomRegion', 3, '/product/*', '', 'col-xs-12 recent-posts shop-items clearfix', 1, '<i class="icon-desktop color"></i> Recommended Products', NULL, '2014-01-04 17:54:27', '2014-01-04 19:49:12'),
(20, 'userAccountMenu', 'contentLeftRegion', 2, '/account*', '', 'col-xs-12 sidey', 0, '', NULL, '2014-01-04 21:17:20', '2014-02-21 13:32:17'),
(21, 'adminOrderSimpleStat', 'adminContentWrapperTopRegion', 5, '/admin', '', 'dashboard_simple_chart col-md-4 col-sm-6 text-center', 0, '', NULL, '2014-01-05 17:34:05', '2014-01-14 16:26:59'),
(22, 'adminOrderSimpleTotalStat', 'adminContentWrapperTopRegion', 4, '/admin', '', 'dashboard_simple_chart col-md-4 col-sm-6 text-center', 0, '', NULL, '2014-01-05 17:34:05', '2014-01-14 16:27:23'),
(23, 'adminDashboardSimpleUserStat', 'adminContentWrapperTopRegion', 3, '/admin', '', 'dashboard_simple_chart col-md-4 col-sm-6 text-center', 0, '', NULL, '2014-01-05 17:44:09', '2014-01-14 16:27:47'),
(24, 'adminDashboardSimpleProductStat', 'adminContentWrapperTopRegion', 2, '/admin', '', 'dashboard_simple_chart col-md-4 col-sm-6 text-center', 0, '', NULL, '2014-01-05 18:03:23', '2014-01-14 16:28:11'),
(26, 'AdminDashboardSimpleOrderSaleStat', 'adminContentWrapperTopRegion', 6, '/admin', '', 'dashboard_simple_chart col-md-4 col-sm-6 text-center', 0, '', NULL, '2014-01-05 18:03:23', '2014-01-14 16:26:31'),
(27, 'adminDashboardOrderSaleGraph', 'adminContentWrapperBottomRegion', 10, '/admin', '', 'col-sm-12 col-md-5', 1, 'Sale', NULL, '2014-01-05 18:26:26', '2014-01-07 12:57:40'),
(28, 'AdminDashboardOrderTables', 'adminContentWrapperBottomRegion', 9, '/admin', '', 'col-sm-12 col-md-7 dashboard_main_content', 1, 'Recent Orders', NULL, '2014-01-05 18:35:29', '2014-01-07 12:57:40'),
(29, 'AdminDashboardUserTables', 'adminContentWrapperBottomRegion', 8, '/admin', '', 'col-sm-12 col-md-7 dashboard_main_content', 1, 'Customers', NULL, '2014-01-05 19:00:49', '2014-01-07 12:57:40'),
(30, 'AdminDashboardUserGraph', 'adminContentWrapperBottomRegion', 7, '/admin', '', 'col-sm-12 col-md-5 dashboard_main_content', 1, 'Customer Stastics', 0, '2014-01-05 19:00:49', '2014-02-18 14:48:11'),
(31, 'AdminDashboardProductTables', 'adminContentWrapperBottomRegion', 5, '/admin', '', 'col-sm-12 col-md-7 dashboard_main_content', 1, 'Products', NULL, '2014-01-05 19:25:56', '2014-01-07 12:57:40'),
(32, 'AdminDashboardProductTopGraph', 'adminContentWrapperBottomRegion', 6, '/admin', '', 'col-sm-12 col-md-5 dashboard_main_content', 1, 'Top Products', 0, '2014-01-05 19:25:56', '2014-02-18 14:48:23'),
(33, 'AdminDashboardDateRangeForm', 'adminContentWrapperTopRegion', 7, '/admin', '', 'col-xs-12 clearfix', 0, '', NULL, '2014-01-06 12:24:10', '2014-01-06 12:41:24'),
(34, 'AdminProductReport', 'adminContentWrapperBottomRegion', 4, '/admin/product/view/*', '', 'col-xs-12', 1, 'Report', NULL, '2014-01-07 11:37:26', '2014-01-07 12:57:40'),
(35, 'AdminUserReport', 'adminContentWrapperBottomRegion', 1, '/admin/user/view/*', '', 'col-xs-12', 1, 'Report', NULL, '2014-01-07 11:37:26', '2014-01-07 12:57:40'),
(36, 'AdminProductViewAttribute', 'adminContentWrapperBottomRegion', 3, '/admin/product/view/*', '', 'col-sm-12', 1, 'Attributes', NULL, '2014-01-07 12:42:30', '2014-01-07 12:57:40'),
(37, 'AdminProductViewAddon', 'adminContentWrapperBottomRegion', 2, '/admin/product/view/*', '', 'col-xs-12', 1, 'Addons', NULL, '2014-01-07 12:56:04', '2014-01-07 12:59:36'),
(38, 'AdminUserViewOrder', 'adminContentWrapperBottomRegion', NULL, '/admin/user/view/*', '', 'col-xs-12', 1, 'Orders', NULL, '2014-01-07 13:14:09', '2014-01-07 13:16:10'),
(39, 'AdminUserViewNewsletter', 'adminContentWrapperBottomRegion', NULL, '/admin/user/view/*', '', 'col-xs-12', 1, 'Subscribed Newsletter', NULL, '2014-01-07 13:14:09', '2014-01-07 13:16:54'),
(40, 'AdminCategoryViewProduct', 'adminContentWrapperBottomRegion', NULL, '/admin/category/view/*', '', 'col-xs-12', 1, 'Products', NULL, '2014-01-07 13:28:13', '2014-01-07 13:29:44'),
(43, 'adminDashboardSimpleCategoryStat', 'adminContentWrapperTopRegion', NULL, '/admin', '', 'dashboard_simple_chart col-md-4 col-sm-6 text-center', 0, '', NULL, '2014-01-14 16:25:54', '2014-01-14 16:28:54'),
(44, 'categorySortProduct', 'contentTopRegion', 2, '/category/*', '', 'col-md-offset-7 col-md-3 col-sm-6', 0, '', NULL, '2014-01-15 15:36:05', '2014-01-22 18:06:31'),
(45, 'categoryAdditionalProduct', 'contentTopRegion', 1, '/category/*', '', 'col-md-2 col-sm-6', 0, '', NULL, '2014-01-15 15:45:47', '2014-01-22 18:06:31'),
(47, 'primaryMenuuu_menuBlock', 'topRegion', 2, '', '', 'col-md-6 col-sm-5', 0, '', 0, '2014-01-20 17:59:29', '2014-01-23 17:34:06'),
(48, 'menuBreadcrumbBlock', 'contentTopRegion', 3, '', '/', 'col-xs-12', 0, '', NULL, '2014-01-22 17:16:59', '2014-01-22 18:05:50'),
(49, 'secondrymenu_menuBlock', 'contentLeftRegion', 3, '/category/*\r\n/product/*', '', 'col-xs-12 sidebar-nav', 0, 'Categories', 0, '2014-02-21 13:09:57', '2014-02-21 15:42:23');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_banner_file1` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `title`, `file_id`, `status`, `weight`, `created_date`, `updated_date`) VALUES
(1, 'HTC', 957, 1, '1000', '2014-01-04 16:51:09', '2014-01-04 16:54:58'),
(2, 'Apple', NULL, 1, '', '2014-01-04 16:52:29', '2014-01-04 16:52:29');

-- --------------------------------------------------------

--
-- Table structure for table `brand_category`
--

CREATE TABLE IF NOT EXISTS `brand_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_product_attribute1` (`brand_id`),
  KEY `fk_attribute_product_product1` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `brand_category`
--

INSERT INTO `brand_category` (`id`, `title`, `brand_id`, `category_id`, `type`, `weight`, `created_date`, `updated_date`) VALUES
(1, NULL, 1, 19, NULL, NULL, '2014-02-24 17:53:09', '2014-02-24 17:53:09'),
(2, NULL, 2, 19, NULL, NULL, '2014-02-24 17:53:09', '2014-02-24 17:53:09');

-- --------------------------------------------------------

--
-- Table structure for table `brand_product`
--

CREATE TABLE IF NOT EXISTS `brand_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` varchar(255) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_category_category1` (`brand_id`),
  KEY `fk_product_category_product1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `brand_product`
--

INSERT INTO `brand_product` (`id`, `weight`, `brand_id`, `product_id`, `created_date`, `updated_date`) VALUES
(1, NULL, 1, 647, '2014-01-04 16:56:21', '2014-01-04 16:56:21'),
(2, NULL, 2, 647, '2014-01-04 16:56:21', '2014-01-04 16:56:21'),
(3, NULL, 1, 646, '2014-02-24 12:21:22', '2014-02-24 12:21:22');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_category1` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `name`, `short_description`, `description`, `status`, `weight`, `parent_id`, `created_date`, `updated_date`) VALUES
(19, 'Notebook', 'notebook', '', '', 1, 40, NULL, '2014-01-02 16:04:46', '2014-02-24 17:28:39'),
(20, 'Laptop', 'laptop', '', '', 1, 30, NULL, '2014-01-02 16:05:53', '2014-02-21 15:38:21'),
(21, 'Desktop', 'desktop', '', '', 1, 10, NULL, '2014-01-22 16:02:10', '2014-02-21 15:21:28'),
(24, 'test', 'test', '', '', 1, 20, NULL, '2014-02-20 15:47:20', '2014-02-21 15:21:28');

-- --------------------------------------------------------

--
-- Table structure for table `category_image`
--

CREATE TABLE IF NOT EXISTS `category_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category_image_category1` (`category_id`),
  KEY `fk_category_image_file1` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(255) DEFAULT NULL,
  `config_value` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=142 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `config_key`, `config_value`, `created_date`, `updated_date`) VALUES
(1, 'SITE_NAME', 'Demo', '2013-11-09 15:52:18', '2014-01-14 19:04:07'),
(2, 'SITE_EMAIL', 'support@blueit-services.co.uk', '2013-11-09 15:52:18', '2013-12-30 14:05:20'),
(3, 'DEFAULT_COUNTRY', 'United Kingdom', '2013-11-09 15:52:18', '2013-11-09 15:52:18'),
(4, 'DEFAULT_TIMEZONE', 'Europe/London', '2013-11-09 15:52:18', '2013-11-09 15:52:18'),
(5, 'STORE_NAME', 'Blucart', '2013-11-09 15:52:18', '2013-11-27 17:03:19'),
(6, 'STORE_ADDRESS_LINE_1', '44', '2013-11-09 15:52:18', '2013-11-09 15:52:18'),
(7, 'STORE_ADDRESS_LINE_2', '', '2013-11-09 15:52:18', '2013-11-09 15:52:18'),
(8, 'STORE_CITY', 'Sky Lines', '2013-11-09 15:52:19', '2013-11-09 15:52:19'),
(9, 'STORE_COUNTRY', 'United Kingdom', '2013-11-09 15:52:19', '2013-11-09 15:52:19'),
(10, 'STORE_STATE', 'Essex', '2013-11-09 15:52:19', '2013-11-09 15:52:19'),
(11, 'STORE_POSTCODE', 'E149TS', '2013-11-09 15:52:19', '2013-11-09 15:52:19'),
(12, 'STORE_PHONE', '02071832157', '2013-11-09 15:52:19', '2013-11-09 15:52:19'),
(13, 'STORE_FAX', '02071832157', '2013-11-09 15:52:19', '2013-11-09 15:52:19'),
(14, 'STORE_WEIGHT', 'gms', '2013-11-09 15:52:19', '2013-11-09 15:52:19'),
(15, 'STORE_LENGTH', 'cms', '2013-11-09 15:52:19', '2013-11-09 15:52:19'),
(16, 'ORDER_NAME', 'Blucart', '2013-11-09 16:16:15', '2013-12-30 14:04:01'),
(17, 'ORDER_EMAIL', 'support@blueit-services.co.uk', '2013-11-09 16:16:15', '2013-12-30 14:04:01'),
(18, 'ORDER_NOTIFICATION_EMAIL', 'support@blueit-services.co.uk', '2013-11-09 16:16:15', '2013-12-30 14:04:01'),
(19, 'ORDER_NOTIFICATION_NAME', 'Orders', '2013-11-09 16:16:15', '2013-11-09 16:16:40'),
(20, 'ORDER_TC', 'I Accept the Terms &amp; Conditions', '2013-11-09 16:16:15', '2013-11-09 16:16:15'),
(21, 'ORDER_INVOICE', '<p><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\r\n<title>Invoice : {{order.id}}</title>\r\n</p>\r\n<table style="width: 600px;" align="center">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<table style="width: 100%;">\r\n<tbody>\r\n<tr>\r\n<td width="300px">\r\n<h1>{{config.STORE_NAME}}</h1>\r\n</td>\r\n<td width="300px">\r\n<table style="font-size: 1.5em; height: 61px;">\r\n<tbody>\r\n<tr>\r\n<td style="color: #f24f4f;">Date:</td>\r\n<td style="color: #4c483d;">{{order.createdDate|date(''d/m/Y'')}}</td>\r\n</tr>\r\n<tr>\r\n<td style="color: #f24f4f;">Phone:</td>\r\n<td style="color: #4c483d;">{{config.STORE_PHONE}}</td>\r\n</tr>\r\n<tr>\r\n<td style="color: #f24f4f;">Email:</td>\r\n<td style="color: #4c483d;">{{config.SITE_EMAIL}}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table style="width: 100%;">\r\n<tbody>\r\n<tr>\r\n<td style="border-right: 0px; border-bottom: solid 1px #333;">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style="border-right: 0px; border-bottom: solid 1px #333;">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style="font-size: 3.2em; paddin-right: 120px;" align="right"><span style="color: #2c4b6a;">Invoice:</span> <span style="color: #ff0000;">#{{order.id}}</span></td>\r\n</tr>\r\n<tr>\r\n<td style="border-bottom: solid 1px #053d70;">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style="border-bottom: solid 1px #053d70;">&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table style="font-size: 1.0em; width: 100%;">\r\n<tbody>\r\n<tr>\r\n<td style="margin-left: 5px;"><span style="color: #f24f4f;">Billing Address</span> <br /> {{billingAddress.firstName }} {{billingAddress.lastName }}<br /> {{ billingAddress.addressLine1 }} {{billingAddress.addressLine2 }}<br /> {{billingAddress.city }}, {{billingAddress.state }}<br /> {{billingAddress.country}}</td>\r\n<td style="margin-left: 5px;"><span style="color: #f24f4f;">Shipping Address</span> <br /> {{shippingAddress.firstName}} {{shippingAddress.lastName}}<br /> {{shippingAddress.addressLine1}} {{shippingAddress.addressLine2 }}<br /> {{shippingAddress.city}}, {{shippingAddress.state}}<br /> {{shippingAddress.country}}</td>\r\n<td style="margin-left: 5px;"><span style="color: #f24f4f;"></span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table style="width: 100%;">\r\n<tbody>\r\n<tr>\r\n<td>{{order.invoiceHtml|raw}}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table style="width: 100%;">\r\n<tbody>\r\n<tr style="color: #ff0000; font-size: 2.0em;">\r\n<td align="right">ANY QUESTIONS? SPEAK TO US {{order.firstName}}!</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '2013-11-09 16:18:26', '2014-01-07 17:00:43'),
(22, 'ORDER_INVOICE_SUBJECT', 'Order Invoice - {{order.id}}', '2013-11-09 16:18:26', '2013-11-09 16:18:26'),
(23, 'ORDER_STATUS_CHECKOUT', '1', '2013-11-09 16:26:03', '2013-11-09 16:26:03'),
(24, 'ORDER_CUSTOMER_NOTIFICATION_PLAINMESSAGE', 'Hello {{order.firstName}}\r\n\r\nThank you for your order!\r\n\r\nWe appreciate the business. Your order has landed and in the processing unit and is just awaiting the green light. We will be back in touch as soon as it is moving into the production department. You should hear from us again shortly!\r\n\r\nWe have attached your invoice and if you need to contact us regarding the goods please quote the Order Reference: {{order.id}}\r\n\r\n \r\n\r\nMany thanks,\r\n\r\n{{config.STORE_NAME}}', '2013-11-09 16:27:25', '2014-01-07 17:02:26'),
(25, 'ORDER_CUSTOMER_NOTIFICATION_HTMLMESSAGE', '<p>Hello {{order.firstName}}</p>\r\n<p>Thank you for your order!</p>\r\n<p>We appreciate the business. Your order has landed and in the processing unit and is just awaiting the green light. We will be back in touch as soon as it is moving into the production department. You should hear from us again shortly!</p>\r\n<p>We have attached your invoice and if you need to contact us regarding the goods please quote the Order Reference: {{order.id}}</p>\r\n<p>&nbsp;</p>\r\n<p>Many thanks,</p>\r\n<p>{{config.STORE_NAME}}</p>', '2013-11-09 16:27:25', '2014-01-07 17:02:26'),
(26, 'ORDER_CUSTOMER_NOTIFICATION_SUBJECT', 'Thank you for Your Order', '2013-11-09 16:27:25', '2014-01-07 17:06:22'),
(27, 'ORDER_ADMIN_NOTIFICATION_PLAINMESSAGE', 'Whey!  You have a new order from the website. Order Id : {{order.id}}\r\n\r\nMany Thanks,\r\n{{config.STORE_NAME}}', '2013-11-09 16:28:01', '2014-01-07 17:05:24'),
(28, 'ORDER_ADMIN_NOTIFICATION_HTMLMESSAGE', 'Whey! You have a new order from the website. Order Id : {{order.id}}\r\n\r\nMany Thanks,\r\n\r\n{{config.STORE_NAME}}', '2013-11-09 16:28:01', '2014-01-07 17:05:24'),
(29, 'ORDER_ADMIN_NOTIFICATION_SUBJECT', 'Notification!! New Order', '2013-11-09 16:28:01', '2014-01-07 17:03:53'),
(30, 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_PLAINMESSAGE', 'Comment Added {{ orderComment.comment }}', '2013-11-09 16:29:41', '2013-11-09 16:29:41'),
(31, 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_HTMLMESSAGE', '<p>Comment Added {{ orderComment.comment }}</p>', '2013-11-09 16:29:41', '2013-11-09 16:29:41'),
(32, 'ORDER_COMMENT_CUSTOMER_NOTIFICATION_SUBJECT', 'Comment Added {{ orderComment.order.id }}', '2013-11-09 16:29:41', '2013-11-09 16:29:41'),
(33, 'ORDER_COMMENT_ADMIN_NOTIFICATION_PLAINMESSAGE', 'Comment Added {{ orderComment.comment }}', '2013-11-09 16:30:09', '2013-11-09 16:30:09'),
(34, 'ORDER_COMMENT_ADMIN_NOTIFICATION_HTMLMESSAGE', 'Comment Added {{ orderComment.comment }}', '2013-11-09 16:30:09', '2013-11-09 16:30:09'),
(35, 'ORDER_COMMENT_ADMIN_NOTIFICATION_SUBJECT', 'Comment Added {{ order.id }}', '2013-11-09 16:30:09', '2013-11-09 16:30:09'),
(36, 'PAYMENT_STATUS_CHECKOUT', '1', '2013-11-09 16:49:29', '2013-11-09 16:49:29'),
(37, 'PAYMENT_STATUS_PENDING', '2', '2013-11-09 16:49:29', '2013-11-09 16:49:29'),
(38, 'PAYMENT_STATUS_CONFIRMED', '3', '2013-11-09 16:49:29', '2013-11-09 16:49:29'),
(39, 'PAYMENT_STATUS_CANCELLED', '4', '2013-11-09 16:49:29', '2013-11-09 16:49:29'),
(40, 'ORDER_PAYMENT_STATUS_PENDING', '2', '2013-11-09 16:49:29', '2013-11-09 16:49:29'),
(41, 'ORDER_PAYMENT_STATUS_CONFIRMED', '3', '2013-11-09 16:49:29', '2013-11-09 16:49:29'),
(42, 'ORDER_PAYMENT_STATUS_CANCELLED', '6', '2013-11-09 16:49:30', '2013-11-09 16:49:30'),
(43, 'SHIPPING_METHOD', 'shippingAmount', '2013-11-09 16:59:24', '2013-11-09 16:59:24'),
(44, 'USER_FROM_EMAIL', 'support@blueit-services.co.uk', '2013-11-09 17:16:00', '2013-12-30 14:03:14'),
(45, 'USER_FROM_NAME', 'Customer Support', '2013-11-09 17:16:00', '2013-11-09 17:16:00'),
(46, 'USER_TC_TEXT', '<p>I Accept the terms and condition</p>', '2013-11-09 17:16:00', '2013-11-09 17:16:00'),
(47, 'USER_REGISTRATION_SUBJECT', 'Welcome {{ user.firstName }}', '2013-11-09 17:16:51', '2014-01-07 17:08:28'),
(48, 'USER_REGISTRATION_BODYHTML', '<p>Hello&nbsp;{{ user.firstName }}</p>\r\n<p>Welcome to TradeShading.com!</p>\r\n<p></p>\r\n<p>Thank you for choosing to register with us please get in touch if we can help at all.</p>\r\n<p></p>\r\n<p>Many thanks,</p>\r\n<p></p>\r\n<p>{{config.STORE_NAME}}</p>', '2013-11-09 17:16:51', '2014-01-07 17:08:14'),
(49, 'USER_REGISTRATION_BODYPLAIN', 'Hello {{ user.firstName }}\r\n\r\nWelcome to TradeShading.com!\r\n\r\nThank you for choosing to register with us please get in touch if we can help at all.\r\n\r\nMany thanks,\r\n\r\n{{config.STORE_NAME}}', '2013-11-09 17:16:51', '2014-01-07 17:08:14'),
(50, 'USER_PASSWORD_RECOVERY_SUBJECT', 'Forgot Password', '2013-11-09 17:18:07', '2013-11-09 17:18:07'),
(51, 'USER_PASSWORD_RECOVERY_BODYHTML', '<p>Hi&nbsp;{{user.firstName}}</p>\r\n<p>Here is your password recovery secret: {{userForgotPassword.secret}}</p>\r\n<p>If you are still struggling to access your account please contact us</p>\r\n<p>Regards,</p>\r\n<p>{{config.STORE_NAME}}</p>', '2013-11-09 17:18:07', '2014-01-07 17:09:46'),
(52, 'USER_PASSWORD_RECOVERY_BODYPLAIN', 'Hi {{user.firstName}}\r\n\r\nHere is your password recovery secret: {{userForgotPassword.secret}}\r\n\r\nIf you are still struggling to access your account please contact us\r\n\r\nRegards,\r\n\r\n{{config.STORE_NAME}}', '2013-11-09 17:18:07', '2014-01-07 17:09:46'),
(53, 'USER_NOTIFICATION_FROM_EMAIL', 'support@blueit-services.co.uk', '2013-11-09 17:18:50', '2014-01-07 17:11:37'),
(54, 'USER_NOTIFICATION_FROM_NAME', 'Customer', '2013-11-09 17:18:50', '2013-11-09 17:18:50'),
(55, 'USER_ADMIN_NOTIFICATION_NOTIFY', '1', '2013-11-09 17:18:50', '2013-11-09 17:18:50'),
(56, 'USER_ADMIN_NOTIFICATION_SUBJECT', 'Welcome Mr. {{ user.firstName }} - Admin notification', '2013-11-09 17:18:50', '2013-11-09 17:18:50'),
(57, 'USER_ADMIN_NOTIFICATION_BODYHTML', '<p>You have a new registration <br />{{user.firstName}} {{user.lastName}}</p>\r\n<p>{{user.email}}</p>\r\n<p>{{user.contactNumber}}</p>\r\n<p>Many thanks,</p>\r\n<p>{{config.STORE_NAME}}</p>', '2013-11-09 17:18:50', '2014-01-07 17:11:15'),
(58, 'USER_ADMIN_NOTIFICATION_BODYPLAIN', 'You have a new registration on TradeShading.com!{{user.firstName}} {{user.lastName}}\r\n{{user.email}}\r\n{{user.contactNumber}}\r\nMany thanks,\r\nGuy Stanley | TradeShading.com\r\nT: 01883 731188 | Live Chat | Facebook | Twitter', '2013-11-09 17:18:50', '2014-01-07 17:11:15'),
(59, 'SITE_LOGO', '', '2013-11-28 14:22:44', '2014-02-19 15:38:34'),
(60, 'FRANCHISE_ROLE', '3', '2013-11-30 18:18:17', '2013-11-30 18:18:17'),
(61, 'VAT_PERCENTAGE', '20', '2013-12-02 14:36:06', '2013-12-02 14:36:06'),
(62, 'VAT_TYPE', '1', '2013-12-02 14:36:06', '2013-12-11 15:50:15'),
(63, 'VAT_ENABLE', '1', '2013-12-02 14:36:06', '2013-12-02 14:36:06'),
(64, 'VAT_TITLE', 'VAT(incl.)', '2013-12-02 14:36:06', '2013-12-11 15:52:12'),
(65, 'PAYMENT_METHOD', 'testPayment', '2013-12-20 18:47:19', '2014-02-19 14:45:06'),
(66, 'PAYMENT_TEST_TITLE', 'Test', '2013-12-20 18:47:45', '2014-02-17 17:48:46'),
(67, 'ORDER_CUSTOMER_NOTIFICATION_ATTACH_INVOICE', '1', '2013-12-30 14:04:15', '2013-12-30 14:04:15'),
(68, 'ORDER_ADMIN_NOTIFICATION_ATTACH_INVOICE', '1', '2013-12-30 14:04:23', '2013-12-30 14:04:23'),
(69, 'CONTACT_FROM_EMAIL', 'admin@blueit-services.co.uk', '2014-01-14 17:44:37', '2014-01-14 17:44:37'),
(70, 'CONTACT_FROM_NAME', 'Admin', '2014-01-14 17:44:37', '2014-01-14 17:44:37'),
(71, 'CONTACT_USER_SUBJECT', 'Thanks for contacting', '2014-01-14 17:45:29', '2014-01-14 17:45:29'),
(72, 'CONTACT_USER_BODYHTML', '<p>Thank You For Your Request!</p>\r\n<p>&nbsp;We will get back to you within 24 hours.</p>\r\n<p></p>', '2014-01-14 17:45:29', '2014-01-14 17:47:22'),
(73, 'CONTACT_USER_BODYPLAIN', 'Thank You For Your Request!\r\n We will get back to you within 24 hours.', '2014-01-14 17:45:29', '2014-01-14 17:47:48'),
(74, 'CONTACT_ADMIN_FROM_EMAIL', 'admin@blueit-services.co.uk', '2014-01-14 17:49:41', '2014-01-14 17:49:41'),
(75, 'CONTACT_ADMIN_FROM_NAME', 'Blueit Services', '2014-01-14 17:49:41', '2014-01-14 17:49:41'),
(76, 'CONTACT_ADMIN_NOTIFY', '1', '2014-01-14 17:49:41', '2014-01-14 17:49:41'),
(77, 'CONTACT_ADMIN_SUBJECT', 'Enquiry', '2014-01-14 17:49:41', '2014-01-14 17:49:41'),
(78, 'CONTACT_ADMIN_BODYHTML', '<p>One of your customer would like to contact you.</p>\r\n<p>Name: {{contact.name}}</p>\r\n<p>Email: {{contact.email}}</p>\r\n<p>Contact Number: {{contact.contactNumber}}</p>\r\n<p>message: {{contact.message}}</p>', '2014-01-14 17:49:41', '2014-01-14 17:49:41'),
(79, 'CONTACT_ADMIN_BODYPLAIN', 'One of your customer would like to contact you.\r\nName: {{contact.name}}\r\nEmail: {{contact.email}}\r\nContact Number: {{contact.contactNumber}}\r\nmessage: {{contact.message}}', '2014-01-14 17:49:41', '2014-01-14 17:49:41'),
(80, 'BREADCRUMB_ENABLE_HOME', '1', '2014-01-23 14:59:49', '2014-01-23 15:13:25'),
(81, 'BREADCRUMB_HOME_LABEL', 'Home', '2014-01-23 14:59:49', '2014-01-23 14:59:49'),
(82, 'BREADCRUMB_LAST_LINK', '0', '2014-01-23 15:13:25', '2014-01-23 15:52:13'),
(83, 'BREADCRUMB_MIN_DEPTH', '', '2014-01-23 15:13:25', '2014-01-23 15:51:56'),
(84, 'BREADCRUMB_MAX_DEPTH', '', '2014-01-23 15:13:25', '2014-01-23 15:51:52'),
(85, 'BREADCRUMB_SEPERATOR', '', '2014-01-23 15:13:25', '2014-01-23 15:51:27'),
(86, 'SEO_DEFAULT_TITLE', 'Blucart Demo', '2014-02-14 11:36:40', '2014-02-15 15:30:59'),
(87, 'SEO_DEFAULT_KEYWORDS', '', '2014-02-14 11:36:40', '2014-02-14 11:37:16'),
(88, 'SEO_DEFAULT_DESCRIPTION', '', '2014-02-14 11:36:40', '2014-02-14 11:37:16'),
(89, 'SEO_DEFAULT_ROBOTS', '', '2014-02-14 11:36:40', '2014-02-14 11:37:16'),
(90, 'SEO_ENABLE', '1', '2014-02-14 11:36:40', '2014-02-15 15:46:47'),
(91, 'SEO_ENABLE_301_REDIRECTION', '1', '2014-02-14 11:36:40', '2014-02-15 17:51:36'),
(92, 'SEO_ENABLE_URLS', '1', '2014-02-14 11:36:40', '2014-02-15 15:47:10'),
(93, 'SEO_PRODUCT_TITLE', '{{product.title}}', '2014-02-15 16:33:51', '2014-02-15 17:40:46'),
(94, 'SEO_PRODUCT_KEYWORDS', '{{product.shortDescription}} \\{\\{page\\}\\}', '2014-02-15 16:33:51', '2014-02-15 17:50:19'),
(95, 'SEO_PRODUCT_DESCRIPTION', '{{product.shortDescription}}', '2014-02-15 16:33:51', '2014-02-15 17:40:46'),
(96, 'SEO_PRODUCT_ROBOTS', '', '2014-02-15 16:33:51', '2014-02-15 17:40:46'),
(97, 'SEO_PRODUCT_URL', '/{{product.title}}', '2014-02-15 16:33:51', '2014-02-15 17:57:16'),
(98, 'SEO_CATEGORY_TITLE', '{{category.title}}', '2014-02-17 12:08:30', '2014-02-17 12:08:30'),
(99, 'SEO_CATEGORY_KEYWORDS', '{{category.shortDescription}}', '2014-02-17 12:08:30', '2014-02-17 12:08:30'),
(100, 'SEO_CATEGORY_DESCRIPTION', '{{category.shortDescription}}', '2014-02-17 12:08:30', '2014-02-17 12:08:30'),
(101, 'SEO_CATEGORY_ROBOTS', '', '2014-02-17 12:08:30', '2014-02-17 12:08:30'),
(102, 'SEO_CATEGORY_URL', '', '2014-02-17 12:08:30', '2014-02-17 12:08:30'),
(103, 'SEO_CATEGORY_PAGINATION_TITLE', '{{category.title}}', '2014-02-17 12:30:16', '2014-02-17 12:30:50'),
(104, 'SEO_CATEGORY_PAGINATION_KEYWORDS', '{{category.shortDescription}}', '2014-02-17 12:30:17', '2014-02-17 12:30:50'),
(105, 'SEO_CATEGORY_PAGINATION_DESCRIPTION', '{{category.shortDescription}} \\{\\{page\\}\\}', '2014-02-17 12:30:17', '2014-02-17 12:30:17'),
(106, 'SEO_CATEGORY_PAGINATION_ROBOTS', '', '2014-02-17 12:30:17', '2014-02-17 12:30:17'),
(107, 'SEO_CATEGORY_PAGINATION_URL', '', '2014-02-17 12:30:17', '2014-02-17 12:30:17'),
(108, 'SEO_STATICPAGE_TITLE', '{{staticPage.title}}', '2014-02-17 12:52:46', '2014-02-17 12:52:46'),
(109, 'SEO_STATICPAGE_KEYWORDS', '', '2014-02-17 12:52:46', '2014-02-17 12:52:46'),
(110, 'SEO_STATICPAGE_DESCRIPTION', '', '2014-02-17 12:52:46', '2014-02-17 12:52:46'),
(111, 'SEO_STATICPAGE_ROBOTS', '', '2014-02-17 12:52:46', '2014-02-17 12:52:46'),
(112, 'SEO_STATICPAGE_URL', '', '2014-02-17 12:52:46', '2014-02-17 12:52:46'),
(138, 'PAYMENT_PAYPAL_TITLE', '', '2014-02-19 17:33:58', '2014-02-19 17:33:58'),
(139, 'PAYMENT_PAYPAL_BUSINESS', 'apple.php-facilitator@gmail.com', '2014-02-19 17:33:58', '2014-02-19 17:33:58'),
(140, 'PAYMENT_PAYPAL_ENVIRONMENT', 'SANDBOX', '2014-02-19 17:33:58', '2014-02-19 17:34:15'),
(141, 'PAYMENT_PAYPAL_CANCEL_URL', '', '2014-02-19 17:33:58', '2014-02-19 17:33:58');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `contact_number`, `email`, `message`, `created_date`, `updated_date`) VALUES
(1, 'Admin Admin', '07885914362', 'admin@blueit-services.co.uk', 'hi how areyou', '2014-01-14 17:41:56', '2014-01-14 17:41:56'),
(2, 'Admin Admin', '07885914362', 'prashi3387@gmail.com', 'hi how are you', '2014-01-14 17:42:54', '2014-01-14 17:42:54');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` varchar(45) DEFAULT NULL,
  `iso3` varchar(45) DEFAULT NULL,
  `fips` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `continent` varchar(45) DEFAULT NULL,
  `currency_code` varchar(45) DEFAULT NULL,
  `currency_name` varchar(45) DEFAULT NULL,
  `phone_prefix` varchar(45) DEFAULT NULL,
  `postal_code` varchar(45) DEFAULT NULL,
  `languages` varchar(45) DEFAULT NULL,
  `geonameid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=895 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `iso3`, `fips`, `name`, `continent`, `currency_code`, `currency_name`, `phone_prefix`, `postal_code`, `languages`, `geonameid`) VALUES
(4, 'AF', 'AFG', 'AF', 'Afghanistan', 'AS', 'AFN', 'Afghani', '93', '', 'fa-AF,ps,uz-AF,tk', '1149361'),
(8, 'AL', 'ALB', 'AL', 'Albania', 'EU', 'ALL', 'Lek', '355', '', 'sq,el', '783754'),
(10, 'AQ', 'ATA', 'AY', 'Antarctica', 'AN', '', '', '', '', '', '6697173'),
(12, 'DZ', 'DZA', 'AG', 'Algeria', 'AF', 'DZD', 'Dinar', '213', '^(d{5})$', 'ar-DZ', '2589581'),
(16, 'AS', 'ASM', 'AQ', 'American Samoa', 'OC', 'USD', 'Dollar', '+1-684', '', 'en-AS,sm,to', '5880801'),
(20, 'AD', 'AND', 'AN', 'Andorra', 'EU', 'EUR', 'Euro', '376', '^(?:AD)*(d{3})$', 'ca,fr-AD,pt', '3041565'),
(24, 'AO', 'AGO', 'AO', 'Angola', 'AF', 'AOA', 'Kwanza', '244', '', 'pt-AO', '3351879'),
(28, 'AG', 'ATG', 'AC', 'Antigua and Barbuda', 'NA', 'XCD', 'Dollar', '+1-268', '', 'en-AG', '3576396'),
(31, 'AZ', 'AZE', 'AJ', 'Azerbaijan', 'AS', 'AZN', 'Manat', '994', '^(?:AZ)*(d{4})$', 'az,ru,hy', '587116'),
(32, 'AR', 'ARG', 'AR', 'Argentina', 'SA', 'ARS', 'Peso', '54', '^([A-Z]d{4}[A-Z]{3})$', 'es-AR,en,it,de,fr', '3865483'),
(36, 'AU', 'AUS', 'AS', 'Australia', 'OC', 'AUD', 'Dollar', '61', '^(d{4})$', 'en-AU', '2077456'),
(40, 'AT', 'AUT', 'AU', 'Austria', 'EU', 'EUR', 'Euro', '43', '^(d{4})$', 'de-AT,hr,hu,sl', '2782113'),
(44, 'BS', 'BHS', 'BF', 'Bahamas', 'NA', 'BSD', 'Dollar', '+1-242', '', 'en-BS', '3572887'),
(48, 'BH', 'BHR', 'BA', 'Bahrain', 'AS', 'BHD', 'Dinar', '973', '^(d{3}d?)$', 'ar-BH,en,fa,ur', '290291'),
(50, 'BD', 'BGD', 'BG', 'Bangladesh', 'AS', 'BDT', 'Taka', '880', '^(d{4})$', 'bn-BD,en', '1210997'),
(51, 'AM', 'ARM', 'AM', 'Armenia', 'AS', 'AMD', 'Dram', '374', '^(d{6})$', 'hy', '174982'),
(52, 'BB', 'BRB', 'BB', 'Barbados', 'NA', 'BBD', 'Dollar', '+1-246', '^(?:BB)*(d{5})$', 'en-BB', '3374084'),
(56, 'BE', 'BEL', 'BE', 'Belgium', 'EU', 'EUR', 'Euro', '32', '^(d{4})$', 'nl-BE,fr-BE,de-BE', '2802361'),
(60, 'BM', 'BMU', 'BD', 'Bermuda', 'NA', 'BMD', 'Dollar', '+1-441', '^([A-Z]{2}d{2})$', 'en-BM,pt', '3573345'),
(64, 'BT', 'BTN', 'BT', 'Bhutan', 'AS', 'BTN', 'Ngultrum', '975', '', 'dz', '1252634'),
(68, 'BO', 'BOL', 'BL', 'Bolivia', 'SA', 'BOB', 'Boliviano', '591', '', 'es-BO,qu,ay', '3923057'),
(70, 'BA', 'BIH', 'BK', 'Bosnia and Herzegovina', 'EU', 'BAM', 'Marka', '387', '^(d{5})$', 'bs,hr-BA,sr-BA', '3277605'),
(72, 'BW', 'BWA', 'BC', 'Botswana', 'AF', 'BWP', 'Pula', '267', '', 'en-BW,tn-BW', '933860'),
(74, 'BV', 'BVT', 'BV', 'Bouvet Island', 'AN', 'NOK', 'Krone', '', '', '', '3371123'),
(76, 'BR', 'BRA', 'BR', 'Brazil', 'SA', 'BRL', 'Real', '55', '^(d{8})$', 'pt-BR,es,en,fr', '3469034'),
(84, 'BZ', 'BLZ', 'BH', 'Belize', 'NA', 'BZD', 'Dollar', '501', '', 'en-BZ,es', '3582678'),
(86, 'IO', 'IOT', 'IO', 'British Indian Ocean Territory', 'AS', 'USD', 'Dollar', '246', '', 'en-IO', '1282588'),
(90, 'SB', 'SLB', 'BP', 'Solomon Islands', 'OC', 'SBD', 'Dollar', '677', '', 'en-SB,tpi', '2103350'),
(92, 'VG', 'VGB', 'VI', 'British Virgin Islands', 'NA', 'USD', 'Dollar', '+1-284', '', 'en-VG', '3577718'),
(96, 'BN', 'BRN', 'BX', 'Brunei', 'AS', 'BND', 'Dollar', '673', '^([A-Z]{2}d{4})$', 'ms-BN,en-BN', '1820814'),
(100, 'BG', 'BGR', 'BU', 'Bulgaria', 'EU', 'BGN', 'Lev', '359', '^(d{4})$', 'bg,tr-BG', '732800'),
(104, 'MM', 'MMR', 'BM', 'Myanmar', 'AS', 'MMK', 'Kyat', '95', '^(d{5})$', 'my', '1327865'),
(108, 'BI', 'BDI', 'BY', 'Burundi', 'AF', 'BIF', 'Franc', '257', '', 'fr-BI,rn', '433561'),
(112, 'BY', 'BLR', 'BO', 'Belarus', 'EU', 'BYR', 'Ruble', '375', '^(d{6})$', 'be,ru', '630336'),
(116, 'KH', 'KHM', 'CB', 'Cambodia', 'AS', 'KHR', 'Riels', '855', '^(d{5})$', 'km,fr,en', '1831722'),
(120, 'CM', 'CMR', 'CM', 'Cameroon', 'AF', 'XAF', 'Franc', '237', '', 'en-CM,fr-CM', '2233387'),
(124, 'CA', 'CAN', 'CA', 'Canada', 'NA', 'CAD', 'Dollar', '1', '^([a-zA-Z]d[a-zA-Z]d[a-zA-Z]d)$', 'en-CA,fr-CA', '6251999'),
(132, 'CV', 'CPV', 'CV', 'Cape Verde', 'AF', 'CVE', 'Escudo', '238', '^(d{4})$', 'pt-CV', '3374766'),
(136, 'KY', 'CYM', 'CJ', 'Cayman Islands', 'NA', 'KYD', 'Dollar', '+1-345', '', 'en-KY', '3580718'),
(140, 'CF', 'CAF', 'CT', 'Central African Republic', 'AF', 'XAF', 'Franc', '236', '', 'fr-CF,ln,kg', '239880'),
(144, 'LK', 'LKA', 'CE', 'Sri Lanka', 'AS', 'LKR', 'Rupee', '94', '^(d{5})$', 'si,ta,en', '1227603'),
(148, 'TD', 'TCD', 'CD', 'Chad', 'AF', 'XAF', 'Franc', '235', '', 'fr-TD,ar-TD,sre', '2434508'),
(152, 'CL', 'CHL', 'CI', 'Chile', 'SA', 'CLP', 'Peso', '56', '^(d{7})$', 'es-CL', '3895114'),
(156, 'CN', 'CHN', 'CH', 'China', 'AS', 'CNY', 'Yuan Renminbi', '86', '^(d{6})$', 'zh-CN,yue,wuu', '1814991'),
(158, 'TW', 'TWN', 'TW', 'Taiwan', 'AS', 'TWD', 'Dollar', '886', '^(d{5})$', 'zh-TW,zh,nan,hak', '1668284'),
(162, 'CX', 'CXR', 'KT', 'Christmas Island', 'AS', 'AUD', 'Dollar', '61', '^(d{4})$', 'en,zh,ms-CC', '2078138'),
(166, 'CC', 'CCK', 'CK', 'Cocos Islands', 'AS', 'AUD', 'Dollar', '61', '', 'ms-CC,en', '1547376'),
(170, 'CO', 'COL', 'CO', 'Colombia', 'SA', 'COP', 'Peso', '57', '', 'es-CO', '3686110'),
(174, 'KM', 'COM', 'CN', 'Comoros', 'AF', 'KMF', 'Franc', '269', '', 'ar,fr-KM', '921929'),
(175, 'YT', 'MYT', 'MF', 'Mayotte', 'AF', 'EUR', 'Euro', '269', '^(d{5})$', 'fr-YT', '1024031'),
(178, 'CG', 'COG', 'CF', 'Republic of the Congo', 'AF', 'XAF', 'Franc', '242', '', 'fr-CG,kg,ln-CG', '2260494'),
(180, 'CD', 'COD', 'CG', 'Democratic Republic of the Congo', 'AF', 'CDF', 'Franc', '243', '', 'fr-CD,ln,kg', '203312'),
(184, 'CK', 'COK', 'CW', 'Cook Islands', 'OC', 'NZD', 'Dollar', '682', '', 'en-CK,mi', '1899402'),
(188, 'CR', 'CRI', 'CS', 'Costa Rica', 'NA', 'CRC', 'Colon', '506', '^(d{4})$', 'es-CR,en', '3624060'),
(191, 'HR', 'HRV', 'HR', 'Croatia', 'EU', 'HRK', 'Kuna', '385', '^(?:HR)*(d{5})$', 'hr-HR,sr', '3202326'),
(192, 'CU', 'CUB', 'CU', 'Cuba', 'NA', 'CUP', 'Peso', '53', '^(?:CP)*(d{5})$', 'es-CU', '3562981'),
(196, 'CY', 'CYP', 'CY', 'Cyprus', 'EU', 'EUR', 'Euro', '357', '^(d{4})$', 'el-CY,tr-CY,en', '146669'),
(203, 'CZ', 'CZE', 'EZ', 'Czech Republic', 'EU', 'CZK', 'Koruna', '420', '^(d{5})$', 'cs,sk', '3077311'),
(204, 'BJ', 'BEN', 'BN', 'Benin', 'AF', 'XOF', 'Franc', '229', '', 'fr-BJ', '2395170'),
(208, 'DK', 'DNK', 'DA', 'Denmark', 'EU', 'DKK', 'Krone', '45', '^(d{4})$', 'da-DK,en,fo,de-DK', '2623032'),
(212, 'DM', 'DMA', 'DO', 'Dominica', 'NA', 'XCD', 'Dollar', '+1-767', '', 'en-DM', '3575830'),
(214, 'DO', 'DOM', 'DR', 'Dominican Republic', 'NA', 'DOP', 'Peso', '+1-809 and 1-829', '^(d{5})$', 'es-DO', '3508796'),
(218, 'EC', 'ECU', 'EC', 'Ecuador', 'SA', 'USD', 'Dollar', '593', '^([a-zA-Z]d{4}[a-zA-Z])$', 'es-EC', '3658394'),
(222, 'SV', 'SLV', 'ES', 'El Salvador', 'NA', 'USD', 'Dollar', '503', '^(?:CP)*(d{4})$', 'es-SV', '3585968'),
(226, 'GQ', 'GNQ', 'EK', 'Equatorial Guinea', 'AF', 'XAF', 'Franc', '240', '', 'es-GQ,fr', '2309096'),
(231, 'ET', 'ETH', 'ET', 'Ethiopia', 'AF', 'ETB', 'Birr', '251', '^(d{4})$', 'am,en-ET,om-ET,ti-ET,so-ET,sid', '337996'),
(232, 'ER', 'ERI', 'ER', 'Eritrea', 'AF', 'ERN', 'Nakfa', '291', '', 'aa-ER,ar,tig,kun,ti-ER', '338010'),
(233, 'EE', 'EST', 'EN', 'Estonia', 'EU', 'EEK', 'Kroon', '372', '^(d{5})$', 'et,ru', '453733'),
(234, 'FO', 'FRO', 'FO', 'Faroe Islands', 'EU', 'DKK', 'Krone', '298', '^(?:FO)*(d{3})$', 'fo,da-FO', '2622320'),
(238, 'FK', 'FLK', 'FK', 'Falkland Islands', 'SA', 'FKP', 'Pound', '500', '', 'en-FK', '3474414'),
(239, 'GS', 'SGS', 'SX', 'South Georgia and the South Sandwich Islands', 'AN', 'GBP', 'Pound', '', '', 'en', '3474415'),
(242, 'FJ', 'FJI', 'FJ', 'Fiji', 'OC', 'FJD', 'Dollar', '679', '', 'en-FJ,fj', '2205218'),
(246, 'FI', 'FIN', 'FI', 'Finland', 'EU', 'EUR', 'Euro', '358', '^(?:FI)*(d{5})$', 'fi-FI,sv-FI,smn', '660013'),
(248, 'AX', 'ALA', '', 'Aland Islands', 'EU', 'EUR', 'Euro', '+358-18', '', 'sv-AX', '661882'),
(250, 'FR', 'FRA', 'FR', 'France', 'EU', 'EUR', 'Euro', '33', '^(d{5})$', 'fr-FR,frp,br,co,ca,eu', '3017382'),
(254, 'GF', 'GUF', 'FG', 'French Guiana', 'SA', 'EUR', 'Euro', '594', '^((97)|(98)3d{2})$', 'fr-GF', '3381670'),
(258, 'PF', 'PYF', 'FP', 'French Polynesia', 'OC', 'XPF', 'Franc', '689', '^((97)|(98)7d{2})$', 'fr-PF,ty', '4020092'),
(260, 'TF', 'ATF', 'FS', 'French Southern Territories', 'AN', 'EUR', 'Euro  ', '', '', 'fr', '1546748'),
(262, 'DJ', 'DJI', 'DJ', 'Djibouti', 'AF', 'DJF', 'Franc', '253', '', 'fr-DJ,ar,so-DJ,aa', '223816'),
(266, 'GA', 'GAB', 'GB', 'Gabon', 'AF', 'XAF', 'Franc', '241', '', 'fr-GA', '2400553'),
(268, 'GE', 'GEO', 'GG', 'Georgia', 'AS', 'GEL', 'Lari', '995', '^(d{4})$', 'ka,ru,hy,az', '614540'),
(270, 'GM', 'GMB', 'GA', 'Gambia', 'AF', 'GMD', 'Dalasi', '220', '', 'en-GM,mnk,wof,wo,ff', '2413451'),
(275, 'PS', 'PSE', 'WE', 'Palestinian Territory', 'AS', 'ILS', 'Shekel', '970', '', 'ar-PS', '6254930'),
(276, 'DE', 'DEU', 'GM', 'Germany', 'EU', 'EUR', 'Euro', '49', '^(d{5})$', 'de', '2921044'),
(288, 'GH', 'GHA', 'GH', 'Ghana', 'AF', 'GHS', 'Cedi', '233', '', 'en-GH,ak,ee,tw', '2300660'),
(292, 'GI', 'GIB', 'GI', 'Gibraltar', 'EU', 'GIP', 'Pound', '350', '', 'en-GI,es,it,pt', '2411586'),
(296, 'KI', 'KIR', 'KR', 'Kiribati', 'OC', 'AUD', 'Dollar', '686', '', 'en-KI,gil', '4030945'),
(300, 'GR', 'GRC', 'GR', 'Greece', 'EU', 'EUR', 'Euro', '30', '^(d{5})$', 'el-GR,en,fr', '390903'),
(304, 'GL', 'GRL', 'GL', 'Greenland', 'NA', 'DKK', 'Krone', '299', '^(d{4})$', 'kl,da-GL,en', '3425505'),
(308, 'GD', 'GRD', 'GJ', 'Grenada', 'NA', 'XCD', 'Dollar', '+1-473', '', 'en-GD', '3580239'),
(312, 'GP', 'GLP', 'GP', 'Guadeloupe', 'NA', 'EUR', 'Euro', '590', '^((97)|(98)d{3})$', 'fr-GP', '3579143'),
(316, 'GU', 'GUM', 'GQ', 'Guam', 'OC', 'USD', 'Dollar', '+1-671', '^(969d{2})$', 'en-GU,ch-GU', '4043988'),
(320, 'GT', 'GTM', 'GT', 'Guatemala', 'NA', 'GTQ', 'Quetzal', '502', '^(d{5})$', 'es-GT', '3595528'),
(324, 'GN', 'GIN', 'GV', 'Guinea', 'AF', 'GNF', 'Franc', '224', '', 'fr-GN', '2420477'),
(328, 'GY', 'GUY', 'GY', 'Guyana', 'SA', 'GYD', 'Dollar', '592', '', 'en-GY', '3378535'),
(332, 'HT', 'HTI', 'HA', 'Haiti', 'NA', 'HTG', 'Gourde', '509', '^(?:HT)*(d{4})$', 'ht,fr-HT', '3723988'),
(334, 'HM', 'HMD', 'HM', 'Heard Island and McDonald Islands', 'AN', 'AUD', 'Dollar', ' ', '', '', '1547314'),
(336, 'VA', 'VAT', 'VT', 'Vatican', 'EU', 'EUR', 'Euro', '379', '', 'la,it,fr', '3164670'),
(340, 'HN', 'HND', 'HO', 'Honduras', 'NA', 'HNL', 'Lempira', '504', '^([A-Z]{2}d{4})$', 'es-HN', '3608932'),
(344, 'HK', 'HKG', 'HK', 'Hong Kong', 'AS', 'HKD', 'Dollar', '852', '', 'zh-HK,yue,zh,en', '1819730'),
(348, 'HU', 'HUN', 'HU', 'Hungary', 'EU', 'HUF', 'Forint', '36', '^(d{4})$', 'hu-HU', '719819'),
(352, 'IS', 'ISL', 'IC', 'Iceland', 'EU', 'ISK', 'Krona', '354', '^(d{3})$', 'is,en,de,da,sv,no', '2629691'),
(356, 'IN', 'IND', 'IN', 'India', 'AS', 'INR', 'Rupee', '91', '^(d{6})$', 'en-IN,hi,bn,te,mr,ta,ur,gu,ml,kn,or,pa,as,ks,', '1269750'),
(360, 'ID', 'IDN', 'ID', 'Indonesia', 'AS', 'IDR', 'Rupiah', '62', '^(d{5})$', 'id,en,nl,jv', '1643084'),
(364, 'IR', 'IRN', 'IR', 'Iran', 'AS', 'IRR', 'Rial', '98', '^(d{10})$', 'fa-IR,ku', '130758'),
(368, 'IQ', 'IRQ', 'IZ', 'Iraq', 'AS', 'IQD', 'Dinar', '964', '^(d{5})$', 'ar-IQ,ku,hy', '99237'),
(372, 'IE', 'IRL', 'EI', 'Ireland', 'EU', 'EUR', 'Euro', '353', '', 'en-IE,ga-IE', '2963597'),
(376, 'IL', 'ISR', 'IS', 'Israel', 'AS', 'ILS', 'Shekel', '972', '^(d{5})$', 'he,ar-IL,en-IL,', '294640'),
(380, 'IT', 'ITA', 'IT', 'Italy', 'EU', 'EUR', 'Euro', '39', '^(d{5})$', 'it-IT,de-IT,fr-IT,sl', '3175395'),
(384, 'CI', 'CIV', 'IV', 'Ivory Coast', 'AF', 'XOF', 'Franc', '225', '', 'fr-CI', '2287781'),
(388, 'JM', 'JAM', 'JM', 'Jamaica', 'NA', 'JMD', 'Dollar', '+1-876', '', 'en-JM', '3489940'),
(392, 'JP', 'JPN', 'JA', 'Japan', 'AS', 'JPY', 'Yen', '81', '^(d{7})$', 'ja', '1861060'),
(398, 'KZ', 'KAZ', 'KZ', 'Kazakhstan', 'AS', 'KZT', 'Tenge', '7', '^(d{6})$', 'kk,ru', '1522867'),
(400, 'JO', 'JOR', 'JO', 'Jordan', 'AS', 'JOD', 'Dinar', '962', '^(d{5})$', 'ar-JO,en', '248816'),
(404, 'KE', 'KEN', 'KE', 'Kenya', 'AF', 'KES', 'Shilling', '254', '^(d{5})$', 'en-KE,sw-KE', '192950'),
(408, 'KP', 'PRK', 'KN', 'North Korea', 'AS', 'KPW', 'Won', '850', '^(d{6})$', 'ko-KP', '1873107'),
(410, 'KR', 'KOR', 'KS', 'South Korea', 'AS', 'KRW', 'Won', '82', '^(?:SEOUL)*(d{6})$', 'ko-KR,en', '1835841'),
(414, 'KW', 'KWT', 'KU', 'Kuwait', 'AS', 'KWD', 'Dinar', '965', '^(d{5})$', 'ar-KW,en', '285570'),
(417, 'KG', 'KGZ', 'KG', 'Kyrgyzstan', 'AS', 'KGS', 'Som', '996', '^(d{6})$', 'ky,uz,ru', '1527747'),
(418, 'LA', 'LAO', 'LA', 'Laos', 'AS', 'LAK', 'Kip', '856', '^(d{5})$', 'lo,fr,en', '1655842'),
(422, 'LB', 'LBN', 'LE', 'Lebanon', 'AS', 'LBP', 'Pound', '961', '^(d{4}(d{4})?)$', 'ar-LB,fr-LB,en,hy', '272103'),
(426, 'LS', 'LSO', 'LT', 'Lesotho', 'AF', 'LSL', 'Loti', '266', '^(d{3})$', 'en-LS,st,zu,xh', '932692'),
(428, 'LV', 'LVA', 'LG', 'Latvia', 'EU', 'LVL', 'Lat', '371', '^(?:LV)*(d{4})$', 'lv,ru,lt', '458258'),
(430, 'LR', 'LBR', 'LI', 'Liberia', 'AF', 'LRD', 'Dollar', '231', '^(d{4})$', 'en-LR', '2275384'),
(434, 'LY', 'LBY', 'LY', 'Libya', 'AF', 'LYD', 'Dinar', '218', '', 'ar-LY,it,en', '2215636'),
(438, 'LI', 'LIE', 'LS', 'Liechtenstein', 'EU', 'CHF', 'Franc', '423', '^(d{4})$', 'de-LI', '3042058'),
(440, 'LT', 'LTU', 'LH', 'Lithuania', 'EU', 'LTL', 'Litas', '370', '^(?:LT)*(d{5})$', 'lt,ru,pl', '597427'),
(442, 'LU', 'LUX', 'LU', 'Luxembourg', 'EU', 'EUR', 'Euro', '352', '^(d{4})$', 'lb,de-LU,fr-LU', '2960313'),
(446, 'MO', 'MAC', 'MC', 'Macao', 'AS', 'MOP', 'Pataca', '853', '', 'zh,zh-MO', '1821275'),
(450, 'MG', 'MDG', 'MA', 'Madagascar', 'AF', 'MGA', 'Ariary', '261', '^(d{3})$', 'fr-MG,mg', '1062947'),
(454, 'MW', 'MWI', 'MI', 'Malawi', 'AF', 'MWK', 'Kwacha', '265', '', 'ny,yao,tum,swk', '927384'),
(458, 'MY', 'MYS', 'MY', 'Malaysia', 'AS', 'MYR', 'Ringgit', '60', '^(d{5})$', 'ms-MY,en,zh,ta,te,ml,pa,th', '1733045'),
(462, 'MV', 'MDV', 'MV', 'Maldives', 'AS', 'MVR', 'Rufiyaa', '960', '^(d{5})$', 'dv,en', '1282028'),
(466, 'ML', 'MLI', 'ML', 'Mali', 'AF', 'XOF', 'Franc', '223', '', 'fr-ML,bm', '2453866'),
(470, 'MT', 'MLT', 'MT', 'Malta', 'EU', 'EUR', 'Euro', '356', '^([A-Z]{3}d{2}d?)$', 'mt,en-MT', '2562770'),
(474, 'MQ', 'MTQ', 'MB', 'Martinique', 'NA', 'EUR', 'Euro', '596', '^(d{5})$', 'fr-MQ', '3570311'),
(478, 'MR', 'MRT', 'MR', 'Mauritania', 'AF', 'MRO', 'Ouguiya', '222', '', 'ar-MR,fuc,snk,fr,mey,wo', '2378080'),
(480, 'MU', 'MUS', 'MP', 'Mauritius', 'AF', 'MUR', 'Rupee', '230', '', 'en-MU,bho,fr', '934292'),
(484, 'MX', 'MEX', 'MX', 'Mexico', 'NA', 'MXN', 'Peso', '52', '^(d{5})$', 'es-MX', '3996063'),
(492, 'MC', 'MCO', 'MN', 'Monaco', 'EU', 'EUR', 'Euro', '377', '^(d{5})$', 'fr-MC,en,it', '2993457'),
(496, 'MN', 'MNG', 'MG', 'Mongolia', 'AS', 'MNT', 'Tugrik', '976', '^(d{6})$', 'mn,ru', '2029969'),
(498, 'MD', 'MDA', 'MD', 'Moldova', 'EU', 'MDL', 'Leu', '373', '^(?:MD)*(d{4})$', 'ro,ru,gag,tr', '617790'),
(499, 'ME', 'MNE', 'MJ', 'Montenegro', 'EU', 'EUR', 'Euro', '381', '^(d{5})$', 'sr,hu,bs,sq,hr,rom', '3194884'),
(500, 'MS', 'MSR', 'MH', 'Montserrat', 'NA', 'XCD', 'Dollar', '+1-664', '', 'en-MS', '3578097'),
(504, 'MA', 'MAR', 'MO', 'Morocco', 'AF', 'MAD', 'Dirham', '212', '^(d{5})$', 'ar-MA,fr', '2542007'),
(508, 'MZ', 'MOZ', 'MZ', 'Mozambique', 'AF', 'MZN', 'Meticail', '258', '^(d{4})$', 'pt-MZ,vmw', '1036973'),
(512, 'OM', 'OMN', 'MU', 'Oman', 'AS', 'OMR', 'Rial', '968', '^(d{3})$', 'ar-OM,en,bal,ur', '286963'),
(516, 'NA', 'NAM', 'WA', 'Namibia', 'AF', 'NAD', 'Dollar', '264', '', 'en-NA,af,de,hz,naq', '3355338'),
(520, 'NR', 'NRU', 'NR', 'Nauru', 'OC', 'AUD', 'Dollar', '674', '', 'na,en-NR', '2110425'),
(524, 'NP', 'NPL', 'NP', 'Nepal', 'AS', 'NPR', 'Rupee', '977', '^(d{5})$', 'ne,en', '1282988'),
(528, 'NL', 'NLD', 'NL', 'Netherlands', 'EU', 'EUR', 'Euro', '31', '^(d{4}[A-Z]{2})$', 'nl-NL,fy-NL', '2750405'),
(530, 'AN', 'ANT', 'NT', 'Netherlands Antilles', 'NA', 'ANG', 'Guilder', '599', '', 'nl-AN,en,es', '3513447'),
(533, 'AW', 'ABW', 'AA', 'Aruba', 'NA', 'AWG', 'Guilder', '297', '', 'nl-AW,es,en', '3577279'),
(540, 'NC', 'NCL', 'NC', 'New Caledonia', 'OC', 'XPF', 'Franc', '687', '^(d{5})$', 'fr-NC', '2139685'),
(548, 'VU', 'VUT', 'NH', 'Vanuatu', 'OC', 'VUV', 'Vatu', '678', '', 'bi,en-VU,fr-VU', '2134431'),
(554, 'NZ', 'NZL', 'NZ', 'New Zealand', 'OC', 'NZD', 'Dollar', '64', '^(d{4})$', 'en-NZ,mi', '2186224'),
(558, 'NI', 'NIC', 'NU', 'Nicaragua', 'NA', 'NIO', 'Cordoba', '505', '^(d{7})$', 'es-NI,en', '3617476'),
(562, 'NE', 'NER', 'NG', 'Niger', 'AF', 'XOF', 'Franc', '227', '^(d{4})$', 'fr-NE,ha,kr,dje', '2440476'),
(566, 'NG', 'NGA', 'NI', 'Nigeria', 'AF', 'NGN', 'Naira', '234', '^(d{6})$', 'en-NG,ha,yo,ig,ff', '2328926'),
(570, 'NU', 'NIU', 'NE', 'Niue', 'OC', 'NZD', 'Dollar', '683', '', 'niu,en-NU', '4036232'),
(574, 'NF', 'NFK', 'NF', 'Norfolk Island', 'OC', 'AUD', 'Dollar', '672', '', 'en-NF', '2155115'),
(578, 'NO', 'NOR', 'NO', 'Norway', 'EU', 'NOK', 'Krone', '47', '^(d{4})$', 'no,nb,nn', '3144096'),
(580, 'MP', 'MNP', 'CQ', 'Northern Mariana Islands', 'OC', 'USD', 'Dollar', '+1-670', '', 'fil,tl,zh,ch-MP,en-MP', '4041467'),
(581, 'UM', 'UMI', '', 'United States Minor Outlying Islands', 'OC', 'USD', 'Dollar ', '', '', 'en-UM', '5854968'),
(583, 'FM', 'FSM', 'FM', 'Micronesia', 'OC', 'USD', 'Dollar', '691', '^(d{5})$', 'en-FM,chk,pon,yap,kos,uli,woe,nkr,kpg', '2081918'),
(584, 'MH', 'MHL', 'RM', 'Marshall Islands', 'OC', 'USD', 'Dollar', '692', '', 'mh,en-MH', '2080185'),
(585, 'PW', 'PLW', 'PS', 'Palau', 'OC', 'USD', 'Dollar', '680', '^(96940)$', 'pau,sov,en-PW,tox,ja,fil,zh', '1559582'),
(586, 'PK', 'PAK', 'PK', 'Pakistan', 'AS', 'PKR', 'Rupee', '92', '^(d{5})$', 'ur-PK,en-PK,pa,sd,ps,brh', '1168579'),
(591, 'PA', 'PAN', 'PM', 'Panama', 'NA', 'PAB', 'Balboa', '507', '', 'es-PA,en', '3703430'),
(598, 'PG', 'PNG', 'PP', 'Papua New Guinea', 'OC', 'PGK', 'Kina', '675', '^(d{3})$', 'en-PG,ho,meu,tpi', '2088628'),
(600, 'PY', 'PRY', 'PA', 'Paraguay', 'SA', 'PYG', 'Guarani', '595', '^(d{4})$', 'es-PY,gn', '3437598'),
(604, 'PE', 'PER', 'PE', 'Peru', 'SA', 'PEN', 'Sol', '51', '', 'es-PE,qu,ay', '3932488'),
(608, 'PH', 'PHL', 'RP', 'Philippines', 'AS', 'PHP', 'Peso', '63', '^(d{4})$', 'tl,en-PH,fil', '1694008'),
(612, 'PN', 'PCN', 'PC', 'Pitcairn', 'OC', 'NZD', 'Dollar', '', '', 'en-PN', '4030699'),
(616, 'PL', 'POL', 'PL', 'Poland', 'EU', 'PLN', 'Zloty', '48', '^(d{5})$', 'pl', '798544'),
(620, 'PT', 'PRT', 'PO', 'Portugal', 'EU', 'EUR', 'Euro', '351', '^(d{7})$', 'pt-PT,mwl', '2264397'),
(624, 'GW', 'GNB', 'PU', 'Guinea-Bissau', 'AF', 'XOF', 'Franc', '245', '^(d{4})$', 'pt-GW,pov', '2372248'),
(626, 'TL', 'TLS', 'TT', 'East Timor', 'OC', 'USD', 'Dollar', '670', '', 'tet,pt-TL,id,en', '1966436'),
(630, 'PR', 'PRI', 'RQ', 'Puerto Rico', 'NA', 'USD', 'Dollar', '+1-787 and 1-939', '^(d{9})$', 'en-PR,es-PR', '4566966'),
(634, 'QA', 'QAT', 'QA', 'Qatar', 'AS', 'QAR', 'Rial', '974', '', 'ar-QA,es', '289688'),
(638, 'RE', 'REU', 'RE', 'Reunion', 'AF', 'EUR', 'Euro', '262', '^((97)|(98)(4|7|8)d{2})$', 'fr-RE', '935317'),
(642, 'RO', 'ROU', 'RO', 'Romania', 'EU', 'RON', 'Leu', '40', '^(d{6})$', 'ro,hu,rom', '798549'),
(643, 'RU', 'RUS', 'RS', 'Russia', 'EU', 'RUB', 'Ruble', '7', '^(d{6})$', 'ru-RU', '2017370'),
(646, 'RW', 'RWA', 'RW', 'Rwanda', 'AF', 'RWF', 'Franc', '250', '', 'rw,en-RW,fr-RW,sw', '49518'),
(652, 'BL', 'BLM', 'TB', 'Saint Barthélemy', 'NA', 'EUR', 'Euro', '590', '', 'fr', '3578476'),
(654, 'SH', 'SHN', 'SH', 'Saint Helena', 'AF', 'SHP', 'Pound', '290', '^(STHL1ZZ)$', 'en-SH', '3370751'),
(659, 'KN', 'KNA', 'SC', 'Saint Kitts and Nevis', 'NA', 'XCD', 'Dollar', '+1-869', '', 'en-KN', '3575174'),
(660, 'AI', 'AIA', 'AV', 'Anguilla', 'NA', 'XCD', 'Dollar', '+1-264', '', 'en-AI', '3573511'),
(662, 'LC', 'LCA', 'ST', 'Saint Lucia', 'NA', 'XCD', 'Dollar', '+1-758', '', 'en-LC', '3576468'),
(663, 'MF', 'MAF', 'RN', 'Saint Martin', 'NA', 'EUR', 'Euro', '590', '', 'fr', '3578421'),
(666, 'PM', 'SPM', 'SB', 'Saint Pierre and Miquelon', 'NA', 'EUR', 'Euro', '508', '^(97500)$', 'fr-PM', '3424932'),
(670, 'VC', 'VCT', 'VC', 'Saint Vincent and the Grenadines', 'NA', 'XCD', 'Dollar', '+1-784', '', 'en-VC,fr', '3577815'),
(674, 'SM', 'SMR', 'SM', 'San Marino', 'EU', 'EUR', 'Euro', '378', '^(4789d)$', 'it-SM', '3168068'),
(678, 'ST', 'STP', 'TP', 'Sao Tome and Principe', 'AF', 'STD', 'Dobra', '239', '', 'pt-ST', '2410758'),
(682, 'SA', 'SAU', 'SA', 'Saudi Arabia', 'AS', 'SAR', 'Rial', '966', '^(d{5})$', 'ar-SA', '102358'),
(686, 'SN', 'SEN', 'SG', 'Senegal', 'AF', 'XOF', 'Franc', '221', '^(d{5})$', 'fr-SN,wo,fuc,mnk', '2245662'),
(688, 'RS', 'SRB', 'RB', 'Serbia', 'EU', 'RSD', 'Dinar', '381', '^(d{6})$', 'sr,hu,bs,rom', '6290252'),
(690, 'SC', 'SYC', 'SE', 'Seychelles', 'AF', 'SCR', 'Rupee', '248', '', 'en-SC,fr-SC', '241170'),
(694, 'SL', 'SLE', 'SL', 'Sierra Leone', 'AF', 'SLL', 'Leone', '232', '', 'en-SL,men,tem', '2403846'),
(702, 'SG', 'SGP', 'SN', 'Singapore', 'AS', 'SGD', 'Dollar', '65', '^(d{6})$', 'cmn,en-SG,ms-SG,ta-SG,zh-SG', '1880251'),
(703, 'SK', 'SVK', 'LO', 'Slovakia', 'EU', 'EUR', 'Euro', '421', '^(d{5})$', 'sk,hu', '3057568'),
(704, 'VN', 'VNM', 'VM', 'Vietnam', 'AS', 'VND', 'Dong', '84', '^(d{6})$', 'vi,en,fr,zh,km', '1562822'),
(705, 'SI', 'SVN', 'SI', 'Slovenia', 'EU', 'EUR', 'Euro', '386', '^(?:SI)*(d{4})$', 'sl,sh', '3190538'),
(706, 'SO', 'SOM', 'SO', 'Somalia', 'AF', 'SOS', 'Shilling', '252', '^([A-Z]{2}d{5})$', 'so-SO,ar-SO,it,en-SO', '51537'),
(710, 'ZA', 'ZAF', 'SF', 'South Africa', 'AF', 'ZAR', 'Rand', '27', '^(d{4})$', 'zu,xh,af,nso,en-ZA,tn,st,ts', '953987'),
(716, 'ZW', 'ZWE', 'ZI', 'Zimbabwe', 'AF', 'ZWL', 'Dollar', '263', '', 'en-ZW,sn,nr,nd', '878675'),
(724, 'ES', 'ESP', 'SP', 'Spain', 'EU', 'EUR', 'Euro', '34', '^(d{5})$', 'es-ES,ca,gl,eu', '2510769'),
(732, 'EH', 'ESH', 'WI', 'Western Sahara', 'AF', 'MAD', 'Dirham', '212', '', 'ar,mey', '2461445'),
(736, 'SD', 'SDN', 'SU', 'Sudan', 'AF', 'SDG', 'Dinar', '249', '^(d{5})$', 'ar-SD,en,fia', '366755'),
(740, 'SR', 'SUR', 'NS', 'Suriname', 'SA', 'SRD', 'Dollar', '597', '', 'nl-SR,en,srn,hns,jv', '3382998'),
(744, 'SJ', 'SJM', 'SV', 'Svalbard and Jan Mayen', 'EU', 'NOK', 'Krone', '47', '', 'no,ru', '607072'),
(748, 'SZ', 'SWZ', 'WZ', 'Swaziland', 'AF', 'SZL', 'Lilangeni', '268', '^([A-Z]d{3})$', 'en-SZ,ss-SZ', '934841'),
(752, 'SE', 'SWE', 'SW', 'Sweden', 'EU', 'SEK', 'Krona', '46', '^(?:SE)*(d{5})$', 'sv-SE,se,sma,fi-SE', '2661886'),
(756, 'CH', 'CHE', 'SZ', 'Switzerland', 'EU', 'CHF', 'Franc', '41', '^(d{4})$', 'de-CH,fr-CH,it-CH,rm', '2658434'),
(760, 'SY', 'SYR', 'SY', 'Syria', 'AS', 'SYP', 'Pound', '963', '', 'ar-SY,ku,hy,arc,fr,en', '163843'),
(762, 'TJ', 'TJK', 'TI', 'Tajikistan', 'AS', 'TJS', 'Somoni', '992', '^(d{6})$', 'tg,ru', '1220409'),
(764, 'TH', 'THA', 'TH', 'Thailand', 'AS', 'THB', 'Baht', '66', '^(d{5})$', 'th,en', '1605651'),
(768, 'TG', 'TGO', 'TO', 'Togo', 'AF', 'XOF', 'Franc', '228', '', 'fr-TG,ee,hna,kbp,dag,ha', '2363686'),
(772, 'TK', 'TKL', 'TL', 'Tokelau', 'OC', 'NZD', 'Dollar', '690', '', 'tkl,en-TK', '4031074'),
(776, 'TO', 'TON', 'TN', 'Tonga', 'OC', 'TOP', 'Pa''anga', '676', '', 'to,en-TO', '4032283'),
(780, 'TT', 'TTO', 'TD', 'Trinidad and Tobago', 'NA', 'TTD', 'Dollar', '+1-868', '', 'en-TT,hns,fr,es,zh', '3573591'),
(784, 'AE', 'ARE', 'AE', 'United Arab Emirates', 'AS', 'AED', 'Dirham', '971', '', 'ar-AE,fa,en,hi,ur', '290557'),
(788, 'TN', 'TUN', 'TS', 'Tunisia', 'AF', 'TND', 'Dinar', '216', '^(d{4})$', 'ar-TN,fr', '2464461'),
(792, 'TR', 'TUR', 'TU', 'Turkey', 'AS', 'TRY', 'Lira', '90', '^(d{5})$', 'tr-TR,ku,diq,az,av', '298795'),
(795, 'TM', 'TKM', 'TX', 'Turkmenistan', 'AS', 'TMT', 'Manat', '993', '^(d{6})$', 'tk,ru,uz', '1218197'),
(796, 'TC', 'TCA', 'TK', 'Turks and Caicos Islands', 'NA', 'USD', 'Dollar', '+1-649', '^(TKCA 1ZZ)$', 'en-TC', '3576916'),
(798, 'TV', 'TUV', 'TV', 'Tuvalu', 'OC', 'AUD', 'Dollar', '688', '', 'tvl,en,sm,gil', '2110297'),
(800, 'UG', 'UGA', 'UG', 'Uganda', 'AF', 'UGX', 'Shilling', '256', '', 'en-UG,lg,sw,ar', '226074'),
(804, 'UA', 'UKR', 'UP', 'Ukraine', 'EU', 'UAH', 'Hryvnia', '380', '^(d{5})$', 'uk,ru-UA,rom,pl,hu', '690791'),
(807, 'MK', 'MKD', 'MK', 'Macedonia', 'EU', 'MKD', 'Denar', '389', '^(d{4})$', 'mk,sq,tr,rmm,sr', '718075'),
(818, 'EG', 'EGY', 'EG', 'Egypt', 'AF', 'EGP', 'Pound', '20', '^(d{5})$', 'ar-EG,en,fr', '357994'),
(826, 'GB', 'GBR', 'UK', 'United Kingdom', 'EU', 'GBP', 'Pound', '44', '^(([A-Z]d{2}[A-Z]{2})|([A-Z]d{3}[A-Z]{2})|([A', 'en-GB,cy-GB,gd', '2635167'),
(831, 'GG', 'GGY', 'GK', 'Guernsey', 'EU', 'GBP', 'Pound', '+44-1481', '^(([A-Z]d{2}[A-Z]{2})|([A-Z]d{3}[A-Z]{2})|([A', 'en,fr', '3042362'),
(832, 'JE', 'JEY', 'JE', 'Jersey', 'EU', 'GBP', 'Pound', '+44-1534', '^(([A-Z]d{2}[A-Z]{2})|([A-Z]d{3}[A-Z]{2})|([A', 'en,pt', '3042142'),
(833, 'IM', 'IMN', 'IM', 'Isle of Man', 'EU', 'GBP', 'Pound', '+44-1624', '^(([A-Z]d{2}[A-Z]{2})|([A-Z]d{3}[A-Z]{2})|([A', 'en,gv', '3042225'),
(834, 'TZ', 'TZA', 'TZ', 'Tanzania', 'AF', 'TZS', 'Shilling', '255', '', 'sw-TZ,en,ar', '149590'),
(840, 'US', 'USA', 'US', 'United States', 'NA', 'USD', 'Dollar', '1', '^(d{9})$', 'en-US,es-US,haw', '6252001'),
(850, 'VI', 'VIR', 'VQ', 'U.S. Virgin Islands', 'NA', 'USD', 'Dollar', '+1-340', '', 'en-VI', '4796775'),
(854, 'BF', 'BFA', 'UV', 'Burkina Faso', 'AF', 'XOF', 'Franc', '226', '', 'fr-BF', '2361809'),
(855, 'XK', 'XKX', 'KV', 'Kosovo', 'EU', 'EUR', 'Euro', '', '', 'sq,sr', '831053'),
(858, 'UY', 'URY', 'UY', 'Uruguay', 'SA', 'UYU', 'Peso', '598', '^(d{5})$', 'es-UY', '3439705'),
(860, 'UZ', 'UZB', 'UZ', 'Uzbekistan', 'AS', 'UZS', 'Som', '998', '^(d{6})$', 'uz,ru,tg', '1512440'),
(862, 'VE', 'VEN', 'VE', 'Venezuela', 'SA', 'VEF', 'Bolivar', '58', '^(d{4})$', 'es-VE', '3625428'),
(876, 'WF', 'WLF', 'WF', 'Wallis and Futuna', 'OC', 'XPF', 'Franc', '681', '^(986d{2})$', 'wls,fud,fr-WF', '4034749'),
(882, 'WS', 'WSM', 'WS', 'Samoa', 'OC', 'WST', 'Tala', '685', '', 'sm,en-WS', '4034894'),
(887, 'YE', 'YEM', 'YM', 'Yemen', 'AS', 'YER', 'Rial', '967', '', 'ar-YE', '69543'),
(891, 'CS', 'SCG', 'YI', 'Serbia and Montenegro', 'EU', 'RSD', 'Dinar', '381', '^(d{5})$', 'cu,hu,sq,sr', '863038'),
(894, 'ZM', 'ZMB', 'ZA', 'Zambia', 'AF', 'ZMK', 'Kwacha', '260', '^(d{5})$', 'en-ZM,bem,loz,lun,lue,ny,toi', '895949');

-- --------------------------------------------------------

--
-- Table structure for table `country_state`
--

CREATE TABLE IF NOT EXISTS `country_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `timezone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subregion_region_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3880 ;

--
-- Dumping data for table `country_state`
--

INSERT INTO `country_state` (`id`, `country_id`, `name`, `timezone`) VALUES
(1, 4, 'Badakhshan', 'Asia/Kabul'),
(2, 4, 'Badghis Province', 'Asia/Kabul'),
(3, 4, 'Baghlān', 'Asia/Kabul'),
(4, 4, 'Bāmīān', 'Asia/Kabul'),
(5, 4, 'Farah', 'Asia/Kabul'),
(6, 4, 'Faryab Province', 'Asia/Kabul'),
(7, 4, 'Ghaznī', 'Asia/Kabul'),
(8, 4, 'Ghowr', 'Asia/Kabul'),
(9, 4, 'Helmand Province', 'Asia/Kabul'),
(10, 4, 'Herat Province', 'Asia/Kabul'),
(11, 4, 'Kabul', 'Asia/Kabul'),
(12, 4, 'Kāpīsā', 'Asia/Kabul'),
(13, 4, 'Lowgar', 'Asia/Kabul'),
(14, 4, 'Nangarhār', 'Asia/Kabul'),
(15, 4, 'Nīmrūz', 'Asia/Kabul'),
(16, 4, 'Orūzgān', 'Asia/Kabul'),
(17, 4, 'Kandahār', 'Asia/Kabul'),
(18, 4, 'Kunduz Province', 'Asia/Kabul'),
(19, 4, 'Takhār', 'Asia/Kabul'),
(20, 4, 'Vardak', 'Asia/Kabul'),
(21, 4, 'Zabul Province', 'Asia/Kabul'),
(22, 4, 'Paktīkā', 'Asia/Kabul'),
(23, 4, 'Balkh', 'Asia/Kabul'),
(24, 4, 'Jowzjān', 'Asia/Kabul'),
(25, 4, 'Samangān', 'Asia/Kabul'),
(26, 4, 'Sar-e Pol', 'Asia/Kabul'),
(27, 4, 'Konar', 'Asia/Kabul'),
(28, 4, 'Laghmān', 'Asia/Kabul'),
(29, 4, 'Paktia Province', 'Asia/Kabul'),
(30, 4, 'Khowst', 'Asia/Kabul'),
(31, 4, 'Nūrestān', 'Asia/Kabul'),
(32, 4, 'Parvān', 'Asia/Kabul'),
(33, 4, 'Dāykondī', 'Asia/Kabul'),
(34, 4, 'Panjshīr', 'Asia/Kabul'),
(35, 8, 'Berat', 'Europe/Tirane'),
(36, 8, 'Dibër', 'Europe/Tirane'),
(37, 8, 'Durrës', 'Europe/Tirane'),
(38, 8, 'Elbasan', 'Europe/Tirane'),
(39, 8, 'Fier', 'Europe/Tirane'),
(40, 8, 'Gjirokastër', 'Europe/Tirane'),
(41, 8, 'Korçë', 'Europe/Tirane'),
(42, 8, 'Kukës', 'Europe/Tirane'),
(43, 8, 'Lezhë', 'Europe/Tirane'),
(44, 8, 'Shkodër', 'Europe/Tirane'),
(45, 8, 'Tiranë', 'Europe/Tirane'),
(46, 8, 'Vlorë', 'Europe/Tirane'),
(47, 12, 'Alger', 'Africa/Algiers'),
(48, 12, 'Batna', 'Africa/Algiers'),
(49, 12, 'Constantine', 'Africa/Algiers'),
(50, 12, 'Médéa', 'Africa/Algiers'),
(51, 12, 'Mostaganem', 'Africa/Algiers'),
(52, 12, 'Oran', 'Africa/Algiers'),
(53, 12, 'Saïda', 'Africa/Algiers'),
(54, 12, 'Sétif', 'Africa/Algiers'),
(55, 12, 'Tiaret', 'Africa/Algiers'),
(56, 12, 'Tizi Ouzou', 'Africa/Algiers'),
(57, 12, 'Tlemcen', 'Africa/Algiers'),
(58, 12, 'Bejaïa', 'Africa/Algiers'),
(59, 12, 'Biskra', 'Africa/Algiers'),
(60, 12, 'Blida', 'Africa/Algiers'),
(61, 12, 'Bouira', 'Africa/Algiers'),
(62, 12, 'Djelfa', 'Africa/Algiers'),
(63, 12, 'Guelma', 'Africa/Algiers'),
(64, 12, 'Jijel', 'Africa/Algiers'),
(65, 12, 'Laghouat', 'Africa/Algiers'),
(66, 12, 'Mascara', 'Africa/Algiers'),
(67, 12, 'Mʼsila', 'Africa/Algiers'),
(68, 12, 'Oum el Bouaghi', 'Africa/Algiers'),
(69, 12, 'Sidi Bel Abbès', 'Africa/Algiers'),
(70, 12, 'Skikda', 'Africa/Algiers'),
(71, 12, 'Tébessa', 'Africa/Algiers'),
(72, 12, 'Adrar', 'Africa/Algiers'),
(73, 12, 'Aïn Defla', 'Africa/Algiers'),
(74, 12, 'Aïn Temouchent', 'Africa/Algiers'),
(75, 12, 'Annaba', 'Africa/Algiers'),
(76, 12, 'Béchar', 'Africa/Algiers'),
(77, 12, 'Bordj Bou Arréridj', 'Africa/Algiers'),
(78, 12, 'Boumerdes', 'Africa/Algiers'),
(79, 12, 'Chlef', 'Africa/Algiers'),
(80, 12, 'El Bayadh', 'Africa/Algiers'),
(81, 12, 'El Oued', 'Africa/Algiers'),
(82, 12, 'El Tarf', 'Africa/Algiers'),
(83, 12, 'Ghardaïa', 'Africa/Algiers'),
(84, 12, 'Illizi', 'Africa/Algiers'),
(85, 12, 'Khenchela', 'Africa/Algiers'),
(86, 12, 'Mila', 'Africa/Algiers'),
(87, 12, 'Naama النعامة', 'Africa/Algiers'),
(88, 12, 'Ouargla', 'Africa/Algiers'),
(89, 12, 'Relizane', 'Africa/Algiers'),
(90, 12, 'Souk Ahras', 'Africa/Algiers'),
(91, 12, 'Tamanghasset', 'Africa/Algiers'),
(92, 12, 'Tindouf', 'Africa/Algiers'),
(93, 12, 'Tipaza', 'Africa/Algiers'),
(94, 12, 'Tissemsilt', 'Africa/Algiers'),
(95, 16, 'American Samoa', 'Pacific/Pago_Pago'),
(96, 20, 'Parròquia de Canillo', 'Europe/Andorra'),
(97, 20, 'Parròquia dʼEncamp', 'Europe/Andorra'),
(98, 20, 'Parròquia de la Massana', 'Europe/Andorra'),
(99, 20, 'Parròquia dʼOrdino', 'Europe/Andorra'),
(100, 20, 'Parròquia de Sant Julià de Lòria', 'Europe/Andorra'),
(101, 20, 'Parròquia dʼAndorra la Vella', 'Europe/Andorra'),
(102, 20, 'Parròquia dʼEscaldes-Engordany', 'Europe/Andorra'),
(103, 24, 'Benguela', 'Africa/Luanda'),
(104, 24, 'Bié', 'Africa/Luanda'),
(105, 24, 'Cabinda', 'Africa/Luanda'),
(106, 24, 'Cuando Cubango', 'Africa/Luanda'),
(107, 24, 'Cuanza Norte', 'Africa/Luanda'),
(108, 24, 'Cuanza Sul', 'Africa/Luanda'),
(109, 24, 'Cunene', 'Africa/Luanda'),
(110, 24, 'Huambo', 'Africa/Luanda'),
(111, 24, 'Huíla', 'Africa/Luanda'),
(112, 24, 'Luanda', 'Africa/Luanda'),
(113, 24, 'Malanje', 'Africa/Luanda'),
(114, 24, 'Namibe', 'Africa/Luanda'),
(115, 24, 'Moxico', 'Africa/Luanda'),
(116, 24, 'Uíge', 'Africa/Luanda'),
(117, 24, 'Zaire', 'Africa/Luanda'),
(118, 24, 'Lunda Norte', 'Africa/Luanda'),
(119, 24, 'Lunda Sul', 'Africa/Luanda'),
(120, 24, 'Bengo', 'Africa/Luanda'),
(121, 28, 'Redonda', 'America/Antigua'),
(122, 28, 'Barbuda', 'America/Antigua'),
(123, 28, 'Saint George', 'America/Antigua'),
(124, 28, 'Saint John', 'America/Antigua'),
(125, 28, 'Saint Mary', 'America/Antigua'),
(126, 28, 'Saint Paul', 'America/Antigua'),
(127, 28, 'Saint Peter', 'America/Antigua'),
(128, 28, 'Saint Philip', 'America/Antigua'),
(129, 31, 'Abşeron', 'Asia/Baku'),
(130, 31, 'Ağcabǝdi', 'Asia/Baku'),
(131, 31, 'Ağdam', 'Asia/Baku'),
(132, 31, 'Ağdaş', 'Asia/Baku'),
(133, 31, 'Ağstafa', 'Asia/Baku'),
(134, 31, 'Ağsu', 'Asia/Baku'),
(135, 31, 'Əli Bayramli', 'Asia/Baku'),
(136, 31, 'Astara', 'Asia/Baku'),
(137, 31, 'Baki', 'Asia/Baku'),
(138, 31, 'Balakǝn', 'Asia/Baku'),
(139, 31, 'Bǝrdǝ', 'Asia/Baku'),
(140, 31, 'Beylǝqan', 'Asia/Baku'),
(141, 31, 'Bilǝsuvar', 'Asia/Baku'),
(142, 31, 'Cǝbrayıl', 'Asia/Baku'),
(143, 31, 'Cǝlilabad', 'Asia/Baku'),
(144, 31, 'Daşkǝsǝn', 'Asia/Baku'),
(145, 31, 'Dǝvǝçi', 'Asia/Baku'),
(146, 31, 'Füzuli', 'Asia/Baku'),
(147, 31, 'Gǝdǝbǝy', 'Asia/Baku'),
(148, 31, 'Gǝncǝ', 'Asia/Baku'),
(149, 31, 'Goranboy', 'Asia/Baku'),
(150, 31, 'Göyçay', 'Asia/Baku'),
(151, 31, 'Hacıqabul', 'Asia/Baku'),
(152, 31, 'İmişli', 'Asia/Baku'),
(153, 31, 'İsmayıllı', 'Asia/Baku'),
(154, 31, 'Kǝlbǝcǝr', 'Asia/Baku'),
(155, 31, 'Kürdǝmir', 'Asia/Baku'),
(156, 31, 'Laçın', 'Asia/Baku'),
(157, 31, 'Lǝnkǝran', 'Asia/Baku'),
(158, 31, 'Lǝnkǝran Şǝhǝri', 'Asia/Baku'),
(159, 31, 'Lerik', 'Asia/Baku'),
(160, 31, 'Masallı', 'Asia/Baku'),
(161, 31, 'Mingǝcevir', 'Asia/Baku'),
(162, 31, 'Naftalan', 'Asia/Baku'),
(163, 31, 'Nakhichevan', 'Asia/Baku'),
(164, 31, 'Neftçala', 'Asia/Baku'),
(165, 31, 'Oğuz', 'Asia/Baku'),
(166, 31, 'Qǝbǝlǝ', 'Asia/Baku'),
(167, 31, 'Qǝx', 'Asia/Baku'),
(168, 31, 'Qazax', 'Asia/Baku'),
(169, 31, 'Qobustan', 'Asia/Baku'),
(170, 31, 'Quba', 'Asia/Baku'),
(171, 31, 'Qubadlı', 'Asia/Baku'),
(172, 31, 'Qusar', 'Asia/Baku'),
(173, 31, 'Saatlı', 'Asia/Baku'),
(174, 31, 'Sabirabad', 'Asia/Baku'),
(175, 31, 'Şǝki', 'Asia/Baku'),
(176, 31, 'Şǝki', 'Asia/Baku'),
(177, 31, 'Salyan', 'Asia/Baku'),
(178, 31, 'Şamaxı', 'Asia/Baku'),
(179, 31, 'Şǝmkir', 'Asia/Baku'),
(180, 31, 'Samux', 'Asia/Baku'),
(181, 31, 'Siyǝzǝn', 'Asia/Baku'),
(182, 31, 'Sumqayit', 'Asia/Baku'),
(183, 31, 'Şuşa', 'Asia/Baku'),
(184, 31, 'Şuşa Şəhəri', 'Asia/Baku'),
(185, 31, 'Tǝrtǝr', 'Asia/Baku'),
(186, 31, 'Tovuz', 'Asia/Baku'),
(187, 31, 'Ucar', 'Asia/Baku'),
(188, 31, 'Xaçmaz', 'Asia/Baku'),
(189, 31, 'Xankǝndi', 'Asia/Baku'),
(190, 31, 'Xanlar', 'Asia/Baku'),
(191, 31, 'Xızı', 'Asia/Baku'),
(192, 31, 'Xocalı', 'Asia/Baku'),
(193, 31, 'Xocavǝnd', 'Asia/Baku'),
(194, 31, 'Yardımlı', 'Asia/Baku'),
(195, 31, 'Yevlax', 'Asia/Baku'),
(196, 31, 'Yevlax', 'Asia/Baku'),
(197, 31, 'Zǝngilan', 'Asia/Baku'),
(198, 31, 'Zaqatala', 'Asia/Baku'),
(199, 31, 'Zǝrdab', 'Asia/Baku'),
(200, 32, 'Buenos Aires', 'America/Argentina/Buenos_Aires'),
(201, 32, 'Catamarca', 'America/Argentina/Catamarca'),
(202, 32, 'Chaco', 'America/Argentina/Cordoba'),
(203, 32, 'Chubut', 'America/Argentina/Catamarca'),
(204, 32, 'Córdoba', 'America/Argentina/Cordoba'),
(205, 32, 'Corrientes', 'America/Argentina/Cordoba'),
(206, 32, 'Distrito Federal', 'America/Argentina/Buenos_Aires'),
(207, 32, 'Entre Ríos', 'America/Argentina/Cordoba'),
(208, 32, 'Formosa', 'America/Argentina/Cordoba'),
(209, 32, 'Jujuy', 'America/Argentina/Jujuy'),
(210, 32, 'La Pampa', 'America/Argentina/Cordoba'),
(211, 32, 'La Rioja', 'America/Argentina/La_Rioja'),
(212, 32, 'Mendoza', 'America/Argentina/Mendoza'),
(213, 32, 'Misiones', 'America/Argentina/Cordoba'),
(214, 32, 'Neuquén', 'America/Argentina/Cordoba'),
(215, 32, 'Río Negro', 'America/Argentina/Cordoba'),
(216, 32, 'Salta', 'America/Argentina/Cordoba'),
(217, 32, 'San Juan', 'America/Argentina/San_Juan'),
(218, 32, 'San Luis', 'America/Argentina/Cordoba'),
(219, 32, 'Santa Cruz', 'America/Argentina/Rio_Gallegos'),
(220, 32, 'Santa Fe', 'America/Argentina/Cordoba'),
(221, 32, 'Santiago del Estero', 'America/Argentina/Cordoba'),
(222, 32, 'Tierra del Fuego, Antártida e Islas del Atlán', 'America/Argentina/Ushuaia'),
(223, 32, 'Tucumán', 'America/Argentina/Tucuman'),
(224, 36, 'Australian Capital Territory', 'Australia/Sydney'),
(225, 36, 'New South Wales', 'Australia/Sydney'),
(226, 36, 'Northern Territory', 'Australia/Darwin'),
(227, 36, 'Queensland', 'Australia/Brisbane'),
(228, 36, 'South Australia', 'Australia/Adelaide'),
(229, 36, 'Tasmania', 'Australia/Hobart'),
(230, 36, 'Victoria', 'Australia/Melbourne'),
(231, 36, 'Western Australia', 'Australia/Perth'),
(232, 40, 'Burgenland', 'Europe/Vienna'),
(233, 40, 'Carinthia', 'Europe/Vienna'),
(234, 40, 'Lower Austria', 'Europe/Vienna'),
(235, 40, 'Upper Austria', 'Europe/Vienna'),
(236, 40, 'Salzburg', 'Europe/Vienna'),
(237, 40, 'Styria', 'Europe/Vienna'),
(238, 40, 'Tyrol', 'Europe/Vienna'),
(239, 40, 'Vorarlberg', 'Europe/Vienna'),
(240, 40, 'Vienna', 'Europe/Vienna'),
(241, 44, 'Bimini', 'America/Nassau'),
(242, 44, 'Cat Island', 'America/Nassau'),
(243, 44, 'Inagua', 'America/Nassau'),
(244, 44, 'Long Island', 'America/Nassau'),
(245, 44, 'Mayaguana', 'America/Nassau'),
(246, 44, 'Ragged Island', 'America/Nassau'),
(247, 44, 'Harbour Island, Eleuthera', 'America/Nassau'),
(248, 44, 'North Abaco', 'America/Nassau'),
(249, 44, 'Acklins', 'America/Nassau'),
(250, 44, 'City of Freeport, Grand Bahama', 'America/Nassau'),
(251, 44, 'South Andros', 'America/Nassau'),
(252, 44, 'Hope Town, Abaco', 'America/Nassau'),
(253, 44, 'Mangrove Cay, Andros', 'America/Nassau'),
(254, 44, 'Mooreʼs Island, Abaco', 'America/Nassau'),
(255, 44, 'Rum Cay', 'America/Nassau'),
(256, 44, 'North Andros', 'America/Nassau'),
(257, 44, 'North Eleuthera', 'America/Nassau'),
(258, 44, 'South Eleuthera', 'America/Nassau'),
(259, 44, 'South Abaco', 'America/Nassau'),
(260, 44, 'San Salvador', 'America/Nassau'),
(261, 44, 'Berry Islands', 'America/Nassau'),
(262, 44, 'Black Point, Exuma', 'America/Nassau'),
(263, 44, 'Central Abaco', 'America/Nassau'),
(264, 44, 'Central Andros', 'America/Nassau'),
(265, 44, 'Central Eleuthera', 'America/Nassau'),
(266, 44, 'Crooked Island', 'America/Nassau'),
(267, 44, 'East Grand Bahama', 'America/Nassau'),
(268, 44, 'Exuma', 'America/Nassau'),
(269, 44, 'Grand Cay, Abaco', 'America/Nassau'),
(270, 44, 'Spanish Wells, Eleuthera', 'America/Nassau'),
(271, 44, 'West Grand Bahama', 'America/Nassau'),
(272, 48, 'Southern Governate', 'Asia/Bahrain'),
(273, 48, 'Northern Governate', 'Asia/Bahrain'),
(274, 48, 'Capital Governate', 'Asia/Bahrain'),
(275, 48, 'Central Governate', 'Asia/Bahrain'),
(276, 48, 'Muharraq Governate', 'Asia/Bahrain'),
(277, 50, 'BG80', 'Asia/Dhaka'),
(278, 50, 'Dhaka', 'Asia/Dhaka'),
(279, 50, 'Khulna', 'Asia/Dhaka'),
(280, 50, 'Rājshāhi', 'Asia/Dhaka'),
(281, 50, 'Chittagong', 'Asia/Dhaka'),
(282, 50, 'Barisāl', 'Asia/Dhaka'),
(283, 50, 'Sylhet', 'Asia/Dhaka'),
(284, 51, 'Aragatsotn', 'Asia/Yerevan'),
(285, 51, 'Ararat', 'Asia/Yerevan'),
(286, 51, 'Armavir', 'Asia/Yerevan'),
(287, 51, 'Gegharkʼunikʼ', 'Asia/Yerevan'),
(288, 51, 'Kotaykʼ', 'Asia/Yerevan'),
(289, 51, 'Lorri', 'Asia/Yerevan'),
(290, 51, 'Shirak', 'Asia/Yerevan'),
(291, 51, 'Syunikʼ', 'Asia/Yerevan'),
(292, 51, 'Tavush', 'Asia/Yerevan'),
(293, 51, 'Vayotsʼ Dzor', 'Asia/Yerevan'),
(294, 51, 'Yerevan', 'Asia/Yerevan'),
(295, 52, 'Christ Church', 'America/Barbados'),
(296, 52, 'Saint Andrew', 'America/Barbados'),
(297, 52, 'Saint George', 'America/Barbados'),
(298, 52, 'Saint James', 'America/Barbados'),
(299, 52, 'Saint John', 'America/Barbados'),
(300, 52, 'Saint Joseph', 'America/Barbados'),
(301, 52, 'Saint Lucy', 'America/Barbados'),
(302, 52, 'Saint Michael', 'America/Barbados'),
(303, 52, 'Saint Peter', 'America/Barbados'),
(304, 52, 'Saint Philip', 'America/Barbados'),
(305, 52, 'Saint Thomas', 'America/Barbados'),
(306, 56, 'Bruxelles-Capitale', 'Europe/Brussels'),
(307, 56, 'Flanders', 'Europe/Brussels'),
(308, 56, 'Wallonia', 'Europe/Brussels'),
(309, 60, 'Devonshire', 'Atlantic/Bermuda'),
(310, 60, 'Hamilton (parish)', 'Atlantic/Bermuda'),
(311, 60, 'Hamilton (city)', 'Atlantic/Bermuda'),
(312, 60, 'Paget', 'Atlantic/Bermuda'),
(313, 60, 'Pembroke', 'Atlantic/Bermuda'),
(314, 60, 'Saint Georgeʼs (parish)', 'Atlantic/Bermuda'),
(315, 60, 'Saint Georgeʼs (city)', 'Atlantic/Bermuda'),
(316, 60, 'Sandys', 'Atlantic/Bermuda'),
(317, 60, 'Smithʼs', 'Atlantic/Bermuda'),
(318, 60, 'Southampton', 'Atlantic/Bermuda'),
(319, 60, 'Warwick', 'Atlantic/Bermuda'),
(320, 64, 'Bumthang', 'Asia/Thimphu'),
(321, 64, 'Chhukha', 'Asia/Thimphu'),
(322, 64, 'Chirang', 'Asia/Thimphu'),
(323, 64, 'Daga', 'Asia/Thimphu'),
(324, 64, 'Geylegphug', 'Asia/Thimphu'),
(325, 64, 'Ha', 'Asia/Thimphu'),
(326, 64, 'Lhuntshi', 'Asia/Thimphu'),
(327, 64, 'Mongar', 'Asia/Thimphu'),
(328, 64, 'Paro District', 'Asia/Thimphu'),
(329, 64, 'Pemagatsel', 'Asia/Thimphu'),
(330, 64, 'Samchi', 'Asia/Thimphu'),
(331, 64, 'Samdrup Jongkhar District', 'Asia/Thimphu'),
(332, 64, 'Shemgang', 'Asia/Thimphu'),
(333, 64, 'Tashigang', 'Asia/Thimphu'),
(334, 64, 'Thimphu', 'Asia/Thimphu'),
(335, 64, 'Tongsa', 'Asia/Thimphu'),
(336, 64, 'Wangdi Phodrang', 'Asia/Thimphu'),
(337, 68, 'Chuquisaca', 'America/La_Paz'),
(338, 68, 'Cochabamba', 'America/La_Paz'),
(339, 68, 'El Beni', 'America/La_Paz'),
(340, 68, 'La Paz', 'America/La_Paz'),
(341, 68, 'Oruro', 'America/La_Paz'),
(342, 68, 'Pando', 'America/La_Paz'),
(343, 68, 'Potosí', 'America/La_Paz'),
(344, 68, 'Santa Cruz', 'America/La_Paz'),
(345, 68, 'Tarija', 'America/La_Paz'),
(346, 70, 'Federation of Bosnia and Herzegovina', 'Europe/Sarajevo'),
(347, 70, 'Republika Srpska', 'Europe/Sarajevo'),
(348, 70, 'Brčko', 'Europe/Sarajevo'),
(349, 72, 'Central', 'Africa/Gaborone'),
(350, 72, 'Chobe', 'Africa/Gaborone'),
(351, 72, 'Ghanzi', 'Africa/Gaborone'),
(352, 72, 'Kgalagadi', 'Africa/Gaborone'),
(353, 72, 'Kgatleng', 'Africa/Gaborone'),
(354, 72, 'Kweneng', 'Africa/Gaborone'),
(355, 72, 'Ngamiland', 'Africa/Gaborone'),
(356, 72, 'North East', 'Africa/Gaborone'),
(357, 72, 'South East', 'Africa/Gaborone'),
(358, 72, 'Southern', 'Africa/Gaborone'),
(359, 72, 'North West', 'Africa/Gaborone'),
(360, 76, 'Acre', 'America/Rio_Branco'),
(361, 76, 'Alagoas', 'America/Maceio'),
(362, 76, 'Amapá', 'America/Belem'),
(363, 76, 'Estado do Amazonas', 'America/Manaus'),
(364, 76, 'Bahia', 'America/Bahia'),
(365, 76, 'Ceará', 'America/Fortaleza'),
(366, 76, 'Distrito Federal', 'America/Sao_Paulo'),
(367, 76, 'Espírito Santo', 'America/Sao_Paulo'),
(368, 76, 'Fernando de Noronha', 'America/Recife'),
(369, 76, 'Goias', 'America/Sao_Paulo'),
(370, 76, 'Mato Grosso do Sul', 'America/Campo_Grande'),
(371, 76, 'Maranhão', 'America/Fortaleza'),
(372, 76, 'Mato Grosso', 'America/Cuiaba'),
(373, 76, 'Minas Gerais', 'America/Sao_Paulo'),
(374, 76, 'Pará', 'America/Belem'),
(375, 76, 'Paraíba', 'America/Fortaleza'),
(376, 76, 'Paraná', 'America/Sao_Paulo'),
(377, 76, 'Pernambuco', 'America/Noronha'),
(378, 76, 'Piauí', 'America/Fortaleza'),
(379, 76, 'State of Rio de Janeiro', 'America/Sao_Paulo'),
(380, 76, 'Rio Grande do Norte', 'America/Fortaleza'),
(381, 76, 'Rio Grande do Sul', 'America/Sao_Paulo'),
(382, 76, 'Rondônia', 'America/Porto_Velho'),
(383, 76, 'Roraima', 'America/Boa_Vista'),
(384, 76, 'Santa Catarina', 'America/Sao_Paulo'),
(385, 76, 'São Paulo', 'America/Sao_Paulo'),
(386, 76, 'Sergipe', 'America/Maceio'),
(387, 76, 'Estado de Goiás', 'America/Sao_Paulo'),
(388, 76, 'Pernambuco', 'America/Noronha'),
(389, 76, 'Tocantins', 'America/Araguaina'),
(390, 84, 'Belize', 'America/Belize'),
(391, 84, 'Cayo', 'America/Belize'),
(392, 84, 'Corozal', 'America/Belize'),
(393, 84, 'Orange Walk', 'America/Belize'),
(394, 84, 'Stann Creek', 'America/Belize'),
(395, 84, 'Toledo', 'America/Belize'),
(396, 86, 'British Indian Ocean Territory', 'Indian/Chagos'),
(397, 90, 'Malaita', 'Pacific/Guadalcanal'),
(398, 90, 'Western', 'Pacific/Guadalcanal'),
(399, 90, 'Central', 'Pacific/Guadalcanal'),
(400, 90, 'Guadalcanal', 'Pacific/Guadalcanal'),
(401, 90, 'Isabel', 'Pacific/Guadalcanal'),
(402, 90, 'Makira', 'Pacific/Guadalcanal'),
(403, 90, 'Temotu', 'Pacific/Guadalcanal'),
(404, 90, 'Central Province', 'Pacific/Guadalcanal'),
(405, 90, 'Choiseul', 'Pacific/Guadalcanal'),
(406, 90, 'Rennell and Bellona', 'Pacific/Guadalcanal'),
(407, 90, 'Rennell and Bellona', 'Pacific/Guadalcanal'),
(408, 92, 'British Virgin Islands', 'America/Tortola'),
(409, 96, 'Belait', 'Asia/Brunei'),
(410, 96, 'Brunei and Muara', 'Asia/Brunei'),
(411, 96, 'Temburong', 'Asia/Brunei'),
(412, 96, 'Tutong', 'Asia/Brunei'),
(413, 100, 'Burgas', 'Europe/Sofia'),
(414, 100, 'Grad', 'Europe/Sofia'),
(415, 100, 'Khaskovo', 'Europe/Sofia'),
(416, 100, 'Lovech', 'Europe/Sofia'),
(417, 100, 'Mikhaylovgrad', 'Europe/Sofia'),
(418, 100, 'Plovdiv', 'Europe/Sofia'),
(419, 100, 'Razgrad', 'Europe/Sofia'),
(420, 100, 'Sofiya', 'Europe/Sofia'),
(421, 100, 'Varna', 'Europe/Sofia'),
(422, 100, 'Blagoevgrad', 'Europe/Sofia'),
(423, 100, 'Burgas', 'Europe/Sofia'),
(424, 100, 'Dobrich', 'Europe/Sofia'),
(425, 100, 'Gabrovo', 'Europe/Sofia'),
(426, 100, 'Oblast Sofiya-Grad', 'Europe/Sofia'),
(427, 100, 'Khaskovo', 'Europe/Sofia'),
(428, 100, 'Kŭrdzhali', 'Europe/Sofia'),
(429, 100, 'Kyustendil', 'Europe/Sofia'),
(430, 100, 'Lovech', 'Europe/Sofia'),
(431, 100, 'Montana', 'Europe/Sofia'),
(432, 100, 'Pazardzhit', 'Europe/Sofia'),
(433, 100, 'Pernik', 'Europe/Sofia'),
(434, 100, 'Pleven', 'Europe/Sofia'),
(435, 100, 'Plovdiv', 'Europe/Sofia'),
(436, 100, 'Razgrad', 'Europe/Sofia'),
(437, 100, 'Ruse', 'Europe/Sofia'),
(438, 100, 'Shumen', 'Europe/Sofia'),
(439, 100, 'Silistra', 'Europe/Sofia'),
(440, 100, 'Sliven', 'Europe/Sofia'),
(441, 100, 'Smolyan', 'Europe/Sofia'),
(442, 100, 'Sofiya', 'Europe/Sofia'),
(443, 100, 'Stara Zagora', 'Europe/Sofia'),
(444, 100, 'Tŭrgovishte', 'Europe/Sofia'),
(445, 100, 'Varna', 'Europe/Sofia'),
(446, 100, 'Veliko Tŭrnovo', 'Europe/Sofia'),
(447, 100, 'Vidin', 'Europe/Sofia'),
(448, 100, 'Vratsa', 'Europe/Sofia'),
(449, 100, 'Yambol', 'Europe/Sofia'),
(450, 104, 'Rakhine State', 'Asia/Rangoon'),
(451, 104, 'Chin State', 'Asia/Rangoon'),
(452, 104, 'Ayeyarwady', 'Asia/Rangoon'),
(453, 104, 'Kachin State', 'Asia/Rangoon'),
(454, 104, 'Kayin State', 'Asia/Rangoon'),
(455, 104, 'Kayah State', 'Asia/Rangoon'),
(456, 104, 'Magwe', 'Asia/Rangoon'),
(457, 104, 'Mandalay', 'Asia/Rangoon'),
(458, 104, 'Pegu', 'Asia/Rangoon'),
(459, 104, 'Sagain', 'Asia/Rangoon'),
(460, 104, 'Shan State', 'Asia/Rangoon'),
(461, 104, 'Tanintharyi', 'Asia/Rangoon'),
(462, 104, 'Mon State', 'Asia/Rangoon'),
(463, 104, 'Rangoon', 'Asia/Rangoon'),
(464, 104, 'Magway', 'Asia/Rangoon'),
(465, 104, 'Bago', 'Asia/Rangoon'),
(466, 104, 'Yangon', 'Asia/Rangoon'),
(467, 108, 'Bujumbura', 'Africa/Bujumbura'),
(468, 108, 'Bubanza', 'Africa/Bujumbura'),
(469, 108, 'Bururi', 'Africa/Bujumbura'),
(470, 108, 'Cankuzo', 'Africa/Bujumbura'),
(471, 108, 'Cibitoke', 'Africa/Bujumbura'),
(472, 108, 'Gitega', 'Africa/Bujumbura'),
(473, 108, 'Karuzi', 'Africa/Bujumbura'),
(474, 108, 'Kayanza', 'Africa/Bujumbura'),
(475, 108, 'Kirundo', 'Africa/Bujumbura'),
(476, 108, 'Makamba', 'Africa/Bujumbura'),
(477, 108, 'Muyinga', 'Africa/Bujumbura'),
(478, 108, 'Ngozi', 'Africa/Bujumbura'),
(479, 108, 'Rutana', 'Africa/Bujumbura'),
(480, 108, 'Ruyigi', 'Africa/Bujumbura'),
(481, 108, 'Muramvya', 'Africa/Bujumbura'),
(482, 108, 'Mwaro', 'Africa/Bujumbura'),
(483, 112, 'Brestskaya Voblastsʼ', 'Europe/Minsk'),
(484, 112, 'Homyelʼskaya Voblastsʼ', 'Europe/Minsk'),
(485, 112, 'Hrodzyenskaya Voblastsʼ', 'Europe/Minsk'),
(486, 112, 'Mahilyowskaya Voblastsʼ', 'Europe/Minsk'),
(487, 112, 'Horad Minsk', 'Europe/Minsk'),
(488, 112, 'Minskaya Voblastsʼ', 'Europe/Minsk'),
(489, 112, 'Vitsyebskaya Voblastsʼ', 'Europe/Minsk'),
(490, 116, 'Krŏng Preăh Seihânŭ', 'Asia/Phnom_Penh'),
(491, 116, 'Kâmpóng Cham', 'Asia/Phnom_Penh'),
(492, 116, 'Kâmpóng Chhnăng', 'Asia/Phnom_Penh'),
(493, 116, 'Khétt Kâmpóng Spœ', 'Asia/Phnom_Penh'),
(494, 116, 'Kâmpóng Thum', 'Asia/Phnom_Penh'),
(495, 116, 'Kândal', 'Asia/Phnom_Penh'),
(496, 116, 'Kaôh Kŏng', 'Asia/Phnom_Penh'),
(497, 116, 'Krâchéh', 'Asia/Phnom_Penh'),
(498, 116, 'Môndól Kiri', 'Asia/Phnom_Penh'),
(499, 116, 'Phnum Penh', 'Asia/Phnom_Penh'),
(500, 116, 'Poŭthĭsăt', 'Asia/Phnom_Penh'),
(501, 116, 'Preăh Vihéar', 'Asia/Phnom_Penh'),
(502, 116, 'Prey Vêng', 'Asia/Phnom_Penh'),
(503, 116, 'Stœ̆ng Trêng', 'Asia/Phnom_Penh'),
(504, 116, 'Svay Riĕng', 'Asia/Phnom_Penh'),
(505, 116, 'Takêv', 'Asia/Phnom_Penh'),
(506, 116, 'Kâmpôt', 'Asia/Phnom_Penh'),
(507, 116, 'Phnum Pénh', 'Asia/Phnom_Penh'),
(508, 116, 'Rôtânăh Kiri', 'Asia/Phnom_Penh'),
(509, 116, 'Siĕm Réab', 'Asia/Phnom_Penh'),
(510, 116, 'Bantéay Méan Cheăy', 'Asia/Phnom_Penh'),
(511, 116, 'Kêb', 'Asia/Phnom_Penh'),
(512, 116, 'Ŏtdâr Méan Cheăy', 'Asia/Phnom_Penh'),
(513, 116, 'Preăh Seihânŭ', 'Asia/Phnom_Penh'),
(514, 116, 'Bătdâmbâng', 'Asia/Phnom_Penh'),
(515, 116, 'Palĭn', 'Asia/Phnom_Penh'),
(516, 120, 'Est', 'Africa/Douala'),
(517, 120, 'Littoral', 'Africa/Douala'),
(518, 120, 'North-West Province', 'Africa/Douala'),
(519, 120, 'Ouest', 'Africa/Douala'),
(520, 120, 'South-West Province', 'Africa/Douala'),
(521, 120, 'Adamaoua', 'Africa/Douala'),
(522, 120, 'Centre', 'Africa/Douala'),
(523, 120, 'Extreme-Nord', 'Africa/Douala'),
(524, 120, 'North Province', 'Africa/Douala'),
(525, 120, 'South Province', 'Africa/Douala'),
(526, 124, 'Alberta', 'America/Edmonton'),
(527, 124, 'British Columbia', 'America/Vancouver'),
(528, 124, 'Manitoba', 'America/Winnipeg'),
(529, 124, 'New Brunswick', 'America/Halifax'),
(530, 124, 'Newfoundland and Labrador', 'America/Goose_Bay'),
(531, 124, 'Nova Scotia', 'America/Halifax'),
(532, 124, 'Ontario', 'America/Toronto'),
(533, 124, 'Prince Edward Island', 'America/Halifax'),
(534, 124, 'Quebec', 'America/Montreal'),
(535, 124, 'Saskatchewan', 'America/Winnipeg'),
(536, 124, 'Yukon', 'America/Whitehorse'),
(537, 124, 'Northwest Territories', 'America/Yellowknife'),
(538, 124, 'Nunavut', 'America/Rankin_Inlet'),
(539, 132, 'Boa Vista', 'Atlantic/Cape_Verde'),
(540, 132, 'Brava', 'Atlantic/Cape_Verde'),
(541, 132, 'Maio', 'Atlantic/Cape_Verde'),
(542, 132, 'Paul', 'Atlantic/Cape_Verde'),
(543, 132, 'Praia', 'Atlantic/Cape_Verde'),
(544, 132, 'Ribeira Grande', 'Atlantic/Cape_Verde'),
(545, 132, 'Sal', 'Atlantic/Cape_Verde'),
(546, 132, 'Santa Catarina   ', 'Atlantic/Cape_Verde'),
(547, 132, 'São Nicolau', 'Atlantic/Cape_Verde'),
(548, 132, 'São Vicente', 'Atlantic/Cape_Verde'),
(549, 132, 'Tarrafal ', 'Atlantic/Cape_Verde'),
(550, 132, 'Mosteiros', 'Atlantic/Cape_Verde'),
(551, 132, 'Praia', 'Atlantic/Cape_Verde'),
(552, 132, 'Santa Catarina', 'Atlantic/Cape_Verde'),
(553, 132, 'Santa Cruz', 'Atlantic/Cape_Verde'),
(554, 132, 'São Domingos', 'Atlantic/Cape_Verde'),
(555, 132, 'São Filipe', 'Atlantic/Cape_Verde'),
(556, 132, 'São Miguel', 'Atlantic/Cape_Verde'),
(557, 132, 'Tarrafal', 'Atlantic/Cape_Verde'),
(558, 136, 'Creek', 'America/Cayman'),
(559, 136, 'Eastern', 'America/Cayman'),
(560, 136, 'Midland', 'America/Cayman'),
(561, 136, 'South Town', 'America/Cayman'),
(562, 136, 'Spot Bay', 'America/Cayman'),
(563, 136, 'Stake Bay', 'America/Cayman'),
(564, 136, 'West End', 'America/Cayman'),
(565, 136, 'Western', 'America/Cayman'),
(566, 140, 'Bamingui-Bangoran', 'Africa/Bangui'),
(567, 140, 'Basse-Kotto', 'Africa/Bangui'),
(568, 140, 'Haute-Kotto', 'Africa/Bangui'),
(569, 140, 'Mambéré-Kadéï', 'Africa/Bangui'),
(570, 140, 'Haut-Mbomou', 'Africa/Bangui'),
(571, 140, 'Kémo', 'Africa/Bangui'),
(572, 140, 'Lobaye', 'Africa/Bangui'),
(573, 140, 'Mbomou', 'Africa/Bangui'),
(574, 140, 'Nana-Mambéré', 'Africa/Bangui'),
(575, 140, 'Ouaka', 'Africa/Bangui'),
(576, 140, 'Ouham', 'Africa/Bangui'),
(577, 140, 'Ouham-Pendé', 'Africa/Bangui'),
(578, 140, 'Vakaga', 'Africa/Bangui'),
(579, 140, 'Nana-Grébizi', 'Africa/Bangui'),
(580, 140, 'Sangha-Mbaéré', 'Africa/Bangui'),
(581, 140, 'Ombella-Mpoko', 'Africa/Bangui'),
(582, 140, 'Bangui', 'Africa/Bangui'),
(583, 144, 'Central', 'Asia/Colombo'),
(584, 144, 'North Central', 'Asia/Colombo'),
(585, 144, 'North Eastern', 'Asia/Colombo'),
(586, 144, 'North Western', 'Asia/Colombo'),
(587, 144, 'Sabaragamuwa', 'Asia/Colombo'),
(588, 144, 'Southern', 'Asia/Colombo'),
(589, 144, 'Uva', 'Asia/Colombo'),
(590, 144, 'Western', 'Asia/Colombo'),
(591, 148, 'Batha', 'Africa/Ndjamena'),
(592, 148, 'Biltine', 'Africa/Ndjamena'),
(593, 148, 'Borkou-Ennedi-Tibesti', 'Africa/Ndjamena'),
(594, 148, 'Chari-Baguirmi', 'Africa/Ndjamena'),
(595, 148, 'Guéra', 'Africa/Ndjamena'),
(596, 148, 'Kanem', 'Africa/Ndjamena'),
(597, 148, 'Lac', 'Africa/Ndjamena'),
(598, 148, 'Logone Occidental', 'Africa/Ndjamena'),
(599, 148, 'Logone Oriental', 'Africa/Ndjamena'),
(600, 148, 'Mayo-Kébbi', 'Africa/Ndjamena'),
(601, 148, 'Moyen-Chari', 'Africa/Ndjamena'),
(602, 148, 'Ouaddaï', 'Africa/Ndjamena'),
(603, 148, 'Salamat', 'Africa/Ndjamena'),
(604, 148, 'Tandjilé', 'Africa/Ndjamena'),
(605, 152, 'Valparaíso', 'America/Santiago'),
(606, 152, 'Aisén del General Carlos Ibáñez del Campo', 'America/Santiago'),
(607, 152, 'Antofagasta', 'America/Santiago'),
(608, 152, 'Araucanía', 'America/Santiago'),
(609, 152, 'Atacama', 'America/Santiago'),
(610, 152, 'Bío-Bío', 'America/Santiago'),
(611, 152, 'Coquimbo', 'America/Santiago'),
(612, 152, 'Libertador General Bernardo OʼHiggins', 'America/Santiago'),
(613, 152, 'Los Lagos', 'America/Santiago'),
(614, 152, 'Magallanes y Antártica Chilena', 'America/Santiago'),
(615, 152, 'Maule', 'America/Santiago'),
(616, 152, 'Región Metropolitana', 'America/Santiago'),
(617, 152, 'Tarapaca', 'America/Santiago'),
(618, 152, 'Los Lagos', 'America/Santiago'),
(619, 152, 'Tarapacá', 'America/Santiago'),
(620, 152, 'Región de Arica y Parinacota', 'America/Santiago'),
(621, 152, 'Región de Los Ríos', 'America/Santiago'),
(622, 156, 'Anhui', 'Asia/Ho_Chi_Minh'),
(623, 156, 'Zhejiang', 'Asia/Ho_Chi_Minh'),
(624, 156, 'Jiangxi', 'Asia/Ho_Chi_Minh'),
(625, 156, 'Jiangsu', 'Asia/Ho_Chi_Minh'),
(626, 156, 'Jilin', 'Asia/Ho_Chi_Minh'),
(627, 156, 'Qinghai', 'Asia/Ho_Chi_Minh'),
(628, 156, 'Fujian', 'Asia/Ho_Chi_Minh'),
(629, 156, 'Heilongjiang', 'Asia/Ho_Chi_Minh'),
(630, 156, 'Henan', 'Asia/Ho_Chi_Minh'),
(631, 156, 'disputed', 'Asia/Ho_Chi_Minh'),
(632, 156, 'Hebei', 'Asia/Ho_Chi_Minh'),
(633, 156, 'Hunan Province', 'Asia/Ho_Chi_Minh'),
(634, 156, 'Hubei', 'Asia/Ho_Chi_Minh'),
(635, 156, 'Xinjiang', 'Asia/Ho_Chi_Minh'),
(636, 156, 'Xizang', 'Asia/Ho_Chi_Minh'),
(637, 156, 'Gansu', 'Asia/Ho_Chi_Minh'),
(638, 156, 'Guangxi', 'Asia/Ho_Chi_Minh'),
(639, 156, 'Guizhou', 'Asia/Ho_Chi_Minh'),
(640, 156, 'Liaoning Province', 'Asia/Ho_Chi_Minh'),
(641, 156, 'Inner Mongolia', 'Asia/Ho_Chi_Minh'),
(642, 156, 'Ningxia', 'Asia/Ho_Chi_Minh'),
(643, 156, 'Beijing', 'Asia/Ho_Chi_Minh'),
(644, 156, 'Shanghai', 'Asia/Ho_Chi_Minh'),
(645, 156, 'Shanxi', 'Asia/Ho_Chi_Minh'),
(646, 156, 'Shandong', 'Asia/Ho_Chi_Minh'),
(647, 156, 'Shaanxi', 'Asia/Ho_Chi_Minh'),
(648, 156, 'Tianjin', 'Asia/Ho_Chi_Minh'),
(649, 156, 'Yunnan Province', 'Asia/Ho_Chi_Minh'),
(650, 156, 'Guangdong', 'Asia/Ho_Chi_Minh'),
(651, 156, 'Hainan Province', 'Asia/Ho_Chi_Minh'),
(652, 156, 'Sichuan', 'Asia/Ho_Chi_Minh'),
(653, 156, 'Chongqing', 'Asia/Ho_Chi_Minh'),
(654, 156, 'PF99', 'Asia/Ho_Chi_Minh'),
(655, 158, 'Fukien', 'Asia/Taipei'),
(656, 158, 'Kaohsiung', 'Asia/Taipei'),
(657, 158, 'Taipei', 'Asia/Taipei'),
(658, 158, 'Taiwan', 'Asia/Taipei'),
(659, 162, 'Christmas Island', 'Indian/Christmas'),
(660, 166, 'Cocos (Keeling) Islands', 'Indian/Cocos'),
(661, 170, 'Amazonas', 'America/Bogota'),
(662, 170, 'Antioquia', 'America/Bogota'),
(663, 170, 'Arauca', 'America/Bogota'),
(664, 170, 'Atlántico', 'America/Bogota'),
(665, 170, 'Bolívar', 'America/Bogota'),
(666, 170, 'Boyacá', 'America/Bogota'),
(667, 170, 'Caldas', 'America/Bogota'),
(668, 170, 'Caquetá', 'America/Bogota'),
(669, 170, 'Cauca', 'America/Bogota'),
(670, 170, 'Cesar', 'America/Bogota'),
(671, 170, 'Chocó', 'America/Bogota'),
(672, 170, 'Córdoba', 'America/Bogota'),
(673, 170, 'Guaviare', 'America/Bogota'),
(674, 170, 'Guainía', 'America/Bogota'),
(675, 170, 'Huila', 'America/Bogota'),
(676, 170, 'La Guajira', 'America/Bogota'),
(677, 170, 'Magdalena', 'America/Bogota'),
(678, 170, 'Meta', 'America/Bogota'),
(679, 170, 'Nariño', 'America/Bogota'),
(680, 170, 'Norte de Santander', 'America/Bogota'),
(681, 170, 'Putumayo', 'America/Bogota'),
(682, 170, 'Quindío', 'America/Bogota'),
(683, 170, 'Risaralda', 'America/Bogota'),
(684, 170, 'Archipiélago de San Andrés, Providencia y San', 'America/Bogota'),
(685, 170, 'Santander', 'America/Bogota'),
(686, 170, 'Sucre', 'America/Bogota'),
(687, 170, 'Tolima', 'America/Bogota'),
(688, 170, 'Valle del Cauca', 'America/Bogota'),
(689, 170, 'Vaupés', 'America/Bogota'),
(690, 170, 'Vichada', 'America/Bogota'),
(691, 170, 'Casanare', 'America/Bogota'),
(692, 170, 'Cundinamarca', 'America/Bogota'),
(693, 170, 'Bogota D.C.', 'America/Bogota'),
(694, 170, 'Bolívar', 'America/Bogota'),
(695, 170, 'Boyacá', 'America/Bogota'),
(696, 170, 'Caldas', 'America/Bogota'),
(697, 170, 'Magdalena', 'America/Bogota'),
(698, 174, 'Anjouan', 'Indian/Comoro'),
(699, 174, 'Grande Comore', 'Indian/Comoro'),
(700, 174, 'Mohéli', 'Indian/Comoro'),
(701, 175, 'Mayotte', 'Indian/Mayotte'),
(702, 178, 'Bouenza', 'Africa/Brazzaville'),
(703, 178, 'CF03', 'Africa/Brazzaville'),
(704, 178, 'Kouilou', 'Africa/Brazzaville'),
(705, 178, 'Lékoumou', 'Africa/Brazzaville'),
(706, 178, 'Likouala', 'Africa/Brazzaville'),
(707, 178, 'Niari', 'Africa/Brazzaville'),
(708, 178, 'Plateaux', 'Africa/Brazzaville'),
(709, 178, 'Sangha', 'Africa/Brazzaville'),
(710, 178, 'Pool', 'Africa/Brazzaville'),
(711, 178, 'Brazzaville', 'Africa/Brazzaville'),
(712, 178, 'Cuvette', 'Africa/Brazzaville'),
(713, 178, 'Cuvette-Ouest', 'Africa/Brazzaville'),
(714, 178, 'Pointe-Noire', 'Africa/Brazzaville'),
(715, 180, 'Bandundu', 'Africa/Kinshasa'),
(716, 180, 'Équateur', 'Africa/Kinshasa'),
(717, 180, 'Kasaï-Occidental', 'Africa/Lubumbashi'),
(718, 180, 'Kasaï-Oriental', 'Africa/Lubumbashi'),
(719, 180, 'Katanga', 'Africa/Lubumbashi'),
(720, 180, 'Kinshasa', 'Africa/Kinshasa'),
(721, 180, 'Bas-Congo', 'Africa/Kinshasa'),
(722, 180, 'Orientale', 'Africa/Lubumbashi'),
(723, 180, 'Maniema', 'Africa/Lubumbashi'),
(724, 180, 'Nord-Kivu', 'Africa/Lubumbashi'),
(725, 180, 'Sud-Kivu', 'Africa/Lubumbashi'),
(726, 184, 'Cook Islands', 'Pacific/Rarotonga'),
(727, 188, 'Alajuela', 'America/Costa_Rica'),
(728, 188, 'Cartago', 'America/Costa_Rica'),
(729, 188, 'Guanacaste', 'America/Costa_Rica'),
(730, 188, 'Heredia', 'America/Costa_Rica'),
(731, 188, 'Limón', 'America/Costa_Rica'),
(732, 188, 'Puntarenas', 'America/Costa_Rica'),
(733, 188, 'San José', 'America/Costa_Rica'),
(734, 191, 'Bjelovarsko-Bilogorska', 'Europe/Zagreb'),
(735, 191, 'Brodsko-Posavska', 'Europe/Zagreb'),
(736, 191, 'Dubrovačko-Neretvanska', 'Europe/Zagreb'),
(737, 191, 'Istarska', 'Europe/Zagreb'),
(738, 191, 'Karlovačka', 'Europe/Zagreb'),
(739, 191, 'Koprivničko-Križevačka', 'Europe/Zagreb'),
(740, 191, 'Krapinsko-Zagorska', 'Europe/Zagreb'),
(741, 191, 'Ličko-Senjska', 'Europe/Zagreb'),
(742, 191, 'Međimurska', 'Europe/Zagreb'),
(743, 191, 'Osječko-Baranjska', 'Europe/Zagreb'),
(744, 191, 'Požeško-Slavonska', 'Europe/Zagreb'),
(745, 191, 'Primorsko-Goranska', 'Europe/Zagreb'),
(746, 191, 'Šibensko-Kniniska', 'Europe/Zagreb'),
(747, 191, 'Sisačko-Moslavačka', 'Europe/Zagreb'),
(748, 191, 'Splitsko-Dalmatinska', 'Europe/Zagreb'),
(749, 191, 'Varaždinska', 'Europe/Zagreb'),
(750, 191, 'Virovitičk-Podravska', 'Europe/Zagreb'),
(751, 191, 'Vukovarsko-Srijemska', 'Europe/Zagreb'),
(752, 191, 'Zadarska', 'Europe/Zagreb'),
(753, 191, 'Zagrebačka', 'Europe/Zagreb'),
(754, 191, 'Grad Zagreb', 'Europe/Zagreb'),
(755, 192, 'Pinar del Río', 'America/Havana'),
(756, 192, 'Ciudad de La Habana', 'America/Havana'),
(757, 192, 'Matanzas', 'America/Havana'),
(758, 192, 'Isla de la Juventud', 'America/Havana'),
(759, 192, 'Camagüey', 'America/Havana'),
(760, 192, 'Ciego de Ávila', 'America/Havana'),
(761, 192, 'Cienfuegos', 'America/Havana'),
(762, 192, 'Granma', 'America/Havana'),
(763, 192, 'Guantánamo', 'America/Havana'),
(764, 192, 'La Habana', 'America/Havana'),
(765, 192, 'Holguín', 'America/Havana'),
(766, 192, 'Las Tunas', 'America/Havana'),
(767, 192, 'Sancti Spíritus', 'America/Havana'),
(768, 192, 'Santiago de Cuba', 'America/Havana'),
(769, 192, 'Villa Clara', 'America/Havana'),
(770, 196, 'Famagusta', 'Asia/Nicosia'),
(771, 196, 'Kyrenia', 'Asia/Nicosia'),
(772, 196, 'Larnaca', 'Asia/Nicosia'),
(773, 196, 'Nicosia', 'Asia/Nicosia'),
(774, 196, 'Limassol', 'Asia/Nicosia'),
(775, 196, 'Paphos', 'Asia/Nicosia'),
(776, 203, 'Hradec Kralove', 'Europe/Prague'),
(777, 203, 'Jablonec nad Nisou', 'Europe/Prague'),
(778, 203, 'Jiein', 'Europe/Prague'),
(779, 203, 'Jihlava', 'Europe/Prague'),
(780, 203, 'Kolin', 'Europe/Prague'),
(781, 203, 'Liberec', 'Europe/Prague'),
(782, 203, 'Melnik', 'Europe/Prague'),
(783, 203, 'Mlada Boleslav', 'Europe/Prague'),
(784, 203, 'Nachod', 'Europe/Prague'),
(785, 203, 'Nymburk', 'Europe/Prague'),
(786, 203, 'Pardubice', 'Europe/Prague'),
(787, 203, 'Hlavní Mesto Praha', 'Europe/Prague'),
(788, 203, 'Semily', 'Europe/Prague'),
(789, 203, 'Trutnov', 'Europe/Prague'),
(790, 203, 'South Moravian Region', 'Europe/Prague'),
(791, 203, 'Jihočeský Kraj', 'Europe/Prague'),
(792, 203, 'Vysočina', 'Europe/Prague'),
(793, 203, 'Karlovarský Kraj', 'Europe/Prague'),
(794, 203, 'Královéhradecký Kraj', 'Europe/Prague'),
(795, 203, 'Liberecký Kraj', 'Europe/Prague'),
(796, 203, 'Olomoucký Kraj', 'Europe/Prague'),
(797, 203, 'Moravskoslezský Kraj', 'Europe/Prague'),
(798, 203, 'Pardubický Kraj', 'Europe/Prague'),
(799, 203, 'Plzeňský Kraj', 'Europe/Prague'),
(800, 203, 'Středočeský Kraj', 'Europe/Prague'),
(801, 203, 'Ústecký Kraj', 'Europe/Prague'),
(802, 203, 'Zlínský Kraj', 'Europe/Prague'),
(803, 204, 'Atakora', 'Africa/Porto-Novo'),
(804, 204, 'Atlantique', 'Africa/Porto-Novo'),
(805, 204, 'Alibori', 'Africa/Porto-Novo'),
(806, 204, 'Borgou', 'Africa/Porto-Novo'),
(807, 204, 'Collines', 'Africa/Porto-Novo'),
(808, 204, 'Kouffo', 'Africa/Porto-Novo'),
(809, 204, 'Donga', 'Africa/Porto-Novo'),
(810, 204, 'Littoral', 'Africa/Porto-Novo'),
(811, 204, 'Mono', 'Africa/Porto-Novo'),
(812, 204, 'Ouémé', 'Africa/Porto-Novo'),
(813, 204, 'Plateau', 'Africa/Porto-Novo'),
(814, 204, 'Zou', 'Africa/Porto-Novo'),
(815, 208, 'Århus', 'Europe/Copenhagen'),
(816, 208, 'Bornholm', 'Europe/Copenhagen'),
(817, 208, 'Frederiksborg', 'Europe/Copenhagen'),
(818, 208, 'Fyn', 'Europe/Copenhagen'),
(819, 208, 'Copenhagen city', 'Europe/Copenhagen'),
(820, 208, 'København', 'Europe/Copenhagen'),
(821, 208, 'Nordjylland', 'Europe/Copenhagen'),
(822, 208, 'Ribe', 'Europe/Copenhagen'),
(823, 208, 'Ringkøbing', 'Europe/Copenhagen'),
(824, 208, 'Roskilde', 'Europe/Copenhagen'),
(825, 208, 'Sønderjylland', 'Europe/Copenhagen'),
(826, 208, 'Storstrøm', 'Europe/Copenhagen'),
(827, 208, 'Vejle', 'Europe/Copenhagen'),
(828, 208, 'Vestsjælland', 'Europe/Copenhagen'),
(829, 208, 'Viborg', 'Europe/Copenhagen'),
(830, 208, 'Fredriksberg', 'Europe/Copenhagen'),
(831, 208, 'Capital Region', 'Europe/Copenhagen'),
(832, 208, 'Central Jutland', 'Europe/Copenhagen'),
(833, 208, 'North Jutland', 'Europe/Copenhagen'),
(834, 208, 'Region Zealand', 'Europe/Copenhagen'),
(835, 208, 'Region South Denmark', 'Europe/Copenhagen'),
(836, 212, 'Saint Andrew', 'America/Dominica'),
(837, 212, 'Saint David', 'America/Dominica'),
(838, 212, 'Saint George', 'America/Dominica'),
(839, 212, 'Saint John', 'America/Dominica'),
(840, 212, 'Saint Joseph', 'America/Dominica'),
(841, 212, 'Saint Luke', 'America/Dominica'),
(842, 212, 'Saint Mark', 'America/Dominica'),
(843, 212, 'Saint Patrick', 'America/Dominica'),
(844, 212, 'Saint Paul', 'America/Dominica'),
(845, 212, 'Saint Peter', 'America/Dominica'),
(846, 214, 'Azua', 'America/Santo_Domingo'),
(847, 214, 'Baoruco', 'America/Santo_Domingo'),
(848, 214, 'Barahona', 'America/Santo_Domingo'),
(849, 214, 'Dajabón', 'America/Santo_Domingo'),
(850, 214, 'Duarte', 'America/Santo_Domingo'),
(851, 214, 'Espaillat', 'America/Santo_Domingo'),
(852, 214, 'Independencia', 'America/Santo_Domingo'),
(853, 214, 'La Altagracia', 'America/Santo_Domingo'),
(854, 214, 'Elías Piña', 'America/Santo_Domingo'),
(855, 214, 'La Romana', 'America/Santo_Domingo'),
(856, 214, 'María Trinidad Sánchez', 'America/Santo_Domingo'),
(857, 214, 'Monte Cristi', 'America/Santo_Domingo'),
(858, 214, 'Pedernales', 'America/Santo_Domingo'),
(859, 214, 'Puerto Plata', 'America/Santo_Domingo'),
(860, 214, 'Salcedo', 'America/Santo_Domingo'),
(861, 214, 'Samaná', 'America/Santo_Domingo'),
(862, 214, 'Sánchez Ramírez', 'America/Santo_Domingo'),
(863, 214, 'San Juan', 'America/Santo_Domingo'),
(864, 214, 'San Pedro de Macorís', 'America/Santo_Domingo'),
(865, 214, 'Santiago', 'America/Santo_Domingo'),
(866, 214, 'Santiago Rodríguez', 'America/Santo_Domingo'),
(867, 214, 'Valverde', 'America/Santo_Domingo'),
(868, 214, 'El Seíbo', 'America/Santo_Domingo'),
(869, 214, 'Hato Mayor', 'America/Santo_Domingo'),
(870, 214, 'La Vega', 'America/Santo_Domingo'),
(871, 214, 'Monseñor Nouel', 'America/Santo_Domingo'),
(872, 214, 'Monte Plata', 'America/Santo_Domingo'),
(873, 214, 'San Cristóbal', 'America/Santo_Domingo'),
(874, 214, 'Distrito Nacional', 'America/Santo_Domingo'),
(875, 214, 'Peravia', 'America/Santo_Domingo'),
(876, 214, 'San José de Ocoa', 'America/Santo_Domingo'),
(877, 214, 'Santo Domingo', 'America/Santo_Domingo'),
(878, 218, 'Galápagos', 'Pacific/Galapagos'),
(879, 218, 'Azuay', 'America/Guayaquil'),
(880, 218, 'Bolívar', 'America/Guayaquil'),
(881, 218, 'Cañar', 'America/Guayaquil'),
(882, 218, 'Carchi', 'America/Guayaquil'),
(883, 218, 'Chimborazo', 'America/Guayaquil'),
(884, 218, 'Cotopaxi', 'America/Guayaquil'),
(885, 218, 'El Oro', 'America/Guayaquil'),
(886, 218, 'Esmeraldas', 'America/Guayaquil'),
(887, 218, 'Guayas', 'America/Guayaquil'),
(888, 218, 'Imbabura', 'America/Guayaquil'),
(889, 218, 'Loja', 'America/Guayaquil'),
(890, 218, 'Los Ríos', 'America/Guayaquil'),
(891, 218, 'Manabí', 'America/Guayaquil'),
(892, 218, 'Morona-Santiago', 'America/Guayaquil'),
(893, 218, 'Napo', 'America/Guayaquil'),
(894, 218, 'Pastaza', 'America/Guayaquil'),
(895, 218, 'Pichincha', 'America/Guayaquil'),
(896, 218, 'Tungurahua', 'America/Guayaquil'),
(897, 218, 'Zamora-Chinchipe', 'America/Guayaquil'),
(898, 218, 'Sucumbios', 'America/Guayaquil'),
(899, 218, 'Napo', 'America/Guayaquil'),
(900, 218, 'Orellana', 'America/Guayaquil'),
(901, 218, 'Provincia de Santa Elena', 'America/Guayaquil'),
(902, 218, 'Provincia de Santo Domingo de los Tsáchilas', 'America/Guayaquil'),
(903, 222, 'Ahuachapán', 'America/El_Salvador'),
(904, 222, 'Cabañas', 'America/El_Salvador'),
(905, 222, 'Chalatenango', 'America/El_Salvador'),
(906, 222, 'Cuscatlán', 'America/El_Salvador'),
(907, 222, 'La Libertad', 'America/El_Salvador'),
(908, 222, 'La Paz', 'America/El_Salvador'),
(909, 222, 'La Unión', 'America/El_Salvador'),
(910, 222, 'Morazán', 'America/El_Salvador'),
(911, 222, 'San Miguel', 'America/El_Salvador'),
(912, 222, 'San Salvador', 'America/El_Salvador'),
(913, 222, 'Santa Ana', 'America/El_Salvador'),
(914, 222, 'San Vicente', 'America/El_Salvador'),
(915, 222, 'Sonsonate', 'America/El_Salvador'),
(916, 222, 'Usulután', 'America/El_Salvador'),
(917, 226, 'Annobón', 'Africa/Malabo'),
(918, 226, 'Bioko Norte', 'Africa/Malabo'),
(919, 226, 'Bioko Sur', 'Africa/Malabo'),
(920, 226, 'Centro Sur', 'Africa/Malabo'),
(921, 226, 'Kié-Ntem', 'Africa/Malabo'),
(922, 226, 'Litoral', 'Africa/Malabo'),
(923, 226, 'Wele-Nzas', 'Africa/Malabo'),
(924, 231, 'Arsi', 'Africa/Addis_Ababa'),
(925, 231, 'Gonder', 'Africa/Addis_Ababa'),
(926, 231, 'Bale', 'Africa/Addis_Ababa'),
(927, 231, 'Eritrea', 'Africa/Addis_Ababa'),
(928, 231, 'Gamo Gofa', 'Africa/Addis_Ababa'),
(929, 231, 'Gojam', 'Africa/Addis_Ababa'),
(930, 231, 'Harerge', 'Africa/Addis_Ababa'),
(931, 231, 'Ilubabor', 'Africa/Addis_Ababa'),
(932, 231, 'Kefa', 'Africa/Addis_Ababa'),
(933, 231, 'Addis Abeba', 'Africa/Addis_Ababa'),
(934, 231, 'Sidamo', 'Africa/Addis_Ababa'),
(935, 231, 'Tigray', 'Africa/Addis_Ababa'),
(936, 231, 'Welega Kifle Hager', 'Africa/Addis_Ababa'),
(937, 231, 'Welo', 'Africa/Addis_Ababa'),
(938, 231, 'Adis Abeba', 'Africa/Addis_Ababa'),
(939, 231, 'Asosa', 'Africa/Addis_Ababa'),
(940, 231, 'Borena ', 'Africa/Addis_Ababa'),
(941, 231, 'Debub Gonder', 'Africa/Addis_Ababa'),
(942, 231, 'Debub Shewa', 'Africa/Addis_Ababa'),
(943, 231, 'Debub Welo', 'Africa/Addis_Ababa'),
(944, 231, 'Dire Dawa', 'Africa/Addis_Ababa'),
(945, 231, 'Gambela', 'Africa/Addis_Ababa'),
(946, 231, 'Metekel', 'Africa/Addis_Ababa'),
(947, 231, 'Mirab Gojam', 'Africa/Addis_Ababa'),
(948, 231, 'Mirab Harerge', 'Africa/Addis_Ababa'),
(949, 231, 'Mirab Shewa', 'Africa/Addis_Ababa'),
(950, 231, 'Misrak Gojam', 'Africa/Addis_Ababa'),
(951, 231, 'Misrak Harerge', 'Africa/Addis_Ababa'),
(952, 231, 'Nazret', 'Africa/Addis_Ababa'),
(953, 231, 'Ogaden', 'Africa/Addis_Ababa'),
(954, 231, 'Omo', 'Africa/Addis_Ababa'),
(955, 231, 'Semen Gonder', 'Africa/Addis_Ababa'),
(956, 231, 'Semen Shewa', 'Africa/Addis_Ababa'),
(957, 231, 'Semen Welo', 'Africa/Addis_Ababa'),
(958, 231, 'Tigray', 'Africa/Addis_Ababa'),
(959, 231, 'Bale', 'Africa/Addis_Ababa'),
(960, 231, 'Gamo Gofa', 'Africa/Addis_Ababa'),
(961, 231, 'Ilubabor', 'Africa/Addis_Ababa'),
(962, 231, 'Kefa', 'Africa/Addis_Ababa'),
(963, 231, 'Sidamo', 'Africa/Addis_Ababa'),
(964, 231, 'Welega', 'Africa/Addis_Ababa'),
(965, 231, 'Ādīs Ābeba', 'Africa/Addis_Ababa'),
(966, 231, 'Āfar', 'Africa/Addis_Ababa'),
(967, 231, 'Āmara', 'Africa/Addis_Ababa'),
(968, 231, 'Bīnshangul Gumuz', 'Africa/Addis_Ababa'),
(969, 231, 'Dirē Dawa', 'Africa/Addis_Ababa'),
(970, 231, 'Gambēla Hizboch', 'Africa/Addis_Ababa'),
(971, 231, 'Hārerī Hizb', 'Africa/Addis_Ababa'),
(972, 231, 'Oromīya', 'Africa/Addis_Ababa'),
(973, 231, 'Sumalē', 'Africa/Addis_Ababa'),
(974, 231, 'Tigray', 'Africa/Addis_Ababa'),
(975, 231, 'YeDebub Bihēroch Bihēreseboch na Hizboch', 'Africa/Addis_Ababa'),
(976, 232, 'Ānseba', 'Africa/Asmara'),
(977, 232, 'Debub', 'Africa/Asmara'),
(978, 232, 'Debubawī Kʼeyih Bahrī', 'Africa/Asmara'),
(979, 232, 'Gash Barka', 'Africa/Asmara'),
(980, 232, 'Maʼākel', 'Africa/Asmara'),
(981, 232, 'Semēnawī Kʼeyih Bahrī', 'Africa/Asmara'),
(982, 233, 'Harjumaa', 'Europe/Tallinn'),
(983, 233, 'Hiiumaa', 'Europe/Tallinn'),
(984, 233, 'Ida-Virumaa', 'Europe/Tallinn'),
(985, 233, 'Järvamaa', 'Europe/Tallinn'),
(986, 233, 'Jõgevamaa', 'Europe/Tallinn'),
(987, 233, 'Läänemaa', 'Europe/Tallinn'),
(988, 233, 'Lääne-Virumaa', 'Europe/Tallinn'),
(989, 233, 'Pärnumaa', 'Europe/Tallinn'),
(990, 233, 'Põlvamaa', 'Europe/Tallinn'),
(991, 233, 'Raplamaa', 'Europe/Tallinn'),
(992, 233, 'Saaremaa', 'Europe/Tallinn'),
(993, 233, 'Tartumaa', 'Europe/Tallinn'),
(994, 233, 'Valgamaa', 'Europe/Tallinn'),
(995, 233, 'Viljandimaa', 'Europe/Tallinn'),
(996, 233, 'Võrumaa', 'Europe/Tallinn'),
(997, 234, 'Norðoyar region', 'Atlantic/Faroe'),
(998, 234, 'Eysturoy region', 'Atlantic/Faroe'),
(999, 234, 'Sandoy region', 'Atlantic/Faroe'),
(1000, 234, 'Streymoy region', 'Atlantic/Faroe'),
(1001, 234, 'Suðuroy region', 'Atlantic/Faroe'),
(1002, 234, 'Vágar region', 'Atlantic/Faroe'),
(1003, 238, 'Falkland Islands (Islas Malvinas)', 'Atlantic/Stanley'),
(1004, 239, 'South Georgia and The South Sandwich Islands ', 'Atlantic/South_Georgia'),
(1005, 242, 'Central', 'Pacific/Fiji'),
(1006, 242, 'Eastern', 'Pacific/Fiji'),
(1007, 242, 'Northern', 'Pacific/Fiji'),
(1008, 242, 'Rotuma', 'Pacific/Fiji'),
(1009, 242, 'Western', 'Pacific/Fiji'),
(1010, 246, 'Åland', 'Europe/Helsinki'),
(1011, 246, 'Hame', 'Europe/Helsinki'),
(1012, 246, 'Keski-Suomi', 'Europe/Helsinki'),
(1013, 246, 'Kuopio', 'Europe/Helsinki'),
(1014, 246, 'Kymi', 'Europe/Helsinki'),
(1015, 246, 'Lapponia', 'Europe/Helsinki'),
(1016, 246, 'Mikkeli', 'Europe/Helsinki'),
(1017, 246, 'Oulu', 'Europe/Helsinki'),
(1018, 246, 'Pohjois-Karjala', 'Europe/Helsinki'),
(1019, 246, 'Turku ja Pori', 'Europe/Helsinki'),
(1020, 246, 'Uusimaa', 'Europe/Helsinki'),
(1021, 246, 'Vaasa', 'Europe/Helsinki'),
(1022, 246, 'Southern Finland', 'Europe/Helsinki'),
(1023, 246, 'Eastern Finland', 'Europe/Helsinki'),
(1024, 246, 'Western Finland', 'Europe/Helsinki'),
(1025, 250, 'Aquitaine', 'Europe/Paris'),
(1026, 250, 'Auvergne', 'Europe/Paris'),
(1027, 250, 'Basse-Normandie', 'Europe/Paris'),
(1028, 250, 'Bourgogne', 'Europe/Paris'),
(1029, 250, 'Brittany', 'Europe/Paris'),
(1030, 250, 'Centre', 'Europe/Paris'),
(1031, 250, 'Champagne-Ardenne', 'Europe/Paris'),
(1032, 250, 'Corsica', 'Europe/Paris'),
(1033, 250, 'Franche-Comté', 'Europe/Paris'),
(1034, 250, 'Haute-Normandie', 'Europe/Paris'),
(1035, 250, 'Île-de-France', 'Europe/Paris'),
(1036, 250, 'Languedoc-Roussillon', 'Europe/Paris'),
(1037, 250, 'Limousin', 'Europe/Paris'),
(1038, 250, 'Lorraine', 'Europe/Paris'),
(1039, 250, 'Midi-Pyrénées', 'Europe/Paris'),
(1040, 250, 'Nord-Pas-de-Calais', 'Europe/Paris'),
(1041, 250, 'Pays de la Loire', 'Europe/Paris'),
(1042, 250, 'Picardie', 'Europe/Paris'),
(1043, 250, 'Poitou-Charentes', 'Europe/Paris'),
(1044, 250, 'Provence-Alpes-Côte dʼAzur', 'Europe/Paris'),
(1045, 250, 'Rhône-Alpes', 'Europe/Paris'),
(1046, 250, 'Alsace', 'Europe/Paris'),
(1047, 254, 'Guyane', 'America/Guyana'),
(1048, 260, 'Saint-Paul-et-Amsterdam', 'Indian/Kerguelen'),
(1049, 260, 'Crozet', 'Indian/Kerguelen'),
(1050, 260, 'Kerguelen', 'Indian/Kerguelen'),
(1051, 260, 'Terre Adélie', 'Indian/Kerguelen'),
(1052, 260, 'Îles Éparses', 'Indian/Kerguelen'),
(1053, 262, 'Ali Sabieh', 'Africa/Djibouti'),
(1054, 262, 'Dikhil   ', 'Africa/Djibouti'),
(1055, 262, 'Djibouti  ', 'Africa/Djibouti'),
(1056, 262, 'Obock', 'Africa/Djibouti'),
(1057, 262, 'Tadjourah', 'Africa/Djibouti'),
(1058, 262, 'Dikhil', 'Africa/Djibouti'),
(1059, 262, 'Djibouti', 'Africa/Djibouti'),
(1060, 262, 'Arta', 'Africa/Djibouti'),
(1061, 266, 'Estuaire', 'Africa/Libreville'),
(1062, 266, 'Haut-Ogooué', 'Africa/Libreville'),
(1063, 266, 'Moyen-Ogooué', 'Africa/Libreville'),
(1064, 266, 'Ngounié', 'Africa/Libreville'),
(1065, 266, 'Nyanga', 'Africa/Libreville'),
(1066, 266, 'Ogooué-Ivindo', 'Africa/Libreville'),
(1067, 266, 'Ogooué-Lolo', 'Africa/Libreville'),
(1068, 266, 'Ogooué-Maritime', 'Africa/Libreville'),
(1069, 266, 'Woleu-Ntem', 'Africa/Libreville'),
(1070, 268, 'Ajaria', 'Asia/Tbilisi'),
(1071, 268, 'Tʼbilisi', 'Asia/Tbilisi'),
(1072, 268, 'Abkhazia', 'Asia/Tbilisi'),
(1073, 268, 'Kvemo Kartli', 'Asia/Tbilisi'),
(1074, 268, 'Kakheti', 'Asia/Tbilisi'),
(1075, 268, 'Guria', 'Asia/Tbilisi'),
(1076, 268, 'Imereti', 'Asia/Tbilisi'),
(1077, 268, 'Shida Kartli', 'Asia/Tbilisi'),
(1078, 268, 'Mtskheta-Mtianeti', 'Asia/Tbilisi'),
(1079, 268, 'Racha-Lechkhumi and Kvemo Svaneti', 'Asia/Tbilisi'),
(1080, 268, 'Samegrelo and Zemo Svaneti', 'Asia/Tbilisi'),
(1081, 268, 'Samtskhe-Javakheti', 'Asia/Tbilisi'),
(1082, 270, 'Banjul', 'Africa/Banjul'),
(1083, 270, 'Lower River', 'Africa/Banjul'),
(1084, 270, 'Central River', 'Africa/Banjul'),
(1085, 270, 'Upper River', 'Africa/Banjul'),
(1086, 270, 'Western', 'Africa/Banjul'),
(1087, 270, 'North Bank', 'Africa/Banjul'),
(1088, 275, 'Gaza Strip', 'Asia/Gaza'),
(1089, 275, 'West Bank', 'Asia/Gaza'),
(1090, 276, 'Baden-Württemberg', 'Europe/Berlin'),
(1091, 276, 'Bavaria', 'Europe/Berlin'),
(1092, 276, 'Bremen', 'Europe/Berlin'),
(1093, 276, 'Hamburg', 'Europe/Berlin'),
(1094, 276, 'Hesse', 'Europe/Berlin'),
(1095, 276, 'Lower Saxony', 'Europe/Berlin'),
(1096, 276, 'North Rhine-Westphalia', 'Europe/Berlin'),
(1097, 276, 'Rhineland-Palatinate', 'Europe/Berlin'),
(1098, 276, 'Saarland', 'Europe/Berlin'),
(1099, 276, 'Schleswig-Holstein', 'Europe/Berlin'),
(1100, 276, 'Brandenburg', 'Europe/Berlin'),
(1101, 276, 'Mecklenburg-Vorpommern', 'Europe/Berlin'),
(1102, 276, 'Saxony', 'Europe/Berlin'),
(1103, 276, 'Saxony-Anhalt', 'Europe/Berlin'),
(1104, 276, 'Thuringia', 'Europe/Berlin'),
(1105, 276, 'Berlin', 'Europe/Berlin'),
(1106, 288, 'Greater Accra', 'Africa/Accra'),
(1107, 288, 'Ashanti', 'Africa/Accra'),
(1108, 288, 'Brong-Ahafo Region', 'Africa/Accra'),
(1109, 288, 'Central', 'Africa/Accra'),
(1110, 288, 'Eastern', 'Africa/Accra'),
(1111, 288, 'Northern', 'Africa/Accra'),
(1112, 288, 'Volta', 'Africa/Accra'),
(1113, 288, 'Western', 'Africa/Accra'),
(1114, 288, 'Upper East', 'Africa/Accra'),
(1115, 288, 'Upper West', 'Africa/Accra'),
(1116, 292, 'Gibraltar', 'Europe/Gibraltar'),
(1117, 296, 'Line Islands', 'Pacific/Kiritimati'),
(1118, 296, 'Gilbert Islands', 'Pacific/Tarawa'),
(1119, 296, 'Phoenix Islands', 'Pacific/Enderbury'),
(1120, 300, 'Mount Athos', 'Europe/Athens'),
(1121, 300, 'East Macedonia and Thrace', 'Europe/Athens'),
(1122, 300, 'Central Macedonia', 'Europe/Athens'),
(1123, 300, 'West Macedonia', 'Europe/Athens'),
(1124, 300, 'Thessaly', 'Europe/Athens'),
(1125, 300, 'Epirus', 'Europe/Athens'),
(1126, 300, 'Ionian Islands', 'Europe/Athens'),
(1127, 300, 'West Greece', 'Europe/Athens'),
(1128, 300, 'Central Greece', 'Europe/Athens'),
(1129, 300, 'Peloponnese', 'Europe/Athens'),
(1130, 300, 'Attica', 'Europe/Athens'),
(1131, 300, 'North Aegean', 'Europe/Athens'),
(1132, 300, 'South Aegean', 'Europe/Athens'),
(1133, 300, 'Crete', 'Europe/Athens'),
(1134, 304, 'Nordgrønland', 'America/Godthab'),
(1135, 304, 'Østgrønland', 'America/Godthab'),
(1136, 304, 'Vestgrønland', 'America/Godthab'),
(1137, 308, 'Saint Andrew', 'America/Grenada'),
(1138, 308, 'Saint David', 'America/Grenada'),
(1139, 308, 'Saint George', 'America/Grenada'),
(1140, 308, 'Saint John', 'America/Grenada'),
(1141, 308, 'Saint Mark', 'America/Grenada'),
(1142, 308, 'Saint Patrick', 'America/Grenada'),
(1143, 312, 'Guadeloupe', 'America/Guadeloupe'),
(1144, 316, 'Guam', 'Pacific/Guam'),
(1145, 320, 'Alta Verapaz', 'America/Guatemala'),
(1146, 320, 'Baja Verapaz', 'America/Guatemala'),
(1147, 320, 'Chimaltenango', 'America/Guatemala'),
(1148, 320, 'Chiquimula', 'America/Guatemala'),
(1149, 320, 'El Progreso', 'America/Guatemala'),
(1150, 320, 'Escuintla', 'America/Guatemala'),
(1151, 320, 'Guatemala', 'America/Guatemala'),
(1152, 320, 'Huehuetenango', 'America/Guatemala'),
(1153, 320, 'Izabal', 'America/Guatemala'),
(1154, 320, 'Jalapa', 'America/Guatemala'),
(1155, 320, 'Jutiapa', 'America/Guatemala'),
(1156, 320, 'Petén', 'America/Guatemala'),
(1157, 320, 'Quetzaltenango', 'America/Guatemala'),
(1158, 320, 'Quiché', 'America/Guatemala'),
(1159, 320, 'Retalhuleu', 'America/Guatemala'),
(1160, 320, 'Sacatepéquez', 'America/Guatemala'),
(1161, 320, 'San Marcos', 'America/Guatemala'),
(1162, 320, 'Santa Rosa', 'America/Guatemala'),
(1163, 320, 'Sololá', 'America/Guatemala'),
(1164, 320, 'Suchitepéquez', 'America/Guatemala'),
(1165, 320, 'Totonicapán', 'America/Guatemala'),
(1166, 320, 'Zacapa', 'America/Guatemala'),
(1167, 324, 'Beyla', 'Africa/Conakry'),
(1168, 324, 'Boffa', 'Africa/Conakry'),
(1169, 324, 'Boké', 'Africa/Conakry'),
(1170, 324, 'Conakry', 'Africa/Conakry'),
(1171, 324, 'Dabola', 'Africa/Conakry'),
(1172, 324, 'Dalaba', 'Africa/Conakry');
INSERT INTO `country_state` (`id`, `country_id`, `name`, `timezone`) VALUES
(1173, 324, 'Dinguiraye', 'Africa/Conakry'),
(1174, 324, 'Faranah', 'Africa/Conakry'),
(1175, 324, 'Forécariah', 'Africa/Conakry'),
(1176, 324, 'Fria', 'Africa/Conakry'),
(1177, 324, 'Gaoual', 'Africa/Conakry'),
(1178, 324, 'Guéckédou', 'Africa/Conakry'),
(1179, 324, 'Kankan', 'Africa/Conakry'),
(1180, 324, 'Kérouané', 'Africa/Conakry'),
(1181, 324, 'Kindia', 'Africa/Conakry'),
(1182, 324, 'Kissidougou', 'Africa/Conakry'),
(1183, 324, 'Koundara', 'Africa/Conakry'),
(1184, 324, 'Kouroussa', 'Africa/Conakry'),
(1185, 324, 'Macenta', 'Africa/Conakry'),
(1186, 324, 'Mali', 'Africa/Conakry'),
(1187, 324, 'Mamou', 'Africa/Conakry'),
(1188, 324, 'Pita', 'Africa/Conakry'),
(1189, 324, 'Siguiri', 'Africa/Conakry'),
(1190, 324, 'Télimélé', 'Africa/Conakry'),
(1191, 324, 'Tougué', 'Africa/Conakry'),
(1192, 324, 'Yomou', 'Africa/Conakry'),
(1193, 324, 'Coyah', 'Africa/Conakry'),
(1194, 324, 'Dubréka', 'Africa/Conakry'),
(1195, 324, 'Kankan', 'Africa/Conakry'),
(1196, 324, 'Koubia', 'Africa/Conakry'),
(1197, 324, 'Labé', 'Africa/Conakry'),
(1198, 324, 'Lélouma', 'Africa/Conakry'),
(1199, 324, 'Lola', 'Africa/Conakry'),
(1200, 324, 'Mandiana', 'Africa/Conakry'),
(1201, 324, 'Nzérékoré', 'Africa/Conakry'),
(1202, 324, 'Siguiri', 'Africa/Conakry'),
(1203, 328, 'Barima-Waini', 'America/Guyana'),
(1204, 328, 'Cuyuni-Mazaruni', 'America/Guyana'),
(1205, 328, 'Demerara-Mahaica', 'America/Guyana'),
(1206, 328, 'East Berbice-Corentyne', 'America/Guyana'),
(1207, 328, 'Essequibo Islands-West Demerara', 'America/Guyana'),
(1208, 328, 'Mahaica-Berbice', 'America/Guyana'),
(1209, 328, 'Pomeroon-Supenaam', 'America/Guyana'),
(1210, 328, 'Potaro-Siparuni', 'America/Guyana'),
(1211, 328, 'Upper Demerara-Berbice', 'America/Guyana'),
(1212, 328, 'Upper Takutu-Upper Essequibo', 'America/Guyana'),
(1213, 332, 'Nord-Ouest', 'America/Port-au-Prince'),
(1214, 332, 'Artibonite', 'America/Port-au-Prince'),
(1215, 332, 'Centre', 'America/Port-au-Prince'),
(1216, 332, 'Nord', 'America/Port-au-Prince'),
(1217, 332, 'Nord-Est', 'America/Port-au-Prince'),
(1218, 332, 'Ouest', 'America/Port-au-Prince'),
(1219, 332, 'Sud', 'America/Port-au-Prince'),
(1220, 332, 'Sud-Est', 'America/Port-au-Prince'),
(1221, 332, 'GrandʼAnse', 'America/Port-au-Prince'),
(1222, 332, 'Nippes', 'America/Port-au-Prince'),
(1223, 336, 'Vatican City', 'Europe/Vatican'),
(1224, 340, 'Atlántida', 'America/Tegucigalpa'),
(1225, 340, 'Choluteca', 'America/Tegucigalpa'),
(1226, 340, 'Colón', 'America/Tegucigalpa'),
(1227, 340, 'Comayagua', 'America/Tegucigalpa'),
(1228, 340, 'Copán', 'America/Tegucigalpa'),
(1229, 340, 'Cortés', 'America/Tegucigalpa'),
(1230, 340, 'El Paraíso', 'America/Tegucigalpa'),
(1231, 340, 'Francisco Morazán', 'America/Tegucigalpa'),
(1232, 340, 'Gracias a Dios', 'America/Tegucigalpa'),
(1233, 340, 'Intibucá', 'America/Tegucigalpa'),
(1234, 340, 'Islas de la Bahía', 'America/Tegucigalpa'),
(1235, 340, 'La Paz', 'America/Tegucigalpa'),
(1236, 340, 'Lempira', 'America/Tegucigalpa'),
(1237, 340, 'Ocotepeque', 'America/Tegucigalpa'),
(1238, 340, 'Olancho', 'America/Tegucigalpa'),
(1239, 340, 'Santa Bárbara', 'America/Tegucigalpa'),
(1240, 340, 'Valle', 'America/Tegucigalpa'),
(1241, 340, 'Yoro', 'America/Tegucigalpa'),
(1242, 344, 'Hong Kong', 'Asia/Hong_Kong'),
(1243, 348, 'Bács-Kiskun', 'Europe/Budapest'),
(1244, 348, 'Baranya', 'Europe/Budapest'),
(1245, 348, 'Békés', 'Europe/Budapest'),
(1246, 348, 'Borsod-Abaúj-Zemplén', 'Europe/Budapest'),
(1247, 348, 'Budapest', 'Europe/Budapest'),
(1248, 348, 'Csongrád', 'Europe/Budapest'),
(1249, 348, 'Fejér', 'Europe/Budapest'),
(1250, 348, 'Győr-Moson-Sopron', 'Europe/Budapest'),
(1251, 348, 'Hajdú-Bihar', 'Europe/Budapest'),
(1252, 348, 'Heves', 'Europe/Budapest'),
(1253, 348, 'Komárom-Esztergom', 'Europe/Budapest'),
(1254, 348, 'Nógrád', 'Europe/Budapest'),
(1255, 348, 'Pest', 'Europe/Budapest'),
(1256, 348, 'Somogy', 'Europe/Budapest'),
(1257, 348, 'Szabolcs-Szatmár-Bereg', 'Europe/Budapest'),
(1258, 348, 'Jász-Nagykun-Szolnok', 'Europe/Budapest'),
(1259, 348, 'Tolna', 'Europe/Budapest'),
(1260, 348, 'Vas', 'Europe/Budapest'),
(1261, 348, 'Veszprém', 'Europe/Budapest'),
(1262, 348, 'Zala', 'Europe/Budapest'),
(1263, 352, 'Borgarfjardarsysla', 'Atlantic/Reykjavik'),
(1264, 352, 'Dalasysla', 'Atlantic/Reykjavik'),
(1265, 352, 'Eyjafjardarsysla', 'Atlantic/Reykjavik'),
(1266, 352, 'Gullbringusysla', 'Atlantic/Reykjavik'),
(1267, 352, 'Hafnarfjördur', 'Atlantic/Reykjavik'),
(1268, 352, 'Husavik', 'Atlantic/Reykjavik'),
(1269, 352, 'Isafjördur', 'Atlantic/Reykjavik'),
(1270, 352, 'Keflavik', 'Atlantic/Reykjavik'),
(1271, 352, 'Kjosarsysla', 'Atlantic/Reykjavik'),
(1272, 352, 'Kopavogur', 'Atlantic/Reykjavik'),
(1273, 352, 'Myrasysla', 'Atlantic/Reykjavik'),
(1274, 352, 'Neskaupstadur', 'Atlantic/Reykjavik'),
(1275, 352, 'Nordur-Isafjardarsysla', 'Atlantic/Reykjavik'),
(1276, 352, 'Nordur-Mulasysla', 'Atlantic/Reykjavik'),
(1277, 352, 'Nordur-Tingeyjarsysla', 'Atlantic/Reykjavik'),
(1278, 352, 'Olafsfjördur', 'Atlantic/Reykjavik'),
(1279, 352, 'Rangarvallasysla', 'Atlantic/Reykjavik'),
(1280, 352, 'Reykjavík', 'Atlantic/Reykjavik'),
(1281, 352, 'Saudarkrokur', 'Atlantic/Reykjavik'),
(1282, 352, 'Seydisfjordur', 'Atlantic/Reykjavik'),
(1283, 352, 'Siglufjordur', 'Atlantic/Reykjavik'),
(1284, 352, 'Skagafjardarsysla', 'Atlantic/Reykjavik'),
(1285, 352, 'Snafellsnes- og Hnappadalssysla', 'Atlantic/Reykjavik'),
(1286, 352, 'Strandasysla', 'Atlantic/Reykjavik'),
(1287, 352, 'Sudur-Mulasysla', 'Atlantic/Reykjavik'),
(1288, 352, 'Sudur-Tingeyjarsysla', 'Atlantic/Reykjavik'),
(1289, 352, 'Vestmannaeyjar', 'Atlantic/Reykjavik'),
(1290, 352, 'Vestur-Bardastrandarsysla', 'Atlantic/Reykjavik'),
(1291, 352, 'Vestur-Hunavatnssysla', 'Atlantic/Reykjavik'),
(1292, 352, 'Vestur-Isafjardarsysla', 'Atlantic/Reykjavik'),
(1293, 352, 'Vestur-Skaftafellssysla', 'Atlantic/Reykjavik'),
(1294, 352, 'East', 'Atlantic/Reykjavik'),
(1295, 352, 'Capital Region', 'Atlantic/Reykjavik'),
(1296, 352, 'Northeast', 'Atlantic/Reykjavik'),
(1297, 352, 'Northwest', 'Atlantic/Reykjavik'),
(1298, 352, 'South', 'Atlantic/Reykjavik'),
(1299, 352, 'Southern Peninsula', 'Atlantic/Reykjavik'),
(1300, 352, 'Westfjords', 'Atlantic/Reykjavik'),
(1301, 352, 'West', 'Atlantic/Reykjavik'),
(1302, 356, 'Andaman and Nicobar Islands', 'Asia/Kolkata'),
(1303, 356, 'Andhra Pradesh', 'Asia/Kolkata'),
(1304, 356, 'Assam', 'Asia/Kolkata'),
(1305, 356, 'Bihar', 'Asia/Kolkata'),
(1306, 356, 'Chandīgarh', 'Asia/Kolkata'),
(1307, 356, 'Dādra and Nagar Haveli', 'Asia/Kolkata'),
(1308, 356, 'NCT', 'Asia/Kolkata'),
(1309, 356, 'Gujarāt', 'Asia/Kolkata'),
(1310, 356, 'Haryana', 'Asia/Kolkata'),
(1311, 356, 'Himāchal Pradesh', 'Asia/Kolkata'),
(1312, 356, 'Kashmir', 'Asia/Kolkata'),
(1313, 356, 'Kerala', 'Asia/Kolkata'),
(1314, 356, 'Laccadives', 'Asia/Kolkata'),
(1315, 356, 'Madhya Pradesh ', 'Asia/Kolkata'),
(1316, 356, 'Mahārāshtra', 'Asia/Kolkata'),
(1317, 356, 'Manipur', 'Asia/Kolkata'),
(1318, 356, 'Meghālaya', 'Asia/Kolkata'),
(1319, 356, 'Karnātaka', 'Asia/Kolkata'),
(1320, 356, 'Nāgāland', 'Asia/Kolkata'),
(1321, 356, 'Orissa', 'Asia/Kolkata'),
(1322, 356, 'Pondicherry', 'Asia/Kolkata'),
(1323, 356, 'Punjab', 'Asia/Kolkata'),
(1324, 356, 'State of Rājasthān', 'Asia/Kolkata'),
(1325, 356, 'Tamil Nādu', 'Asia/Kolkata'),
(1326, 356, 'Tripura', 'Asia/Kolkata'),
(1327, 356, 'Uttar Pradesh', 'Asia/Kolkata'),
(1328, 356, 'Bengal', 'Asia/Kolkata'),
(1329, 356, 'Sikkim', 'Asia/Kolkata'),
(1330, 356, 'Arunāchal Pradesh', 'Asia/Kolkata'),
(1331, 356, 'Mizoram', 'Asia/Kolkata'),
(1332, 356, 'Daman and Diu', 'Asia/Kolkata'),
(1333, 356, 'Goa', 'Asia/Kolkata'),
(1334, 356, 'Bihār', 'Asia/Kolkata'),
(1335, 356, 'Madhya Pradesh', 'Asia/Kolkata'),
(1336, 356, 'Uttar Pradesh', 'Asia/Kolkata'),
(1337, 356, 'Chhattisgarh', 'Asia/Kolkata'),
(1338, 356, 'Jharkhand', 'Asia/Kolkata'),
(1339, 356, 'Uttarakhand', 'Asia/Kolkata'),
(1340, 360, 'Aceh', 'Asia/Jakarta'),
(1341, 360, 'Bali', 'Asia/Ujung_Pandang'),
(1342, 360, 'Bengkulu', 'Asia/Jakarta'),
(1343, 360, 'Jakarta Raya', 'Asia/Jakarta'),
(1344, 360, 'Jambi', 'Asia/Jakarta'),
(1345, 360, 'Jawa Barat', 'Asia/Jakarta'),
(1346, 360, 'Central Java', 'Asia/Jakarta'),
(1347, 360, 'East Java', 'Asia/Jakarta'),
(1348, 360, 'Yogyakarta ', 'Asia/Jakarta'),
(1349, 360, 'West Kalimantan', 'Asia/Jakarta'),
(1350, 360, 'South Kalimantan', 'Asia/Ujung_Pandang'),
(1351, 360, 'Kalimantan Tengah', 'Asia/Jakarta'),
(1352, 360, 'East Kalimantan', 'Asia/Ujung_Pandang'),
(1353, 360, 'Lampung', 'Asia/Jakarta'),
(1354, 360, 'Nusa Tenggara Barat', 'Asia/Ujung_Pandang'),
(1355, 360, 'East Nusa Tenggara', 'Asia/Ujung_Pandang'),
(1356, 360, 'Central Sulawesi', 'Asia/Ujung_Pandang'),
(1357, 360, 'Sulawesi Tenggara', 'Asia/Ujung_Pandang'),
(1358, 360, 'Sulawesi Utara', 'Asia/Ujung_Pandang'),
(1359, 360, 'West Sumatra', 'Asia/Jakarta'),
(1360, 360, 'Sumatera Selatan', 'Asia/Jakarta'),
(1361, 360, 'North Sumatra', 'Asia/Jakarta'),
(1362, 360, 'Timor Timur', 'Asia/Ujung_Pandang'),
(1363, 360, 'Maluku ', 'Asia/Jayapura'),
(1364, 360, 'Maluku Utara', 'Asia/Jayapura'),
(1365, 360, 'West Java', 'Asia/Jakarta'),
(1366, 360, 'North Sulawesi', 'Asia/Ujung_Pandang'),
(1367, 360, 'South Sumatra', 'Asia/Jakarta'),
(1368, 360, 'Banten', 'Asia/Jakarta'),
(1369, 360, 'Gorontalo', 'Asia/Ujung_Pandang'),
(1370, 360, 'Bangka-Belitung', 'Asia/Jakarta'),
(1371, 360, 'Papua', 'Asia/Jayapura'),
(1372, 360, 'Riau', 'Asia/Jakarta'),
(1373, 360, 'South Sulawesi', 'Asia/Ujung_Pandang'),
(1374, 360, 'Irian Jaya Barat', 'Asia/Jayapura'),
(1375, 360, 'Riau Islands', 'Asia/Jakarta'),
(1376, 360, 'Sulawesi Barat', 'Asia/Ujung_Pandang'),
(1377, 364, 'Āz̄ārbāyjān-e Gharbī', 'Asia/Tehran'),
(1378, 364, 'Chahār Maḩāll va Bakhtīārī', 'Asia/Tehran'),
(1379, 364, 'Sīstān va Balūchestān', 'Asia/Tehran'),
(1380, 364, 'Kohgīlūyeh va Būyer Aḩmad', 'Asia/Tehran'),
(1381, 364, 'Fārs Province', 'Asia/Tehran'),
(1382, 364, 'Gīlān', 'Asia/Tehran'),
(1383, 364, 'Hamadān', 'Asia/Tehran'),
(1384, 364, 'Īlām', 'Asia/Tehran'),
(1385, 364, 'Hormozgān Province', 'Asia/Tehran'),
(1386, 364, 'Kerman', 'Asia/Tehran'),
(1387, 364, 'Kermānshāh', 'Asia/Tehran'),
(1388, 364, 'Khūzestān', 'Asia/Tehran'),
(1389, 364, 'Kordestān', 'Asia/Tehran'),
(1390, 364, 'Mazandaran', 'Asia/Tehran'),
(1391, 364, 'Markazi', 'Asia/Tehran'),
(1392, 364, 'Zanjan', 'Asia/Tehran'),
(1393, 364, 'Bushehr Province', 'Asia/Tehran'),
(1394, 364, 'Lorestān', 'Asia/Tehran'),
(1395, 364, 'Markazi', 'Asia/Tehran'),
(1396, 364, 'Semnān Province', 'Asia/Tehran'),
(1397, 364, 'Tehrān', 'Asia/Tehran'),
(1398, 364, 'Zanjan', 'Asia/Tehran'),
(1399, 364, 'Eşfahān', 'Asia/Tehran'),
(1400, 364, 'Kermān', 'Asia/Tehran'),
(1401, 364, 'Ostan-e Khorasan-e Razavi', 'Asia/Tehran'),
(1402, 364, 'Yazd', 'Asia/Tehran'),
(1403, 364, 'Ardabīl', 'Asia/Tehran'),
(1404, 364, 'Āz̄ārbāyjān-e Sharqī', 'Asia/Tehran'),
(1405, 364, 'Markazi Province', 'Asia/Tehran'),
(1406, 364, 'Māzandarān Province', 'Asia/Tehran'),
(1407, 364, 'Zanjan Province', 'Asia/Tehran'),
(1408, 364, 'Golestān', 'Asia/Tehran'),
(1409, 364, 'Qazvīn', 'Asia/Tehran'),
(1410, 364, 'Qom', 'Asia/Tehran'),
(1411, 364, 'Yazd', 'Asia/Tehran'),
(1412, 364, 'Khorāsān-e Jonūbī', 'Asia/Tehran'),
(1413, 364, 'Razavi Khorasan Province', 'Asia/Tehran'),
(1414, 364, 'Khorāsān-e Shomālī', 'Asia/Tehran'),
(1415, 368, 'Anbar', 'Asia/Baghdad'),
(1416, 368, 'Al Başrah', 'Asia/Baghdad'),
(1417, 368, 'Al Muthanná', 'Asia/Baghdad'),
(1418, 368, 'Al Qādisīyah', 'Asia/Baghdad'),
(1419, 368, 'As Sulaymānīyah', 'Asia/Baghdad'),
(1420, 368, 'Bābil', 'Asia/Baghdad'),
(1421, 368, 'Baghdād', 'Asia/Baghdad'),
(1422, 368, 'Dahūk', 'Asia/Baghdad'),
(1423, 368, 'Dhi Qar', 'Asia/Baghdad'),
(1424, 368, 'Diyala', 'Asia/Baghdad'),
(1425, 368, 'Arbīl', 'Asia/Baghdad'),
(1426, 368, 'Karbalāʼ', 'Asia/Baghdad'),
(1427, 368, 'At Taʼmīm', 'Asia/Baghdad'),
(1428, 368, 'Maysan', 'Asia/Baghdad'),
(1429, 368, 'Nīnawá', 'Asia/Baghdad'),
(1430, 368, 'Wāsiţ', 'Asia/Baghdad'),
(1431, 368, 'An Najaf', 'Asia/Baghdad'),
(1432, 368, 'Şalāḩ ad Dīn', 'Asia/Baghdad'),
(1433, 372, 'Carlow', 'Europe/Dublin'),
(1434, 372, 'Cavan', 'Europe/Dublin'),
(1435, 372, 'County Clare', 'Europe/Dublin'),
(1436, 372, 'Cork', 'Europe/Dublin'),
(1437, 372, 'Donegal', 'Europe/Dublin'),
(1438, 372, 'Galway', 'Europe/Dublin'),
(1439, 372, 'County Kerry', 'Europe/Dublin'),
(1440, 372, 'County Kildare', 'Europe/Dublin'),
(1441, 372, 'County Kilkenny', 'Europe/Dublin'),
(1442, 372, 'Leitrim', 'Europe/Dublin'),
(1443, 372, 'Laois', 'Europe/Dublin'),
(1444, 372, 'Limerick', 'Europe/Dublin'),
(1445, 372, 'County Longford', 'Europe/Dublin'),
(1446, 372, 'County Louth', 'Europe/Dublin'),
(1447, 372, 'County Mayo', 'Europe/Dublin'),
(1448, 372, 'County Meath', 'Europe/Dublin'),
(1449, 372, 'Monaghan', 'Europe/Dublin'),
(1450, 372, 'County Offaly', 'Europe/Dublin'),
(1451, 372, 'County Roscommon', 'Europe/Dublin'),
(1452, 372, 'County Sligo', 'Europe/Dublin'),
(1453, 372, 'County Waterford', 'Europe/Dublin'),
(1454, 372, 'County Westmeath', 'Europe/Dublin'),
(1455, 372, 'County Wexford', 'Europe/Dublin'),
(1456, 372, 'County Wicklow', 'Europe/Dublin'),
(1457, 372, 'Dún Laoghaire-Rathdown', 'Europe/Dublin'),
(1458, 372, 'Fingal County', 'Europe/Dublin'),
(1459, 372, 'Tipperary North Riding', 'Europe/Dublin'),
(1460, 372, 'South Dublin', 'Europe/Dublin'),
(1461, 372, 'Tipperary South Riding', 'Europe/Dublin'),
(1462, 376, 'HaDarom', 'Asia/Jerusalem'),
(1463, 376, 'HaMerkaz', 'Asia/Jerusalem'),
(1464, 376, 'Northern District', 'Asia/Jerusalem'),
(1465, 376, 'H̱efa', 'Asia/Jerusalem'),
(1466, 376, 'Tel Aviv', 'Asia/Jerusalem'),
(1467, 376, 'Yerushalayim', 'Asia/Jerusalem'),
(1468, 380, 'Abruzzo', 'Europe/Rome'),
(1469, 380, 'Basilicate', 'Europe/Rome'),
(1470, 380, 'Calabria', 'Europe/Rome'),
(1471, 380, 'Campania', 'Europe/Rome'),
(1472, 380, 'Emilia-Romagna', 'Europe/Rome'),
(1473, 380, 'Friuli', 'Europe/Rome'),
(1474, 380, 'Lazio', 'Europe/Rome'),
(1475, 380, 'Liguria', 'Europe/Rome'),
(1476, 380, 'Lombardy', 'Europe/Rome'),
(1477, 380, 'The Marches', 'Europe/Rome'),
(1478, 380, 'Molise', 'Europe/Rome'),
(1479, 380, 'Piedmont', 'Europe/Rome'),
(1480, 380, 'Apulia', 'Europe/Rome'),
(1481, 380, 'Sardinia', 'Europe/Rome'),
(1482, 380, 'Sicily', 'Europe/Rome'),
(1483, 380, 'Tuscany', 'Europe/Rome'),
(1484, 380, 'Trentino-Alto Adige', 'Europe/Rome'),
(1485, 380, 'Umbria', 'Europe/Rome'),
(1486, 380, 'Aosta Valley', 'Europe/Rome'),
(1487, 380, 'Veneto', 'Europe/Rome'),
(1488, 384, 'Valparaíso Region', 'Africa/Abidjan'),
(1489, 384, 'Antofagasta Region', 'Africa/Abidjan'),
(1490, 384, 'Araucanía Region', 'Africa/Abidjan'),
(1491, 384, 'Atacama Region', 'Africa/Abidjan'),
(1492, 384, 'Biobío Region', 'Africa/Abidjan'),
(1493, 384, 'Coquimbo Region', 'Africa/Abidjan'),
(1494, 384, 'Maule Region', 'Africa/Abidjan'),
(1495, 384, 'Santiago Metropolitan Region', 'Africa/Abidjan'),
(1496, 384, 'Danane', 'Africa/Abidjan'),
(1497, 384, 'Divo', 'Africa/Abidjan'),
(1498, 384, 'Ferkessedougou', 'Africa/Abidjan'),
(1499, 384, 'Gagnoa', 'Africa/Abidjan'),
(1500, 384, 'Katiola', 'Africa/Abidjan'),
(1501, 384, 'Korhogo', 'Africa/Abidjan'),
(1502, 384, 'Odienne', 'Africa/Abidjan'),
(1503, 384, 'Seguela', 'Africa/Abidjan'),
(1504, 384, 'Touba', 'Africa/Abidjan'),
(1505, 384, 'Bongouanou', 'Africa/Abidjan'),
(1506, 384, 'Issia', 'Africa/Abidjan'),
(1507, 384, 'Lakota', 'Africa/Abidjan'),
(1508, 384, 'Mankono', 'Africa/Abidjan'),
(1509, 384, 'Oume', 'Africa/Abidjan'),
(1510, 384, 'Soubre', 'Africa/Abidjan'),
(1511, 384, 'Tingrela', 'Africa/Abidjan'),
(1512, 384, 'Zuenoula', 'Africa/Abidjan'),
(1513, 384, 'Bangolo', 'Africa/Abidjan'),
(1514, 384, 'Beoumi', 'Africa/Abidjan'),
(1515, 384, 'Bondoukou', 'Africa/Abidjan'),
(1516, 384, 'Bouafle', 'Africa/Abidjan'),
(1517, 384, 'Bouake', 'Africa/Abidjan'),
(1518, 384, 'Daloa', 'Africa/Abidjan'),
(1519, 384, 'Daoukro', 'Africa/Abidjan'),
(1520, 384, 'Duekoue', 'Africa/Abidjan'),
(1521, 384, 'Grand-Lahou', 'Africa/Abidjan'),
(1522, 384, 'Man', 'Africa/Abidjan'),
(1523, 384, 'Mbahiakro', 'Africa/Abidjan'),
(1524, 384, 'Sakassou', 'Africa/Abidjan'),
(1525, 384, 'San Pedro', 'Africa/Abidjan'),
(1526, 384, 'Sassandra', 'Africa/Abidjan'),
(1527, 384, 'Sinfra', 'Africa/Abidjan'),
(1528, 384, 'Tabou', 'Africa/Abidjan'),
(1529, 384, 'Tanda', 'Africa/Abidjan'),
(1530, 384, 'Tiassale', 'Africa/Abidjan'),
(1531, 384, 'Toumodi', 'Africa/Abidjan'),
(1532, 384, 'Vavoua', 'Africa/Abidjan'),
(1533, 384, 'Abidjan', 'Africa/Abidjan'),
(1534, 384, 'Aboisso', 'Africa/Abidjan'),
(1535, 384, 'Adiake', 'Africa/Abidjan'),
(1536, 384, 'Alepe', 'Africa/Abidjan'),
(1537, 384, 'Bocanda', 'Africa/Abidjan'),
(1538, 384, 'Dabou', 'Africa/Abidjan'),
(1539, 384, 'Dimbokro', 'Africa/Abidjan'),
(1540, 384, 'Grand-Bassam', 'Africa/Abidjan'),
(1541, 384, 'Guiglo', 'Africa/Abidjan'),
(1542, 384, 'Jacqueville', 'Africa/Abidjan'),
(1543, 384, 'Tiebissou', 'Africa/Abidjan'),
(1544, 384, 'Toulepleu', 'Africa/Abidjan'),
(1545, 384, 'Yamoussoukro', 'Africa/Abidjan'),
(1546, 384, 'Agnéby', 'Africa/Abidjan'),
(1547, 384, 'Bafing', 'Africa/Abidjan'),
(1548, 384, 'Bas-Sassandra', 'Africa/Abidjan'),
(1549, 384, 'Denguélé', 'Africa/Abidjan'),
(1550, 384, 'Dix-Huit Montagnes', 'Africa/Abidjan'),
(1551, 384, 'Fromager', 'Africa/Abidjan'),
(1552, 384, 'Haut-Sassandra', 'Africa/Abidjan'),
(1553, 384, 'Lacs', 'Africa/Abidjan'),
(1554, 384, 'Lagunes', 'Africa/Abidjan'),
(1555, 384, 'Marahoué', 'Africa/Abidjan'),
(1556, 384, 'Moyen-Cavally', 'Africa/Abidjan'),
(1557, 384, 'Moyen-Comoé', 'Africa/Abidjan'),
(1558, 384, 'Nʼzi-Comoé', 'Africa/Abidjan'),
(1559, 384, 'Savanes', 'Africa/Abidjan'),
(1560, 384, 'Sud-Bandama', 'Africa/Abidjan'),
(1561, 384, 'Sud-Comoé', 'Africa/Abidjan'),
(1562, 384, 'Vallée du Bandama', 'Africa/Abidjan'),
(1563, 384, 'Worodougou', 'Africa/Abidjan'),
(1564, 384, 'Zanzan', 'Africa/Abidjan'),
(1565, 388, 'Clarendon', 'America/Jamaica'),
(1566, 388, 'Hanover Parish', 'America/Jamaica'),
(1567, 388, 'Manchester', 'America/Jamaica'),
(1568, 388, 'Portland', 'America/Jamaica'),
(1569, 388, 'Saint Andrew', 'America/Jamaica'),
(1570, 388, 'Saint Ann', 'America/Jamaica'),
(1571, 388, 'Saint Catherine', 'America/Jamaica'),
(1572, 388, 'St. Elizabeth', 'America/Jamaica'),
(1573, 388, 'Saint James', 'America/Jamaica'),
(1574, 388, 'Saint Mary', 'America/Jamaica'),
(1575, 388, 'Saint Thomas', 'America/Jamaica'),
(1576, 388, 'Trelawny', 'America/Jamaica'),
(1577, 388, 'Westmoreland', 'America/Jamaica'),
(1578, 388, 'Kingston', 'America/Jamaica'),
(1579, 392, 'Aichi', 'Asia/Tokyo'),
(1580, 392, 'Akita', 'Asia/Tokyo'),
(1581, 392, 'Aomori', 'Asia/Tokyo'),
(1582, 392, 'Chiba', 'Asia/Tokyo'),
(1583, 392, 'Ehime', 'Asia/Tokyo'),
(1584, 392, 'Fukui', 'Asia/Tokyo'),
(1585, 392, 'Fukuoka', 'Asia/Tokyo'),
(1586, 392, 'Fukushima', 'Asia/Tokyo'),
(1587, 392, 'Gifu', 'Asia/Tokyo'),
(1588, 392, 'Gumma', 'Asia/Tokyo'),
(1589, 392, 'Hiroshima', 'Asia/Tokyo'),
(1590, 392, 'Hokkaidō', 'Asia/Tokyo'),
(1591, 392, 'Hyōgo', 'Asia/Tokyo'),
(1592, 392, 'Ibaraki', 'Asia/Tokyo'),
(1593, 392, 'Ishikawa', 'Asia/Tokyo'),
(1594, 392, 'Iwate', 'Asia/Tokyo'),
(1595, 392, 'Kagawa', 'Asia/Tokyo'),
(1596, 392, 'Kagoshima', 'Asia/Tokyo'),
(1597, 392, 'Kanagawa', 'Asia/Tokyo'),
(1598, 392, 'Kōchi', 'Asia/Tokyo'),
(1599, 392, 'Kumamoto', 'Asia/Tokyo'),
(1600, 392, 'Kyōto', 'Asia/Tokyo'),
(1601, 392, 'Mie', 'Asia/Tokyo'),
(1602, 392, 'Miyagi', 'Asia/Tokyo'),
(1603, 392, 'Miyazaki', 'Asia/Tokyo'),
(1604, 392, 'Nagano', 'Asia/Tokyo'),
(1605, 392, 'Nagasaki', 'Asia/Tokyo'),
(1606, 392, 'Nara', 'Asia/Tokyo'),
(1607, 392, 'Niigata', 'Asia/Tokyo'),
(1608, 392, 'Ōita', 'Asia/Tokyo'),
(1609, 392, 'Okayama', 'Asia/Tokyo'),
(1610, 392, 'Ōsaka', 'Asia/Tokyo'),
(1611, 392, 'Saga', 'Asia/Tokyo'),
(1612, 392, 'Saitama', 'Asia/Tokyo'),
(1613, 392, 'Shiga', 'Asia/Tokyo'),
(1614, 392, 'Shimane', 'Asia/Tokyo'),
(1615, 392, 'Shizuoka', 'Asia/Tokyo'),
(1616, 392, 'Tochigi', 'Asia/Tokyo'),
(1617, 392, 'Tokushima', 'Asia/Tokyo'),
(1618, 392, 'Tōkyō', 'Asia/Tokyo'),
(1619, 392, 'Tottori', 'Asia/Tokyo'),
(1620, 392, 'Toyama', 'Asia/Tokyo'),
(1621, 392, 'Wakayama', 'Asia/Tokyo'),
(1622, 392, 'Yamagata', 'Asia/Tokyo'),
(1623, 392, 'Yamaguchi', 'Asia/Tokyo'),
(1624, 392, 'Yamanashi', 'Asia/Tokyo'),
(1625, 392, 'Okinawa', 'Asia/Tokyo'),
(1626, 398, 'Almaty', 'Asia/Almaty'),
(1627, 398, 'Almaty Qalasy', 'Asia/Almaty'),
(1628, 398, 'Aqmola', 'Asia/Almaty'),
(1629, 398, 'Aqtöbe', 'Asia/Aqtobe'),
(1630, 398, 'Astana Qalasy', 'Asia/Almaty'),
(1631, 398, 'Atyraū', 'Asia/Oral'),
(1632, 398, 'Batys Qazaqstan', 'Asia/Oral'),
(1633, 398, 'Bayqongyr Qalasy', 'Asia/Almaty'),
(1634, 398, 'Mangghystaū', 'Asia/Aqtau'),
(1635, 398, 'Ongtüstik Qazaqstan', 'Asia/Almaty'),
(1636, 398, 'Pavlodar', 'Asia/Almaty'),
(1637, 398, 'Qaraghandy', 'Asia/Almaty'),
(1638, 398, 'Qostanay', 'Asia/Qyzylorda'),
(1639, 398, 'Qyzylorda', 'Asia/Qyzylorda'),
(1640, 398, 'East Kazakhstan', 'Asia/Almaty'),
(1641, 398, 'Soltüstik Qazaqstan', 'Asia/Almaty'),
(1642, 398, 'Zhambyl', 'Asia/Almaty'),
(1643, 400, 'Balqa', 'Asia/Amman'),
(1644, 400, 'Ma’an', 'Asia/Amman'),
(1645, 400, 'Karak', 'Asia/Amman'),
(1646, 400, 'Al Mafraq', 'Asia/Amman'),
(1647, 400, 'Tafielah', 'Asia/Amman'),
(1648, 400, 'Az Zarqa', 'Asia/Amman'),
(1649, 400, 'Irbid', 'Asia/Amman'),
(1650, 400, 'Mafraq', 'Asia/Amman'),
(1651, 400, 'Amman', 'Asia/Amman'),
(1652, 400, 'Zarqa', 'Asia/Amman'),
(1653, 400, 'Irbid', 'Asia/Amman'),
(1654, 400, 'Ma’an', 'Asia/Amman'),
(1655, 400, 'Ajlun', 'Asia/Amman'),
(1656, 400, 'Aqaba', 'Asia/Amman'),
(1657, 400, 'Jerash', 'Asia/Amman'),
(1658, 400, 'Madaba', 'Asia/Amman'),
(1659, 404, 'Central', 'Africa/Nairobi'),
(1660, 404, 'Coast', 'Africa/Nairobi'),
(1661, 404, 'Eastern', 'Africa/Nairobi'),
(1662, 404, 'Nairobi Area', 'Africa/Nairobi'),
(1663, 404, 'North-Eastern', 'Africa/Nairobi'),
(1664, 404, 'Nyanza', 'Africa/Nairobi'),
(1665, 404, 'Rift Valley', 'Africa/Nairobi'),
(1666, 404, 'Western', 'Africa/Nairobi'),
(1667, 408, 'Chagang-do', 'Asia/Pyongyang'),
(1668, 408, 'Hamgyŏng-namdo', 'Asia/Pyongyang'),
(1669, 408, 'Hwanghae-namdo', 'Asia/Pyongyang'),
(1670, 408, 'Hwanghae-bukto', 'Asia/Pyongyang'),
(1671, 408, 'Kaesŏng-si', 'Asia/Pyongyang'),
(1672, 408, 'Gangwon', 'Asia/Pyongyang'),
(1673, 408, 'Pʼyŏngan-bukto', 'Asia/Pyongyang'),
(1674, 408, 'Pʼyŏngyang-si', 'Asia/Pyongyang'),
(1675, 408, 'Yanggang-do', 'Asia/Pyongyang'),
(1676, 408, 'Nampʼo-si', 'Asia/Pyongyang'),
(1677, 408, 'Pʼyŏngan-namdo', 'Asia/Pyongyang'),
(1678, 408, 'Hamgyŏng-bukto', 'Asia/Pyongyang'),
(1679, 408, 'Najin Sŏnbong-si', 'Asia/Pyongyang'),
(1680, 410, 'Jeju', 'Asia/Seoul'),
(1681, 410, 'North Jeolla', 'Asia/Seoul'),
(1682, 410, 'North Chungcheong', 'Asia/Seoul'),
(1683, 410, 'Gangwon', 'Asia/Seoul'),
(1684, 410, 'Busan', 'Asia/Seoul'),
(1685, 410, 'Seoul', 'Asia/Seoul'),
(1686, 410, 'Incheon', 'Asia/Seoul'),
(1687, 410, 'Gyeonggi', 'Asia/Seoul'),
(1688, 410, 'North Gyeongsang', 'Asia/Seoul'),
(1689, 410, 'Daegu', 'Asia/Seoul'),
(1690, 410, 'South Jeolla', 'Asia/Seoul'),
(1691, 410, 'South Chungcheong', 'Asia/Seoul'),
(1692, 410, 'Gwangju', 'Asia/Seoul'),
(1693, 410, 'Daejeon', 'Asia/Seoul'),
(1694, 410, 'South Gyeongsang', 'Asia/Seoul'),
(1695, 410, 'Ulsan', 'Asia/Seoul'),
(1696, 414, 'Muḩāfaz̧atalWafrah', 'Asia/Kuwait'),
(1697, 414, 'Al ‘Āşimah', 'Asia/Kuwait'),
(1698, 414, 'Al Aḩmadī', 'Asia/Kuwait'),
(1699, 414, 'Al Jahrāʼ', 'Asia/Kuwait'),
(1700, 414, 'Al Farwaniyah', 'Asia/Kuwait'),
(1701, 414, 'Ḩawallī', 'Asia/Kuwait'),
(1702, 417, 'Bishkek', 'Asia/Bishkek'),
(1703, 417, 'Chüy', 'Asia/Bishkek'),
(1704, 417, 'Jalal-Abad', 'Asia/Bishkek'),
(1705, 417, 'Naryn', 'Asia/Bishkek'),
(1706, 417, 'Talas', 'Asia/Bishkek'),
(1707, 417, 'Ysyk-Köl', 'Asia/Bishkek'),
(1708, 417, 'Osh', 'Asia/Bishkek'),
(1709, 417, 'Batken', 'Asia/Bishkek'),
(1710, 418, 'Attapu', 'Asia/Vientiane'),
(1711, 418, 'Champasak', 'Asia/Vientiane'),
(1712, 418, 'Houaphan', 'Asia/Vientiane'),
(1713, 418, 'Khammouan', 'Asia/Vientiane'),
(1714, 418, 'Louang Namtha', 'Asia/Vientiane'),
(1715, 418, 'Louangphrabang', 'Asia/Vientiane'),
(1716, 418, 'Oudômxai', 'Asia/Vientiane'),
(1717, 418, 'Phongsali', 'Asia/Vientiane'),
(1718, 418, 'Saravan', 'Asia/Vientiane'),
(1719, 418, 'Savannakhet', 'Asia/Vientiane'),
(1720, 418, 'Vientiane', 'Asia/Vientiane'),
(1721, 418, 'Xiagnabouli', 'Asia/Vientiane'),
(1722, 418, 'Xiangkhoang', 'Asia/Vientiane'),
(1723, 418, 'Khammouan', 'Asia/Vientiane'),
(1724, 418, 'Loungnamtha', 'Asia/Vientiane'),
(1725, 418, 'Louangphabang', 'Asia/Vientiane'),
(1726, 418, 'Phôngsali', 'Asia/Vientiane'),
(1727, 418, 'Salavan', 'Asia/Vientiane'),
(1728, 418, 'Savannahkhét', 'Asia/Vientiane'),
(1729, 418, 'Bokèo', 'Asia/Vientiane'),
(1730, 418, 'Bolikhamxai', 'Asia/Vientiane'),
(1731, 418, 'Viangchan', 'Asia/Vientiane'),
(1732, 418, 'Xékong', 'Asia/Vientiane'),
(1733, 418, 'Viangchan', 'Asia/Vientiane'),
(1734, 422, 'Béqaa', 'Asia/Beirut'),
(1735, 422, 'Liban-Nord', 'Asia/Beirut'),
(1736, 422, 'Beyrouth', 'Asia/Beirut'),
(1737, 422, 'Mont-Liban', 'Asia/Beirut'),
(1738, 422, 'Liban-Sud', 'Asia/Beirut'),
(1739, 422, 'Nabatîyé', 'Asia/Beirut'),
(1740, 422, 'Béqaa', 'Asia/Beirut'),
(1741, 422, 'Liban-Nord', 'Asia/Beirut'),
(1742, 422, 'Aakkâr', 'Asia/Beirut'),
(1743, 422, 'Baalbek-Hermel', 'Asia/Beirut'),
(1744, 426, 'Balzers Commune', 'Africa/Maseru'),
(1745, 426, 'Eschen Commune', 'Africa/Maseru'),
(1746, 426, 'Gamprin Commune', 'Africa/Maseru'),
(1747, 426, 'Mauren Commune', 'Africa/Maseru'),
(1748, 426, 'Planken Commune', 'Africa/Maseru'),
(1749, 426, 'Ruggell Commune', 'Africa/Maseru'),
(1750, 426, 'Berea District', 'Africa/Maseru'),
(1751, 426, 'Butha-Buthe District', 'Africa/Maseru'),
(1752, 426, 'Leribe District', 'Africa/Maseru'),
(1753, 426, 'Mafeteng', 'Africa/Maseru'),
(1754, 426, 'Maseru', 'Africa/Maseru'),
(1755, 426, 'Mohaleʼs Hoek', 'Africa/Maseru'),
(1756, 426, 'Mokhotlong', 'Africa/Maseru'),
(1757, 426, 'Qachaʼs Nek', 'Africa/Maseru'),
(1758, 426, 'Quthing District', 'Africa/Maseru'),
(1759, 426, 'Thaba-Tseka District', 'Africa/Maseru'),
(1760, 428, 'Dobeles Rajons', 'Europe/Riga'),
(1761, 428, 'Aizkraukles Rajons', 'Europe/Riga'),
(1762, 428, 'Alūksnes Rajons', 'Europe/Riga'),
(1763, 428, 'Balvu Rajons', 'Europe/Riga'),
(1764, 428, 'Bauskas Rajons', 'Europe/Riga'),
(1765, 428, 'Cēsu Rajons', 'Europe/Riga'),
(1766, 428, 'Daugavpils', 'Europe/Riga'),
(1767, 428, 'Daugavpils Rajons', 'Europe/Riga'),
(1768, 428, 'Dobeles Rajons', 'Europe/Riga'),
(1769, 428, 'Gulbenes Rajons', 'Europe/Riga'),
(1770, 428, 'Jēkabpils Rajons', 'Europe/Riga'),
(1771, 428, 'Jelgava', 'Europe/Riga'),
(1772, 428, 'Jelgavas Rajons', 'Europe/Riga'),
(1773, 428, 'Jūrmala', 'Europe/Riga'),
(1774, 428, 'Krāslavas Rajons', 'Europe/Riga'),
(1775, 428, 'Kuldīgas Rajons', 'Europe/Riga'),
(1776, 428, 'Liepāja', 'Europe/Riga'),
(1777, 428, 'Liepājas Rajons', 'Europe/Riga'),
(1778, 428, 'Limbažu Rajons', 'Europe/Riga'),
(1779, 428, 'Ludzas Rajons', 'Europe/Riga'),
(1780, 428, 'Madonas Rajons', 'Europe/Riga'),
(1781, 428, 'Ogres Rajons', 'Europe/Riga'),
(1782, 428, 'Preiļu Rajons', 'Europe/Riga'),
(1783, 428, 'Rēzekne', 'Europe/Riga'),
(1784, 428, 'Rēzeknes Rajons', 'Europe/Riga'),
(1785, 428, 'Rīga', 'Europe/Riga'),
(1786, 428, 'Rīgas Rajons', 'Europe/Riga'),
(1787, 428, 'Saldus Rajons', 'Europe/Riga'),
(1788, 428, 'Talsu Rajons', 'Europe/Riga'),
(1789, 428, 'Tukuma Rajons', 'Europe/Riga'),
(1790, 428, 'Valkas Rajons', 'Europe/Riga'),
(1791, 428, 'Valmieras Rajons', 'Europe/Riga'),
(1792, 428, 'Ventspils', 'Europe/Riga'),
(1793, 428, 'Ventspils Rajons', 'Europe/Riga'),
(1794, 430, 'Bong', 'Africa/Monrovia'),
(1795, 430, 'Grand Jide', 'Africa/Monrovia'),
(1796, 430, 'Grand Cape Mount', 'Africa/Monrovia'),
(1797, 430, 'Lofa', 'Africa/Monrovia'),
(1798, 430, 'Nimba', 'Africa/Monrovia'),
(1799, 430, 'Sinoe', 'Africa/Monrovia'),
(1800, 430, 'Grand Bassa County', 'Africa/Monrovia'),
(1801, 430, 'Grand Cape Mount', 'Africa/Monrovia'),
(1802, 430, 'Maryland', 'Africa/Monrovia'),
(1803, 430, 'Montserrado', 'Africa/Monrovia'),
(1804, 430, 'Bomi', 'Africa/Monrovia'),
(1805, 430, 'Grand Kru', 'Africa/Monrovia'),
(1806, 430, 'Margibi', 'Africa/Monrovia'),
(1807, 430, 'River Cess', 'Africa/Monrovia'),
(1808, 430, 'Grand Gedeh', 'Africa/Monrovia'),
(1809, 430, 'Lofa', 'Africa/Monrovia'),
(1810, 430, 'Gbarpolu', 'Africa/Monrovia'),
(1811, 430, 'River Gee', 'Africa/Monrovia'),
(1812, 434, 'Al Abyār', 'Africa/Tripoli'),
(1813, 434, 'Al ‘Azīzīyah', 'Africa/Tripoli'),
(1814, 434, 'Al Bayḑā’', 'Africa/Tripoli'),
(1815, 434, 'Al Jufrah', 'Africa/Tripoli'),
(1816, 434, 'Al Jumayl', 'Africa/Tripoli'),
(1817, 434, 'Al Kufrah', 'Africa/Tripoli'),
(1818, 434, 'Al Marj', 'Africa/Tripoli'),
(1819, 434, 'Al Qarābūll', 'Africa/Tripoli'),
(1820, 434, 'Al Qubbah', 'Africa/Tripoli'),
(1821, 434, 'Al Ajaylāt', 'Africa/Tripoli'),
(1822, 434, 'Ash Shāţiʼ', 'Africa/Tripoli'),
(1823, 434, 'Az Zahra’', 'Africa/Tripoli'),
(1824, 434, 'Banī Walīd', 'Africa/Tripoli'),
(1825, 434, 'Bin Jawwād', 'Africa/Tripoli'),
(1826, 434, 'Ghāt', 'Africa/Tripoli'),
(1827, 434, 'Jādū', 'Africa/Tripoli'),
(1828, 434, 'Jālū', 'Africa/Tripoli'),
(1829, 434, 'Janzūr', 'Africa/Tripoli'),
(1830, 434, 'Masallatah', 'Africa/Tripoli'),
(1831, 434, 'Mizdah', 'Africa/Tripoli'),
(1832, 434, 'Murzuq', 'Africa/Tripoli'),
(1833, 434, 'Nālūt', 'Africa/Tripoli'),
(1834, 434, 'Qamīnis', 'Africa/Tripoli'),
(1835, 434, 'Qaşr Bin Ghashīr', 'Africa/Tripoli'),
(1836, 434, 'Sabhā', 'Africa/Tripoli'),
(1837, 434, 'Şabrātah', 'Africa/Tripoli'),
(1838, 434, 'Shaḩḩāt', 'Africa/Tripoli'),
(1839, 434, 'Şurmān', 'Africa/Tripoli'),
(1840, 434, 'Tajura’ ', 'Africa/Tripoli'),
(1841, 434, 'Tarhūnah', 'Africa/Tripoli'),
(1842, 434, 'Ţubruq', 'Africa/Tripoli'),
(1843, 434, 'Tūkrah', 'Africa/Tripoli'),
(1844, 434, 'Zlīţan', 'Africa/Tripoli'),
(1845, 434, 'Zuwārah', 'Africa/Tripoli'),
(1846, 434, 'Ajdābiyā', 'Africa/Tripoli'),
(1847, 434, 'Al Fātiḩ', 'Africa/Tripoli'),
(1848, 434, 'Al Jabal al Akhḑar', 'Africa/Tripoli'),
(1849, 434, 'Al Khums', 'Africa/Tripoli'),
(1850, 434, 'An Nuqāţ al Khams', 'Africa/Tripoli'),
(1851, 434, 'Awbārī', 'Africa/Tripoli'),
(1852, 434, 'Az Zāwiyah', 'Africa/Tripoli'),
(1853, 434, 'Banghāzī', 'Africa/Tripoli'),
(1854, 434, 'Darnah', 'Africa/Tripoli'),
(1855, 434, 'Ghadāmis', 'Africa/Tripoli'),
(1856, 434, 'Gharyān', 'Africa/Tripoli'),
(1857, 434, 'Mişrātah', 'Africa/Tripoli'),
(1858, 434, 'Sawfajjīn', 'Africa/Tripoli'),
(1859, 434, 'Surt', 'Africa/Tripoli'),
(1860, 434, 'Ţarābulus', 'Africa/Tripoli'),
(1861, 434, 'Yafran', 'Africa/Tripoli'),
(1862, 438, 'Balzers', 'Europe/Vaduz'),
(1863, 438, 'Eschen', 'Europe/Vaduz'),
(1864, 438, 'Gamprin', 'Europe/Vaduz'),
(1865, 438, 'Mauren', 'Europe/Vaduz'),
(1866, 438, 'Planken', 'Europe/Vaduz'),
(1867, 438, 'Ruggell', 'Europe/Vaduz'),
(1868, 438, 'Schaan', 'Europe/Vaduz'),
(1869, 438, 'Schellenberg', 'Europe/Vaduz'),
(1870, 438, 'Triesen', 'Europe/Vaduz'),
(1871, 438, 'Triesenberg', 'Europe/Vaduz'),
(1872, 438, 'Vaduz', 'Europe/Vaduz'),
(1873, 440, 'Alytaus Apskritis', 'Europe/Vilnius'),
(1874, 440, 'Kauno Apskritis', 'Europe/Vilnius'),
(1875, 440, 'Klaipėdos Apskritis', 'Europe/Vilnius'),
(1876, 440, 'Marijampolės Apskritis', 'Europe/Vilnius'),
(1877, 440, 'Panevėžio Apskritis', 'Europe/Vilnius'),
(1878, 440, 'Šiaulių Apskritis', 'Europe/Vilnius'),
(1879, 440, 'Tauragės Apskritis', 'Europe/Vilnius'),
(1880, 440, 'Telšių Apskritis', 'Europe/Vilnius'),
(1881, 440, 'Utenos Apskritis', 'Europe/Vilnius'),
(1882, 440, 'Vilniaus Apskritis', 'Europe/Vilnius'),
(1883, 442, 'Diekirch', 'Europe/Luxembourg'),
(1884, 442, 'Grevenmacher', 'Europe/Luxembourg'),
(1885, 442, 'Luxembourg', 'Europe/Luxembourg'),
(1886, 446, 'Ilhas', 'Asia/Macau'),
(1887, 446, 'Macau', 'Asia/Macau'),
(1888, 450, 'Antsiranana', 'Indian/Antananarivo'),
(1889, 450, 'Fianarantsoa', 'Indian/Antananarivo'),
(1890, 450, 'Mahajanga', 'Indian/Antananarivo'),
(1891, 450, 'Toamasina', 'Indian/Antananarivo'),
(1892, 450, 'Antananarivo', 'Indian/Antananarivo'),
(1893, 450, 'Toliara', 'Indian/Antananarivo'),
(1894, 454, 'Chikwawa', 'Africa/Blantyre'),
(1895, 454, 'Chiradzulu', 'Africa/Blantyre'),
(1896, 454, 'Chitipa', 'Africa/Blantyre'),
(1897, 454, 'Thyolo', 'Africa/Blantyre'),
(1898, 454, 'Dedza', 'Africa/Blantyre'),
(1899, 454, 'Dowa', 'Africa/Blantyre'),
(1900, 454, 'Karonga', 'Africa/Blantyre'),
(1901, 454, 'Kasungu', 'Africa/Blantyre'),
(1902, 454, 'Lilongwe', 'Africa/Blantyre'),
(1903, 454, 'Mangochi', 'Africa/Blantyre'),
(1904, 454, 'Mchinji', 'Africa/Blantyre'),
(1905, 454, 'Mzimba', 'Africa/Blantyre'),
(1906, 454, 'Ntcheu', 'Africa/Blantyre'),
(1907, 454, 'Nkhata Bay', 'Africa/Blantyre'),
(1908, 454, 'Nkhotakota', 'Africa/Blantyre'),
(1909, 454, 'Nsanje', 'Africa/Blantyre'),
(1910, 454, 'Ntchisi', 'Africa/Blantyre'),
(1911, 454, 'Rumphi', 'Africa/Blantyre'),
(1912, 454, 'Salima', 'Africa/Blantyre'),
(1913, 454, 'Zomba', 'Africa/Blantyre'),
(1914, 454, 'Blantyre', 'Africa/Blantyre'),
(1915, 454, 'Mwanza', 'Africa/Blantyre'),
(1916, 454, 'Balaka', 'Africa/Blantyre'),
(1917, 454, 'Likoma', 'Africa/Blantyre'),
(1918, 454, 'Machinga', 'Africa/Blantyre'),
(1919, 454, 'Mulanje', 'Africa/Blantyre'),
(1920, 454, 'Phalombe', 'Africa/Blantyre'),
(1921, 458, 'Johor', 'Asia/Kuala_Lumpur'),
(1922, 458, 'Kedah', 'Asia/Kuala_Lumpur'),
(1923, 458, 'Kelantan', 'Asia/Kuala_Lumpur'),
(1924, 458, 'Melaka', 'Asia/Kuala_Lumpur'),
(1925, 458, 'Negeri Sembilan', 'Asia/Kuala_Lumpur'),
(1926, 458, 'Pahang', 'Asia/Kuala_Lumpur'),
(1927, 458, 'Perak', 'Asia/Kuala_Lumpur'),
(1928, 458, 'Perlis', 'Asia/Kuala_Lumpur'),
(1929, 458, 'Pulau Pinang', 'Asia/Kuala_Lumpur'),
(1930, 458, 'Sarawak', 'Asia/Kuching'),
(1931, 458, 'Selangor', 'Asia/Kuala_Lumpur'),
(1932, 458, 'Terengganu', 'Asia/Kuala_Lumpur'),
(1933, 458, 'Kuala Lumpur', 'Asia/Kuala_Lumpur'),
(1934, 458, 'Federal Territory of Labuan', 'Asia/Kuala_Lumpur'),
(1935, 458, 'Sabah', 'Asia/Kuching'),
(1936, 458, 'Putrajaya', 'Asia/Kuala_Lumpur'),
(1937, 462, 'Maale', 'Indian/Maldives'),
(1938, 462, 'Seenu', 'Indian/Maldives'),
(1939, 462, 'Alifu Atholhu', 'Indian/Maldives'),
(1940, 462, 'Lhaviyani Atholhu', 'Indian/Maldives'),
(1941, 462, 'Vaavu Atholhu', 'Indian/Maldives'),
(1942, 462, 'Laamu', 'Indian/Maldives'),
(1943, 462, 'Haa Alifu Atholhu', 'Indian/Maldives'),
(1944, 462, 'Thaa Atholhu', 'Indian/Maldives'),
(1945, 462, 'Meemu Atholhu', 'Indian/Maldives'),
(1946, 462, 'Raa Atholhu', 'Indian/Maldives'),
(1947, 462, 'Faafu Atholhu', 'Indian/Maldives'),
(1948, 462, 'Dhaalu Atholhu', 'Indian/Maldives'),
(1949, 462, 'Baa Atholhu', 'Indian/Maldives'),
(1950, 462, 'Haa Dhaalu Atholhu', 'Indian/Maldives'),
(1951, 462, 'Shaviyani Atholhu', 'Indian/Maldives'),
(1952, 462, 'Noonu Atholhu', 'Indian/Maldives'),
(1953, 462, 'Kaafu Atholhu', 'Indian/Maldives'),
(1954, 462, 'Gaafu Alifu Atholhu', 'Indian/Maldives'),
(1955, 462, 'Gaafu Dhaalu Atholhu', 'Indian/Maldives'),
(1956, 462, 'Gnyaviyani Atoll', 'Indian/Maldives'),
(1957, 462, 'Alifu', 'Indian/Maldives'),
(1958, 462, 'Baa', 'Indian/Maldives'),
(1959, 462, 'Dhaalu', 'Indian/Maldives'),
(1960, 462, 'Faafu', 'Indian/Maldives'),
(1961, 462, 'Gaafu Alifu', 'Indian/Maldives'),
(1962, 462, 'Gaafu Dhaalu', 'Indian/Maldives'),
(1963, 462, 'Haa Alifu', 'Indian/Maldives'),
(1964, 462, 'Haa Dhaalu', 'Indian/Maldives'),
(1965, 462, 'Kaafu', 'Indian/Maldives'),
(1966, 462, 'Lhaviyani', 'Indian/Maldives'),
(1967, 462, 'Maale', 'Indian/Maldives'),
(1968, 462, 'Meemu', 'Indian/Maldives'),
(1969, 462, 'Gnaviyani', 'Indian/Maldives'),
(1970, 462, 'Noonu', 'Indian/Maldives'),
(1971, 462, 'Raa', 'Indian/Maldives'),
(1972, 462, 'Shaviyani', 'Indian/Maldives'),
(1973, 462, 'Thaa', 'Indian/Maldives'),
(1974, 462, 'Vaavu', 'Indian/Maldives'),
(1975, 466, 'Bamako', 'Africa/Bamako'),
(1976, 466, 'Gao', 'Africa/Bamako'),
(1977, 466, 'Kayes', 'Africa/Bamako'),
(1978, 466, 'Mopti', 'Africa/Bamako'),
(1979, 466, 'Ségou', 'Africa/Bamako'),
(1980, 466, 'Sikasso', 'Africa/Bamako'),
(1981, 466, 'Koulikoro', 'Africa/Bamako'),
(1982, 466, 'Tombouctou', 'Africa/Bamako'),
(1983, 466, 'Gao', 'Africa/Bamako'),
(1984, 466, 'Kidal', 'Africa/Bamako'),
(1985, 470, 'Malta', 'Europe/Malta'),
(1986, 474, 'Martinique', 'America/Martinique'),
(1987, 478, 'Nouakchott', 'Africa/Nouakchott'),
(1988, 478, 'Hodh Ech Chargui', 'Africa/Nouakchott'),
(1989, 478, 'Hodh El Gharbi', 'Africa/Nouakchott'),
(1990, 478, 'Assaba', 'Africa/Nouakchott'),
(1991, 478, 'Gorgol', 'Africa/Nouakchott'),
(1992, 478, 'Brakna', 'Africa/Nouakchott'),
(1993, 478, 'Trarza', 'Africa/Nouakchott'),
(1994, 478, 'Adrar', 'Africa/Nouakchott'),
(1995, 478, 'Dakhlet Nouadhibou', 'Africa/Nouakchott'),
(1996, 478, 'Tagant', 'Africa/Nouakchott'),
(1997, 478, 'Guidimaka', 'Africa/Nouakchott'),
(1998, 478, 'Tiris Zemmour', 'Africa/Nouakchott'),
(1999, 478, 'Inchiri', 'Africa/Nouakchott'),
(2000, 480, 'Black River', 'Indian/Mauritius'),
(2001, 480, 'Flacq', 'Indian/Mauritius'),
(2002, 480, 'Grand Port', 'Indian/Mauritius'),
(2003, 480, 'Moka', 'Indian/Mauritius'),
(2004, 480, 'Pamplemousses', 'Indian/Mauritius'),
(2005, 480, 'Plaines Wilhems', 'Indian/Mauritius'),
(2006, 480, 'Port Louis', 'Indian/Mauritius'),
(2007, 480, 'Rivière du Rempart', 'Indian/Mauritius'),
(2008, 480, 'Savanne', 'Indian/Mauritius'),
(2009, 480, 'Agalega Islands', 'Indian/Mauritius'),
(2010, 480, 'Cargados Carajos', 'Indian/Mauritius'),
(2011, 480, 'Rodrigues', 'Indian/Mauritius'),
(2012, 484, 'Aguascalientes', 'America/Mexico_City'),
(2013, 484, 'Baja California', 'America/Tijuana'),
(2014, 484, 'Baja California Sur', 'America/Mazatlan'),
(2015, 484, 'Campeche', 'America/Merida'),
(2016, 484, 'Chiapas', 'America/Mexico_City'),
(2017, 484, 'Chihuahua', 'America/Chihuahua'),
(2018, 484, 'Coahuila', 'America/Monterrey'),
(2019, 484, 'Colima', 'America/Mexico_City'),
(2020, 484, 'The Federal District', 'America/Mexico_City'),
(2021, 484, 'Durango', 'America/Monterrey'),
(2022, 484, 'Guanajuato', 'America/Mexico_City'),
(2023, 484, 'Guerrero', 'America/Mexico_City'),
(2024, 484, 'Hidalgo', 'America/Mexico_City'),
(2025, 484, 'Jalisco', 'America/Mexico_City'),
(2026, 484, 'México', 'America/Mexico_City'),
(2027, 484, 'Michoacán', 'America/Mexico_City'),
(2028, 484, 'Morelos', 'America/Mexico_City'),
(2029, 484, 'Nayarit', 'America/Mazatlan'),
(2030, 484, 'Nuevo León', 'America/Monterrey'),
(2031, 484, 'Oaxaca', 'America/Mexico_City'),
(2032, 484, 'Puebla', 'America/Mexico_City'),
(2033, 484, 'Querétaro', 'America/Mexico_City'),
(2034, 484, 'Quintana Roo', 'America/Cancun'),
(2035, 484, 'San Luis Potosí', 'America/Mexico_City'),
(2036, 484, 'Sinaloa', 'America/Mazatlan'),
(2037, 484, 'Sonora', 'America/Hermosillo'),
(2038, 484, 'Tabasco', 'America/Mexico_City'),
(2039, 484, 'Tamaulipas', 'America/Monterrey'),
(2040, 484, 'Tlaxcala', 'America/Mexico_City'),
(2041, 484, 'Veracruz-Llave', 'America/Mexico_City'),
(2042, 484, 'Yucatán', 'America/Merida'),
(2043, 484, 'Zacatecas', 'America/Mexico_City'),
(2044, 492, 'Monaco', 'Europe/Monaco'),
(2045, 496, 'Arhangay', 'Asia/Ulaanbaatar'),
(2046, 496, 'Bayanhongor', 'Asia/Ulaanbaatar'),
(2047, 496, 'Bayan-Ölgiy', 'Asia/Hovd'),
(2048, 496, 'East Aimak', 'Asia/Ulaanbaatar'),
(2049, 496, 'East Gobi Aymag', 'Asia/Ulaanbaatar'),
(2050, 496, 'Middle Govĭ', 'Asia/Ulaanbaatar'),
(2051, 496, 'Dzavhan', 'Asia/Hovd'),
(2052, 496, 'Govĭ-Altay', 'Asia/Hovd'),
(2053, 496, 'Hentiy', 'Asia/Ulaanbaatar'),
(2054, 496, 'Hovd', 'Asia/Hovd'),
(2055, 496, 'Hövsgöl', 'Asia/Ulaanbaatar'),
(2056, 496, 'South Gobi Aimak', 'Asia/Ulaanbaatar'),
(2057, 496, 'South Hangay', 'Asia/Ulaanbaatar'),
(2058, 496, 'Selenge', 'Asia/Ulaanbaatar'),
(2059, 496, 'Sühbaatar', 'Asia/Choibalsan'),
(2060, 496, 'Central Aimak', 'Asia/Ulaanbaatar'),
(2061, 496, 'Uvs', 'Asia/Hovd'),
(2062, 496, 'Ulaanbaatar', 'Asia/Ulaanbaatar'),
(2063, 496, 'Bulgan', 'Asia/Ulaanbaatar'),
(2064, 496, 'Darhan Uul', 'Asia/Ulaanbaatar'),
(2065, 496, 'Govĭ-Sumber', 'Asia/Ulaanbaatar'),
(2066, 496, 'Orhon', 'Asia/Ulaanbaatar'),
(2067, 498, 'Ungheni Judetul', 'Europe/Chisinau'),
(2068, 498, 'Balti', 'Europe/Chisinau'),
(2069, 498, 'Cahul', 'Europe/Chisinau'),
(2070, 498, 'Stinga Nistrului', 'Europe/Chisinau'),
(2071, 498, 'Edinet', 'Europe/Chisinau'),
(2072, 498, 'Găgăuzia', 'Europe/Chisinau'),
(2073, 498, 'Lapusna', 'Europe/Chisinau'),
(2074, 498, 'Orhei', 'Europe/Chisinau'),
(2075, 498, 'Soroca', 'Europe/Chisinau'),
(2076, 498, 'Tighina', 'Europe/Chisinau'),
(2077, 498, 'Ungheni', 'Europe/Chisinau'),
(2078, 498, 'Chişinău', 'Europe/Chisinau'),
(2079, 498, 'Stînga Nistrului', 'Europe/Chisinau'),
(2080, 498, 'Raionul Anenii Noi', 'Europe/Chisinau'),
(2081, 498, 'Bălţi', 'Europe/Chisinau'),
(2082, 498, 'Raionul Basarabeasca', 'Europe/Chisinau'),
(2083, 498, 'Bender', 'Europe/Chisinau'),
(2084, 498, 'Raionul Briceni', 'Europe/Chisinau'),
(2085, 498, 'Raionul Cahul', 'Europe/Chisinau'),
(2086, 498, 'Raionul Cantemir', 'Europe/Chisinau'),
(2087, 498, 'Călăraşi', 'Europe/Chisinau'),
(2088, 498, 'Căuşeni', 'Europe/Chisinau'),
(2089, 498, 'Raionul Cimişlia', 'Europe/Chisinau'),
(2090, 498, 'Raionul Criuleni', 'Europe/Chisinau'),
(2091, 498, 'Raionul Donduşeni', 'Europe/Chisinau'),
(2092, 498, 'Raionul Drochia', 'Europe/Chisinau'),
(2093, 498, 'Dubăsari', 'Europe/Chisinau'),
(2094, 498, 'Raionul Edineţ', 'Europe/Chisinau'),
(2095, 498, 'Raionul Făleşti', 'Europe/Chisinau'),
(2096, 498, 'Raionul Floreşti', 'Europe/Chisinau'),
(2097, 498, 'Raionul Glodeni', 'Europe/Chisinau'),
(2098, 498, 'Raionul Hînceşti', 'Europe/Chisinau'),
(2099, 498, 'Raionul Ialoveni', 'Europe/Chisinau'),
(2100, 498, 'Raionul Leova', 'Europe/Chisinau'),
(2101, 498, 'Raionul Nisporeni', 'Europe/Chisinau'),
(2102, 498, 'Raionul Ocniţa', 'Europe/Chisinau'),
(2103, 498, 'Raionul Orhei', 'Europe/Chisinau'),
(2104, 498, 'Raionul Rezina', 'Europe/Chisinau'),
(2105, 498, 'Raionul Rîşcani', 'Europe/Chisinau'),
(2106, 498, 'Raionul Sîngerei', 'Europe/Chisinau'),
(2107, 498, 'Raionul Şoldăneşti', 'Europe/Chisinau'),
(2108, 498, 'Raionul Soroca', 'Europe/Chisinau'),
(2109, 498, 'Ştefan-Vodă', 'Europe/Chisinau'),
(2110, 498, 'Raionul Străşeni', 'Europe/Chisinau'),
(2111, 498, 'Raionul Taraclia', 'Europe/Chisinau'),
(2112, 498, 'Raionul Teleneşti', 'Europe/Chisinau'),
(2113, 498, 'Raionul Ungheni', 'Europe/Chisinau'),
(2114, 499, 'Opština Andrijevica', 'Europe/Podgorica'),
(2115, 499, 'Opština Bar', 'Europe/Podgorica'),
(2116, 499, 'Opština Berane', 'Europe/Podgorica'),
(2117, 499, 'Opština Bijelo Polje', 'Europe/Podgorica'),
(2118, 499, 'Opština Budva', 'Europe/Podgorica'),
(2119, 499, 'Opština Cetinje', 'Europe/Podgorica'),
(2120, 499, 'Opština Danilovgrad', 'Europe/Podgorica'),
(2121, 499, 'Opština Herceg Novi', 'Europe/Podgorica'),
(2122, 499, 'Opština Kolašin', 'Europe/Podgorica'),
(2123, 499, 'Opština Kotor', 'Europe/Podgorica'),
(2124, 499, 'Opština Mojkovac', 'Europe/Podgorica'),
(2125, 499, 'Opština Nikšić', 'Europe/Podgorica'),
(2126, 499, 'Opština Plav', 'Europe/Podgorica'),
(2127, 499, 'Opština Pljevlja', 'Europe/Podgorica'),
(2128, 499, 'Opština Plužine', 'Europe/Podgorica'),
(2129, 499, 'Opština Podgorica', 'Europe/Podgorica'),
(2130, 499, 'Opština Rožaje', 'Europe/Podgorica'),
(2131, 499, 'Opština Šavnik', 'Europe/Podgorica'),
(2132, 499, 'Opština Tivat', 'Europe/Podgorica'),
(2133, 499, 'Opština Ulcinj', 'Europe/Podgorica'),
(2134, 499, 'Opština Žabljak', 'Europe/Podgorica'),
(2135, 500, 'Saint Anthony', 'America/Montserrat'),
(2136, 500, 'Saint Georges', 'America/Montserrat'),
(2137, 500, 'Saint Peter', 'America/Montserrat'),
(2138, 504, 'Agadir', 'Africa/Casablanca'),
(2139, 504, 'Al Hoceïma', 'Africa/Casablanca'),
(2140, 504, 'Azizal', 'Africa/Casablanca'),
(2141, 504, 'Ben Slimane', 'Africa/Casablanca'),
(2142, 504, 'Beni Mellal', 'Africa/Casablanca'),
(2143, 504, 'Boulemane', 'Africa/Casablanca'),
(2144, 504, 'Casablanca', 'Africa/Casablanca'),
(2145, 504, 'Chaouen', 'Africa/Casablanca'),
(2146, 504, 'El Jadida', 'Africa/Casablanca'),
(2147, 504, 'El Kelaa des Srarhna', 'Africa/Casablanca'),
(2148, 504, 'Er Rachidia', 'Africa/Casablanca'),
(2149, 504, 'Essaouira', 'Africa/Casablanca'),
(2150, 504, 'Fes', 'Africa/Casablanca'),
(2151, 504, 'Figuig', 'Africa/Casablanca'),
(2152, 504, 'Kenitra', 'Africa/Casablanca'),
(2153, 504, 'Khemisset', 'Africa/Casablanca'),
(2154, 504, 'Khenifra', 'Africa/Casablanca'),
(2155, 504, 'Khouribga', 'Africa/Casablanca'),
(2156, 504, 'Marrakech', 'Africa/Casablanca'),
(2157, 504, 'Meknes', 'Africa/Casablanca'),
(2158, 504, 'Nador', 'Africa/Casablanca'),
(2159, 504, 'Ouarzazate', 'Africa/Casablanca'),
(2160, 504, 'Oujda', 'Africa/Casablanca'),
(2161, 504, 'Rabat-Sale', 'Africa/Casablanca'),
(2162, 504, 'Safi', 'Africa/Casablanca'),
(2163, 504, 'Settat', 'Africa/Casablanca'),
(2164, 504, 'Tanger', 'Africa/Casablanca'),
(2165, 504, 'Tata', 'Africa/Casablanca'),
(2166, 504, 'Taza', 'Africa/Casablanca'),
(2167, 504, 'Tiznit', 'Africa/Casablanca'),
(2168, 504, 'Guelmim', 'Africa/Casablanca'),
(2169, 504, 'Ifrane', 'Africa/Casablanca'),
(2170, 504, 'Laayoune', 'Africa/Casablanca'),
(2171, 504, 'Tan-Tan', 'Africa/Casablanca'),
(2172, 504, 'Taounate', 'Africa/Casablanca'),
(2173, 504, 'Sidi Kacem', 'Africa/Casablanca'),
(2174, 504, 'Taroudannt', 'Africa/Casablanca'),
(2175, 504, 'Tetouan', 'Africa/Casablanca'),
(2176, 504, 'Larache', 'Africa/Casablanca'),
(2177, 504, 'Grand Casablanca', 'Africa/Casablanca'),
(2178, 504, 'Fès-Boulemane', 'Africa/Casablanca'),
(2179, 504, 'Marrakech-Tensift-Al Haouz', 'Africa/Casablanca'),
(2180, 504, 'Meknès-Tafilalet', 'Africa/Casablanca'),
(2181, 504, 'Rabat-Salé-Zemmour-Zaër', 'Africa/Casablanca'),
(2182, 504, 'Chaouia-Ouardigha', 'Africa/Casablanca'),
(2183, 504, 'Doukkala-Abda', 'Africa/Casablanca'),
(2184, 504, 'Gharb-Chrarda-Beni Hssen', 'Africa/Casablanca'),
(2185, 504, 'Guelmim-Es Smara', 'Africa/Casablanca'),
(2186, 504, 'Oriental', 'Africa/Casablanca'),
(2187, 504, 'Souss-Massa-Drâa', 'Africa/Casablanca'),
(2188, 504, 'Tadla-Azilal', 'Africa/Casablanca'),
(2189, 504, 'Tanger-Tétouan', 'Africa/Casablanca'),
(2190, 504, 'Taza-Al Hoceima-Taounate', 'Africa/Casablanca'),
(2191, 504, 'Laâyoune-Boujdour-Sakia El Hamra', 'Africa/Casablanca'),
(2192, 508, 'Cabo Delgado', 'Africa/Maputo'),
(2193, 508, 'Gaza', 'Africa/Maputo'),
(2194, 508, 'Inhambane', 'Africa/Maputo'),
(2195, 508, 'Maputo Province', 'Africa/Maputo'),
(2196, 508, 'Sofala', 'Africa/Maputo'),
(2197, 508, 'Nampula', 'Africa/Maputo'),
(2198, 508, 'Niassa', 'Africa/Maputo'),
(2199, 508, 'Tete', 'Africa/Maputo'),
(2200, 508, 'Zambézia', 'Africa/Maputo'),
(2201, 508, 'Manica', 'Africa/Maputo'),
(2202, 508, 'Maputo', 'Africa/Maputo'),
(2203, 512, 'Ad Dākhilīyah', 'Asia/Muscat'),
(2204, 512, 'Al Bāţinah', 'Asia/Muscat'),
(2205, 512, 'Al Wusţá', 'Asia/Muscat'),
(2206, 512, 'Ash Sharqīyah', 'Asia/Muscat'),
(2207, 512, 'Masqaţ', 'Asia/Muscat'),
(2208, 512, 'Musandam', 'Asia/Muscat'),
(2209, 512, 'Z̧ufār', 'Asia/Muscat'),
(2210, 512, 'Az̧ Z̧āhirah', 'Asia/Muscat'),
(2211, 512, 'Muḩāfaz̧at al Buraymī', 'Asia/Muscat'),
(2212, 516, 'Bethanien', 'Africa/Windhoek'),
(2213, 516, 'Caprivi Oos', 'Africa/Windhoek'),
(2214, 516, 'Kaokoland', 'Africa/Windhoek'),
(2215, 516, 'Otjiwarongo', 'Africa/Windhoek'),
(2216, 516, 'Outjo', 'Africa/Windhoek'),
(2217, 516, 'Owambo', 'Africa/Windhoek'),
(2218, 516, 'Khomas', 'Africa/Windhoek'),
(2219, 516, 'Kavango', 'Africa/Windhoek'),
(2220, 516, 'Caprivi', 'Africa/Windhoek'),
(2221, 516, 'Erongo', 'Africa/Windhoek'),
(2222, 516, 'Hardap', 'Africa/Windhoek'),
(2223, 516, 'Karas', 'Africa/Windhoek'),
(2224, 516, 'Kunene', 'Africa/Windhoek'),
(2225, 516, 'Ohangwena', 'Africa/Windhoek'),
(2226, 516, 'Okavango', 'Africa/Windhoek'),
(2227, 516, 'Omaheke', 'Africa/Windhoek'),
(2228, 516, 'Omusati', 'Africa/Windhoek'),
(2229, 516, 'Oshana', 'Africa/Windhoek'),
(2230, 516, 'Oshikoto', 'Africa/Windhoek'),
(2231, 516, 'Otjozondjupa', 'Africa/Windhoek'),
(2232, 520, 'Aiwo', 'Pacific/Nauru'),
(2233, 520, 'Anabar', 'Pacific/Nauru'),
(2234, 520, 'Anetan', 'Pacific/Nauru'),
(2235, 520, 'Anibare', 'Pacific/Nauru'),
(2236, 520, 'Baiti', 'Pacific/Nauru'),
(2237, 520, 'Boe', 'Pacific/Nauru'),
(2238, 520, 'Buada', 'Pacific/Nauru'),
(2239, 520, 'Denigomodu', 'Pacific/Nauru'),
(2240, 520, 'Ewa', 'Pacific/Nauru'),
(2241, 520, 'Ijuw', 'Pacific/Nauru'),
(2242, 520, 'Meneng', 'Pacific/Nauru'),
(2243, 520, 'Nibok', 'Pacific/Nauru'),
(2244, 520, 'Uaboe', 'Pacific/Nauru'),
(2245, 520, 'Yaren', 'Pacific/Nauru'),
(2246, 524, 'Bāgmatī', 'Asia/Kathmandu'),
(2247, 524, 'Bherī', 'Asia/Kathmandu'),
(2248, 524, 'Dhawalāgiri', 'Asia/Kathmandu'),
(2249, 524, 'Gandakī', 'Asia/Kathmandu'),
(2250, 524, 'Janakpur', 'Asia/Kathmandu'),
(2251, 524, 'Karnālī', 'Asia/Kathmandu'),
(2252, 524, 'Kosī', 'Asia/Kathmandu'),
(2253, 524, 'Lumbinī', 'Asia/Kathmandu'),
(2254, 524, 'Mahākālī', 'Asia/Kathmandu'),
(2255, 524, 'Mechī', 'Asia/Kathmandu'),
(2256, 524, 'Nārāyanī', 'Asia/Kathmandu'),
(2257, 524, 'Rāptī', 'Asia/Kathmandu'),
(2258, 524, 'Sagarmāthā', 'Asia/Kathmandu'),
(2259, 524, 'Setī', 'Asia/Kathmandu'),
(2260, 528, 'Provincie Drenthe', 'Europe/Amsterdam'),
(2261, 528, 'Provincie Friesland', 'Europe/Amsterdam'),
(2262, 528, 'Gelderland', 'Europe/Amsterdam'),
(2263, 528, 'Groningen', 'Europe/Amsterdam'),
(2264, 528, 'Limburg', 'Europe/Amsterdam'),
(2265, 528, 'North Brabant', 'Europe/Amsterdam'),
(2266, 528, 'North Holland', 'Europe/Amsterdam'),
(2267, 528, 'Utrecht', 'Europe/Amsterdam'),
(2268, 528, 'Zeeland', 'Europe/Amsterdam'),
(2269, 528, 'South Holland', 'Europe/Amsterdam'),
(2270, 528, 'Overijssel', 'Europe/Amsterdam'),
(2271, 528, 'Flevoland', 'Europe/Amsterdam'),
(2272, 530, 'Netherlands Antilles', 'America/Curacao'),
(2273, 533, 'Aruba', 'America/Aruba'),
(2274, 548, 'Ambrym', 'Pacific/Efate'),
(2275, 548, 'Aoba/Maéwo', 'Pacific/Efate'),
(2276, 548, 'Torba', 'Pacific/Efate'),
(2277, 548, 'Éfaté', 'Pacific/Efate'),
(2278, 548, 'Épi', 'Pacific/Efate'),
(2279, 548, 'Malakula', 'Pacific/Efate'),
(2280, 548, 'Paama', 'Pacific/Efate'),
(2281, 548, 'Pentecôte', 'Pacific/Efate'),
(2282, 548, 'Sanma', 'Pacific/Efate'),
(2283, 548, 'Shepherd', 'Pacific/Efate'),
(2284, 548, 'Tafea', 'Pacific/Efate'),
(2285, 548, 'Malampa', 'Pacific/Efate'),
(2286, 548, 'Penama', 'Pacific/Efate'),
(2287, 548, 'Shefa', 'Pacific/Efate'),
(2288, 554, 'Tasman', 'Pacific/Auckland'),
(2289, 554, 'Auckland', 'Pacific/Auckland'),
(2290, 554, 'Bay of Plenty', 'Pacific/Auckland'),
(2291, 554, 'Canterbury', 'Pacific/Auckland'),
(2292, 554, 'Gisborne', 'Pacific/Auckland'),
(2293, 554, 'Hawkeʼs Bay', 'Pacific/Auckland'),
(2294, 554, 'Manawatu-Wanganui', 'Pacific/Auckland'),
(2295, 554, 'Marlborough', 'Pacific/Auckland'),
(2296, 554, 'Nelson', 'Pacific/Auckland'),
(2297, 554, 'Northland', 'Pacific/Auckland'),
(2298, 554, 'Otago', 'Pacific/Auckland'),
(2299, 554, 'Southland', 'Pacific/Auckland'),
(2300, 554, 'Taranaki', 'Pacific/Auckland'),
(2301, 554, 'Waikato', 'Pacific/Auckland'),
(2302, 554, 'Wellington', 'Pacific/Auckland'),
(2303, 554, 'West Coast', 'Pacific/Auckland'),
(2304, 558, 'Boaco', 'America/Managua'),
(2305, 558, 'Carazo', 'America/Managua'),
(2306, 558, 'Chinandega', 'America/Managua'),
(2307, 558, 'Chontales', 'America/Managua'),
(2308, 558, 'Estelí', 'America/Managua'),
(2309, 558, 'Granada', 'America/Managua'),
(2310, 558, 'Jinotega', 'America/Managua'),
(2311, 558, 'León', 'America/Managua'),
(2312, 558, 'Madriz', 'America/Managua'),
(2313, 558, 'Managua', 'America/Managua'),
(2314, 558, 'Masaya', 'America/Managua'),
(2315, 558, 'Matagalpa', 'America/Managua'),
(2316, 558, 'Nueva Segovia', 'America/Managua'),
(2317, 558, 'Río San Juan', 'America/Managua'),
(2318, 558, 'Rivas', 'America/Managua'),
(2319, 558, 'Ogun State', 'America/Managua'),
(2320, 558, 'Atlántico Norte', 'America/Managua'),
(2321, 558, 'Atlántico Sur', 'America/Managua'),
(2322, 562, 'Agadez', 'Africa/Niamey'),
(2323, 562, 'Diffa', 'Africa/Niamey'),
(2324, 562, 'Dosso', 'Africa/Niamey'),
(2325, 562, 'Maradi', 'Africa/Niamey');
INSERT INTO `country_state` (`id`, `country_id`, `name`, `timezone`) VALUES
(2326, 562, 'Tahoua', 'Africa/Niamey'),
(2327, 562, 'Zinder', 'Africa/Niamey'),
(2328, 562, 'Niamey', 'Africa/Niamey'),
(2329, 562, 'Tillabéri', 'Africa/Niamey'),
(2330, 566, 'Lagos', 'Africa/Lagos'),
(2331, 566, 'Abuja Federal Capital Territory', 'Africa/Lagos'),
(2332, 566, 'Ogun', 'Africa/Lagos'),
(2333, 566, 'Akwa Ibom', 'Africa/Lagos'),
(2334, 566, 'Cross River', 'Africa/Lagos'),
(2335, 566, 'Kaduna', 'Africa/Lagos'),
(2336, 566, 'Katsina', 'Africa/Lagos'),
(2337, 566, 'Anambra', 'Africa/Lagos'),
(2338, 566, 'Benue', 'Africa/Lagos'),
(2339, 566, 'Borno', 'Africa/Lagos'),
(2340, 566, 'Imo', 'Africa/Lagos'),
(2341, 566, 'Kano', 'Africa/Lagos'),
(2342, 566, 'Kwara', 'Africa/Lagos'),
(2343, 566, 'Niger', 'Africa/Lagos'),
(2344, 566, 'Oyo', 'Africa/Lagos'),
(2345, 566, 'Adamawa', 'Africa/Lagos'),
(2346, 566, 'Delta', 'Africa/Lagos'),
(2347, 566, 'Edo', 'Africa/Lagos'),
(2348, 566, 'Jigawa', 'Africa/Lagos'),
(2349, 566, 'Kebbi', 'Africa/Lagos'),
(2350, 566, 'Kogi', 'Africa/Lagos'),
(2351, 566, 'Osun', 'Africa/Lagos'),
(2352, 566, 'Taraba', 'Africa/Lagos'),
(2353, 566, 'Yobe', 'Africa/Lagos'),
(2354, 566, 'Abia', 'Africa/Lagos'),
(2355, 566, 'Bauchi', 'Africa/Lagos'),
(2356, 566, 'Enugu', 'Africa/Lagos'),
(2357, 566, 'Ondo', 'Africa/Lagos'),
(2358, 566, 'Plateau', 'Africa/Lagos'),
(2359, 566, 'Rivers', 'Africa/Lagos'),
(2360, 566, 'Sokoto', 'Africa/Lagos'),
(2361, 566, 'Bayelsa', 'Africa/Lagos'),
(2362, 566, 'Ebonyi', 'Africa/Lagos'),
(2363, 566, 'Ekiti', 'Africa/Lagos'),
(2364, 566, 'Gombe', 'Africa/Lagos'),
(2365, 566, 'Nassarawa', 'Africa/Lagos'),
(2366, 566, 'Zamfara', 'Africa/Lagos'),
(2367, 570, 'Niue', 'Pacific/Niue'),
(2368, 574, 'Norfolk Island', 'Pacific/Norfolk'),
(2369, 578, 'Svalbard', 'Europe/Oslo'),
(2370, 578, 'Akershus', 'Europe/Oslo'),
(2371, 578, 'Aust-Agder', 'Europe/Oslo'),
(2372, 578, 'Buskerud', 'Europe/Oslo'),
(2373, 578, 'Finnmark', 'Europe/Oslo'),
(2374, 578, 'Hedmark', 'Europe/Oslo'),
(2375, 578, 'Hordaland', 'Europe/Oslo'),
(2376, 578, 'Møre og Romsdal', 'Europe/Oslo'),
(2377, 578, 'Nordland', 'Europe/Oslo'),
(2378, 578, 'Nord-Trøndelag', 'Europe/Oslo'),
(2379, 578, 'Oppland', 'Europe/Oslo'),
(2380, 578, 'Oslo county', 'Europe/Oslo'),
(2381, 578, 'Østfold', 'Europe/Oslo'),
(2382, 578, 'Rogaland', 'Europe/Oslo'),
(2383, 578, 'Sogn og Fjordane', 'Europe/Oslo'),
(2384, 578, 'Sør-Trøndelag', 'Europe/Oslo'),
(2385, 578, 'Telemark', 'Europe/Oslo'),
(2386, 578, 'Troms', 'Europe/Oslo'),
(2387, 578, 'Vest-Agder', 'Europe/Oslo'),
(2388, 578, 'Vestfold', 'Europe/Oslo'),
(2389, 583, 'Kosrae', 'Pacific/Kosrae'),
(2390, 583, 'Pohnpei', 'Pacific/Ponape'),
(2391, 583, 'Chuuk', 'Pacific/Truk'),
(2392, 583, 'Yap', 'Pacific/Truk'),
(2393, 584, 'Marshall Islands', 'Pacific/Majuro'),
(2394, 585, 'State of Ngeremlengui', 'Pacific/Palau'),
(2395, 586, 'Federally Administered Tribal Areas', 'Asia/Karachi'),
(2396, 586, 'Balochistān', 'Asia/Karachi'),
(2397, 586, 'North West Frontier Province', 'Asia/Karachi'),
(2398, 586, 'Punjab', 'Asia/Karachi'),
(2399, 586, 'Sindh', 'Asia/Karachi'),
(2400, 586, 'Azad Kashmir', 'Asia/Karachi'),
(2401, 586, 'Gilgit-Baltistan', 'Asia/Karachi'),
(2402, 586, 'Islāmābād', 'Asia/Karachi'),
(2403, 591, 'Bocas del Toro', 'America/Panama'),
(2404, 591, 'Chiriquí', 'America/Panama'),
(2405, 591, 'Coclé', 'America/Panama'),
(2406, 591, 'Colón', 'America/Panama'),
(2407, 591, 'Darién', 'America/Panama'),
(2408, 591, 'Herrera', 'America/Panama'),
(2409, 591, 'Los Santos', 'America/Panama'),
(2410, 591, 'Panamá', 'America/Panama'),
(2411, 591, 'San Blas', 'America/Panama'),
(2412, 591, 'Veraguas', 'America/Panama'),
(2413, 598, 'Central', 'Pacific/Port_Moresby'),
(2414, 598, 'Gulf', 'Pacific/Port_Moresby'),
(2415, 598, 'Milne Bay', 'Pacific/Port_Moresby'),
(2416, 598, 'Northern', 'Pacific/Port_Moresby'),
(2417, 598, 'Southern Highlands', 'Pacific/Port_Moresby'),
(2418, 598, 'Western', 'Pacific/Port_Moresby'),
(2419, 598, 'Bougainville', 'Pacific/Port_Moresby'),
(2420, 598, 'Chimbu', 'Pacific/Port_Moresby'),
(2421, 598, 'Eastern Highlands', 'Pacific/Port_Moresby'),
(2422, 598, 'East New Britain', 'Pacific/Port_Moresby'),
(2423, 598, 'East Sepik', 'Pacific/Port_Moresby'),
(2424, 598, 'Madang', 'Pacific/Port_Moresby'),
(2425, 598, 'Manus', 'Pacific/Port_Moresby'),
(2426, 598, 'Morobe', 'Pacific/Port_Moresby'),
(2427, 598, 'New Ireland', 'Pacific/Port_Moresby'),
(2428, 598, 'Western Highlands', 'Pacific/Port_Moresby'),
(2429, 598, 'West New Britain', 'Pacific/Port_Moresby'),
(2430, 598, 'Sandaun', 'Pacific/Port_Moresby'),
(2431, 598, 'Enga', 'Pacific/Port_Moresby'),
(2432, 598, 'National Capital', 'Pacific/Port_Moresby'),
(2433, 600, 'Alto Paraná', 'America/Asuncion'),
(2434, 600, 'Amambay', 'America/Asuncion'),
(2435, 600, 'Caaguazú', 'America/Asuncion'),
(2436, 600, 'Caazapá', 'America/Asuncion'),
(2437, 600, 'Central', 'America/Asuncion'),
(2438, 600, 'Concepción', 'America/Asuncion'),
(2439, 600, 'Cordillera', 'America/Asuncion'),
(2440, 600, 'Guairá', 'America/Asuncion'),
(2441, 600, 'Itapúa', 'America/Asuncion'),
(2442, 600, 'Misiones', 'America/Asuncion'),
(2443, 600, 'Ñeembucú', 'America/Asuncion'),
(2444, 600, 'Paraguarí', 'America/Asuncion'),
(2445, 600, 'Presidente Hayes', 'America/Asuncion'),
(2446, 600, 'San Pedro', 'America/Asuncion'),
(2447, 600, 'Canindeyú', 'America/Asuncion'),
(2448, 600, 'Asunción', 'America/Asuncion'),
(2449, 600, 'Departamento de Alto Paraguay', 'America/Asuncion'),
(2450, 600, 'Boquerón', 'America/Asuncion'),
(2451, 604, 'Amazonas', 'America/Lima'),
(2452, 604, 'Ancash', 'America/Lima'),
(2453, 604, 'Apurímac', 'America/Lima'),
(2454, 604, 'Arequipa', 'America/Lima'),
(2455, 604, 'Ayacucho', 'America/Lima'),
(2456, 604, 'Cajamarca', 'America/Lima'),
(2457, 604, 'Callao', 'America/Lima'),
(2458, 604, 'Cusco', 'America/Lima'),
(2459, 604, 'Huancavelica', 'America/Lima'),
(2460, 604, 'Huanuco', 'America/Lima'),
(2461, 604, 'Ica', 'America/Lima'),
(2462, 604, 'Junín', 'America/Lima'),
(2463, 604, 'La Libertad', 'America/Lima'),
(2464, 604, 'Lambayeque', 'America/Lima'),
(2465, 604, 'Lima', 'America/Lima'),
(2466, 604, 'Provincia de Lima', 'America/Lima'),
(2467, 604, 'Loreto', 'America/Lima'),
(2468, 604, 'Madre de Dios', 'America/Lima'),
(2469, 604, 'Moquegua', 'America/Lima'),
(2470, 604, 'Pasco', 'America/Lima'),
(2471, 604, 'Piura', 'America/Lima'),
(2472, 604, 'Puno', 'America/Lima'),
(2473, 604, 'San Martín', 'America/Lima'),
(2474, 604, 'Tacna', 'America/Lima'),
(2475, 604, 'Tumbes', 'America/Lima'),
(2476, 604, 'Ucayali', 'America/Lima'),
(2477, 608, 'Abra', 'Asia/Manila'),
(2478, 608, 'Agusan del Norte', 'Asia/Manila'),
(2479, 608, 'Agusan del Sur', 'Asia/Manila'),
(2480, 608, 'Aklan', 'Asia/Manila'),
(2481, 608, 'Albay', 'Asia/Manila'),
(2482, 608, 'Antique', 'Asia/Manila'),
(2483, 608, 'Bataan', 'Asia/Manila'),
(2484, 608, 'Batanes', 'Asia/Manila'),
(2485, 608, 'Batangas', 'Asia/Manila'),
(2486, 608, 'Benguet', 'Asia/Manila'),
(2487, 608, 'Bohol', 'Asia/Manila'),
(2488, 608, 'Bukidnon', 'Asia/Manila'),
(2489, 608, 'Bulacan', 'Asia/Manila'),
(2490, 608, 'Cagayan', 'Asia/Manila'),
(2491, 608, 'Camarines Norte', 'Asia/Manila'),
(2492, 608, 'Camarines Sur', 'Asia/Manila'),
(2493, 608, 'Camiguin', 'Asia/Manila'),
(2494, 608, 'Capiz', 'Asia/Manila'),
(2495, 608, 'Catanduanes', 'Asia/Manila'),
(2496, 608, 'Cebu', 'Asia/Manila'),
(2497, 608, 'Basilan', 'Asia/Manila'),
(2498, 608, 'Eastern Samar', 'Asia/Manila'),
(2499, 608, 'Davao del Sur', 'Asia/Manila'),
(2500, 608, 'Davao Oriental', 'Asia/Manila'),
(2501, 608, 'Ifugao', 'Asia/Manila'),
(2502, 608, 'Ilocos Norte', 'Asia/Manila'),
(2503, 608, 'Ilocos Sur', 'Asia/Manila'),
(2504, 608, 'Iloilo', 'Asia/Manila'),
(2505, 608, 'Isabela', 'Asia/Manila'),
(2506, 608, 'Laguna', 'Asia/Manila'),
(2507, 608, 'Lanao del Sur', 'Asia/Manila'),
(2508, 608, 'La Union', 'Asia/Manila'),
(2509, 608, 'Leyte', 'Asia/Manila'),
(2510, 608, 'Marinduque', 'Asia/Manila'),
(2511, 608, 'Masbate', 'Asia/Manila'),
(2512, 608, 'Occidental Mindoro', 'Asia/Manila'),
(2513, 608, 'Oriental Mindoro', 'Asia/Manila'),
(2514, 608, 'Misamis Oriental', 'Asia/Manila'),
(2515, 608, 'Mountain Province', 'Asia/Manila'),
(2516, 608, 'Negros Oriental', 'Asia/Manila'),
(2517, 608, 'Nueva Ecija', 'Asia/Manila'),
(2518, 608, 'Nueva Vizcaya', 'Asia/Manila'),
(2519, 608, 'Palawan', 'Asia/Manila'),
(2520, 608, 'Pampanga', 'Asia/Manila'),
(2521, 608, 'Pangasinan', 'Asia/Manila'),
(2522, 608, 'Rizal', 'Asia/Manila'),
(2523, 608, 'Romblon', 'Asia/Manila'),
(2524, 608, 'Samar', 'Asia/Manila'),
(2525, 608, 'Maguindanao', 'Asia/Manila'),
(2526, 608, 'Cotabato City', 'Asia/Manila'),
(2527, 608, 'Sorsogon', 'Asia/Manila'),
(2528, 608, 'Southern Leyte', 'Asia/Manila'),
(2529, 608, 'Sulu', 'Asia/Manila'),
(2530, 608, 'Surigao del Norte', 'Asia/Manila'),
(2531, 608, 'Surigao del Sur', 'Asia/Manila'),
(2532, 608, 'Tarlac', 'Asia/Manila'),
(2533, 608, 'Zambales', 'Asia/Manila'),
(2534, 608, 'Zamboanga del Norte', 'Asia/Manila'),
(2535, 608, 'Zamboanga del Sur', 'Asia/Manila'),
(2536, 608, 'Northern Samar', 'Asia/Manila'),
(2537, 608, 'Quirino', 'Asia/Manila'),
(2538, 608, 'Siquijor', 'Asia/Manila'),
(2539, 608, 'South Cotabato', 'Asia/Manila'),
(2540, 608, 'Sultan Kudarat', 'Asia/Manila'),
(2541, 608, 'Kalinga', 'Asia/Manila'),
(2542, 608, 'Apayao', 'Asia/Manila'),
(2543, 608, 'Tawi-Tawi', 'Asia/Manila'),
(2544, 608, 'Angeles', 'Asia/Manila'),
(2545, 608, 'Bacolod City', 'Asia/Manila'),
(2546, 608, 'Compostela Valley', 'Asia/Manila'),
(2547, 608, 'Baguio', 'Asia/Manila'),
(2548, 608, 'Davao del Norte', 'Asia/Manila'),
(2549, 608, 'Butuan', 'Asia/Manila'),
(2550, 608, 'Guimaras', 'Asia/Manila'),
(2551, 608, 'Lanao del Norte', 'Asia/Manila'),
(2552, 608, 'Misamis Occidental', 'Asia/Manila'),
(2553, 608, 'Caloocan', 'Asia/Manila'),
(2554, 608, 'Cavite', 'Asia/Manila'),
(2555, 608, 'Cebu City', 'Asia/Manila'),
(2556, 608, 'Cotabato', 'Asia/Manila'),
(2557, 608, 'Dagupan', 'Asia/Manila'),
(2558, 608, 'Cagayan de Oro', 'Asia/Manila'),
(2559, 608, 'Iligan', 'Asia/Manila'),
(2560, 608, 'Davao', 'Asia/Manila'),
(2561, 608, 'Las Piñas', 'Asia/Manila'),
(2562, 608, 'Malabon', 'Asia/Manila'),
(2563, 608, 'General Santos', 'Asia/Manila'),
(2564, 608, 'Muntinlupa', 'Asia/Manila'),
(2565, 608, 'Iloilo City', 'Asia/Manila'),
(2566, 608, 'Navotas', 'Asia/Manila'),
(2567, 608, 'Parañaque', 'Asia/Manila'),
(2568, 608, 'Quezon City', 'Asia/Manila'),
(2569, 608, 'Lapu-Lapu', 'Asia/Manila'),
(2570, 608, 'Taguig', 'Asia/Manila'),
(2571, 608, 'Valenzuela', 'Asia/Manila'),
(2572, 608, 'Lucena', 'Asia/Manila'),
(2573, 608, 'Mandaue', 'Asia/Manila'),
(2574, 608, 'Manila', 'Asia/Manila'),
(2575, 608, 'Zamboanga Sibugay', 'Asia/Manila'),
(2576, 608, 'Naga', 'Asia/Manila'),
(2577, 608, 'Olongapo', 'Asia/Manila'),
(2578, 608, 'Ormoc', 'Asia/Manila'),
(2579, 608, 'Santiago', 'Asia/Manila'),
(2580, 608, 'Pateros', 'Asia/Manila'),
(2581, 608, 'Pasay', 'Asia/Manila'),
(2582, 608, 'Puerto Princesa', 'Asia/Manila'),
(2583, 608, 'Quezon', 'Asia/Manila'),
(2584, 608, 'Tacloban', 'Asia/Manila'),
(2585, 608, 'Zamboanga City', 'Asia/Manila'),
(2586, 608, 'Aurora', 'Asia/Manila'),
(2587, 608, 'Negros Occidental', 'Asia/Manila'),
(2588, 608, 'Biliran', 'Asia/Manila'),
(2589, 608, 'Makati City', 'Asia/Manila'),
(2590, 608, 'Sarangani', 'Asia/Manila'),
(2591, 608, 'Mandaluyong City', 'Asia/Manila'),
(2592, 608, 'Marikina', 'Asia/Manila'),
(2593, 608, 'Pasig', 'Asia/Manila'),
(2594, 608, 'San Juan', 'Asia/Manila'),
(2595, 612, 'Pitcairn Islands', 'Pacific/Pitcairn'),
(2596, 616, 'Biala Podlaska', 'Europe/Warsaw'),
(2597, 616, 'Bialystok', 'Europe/Warsaw'),
(2598, 616, 'Bielsko', 'Europe/Warsaw'),
(2599, 616, 'Bydgoszcz', 'Europe/Warsaw'),
(2600, 616, 'Chelm', 'Europe/Warsaw'),
(2601, 616, 'Ciechanow', 'Europe/Warsaw'),
(2602, 616, 'Czestochowa', 'Europe/Warsaw'),
(2603, 616, 'Elblag', 'Europe/Warsaw'),
(2604, 616, 'Gdansk', 'Europe/Warsaw'),
(2605, 616, 'Gorzow', 'Europe/Warsaw'),
(2606, 616, 'Jelenia Gora', 'Europe/Warsaw'),
(2607, 616, 'Kalisz', 'Europe/Warsaw'),
(2608, 616, 'Katowice', 'Europe/Warsaw'),
(2609, 616, 'Kielce', 'Europe/Warsaw'),
(2610, 616, 'Konin', 'Europe/Warsaw'),
(2611, 616, 'Koszalin', 'Europe/Warsaw'),
(2612, 616, 'Krakow', 'Europe/Warsaw'),
(2613, 616, 'Krosno', 'Europe/Warsaw'),
(2614, 616, 'Legnica', 'Europe/Warsaw'),
(2615, 616, 'Leszno', 'Europe/Warsaw'),
(2616, 616, 'Lodz', 'Europe/Warsaw'),
(2617, 616, 'Lomza', 'Europe/Warsaw'),
(2618, 616, 'Lublin', 'Europe/Warsaw'),
(2619, 616, 'Nowy Sacz', 'Europe/Warsaw'),
(2620, 616, 'Olsztyn', 'Europe/Warsaw'),
(2621, 616, 'Opole', 'Europe/Warsaw'),
(2622, 616, 'Ostroleka', 'Europe/Warsaw'),
(2623, 616, 'Pita', 'Europe/Warsaw'),
(2624, 616, 'Piotrkow', 'Europe/Warsaw'),
(2625, 616, 'Plock', 'Europe/Warsaw'),
(2626, 616, 'Poznan', 'Europe/Warsaw'),
(2627, 616, 'Przemysl', 'Europe/Warsaw'),
(2628, 616, 'Radom', 'Europe/Warsaw'),
(2629, 616, 'Rzeszow', 'Europe/Warsaw'),
(2630, 616, 'Siedlce', 'Europe/Warsaw'),
(2631, 616, 'Sieradz', 'Europe/Warsaw'),
(2632, 616, 'Skierniewice', 'Europe/Warsaw'),
(2633, 616, 'Slupsk', 'Europe/Warsaw'),
(2634, 616, 'Suwalki', 'Europe/Warsaw'),
(2635, 616, 'Szczecin', 'Europe/Warsaw'),
(2636, 616, 'Tarnobrzeg', 'Europe/Warsaw'),
(2637, 616, 'Tarnow', 'Europe/Warsaw'),
(2638, 616, 'Torufi', 'Europe/Warsaw'),
(2639, 616, 'Walbrzych', 'Europe/Warsaw'),
(2640, 616, 'Warszawa', 'Europe/Warsaw'),
(2641, 616, 'Wloclawek', 'Europe/Warsaw'),
(2642, 616, 'Wroclaw', 'Europe/Warsaw'),
(2643, 616, 'Zamosc', 'Europe/Warsaw'),
(2644, 616, 'Zielona Gora', 'Europe/Warsaw'),
(2645, 616, 'Lower Silesian Voivodeship', 'Europe/Warsaw'),
(2646, 616, 'Kujawsko-Pomorskie Voivodship', 'Europe/Warsaw'),
(2647, 616, 'Łódź Voivodeship', 'Europe/Warsaw'),
(2648, 616, 'Lublin Voivodeship', 'Europe/Warsaw'),
(2649, 616, 'Lubusz Voivodship', 'Europe/Warsaw'),
(2650, 616, 'Lesser Poland Voivodeship', 'Europe/Warsaw'),
(2651, 616, 'Masovian Voivodeship', 'Europe/Warsaw'),
(2652, 616, 'Opole Voivodeship', 'Europe/Warsaw'),
(2653, 616, 'Subcarpathian Voivodeship', 'Europe/Warsaw'),
(2654, 616, 'Podlasie Voivodship', 'Europe/Warsaw'),
(2655, 616, 'Pomeranian Voivodeship', 'Europe/Warsaw'),
(2656, 616, 'Silesian Voivodeship', 'Europe/Warsaw'),
(2657, 616, 'Świętokrzyskie Voivodship', 'Europe/Warsaw'),
(2658, 616, 'Warmian-Masurian Voivodeship', 'Europe/Warsaw'),
(2659, 616, 'Greater Poland Voivodeship', 'Europe/Warsaw'),
(2660, 616, 'West Pomeranian Voivodeship', 'Europe/Warsaw'),
(2661, 620, 'Aveiro', 'Europe/Lisbon'),
(2662, 620, 'Beja', 'Europe/Lisbon'),
(2663, 620, 'Braga', 'Europe/Lisbon'),
(2664, 620, 'Bragança', 'Europe/Lisbon'),
(2665, 620, 'Castelo Branco', 'Europe/Lisbon'),
(2666, 620, 'Coimbra', 'Europe/Lisbon'),
(2667, 620, 'Évora', 'Europe/Lisbon'),
(2668, 620, 'Faro', 'Europe/Lisbon'),
(2669, 620, 'Madeira', 'Atlantic/Madeira'),
(2670, 620, 'Guarda', 'Europe/Lisbon'),
(2671, 620, 'Leiria', 'Europe/Lisbon'),
(2672, 620, 'Lisbon', 'Europe/Lisbon'),
(2673, 620, 'Portalegre', 'Europe/Lisbon'),
(2674, 620, 'Porto', 'Europe/Lisbon'),
(2675, 620, 'Santarém', 'Europe/Lisbon'),
(2676, 620, 'Setúbal', 'Europe/Lisbon'),
(2677, 620, 'Viana do Castelo', 'Europe/Lisbon'),
(2678, 620, 'Vila Real', 'Europe/Lisbon'),
(2679, 620, 'Viseu', 'Europe/Lisbon'),
(2680, 620, 'Azores', 'Atlantic/Azores'),
(2681, 624, 'Bafatá', 'Africa/Bissau'),
(2682, 624, 'Quinara', 'Africa/Bissau'),
(2683, 624, 'Oio', 'Africa/Bissau'),
(2684, 624, 'Bolama', 'Africa/Bissau'),
(2685, 624, 'Cacheu', 'Africa/Bissau'),
(2686, 624, 'Tombali', 'Africa/Bissau'),
(2687, 624, 'Gabú', 'Africa/Bissau'),
(2688, 624, 'Bissau', 'Africa/Bissau'),
(2689, 624, 'Biombo', 'Africa/Bissau'),
(2690, 626, 'Bobonaro', 'Asia/Dili'),
(2691, 630, 'Puerto Rico', 'America/Puerto_Rico'),
(2692, 634, 'Ad Dawḩah', 'Asia/Qatar'),
(2693, 634, 'Al Ghuwayrīyah', 'Asia/Qatar'),
(2694, 634, 'Al Jumaylīyah', 'Asia/Qatar'),
(2695, 634, 'Al Khawr', 'Asia/Qatar'),
(2696, 634, 'Al Wakrah Municipality', 'Asia/Qatar'),
(2697, 634, 'Ar Rayyān', 'Asia/Qatar'),
(2698, 634, 'Jarayan al Batinah', 'Asia/Qatar'),
(2699, 634, 'Madīnat ash Shamāl', 'Asia/Qatar'),
(2700, 634, 'Umm Şalāl', 'Asia/Qatar'),
(2701, 634, 'Al Wakrah', 'Asia/Qatar'),
(2702, 634, 'Jarayān al Bāţinah', 'Asia/Qatar'),
(2703, 634, 'Umm Sa‘īd', 'Asia/Qatar'),
(2704, 638, 'Réunion', 'Indian/Reunion'),
(2705, 642, 'Alba', 'Europe/Bucharest'),
(2706, 642, 'Arad', 'Europe/Bucharest'),
(2707, 642, 'Argeş', 'Europe/Bucharest'),
(2708, 642, 'Bacău', 'Europe/Bucharest'),
(2709, 642, 'Bihor', 'Europe/Bucharest'),
(2710, 642, 'Bistriţa-Năsăud', 'Europe/Bucharest'),
(2711, 642, 'Botoşani', 'Europe/Bucharest'),
(2712, 642, 'Brăila', 'Europe/Bucharest'),
(2713, 642, 'Braşov', 'Europe/Bucharest'),
(2714, 642, 'Bucureşti', 'Europe/Bucharest'),
(2715, 642, 'Buzău', 'Europe/Bucharest'),
(2716, 642, 'Caraş-Severin', 'Europe/Bucharest'),
(2717, 642, 'Cluj', 'Europe/Bucharest'),
(2718, 642, 'Constanţa', 'Europe/Bucharest'),
(2719, 642, 'Covasna', 'Europe/Bucharest'),
(2720, 642, 'Dâmboviţa', 'Europe/Bucharest'),
(2721, 642, 'Dolj', 'Europe/Bucharest'),
(2722, 642, 'Galaţi', 'Europe/Bucharest'),
(2723, 642, 'Gorj', 'Europe/Bucharest'),
(2724, 642, 'Harghita', 'Europe/Bucharest'),
(2725, 642, 'Hunedoara', 'Europe/Bucharest'),
(2726, 642, 'Ialomiţa', 'Europe/Bucharest'),
(2727, 642, 'Iaşi', 'Europe/Bucharest'),
(2728, 642, 'Maramureş', 'Europe/Bucharest'),
(2729, 642, 'Mehedinţi', 'Europe/Bucharest'),
(2730, 642, 'Mureş', 'Europe/Bucharest'),
(2731, 642, 'Neamţ', 'Europe/Bucharest'),
(2732, 642, 'Olt', 'Europe/Bucharest'),
(2733, 642, 'Prahova', 'Europe/Bucharest'),
(2734, 642, 'Sălaj', 'Europe/Bucharest'),
(2735, 642, 'Satu Mare', 'Europe/Bucharest'),
(2736, 642, 'Sibiu', 'Europe/Bucharest'),
(2737, 642, 'Suceava', 'Europe/Bucharest'),
(2738, 642, 'Teleorman', 'Europe/Bucharest'),
(2739, 642, 'Timiş', 'Europe/Bucharest'),
(2740, 642, 'Tulcea', 'Europe/Bucharest'),
(2741, 642, 'Vaslui', 'Europe/Bucharest'),
(2742, 642, 'Vâlcea', 'Europe/Bucharest'),
(2743, 642, 'Judeţul Vrancea', 'Europe/Bucharest'),
(2744, 642, 'Călăraşi', 'Europe/Bucharest'),
(2745, 642, 'Giurgiu', 'Europe/Bucharest'),
(2746, 642, 'Ilfov', 'Europe/Bucharest'),
(2747, 643, 'Adygeya', 'Europe/Moscow'),
(2748, 643, 'Altay', 'Asia/Krasnoyarsk'),
(2749, 643, 'Altayskiy Kray', 'Asia/Krasnoyarsk'),
(2750, 643, 'Amur', 'Asia/Yakutsk'),
(2751, 643, 'Arkhangelskaya oblast', 'Europe/Moscow'),
(2752, 643, 'Astrakhan', 'Europe/Samara'),
(2753, 643, 'Bashkortostan', 'Asia/Yekaterinburg'),
(2754, 643, 'Belgorod', 'Europe/Moscow'),
(2755, 643, 'Brjansk', 'Europe/Moscow'),
(2756, 643, 'Buryatiya', 'Asia/Irkutsk'),
(2757, 643, 'Chechnya', 'Europe/Moscow'),
(2758, 643, 'Tsjeljabinsk', 'Asia/Yekaterinburg'),
(2759, 643, 'Zabaïkalski Kray', 'Asia/Yakutsk'),
(2760, 643, 'Chukotskiy Avtonomnyy Okrug', 'Asia/Kamchatka'),
(2761, 643, 'Chuvashia', 'Europe/Moscow'),
(2762, 643, 'Dagestan', 'Europe/Moscow'),
(2763, 643, 'Ingushetiya', 'Europe/Moscow'),
(2764, 643, 'Irkutsk', 'Asia/Irkutsk'),
(2765, 643, 'Ivanovo', 'Europe/Moscow'),
(2766, 643, 'Kabardino-Balkariya', 'Europe/Moscow'),
(2767, 643, 'Kaliningrad', 'Europe/Kaliningrad'),
(2768, 643, 'Kalmykiya', 'Europe/Moscow'),
(2769, 643, 'Kaluga', 'Europe/Moscow'),
(2770, 643, 'Karachayevo-Cherkesiya', 'Europe/Moscow'),
(2771, 643, 'Kareliya', 'Europe/Moscow'),
(2772, 643, 'Kemerovo', 'Asia/Krasnoyarsk'),
(2773, 643, 'Khabarovsk Krai', 'Asia/Vladivostok'),
(2774, 643, 'Khakasiya', 'Asia/Krasnoyarsk'),
(2775, 643, 'Khanty-Mansiyskiy Avtonomnyy Okrug', 'Asia/Yekaterinburg'),
(2776, 643, 'Kirov', 'Europe/Samara'),
(2777, 643, 'Komi', 'Europe/Moscow'),
(2778, 643, 'Kostroma', 'Europe/Moscow'),
(2779, 643, 'Krasnodarskiy Kray', 'Asia/Krasnoyarsk'),
(2780, 643, 'Kurgan', 'Asia/Yekaterinburg'),
(2781, 643, 'Kursk', 'Europe/Moscow'),
(2782, 643, 'Leningradskaya Oblastʼ', 'Europe/Moscow'),
(2783, 643, 'Lipetsk', 'Europe/Moscow'),
(2784, 643, 'Magadan', 'Asia/Magadan'),
(2785, 643, 'Mariy-El', 'Europe/Moscow'),
(2786, 643, 'Mordoviya', 'Europe/Moscow'),
(2787, 643, 'Moskovskaya Oblastʼ', 'Europe/Moscow'),
(2788, 643, 'Moscow', 'Europe/Moscow'),
(2789, 643, 'Murmansk Oblast', 'Europe/Moscow'),
(2790, 643, 'Nenetskiy Avtonomnyy Okrug', 'Europe/Moscow'),
(2791, 643, 'Nizjnij Novgorod', 'Europe/Moscow'),
(2792, 643, 'Novgorod', 'Europe/Moscow'),
(2793, 643, 'Novosibirsk', 'Asia/Novosibirsk'),
(2794, 643, 'Omsk', 'Asia/Omsk'),
(2795, 643, 'Orenburg', 'Asia/Yekaterinburg'),
(2796, 643, 'Orjol', 'Europe/Moscow'),
(2797, 643, 'Penza', 'Europe/Moscow'),
(2798, 643, 'Primorskiy Kray', 'Asia/Vladivostok'),
(2799, 643, 'Pskov', 'Europe/Moscow'),
(2800, 643, 'Rostov', 'Europe/Moscow'),
(2801, 643, 'Rjazan', 'Europe/Moscow'),
(2802, 643, 'Sakha', 'Asia/Vladivostok'),
(2803, 643, 'Sakhalin', 'Asia/Vladivostok'),
(2804, 643, 'Samara', 'Europe/Samara'),
(2805, 643, 'Sankt-Peterburg', 'Europe/Moscow'),
(2806, 643, 'Saratov', 'Europe/Samara'),
(2807, 643, 'Severnaya Osetiya-Alaniya', 'Europe/Moscow'),
(2808, 643, 'Smolensk', 'Europe/Moscow'),
(2809, 643, 'Stavropolʼskiy Kray', 'Europe/Moscow'),
(2810, 643, 'Sverdlovsk', 'Asia/Yekaterinburg'),
(2811, 643, 'Tambov', 'Europe/Moscow'),
(2812, 643, 'Tatarstan', 'Europe/Moscow'),
(2813, 643, 'Tomsk', 'Asia/Krasnoyarsk'),
(2814, 643, 'Tula', 'Europe/Moscow'),
(2815, 643, 'Tverskaya Oblast’', 'Europe/Moscow'),
(2816, 643, 'Tjumen', 'Asia/Yekaterinburg'),
(2817, 643, 'Tyva', 'Asia/Krasnoyarsk'),
(2818, 643, 'Udmurtiya', 'Europe/Samara'),
(2819, 643, 'Uljanovsk', 'Europe/Samara'),
(2820, 643, 'Vladimir', 'Europe/Moscow'),
(2821, 643, 'Volgograd', 'Europe/Samara'),
(2822, 643, 'Vologda', 'Europe/Moscow'),
(2823, 643, 'Voronezj', 'Europe/Moscow'),
(2824, 643, 'Yamalo-Nenetskiy Avtonomnyy Okrug', 'Asia/Yekaterinburg'),
(2825, 643, 'Jaroslavl', 'Europe/Moscow'),
(2826, 643, 'Jewish Autonomous Oblast', 'Asia/Vladivostok'),
(2827, 643, 'Perm', 'Asia/Yekaterinburg'),
(2828, 643, 'Krasnoyarskiy Kray', 'Asia/Krasnoyarsk'),
(2829, 643, 'Kamtsjatka', 'Asia/Kamchatka'),
(2830, 643, 'RSJA', 'Europe/Moscow'),
(2831, 646, 'Eastern Province', 'Africa/Kigali'),
(2832, 646, 'Kigali City', 'Africa/Kigali'),
(2833, 646, 'Northern Province', 'Africa/Kigali'),
(2834, 646, 'Western Province', 'Africa/Kigali'),
(2835, 646, 'Southern Province', 'Africa/Kigali'),
(2836, 654, 'Ascension', 'Atlantic/St_Helena'),
(2837, 654, 'Saint Helena', 'Atlantic/St_Helena'),
(2838, 654, 'Tristan da Cunha', 'Atlantic/St_Helena'),
(2839, 659, 'Christ Church Nichola Town', 'America/St_Kitts'),
(2840, 659, 'Saint Anne Sandy Point', 'America/St_Kitts'),
(2841, 659, 'Saint George Basseterre', 'America/St_Kitts'),
(2842, 659, 'Saint George Gingerland', 'America/St_Kitts'),
(2843, 659, 'Saint James Windwa', 'America/St_Kitts'),
(2844, 659, 'Saint John Capesterre', 'America/St_Kitts'),
(2845, 659, 'Saint John Figtree', 'America/St_Kitts'),
(2846, 659, 'Saint Mary Cayon', 'America/St_Kitts'),
(2847, 659, 'Saint Paul Capesterre', 'America/St_Kitts'),
(2848, 659, 'Saint Paul Charlestown', 'America/St_Kitts'),
(2849, 659, 'Saint Peter Basseterre', 'America/St_Kitts'),
(2850, 659, 'Saint Thomas Lowland', 'America/St_Kitts'),
(2851, 659, 'Saint Thomas Middle Island', 'America/St_Kitts'),
(2852, 659, 'Trinity Palmetto Point', 'America/St_Kitts'),
(2853, 660, 'Anguilla', 'America/Anguilla'),
(2854, 662, 'Anse-la-Raye', 'America/St_Lucia'),
(2855, 662, 'Dauphin', 'America/St_Lucia'),
(2856, 662, 'Castries', 'America/St_Lucia'),
(2857, 662, 'Choiseul', 'America/St_Lucia'),
(2858, 662, 'Dennery', 'America/St_Lucia'),
(2859, 662, 'Gros-Islet', 'America/St_Lucia'),
(2860, 662, 'Laborie', 'America/St_Lucia'),
(2861, 662, 'Micoud', 'America/St_Lucia'),
(2862, 662, 'Soufrière', 'America/St_Lucia'),
(2863, 662, 'Vieux-Fort', 'America/St_Lucia'),
(2864, 662, 'Praslin', 'America/St_Lucia'),
(2865, 666, 'Saint-Pierre-et-Miquelon', 'America/Miquelon'),
(2866, 670, 'Charlotte', 'America/St_Vincent'),
(2867, 670, 'Saint Andrew', 'America/St_Vincent'),
(2868, 670, 'Saint David', 'America/St_Vincent'),
(2869, 670, 'Saint George', 'America/St_Vincent'),
(2870, 670, 'Saint Patrick', 'America/St_Vincent'),
(2871, 670, 'Grenadines', 'America/St_Vincent'),
(2872, 674, 'Acquaviva', 'Europe/San_Marino'),
(2873, 674, 'Chiesanuova', 'Europe/San_Marino'),
(2874, 674, 'Domagnano', 'Europe/San_Marino'),
(2875, 674, 'Faetano', 'Europe/San_Marino'),
(2876, 674, 'Fiorentino', 'Europe/San_Marino'),
(2877, 674, 'Borgo Maggiore', 'Europe/San_Marino'),
(2878, 674, 'San Marino', 'Europe/San_Marino'),
(2879, 674, 'Montegiardino', 'Europe/San_Marino'),
(2880, 674, 'Serravalle', 'Europe/San_Marino'),
(2881, 678, 'Príncipe', 'Africa/Sao_Tome'),
(2882, 678, 'Príncipe', 'Africa/Sao_Tome'),
(2883, 678, 'São Tomé', 'Africa/Sao_Tome'),
(2884, 682, 'Al Bāḩah', 'Asia/Riyadh'),
(2885, 682, 'Al Madīnah', 'Asia/Riyadh'),
(2886, 682, 'Ash Sharqīyah', 'Asia/Riyadh'),
(2887, 682, 'Al Qaşīm', 'Asia/Riyadh'),
(2888, 682, 'Ar Riyāḑ', 'Asia/Riyadh'),
(2889, 682, '‘Asīr', 'Asia/Riyadh'),
(2890, 682, 'Ḩāʼil', 'Asia/Riyadh'),
(2891, 682, 'Makkah', 'Asia/Riyadh'),
(2892, 682, 'Northern Borders Region', 'Asia/Riyadh'),
(2893, 682, 'Najrān', 'Asia/Riyadh'),
(2894, 682, 'Jīzān', 'Asia/Riyadh'),
(2895, 682, 'Tabūk', 'Asia/Riyadh'),
(2896, 682, 'Al Jawf', 'Asia/Riyadh'),
(2897, 686, 'Dakar', 'Africa/Dakar'),
(2898, 686, 'Diourbel', 'Africa/Dakar'),
(2899, 686, 'Saint-Louis', 'Africa/Dakar'),
(2900, 686, 'Tambacounda', 'Africa/Dakar'),
(2901, 686, 'Thiès', 'Africa/Dakar'),
(2902, 686, 'Louga', 'Africa/Dakar'),
(2903, 686, 'Fatick', 'Africa/Dakar'),
(2904, 686, 'Kaolack', 'Africa/Dakar'),
(2905, 686, 'Kolda Region', 'Africa/Dakar'),
(2906, 686, 'Ziguinchor', 'Africa/Dakar'),
(2907, 686, 'Louga', 'Africa/Dakar'),
(2908, 686, 'Saint-Louis', 'Africa/Dakar'),
(2909, 686, 'Matam', 'Africa/Dakar'),
(2910, 688, 'Autonomna Pokrajina Vojvodina', 'Europe/Belgrade'),
(2911, 690, 'Anse aux Pins', 'Indian/Mahe'),
(2912, 690, 'Anse Boileau', 'Indian/Mahe'),
(2913, 690, 'Anse Etoile', 'Indian/Mahe'),
(2914, 690, 'Anse Louis', 'Indian/Mahe'),
(2915, 690, 'Anse Royale', 'Indian/Mahe'),
(2916, 690, 'Baie Lazare', 'Indian/Mahe'),
(2917, 690, 'Baie Sainte Anne', 'Indian/Mahe'),
(2918, 690, 'Beau Vallon', 'Indian/Mahe'),
(2919, 690, 'Bel Air', 'Indian/Mahe'),
(2920, 690, 'Bel Ombre', 'Indian/Mahe'),
(2921, 690, 'Cascade', 'Indian/Mahe'),
(2922, 690, 'Glacis', 'Indian/Mahe'),
(2923, 690, 'Saint Thomas Middle Island Parish', 'Indian/Mahe'),
(2924, 690, 'Grand Anse Praslin', 'Indian/Mahe'),
(2925, 690, 'Trinity Palmetto Point Parish', 'Indian/Mahe'),
(2926, 690, 'La Riviere Anglaise', 'Indian/Mahe'),
(2927, 690, 'Mont Buxton', 'Indian/Mahe'),
(2928, 690, 'Mont Fleuri', 'Indian/Mahe'),
(2929, 690, 'Plaisance', 'Indian/Mahe'),
(2930, 690, 'Pointe Larue', 'Indian/Mahe'),
(2931, 690, 'Port Glaud', 'Indian/Mahe'),
(2932, 690, 'Saint Louis', 'Indian/Mahe'),
(2933, 690, 'Takamaka', 'Indian/Mahe'),
(2934, 690, 'Anse aux Pins', 'Indian/Mahe'),
(2935, 690, 'Inner Islands', 'Indian/Mahe'),
(2936, 690, 'English River', 'Indian/Mahe'),
(2937, 690, 'Port Glaud', 'Indian/Mahe'),
(2938, 690, 'Baie Lazare', 'Indian/Mahe'),
(2939, 690, 'Beau Vallon', 'Indian/Mahe'),
(2940, 690, 'Bel Ombre', 'Indian/Mahe'),
(2941, 690, 'Glacis', 'Indian/Mahe'),
(2942, 690, 'Grand Anse Mahe', 'Indian/Mahe'),
(2943, 690, 'Grand Anse Praslin', 'Indian/Mahe'),
(2944, 690, 'Inner Islands', 'Indian/Mahe'),
(2945, 690, 'English River', 'Indian/Mahe'),
(2946, 690, 'Mont Fleuri', 'Indian/Mahe'),
(2947, 690, 'Plaisance', 'Indian/Mahe'),
(2948, 690, 'Pointe Larue', 'Indian/Mahe'),
(2949, 690, 'Port Glaud', 'Indian/Mahe'),
(2950, 690, 'Takamaka', 'Indian/Mahe'),
(2951, 690, 'Au Cap', 'Indian/Mahe'),
(2952, 690, 'Les Mamelles', 'Indian/Mahe'),
(2953, 690, 'Roche Caiman', 'Indian/Mahe'),
(2954, 694, 'Eastern Province', 'Africa/Freetown'),
(2955, 694, 'Northern Province', 'Africa/Freetown'),
(2956, 694, 'Southern Province', 'Africa/Freetown'),
(2957, 694, 'Western Area', 'Africa/Freetown'),
(2958, 702, 'Singapore', 'Asia/Singapore'),
(2959, 703, 'Banskobystrický', 'Europe/Bratislava'),
(2960, 703, 'Bratislavský', 'Europe/Bratislava'),
(2961, 703, 'Košický', 'Europe/Bratislava'),
(2962, 703, 'Nitriansky', 'Europe/Bratislava'),
(2963, 703, 'Prešovský', 'Europe/Bratislava'),
(2964, 703, 'Trenčiansky', 'Europe/Bratislava'),
(2965, 703, 'Trnavský', 'Europe/Bratislava'),
(2966, 703, 'Žilinský', 'Europe/Bratislava'),
(2967, 704, 'An Giang', 'Asia/Ho_Chi_Minh'),
(2968, 704, 'Bắc Thái Tỉnh', 'Asia/Ho_Chi_Minh'),
(2969, 704, 'Bến Tre', 'Asia/Ho_Chi_Minh'),
(2970, 704, 'Cao Bang', 'Asia/Ho_Chi_Minh'),
(2971, 704, 'Cao Bằng', 'Asia/Ho_Chi_Minh'),
(2972, 704, 'Ten Bai', 'Asia/Ho_Chi_Minh'),
(2973, 704, 'Ðồng Tháp', 'Asia/Ho_Chi_Minh'),
(2974, 704, 'Hà Bắc Tỉnh', 'Asia/Ho_Chi_Minh'),
(2975, 704, 'Hải Hưng Tỉnh', 'Asia/Ho_Chi_Minh'),
(2976, 704, 'Hải Phòng', 'Asia/Ho_Chi_Minh'),
(2977, 704, 'Hoa Binh', 'Asia/Ho_Chi_Minh'),
(2978, 704, 'Ha Tay', 'Asia/Ho_Chi_Minh'),
(2979, 704, 'Hồ Chí Minh', 'Asia/Ho_Chi_Minh'),
(2980, 704, 'Kiến Giang', 'Asia/Ho_Chi_Minh'),
(2981, 704, 'Lâm Ðồng', 'Asia/Ho_Chi_Minh'),
(2982, 704, 'Long An', 'Asia/Ho_Chi_Minh'),
(2983, 704, 'Minh Hải Tỉnh', 'Asia/Ho_Chi_Minh'),
(2984, 704, 'Thua Thien-Hue', 'Asia/Ho_Chi_Minh'),
(2985, 704, 'Quang Nam', 'Asia/Ho_Chi_Minh'),
(2986, 704, 'Kon Tum', 'Asia/Ho_Chi_Minh'),
(2987, 704, 'Quảng Nam-Ðà Nẵng Tỉnh', 'Asia/Ho_Chi_Minh'),
(2988, 704, 'Quảng Ninh', 'Asia/Ho_Chi_Minh'),
(2989, 704, 'Sông Bé Tỉnh', 'Asia/Ho_Chi_Minh'),
(2990, 704, 'Sơn La', 'Asia/Ho_Chi_Minh'),
(2991, 704, 'Tây Ninh', 'Asia/Ho_Chi_Minh'),
(2992, 704, 'Thanh Hóa', 'Asia/Ho_Chi_Minh'),
(2993, 704, 'Thái Bình', 'Asia/Ho_Chi_Minh'),
(2994, 704, 'Nin Thuan', 'Asia/Ho_Chi_Minh'),
(2995, 704, 'Tiền Giang', 'Asia/Ho_Chi_Minh'),
(2996, 704, 'Vinh Phú Tỉnh', 'Asia/Ho_Chi_Minh'),
(2997, 704, 'Lạng Sơn', 'Asia/Ho_Chi_Minh'),
(2998, 704, 'Binh Thuan', 'Asia/Ho_Chi_Minh'),
(2999, 704, 'Long An', 'Asia/Ho_Chi_Minh'),
(3000, 704, 'Ðồng Nai', 'Asia/Ho_Chi_Minh'),
(3001, 704, 'Ha Nội', 'Asia/Ho_Chi_Minh'),
(3002, 704, 'Bà Rịa-Vũng Tàu', 'Asia/Ho_Chi_Minh'),
(3003, 704, 'Bình Ðịnh', 'Asia/Ho_Chi_Minh'),
(3004, 704, 'Bình Thuận', 'Asia/Ho_Chi_Minh'),
(3005, 704, 'Gia Lai', 'Asia/Ho_Chi_Minh'),
(3006, 704, 'Hà Giang', 'Asia/Ho_Chi_Minh'),
(3007, 704, 'Hà Tây', 'Asia/Ho_Chi_Minh'),
(3008, 704, 'Hà Tĩnh', 'Asia/Ho_Chi_Minh'),
(3009, 704, 'Hòa Bình', 'Asia/Ho_Chi_Minh'),
(3010, 704, 'Khánh Hòa', 'Asia/Ho_Chi_Minh'),
(3011, 704, 'Kon Tum', 'Asia/Ho_Chi_Minh'),
(3012, 704, 'Nam Hà Tỉnh', 'Asia/Ho_Chi_Minh'),
(3013, 704, 'Nghệ An', 'Asia/Ho_Chi_Minh'),
(3014, 704, 'Ninh Bình', 'Asia/Ho_Chi_Minh'),
(3015, 704, 'Ninh Thuận', 'Asia/Ho_Chi_Minh'),
(3016, 704, 'Phú Yên', 'Asia/Ho_Chi_Minh'),
(3017, 704, 'Quảng Bình', 'Asia/Ho_Chi_Minh'),
(3018, 704, 'Quảng Ngãi', 'Asia/Ho_Chi_Minh'),
(3019, 704, 'Quảng Trị', 'Asia/Ho_Chi_Minh'),
(3020, 704, 'Sóc Trăng', 'Asia/Ho_Chi_Minh'),
(3021, 704, 'Thừa Thiên-Huế', 'Asia/Ho_Chi_Minh'),
(3022, 704, 'Trà Vinh', 'Asia/Ho_Chi_Minh'),
(3023, 704, 'Tuyên Quang', 'Asia/Ho_Chi_Minh'),
(3024, 704, 'Vĩnh Long', 'Asia/Ho_Chi_Minh'),
(3025, 704, 'Yên Bái', 'Asia/Ho_Chi_Minh'),
(3026, 704, 'Bắc Giang', 'Asia/Ho_Chi_Minh'),
(3027, 704, 'Bắc Kạn', 'Asia/Ho_Chi_Minh'),
(3028, 704, 'Bạc Liêu', 'Asia/Ho_Chi_Minh'),
(3029, 704, 'Bắc Ninh', 'Asia/Ho_Chi_Minh'),
(3030, 704, 'Bình Dương', 'Asia/Ho_Chi_Minh'),
(3031, 704, 'Bình Phước', 'Asia/Ho_Chi_Minh'),
(3032, 704, 'Cà Mau', 'Asia/Ho_Chi_Minh'),
(3033, 704, 'Ðà Nẵng', 'Asia/Ho_Chi_Minh'),
(3034, 704, 'Hải Dương', 'Asia/Ho_Chi_Minh'),
(3035, 704, 'Hà Nam', 'Asia/Ho_Chi_Minh'),
(3036, 704, 'Hưng Yên', 'Asia/Ho_Chi_Minh'),
(3037, 704, 'Nam Ðịnh', 'Asia/Ho_Chi_Minh'),
(3038, 704, 'Phú Thọ', 'Asia/Ho_Chi_Minh'),
(3039, 704, 'Quảng Nam', 'Asia/Ho_Chi_Minh'),
(3040, 704, 'Thái Nguyên', 'Asia/Ho_Chi_Minh'),
(3041, 704, 'Vĩnh Phúc', 'Asia/Ho_Chi_Minh'),
(3042, 704, 'Cần Thơ', 'Asia/Ho_Chi_Minh'),
(3043, 704, 'Ðắc Lắk', 'Asia/Ho_Chi_Minh'),
(3044, 704, 'Lai Châu', 'Asia/Ho_Chi_Minh'),
(3045, 704, 'Lào Cai', 'Asia/Ho_Chi_Minh'),
(3046, 705, 'Notranjska', 'Europe/Ljubljana'),
(3047, 705, 'Koroška', 'Europe/Ljubljana'),
(3048, 705, 'Štajerska', 'Europe/Ljubljana'),
(3049, 705, 'Prekmurje', 'Europe/Ljubljana'),
(3050, 705, 'Primorska', 'Europe/Ljubljana'),
(3051, 705, 'Gorenjska', 'Europe/Ljubljana'),
(3052, 705, 'Dolenjska', 'Europe/Ljubljana'),
(3053, 706, 'Bakool', 'Africa/Mogadishu'),
(3054, 706, 'Banaadir', 'Africa/Mogadishu'),
(3055, 706, 'Bari', 'Africa/Mogadishu'),
(3056, 706, 'Bay', 'Africa/Mogadishu'),
(3057, 706, 'Galguduud', 'Africa/Mogadishu'),
(3058, 706, 'Gedo', 'Africa/Mogadishu'),
(3059, 706, 'Hiiraan', 'Africa/Mogadishu'),
(3060, 706, 'Middle Juba', 'Africa/Mogadishu'),
(3061, 706, 'Lower Juba', 'Africa/Mogadishu'),
(3062, 706, 'Mudug', 'Africa/Mogadishu'),
(3063, 706, 'Sanaag', 'Africa/Mogadishu'),
(3064, 706, 'Middle Shabele', 'Africa/Mogadishu'),
(3065, 706, 'Shabeellaha Hoose', 'Africa/Mogadishu'),
(3066, 706, 'Nugaal', 'Africa/Mogadishu'),
(3067, 706, 'Togdheer', 'Africa/Mogadishu'),
(3068, 706, 'Woqooyi Galbeed', 'Africa/Mogadishu'),
(3069, 706, 'Awdal', 'Africa/Mogadishu'),
(3070, 706, 'Sool', 'Africa/Mogadishu'),
(3071, 710, 'KwaZulu-Natal', 'Africa/Johannesburg'),
(3072, 710, 'Free State', 'Africa/Johannesburg'),
(3073, 710, 'Eastern Cape', 'Africa/Johannesburg'),
(3074, 710, 'Gauteng', 'Africa/Johannesburg'),
(3075, 710, 'Mpumalanga', 'Africa/Johannesburg'),
(3076, 710, 'Northern Cape', 'Africa/Johannesburg'),
(3077, 710, 'Limpopo', 'Africa/Johannesburg'),
(3078, 710, 'North-West', 'Africa/Johannesburg'),
(3079, 710, 'Western Cape', 'Africa/Johannesburg'),
(3080, 716, 'Manicaland', 'Africa/Harare'),
(3081, 716, 'Midlands', 'Africa/Harare'),
(3082, 716, 'Mashonaland Central', 'Africa/Harare'),
(3083, 716, 'Mashonaland East', 'Africa/Harare'),
(3084, 716, 'Mashonaland West', 'Africa/Harare'),
(3085, 716, 'Matabeleland North', 'Africa/Harare'),
(3086, 716, 'Matabeleland South', 'Africa/Harare'),
(3087, 716, 'Masvingo', 'Africa/Harare'),
(3088, 716, 'Bulawayo', 'Africa/Harare'),
(3089, 716, 'Harare', 'Africa/Harare'),
(3090, 724, 'Ceuta', 'Africa/Ceuta'),
(3091, 724, 'Balearic Islands', 'Europe/Madrid'),
(3092, 724, 'La Rioja', 'Europe/Madrid'),
(3093, 724, 'Autonomous Region of Madrid', 'Europe/Madrid'),
(3094, 724, 'Murcia', 'Europe/Madrid'),
(3095, 724, 'Navarre', 'Europe/Madrid'),
(3096, 724, 'Asturias', 'Europe/Madrid'),
(3097, 724, 'Cantabria', 'Europe/Madrid'),
(3098, 724, 'Andalusia', 'Europe/Madrid'),
(3099, 724, 'Aragon', 'Europe/Madrid'),
(3100, 724, 'Canary Islands', 'Atlantic/Canary'),
(3101, 724, 'Castille-La Mancha', 'Europe/Madrid'),
(3102, 724, 'Castille and León', 'Europe/Madrid'),
(3103, 724, 'Catalonia', 'Europe/Madrid'),
(3104, 724, 'Extremadura', 'Europe/Madrid'),
(3105, 724, 'Galicia', 'Europe/Madrid'),
(3106, 724, 'Basque Country', 'Europe/Madrid'),
(3107, 724, 'Valencia', 'Europe/Madrid'),
(3108, 732, 'Western Sahara', 'Africa/El_Aaiun'),
(3109, 736, 'Al Wilāyah al Wusţá', 'Africa/Khartoum'),
(3110, 736, 'Al Wilāyah al Istiwāʼīyah', 'Africa/Khartoum'),
(3111, 736, 'Khartoum', 'Africa/Khartoum'),
(3112, 736, 'Ash Shamaliyah', 'Africa/Khartoum'),
(3113, 736, 'Al Wilāyah ash Sharqīyah', 'Africa/Khartoum'),
(3114, 736, 'Ba?r al Ghazal Wilayat', 'Africa/Khartoum'),
(3115, 736, 'Darfur Wilayat', 'Africa/Khartoum'),
(3116, 736, 'Kurdufan Wilayat', 'Africa/Khartoum'),
(3117, 736, 'Upper Nile', 'Africa/Khartoum'),
(3118, 736, 'Red Sea', 'Africa/Khartoum'),
(3119, 736, 'Lakes', 'Africa/Khartoum'),
(3120, 736, 'Al Jazirah', 'Africa/Khartoum'),
(3121, 736, 'Al Qadarif', 'Africa/Khartoum'),
(3122, 736, 'Unity', 'Africa/Khartoum'),
(3123, 736, 'White Nile', 'Africa/Khartoum'),
(3124, 736, 'Blue Nile', 'Africa/Khartoum'),
(3125, 736, 'Northern', 'Africa/Khartoum'),
(3126, 736, 'Central Equatoria', 'Africa/Khartoum'),
(3127, 736, 'Gharb al Istiwāʼīyah', 'Africa/Khartoum'),
(3128, 736, 'Western Bahr al Ghazal', 'Africa/Khartoum'),
(3129, 736, 'Gharb Dārfūr', 'Africa/Khartoum'),
(3130, 736, 'Gharb Kurdufān', 'Africa/Khartoum'),
(3131, 736, 'Janūb Dārfūr', 'Africa/Khartoum'),
(3132, 736, 'Janūb Kurdufān', 'Africa/Khartoum'),
(3133, 736, 'Junqalī', 'Africa/Khartoum'),
(3134, 736, 'Kassalā', 'Africa/Khartoum'),
(3135, 736, 'Nahr an Nīl', 'Africa/Khartoum'),
(3136, 736, 'Shamāl Baḩr al Ghazāl', 'Africa/Khartoum'),
(3137, 736, 'Shamāl Dārfūr', 'Africa/Khartoum'),
(3138, 736, 'Shamāl Kurdufān', 'Africa/Khartoum'),
(3139, 736, 'Eastern Equatoria', 'Africa/Khartoum'),
(3140, 736, 'Sinnār', 'Africa/Khartoum'),
(3141, 736, 'Warab', 'Africa/Khartoum'),
(3142, 740, 'Brokopondo', 'America/Paramaribo'),
(3143, 740, 'Commewijne', 'America/Paramaribo'),
(3144, 740, 'Coronie', 'America/Paramaribo'),
(3145, 740, 'Marowijne', 'America/Paramaribo'),
(3146, 740, 'Nickerie', 'America/Paramaribo'),
(3147, 740, 'Para', 'America/Paramaribo'),
(3148, 740, 'Paramaribo', 'America/Paramaribo'),
(3149, 740, 'Saramacca', 'America/Paramaribo'),
(3150, 740, 'Sipaliwini', 'America/Paramaribo'),
(3151, 740, 'Wanica', 'America/Paramaribo'),
(3152, 748, 'Hhohho', 'Africa/Mbabane'),
(3153, 748, 'Lubombo', 'Africa/Mbabane'),
(3154, 748, 'Manzini', 'Africa/Mbabane'),
(3155, 748, 'Shiselweni', 'Africa/Mbabane'),
(3156, 752, 'Blekinge', 'Europe/Stockholm'),
(3157, 752, 'Gävleborg', 'Europe/Stockholm'),
(3158, 752, 'Gotland', 'Europe/Stockholm'),
(3159, 752, 'Halland', 'Europe/Stockholm'),
(3160, 752, 'Jämtland', 'Europe/Stockholm'),
(3161, 752, 'Jönköping', 'Europe/Stockholm'),
(3162, 752, 'Kalmar', 'Europe/Stockholm'),
(3163, 752, 'Dalarna', 'Europe/Stockholm'),
(3164, 752, 'Kronoberg', 'Europe/Stockholm'),
(3165, 752, 'Norrbotten', 'Europe/Stockholm'),
(3166, 752, 'Örebro', 'Europe/Stockholm'),
(3167, 752, 'Östergötland', 'Europe/Stockholm'),
(3168, 752, 'Södermanland', 'Europe/Stockholm'),
(3169, 752, 'Uppsala', 'Europe/Stockholm'),
(3170, 752, 'Värmland', 'Europe/Stockholm'),
(3171, 752, 'Västerbotten', 'Europe/Stockholm'),
(3172, 752, 'Västernorrland', 'Europe/Stockholm'),
(3173, 752, 'Västmanland', 'Europe/Stockholm'),
(3174, 752, 'Stockholm', 'Europe/Stockholm'),
(3175, 752, 'Skåne', 'Europe/Stockholm'),
(3176, 752, 'Västra Götaland', 'Europe/Stockholm'),
(3177, 756, 'Aargau', 'Europe/Zurich'),
(3178, 756, 'Appenzell Innerrhoden', 'Europe/Zurich'),
(3179, 756, 'Appenzell Ausserrhoden', 'Europe/Zurich'),
(3180, 756, 'Bern', 'Europe/Zurich'),
(3181, 756, 'Basel-Landschaft', 'Europe/Zurich'),
(3182, 756, 'Kanton Basel-Stadt', 'Europe/Zurich'),
(3183, 756, 'Fribourg', 'Europe/Zurich'),
(3184, 756, 'Genève', 'Europe/Zurich'),
(3185, 756, 'Glarus', 'Europe/Zurich'),
(3186, 756, 'Graubünden', 'Europe/Zurich'),
(3187, 756, 'Jura', 'Europe/Zurich'),
(3188, 756, 'Luzern', 'Europe/Zurich'),
(3189, 756, 'Neuchâtel', 'Europe/Zurich'),
(3190, 756, 'Nidwalden', 'Europe/Zurich'),
(3191, 756, 'Obwalden', 'Europe/Zurich'),
(3192, 756, 'Kanton St. Gallen', 'Europe/Zurich'),
(3193, 756, 'Schaffhausen', 'Europe/Zurich'),
(3194, 756, 'Solothurn', 'Europe/Zurich'),
(3195, 756, 'Schwyz', 'Europe/Zurich'),
(3196, 756, 'Thurgau', 'Europe/Zurich'),
(3197, 756, 'Ticino', 'Europe/Zurich'),
(3198, 756, 'Uri', 'Europe/Zurich'),
(3199, 756, 'Vaud', 'Europe/Zurich'),
(3200, 756, 'Valais', 'Europe/Zurich'),
(3201, 756, 'Zug', 'Europe/Zurich'),
(3202, 756, 'Zürich', 'Europe/Zurich'),
(3203, 760, 'Al-Hasakah', 'Asia/Damascus'),
(3204, 760, 'Latakia', 'Asia/Damascus'),
(3205, 760, 'Quneitra', 'Asia/Damascus'),
(3206, 760, 'Ar-Raqqah', 'Asia/Damascus'),
(3207, 760, 'As-Suwayda', 'Asia/Damascus'),
(3208, 760, 'Daraa', 'Asia/Damascus'),
(3209, 760, 'Deir ez-Zor', 'Asia/Damascus'),
(3210, 760, 'Rif-dimashq', 'Asia/Damascus'),
(3211, 760, 'Aleppo', 'Asia/Damascus'),
(3212, 760, 'Hama Governorate', 'Asia/Damascus'),
(3213, 760, 'Homs', 'Asia/Damascus'),
(3214, 760, 'Idlib', 'Asia/Damascus'),
(3215, 760, 'Damascus City', 'Asia/Damascus'),
(3216, 760, 'Tartus', 'Asia/Damascus'),
(3217, 762, 'Gorno-Badakhshan', 'Asia/Dushanbe'),
(3218, 762, 'Khatlon', 'Asia/Dushanbe'),
(3219, 762, 'Sughd', 'Asia/Dushanbe'),
(3220, 762, 'Dushanbe', 'Asia/Dushanbe'),
(3221, 762, 'Region of Republican Subordination', 'Asia/Dushanbe'),
(3222, 764, 'Mae Hong Son', 'Asia/Bangkok'),
(3223, 764, 'Chiang Mai', 'Asia/Bangkok'),
(3224, 764, 'Chiang Rai', 'Asia/Bangkok'),
(3225, 764, 'Nan', 'Asia/Bangkok'),
(3226, 764, 'Lamphun', 'Asia/Bangkok'),
(3227, 764, 'Lampang', 'Asia/Bangkok'),
(3228, 764, 'Phrae', 'Asia/Bangkok'),
(3229, 764, 'Tak', 'Asia/Bangkok'),
(3230, 764, 'Sukhothai', 'Asia/Bangkok'),
(3231, 764, 'Uttaradit', 'Asia/Bangkok'),
(3232, 764, 'Kamphaeng Phet', 'Asia/Bangkok'),
(3233, 764, 'Phitsanulok', 'Asia/Bangkok'),
(3234, 764, 'Phichit', 'Asia/Bangkok'),
(3235, 764, 'Phetchabun', 'Asia/Bangkok'),
(3236, 764, 'Uthai Thani', 'Asia/Bangkok'),
(3237, 764, 'Nakhon Sawan', 'Asia/Bangkok'),
(3238, 764, 'Nong Khai', 'Asia/Bangkok'),
(3239, 764, 'Loei', 'Asia/Bangkok'),
(3240, 764, 'Sakon Nakhon', 'Asia/Bangkok'),
(3241, 764, 'Nakhon Phanom', 'Asia/Bangkok'),
(3242, 764, 'Khon Kaen', 'Asia/Bangkok'),
(3243, 764, 'Kalasin', 'Asia/Bangkok'),
(3244, 764, 'Maha Sarakham', 'Asia/Bangkok'),
(3245, 764, 'Roi Et', 'Asia/Bangkok'),
(3246, 764, 'Chaiyaphum', 'Asia/Bangkok'),
(3247, 764, 'Nakhon Ratchasima', 'Asia/Bangkok'),
(3248, 764, 'Buriram', 'Asia/Bangkok'),
(3249, 764, 'Surin', 'Asia/Bangkok'),
(3250, 764, 'Sisaket', 'Asia/Bangkok'),
(3251, 764, 'Narathiwat', 'Asia/Bangkok'),
(3252, 764, 'Chai Nat', 'Asia/Bangkok'),
(3253, 764, 'Sing Buri', 'Asia/Bangkok'),
(3254, 764, 'Lop Buri', 'Asia/Bangkok'),
(3255, 764, 'Ang Thong', 'Asia/Bangkok'),
(3256, 764, 'Phra Nakhon Si Ayutthaya', 'Asia/Bangkok'),
(3257, 764, 'Sara Buri', 'Asia/Bangkok'),
(3258, 764, 'Nonthaburi', 'Asia/Bangkok'),
(3259, 764, 'Pathum Thani', 'Asia/Bangkok'),
(3260, 764, 'Bangkok', 'Asia/Bangkok'),
(3261, 764, 'Phayao', 'Asia/Bangkok'),
(3262, 764, 'Samut Prakan', 'Asia/Bangkok'),
(3263, 764, 'Nakhon Nayok', 'Asia/Bangkok'),
(3264, 764, 'Chachoengsao', 'Asia/Bangkok'),
(3265, 764, 'Chon Buri', 'Asia/Bangkok'),
(3266, 764, 'Rayong', 'Asia/Bangkok'),
(3267, 764, 'Chanthaburi', 'Asia/Bangkok'),
(3268, 764, 'Trat', 'Asia/Bangkok'),
(3269, 764, 'Kanchanaburi', 'Asia/Bangkok'),
(3270, 764, 'Suphan Buri', 'Asia/Bangkok'),
(3271, 764, 'Ratchaburi', 'Asia/Bangkok'),
(3272, 764, 'Nakhon Pathom', 'Asia/Bangkok'),
(3273, 764, 'Samut Songkhram', 'Asia/Bangkok'),
(3274, 764, 'Samut Sakhon', 'Asia/Bangkok'),
(3275, 764, 'Phetchaburi', 'Asia/Bangkok'),
(3276, 764, 'Prachuap Khiri Khan', 'Asia/Bangkok'),
(3277, 764, 'Chumphon', 'Asia/Bangkok'),
(3278, 764, 'Ranong', 'Asia/Bangkok'),
(3279, 764, 'Surat Thani', 'Asia/Bangkok'),
(3280, 764, 'Phangnga', 'Asia/Bangkok'),
(3281, 764, 'Phuket', 'Asia/Bangkok'),
(3282, 764, 'Krabi', 'Asia/Bangkok'),
(3283, 764, 'Nakhon Si Thammarat', 'Asia/Bangkok'),
(3284, 764, 'Trang', 'Asia/Bangkok'),
(3285, 764, 'Phatthalung', 'Asia/Bangkok'),
(3286, 764, 'Satun', 'Asia/Bangkok'),
(3287, 764, 'Songkhla', 'Asia/Bangkok'),
(3288, 764, 'Pattani', 'Asia/Bangkok'),
(3289, 764, 'Yala', 'Asia/Bangkok'),
(3290, 764, 'Yasothon', 'Asia/Bangkok'),
(3291, 764, 'Nakhon Phanom', 'Asia/Bangkok'),
(3292, 764, 'Prachin Buri', 'Asia/Bangkok'),
(3293, 764, 'Ubon Ratchathani', 'Asia/Bangkok'),
(3294, 764, 'Udon Thani', 'Asia/Bangkok'),
(3295, 764, 'Amnat Charoen', 'Asia/Bangkok'),
(3296, 764, 'Mukdahan', 'Asia/Bangkok'),
(3297, 764, 'Nong Bua Lamphu', 'Asia/Bangkok'),
(3298, 764, 'Sa Kaeo', 'Asia/Bangkok'),
(3299, 768, 'Amlame', 'Africa/Lome'),
(3300, 768, 'Aneho', 'Africa/Lome'),
(3301, 768, 'Atakpame', 'Africa/Lome'),
(3302, 768, 'Bafilo', 'Africa/Lome'),
(3303, 768, 'Bassar', 'Africa/Lome'),
(3304, 768, 'Dapaong', 'Africa/Lome'),
(3305, 768, 'Kante', 'Africa/Lome'),
(3306, 768, 'Klouto', 'Africa/Lome'),
(3307, 768, 'Lama-Kara', 'Africa/Lome'),
(3308, 768, 'Lome', 'Africa/Lome'),
(3309, 768, 'Mango', 'Africa/Lome'),
(3310, 768, 'Niamtougou', 'Africa/Lome'),
(3311, 768, 'Notse', 'Africa/Lome'),
(3312, 768, 'Kpagouda', 'Africa/Lome'),
(3313, 768, 'Badou', 'Africa/Lome'),
(3314, 768, 'Sotouboua', 'Africa/Lome'),
(3315, 768, 'Tabligbo', 'Africa/Lome'),
(3316, 768, 'Tsevie', 'Africa/Lome'),
(3317, 768, 'Tchamba', 'Africa/Lome'),
(3318, 768, 'Tchaoudjo', 'Africa/Lome'),
(3319, 768, 'Vogan', 'Africa/Lome'),
(3320, 768, 'Centrale', 'Africa/Lome'),
(3321, 768, 'Kara', 'Africa/Lome'),
(3322, 768, 'Maritime', 'Africa/Lome'),
(3323, 768, 'Plateaux', 'Africa/Lome'),
(3324, 768, 'Savanes', 'Africa/Lome'),
(3325, 772, 'Tokelau', 'Pacific/Fakaofo'),
(3326, 776, 'Ha‘apai', 'Pacific/Tongatapu'),
(3327, 776, 'Tongatapu', 'Pacific/Tongatapu'),
(3328, 776, 'Vava‘u', 'Pacific/Tongatapu'),
(3329, 780, 'Port of Spain', 'America/Port_of_Spain'),
(3330, 780, 'San Fernando', 'America/Port_of_Spain'),
(3331, 780, 'Chaguanas', 'America/Port_of_Spain'),
(3332, 780, 'Arima', 'America/Port_of_Spain'),
(3333, 780, 'Point Fortin Borough', 'America/Port_of_Spain'),
(3334, 780, 'Couva-Tabaquite-Talparo', 'America/Port_of_Spain'),
(3335, 780, 'Diego Martin', 'America/Port_of_Spain'),
(3336, 780, 'Penal-Debe', 'America/Port_of_Spain'),
(3337, 780, 'Princes Town', 'America/Port_of_Spain'),
(3338, 780, 'Rio Claro-Mayaro', 'America/Port_of_Spain'),
(3339, 780, 'San Juan-Laventille', 'America/Port_of_Spain'),
(3340, 780, 'Sangre Grande', 'America/Port_of_Spain'),
(3341, 780, 'Siparia', 'America/Port_of_Spain'),
(3342, 780, 'Tunapuna-Piarco', 'America/Port_of_Spain'),
(3343, 784, 'Abū Z̧aby', 'Asia/Dubai'),
(3344, 784, 'Ajman', 'Asia/Dubai'),
(3345, 784, 'Dubayy', 'Asia/Dubai'),
(3346, 784, 'Al Fujayrah', 'Asia/Dubai'),
(3347, 784, 'Raʼs al Khaymah', 'Asia/Dubai'),
(3348, 784, 'Ash Shāriqah', 'Asia/Dubai'),
(3349, 784, 'Umm al Qaywayn', 'Asia/Dubai'),
(3350, 788, 'Tunis al Janubiyah Wilayat', 'Africa/Tunis'),
(3351, 788, 'Al Qaşrayn', 'Africa/Tunis'),
(3352, 788, 'Al Qayrawān', 'Africa/Tunis'),
(3353, 788, 'Jundūbah', 'Africa/Tunis'),
(3354, 788, 'Kef', 'Africa/Tunis'),
(3355, 788, 'Al Mahdīyah', 'Africa/Tunis'),
(3356, 788, 'Al Munastīr', 'Africa/Tunis'),
(3357, 788, 'Bājah', 'Africa/Tunis'),
(3358, 788, 'Banzart', 'Africa/Tunis'),
(3359, 788, 'Nābul', 'Africa/Tunis'),
(3360, 788, 'Silyānah', 'Africa/Tunis'),
(3361, 788, 'Sūsah', 'Africa/Tunis'),
(3362, 788, 'Bin ‘Arūs', 'Africa/Tunis'),
(3363, 788, 'Madanīn', 'Africa/Tunis'),
(3364, 788, 'Qābis', 'Africa/Tunis'),
(3365, 788, 'Qafşah', 'Africa/Tunis'),
(3366, 788, 'Qibilī', 'Africa/Tunis'),
(3367, 788, 'Şafāqis', 'Africa/Tunis'),
(3368, 788, 'Sīdī Bū Zayd', 'Africa/Tunis'),
(3369, 788, 'Taţāwīn', 'Africa/Tunis'),
(3370, 788, 'Tawzar', 'Africa/Tunis'),
(3371, 788, 'Tūnis', 'Africa/Tunis'),
(3372, 788, 'Zaghwān', 'Africa/Tunis'),
(3373, 788, 'Ariana', 'Africa/Tunis'),
(3374, 788, 'Manouba', 'Africa/Tunis'),
(3375, 792, 'Adıyaman', 'Europe/Istanbul'),
(3376, 792, 'Afyonkarahisar', 'Europe/Istanbul'),
(3377, 792, 'Ağrı Province', 'Europe/Istanbul'),
(3378, 792, 'Amasya Province', 'Europe/Istanbul'),
(3379, 792, 'Antalya Province', 'Europe/Istanbul'),
(3380, 792, 'Artvin Province', 'Europe/Istanbul'),
(3381, 792, 'Aydın Province', 'Europe/Istanbul'),
(3382, 792, 'Balıkesir Province', 'Europe/Istanbul'),
(3383, 792, 'Bilecik Province', 'Europe/Istanbul'),
(3384, 792, 'Bingöl Province', 'Europe/Istanbul'),
(3385, 792, 'Bitlis Province', 'Europe/Istanbul'),
(3386, 792, 'Bolu Province', 'Europe/Istanbul'),
(3387, 792, 'Burdur Province', 'Europe/Istanbul'),
(3388, 792, 'Bursa', 'Europe/Istanbul'),
(3389, 792, 'Çanakkale Province', 'Europe/Istanbul'),
(3390, 792, 'Çorum Province', 'Europe/Istanbul'),
(3391, 792, 'Denizli Province', 'Europe/Istanbul'),
(3392, 792, 'Diyarbakır', 'Europe/Istanbul'),
(3393, 792, 'Edirne Province', 'Europe/Istanbul'),
(3394, 792, 'Elazığ', 'Europe/Istanbul'),
(3395, 792, 'Erzincan Province', 'Europe/Istanbul'),
(3396, 792, 'Erzurum Province', 'Europe/Istanbul'),
(3397, 792, 'Eskişehir Province', 'Europe/Istanbul'),
(3398, 792, 'Giresun Province', 'Europe/Istanbul'),
(3399, 792, 'Hatay Province', 'Europe/Istanbul'),
(3400, 792, 'Mersin Province', 'Europe/Istanbul'),
(3401, 792, 'Isparta Province', 'Europe/Istanbul'),
(3402, 792, 'Istanbul', 'Europe/Istanbul'),
(3403, 792, 'İzmir', 'Europe/Istanbul'),
(3404, 792, 'Kastamonu Province', 'Europe/Istanbul'),
(3405, 792, 'Kayseri Province', 'Europe/Istanbul'),
(3406, 792, 'Kırklareli Province', 'Europe/Istanbul'),
(3407, 792, 'Kırşehir Province', 'Europe/Istanbul'),
(3408, 792, 'Kocaeli Province', 'Europe/Istanbul'),
(3409, 792, 'Kütahya Province', 'Europe/Istanbul'),
(3410, 792, 'Malatya Province', 'Europe/Istanbul'),
(3411, 792, 'Manisa Province', 'Europe/Istanbul'),
(3412, 792, 'Kahramanmaraş Province', 'Europe/Istanbul'),
(3413, 792, 'Muğla Province', 'Europe/Istanbul'),
(3414, 792, 'Muş Province', 'Europe/Istanbul'),
(3415, 792, 'Nevşehir', 'Europe/Istanbul'),
(3416, 792, 'Ordu', 'Europe/Istanbul'),
(3417, 792, 'Rize', 'Europe/Istanbul'),
(3418, 792, 'Sakarya Province', 'Europe/Istanbul'),
(3419, 792, 'Samsun Province', 'Europe/Istanbul'),
(3420, 792, 'Sinop Province', 'Europe/Istanbul'),
(3421, 792, 'Sivas Province', 'Europe/Istanbul'),
(3422, 792, 'Tekirdağ Province', 'Europe/Istanbul'),
(3423, 792, 'Tokat', 'Europe/Istanbul'),
(3424, 792, 'Trabzon Province', 'Europe/Istanbul'),
(3425, 792, 'Tunceli Province', 'Europe/Istanbul'),
(3426, 792, 'Şanlıurfa Province', 'Europe/Istanbul'),
(3427, 792, 'Uşak Province', 'Europe/Istanbul'),
(3428, 792, 'Van Province', 'Europe/Istanbul'),
(3429, 792, 'Yozgat Province', 'Europe/Istanbul'),
(3430, 792, 'Ankara Province', 'Europe/Istanbul'),
(3431, 792, 'Gümüşhane', 'Europe/Istanbul'),
(3432, 792, 'Hakkâri Province', 'Europe/Istanbul'),
(3433, 792, 'Konya Province', 'Europe/Istanbul'),
(3434, 792, 'Mardin Province', 'Europe/Istanbul'),
(3435, 792, 'Niğde', 'Europe/Istanbul'),
(3436, 792, 'Siirt Province', 'Europe/Istanbul'),
(3437, 792, 'Aksaray Province', 'Europe/Istanbul'),
(3438, 792, 'Batman Province', 'Europe/Istanbul'),
(3439, 792, 'Bayburt', 'Europe/Istanbul'),
(3440, 792, 'Karaman Province', 'Europe/Istanbul'),
(3441, 792, 'Kırıkkale Province', 'Europe/Istanbul'),
(3442, 792, 'Şırnak Province', 'Europe/Istanbul'),
(3443, 792, 'Adana Province', 'Europe/Istanbul'),
(3444, 792, 'Çankırı Province', 'Europe/Istanbul'),
(3445, 792, 'Gaziantep Province', 'Europe/Istanbul'),
(3446, 792, 'Kars', 'Europe/Istanbul'),
(3447, 792, 'Zonguldak', 'Europe/Istanbul'),
(3448, 792, 'Ardahan Province', 'Europe/Istanbul'),
(3449, 792, 'Bartın Province', 'Europe/Istanbul'),
(3450, 792, 'Iğdır Province', 'Europe/Istanbul'),
(3451, 792, 'Karabük', 'Europe/Istanbul'),
(3452, 792, 'Kilis Province', 'Europe/Istanbul'),
(3453, 792, 'Osmaniye Province', 'Europe/Istanbul'),
(3454, 792, 'Yalova Province', 'Europe/Istanbul'),
(3455, 792, 'Düzce', 'Europe/Istanbul'),
(3456, 795, 'Ahal', 'Asia/Ashgabat'),
(3457, 795, 'Balkan', 'Asia/Ashgabat'),
(3458, 795, 'Daşoguz', 'Asia/Ashgabat'),
(3459, 795, 'Lebap', 'Asia/Ashgabat'),
(3460, 795, 'Mary', 'Asia/Ashgabat'),
(3461, 796, 'Turks and Caicos Islands', 'America/Grand_Turk'),
(3462, 798, 'Tuvalu', 'Pacific/Funafuti'),
(3463, 800, 'Eastern Region', 'Africa/Kampala'),
(3464, 800, 'Northern Region', 'Africa/Kampala'),
(3465, 800, 'Central Region', 'Africa/Kampala'),
(3466, 800, 'Western Region', 'Africa/Kampala'),
(3467, 804, 'Cherkasʼka Oblastʼ', 'Europe/Kiev'),
(3468, 804, 'Chernihivsʼka Oblastʼ', 'Europe/Kiev'),
(3469, 804, 'Chernivetsʼka Oblastʼ', 'Europe/Kiev'),
(3470, 804, 'Dnipropetrovsʼka Oblastʼ', 'Europe/Kiev'),
(3471, 804, 'Donetsʼka Oblastʼ', 'Europe/Kiev'),
(3472, 804, 'Ivano-Frankivsʼka Oblastʼ', 'Europe/Kiev'),
(3473, 804, 'Kharkivsʼka Oblastʼ', 'Europe/Kiev'),
(3474, 804, 'Kherson Oblast', 'Europe/Kiev'),
(3475, 804, 'Khmelʼnytsʼka Oblastʼ', 'Europe/Kiev'),
(3476, 804, 'Kirovohradsʼka Oblastʼ', 'Europe/Kiev'),
(3477, 804, 'Avtonomna Respublika Krym', 'Europe/Kiev'),
(3478, 804, 'Misto Kyyiv', 'Europe/Kiev'),
(3479, 804, 'Kiev Oblast', 'Europe/Kiev');
INSERT INTO `country_state` (`id`, `country_id`, `name`, `timezone`) VALUES
(3480, 804, 'Luhansʼka Oblastʼ', 'Europe/Kiev'),
(3481, 804, 'Lʼvivsʼka Oblastʼ', 'Europe/Kiev'),
(3482, 804, 'Mykolayivsʼka Oblastʼ', 'Europe/Kiev'),
(3483, 804, 'Odessa Oblast', 'Europe/Kiev'),
(3484, 804, 'Poltava Oblast', 'Europe/Kiev'),
(3485, 804, 'Rivnensʼka Oblastʼ', 'Europe/Kiev'),
(3486, 804, 'Misto Sevastopol', 'Europe/Kiev'),
(3487, 804, 'Sumy Oblast', 'Europe/Kiev'),
(3488, 804, 'Ternopilʼsʼka Oblastʼ', 'Europe/Kiev'),
(3489, 804, 'Vinnytsʼka Oblastʼ', 'Europe/Kiev'),
(3490, 804, 'Volynsʼka Oblastʼ', 'Europe/Kiev'),
(3491, 804, 'Zakarpatsʼka Oblastʼ', 'Europe/Kiev'),
(3492, 804, 'Zaporizʼka Oblastʼ', 'Europe/Kiev'),
(3493, 804, 'Zhytomyrsʼka Oblastʼ', 'Europe/Kiev'),
(3494, 807, 'Macedonia, The Former Yugoslav Republic of', 'Europe/Skopje'),
(3495, 807, 'Aračinovo', 'Europe/Skopje'),
(3496, 807, 'Bač', 'Europe/Skopje'),
(3497, 807, 'Belčišta', 'Europe/Skopje'),
(3498, 807, 'Berovo', 'Europe/Skopje'),
(3499, 807, 'Bistrica', 'Europe/Skopje'),
(3500, 807, 'Bitola', 'Europe/Skopje'),
(3501, 807, 'Blatec', 'Europe/Skopje'),
(3502, 807, 'Bogdanci', 'Europe/Skopje'),
(3503, 807, 'Opstina Bogomila', 'Europe/Skopje'),
(3504, 807, 'Bogovinje', 'Europe/Skopje'),
(3505, 807, 'Bosilovo', 'Europe/Skopje'),
(3506, 807, 'Brvenica', 'Europe/Skopje'),
(3507, 807, 'Čair', 'Europe/Skopje'),
(3508, 807, 'Capari', 'Europe/Skopje'),
(3509, 807, 'Čaška', 'Europe/Skopje'),
(3510, 807, 'Čegrana', 'Europe/Skopje'),
(3511, 807, 'Centar', 'Europe/Skopje'),
(3512, 807, 'Centar Župa', 'Europe/Skopje'),
(3513, 807, 'Češinovo', 'Europe/Skopje'),
(3514, 807, 'Čučer-Sandevo', 'Europe/Skopje'),
(3515, 807, 'Debar', 'Europe/Skopje'),
(3516, 807, 'Delčevo', 'Europe/Skopje'),
(3517, 807, 'Delogoždi', 'Europe/Skopje'),
(3518, 807, 'Demir Hisar', 'Europe/Skopje'),
(3519, 807, 'Demir Kapija', 'Europe/Skopje'),
(3520, 807, 'Dobruševo', 'Europe/Skopje'),
(3521, 807, 'Dolna Banjica', 'Europe/Skopje'),
(3522, 807, 'Dolneni', 'Europe/Skopje'),
(3523, 807, 'Opstina Gjorce Petrov', 'Europe/Skopje'),
(3524, 807, 'Drugovo', 'Europe/Skopje'),
(3525, 807, 'Džepčište', 'Europe/Skopje'),
(3526, 807, 'Gazi Baba', 'Europe/Skopje'),
(3527, 807, 'Gevgelija', 'Europe/Skopje'),
(3528, 807, 'Gostivar', 'Europe/Skopje'),
(3529, 807, 'Gradsko', 'Europe/Skopje'),
(3530, 807, 'Ilinden', 'Europe/Skopje'),
(3531, 807, 'Izvor', 'Europe/Skopje'),
(3532, 807, 'Jegunovce', 'Europe/Skopje'),
(3533, 807, 'Kamenjane', 'Europe/Skopje'),
(3534, 807, 'Karbinci', 'Europe/Skopje'),
(3535, 807, 'Karpoš', 'Europe/Skopje'),
(3536, 807, 'Kavadarci', 'Europe/Skopje'),
(3537, 807, 'Kičevo', 'Europe/Skopje'),
(3538, 807, 'Kisela Voda', 'Europe/Skopje'),
(3539, 807, 'Klečevce', 'Europe/Skopje'),
(3540, 807, 'Kočani', 'Europe/Skopje'),
(3541, 807, 'Konče', 'Europe/Skopje'),
(3542, 807, 'Kondovo', 'Europe/Skopje'),
(3543, 807, 'Konopište', 'Europe/Skopje'),
(3544, 807, 'Kosel', 'Europe/Skopje'),
(3545, 807, 'Kratovo', 'Europe/Skopje'),
(3546, 807, 'Kriva Palanka', 'Europe/Skopje'),
(3547, 807, 'Krivogaštani', 'Europe/Skopje'),
(3548, 807, 'Kruševo', 'Europe/Skopje'),
(3549, 807, 'Kukliš', 'Europe/Skopje'),
(3550, 807, 'Kukurečani', 'Europe/Skopje'),
(3551, 807, 'Kumanovo', 'Europe/Skopje'),
(3552, 807, 'Labuništa', 'Europe/Skopje'),
(3553, 807, 'Opstina Lipkovo', 'Europe/Skopje'),
(3554, 807, 'Lozovo', 'Europe/Skopje'),
(3555, 807, 'Lukovo', 'Europe/Skopje'),
(3556, 807, 'Makedonska Kamenica', 'Europe/Skopje'),
(3557, 807, 'Makedonski Brod', 'Europe/Skopje'),
(3558, 807, 'Mavrovi Anovi', 'Europe/Skopje'),
(3559, 807, 'Mešeišta', 'Europe/Skopje'),
(3560, 807, 'Miravci', 'Europe/Skopje'),
(3561, 807, 'Mogila', 'Europe/Skopje'),
(3562, 807, 'Murtino', 'Europe/Skopje'),
(3563, 807, 'Negotino', 'Europe/Skopje'),
(3564, 807, 'Negotino-Pološko', 'Europe/Skopje'),
(3565, 807, 'Novaci', 'Europe/Skopje'),
(3566, 807, 'Novo Selo', 'Europe/Skopje'),
(3567, 807, 'Obleševo', 'Europe/Skopje'),
(3568, 807, 'Ohrid', 'Europe/Skopje'),
(3569, 807, 'Orašac', 'Europe/Skopje'),
(3570, 807, 'Orizari', 'Europe/Skopje'),
(3571, 807, 'Oslomej', 'Europe/Skopje'),
(3572, 807, 'Pehčevo', 'Europe/Skopje'),
(3573, 807, 'Petrovec', 'Europe/Skopje'),
(3574, 807, 'Plasnica', 'Europe/Skopje'),
(3575, 807, 'Podareš', 'Europe/Skopje'),
(3576, 807, 'Prilep', 'Europe/Skopje'),
(3577, 807, 'Probištip', 'Europe/Skopje'),
(3578, 807, 'Radoviš', 'Europe/Skopje'),
(3579, 807, 'Opstina Rankovce', 'Europe/Skopje'),
(3580, 807, 'Resen', 'Europe/Skopje'),
(3581, 807, 'Rosoman', 'Europe/Skopje'),
(3582, 807, 'Opština Rostuša', 'Europe/Skopje'),
(3583, 807, 'Samokov', 'Europe/Skopje'),
(3584, 807, 'Saraj', 'Europe/Skopje'),
(3585, 807, 'Šipkovica', 'Europe/Skopje'),
(3586, 807, 'Sopište', 'Europe/Skopje'),
(3587, 807, 'Sopotnica', 'Europe/Skopje'),
(3588, 807, 'Srbinovo', 'Europe/Skopje'),
(3589, 807, 'Staravina', 'Europe/Skopje'),
(3590, 807, 'Star Dojran', 'Europe/Skopje'),
(3591, 807, 'Staro Nagoričane', 'Europe/Skopje'),
(3592, 807, 'Štip', 'Europe/Skopje'),
(3593, 807, 'Struga', 'Europe/Skopje'),
(3594, 807, 'Strumica', 'Europe/Skopje'),
(3595, 807, 'Studeničani', 'Europe/Skopje'),
(3596, 807, 'Šuto Orizari', 'Europe/Skopje'),
(3597, 807, 'Sveti Nikole', 'Europe/Skopje'),
(3598, 807, 'Tearce', 'Europe/Skopje'),
(3599, 807, 'Tetovo', 'Europe/Skopje'),
(3600, 807, 'Topolčani', 'Europe/Skopje'),
(3601, 807, 'Valandovo', 'Europe/Skopje'),
(3602, 807, 'Vasilevo', 'Europe/Skopje'),
(3603, 807, 'Veles', 'Europe/Skopje'),
(3604, 807, 'Velešta', 'Europe/Skopje'),
(3605, 807, 'Vevčani', 'Europe/Skopje'),
(3606, 807, 'Vinica', 'Europe/Skopje'),
(3607, 807, 'Vitolište', 'Europe/Skopje'),
(3608, 807, 'Vraneštica', 'Europe/Skopje'),
(3609, 807, 'Vrapčište', 'Europe/Skopje'),
(3610, 807, 'Vratnica', 'Europe/Skopje'),
(3611, 807, 'Vrutok', 'Europe/Skopje'),
(3612, 807, 'Zajas', 'Europe/Skopje'),
(3613, 807, 'Zelenikovo', 'Europe/Skopje'),
(3614, 807, 'Želino', 'Europe/Skopje'),
(3615, 807, 'Žitoše', 'Europe/Skopje'),
(3616, 807, 'Zletovo', 'Europe/Skopje'),
(3617, 807, 'Zrnovci', 'Europe/Skopje'),
(3618, 818, 'Ad Daqahlīyah', 'Africa/Cairo'),
(3619, 818, 'Al Baḩr al Aḩmar', 'Africa/Cairo'),
(3620, 818, 'Al Buḩayrah', 'Africa/Cairo'),
(3621, 818, 'Al Fayyūm', 'Africa/Cairo'),
(3622, 818, 'Al Gharbīyah', 'Africa/Cairo'),
(3623, 818, 'Alexandria', 'Africa/Cairo'),
(3624, 818, 'Al Ismā‘īlīyah', 'Africa/Cairo'),
(3625, 818, 'Al Jīzah', 'Africa/Cairo'),
(3626, 818, 'Al Minūfīyah', 'Africa/Cairo'),
(3627, 818, 'Al Minyā', 'Africa/Cairo'),
(3628, 818, 'Al Qāhirah', 'Africa/Cairo'),
(3629, 818, 'Al Qalyūbīyah', 'Africa/Cairo'),
(3630, 818, 'Al Wādī al Jadīd', 'Africa/Cairo'),
(3631, 818, 'Eastern Province', 'Africa/Cairo'),
(3632, 818, 'As Suways', 'Africa/Cairo'),
(3633, 818, 'Aswān', 'Africa/Cairo'),
(3634, 818, 'Asyūţ', 'Africa/Cairo'),
(3635, 818, 'Banī Suwayf', 'Africa/Cairo'),
(3636, 818, 'Būr Sa‘īd', 'Africa/Cairo'),
(3637, 818, 'Dumyāţ', 'Africa/Cairo'),
(3638, 818, 'Kafr ash Shaykh', 'Africa/Cairo'),
(3639, 818, 'Maţrūḩ', 'Africa/Cairo'),
(3640, 818, 'Qinā', 'Africa/Cairo'),
(3641, 818, 'Sūhāj', 'Africa/Cairo'),
(3642, 818, 'Janūb Sīnāʼ', 'Africa/Cairo'),
(3643, 818, 'Shamāl Sīnāʼ', 'Africa/Cairo'),
(3644, 818, 'Luxor', 'Africa/Cairo'),
(3645, 818, 'Helwan', 'Africa/Cairo'),
(3646, 818, '6th of October', 'Africa/Cairo'),
(3647, 826, 'England', 'Europe/London'),
(3648, 826, 'Northern Ireland', 'Europe/London'),
(3649, 826, 'Scotland', 'Europe/London'),
(3650, 826, 'Wales', 'Europe/London'),
(3651, 831, 'Guernsey', 'Europe/Guernsey'),
(3652, 833, 'Isle of Man', 'Europe/Isle_of_Man'),
(3653, 834, 'Arusha', 'Africa/Dar_es_Salaam'),
(3654, 834, 'Pwani', 'Africa/Dar_es_Salaam'),
(3655, 834, 'Dodoma', 'Africa/Dar_es_Salaam'),
(3656, 834, 'Iringa', 'Africa/Dar_es_Salaam'),
(3657, 834, 'Kigoma', 'Africa/Dar_es_Salaam'),
(3658, 834, 'Kilimanjaro', 'Africa/Dar_es_Salaam'),
(3659, 834, 'Lindi', 'Africa/Dar_es_Salaam'),
(3660, 834, 'Mara', 'Africa/Dar_es_Salaam'),
(3661, 834, 'Mbeya', 'Africa/Dar_es_Salaam'),
(3662, 834, 'Morogoro', 'Africa/Dar_es_Salaam'),
(3663, 834, 'Mtwara', 'Africa/Dar_es_Salaam'),
(3664, 834, 'Mwanza', 'Africa/Dar_es_Salaam'),
(3665, 834, 'Pemba North', 'Africa/Dar_es_Salaam'),
(3666, 834, 'Ruvuma', 'Africa/Dar_es_Salaam'),
(3667, 834, 'Shinyanga', 'Africa/Dar_es_Salaam'),
(3668, 834, 'Singida', 'Africa/Dar_es_Salaam'),
(3669, 834, 'Tabora', 'Africa/Dar_es_Salaam'),
(3670, 834, 'Tanga', 'Africa/Dar_es_Salaam'),
(3671, 834, 'Kagera', 'Africa/Dar_es_Salaam'),
(3672, 834, 'Pemba South', 'Africa/Dar_es_Salaam'),
(3673, 834, 'Zanzibar Central/South', 'Africa/Dar_es_Salaam'),
(3674, 834, 'Zanzibar North', 'Africa/Dar_es_Salaam'),
(3675, 834, 'Dar es Salaam', 'Africa/Dar_es_Salaam'),
(3676, 834, 'Rukwa', 'Africa/Dar_es_Salaam'),
(3677, 834, 'Zanzibar Urban/West', 'Africa/Dar_es_Salaam'),
(3678, 834, 'Arusha', 'Africa/Dar_es_Salaam'),
(3679, 834, 'Manyara', 'Africa/Dar_es_Salaam'),
(3680, 840, 'Alaska', 'America/Anchorage'),
(3681, 840, 'Alabama', 'America/Chicago'),
(3682, 840, 'Arkansas', 'America/Chicago'),
(3683, 840, 'Arizona', 'America/Phoenix'),
(3684, 840, 'California', 'America/Los_Angeles'),
(3685, 840, 'Colorado', 'America/Denver'),
(3686, 840, 'Connecticut', 'America/New_York'),
(3687, 840, 'District of Columbia', 'America/New_York'),
(3688, 840, 'Delaware', 'America/New_York'),
(3689, 840, 'Florida', 'America/New_York'),
(3690, 840, 'Georgia', 'America/New_York'),
(3691, 840, 'Hawaii', 'Pacific/Honolulu'),
(3692, 840, 'Iowa', 'America/Chicago'),
(3693, 840, 'Idaho', 'America/Denver'),
(3694, 840, 'Illinois', 'America/Chicago'),
(3695, 840, 'Indiana', 'America/New_York'),
(3696, 840, 'Kansas', 'America/Chicago'),
(3697, 840, 'Kentucky', 'America/New_York'),
(3698, 840, 'Louisiana', 'America/Chicago'),
(3699, 840, 'Massachusetts', 'America/New_York'),
(3700, 840, 'Maryland', 'America/New_York'),
(3701, 840, 'Maine', 'America/New_York'),
(3702, 840, 'Michigan', 'America/New_York'),
(3703, 840, 'Minnesota', 'America/Chicago'),
(3704, 840, 'Missouri', 'America/Chicago'),
(3705, 840, 'Mississippi', 'America/Chicago'),
(3706, 840, 'Montana', 'America/Denver'),
(3707, 840, 'North Carolina', 'America/New_York'),
(3708, 840, 'North Dakota', 'America/Chicago'),
(3709, 840, 'Nebraska', 'America/Chicago'),
(3710, 840, 'New Hampshire', 'America/New_York'),
(3711, 840, 'New Jersey', 'America/New_York'),
(3712, 840, 'New Mexico', 'America/Denver'),
(3713, 840, 'Nevada', 'America/Los_Angeles'),
(3714, 840, 'New York', 'America/New_York'),
(3715, 840, 'Ohio', 'America/New_York'),
(3716, 840, 'Oklahoma', 'America/Chicago'),
(3717, 840, 'Oregon', 'America/Los_Angeles'),
(3718, 840, 'Pennsylvania', 'America/New_York'),
(3719, 840, 'Rhode Island', 'America/New_York'),
(3720, 840, 'South Carolina', 'America/New_York'),
(3721, 840, 'South Dakota', 'America/Chicago'),
(3722, 840, 'Tennessee', 'America/Chicago'),
(3723, 840, 'Texas', 'America/Chicago'),
(3724, 840, 'Utah', 'America/Denver'),
(3725, 840, 'Virginia', 'America/New_York'),
(3726, 840, 'Vermont', 'America/New_York'),
(3727, 840, 'Washington', 'America/Los_Angeles'),
(3728, 840, 'Wisconsin', 'America/Chicago'),
(3729, 840, 'West Virginia', 'America/New_York'),
(3730, 840, 'Wyoming', 'America/Denver'),
(3731, 850, 'Virgin Islands', 'America/St_Thomas'),
(3732, 854, 'Boucle du Mouhoun', 'Africa/Ouagadougou'),
(3733, 854, 'Cascades', 'Africa/Ouagadougou'),
(3734, 854, 'Centre', 'Africa/Ouagadougou'),
(3735, 854, 'Centre-Est', 'Africa/Ouagadougou'),
(3736, 854, 'Centre-Nord', 'Africa/Ouagadougou'),
(3737, 854, 'Centre-Ouest', 'Africa/Ouagadougou'),
(3738, 854, 'Centre-Sud', 'Africa/Ouagadougou'),
(3739, 854, 'Est', 'Africa/Ouagadougou'),
(3740, 854, 'Hauts-Bassins', 'Africa/Ouagadougou'),
(3741, 854, 'Nord', 'Africa/Ouagadougou'),
(3742, 854, 'Plateau-Central', 'Africa/Ouagadougou'),
(3743, 854, 'Sahel', 'Africa/Ouagadougou'),
(3744, 854, 'Sud-Ouest', 'Africa/Ouagadougou'),
(3745, 855, 'Komuna e Deçanit', 'Europe/Belgrade'),
(3746, 855, 'Komuna e Dragashit', 'Europe/Belgrade'),
(3747, 855, 'Komuna e Ferizajt', 'Europe/Belgrade'),
(3748, 855, 'Komuna e Fushë Kosovës', 'Europe/Belgrade'),
(3749, 855, 'Komuna e Gjakovës', 'Europe/Belgrade'),
(3750, 855, 'Komuna e Gjilanit', 'Europe/Belgrade'),
(3751, 855, 'Komuna e Drenasit', 'Europe/Belgrade'),
(3752, 855, 'Komuna e Istogut', 'Europe/Belgrade'),
(3753, 855, 'Komuna e Kaçanikut', 'Europe/Belgrade'),
(3754, 855, 'Komuna e Kamenicës', 'Europe/Belgrade'),
(3755, 855, 'Komuna e Klinës', 'Europe/Belgrade'),
(3756, 855, 'Komuna e Leposaviqit', 'Europe/Belgrade'),
(3757, 855, 'Komuna e Lipjanit', 'Europe/Belgrade'),
(3758, 855, 'Komuna e Malisheves', 'Europe/Belgrade'),
(3759, 855, 'Komuna e Mitrovicës', 'Europe/Belgrade'),
(3760, 855, 'Komuna e Novobërdës', 'Europe/Belgrade'),
(3761, 855, 'Komuna e Obiliqit', 'Europe/Belgrade'),
(3762, 855, 'Komuna e Pejës', 'Europe/Belgrade'),
(3763, 855, 'Komuna e Podujevës', 'Europe/Belgrade'),
(3764, 855, 'Komuna e Prishtinës', 'Europe/Belgrade'),
(3765, 855, 'Komuna e Prizrenit', 'Europe/Belgrade'),
(3766, 855, 'Komuna e Rahovecit', 'Europe/Belgrade'),
(3767, 855, 'Komuna e Shtërpcës', 'Europe/Belgrade'),
(3768, 855, 'Komuna e Shtimes', 'Europe/Belgrade'),
(3769, 855, 'Komuna e Skenderajt', 'Europe/Belgrade'),
(3770, 855, 'Komuna e Thërandës', 'Europe/Belgrade'),
(3771, 855, 'Komuna e Vitisë', 'Europe/Belgrade'),
(3772, 855, 'Komuna e Vushtrrisë', 'Europe/Belgrade'),
(3773, 855, 'Komuna e Zubin Potokut', 'Europe/Belgrade'),
(3774, 855, 'Komuna e Zveçanit', 'Europe/Belgrade'),
(3775, 858, 'Artigas Department', 'America/Montevideo'),
(3776, 858, 'Canelones Department', 'America/Montevideo'),
(3777, 858, 'Cerro Largo Department', 'America/Montevideo'),
(3778, 858, 'Colonia Department', 'America/Montevideo'),
(3779, 858, 'Durazno', 'America/Montevideo'),
(3780, 858, 'Flores', 'America/Montevideo'),
(3781, 858, 'Florida Department', 'America/Montevideo'),
(3782, 858, 'Lavalleja Department', 'America/Montevideo'),
(3783, 858, 'Maldonado Department', 'America/Montevideo'),
(3784, 858, 'Montevideo', 'America/Montevideo'),
(3785, 858, 'Paysandú', 'America/Montevideo'),
(3786, 858, 'Río Negro', 'America/Montevideo'),
(3787, 858, 'Rivera', 'America/Montevideo'),
(3788, 858, 'Rocha', 'America/Montevideo'),
(3789, 858, 'Salto', 'America/Montevideo'),
(3790, 858, 'San José', 'America/Montevideo'),
(3791, 858, 'Soriano Department', 'America/Montevideo'),
(3792, 858, 'Tacuarembó', 'America/Montevideo'),
(3793, 858, 'Treinta y Tres', 'America/Montevideo'),
(3794, 860, 'Andijon', 'Asia/Samarkand'),
(3795, 860, 'Buxoro', 'Asia/Samarkand'),
(3796, 860, 'Farg ona', 'Asia/Samarkand'),
(3797, 860, 'Xorazm', 'Asia/Samarkand'),
(3798, 860, 'Namangan', 'Asia/Samarkand'),
(3799, 860, 'Navoiy', 'Asia/Samarkand'),
(3800, 860, 'Qashqadaryo', 'Asia/Samarkand'),
(3801, 860, 'Karakalpakstan', 'Asia/Samarkand'),
(3802, 860, 'Samarqand', 'Asia/Samarkand'),
(3803, 860, 'Surxondaryo', 'Asia/Samarkand'),
(3804, 860, 'Toshkent Shahri', 'Asia/Samarkand'),
(3805, 860, 'Toshkent', 'Asia/Samarkand'),
(3806, 860, 'Jizzax', 'Asia/Samarkand'),
(3807, 860, 'Sirdaryo', 'Asia/Samarkand'),
(3808, 862, 'Amazonas', 'America/Caracas'),
(3809, 862, 'Anzoátegui', 'America/Caracas'),
(3810, 862, 'Apure', 'America/Caracas'),
(3811, 862, 'Aragua', 'America/Caracas'),
(3812, 862, 'Barinas', 'America/Caracas'),
(3813, 862, 'Bolívar', 'America/Caracas'),
(3814, 862, 'Carabobo', 'America/Caracas'),
(3815, 862, 'Cojedes', 'America/Caracas'),
(3816, 862, 'Delta Amacuro', 'America/Caracas'),
(3817, 862, 'Distrito Federal', 'America/Caracas'),
(3818, 862, 'Falcón', 'America/Caracas'),
(3819, 862, 'Guárico', 'America/Caracas'),
(3820, 862, 'Lara', 'America/Caracas'),
(3821, 862, 'Mérida', 'America/Caracas'),
(3822, 862, 'Miranda', 'America/Caracas'),
(3823, 862, 'Monagas', 'America/Caracas'),
(3824, 862, 'Isla Margarita', 'America/Caracas'),
(3825, 862, 'Portuguesa', 'America/Caracas'),
(3826, 862, 'Sucre', 'America/Caracas'),
(3827, 862, 'Táchira', 'America/Caracas'),
(3828, 862, 'Trujillo', 'America/Caracas'),
(3829, 862, 'Yaracuy', 'America/Caracas'),
(3830, 862, 'Zulia', 'America/Caracas'),
(3831, 862, 'Dependencias Federales', 'America/Caracas'),
(3832, 862, 'Distrito Capital', 'America/Caracas'),
(3833, 862, 'Vargas', 'America/Caracas'),
(3834, 882, 'A‘ana', 'Pacific/Apia'),
(3835, 882, 'Aiga-i-le-Tai', 'Pacific/Apia'),
(3836, 882, 'Atua', 'Pacific/Apia'),
(3837, 882, 'Fa‘asaleleaga', 'Pacific/Apia'),
(3838, 882, 'Gaga‘emauga', 'Pacific/Apia'),
(3839, 882, 'Va‘a-o-Fonoti', 'Pacific/Apia'),
(3840, 882, 'Gagaifomauga', 'Pacific/Apia'),
(3841, 882, 'Palauli', 'Pacific/Apia'),
(3842, 882, 'Satupa‘itea', 'Pacific/Apia'),
(3843, 882, 'Tuamasaga', 'Pacific/Apia'),
(3844, 882, 'Vaisigano', 'Pacific/Apia'),
(3845, 887, 'Abyan', 'Asia/Aden'),
(3846, 887, '‘Adan', 'Asia/Aden'),
(3847, 887, 'Al Mahrah', 'Asia/Aden'),
(3848, 887, 'Ḩaḑramawt', 'Asia/Aden'),
(3849, 887, 'Shabwah', 'Asia/Aden'),
(3850, 887, 'San’a’', 'Asia/Aden'),
(3851, 887, 'Ta’izz', 'Asia/Aden'),
(3852, 887, 'Al Ḩudaydah', 'Asia/Aden'),
(3853, 887, 'Dhamar', 'Asia/Aden'),
(3854, 887, 'Al Maḩwīt', 'Asia/Aden'),
(3855, 887, 'Dhamār', 'Asia/Aden'),
(3856, 887, 'Maʼrib', 'Asia/Aden'),
(3857, 887, 'Şa‘dah', 'Asia/Aden'),
(3858, 887, 'Şan‘āʼ', 'Asia/Aden'),
(3859, 887, 'Aḑ Ḑāli‘', 'Asia/Aden'),
(3860, 887, 'Omran', 'Asia/Aden'),
(3861, 887, 'Al Bayḑāʼ', 'Asia/Aden'),
(3862, 887, 'Al Jawf', 'Asia/Aden'),
(3863, 887, 'Ḩajjah', 'Asia/Aden'),
(3864, 887, 'Ibb', 'Asia/Aden'),
(3865, 887, 'Laḩij', 'Asia/Aden'),
(3866, 887, 'Ta‘izz', 'Asia/Aden'),
(3867, 887, 'Amanat Al Asimah', 'Asia/Aden'),
(3868, 887, 'Muḩāfaz̧at Raymah', 'Asia/Aden'),
(3869, 891, 'Crna Gora (Montenegro)', 'Europe/Podgorica'),
(3870, 891, 'Srbija (Serbia)', 'Europe/Podgorica'),
(3871, 894, 'North-Western', 'Africa/Lusaka'),
(3872, 894, 'Copperbelt', 'Africa/Lusaka'),
(3873, 894, 'Western', 'Africa/Lusaka'),
(3874, 894, 'Southern', 'Africa/Lusaka'),
(3875, 894, 'Central', 'Africa/Lusaka'),
(3876, 894, 'Eastern', 'Africa/Lusaka'),
(3877, 894, 'Northern', 'Africa/Lusaka'),
(3878, 894, 'Luapula', 'Africa/Lusaka'),
(3879, 894, 'Lusaka', 'Africa/Lusaka');

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE IF NOT EXISTS `coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `coupon` varchar(255) DEFAULT NULL,
  `usage_limit` int(11) DEFAULT NULL,
  `usage_limit_per_user` int(11) DEFAULT NULL,
  `min_amount` float DEFAULT NULL,
  `is_percentage_discount` tinyint(1) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `coupon`
--

INSERT INTO `coupon` (`id`, `name`, `coupon`, `usage_limit`, `usage_limit_per_user`, `min_amount`, `is_percentage_discount`, `discount`, `start_date`, `end_date`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Test', 'test3387', 0, 0, 0, 1, 10, NULL, NULL, 1, '2014-01-14 17:55:13', '2014-01-14 17:55:13');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_order`
--

CREATE TABLE IF NOT EXISTS `coupon_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `order_id` (`order_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `rate`, `weight`, `status`, `created_date`, `updated_date`) VALUES
(1, 'GBP', 'GBP', 1, 1000, 1, '2013-11-09 15:39:41', '2013-11-09 15:39:41');

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE IF NOT EXISTS `feature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_file1` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `name`, `title`, `description`, `file_id`, `type`, `created_date`, `updated_date`) VALUES
(1, 'ram', 'RAM', NULL, NULL, NULL, '2014-01-14 12:43:38', '2014-01-14 12:43:38');

-- --------------------------------------------------------

--
-- Table structure for table `feature_category`
--

CREATE TABLE IF NOT EXISTS `feature_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_product_attribute1` (`feature_id`),
  KEY `fk_attribute_product_product1` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `feature_category`
--

INSERT INTO `feature_category` (`id`, `title`, `feature_id`, `category_id`, `type`, `weight`, `created_date`, `updated_date`) VALUES
(1, NULL, 1, 19, NULL, NULL, '2014-02-24 17:54:04', '2014-02-24 17:54:04');

-- --------------------------------------------------------

--
-- Table structure for table `feature_option`
--

CREATE TABLE IF NOT EXISTS `feature_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_option_attribute1` (`feature_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `feature_option`
--

INSERT INTO `feature_option` (`id`, `name`, `title`, `weight`, `feature_id`, `created_date`, `updated_date`) VALUES
(1, 'htc', 'HTC', '', 1, '2014-01-14 12:44:31', '2014-01-14 12:44:31'),
(2, 'test', 'test', NULL, 1, '2014-01-14 14:58:14', '2014-01-14 14:58:14'),
(3, 'test2', 'test2', NULL, 1, '2014-01-14 14:59:36', '2014-01-14 14:59:36');

-- --------------------------------------------------------

--
-- Table structure for table `feature_option_product`
--

CREATE TABLE IF NOT EXISTS `feature_option_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_option_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `addon_option_id` (`feature_option_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `feature_option_product`
--

INSERT INTO `feature_option_product` (`id`, `feature_option_id`, `product_id`, `created_date`, `updated_date`) VALUES
(1, 1, 647, '2014-01-14 13:06:48', '2014-01-14 13:06:48'),
(3, 3, 647, '2014-01-14 14:59:37', '2014-01-14 14:59:37'),
(4, 1, 646, '2014-02-24 17:47:12', '2014-02-24 17:47:12'),
(5, 2, 646, '2014-02-24 17:47:12', '2014-02-24 17:47:12'),
(6, 3, 646, '2014-02-24 17:47:12', '2014-02-24 17:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `feature_product`
--

CREATE TABLE IF NOT EXISTS `feature_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `feature_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attribute_product_attribute1` (`feature_id`),
  KEY `fk_attribute_product_product1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `feature_product`
--

INSERT INTO `feature_product` (`id`, `title`, `feature_id`, `product_id`, `weight`, `created_date`, `updated_date`) VALUES
(1, NULL, 1, 647, NULL, '2014-01-14 13:06:48', '2014-01-14 13:06:48'),
(2, NULL, 1, 646, NULL, '2014-02-24 17:47:12', '2014-02-24 17:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=965 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `title`, `alt`, `path`, `created_date`, `updated_date`) VALUES
(915, 'HTC One V', 'HTC One V', 'htc-one-v-12.png', '2014-01-02 16:42:26', '2014-01-02 16:42:26'),
(917, 'Cannon One V', 'Cannon One V', 'cannon-one-v-11.png', '2014-01-02 16:42:27', '2014-01-02 16:42:27'),
(918, 'Apple One V', 'Apple One V', 'apple-one-v-8.png', '2014-01-02 16:42:28', '2014-01-02 16:42:28'),
(919, 'Sony Xperia Z', 'Sony Xperia Z', 'sony-xperia-z-8.png', '2014-01-02 16:42:28', '2014-01-02 16:42:28'),
(920, 'Nokia One V', 'Nokia One V', 'nokia-one-v-8.png', '2014-01-02 16:42:29', '2014-01-02 16:42:29'),
(921, 'Micromax One V', 'Micromax One V', 'micromax-one-v-8.png', '2014-01-02 16:42:29', '2014-01-02 16:42:29'),
(922, 'LG', 'LG', 'lg-8.png', '2014-01-02 16:42:30', '2014-01-02 16:42:30'),
(923, 'Samsung One V', 'Samsung One V', 'samsung-one-v-8.png', '2014-01-02 16:42:31', '2014-01-02 16:42:31'),
(924, 'HTC One V', 'HTC One V', 'htc-one-v-13.png', '2014-01-02 16:42:31', '2014-01-02 16:42:31'),
(925, 'Dell One V', 'Dell One V', 'dell-one-v-12.png', '2014-01-02 16:42:32', '2014-01-02 16:42:32'),
(926, 'Cannon One V', 'Cannon One V', 'cannon-one-v-12.png', '2014-01-02 16:42:32', '2014-01-02 16:42:32'),
(927, 'Apple One V', 'Apple One V', 'apple-one-v-9.png', '2014-01-02 16:42:33', '2014-01-02 16:42:33'),
(928, 'Sony Xperia Z', 'Sony Xperia Z', 'sony-xperia-z-9.png', '2014-01-02 16:42:34', '2014-01-02 16:42:34'),
(929, 'Nokia One V', 'Nokia One V', 'nokia-one-v-9.png', '2014-01-02 16:42:34', '2014-01-02 16:42:34'),
(930, 'Micromax One V', 'Micromax One V', 'micromax-one-v-9.png', '2014-01-02 16:42:35', '2014-01-02 16:42:35'),
(931, 'LG', 'LG', 'lg-9.png', '2014-01-02 16:42:35', '2014-01-02 16:42:35'),
(932, 'Samsung One V', 'Samsung One V', 'samsung-one-v-9.png', '2014-01-02 16:42:36', '2014-01-02 16:42:36'),
(933, 'HTC One V', 'HTC One V', 'htc-one-v-14.png', '2014-01-02 16:42:37', '2014-01-02 16:42:37'),
(934, 'Dell One V', 'Dell One V', 'dell-one-v-13.png', '2014-01-02 16:42:37', '2014-01-02 16:42:37'),
(935, 'Cannon One V', 'Cannon One V', 'cannon-one-v-13.png', '2014-01-02 16:42:38', '2014-01-02 16:42:38'),
(936, 'Apple One V', 'Apple One V', 'apple-one-v-10.png', '2014-01-02 16:42:38', '2014-01-02 16:42:38'),
(937, 'Sony Xperia Z', 'Sony Xperia Z', 'sony-xperia-z-6.png', '2014-01-02 16:42:39', '2014-01-02 16:42:39'),
(938, 'Nokia One V', 'Nokia One V', 'nokia-one-v-6.png', '2014-01-02 16:42:40', '2014-01-02 16:42:40'),
(939, 'Micromax One V', 'Micromax One V', 'micromax-one-v-6.png', '2014-01-02 16:42:41', '2014-01-02 16:42:41'),
(940, 'LG', 'LG', 'lg-6.png', '2014-01-02 16:42:41', '2014-01-02 16:42:41'),
(941, 'Samsung One V', 'Samsung One V', 'samsung-one-v-6.png', '2014-01-02 16:42:42', '2014-01-02 16:42:42'),
(942, 'HTC One V', 'HTC One V', 'htc-one-v-10.png', '2014-01-02 16:42:43', '2014-01-02 16:42:43'),
(943, 'Dell One V', 'Dell One V', 'dell-one-v-9.png', '2014-01-02 16:42:43', '2014-01-02 16:42:43'),
(944, 'Cannon One V', 'Cannon One V', 'cannon-one-v-9.png', '2014-01-02 16:42:44', '2014-01-02 16:42:44'),
(945, 'Apple One V', 'Apple One V', 'apple-one-v-7.png', '2014-01-02 16:42:45', '2014-01-02 16:42:45'),
(946, 'Sony Xperia Z', 'Sony Xperia Z', 'sony-xperia-z-7.png', '2014-01-02 16:42:45', '2014-01-02 16:42:45'),
(947, 'Nokia One V', 'Nokia One V', 'nokia-one-v-7.png', '2014-01-02 16:42:46', '2014-01-02 16:42:46'),
(948, 'Micromax One V', 'Micromax One V', 'micromax-one-v-7.png', '2014-01-02 16:42:46', '2014-01-02 16:42:46'),
(949, 'LG', 'LG', 'lg-7.png', '2014-01-02 16:42:47', '2014-01-02 16:42:47'),
(950, 'Samsung One V', 'Samsung One V', 'samsung-one-v-7.png', '2014-01-02 16:42:48', '2014-01-02 16:42:48'),
(951, 'HTC One V', 'HTC One V', 'htc-one-v-11.png', '2014-01-02 16:42:49', '2014-01-02 16:42:49'),
(952, 'Dell One V', 'Dell One V', 'dell-one-v-10.png', '2014-01-02 16:42:49', '2014-01-02 16:42:49'),
(953, 'Cannon One V', 'Cannon One V', 'cannon-one-v-10.png', '2014-01-02 16:42:50', '2014-01-02 16:42:50'),
(955, '', '', 'back1.jpg', '2014-01-03 17:42:53', '2014-01-03 17:42:53'),
(956, '', '', 'back2.jpg', '2014-01-03 17:43:22', '2014-01-03 17:43:22'),
(957, 'HTC', NULL, '', '2014-01-04 16:54:58', '2014-01-04 16:54:58'),
(958, 'Admin Admin', 'Admin Admin', 'user-2.jpg', '2014-01-04 21:32:30', '2014-01-04 21:32:30'),
(961, '', '', 'print-large-roll-up-banner-220x296-pixels-edited.jpg', '2014-01-13 17:34:54', '2014-01-13 17:34:54'),
(963, '', '', 'sony-xperia-z-9-1.png', '2014-02-19 15:50:59', '2014-02-19 15:50:59'),
(964, '', '', 'sony-xperia-z-4-1.png', '2014-02-19 15:51:14', '2014-02-19 15:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `length_type`
--

CREATE TABLE IF NOT EXISTS `length_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `short_name` varchar(45) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `length_type`
--

INSERT INTO `length_type` (`id`, `name`, `short_name`, `rate`, `created_date`, `updated_date`) VALUES
(1, 'Centimeters', 'cms', 1, '2013-11-09 15:49:58', '2013-11-09 15:49:58'),
(2, 'Meters', 'mts', 100, '2013-11-09 15:49:58', '2013-11-09 15:49:58'),
(3, 'Kilometers', 'Kms', 1000, '2013-11-09 15:49:58', '2013-11-09 15:49:58');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `menu_class` varchar(255) DEFAULT NULL,
  `is_collapasable` tinyint(1) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `is_breadcrumb` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_vertical_menu` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `title`, `menu_class`, `is_collapasable`, `weight`, `is_breadcrumb`, `created_date`, `updated_date`, `is_vertical_menu`) VALUES
(1, 'primaryMenuuu', 'Site Primary Menu', 'nav navbar-nav', 1, '10000', 1, '2014-01-17 00:00:00', '2014-02-24 19:16:16', 0),
(6, 'secondrymenu', 'Secondry Menu', 'nav', 1, '10000', 0, '2014-01-22 17:52:25', '2014-02-21 14:00:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_category`
--

CREATE TABLE IF NOT EXISTS `menu_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `menu_option_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `menu_option_id` (`menu_option_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `menu_category`
--

INSERT INTO `menu_category` (`id`, `menu_id`, `menu_option_id`, `created_date`, `updated_date`) VALUES
(4, 1, NULL, '2014-01-18 18:21:22', '2014-01-20 12:02:18'),
(9, 6, NULL, '2014-01-22 17:52:25', '2014-01-22 17:52:25');

-- --------------------------------------------------------

--
-- Table structure for table `menu_option`
--

CREATE TABLE IF NOT EXISTS `menu_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `link_class` varchar(255) DEFAULT NULL,
  `icon_class` varchar(255) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `link_target` varchar(255) DEFAULT NULL,
  `display_to` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `menu_id_2` (`menu_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=393 ;

--
-- Dumping data for table `menu_option`
--

INSERT INTO `menu_option` (`id`, `name`, `title`, `uri`, `weight`, `link_class`, `icon_class`, `menu_id`, `status`, `parent_id`, `link_target`, `display_to`, `created_date`, `updated_date`) VALUES
(385, 'category_19', 'Notebook', '/category/19', 40, NULL, NULL, 1, 1, NULL, NULL, NULL, '2014-02-24 12:16:46', '2014-02-24 17:28:39'),
(386, 'category_20', 'Laptop', '/category/20', 30, NULL, NULL, 1, 1, NULL, NULL, NULL, '2014-02-24 12:16:46', '2014-02-24 12:16:46'),
(387, 'category_24', 'test', '/category/24', 20, NULL, NULL, 1, 1, NULL, NULL, NULL, '2014-02-24 12:16:46', '2014-02-24 12:16:46'),
(388, 'category_21', 'Desktop', '/category/21', 10, NULL, NULL, 1, 1, NULL, NULL, NULL, '2014-02-24 12:16:46', '2014-02-24 12:16:46'),
(389, 'category_19', 'Notebook', '/category/19', 40, NULL, NULL, 6, 1, NULL, NULL, NULL, '2014-02-24 12:16:47', '2014-02-24 17:28:39'),
(390, 'category_20', 'Laptop', '/category/20', 30, NULL, NULL, 6, 1, NULL, NULL, NULL, '2014-02-24 12:16:47', '2014-02-24 12:16:47'),
(391, 'category_24', 'test', '/category/24', 20, NULL, NULL, 6, 1, NULL, NULL, NULL, '2014-02-24 12:16:47', '2014-02-24 12:16:47'),
(392, 'category_21', 'Desktop', '/category/21', 10, NULL, NULL, 6, 1, NULL, NULL, NULL, '2014-02-24 12:16:47', '2014-02-24 12:16:47');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` text,
  `subject` varchar(1024) DEFAULT NULL,
  `body_plain` text,
  `newsletter_category_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `newsletter_category_id` (`newsletter_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_category`
--

CREATE TABLE IF NOT EXISTS `newsletter_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `from_email` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_subscriber`
--

CREATE TABLE IF NOT EXISTS `newsletter_subscriber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `newsletter_category_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `newsletter_category_id` (`newsletter_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_unsubscriber`
--

CREATE TABLE IF NOT EXISTS `newsletter_unsubscriber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletter_subscriber_id` int(11) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `updated_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `newsletter_subscriber_id` (`newsletter_subscriber_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_billing_address`
--

CREATE TABLE IF NOT EXISTS `order_billing_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `address_line2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_billing_address_order_main1` (`order_main_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `order_billing_address`
--

INSERT INTO `order_billing_address` (`id`, `first_name`, `last_name`, `email`, `address_line1`, `address_line2`, `city`, `state`, `country`, `postcode`, `contact_number`, `order_main_id`, `created_date`, `updated_date`) VALUES
(53, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 53, '2014-02-19 15:58:48', '2014-02-19 15:58:48'),
(54, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 54, '2014-02-19 15:58:58', '2014-02-19 15:58:58'),
(55, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 55, '2014-02-19 15:59:24', '2014-02-19 15:59:24'),
(56, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 56, '2014-02-19 16:00:10', '2014-02-19 16:00:10'),
(57, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 57, '2014-02-19 16:01:55', '2014-02-19 16:01:55'),
(58, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 58, '2014-02-19 16:02:41', '2014-02-19 16:02:41'),
(59, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 59, '2014-02-19 16:06:31', '2014-02-19 16:06:31'),
(60, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 60, '2014-02-19 16:06:41', '2014-02-19 16:06:41'),
(61, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 61, '2014-02-19 16:07:08', '2014-02-19 16:07:08'),
(62, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 62, '2014-02-19 16:07:15', '2014-02-19 16:07:15'),
(63, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 63, '2014-02-19 16:09:04', '2014-02-19 16:09:04'),
(64, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 64, '2014-02-19 16:09:31', '2014-02-19 16:09:31'),
(65, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 65, '2014-02-19 16:10:18', '2014-02-19 16:10:18'),
(66, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 66, '2014-02-19 16:11:14', '2014-02-19 16:11:14'),
(67, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 67, '2014-02-19 16:11:49', '2014-02-19 16:11:49'),
(68, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 68, '2014-02-19 16:12:46', '2014-02-19 16:12:46'),
(69, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 69, '2014-02-19 16:13:38', '2014-02-19 16:13:38');

-- --------------------------------------------------------

--
-- Table structure for table `order_comment`
--

CREATE TABLE IF NOT EXISTS `order_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text,
  `display_frontend` tinyint(1) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_comment_order_main1` (`order_main_id`),
  KEY `fk_order_comment_user1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_lineitem`
--

CREATE TABLE IF NOT EXISTS `order_lineitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_lineitem_order_main1` (`order_main_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=132 ;

--
-- Dumping data for table `order_lineitem`
--

INSERT INTO `order_lineitem` (`id`, `title`, `value`, `weight`, `order_main_id`, `created_date`, `updated_date`) VALUES
(98, 'Sub Total', '56', 1000, 53, '2014-02-19 15:58:48', '2014-02-19 15:58:48'),
(99, 'VAT(incl.)', '9.3333333333333', 999, 53, '2014-02-19 15:58:48', '2014-02-19 15:58:48'),
(100, 'Sub Total', '56', 1000, 54, '2014-02-19 15:58:59', '2014-02-19 15:58:59'),
(101, 'VAT(incl.)', '9.3333333333333', 999, 54, '2014-02-19 15:58:59', '2014-02-19 15:58:59'),
(102, 'Sub Total', '56', 1000, 55, '2014-02-19 15:59:24', '2014-02-19 15:59:24'),
(103, 'VAT(incl.)', '9.3333333333333', 999, 55, '2014-02-19 15:59:24', '2014-02-19 15:59:24'),
(104, 'Sub Total', '56', 1000, 56, '2014-02-19 16:00:10', '2014-02-19 16:00:10'),
(105, 'VAT(incl.)', '9.3333333333333', 999, 56, '2014-02-19 16:00:10', '2014-02-19 16:00:10'),
(106, 'Sub Total', '56', 1000, 57, '2014-02-19 16:01:55', '2014-02-19 16:01:55'),
(107, 'VAT(incl.)', '9.3333333333333', 999, 57, '2014-02-19 16:01:55', '2014-02-19 16:01:55'),
(108, 'Sub Total', '56', 1000, 58, '2014-02-19 16:02:41', '2014-02-19 16:02:41'),
(109, 'VAT(incl.)', '9.3333333333333', 999, 58, '2014-02-19 16:02:41', '2014-02-19 16:02:41'),
(110, 'Sub Total', '56', 1000, 59, '2014-02-19 16:06:31', '2014-02-19 16:06:31'),
(111, 'VAT(incl.)', '9.3333333333333', 999, 59, '2014-02-19 16:06:31', '2014-02-19 16:06:31'),
(112, 'Sub Total', '56', 1000, 60, '2014-02-19 16:06:41', '2014-02-19 16:06:41'),
(113, 'VAT(incl.)', '9.3333333333333', 999, 60, '2014-02-19 16:06:41', '2014-02-19 16:06:41'),
(114, 'Sub Total', '56', 1000, 61, '2014-02-19 16:07:08', '2014-02-19 16:07:08'),
(115, 'VAT(incl.)', '9.3333333333333', 999, 61, '2014-02-19 16:07:08', '2014-02-19 16:07:08'),
(116, 'Sub Total', '56', 1000, 62, '2014-02-19 16:07:15', '2014-02-19 16:07:15'),
(117, 'VAT(incl.)', '9.3333333333333', 999, 62, '2014-02-19 16:07:15', '2014-02-19 16:07:15'),
(118, 'Sub Total', '56', 1000, 63, '2014-02-19 16:09:04', '2014-02-19 16:09:04'),
(119, 'VAT(incl.)', '9.3333333333333', 999, 63, '2014-02-19 16:09:04', '2014-02-19 16:09:04'),
(120, 'Sub Total', '56', 1000, 64, '2014-02-19 16:09:31', '2014-02-19 16:09:31'),
(121, 'VAT(incl.)', '9.3333333333333', 999, 64, '2014-02-19 16:09:31', '2014-02-19 16:09:31'),
(122, 'Sub Total', '56', 1000, 65, '2014-02-19 16:10:18', '2014-02-19 16:10:18'),
(123, 'VAT(incl.)', '9.3333333333333', 999, 65, '2014-02-19 16:10:18', '2014-02-19 16:10:18'),
(124, 'Sub Total', '56', 1000, 66, '2014-02-19 16:11:14', '2014-02-19 16:11:14'),
(125, 'VAT(incl.)', '9.3333333333333', 999, 66, '2014-02-19 16:11:14', '2014-02-19 16:11:14'),
(126, 'Sub Total', '56', 1000, 67, '2014-02-19 16:11:49', '2014-02-19 16:11:49'),
(127, 'VAT(incl.)', '9.3333333333333', 999, 67, '2014-02-19 16:11:49', '2014-02-19 16:11:49'),
(128, 'Sub Total', '56', 1000, 68, '2014-02-19 16:12:46', '2014-02-19 16:12:46'),
(129, 'VAT(incl.)', '9.3333333333333', 999, 68, '2014-02-19 16:12:46', '2014-02-19 16:12:46'),
(130, 'Sub Total', '56', 1000, 69, '2014-02-19 16:13:38', '2014-02-19 16:13:38'),
(131, 'VAT(incl.)', '9.3333333333333', 999, 69, '2014-02-19 16:13:38', '2014-02-19 16:13:38');

-- --------------------------------------------------------

--
-- Table structure for table `order_log`
--

CREATE TABLE IF NOT EXISTS `order_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` longtext COLLATE utf8_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CC6427A594040EC1` (`order_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_main`
--

CREATE TABLE IF NOT EXISTS `order_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `order_status_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_main_order_status1` (`order_status_id`),
  KEY `fk_order_main_user1` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `order_main`
--

INSERT INTO `order_main` (`id`, `first_name`, `last_name`, `email`, `amount`, `order_status_id`, `user_id`, `created_date`, `updated_date`) VALUES
(53, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 15:58:48', '2014-02-19 15:58:48'),
(54, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 15:58:58', '2014-02-19 15:58:58'),
(55, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 15:59:24', '2014-02-19 15:59:24'),
(56, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:00:10', '2014-02-19 16:00:10'),
(57, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:01:55', '2014-02-19 16:01:55'),
(58, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:02:41', '2014-02-19 16:02:41'),
(59, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:06:31', '2014-02-19 16:06:31'),
(60, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:06:40', '2014-02-19 16:06:40'),
(61, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:07:08', '2014-02-19 16:07:08'),
(62, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:07:15', '2014-02-19 16:07:15'),
(63, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:09:04', '2014-02-19 16:09:04'),
(64, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:09:31', '2014-02-19 16:09:31'),
(65, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:10:18', '2014-02-19 16:10:18'),
(66, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:11:14', '2014-02-19 16:11:14'),
(67, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:11:49', '2014-02-19 16:11:49'),
(68, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 1, 2, '2014-02-19 16:12:46', '2014-02-19 16:12:46'),
(69, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', 56, 3, 2, '2014-02-19 16:13:38', '2014-02-19 16:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE IF NOT EXISTS `order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cost_price` float DEFAULT NULL,
  `list_price` float DEFAULT NULL,
  `sell_price` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_title` varchar(255) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_product_order_main1` (`order_main_id`),
  KEY `fk_order_product_product1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `cost_price`, `list_price`, `sell_price`, `amount`, `weight`, `qty`, `product_id`, `product_title`, `order_main_id`, `created_date`, `updated_date`) VALUES
(57, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 53, '2014-02-19 15:58:48', '2014-02-19 15:58:48'),
(58, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 54, '2014-02-19 15:58:58', '2014-02-19 15:58:58'),
(59, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 55, '2014-02-19 15:59:24', '2014-02-19 15:59:24'),
(60, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 56, '2014-02-19 16:00:10', '2014-02-19 16:00:10'),
(61, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 57, '2014-02-19 16:01:55', '2014-02-19 16:01:55'),
(62, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 58, '2014-02-19 16:02:41', '2014-02-19 16:02:41'),
(63, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 59, '2014-02-19 16:06:31', '2014-02-19 16:06:31'),
(64, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 60, '2014-02-19 16:06:41', '2014-02-19 16:06:41'),
(65, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 61, '2014-02-19 16:07:08', '2014-02-19 16:07:08'),
(66, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 62, '2014-02-19 16:07:15', '2014-02-19 16:07:15'),
(67, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 63, '2014-02-19 16:09:04', '2014-02-19 16:09:04'),
(68, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 64, '2014-02-19 16:09:31', '2014-02-19 16:09:31'),
(69, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 65, '2014-02-19 16:10:18', '2014-02-19 16:10:18'),
(70, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 66, '2014-02-19 16:11:14', '2014-02-19 16:11:14'),
(71, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 67, '2014-02-19 16:11:49', '2014-02-19 16:11:49'),
(72, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 68, '2014-02-19 16:12:46', '2014-02-19 16:12:46'),
(73, 20, 36, 28, 56, NULL, 2, 649, 'Apple One V', 69, '2014-02-19 16:13:38', '2014-02-19 16:13:38');

-- --------------------------------------------------------

--
-- Table structure for table `order_product_lineitem`
--

CREATE TABLE IF NOT EXISTS `order_product_lineitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `order_product_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_product_lineitem_order_product1` (`order_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `is_sale` tinyint(1) DEFAULT NULL,
  `is_pending` tinyint(1) DEFAULT NULL,
  `is_cancelled` tinyint(1) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body_html` text,
  `body_plain` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `name`, `weight`, `is_sale`, `is_pending`, `is_cancelled`, `subject`, `body_html`, `body_plain`, `created_date`, `updated_date`) VALUES
(1, 'Checkout', 0, 0, 1, 0, 'Order {{order.id}}', '<p>Your Order is in {{order.orderStatus.name}} {{order.id}}</p>', 'Your Order is in {{order.orderStatus.name}} {{order.id}}', '2013-11-09 16:01:57', '2013-11-09 16:07:42'),
(2, 'Pending', 0, 0, 1, 0, 'Pending Order {{order.id}}', '<p>Your Order is in {{order.orderStatus.name}} {{order.id}}</p>', 'Your Order is in {{order.orderStatus.name}} {{order.id}}', '2013-11-09 16:05:47', '2013-11-09 16:07:14'),
(3, 'Accepted', 0, 1, 1, 0, 'Order {{order.id}}', '<p>Your Order is in {{order.orderStatus.name}} {{order.id}}</p>', 'Your Order is in {{order.orderStatus.name}} {{order.id}}', '2013-11-09 16:08:34', '2013-11-09 16:08:34'),
(4, 'Dispatched', 0, 1, 0, 0, 'Order {{order.id}}', '<p>Your Order is in {{order.orderStatus.name}} {{order.id}}</p>', 'Your Order is in {{order.orderStatus.name}} {{order.id}}', '2013-11-09 16:09:37', '2013-11-09 16:09:37'),
(5, 'Completed', 0, 1, 0, 0, 'Order {{order.id}}', '<p>Your Order is in {{order.orderStatus.name}} {{order.id}}</p>', 'Your Order is in {{order.orderStatus.name}} {{order.id}}', '2013-11-09 16:10:28', '2013-11-09 16:10:28'),
(6, 'Cancelled', 0, 0, 0, 1, 'Order {{order.id}}', '<p>Your Order is in {{order.orderStatus.name}} {{order.id}}</p>', 'Your Order is in {{order.orderStatus.name}} {{order.id}}', '2013-11-09 16:12:21', '2013-11-09 16:12:21'),
(7, 'Rejected', 0, 0, 0, 1, 'Order {{order.id}}', '<p>Your Order is in {{order.orderStatus.name}} {{order.id}}</p>', 'Your Order is in {{order.orderStatus.name}} {{order.id}}', '2013-11-09 16:12:59', '2013-11-09 16:12:59');

-- --------------------------------------------------------

--
-- Table structure for table `payment_order`
--

CREATE TABLE IF NOT EXISTS `payment_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `payment_ref_no` varchar(255) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `payment_status_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_main_id`),
  KEY `payment_status_id` (`payment_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `payment_order`
--

INSERT INTO `payment_order` (`id`, `amount`, `payment_type`, `payment_ref_no`, `order_main_id`, `payment_status_id`, `created_date`, `updated_date`) VALUES
(53, 56, 'Test', NULL, 69, 3, '2014-02-19 16:13:38', '2014-02-19 16:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `payment_status`
--

CREATE TABLE IF NOT EXISTS `payment_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body_html` text,
  `body_plain` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `payment_status`
--

INSERT INTO `payment_status` (`id`, `name`, `weight`, `subject`, `body_html`, `body_plain`, `created_date`, `updated_date`) VALUES
(1, 'Checkout', 0, 'Payment Status {{order.id}}', '<p>Payment Status in {{payment.paymentStatus.name}} {{order.id}}</p>\r\n<p></p>', 'Payment Status in {{payment.paymentStatus.name}} {{order.id}}', '2013-11-09 16:42:08', '2013-11-09 16:44:25'),
(2, 'Pending', 0, 'Payment Status in {{payment.paymentStatus.name}} ', '<p>Payment Status in {{payment.paymentStatus.name}} {{order.id}}</p>', 'Payment Status in {{payment.paymentStatus.name}} {{order.id}}', '2013-11-09 16:46:52', '2013-11-09 16:46:52'),
(3, 'Paid', 0, 'Order {{order.id}}', '<p>Payment Status in {{payment.paymentStatus.name}} {{order.id}}</p>', 'Payment Status in {{payment.paymentStatus.name}} {{order.id}}', '2013-11-09 16:47:41', '2013-11-09 16:47:41'),
(4, 'Cancelled', 0, 'Payment Update {{order.id}}', '<p>Payment Status in {{payment.paymentStatus.name}} {{order.id}}</p>', 'Payment Status in {{payment.paymentStatus.name}} {{order.id}}', '2013-11-09 16:48:17', '2013-11-09 16:48:17');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `list_price` float DEFAULT NULL,
  `cost_price` float DEFAULT NULL,
  `sell_price` float DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `search_keywords` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=685 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `short_description`, `description`, `list_price`, `cost_price`, `sell_price`, `sku`, `weight`, `status`, `created_date`, `updated_date`, `search_keywords`) VALUES
(646, 'HTC One V', 'Something about the product goes here. Not More than 2 lines.', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p><iframe width="560" height="315" src="http://www.youtube.com/embed/kde0XOnxeJM" frameborder="0" allowfullscreen=""></iframe></p>', 12, 8, 10, 'ABCDEF1', 0, 1, '2014-01-02 16:20:56', '2014-03-07 12:24:57', ''),
(647, 'Dell One V', 'Something about the product goes here. Not More than 2 lines.', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 20, 12, 16, 'ABCDEF2', 0, 1, '2014-01-02 16:20:56', '2014-02-13 16:49:17', NULL),
(648, 'Cannon One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 28, 16, 22, 'ABCDEF3', NULL, 1, '2014-01-02 16:20:57', '2014-01-02 16:42:27', NULL),
(649, 'Apple One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 36, 20, 28, 'ABCDEF4', NULL, 1, '2014-01-02 16:20:58', '2014-01-02 16:42:28', NULL),
(650, 'Sony Xperia Z', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 44, 24, 34, 'ABCDEF5', NULL, 1, '2014-01-02 16:20:58', '2014-01-02 16:42:28', NULL),
(651, 'Nokia One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 52, 28, 40, 'ABCDEF6', NULL, 1, '2014-01-02 16:21:00', '2014-01-02 16:42:29', NULL),
(652, 'Micromax One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 60, 32, 46, 'ABCDEF7', NULL, 1, '2014-01-02 16:21:01', '2014-01-02 16:42:29', NULL),
(653, 'LG', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 68, 36, 52, 'ABCDEF8', NULL, 1, '2014-01-02 16:21:02', '2014-01-02 16:42:30', NULL),
(654, 'Samsung One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 76, 40, 58, 'ABCDEF9', NULL, 1, '2014-01-02 16:21:04', '2014-01-02 16:42:30', NULL),
(655, 'HTC One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 84, 44, 64, 'ABCDEF10', NULL, 1, '2014-01-02 16:21:06', '2014-01-02 16:42:31', NULL),
(656, 'Dell One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 92, 48, 70, 'ABCDEF11', NULL, 1, '2014-01-02 16:21:09', '2014-01-02 16:42:32', NULL),
(657, 'Cannon One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 100, 52, 76, 'ABCDEF12', NULL, 1, '2014-01-02 16:21:11', '2014-01-02 16:42:32', NULL),
(658, 'Apple One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 108, 56, 82, 'ABCDEF13', NULL, 1, '2014-01-02 16:21:13', '2014-01-02 16:42:33', NULL),
(659, 'Sony Xperia Z', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 116, 60, 88, 'ABCDEF14', NULL, 1, '2014-01-02 16:21:15', '2014-01-02 16:42:33', NULL),
(660, 'Nokia One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 124, 64, 94, 'ABCDEF15', NULL, 1, '2014-01-02 16:21:17', '2014-01-02 16:42:34', NULL),
(661, 'Micromax One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 132, 68, 100, 'ABCDEF16', NULL, 1, '2014-01-02 16:21:19', '2014-01-02 16:42:35', NULL),
(662, 'LG', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 140, 72, 106, 'ABCDEF17', NULL, 1, '2014-01-02 16:21:22', '2014-01-02 16:42:35', NULL),
(663, 'Samsung One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 148, 76, 112, 'ABCDEF18', NULL, 1, '2014-01-02 16:21:25', '2014-01-02 16:42:36', NULL),
(664, 'HTC One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 156, 80, 118, 'ABCDEF19', NULL, 1, '2014-01-02 16:21:28', '2014-01-02 16:42:37', NULL),
(665, 'Dell One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 164, 84, 124, 'ABCDEF20', NULL, 1, '2014-01-02 16:21:31', '2014-01-02 16:42:37', NULL),
(666, 'Cannon One V', 'Something about the product goes here. Not More than 2 lines.', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 172, 88, 130, 'ABCDEF21', 0, 1, '2014-01-02 16:21:34', '2014-01-07 10:34:46', NULL),
(667, 'Apple One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 180, 92, 136, 'ABCDEF22', NULL, 1, '2014-01-02 16:21:38', '2014-01-02 16:42:38', NULL),
(668, 'Sony Xperia Z', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 188, 96, 142, 'ABCDEF23', NULL, 1, '2014-01-02 16:21:42', '2014-01-02 16:42:39', NULL),
(669, 'Nokia One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 196, 100, 148, 'ABCDEF24', NULL, 1, '2014-01-02 16:21:46', '2014-01-02 16:42:40', NULL),
(670, 'Micromax One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 204, 104, 154, 'ABCDEF25', NULL, 1, '2014-01-02 16:21:50', '2014-01-02 16:42:40', NULL),
(671, 'LG', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 212, 108, 160, 'ABCDEF26', NULL, 1, '2014-01-02 16:21:55', '2014-01-02 16:42:41', NULL),
(672, 'Samsung One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 220, 112, 166, 'ABCDEF27', NULL, 1, '2014-01-02 16:22:00', '2014-01-02 16:42:42', NULL),
(673, 'HTC One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 228, 116, 172, 'ABCDEF28', NULL, 1, '2014-01-02 16:22:05', '2014-01-02 16:42:42', NULL),
(674, 'Dell One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 236, 120, 178, 'ABCDEF29', NULL, 1, '2014-01-02 16:22:11', '2014-01-02 16:42:43', NULL),
(675, 'Cannon One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 244, 124, 184, 'ABCDEF30', NULL, 1, '2014-01-02 16:22:17', '2014-01-02 16:42:44', NULL),
(676, 'Apple One V', 'Something about the product goes here. Not More than 2 lines.', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 252, 128, 190, 'ABCDEF31', NULL, 1, '2014-01-02 16:22:23', '2014-02-24 17:08:45', NULL),
(677, 'Sony Xperia Z', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 260, 132, 196, 'ABCDEF32', NULL, 1, '2014-01-02 16:22:30', '2014-01-02 16:42:45', NULL),
(678, 'Nokia One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 268, 136, 202, 'ABCDEF33', NULL, 1, '2014-01-02 16:22:37', '2014-01-02 16:42:46', NULL),
(679, 'Micromax One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 276, 140, 208, 'ABCDEF34', NULL, 1, '2014-01-02 16:22:44', '2014-01-02 16:42:46', NULL),
(680, 'LG', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 284, 144, 214, 'ABCDEF35', NULL, 1, '2014-01-02 16:22:52', '2014-01-02 16:42:47', NULL),
(681, 'Samsung One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 292, 148, 220, 'ABCDEF36', NULL, 1, '2014-01-02 16:23:00', '2014-01-02 16:42:48', NULL),
(682, 'HTC One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 300, 152, 226, 'ABCDEF37', NULL, 1, '2014-01-02 16:23:08', '2014-01-02 16:42:48', NULL),
(683, 'Dell One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 308, 156, 232, 'ABCDEF38', NULL, 1, '2014-01-02 16:23:17', '2014-01-02 16:42:49', NULL),
(684, 'Cannon One V', 'Something about the product goes here. Not More than 2 lines.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 316, 160, 238, 'ABCDEF39', NULL, 1, '2014-01-02 16:23:27', '2014-01-02 16:42:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_category_category1` (`category_id`),
  KEY `fk_product_category_product1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=912 ;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `weight`, `category_id`, `product_id`, `created_date`, `updated_date`) VALUES
(884, NULL, 19, 647, '2014-02-25 10:39:04', '2014-02-25 10:39:04'),
(885, NULL, 20, 649, '2014-02-25 10:39:05', '2014-02-25 10:39:05'),
(886, NULL, 21, 650, '2014-02-25 10:39:06', '2014-02-25 10:39:06'),
(887, NULL, 19, 651, '2014-02-25 10:39:06', '2014-02-25 10:39:06'),
(888, NULL, 20, 653, '2014-02-25 10:39:07', '2014-02-25 10:39:07'),
(889, NULL, 21, 654, '2014-02-25 10:39:08', '2014-02-25 10:39:08'),
(890, NULL, 19, 655, '2014-02-25 10:39:09', '2014-02-25 10:39:09'),
(891, NULL, 20, 657, '2014-02-25 10:39:10', '2014-02-25 10:39:10'),
(892, NULL, 21, 658, '2014-02-25 10:39:10', '2014-02-25 10:39:10'),
(893, NULL, 19, 659, '2014-02-25 10:39:11', '2014-02-25 10:39:11'),
(894, NULL, 20, 661, '2014-02-25 10:39:12', '2014-02-25 10:39:12'),
(895, NULL, 21, 662, '2014-02-25 10:39:13', '2014-02-25 10:39:13'),
(896, NULL, 19, 663, '2014-02-25 10:39:13', '2014-02-25 10:39:13'),
(897, NULL, 20, 665, '2014-02-25 10:39:14', '2014-02-25 10:39:14'),
(898, NULL, 21, 666, '2014-02-25 10:39:15', '2014-02-25 10:39:15'),
(899, NULL, 19, 667, '2014-02-25 10:39:15', '2014-02-25 10:39:15'),
(900, NULL, 20, 669, '2014-02-25 10:39:16', '2014-02-25 10:39:16'),
(901, NULL, 21, 670, '2014-02-25 10:39:17', '2014-02-25 10:39:17'),
(902, NULL, 19, 671, '2014-02-25 10:39:17', '2014-02-25 10:39:17'),
(903, NULL, 20, 673, '2014-02-25 10:39:18', '2014-02-25 10:39:18'),
(904, NULL, 21, 674, '2014-02-25 10:39:19', '2014-02-25 10:39:19'),
(905, NULL, 19, 675, '2014-02-25 10:39:19', '2014-02-25 10:39:19'),
(906, NULL, 20, 677, '2014-02-25 10:39:20', '2014-02-25 10:39:20'),
(907, NULL, 21, 678, '2014-02-25 10:39:21', '2014-02-25 10:39:21'),
(908, NULL, 19, 679, '2014-02-25 10:39:21', '2014-02-25 10:39:21'),
(909, NULL, 20, 681, '2014-02-25 10:39:22', '2014-02-25 10:39:22'),
(910, NULL, 21, 682, '2014-02-25 10:39:23', '2014-02-25 10:39:23'),
(911, NULL, 19, 683, '2014-02-25 10:39:23', '2014-02-25 10:39:23');

-- --------------------------------------------------------

--
-- Table structure for table `product_featured`
--

CREATE TABLE IF NOT EXISTS `product_featured` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `product_featured`
--

INSERT INTO `product_featured` (`id`, `product_id`, `weight`, `created_date`, `updated_date`) VALUES
(1, 646, NULL, '2014-01-03 18:25:05', '2014-01-03 18:25:05'),
(2, 647, NULL, '2014-01-03 18:25:05', '2014-01-03 18:25:05'),
(3, 649, NULL, '2014-01-03 18:25:05', '2014-01-03 18:25:05'),
(4, 650, NULL, '2014-01-03 18:25:05', '2014-01-03 18:25:05'),
(5, 653, NULL, '2014-01-03 18:25:06', '2014-01-03 18:25:06'),
(6, 651, NULL, '2014-01-03 19:02:44', '2014-01-03 19:02:44'),
(7, 652, NULL, '2014-01-03 19:02:44', '2014-01-03 19:02:44'),
(8, 654, NULL, '2014-01-03 19:02:44', '2014-01-03 19:02:44');

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_image_product1` (`product_id`),
  KEY `fk_product_image_file1` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=942 ;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`id`, `weight`, `status`, `product_id`, `file_id`, `created_date`, `updated_date`) VALUES
(899, NULL, 1, 646, 915, '2014-01-02 16:42:26', '2014-01-02 16:42:26'),
(901, NULL, 1, 648, 917, '2014-01-02 16:42:27', '2014-01-02 16:42:27'),
(902, NULL, 1, 649, 918, '2014-01-02 16:42:28', '2014-01-02 16:42:28'),
(903, NULL, 1, 650, 919, '2014-01-02 16:42:28', '2014-01-02 16:42:28'),
(904, NULL, 1, 651, 920, '2014-01-02 16:42:29', '2014-01-02 16:42:29'),
(905, NULL, 1, 652, 921, '2014-01-02 16:42:29', '2014-01-02 16:42:29'),
(906, NULL, 1, 653, 922, '2014-01-02 16:42:30', '2014-01-02 16:42:30'),
(907, NULL, 1, 654, 923, '2014-01-02 16:42:31', '2014-01-02 16:42:31'),
(908, NULL, 1, 655, 924, '2014-01-02 16:42:31', '2014-01-02 16:42:31'),
(909, NULL, 1, 656, 925, '2014-01-02 16:42:32', '2014-01-02 16:42:32'),
(910, NULL, 1, 657, 926, '2014-01-02 16:42:32', '2014-01-02 16:42:32'),
(911, NULL, 1, 658, 927, '2014-01-02 16:42:33', '2014-01-02 16:42:33'),
(912, NULL, 1, 659, 928, '2014-01-02 16:42:34', '2014-01-02 16:42:34'),
(913, NULL, 1, 660, 929, '2014-01-02 16:42:34', '2014-01-02 16:42:34'),
(914, NULL, 1, 661, 930, '2014-01-02 16:42:35', '2014-01-02 16:42:35'),
(915, NULL, 1, 662, 931, '2014-01-02 16:42:35', '2014-01-02 16:42:35'),
(916, NULL, 1, 663, 932, '2014-01-02 16:42:36', '2014-01-02 16:42:36'),
(917, NULL, 1, 664, 933, '2014-01-02 16:42:37', '2014-01-02 16:42:37'),
(918, NULL, 1, 665, 934, '2014-01-02 16:42:37', '2014-01-02 16:42:37'),
(919, NULL, 1, 666, 935, '2014-01-02 16:42:38', '2014-01-02 16:42:38'),
(920, NULL, 1, 667, 936, '2014-01-02 16:42:39', '2014-01-02 16:42:39'),
(921, NULL, 1, 668, 937, '2014-01-02 16:42:39', '2014-01-02 16:42:39'),
(922, NULL, 1, 669, 938, '2014-01-02 16:42:40', '2014-01-02 16:42:40'),
(923, NULL, 1, 670, 939, '2014-01-02 16:42:41', '2014-01-02 16:42:41'),
(924, NULL, 1, 671, 940, '2014-01-02 16:42:41', '2014-01-02 16:42:41'),
(925, NULL, 1, 672, 941, '2014-01-02 16:42:42', '2014-01-02 16:42:42'),
(926, NULL, 1, 673, 942, '2014-01-02 16:42:43', '2014-01-02 16:42:43'),
(927, NULL, 1, 674, 943, '2014-01-02 16:42:43', '2014-01-02 16:42:43'),
(928, NULL, 1, 675, 944, '2014-01-02 16:42:44', '2014-01-02 16:42:44'),
(929, NULL, 1, 676, 945, '2014-01-02 16:42:45', '2014-01-02 16:42:45'),
(930, NULL, 1, 677, 946, '2014-01-02 16:42:45', '2014-01-02 16:42:45'),
(931, NULL, 1, 678, 947, '2014-01-02 16:42:46', '2014-01-02 16:42:46'),
(932, NULL, 1, 679, 948, '2014-01-02 16:42:46', '2014-01-02 16:42:46'),
(933, NULL, 1, 680, 949, '2014-01-02 16:42:47', '2014-01-02 16:42:47'),
(934, NULL, 1, 681, 950, '2014-01-02 16:42:48', '2014-01-02 16:42:48'),
(935, NULL, 1, 682, 951, '2014-01-02 16:42:49', '2014-01-02 16:42:49'),
(936, NULL, 1, 683, 952, '2014-01-02 16:42:49', '2014-01-02 16:42:49'),
(937, NULL, 1, 684, 953, '2014-01-02 16:42:50', '2014-01-02 16:42:50'),
(940, 0, 1, 647, 963, '2014-02-19 15:50:59', '2014-02-19 15:50:59'),
(941, 0, 1, 647, 964, '2014-02-19 15:51:14', '2014-02-19 15:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `product_recommended`
--

CREATE TABLE IF NOT EXISTS `product_recommended` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `recommended_product_id` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `recommended_product_id` (`recommended_product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `product_recommended`
--

INSERT INTO `product_recommended` (`id`, `product_id`, `recommended_product_id`, `weight`, `created_date`, `updated_date`) VALUES
(1, 647, 647, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(2, 647, 646, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(3, 647, 648, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(4, 647, 649, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(5, 647, 650, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(6, 647, 651, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(7, 647, 652, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(8, 647, 653, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(9, 647, 654, NULL, '2014-01-04 17:37:49', '2014-01-04 17:37:49'),
(10, 646, 647, NULL, '2014-02-24 17:48:26', '2014-02-24 17:48:26'),
(11, 646, 666, NULL, '2014-02-24 17:48:26', '2014-02-24 17:48:26'),
(12, 646, 646, NULL, '2014-02-24 17:48:26', '2014-02-24 17:48:26'),
(13, 646, 648, NULL, '2014-02-24 17:48:26', '2014-02-24 17:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE IF NOT EXISTS `seo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `robots` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=601 ;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `url`, `alias`, `page_title`, `meta_keywords`, `meta_description`, `robots`, `created_date`, `updated_date`) VALUES
(5, '/', '', 'Welcome to the test', 'hi how are you', 'hi how are you', 'robots', '2014-02-15 15:18:18', '2014-02-15 15:18:18'),
(525, '/product/647', '/dell-one-v', 'Dell One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:43', '2014-02-15 17:57:43'),
(526, '/product/666', '/cannon-one-v', 'Cannon One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:43', '2014-02-15 17:57:43'),
(527, '/product/646', '/htc-one-v', 'HTC One V3', 'Something about the product goes here. Not More than 2 lines3. {{page}}', 'Something about the product goes here. Not More than 2 32 ine3s.', 'dfsdf', '2014-02-15 17:57:43', '2014-02-17 11:58:02'),
(528, '/product/648', '/cannon-one-v-1', 'Cannon One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:43', '2014-02-15 17:57:43'),
(529, '/product/649', '/apple-one-v', 'Apple One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:43', '2014-02-15 17:57:43'),
(530, '/product/650', '/sony-xperia-z', 'Sony Xperia Z', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:43', '2014-02-15 17:57:43'),
(531, '/product/651', '/nokia-one-v', 'Nokia One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:43', '2014-02-15 17:57:43'),
(532, '/product/652', '/micromax-one-v', 'Micromax One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:43', '2014-02-15 17:57:43'),
(533, '/product/653', '/lg', 'LG', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:43', '2014-02-15 17:57:43'),
(534, '/product/654', '/samsung-one-v', 'Samsung One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(535, '/product/655', '/htc-one-v-1', 'HTC One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(536, '/product/656', '/dell-one-v-1', 'Dell One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(537, '/product/657', '/cannon-one-v-2', 'Cannon One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(538, '/product/658', '/apple-one-v-1', 'Apple One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(539, '/product/659', '/sony-xperia-z-1', 'Sony Xperia Z', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(540, '/product/660', '/nokia-one-v-1', 'Nokia One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(541, '/product/661', '/micromax-one-v-1', 'Micromax One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(542, '/product/662', '/lg-1', 'LG', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(543, '/product/663', '/samsung-one-v-1', 'Samsung One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(544, '/product/664', '/htc-one-v-2', 'HTC One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(545, '/product/665', '/dell-one-v-2', 'Dell One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(546, '/product/667', '/apple-one-v-2', 'Apple One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(547, '/product/668', '/sony-xperia-z-2', 'Sony Xperia Z', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:44', '2014-02-15 17:57:44'),
(548, '/product/669', '/nokia-one-v-2', 'Nokia One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(549, '/product/670', '/micromax-one-v-2', 'Micromax One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(550, '/product/671', '/lg-2', 'LG', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(551, '/product/672', '/samsung-one-v-2', 'Samsung One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(552, '/product/673', '/htc-one-v-3', 'HTC One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(553, '/product/674', '/dell-one-v-3', 'Dell One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(554, '/product/675', '/cannon-one-v-3', 'Cannon One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(555, '/product/676', '/apple-one-v-3', 'Apple One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(556, '/product/677', '/sony-xperia-z-3', 'Sony Xperia Z', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(557, '/product/678', '/nokia-one-v-3', 'Nokia One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(558, '/product/679', '/micromax-one-v-3', 'Micromax One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(559, '/product/680', '/lg-3', 'LG', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(560, '/product/681', '/samsung-one-v-3', 'Samsung One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(561, '/product/682', '/htc-one-v-4', 'HTC One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:45', '2014-02-15 17:57:45'),
(562, '/product/683', '/dell-one-v-4', 'Dell One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:46', '2014-02-15 17:57:46'),
(563, '/product/684', '/cannon-one-v-4', 'Cannon One V', 'Something about the product goes here. Not More than 2 lines. {{page}}', 'Something about the product goes here. Not More than 2 lines.', '', '2014-02-15 17:57:46', '2014-02-15 17:57:46'),
(587, '/category/20', '/laptop', 'Laptop', '', '', '', '2014-02-17 12:30:50', '2014-02-17 12:30:50'),
(588, '/category/20/*', '/laptop/*', 'Laptop', '', ' {{page}}', '', '2014-02-17 12:30:50', '2014-02-17 12:30:50'),
(589, '/category/19', '/notebook', 'Notebook', 'dfa', 'dfs', 'dsffffffffff', '2014-02-17 12:30:51', '2014-02-17 12:41:04'),
(590, '/category/19/*', '/notebook/*', 'Notebook', 'dfasd', '{{page}}', 'ssssssssssssss', '2014-02-17 12:30:51', '2014-02-17 12:41:32'),
(591, '/category/21', '/desktop', 'Desktop', '', '', '', '2014-02-17 12:30:51', '2014-02-17 12:30:51'),
(592, '/category/21/*', '/desktop/*', 'Desktop', '', ' {{page}}', '', '2014-02-17 12:30:51', '2014-02-17 12:30:51'),
(593, '/category/22', '/clothing', 'Clothing', '', '', '', '2014-02-17 12:30:51', '2014-02-17 12:30:51'),
(594, '/category/22/*', '/clothing/*', 'Clothing', '', ' {{page}}', '', '2014-02-17 12:30:51', '2014-02-17 12:30:51'),
(595, '/category/23', '/dresses', 'Dresses', '', '', '', '2014-02-17 12:30:51', '2014-02-17 12:30:51'),
(596, '/category/23/*', '/dresses/*', 'Dresses', '', ' {{page}}', '', '2014-02-17 12:30:51', '2014-02-17 12:30:51'),
(599, '/static-page/1', '/about-us', 'About Us', '', '', '', '2014-02-17 13:59:23', '2014-02-17 13:59:23'),
(600, '/static-page/2', '/privacy-policy', 'Privacy policy', '', '', '', '2014-02-17 13:59:23', '2014-02-17 13:59:23');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_amount`
--

CREATE TABLE IF NOT EXISTS `shipping_amount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_amount` float DEFAULT NULL,
  `max_amount` float DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_amount_country`
--

CREATE TABLE IF NOT EXISTS `shipping_amount_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) DEFAULT NULL,
  `shipping_amount_id` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_shipping_country_shipping1` (`shipping_amount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_order`
--

CREATE TABLE IF NOT EXISTS `shipping_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `tracking_no` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `shipping_status_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_status_id` (`shipping_status_id`),
  KEY `IDX_1BB64E2294040EC1` (`order_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_order_address`
--

CREATE TABLE IF NOT EXISTS `shipping_order_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `address_line2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_shipping_address_order_main1` (`order_main_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `shipping_order_address`
--

INSERT INTO `shipping_order_address` (`id`, `first_name`, `last_name`, `email`, `address_line1`, `address_line2`, `city`, `state`, `country`, `postcode`, `contact_number`, `order_main_id`, `created_date`, `updated_date`) VALUES
(53, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 53, '2014-02-19 15:58:48', '2014-02-19 15:58:48'),
(54, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 54, '2014-02-19 15:58:59', '2014-02-19 15:58:59'),
(55, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 55, '2014-02-19 15:59:24', '2014-02-19 15:59:24'),
(56, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 56, '2014-02-19 16:00:10', '2014-02-19 16:00:10'),
(57, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 57, '2014-02-19 16:01:56', '2014-02-19 16:01:56'),
(58, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 58, '2014-02-19 16:02:41', '2014-02-19 16:02:41'),
(59, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 59, '2014-02-19 16:06:31', '2014-02-19 16:06:31'),
(60, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 60, '2014-02-19 16:06:41', '2014-02-19 16:06:41'),
(61, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 61, '2014-02-19 16:07:08', '2014-02-19 16:07:08'),
(62, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 62, '2014-02-19 16:07:15', '2014-02-19 16:07:15'),
(63, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 63, '2014-02-19 16:09:04', '2014-02-19 16:09:04'),
(64, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 64, '2014-02-19 16:09:31', '2014-02-19 16:09:31'),
(65, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 65, '2014-02-19 16:10:18', '2014-02-19 16:10:18'),
(66, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 66, '2014-02-19 16:11:15', '2014-02-19 16:11:15'),
(67, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 67, '2014-02-19 16:11:50', '2014-02-19 16:11:50'),
(68, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 68, '2014-02-19 16:12:46', '2014-02-19 16:12:46'),
(69, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 69, '2014-02-19 16:13:38', '2014-02-19 16:13:38');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_status`
--

CREATE TABLE IF NOT EXISTS `shipping_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body_html` text,
  `body_plain` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `shipping_status`
--

INSERT INTO `shipping_status` (`id`, `name`, `weight`, `subject`, `body_html`, `body_plain`, `created_date`, `updated_date`) VALUES
(1, 'Pending', 0, 'Shipping Status Updated {{order.id}}', '<p>Shipping Status Updated {{shipping.name}}</p>', 'Shipping Status Updated {{shipping.name}}', '2013-11-09 17:03:48', '2013-11-09 17:09:57'),
(2, 'Dispatched', 0, 'Shipping Status Updated {{order.name}}', '<p>Shipping Status Updated {{shipping.name}}</p>', 'Shipping Status Updated {{shipping.name}}', '2013-11-09 17:04:32', '2013-11-09 17:10:51'),
(3, 'Delivered', 0, 'Order {{order.id}}', '<p>Shipping Status Updated {{shipping.name}}</p>', 'Shipping Status Updated {{shipping.name}}', '2013-11-09 17:05:27', '2013-11-09 17:11:38'),
(4, 'Delivery Failed', 0, 'Shipping Status Updated {{shipping.name}}', '<p>Shipping Status Updated {{shipping.name}}</p>', 'Shipping Status Updated {{shipping.name}}', '2013-11-09 17:06:01', '2013-11-09 17:06:26');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_weight`
--

CREATE TABLE IF NOT EXISTS `shipping_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_weight` float DEFAULT NULL,
  `max_weight` float DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_weight_country`
--

CREATE TABLE IF NOT EXISTS `shipping_weight_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `shipping_weight_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_table1_shipping_weight1` (`shipping_weight_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `static_block`
--

CREATE TABLE IF NOT EXISTS `static_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `static_block`
--

INSERT INTO `static_block` (`id`, `name`, `title`, `description`, `status`, `created_date`, `updated_date`) VALUES
(1, 'footerMenu', 'Quick Links', '<p></p>\r\n<ul class="nav navbar-stacked footmenu">\r\n<li><a href="#">About Us </a></li>\r\n<li><a href="#">Contact Us</a></li>\r\n<li><a href="#">Terms and Conditions </a></li>\r\n<li><a href="#">Privacy </a></li>\r\n</ul>', 1, '2013-11-29 11:41:32', '2014-02-24 19:27:00'),
(2, 'getInTouch', 'Get In Touch', '<div class="address">\r\n<p>#44,Sky Lines Village,</p>\r\n<p>Limeharbour,</p>\r\n<p>Canary Wharf, London.</p>\r\n<p>+44 (0)20 7093 1216</p>\r\n<p>info@blueit-services.co.uk</p>\r\n</div>', 1, '2013-12-02 12:06:53', '2014-02-19 15:55:23'),
(5, 'footerDummyMessage', 'Oslon de'' Techno', '<div class="fwidget">\r\n<h4>Oslon de<span class="color">''</span> Techno</h4>\r\n<hr />\r\n<p>Duis leo risus, vehicula luctus nunc. Quiue rhoncus, a sodales enim arcu quis turpis. Duis leo risus, condimentum ut posuere ac, vehicula luctus nunc. Quisque rhoncus, a sodales enim arcu quis turpis.</p>\r\n<div class="social"><a class="facebook" href="#"><i class="icon-facebook">&nbsp;</i></a> <a class="twitter" href="#"><i class="icon-twitter">&nbsp;</i></a> <a class="google-plus" href="#"><i class="icon-google-plus">&nbsp;</i></a> <a class="linkedin" href="#"><i class="icon-linkedin">&nbsp;</i></a> <a class="pinterest" href="#"><i class="icon-pinterest">&nbsp;</i></a></div>\r\n</div>', 1, '2014-01-03 20:11:20', '2014-01-03 20:20:42');

-- --------------------------------------------------------

--
-- Table structure for table `static_page`
--

CREATE TABLE IF NOT EXISTS `static_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `static_page`
--

INSERT INTO `static_page` (`id`, `title`, `description`, `created_date`, `updated_date`) VALUES
(1, 'About Us', '<div>\r\n<div class="lc">\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n</div>\r\n<div class="rc">\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<div class="lc">\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n</div>\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', '2014-01-14 16:39:56', '2014-01-14 16:39:56'),
(2, 'Privacy policy', '<div>\r\n<div class="lc">\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n</div>\r\n<div class="rc">\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<div class="lc">\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>\r\n</div>\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', '2014-01-14 16:40:57', '2014-01-14 16:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stock_product1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=683 ;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `qty`, `product_id`, `status`, `created_date`, `updated_date`) VALUES
(640, NULL, 668, NULL, '2014-01-02 16:34:37', '2014-01-02 16:34:37'),
(641, NULL, 669, NULL, '2014-01-02 16:34:41', '2014-01-02 16:34:41'),
(642, NULL, 670, NULL, '2014-01-02 16:34:41', '2014-01-02 16:34:41'),
(643, NULL, 671, NULL, '2014-01-02 16:34:42', '2014-01-02 16:34:42'),
(644, NULL, 672, NULL, '2014-01-02 16:34:43', '2014-01-02 16:34:43'),
(645, NULL, 673, NULL, '2014-01-02 16:34:43', '2014-01-02 16:34:43'),
(646, NULL, 674, NULL, '2014-01-02 16:34:44', '2014-01-02 16:34:44'),
(647, NULL, 675, NULL, '2014-01-02 16:34:44', '2014-01-02 16:34:44'),
(649, NULL, 677, NULL, '2014-01-02 16:34:45', '2014-01-02 16:34:45'),
(650, NULL, 678, NULL, '2014-01-02 16:34:46', '2014-01-02 16:34:46'),
(651, NULL, 679, NULL, '2014-01-02 16:34:47', '2014-01-02 16:34:47'),
(652, NULL, 680, NULL, '2014-01-02 16:34:47', '2014-01-02 16:34:47'),
(653, NULL, 681, NULL, '2014-01-02 16:34:48', '2014-01-02 16:34:48'),
(654, NULL, 682, NULL, '2014-01-02 16:34:49', '2014-01-02 16:34:49'),
(655, NULL, 683, NULL, '2014-01-02 16:34:49', '2014-01-02 16:34:49'),
(656, NULL, 684, NULL, '2014-01-02 16:34:50', '2014-01-02 16:34:50'),
(659, NULL, 648, NULL, '2014-01-02 16:42:27', '2014-01-02 16:42:27'),
(660, NULL, 649, NULL, '2014-01-02 16:42:28', '2014-01-02 16:42:28'),
(661, NULL, 650, NULL, '2014-01-02 16:42:28', '2014-01-02 16:42:28'),
(662, NULL, 651, NULL, '2014-01-02 16:42:29', '2014-01-02 16:42:29'),
(663, NULL, 652, NULL, '2014-01-02 16:42:29', '2014-01-02 16:42:29'),
(664, NULL, 653, NULL, '2014-01-02 16:42:30', '2014-01-02 16:42:30'),
(665, NULL, 654, NULL, '2014-01-02 16:42:31', '2014-01-02 16:42:31'),
(666, NULL, 655, NULL, '2014-01-02 16:42:31', '2014-01-02 16:42:31'),
(667, NULL, 656, NULL, '2014-01-02 16:42:32', '2014-01-02 16:42:32'),
(668, NULL, 657, NULL, '2014-01-02 16:42:32', '2014-01-02 16:42:32'),
(669, NULL, 658, NULL, '2014-01-02 16:42:33', '2014-01-02 16:42:33'),
(670, NULL, 659, NULL, '2014-01-02 16:42:34', '2014-01-02 16:42:34'),
(671, NULL, 660, NULL, '2014-01-02 16:42:34', '2014-01-02 16:42:34'),
(672, NULL, 661, NULL, '2014-01-02 16:42:35', '2014-01-02 16:42:35'),
(673, NULL, 662, NULL, '2014-01-02 16:42:35', '2014-01-02 16:42:35'),
(674, NULL, 663, NULL, '2014-01-02 16:42:36', '2014-01-02 16:42:36'),
(675, NULL, 664, NULL, '2014-01-02 16:42:37', '2014-01-02 16:42:37'),
(676, NULL, 665, NULL, '2014-01-02 16:42:37', '2014-01-02 16:42:37'),
(678, NULL, 667, NULL, '2014-01-02 16:42:39', '2014-01-02 16:42:39'),
(680, NULL, 647, NULL, '2014-02-25 09:57:24', '2014-02-25 09:57:24'),
(681, NULL, 666, NULL, '2014-02-25 09:57:32', '2014-02-25 09:57:32'),
(682, NULL, 676, NULL, '2014-02-25 09:57:36', '2014-02-25 09:57:36');

-- --------------------------------------------------------

--
-- Table structure for table `stock_attribute`
--

CREATE TABLE IF NOT EXISTS `stock_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` varchar(255) DEFAULT NULL,
  `attribute_option_product_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stock_product1` (`attribute_option_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stock_attribute_block`
--

CREATE TABLE IF NOT EXISTS `stock_attribute_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` varchar(255) DEFAULT NULL,
  `attribute_option_product_id` int(11) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stock_product1` (`attribute_option_product_id`),
  KEY `order_main_id` (`order_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stock_block`
--

CREATE TABLE IF NOT EXISTS `stock_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `qty` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `order_main_id` (`order_main_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stock_order_revert`
--

CREATE TABLE IF NOT EXISTS `stock_order_revert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` varchar(255) DEFAULT NULL,
  `order_main_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_stock_order_revert_order_main1` (`order_main_id`),
  KEY `fk_stock_order_revert_product1` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `weight` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_banner_file1` (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `title`, `file_id`, `status`, `weight`, `created_date`, `updated_date`) VALUES
(1, 'Hot', NULL, 1, '', '2014-01-04 17:30:00', '2014-01-04 17:30:00'),
(2, 'Sale', NULL, 1, '', '2014-01-04 17:31:13', '2014-01-04 17:31:13');

-- --------------------------------------------------------

--
-- Table structure for table `tag_product`
--

CREATE TABLE IF NOT EXISTS `tag_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weight` varchar(255) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_category_category1` (`tag_id`),
  KEY `fk_product_category_product1` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tag_product`
--

INSERT INTO `tag_product` (`id`, `weight`, `tag_id`, `product_id`, `created_date`, `updated_date`) VALUES
(1, NULL, 1, 647, '2014-01-04 17:31:49', '2014-01-04 17:31:49'),
(2, NULL, 2, 647, '2014-01-04 17:31:49', '2014-01-04 17:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE IF NOT EXISTS `testimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testimonial` longtext COLLATE utf8_unicode_ci,
  `status` tinyint(1) DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E6BDCDF793CB796C` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `avatar_id` int(11) DEFAULT NULL,
  `user_role_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_file` (`avatar_id`),
  KEY `fk_user_user_role1` (`user_role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `password`, `contact_number`, `avatar_id`, `user_role_id`, `status`, `created_date`, `updated_date`) VALUES
(1, 'Admin', 'Admin', 'admin@blueit-services.co.uk', 'e64b78fc3bc91bcbc7dc232ba8ec59e0', '07885914362', 958, 1, 1, '2013-11-09 00:00:00', '2014-01-04 21:32:30'),
(2, 'James', 'Anderson', 'apple.php@gmail.com', 'e64b78fc3bc91bcbc7dc232ba8ec59e0', '07885914362', NULL, 2, 1, '2014-02-18 15:06:43', '2014-02-18 15:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE IF NOT EXISTS `user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `address_line2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_order_shipping_address_order_main1` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_address`
--

INSERT INTO `user_address` (`id`, `first_name`, `last_name`, `email`, `address_line1`, `address_line2`, `city`, `state`, `country`, `postcode`, `contact_number`, `user_id`, `is_default`, `created_date`, `updated_date`) VALUES
(1, 'James', 'Anderson', 'apple.php@gmail.com', '1245645', '1235', 'Essex', 'Essex', 'United Kingdom', 'ig27tl', '07885914362', 1, 0, '2013-11-29 18:01:18', '2014-01-11 16:15:10'),
(2, 'James', 'Anderson', 'apple.php@gmail.com', '1245', '', 'Essex', 'Essex', 'United Kingdom', 'ig27tl', '07885914362', 1, 0, '2013-11-30 14:02:23', '2013-11-30 14:06:32'),
(4, 'Prashanth', 'Pratapagiri', 'prashi3387@gmail.com', '119', '', 'Ilford', 'Essex', 'United Kingdom', 'IG2 7TL', '07885914362', 2, 1, '2014-02-19 12:05:33', '2014-02-19 12:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_forgot_password`
--

CREATE TABLE IF NOT EXISTS `user_forgot_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `secret` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_forgot_password_user1` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_forgot_password`
--

INSERT INTO `user_forgot_password` (`id`, `secret`, `user_id`, `created_date`, `updated_date`) VALUES
(1, '3e6254f588dc1d8ddb8f72e5cbd29514', 1, '2014-01-04 20:00:46', '2014-01-04 20:00:46');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `access_admin` tinyint(1) DEFAULT NULL,
  `display_at_registration` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `title`, `access_admin`, `display_at_registration`, `created_date`, `updated_date`) VALUES
(1, 'Administrator', 1, 0, '2013-11-09 00:00:00', '2013-11-09 00:00:00'),
(2, 'User', 0, 1, '2013-11-09 00:00:00', '2013-11-09 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vat_product`
--

CREATE TABLE IF NOT EXISTS `vat_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `vat_product`
--

INSERT INTO `vat_product` (`id`, `product_id`, `status`, `created_date`, `updated_date`) VALUES
(1, 647, 1, '2014-01-04 17:31:49', '2014-02-13 16:49:17'),
(2, 646, 0, '2014-01-05 14:03:33', '2014-03-07 12:24:57'),
(3, 666, 1, '2014-01-07 10:34:47', '2014-01-07 10:34:47'),
(4, 676, 1, '2014-02-24 17:08:15', '2014-02-24 17:08:46');

-- --------------------------------------------------------

--
-- Table structure for table `weight_type`
--

CREATE TABLE IF NOT EXISTS `weight_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `short_name` varchar(45) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `weight_type`
--

INSERT INTO `weight_type` (`id`, `name`, `short_name`, `rate`, `created_date`, `updated_date`) VALUES
(1, 'Grams', 'gms', 1, '2013-11-09 15:41:07', '2013-11-09 15:45:30'),
(2, 'Kilo Grams', 'Kgs', 100, '2013-11-09 15:41:28', '2013-11-09 15:45:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addon`
--
ALTER TABLE `addon`
  ADD CONSTRAINT `addon_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `addon_category`
--
ALTER TABLE `addon_category`
  ADD CONSTRAINT `addon_category_ibfk_1` FOREIGN KEY (`addon_id`) REFERENCES `addon` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `addon_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `addon_option`
--
ALTER TABLE `addon_option`
  ADD CONSTRAINT `addon_option_ibfk_1` FOREIGN KEY (`addon_id`) REFERENCES `addon` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `addon_option_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `addon_option_product`
--
ALTER TABLE `addon_option_product`
  ADD CONSTRAINT `addon_option_product_ibfk_1` FOREIGN KEY (`addon_option_id`) REFERENCES `addon_option` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `addon_option_product_ibfk_2` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `addon_option_product_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `addon_option_product_order`
--
ALTER TABLE `addon_option_product_order`
  ADD CONSTRAINT `FK_80AECE7F2E659EFF` FOREIGN KEY (`addon_option_product_id`) REFERENCES `addon_option_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_80AECE7FF65E9B0F` FOREIGN KEY (`order_product_id`) REFERENCES `order_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `addon_product`
--
ALTER TABLE `addon_product`
  ADD CONSTRAINT `addon_product_ibfk_1` FOREIGN KEY (`addon_id`) REFERENCES `addon` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `addon_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `attribute`
--
ALTER TABLE `attribute`
  ADD CONSTRAINT `FK_FA7AEFFB93CB796C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `attribute_category`
--
ALTER TABLE `attribute_category`
  ADD CONSTRAINT `attribute_category_ibfk_1` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `attribute_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `attribute_option`
--
ALTER TABLE `attribute_option`
  ADD CONSTRAINT `FK_78672EEA93CB796C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_attribute_option_attribute1` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `attribute_option_product`
--
ALTER TABLE `attribute_option_product`
  ADD CONSTRAINT `attribute_option_product_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_F6C30A5493CB796C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `attribute_option_product_order`
--
ALTER TABLE `attribute_option_product_order`
  ADD CONSTRAINT `FK_770F63CEF576382A` FOREIGN KEY (`attribute_option_product_id`) REFERENCES `attribute_option_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_770F63CEF65E9B0F` FOREIGN KEY (`order_product_id`) REFERENCES `order_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_product`
--
ALTER TABLE `attribute_product`
  ADD CONSTRAINT `fk_attribute_product_attribute1` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_attribute_product_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `banner`
--
ALTER TABLE `banner`
  ADD CONSTRAINT `fk_banner_file1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `banner2`
--
ALTER TABLE `banner2`
  ADD CONSTRAINT `FK_24D4546E93CB796C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `brand`
--
ALTER TABLE `brand`
  ADD CONSTRAINT `brand_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `brand_category`
--
ALTER TABLE `brand_category`
  ADD CONSTRAINT `brand_category_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `brand_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `brand_product`
--
ALTER TABLE `brand_product`
  ADD CONSTRAINT `brand_product_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `brand_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `fk_category_category1` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `category_image`
--
ALTER TABLE `category_image`
  ADD CONSTRAINT `fk_category_image_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_category_image_file1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `country_state`
--
ALTER TABLE `country_state`
  ADD CONSTRAINT `FK_473C711F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `coupon_order`
--
ALTER TABLE `coupon_order`
  ADD CONSTRAINT `coupon_order_ibfk_3` FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `coupon_order_ibfk_4` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `feature`
--
ALTER TABLE `feature`
  ADD CONSTRAINT `FK_1FD7756693CB796C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `feature_category`
--
ALTER TABLE `feature_category`
  ADD CONSTRAINT `feature_category_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `feature_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `feature_option`
--
ALTER TABLE `feature_option`
  ADD CONSTRAINT `feature_option_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `feature_option_product`
--
ALTER TABLE `feature_option_product`
  ADD CONSTRAINT `feature_option_product_ibfk_1` FOREIGN KEY (`feature_option_id`) REFERENCES `feature_option` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `feature_option_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `feature_product`
--
ALTER TABLE `feature_product`
  ADD CONSTRAINT `feature_product_ibfk_1` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `feature_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `menu_category`
--
ALTER TABLE `menu_category`
  ADD CONSTRAINT `FK_2A1D5C57BE847A5B` FOREIGN KEY (`menu_option_id`) REFERENCES `menu_option` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_category_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `menu_option`
--
ALTER TABLE `menu_option`
  ADD CONSTRAINT `FK_89C15868727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_option` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_option_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD CONSTRAINT `FK_7E8585C824A8C22D` FOREIGN KEY (`newsletter_category_id`) REFERENCES `newsletter_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `newsletter_subscriber`
--
ALTER TABLE `newsletter_subscriber`
  ADD CONSTRAINT `FK_401562C324A8C22D` FOREIGN KEY (`newsletter_category_id`) REFERENCES `newsletter_category` (`id`);

--
-- Constraints for table `newsletter_unsubscriber`
--
ALTER TABLE `newsletter_unsubscriber`
  ADD CONSTRAINT `newsletter_unsubscriber_ibfk_1` FOREIGN KEY (`newsletter_subscriber_id`) REFERENCES `newsletter_subscriber` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `order_billing_address`
--
ALTER TABLE `order_billing_address`
  ADD CONSTRAINT `fk_order_billing_address_order_main1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `order_comment`
--
ALTER TABLE `order_comment`
  ADD CONSTRAINT `fk_order_comment_order_main1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_comment_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `order_lineitem`
--
ALTER TABLE `order_lineitem`
  ADD CONSTRAINT `fk_order_lineitem_order_main1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `order_log`
--
ALTER TABLE `order_log`
  ADD CONSTRAINT `FK_CC6427A594040EC1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_main`
--
ALTER TABLE `order_main`
  ADD CONSTRAINT `FK_F2D9F773A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_order_main_order_status1` FOREIGN KEY (`order_status_id`) REFERENCES `order_status` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `FK_2530ADE64584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_order_product_order_main1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `order_product_lineitem`
--
ALTER TABLE `order_product_lineitem`
  ADD CONSTRAINT `fk_order_product_lineitem_order_product1` FOREIGN KEY (`order_product_id`) REFERENCES `order_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `payment_order`
--
ALTER TABLE `payment_order`
  ADD CONSTRAINT `payment_order_ibfk_1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `payment_order_ibfk_2` FOREIGN KEY (`payment_status_id`) REFERENCES `payment_status` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `product_category`
--
ALTER TABLE `product_category`
  ADD CONSTRAINT `fk_product_category_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_category_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_featured`
--
ALTER TABLE `product_featured`
  ADD CONSTRAINT `product_featured_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_image`
--
ALTER TABLE `product_image`
  ADD CONSTRAINT `fk_product_image_file1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_image_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_recommended`
--
ALTER TABLE `product_recommended`
  ADD CONSTRAINT `product_recommended_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `product_recommended_ibfk_2` FOREIGN KEY (`recommended_product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `shipping_amount_country`
--
ALTER TABLE `shipping_amount_country`
  ADD CONSTRAINT `fk_shipping_country_shipping1` FOREIGN KEY (`shipping_amount_id`) REFERENCES `shipping_amount` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `shipping_order`
--
ALTER TABLE `shipping_order`
  ADD CONSTRAINT `FK_1BB64E222D02518A` FOREIGN KEY (`shipping_status_id`) REFERENCES `shipping_status` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_1BB64E2294040EC1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shipping_order_address`
--
ALTER TABLE `shipping_order_address`
  ADD CONSTRAINT `fk_order_shipping_address_order_main1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `shipping_weight_country`
--
ALTER TABLE `shipping_weight_country`
  ADD CONSTRAINT `fk_table1_shipping_weight1` FOREIGN KEY (`shipping_weight_id`) REFERENCES `shipping_weight` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `fk_stock_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `stock_attribute`
--
ALTER TABLE `stock_attribute`
  ADD CONSTRAINT `stock_attribute_ibfk_1` FOREIGN KEY (`attribute_option_product_id`) REFERENCES `attribute_option_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `stock_attribute_block`
--
ALTER TABLE `stock_attribute_block`
  ADD CONSTRAINT `stock_attribute_block_ibfk_1` FOREIGN KEY (`attribute_option_product_id`) REFERENCES `attribute_option_product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `stock_attribute_block_ibfk_2` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `stock_block`
--
ALTER TABLE `stock_block`
  ADD CONSTRAINT `stock_block_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `stock_block_ibfk_2` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `stock_order_revert`
--
ALTER TABLE `stock_order_revert`
  ADD CONSTRAINT `fk_stock_order_revert_order_main1` FOREIGN KEY (`order_main_id`) REFERENCES `order_main` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_stock_order_revert_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `tag`
--
ALTER TABLE `tag`
  ADD CONSTRAINT `tag_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `tag_product`
--
ALTER TABLE `tag_product`
  ADD CONSTRAINT `tag_product_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `tag_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD CONSTRAINT `FK_E6BDCDF793CB796C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D6498E0E3CA6` FOREIGN KEY (`user_role_id`) REFERENCES `user_role` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_user_file` FOREIGN KEY (`avatar_id`) REFERENCES `file` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `user_address`
--
ALTER TABLE `user_address`
  ADD CONSTRAINT `user_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_forgot_password`
--
ALTER TABLE `user_forgot_password`
  ADD CONSTRAINT `fk_user_forgot_password_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `vat_product`
--
ALTER TABLE `vat_product`
  ADD CONSTRAINT `vat_product_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
